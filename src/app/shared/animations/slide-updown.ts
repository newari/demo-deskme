import { trigger, state, style, transition,
    animate, group, query, stagger, keyframes
} from '@angular/animations';

export const SlideUpDownAnimation = [
    trigger('slideUpDown', [
        state('*', style({
            opacity: '0',
            overflow: 'hidden',
            height: '0px',
        })),
        state('1', style({
          overflow: 'hidden',
          height: '*',
          opacity: '1',
        })),
        state('0', style({
          opacity: '0',
          overflow: 'hidden',
          height: '0px',
        })),
        transition('* => 1', animate('400ms ease-in-out')),
        transition('0 => 1', animate('400ms ease-in-out')),
        transition('1 => 0', animate('400ms ease-in-out'))
    ])
]
