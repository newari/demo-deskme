import { Routes, CanActivateChild } from '@angular/router';

import { StudentDahsboardComponent } from './app.component';

export const APP_ROUTES:Routes=[
	{
		path: '',
		component:StudentDahsboardComponent,
		children: [
            { path: '',
            // loadChildren: './activities/home/home.activity.sdh#StudentDashboardHomeActivity'
            loadChildren: () => import('./activities/home/home.activity.sdh').then(m => m.StudentDashboardHomeActivity)
        },
            { path: 'home',loadChildren: () => import('./activities/home/home.activity.sdh').then(m => m.StudentDashboardHomeActivity) },
			{ path: 'dashboard', loadChildren: () => import('./activities/home/home.activity.sdh').then(m => m.StudentDashboardHomeActivity) },
		]
	}
]
