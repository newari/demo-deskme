import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";

import { StudentDahsboardComponent } from './app.component';

@NgModule({
    declarations: [StudentDahsboardComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[]
})
export class StudentDashboardAppModule { }
