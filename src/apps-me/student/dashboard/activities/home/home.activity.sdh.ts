import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentDashboardHomeComponent } from './home.component';
import { StudentProfileCardModule } from "../../../shared/modules/student-profile-card/student-profile-card.module";
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FilemanagerService } from "../../../../../lib/components/filemanager/filemanager.service";
import { NotifierModule } from "../../../../../lib/components/notifier/notifier.module";
import { FileinputModule } from "../../../../../lib/components/filemanager/fileinput.module";
import { FormService } from "../../../../../lib/services/form.service";
import { StudentPanelService } from "../../../../../lib/services/student-panel.service";
import { SettingsService } from "../../../../../lib/services/settings.service";
// import exportedComponent from './index.component';
import { ProductService } from "../../../../../lib/services/product.service";
import { TestMentService } from "../../../../../lib/services/testment.service";
import { TestseriesService } from "../../../../../lib/services/testseries.service";
import { EventService } from "../../../../../lib/services/event.service";
import { StudentService } from "../../../../../lib/services/student.service";
import { NewsCategoryService } from "../../../../../lib/services/newscategory.service";
import { GalleriaModule } from 'primeng/galleria';

export const ROUTES:Routes=[
    {path: '', component: StudentDashboardHomeComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentDashboardHomeComponent],
    imports:[
        StudentProfileCardModule,
        // GalleriaModule,
        CommonModule,
        RouterModule.forChild(ROUTES),
        FormsModule,
        ReactiveFormsModule,
        NotifierModule,
        FileinputModule,
        GalleriaModule
        // DialogModule,
        // TabViewModule,
        // CalendarModule
    ],
    providers: [StudentService,StudentPanelService, FilemanagerService, FormService, SettingsService, ProductService,TestMentService, TestseriesService, EventService, NewsCategoryService]
})
export class StudentDashboardHomeActivity { }
