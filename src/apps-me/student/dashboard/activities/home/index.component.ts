import { StudentDashboardHomeComponent } from "./home.component";
import { ClientService } from '../../../../../lib/services/client.service';
var x:any=ClientService.getClient();

var getComponent=function(clientConfig:any){
    if(!clientConfig||!clientConfig.studentZoneTheme){
        return StudentDashboardHomeComponent;
    }
    switch (clientConfig.studentZoneTheme) {
        default:
            return StudentDashboardHomeComponent
    }
}
const exportedComponent =  getComponent(x);

export default exportedComponent;
