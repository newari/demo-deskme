import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentCourseBoxLectureComponent } from './lecture.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { DialogModule } from 'primeng/primeng';
import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';
import { VideoService } from '../../../../../lib/services/video.service';
export const ROUTES:Routes=[
    {path: '', component: StudentCourseBoxLectureComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentCourseBoxLectureComponent],
    imports:[
        VgCoreModule,
        VgControlsModule,
        VgOverlayPlayModule,
        VgBufferingModule,
        CommonModule,
        DialogModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[VideoService]
})
export class StudentCourseBoxLectureActivity { }