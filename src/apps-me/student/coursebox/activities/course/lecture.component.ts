import { Component, ElementRef, ViewChild } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { VgDASH } from 'videogular2/src/streaming/vg-dash/vg-dash';
import { BitrateOption } from '../../../../../../node_modules/videogular2/core';
// import Player from '@vimeo/player';
import { DomSanitizer } from '@angular/platform-browser';
import { VideoService } from '../../../../../lib/services/video.service';
import { Video } from '../../../../../lib/models/video.model';

@Component({
  templateUrl: './lecture.component.html'
})
export class StudentCourseBoxLectureComponent {
    panelLoader:string;
		sessionStd:any;
		video:Video;
		vidUrl:any;
    constructor(
      	private authService: AuthService,
        private notifier: NotifierService,
				private router:Router,
				private sanitizer:DomSanitizer,
				private activatedRoute:ActivatedRoute,
				private videoService:VideoService
    ){
        
    }

	ngOnInit(){

		this.sessionStd=this.authService.student();
		this.activatedRoute.params.subscribe((params: Params) => {
			let videoId = params['lectureId'];
			this.loadVideo(videoId);
			
		});
		
	}
	loadVideo(id){
			this.videoService.getOneVideo(id).subscribe(
				vid=>{
					this.video=vid;
					this.vidUrl=this.sanitizer.bypassSecurityTrustResourceUrl('https://hub.iesmaster.org/api/video-player/vimeo/'+vid.videoSourceId+"?uid="+this.sessionStd.id);
				},
				err=>{

				}
			)
	}
	
	

}
