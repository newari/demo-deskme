import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentCourseBoxHomeComponent } from './home.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { DialogModule } from 'primeng/primeng';
import { VideoService } from '../../../../../lib/services/video.service';
export const ROUTES:Routes=[
    {path: '', component: StudentCourseBoxHomeComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentCourseBoxHomeComponent],
    imports:[
        CommonModule,
        DialogModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[VideoService]
})
export class StudentCourseBoxHomeActivity { }