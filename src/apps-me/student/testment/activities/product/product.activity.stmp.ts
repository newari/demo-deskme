import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TestMentProductContent } from './product';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { FilemanagerService } from '../../../../../lib/components/filemanager/filemanager.service';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TestseriesService } from "../../../../../lib/services/testseries.service";
import { OrderPaymentModule } from "../../../../../lib/components/order-payment/order-payment.module";
import { OrderService } from "../../../../../lib/services/order.service";
import { StudentFullOrderFormModule } from "../../../shared/modules/full-order-form/full-order-form.module";
import { StudentOrderPaymentModule } from "../../../shared/modules/order-payment/order-payment.module";
import { ProductService } from "../../../../../lib/services/product.service";
import { ClientService } from "../../../../../lib/services/client.service";
import { PaymentService } from "../../../../../lib/services/payment.service";
import { StudentService } from "../../../../../lib/services/student.service";
import { DialogModule } from 'primeng/dialog';
import { FieldsetModule } from 'primeng/fieldset';

export const ROUTES:Routes=[
    {path: '', component: TestMentProductContent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [TestMentProductContent],
    imports:[
        CommonModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        FieldsetModule,
        RouterModule.forChild(ROUTES),
        StudentOrderPaymentModule,
        FieldsetModule
    ],
    providers: [FilemanagerService,TestseriesService,OrderService,ProductService,ClientService,PaymentService, StudentService]
})
export class StudentTestMentProductActivity { }
