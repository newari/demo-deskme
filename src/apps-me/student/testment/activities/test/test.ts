import { Component } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { TestMentService } from '../../../../../lib/services/testment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EdukitConfig } from '../../../../../ezukit.config';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TmFeedbackService } from '../../../../../lib/services/tmfeedback.service';
@Component({
  templateUrl: './test.html'
})
export class TestMentTestContent{
	userTestSeries;
	feedbackForm : FormGroup;
	panelLoader ="none";
	testseriesId = null;
	activeCourse;
	activeTestSeries;
	courses:any;
	courseIds:string[]=[];
	sessionStd;
	testOpened = false;
	basicAPI;
	currentDate = new Date();
	preDisVideoURL : any;
	displayPreDisVideoModal : boolean =false;
	activeTest : any;
	activeVideo : number =0;
	displayFeedbackModal : boolean;
	fbSubjects : any;
	test : any;
	displayTestModal : boolean;
    constructor(
      	private testmentService:TestMentService,
      	private authService: AuthService,
		private notifier: NotifierService,
		private router : Router,
		private activatedRoute: ActivatedRoute,
		private sanitizer : DomSanitizer,
		private tmFeedbackService : TmFeedbackService,
		private fb : FormBuilder
    ){}
    ngOnInit(){
		this.activatedRoute.queryParams.subscribe(params=>{
			this.testseriesId = params['tsId']
		})
		this.feedbackForm= this.fb.group({
			type : ['',Validators.required],
			user : ['',Validators.required],
			subject :['',Validators.required],
			testSeries : ['',Validators.required],
			stream : ['',Validators.required],
			query : ['',Validators.required],
		});
		this.sessionStd=this.authService.student();
		this.loadTests();
		this.basicAPI = EdukitConfig.BASICS.API_URL;
		// // var decodedCookie = decodeURIComponent(document.cookie);
		// let token = window.localStorage.getItem('authToken');
		// document.cookie="TOKEN = "+token;
    }
	loadTests(){
		this.panelLoader ="show";
		this.testmentService.getUserOnlineTests(this.sessionStd.id,{isExpired : true}).subscribe(
		  res=>{
				this.courseIds = [];
				this.userTestSeries=[];
				this.userTestSeries=res;
				this.courses={};
				if(this.userTestSeries.length>0){
					for(let ci=0; ci<res.length; ci++){
						if(this.testseriesId == res[ci].tsDetail._id){
							this.activeCourse = res[ci].course._id;
							this.activeTestSeries = res[ci].tsDetail._id;
						}
						if(!this.courses[res[ci].course._id]){
							this.courses[res[ci].course._id]=res[ci].course;
							this.courseIds.push(res[ci].course._id);
						}
					}
					if(this.testseriesId == null){
						this.activeCourse = this.courseIds[0];
						this.activeTestSeries = this.userTestSeries[0].tsDetail._id;
					}
				}
				this.testOpened=false;
				this.panelLoader ="none";
			},
			err=>{
				this.panelLoader ="none";
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	testWindow(data) {
        // const remote = require('electron').remote;
		let view='w';
        if(screen.width < 672){
            view='m';
        }
		let url = this.basicAPI +'/student/testment/testwindow/'+data.test.theme+'/'+view+'/'+data._id;
		let params  = 'width='+screen.width+',height='+screen.height+',top=0,left=0,fullscreen=yes,resizable=0';
		let newWindow=window.open(url,'EXAM',params);
		this.testOpened = true;
		if (window.focus) {newWindow.focus()}
		return false;
	}
	setQueryParams(ts){
		this.testseriesId=ts;
		this.router.navigate(['/student/testment/test/'], { queryParams: {tsId: this.testseriesId}});
	}
	// showPreDisVideoModal(url){
	// 	this.displayPreDisVideoModal = true;
	// 	this.preDisVideoURL=this.sanitizer.bypassSecurityTrustResourceUrl(url);
	// }
	getVideoURL(videoId,source){
		let url = '';
		if(source ==='youtube'){
			url = 'https://www.youtube.com/embed/' + videoId;
		}else if(source ==='vimeo'){
			url = 'https://player.vimeo.com/video/' + videoId;
		}
		return this.sanitizer.bypassSecurityTrustResourceUrl(url);
	}
	showPreDisVideosModal(data){
		this.displayPreDisVideoModal = true;

		this.activeTest = data;
		this.preDisVideoURL= this.getVideoURL(this.activeTest.test.preDiscussionVideos.ids[0],this.activeTest.test.preDiscussionVideos.source);
	}
	changeVideoURL(index,videoId,source){
		this.activeVideo = index;
		this.preDisVideoURL = this.getVideoURL(videoId,source);
	}
	ShowFeedbackModal(testSeries){
		this.displayFeedbackModal=true;
		let patchValue : any ={
			type: 'TEST_SERIES',
			user : this.sessionStd.id,
			testSeries : testSeries._id,
			stream : testSeries.stream,
		};
		this.feedbackForm.patchValue(patchValue);
		if(!this.fbSubjects){
			this.loadFbSubjects();
		}
	}
	loadFbSubjects(){
		let filter={type: 'TEST_SERIES',status : true,limit : 'all'};
		this.panelLoader = 'show';
		this.tmFeedbackService.getFbSubjects(filter,true).subscribe(
			res=>{
				this.fbSubjects=res.body;
				this.panelLoader = 'none';
			},
			err=>{
				this.notifier.alert(err.code,err.message,'danger',2000);
				this.panelLoader = 'none';
			}
		);
	}
	submitFeedback(){
		let formData=this.feedbackForm.value;
		this.panelLoader = 'show';
		this.tmFeedbackService.addFeedbackQs(formData).subscribe(
			res=>{
				this.panelLoader = 'none';
				this.displayFeedbackModal=false;
				this.feedbackForm.reset();
				this.notifier.alert('Success','Feedback Sent Successfully','success',2000);
			},
			err=>{
				this.notifier.alert(err.code,err.message,'danger',2000);
				this.panelLoader = 'none';
			}
		);
	}
	viewTest(test){
		this.displayTestModal = true;
		this.test=test;
	}
}
