import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TestMentTestContent } from './test';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { ConventionalExamService } from '../../../../../lib/services/conventional-exam.service';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TestMentService } from "../../../../../lib/services/testment.service";
import { TmFeedbackService } from "../../../../../lib/services/tmfeedback.service";
import { TestseriesService } from "../../../../../lib/services/testseries.service";
import { SafeHtmlPipeModule } from "../../../../../lib/filters/safehtml.pipe";
import { DialogModule } from 'primeng/dialog';
import { AccordionModule } from 'primeng/accordion';
import { TabViewModule } from 'primeng/tabview';
import { FieldsetModule } from 'primeng/fieldset';
export const ROUTES:Routes=[
    {path: '', component: TestMentTestContent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Test'}},
];
@NgModule({
    declarations: [TestMentTestContent],
    imports:[
        CommonModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ROUTES),
        AccordionModule,
        TabViewModule,
        FieldsetModule,
        SafeHtmlPipeModule
    ],
    providers: [ConventionalExamService,TestMentService,TmFeedbackService,TestseriesService]
})
export class StudentTestMentTestActivity { }
