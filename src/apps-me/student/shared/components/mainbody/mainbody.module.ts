import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { StudentPanelMainbody } from './student-panel-mainbody';
import { StudentSidebarModule } from '../sidebar/sidebar.module';
import { CommonModule } from '@angular/common';
import { StudentTopbarModule } from '../topbar/topbar.module';

@NgModule({
    declarations:[StudentPanelMainbody],
    imports:[StudentSidebarModule, StudentTopbarModule, RouterModule, CommonModule],
    exports:[StudentPanelMainbody]
})
export class StudentMainbodyModule{
    
}