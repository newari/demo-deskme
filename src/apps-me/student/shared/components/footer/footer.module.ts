import { NgModule } from '@angular/core';
import { StudentPanelFooter } from './student-panel-footer';

@NgModule({
    declarations:[StudentPanelFooter],
    exports:[StudentPanelFooter]
})
export class StudentFooterModule{
    
}