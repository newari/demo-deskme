import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentPanelTopbar } from './student-panel-topbar';
import { ClientService } from '../../../../../lib/services/client.service';

@NgModule({
    declarations:[StudentPanelTopbar],
    imports:[CommonModule, RouterModule],
    exports:[StudentPanelTopbar]
})
export class StudentTopbarModule{
    
}