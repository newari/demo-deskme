import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { ClientConfig } from '../../../../../lib/models/clientconfig.model';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';
import { ClientService } from '../../../../../lib/services/client.service';

@Component({
    selector: 'student-panel-topbar',
    templateUrl: './student-panel-topbar.html'
})
export class StudentPanelTopbar implements OnInit { 
    sidebarState='CLOSED';
    sideMenu:any;
    stdMenu:any[];
    apps:any[];
    errorMsg:string;
    menuItems:any;
    stdApps:string[];
    @Input() client:ClientConfig;
    constructor(private authService:AuthService, private notifier:NotifierService, private clientService:ClientService){
        this.sideMenu={
            "LIBRARY":{
                "title":'Library',
                "url":'',
                "modules":[],
                "menuItems":[
                    {
                        title:"Dashboard",
                        path:"/ims/library/dashboard",
                        activity:'DASHBOARD',
                        action:'LIST'
                        
                    },
                    {
                        title:"Issue Book",
                        path:"/ims/library/issue-book",
                        activity:'DASHBOARD',
                        action:'LIST'
                        
                    },
                    {
                        title:"Books",
                        path:"/ims/videovibe/video",
                        activity:'DASHBOARD',
                        action:'LIST'
                        
                    }
                ]
            },
            "TESTMENT": {
                "title": 'Online Test Series',
                "url": '',
                "modules": [],
                "menuItems": [
                    {
                        title: "Dashboard",
                        path: "/student/testment/dashboard",
                        activity: 'DASHBOARD',
                        action: 'LIST'
                    },
                    {
                        title: "Test Series",
                        path: "/student/testment/testseries",
                        activity: 'DASHBOARD',
                        action: 'LIST'
                    },
                    {
                        title: "Report",
                        path: "/student/testment/report",
                        activity: 'DASHBOARD',
                        action: 'LIST'
                    },
                    {
                        title: "Bookmark Questions",
                        path: "/student/testment/bookmark",
                        activity: 'DASHBOARD',
                        action: 'LIST'
                    }
                ]
            },
            "CONVENTIONAL_EXAM":{
                "title":'Conventional Exam',
                "url":'',
                "modules":[],
                "menuItems":[
                    {
                        title:"Online Tests",
                        path:"/student/conventional-exam",
                        icon:'glyphicon glyphicon-duplicate'
                    },
                    {
                        title:"Offline Tests",
                        path:"/student/conventional-exam/offline-test",
                        icon:'glyphicon glyphicon-duplicate'
                    }
                ]
            },
            "CLASS_TEST": {
                "title": 'Class Test',
                "url": '',
                "modules": [],
                "menuItems": [
                    {
                        title: "Class Tests",
                        path: "/student/class-test",
                        icon: 'glyphicon glyphicon-duplicate'
                    } 
                ]
            },
            "FORUM":{
                "title":'Doubt Forum',
                "url":'',
                "modules":[],
                "menuItems":[
                    {
                        title:"Ask Question",
                        path:"/student/forum/question/ask",
                        icon:"glyphicon glyphicon-question-sign link-icon",
                        activity:'QUESTION',
                        action:'ADD'
                        
                    },
                    {
                        title:"My Questions",
                        path:"/student/forum/question",
                        icon:"glyphicon glyphicon-question-sign link-icon",
                        activity:'QUESTION',
                        action:'LIST'
                        
                    }
                ]
            },
            "COURSEBOX":{
                "title":'Online Courses',
                "url":'',
                "modules":[],
                "menuItems":[
                    {
                        title:"Interview Guidance",
                        path:"/student/courses",
                        icon:'glyphicon glyphicon-book'
                    }
                ]
            },
            "COURSE_CREATOR":{
                "title":'Online Courses',
                "url":'',
                "modules":[],
                "menuItems":[
                    {
                        title:"My Courses",
                        path:"/student/online-courses/my-courses",
                        icon:'glyphicon glyphicon-book'
                    },
                    {
                        title:"Buy Courses",
                        path:"/student/online-courses/buy-courses",
                        icon:'glyphicon glyphicon-book'
                    }
                ]
            },
            "BOOK_STORE":{
                "title":'Library',
                "url":'',
                "modules":[],
                "menuItems":[
                    {
                        title:"My Books",
                        path:"/student/library",
                        icon:'glyphicon glyphicon-book'
                    }
                ]
            },
        }
    }
    ngOnInit(){
        let student=this.authService.student();
        if(!student){
            this.authService.logout();   
        }
        if(student.apps&&student.apps.student){
            this.stdMenu=student.apps.student;
        }
        this.loadClientApps();
    }
    toggleSidebar(){
        if(this.sidebarState=="OPENED"){
            this.sidebarState="CLOSED";
        }else{
            this.sidebarState="OPENED";
        }
    }

    setSideMenu(clientApps){
        let mItems={};
        let stdApps:string[]=[];
        let apsLen=clientApps.length;
        for(let ai=0; ai<apsLen; ai++){
            let ca=clientApps[ai];
            let appCode=ca.app.code;
            if(this.sideMenu[appCode]&&(this.stdMenu.indexOf(appCode)>-1||ca.defaultInStudentpanel)&&!mItems[appCode]){
                stdApps.push(appCode);
                mItems[appCode]=this.sideMenu[appCode];
            }
            
        }
        this.stdApps=stdApps;
        this.menuItems=mItems;
    }
    loadClientApps(){
        this.clientService.getApps().subscribe(
            res=>{
                this.apps=res;
                this.setSideMenu(res);
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);

            }
        )
    }

    
}