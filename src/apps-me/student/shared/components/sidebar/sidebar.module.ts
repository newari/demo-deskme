import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentPanelSidebar } from './student-panel-sidebar';
import { ClientService } from '../../../../../lib/services/client.service';

@NgModule({
    declarations:[StudentPanelSidebar],
    imports:[CommonModule, RouterModule],
    exports:[StudentPanelSidebar]
})
export class StudentSidebarModule{
    
}