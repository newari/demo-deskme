import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";

import { EdukitConfig } from '../../../../ezukit.config';

import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class StudentPanelService{
    constructor(private http:HttpClient){ }
    getDashboardSlider() :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-panel/config/getDashboardSlider");
    }
    setDashboardSlider(opt) :Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/student-panel/config/setDashboardSlider", opt);
    }

    getDashboardAdGroup() :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-panel/config/getDashboardAdGroup");
    }
    setDashboardAdGroup(opt) :Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/student-panel/config/setDashboardAdGroup", opt);
    }

    uploadESEAdmitCard(data):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/core/formresponse",data);
    }
    getESEAdmitCardDetails(userId,form):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL +"/core/formresponse/ese-admit-card",{params:{userId:userId,form:form}});
    }

    getBatchFeed(data):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/cmn/batch-feeds",{params:data})
    }

    getBatchNotes(batchId,filter?:any,withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter}
        if(withHeaders){
            opts.observe="response";
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/space/"+batchId+"/notes",opts);
    }
    getBatchArticles(batchId,filter?:any,withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter}
        if(withHeaders){
            opts.observe="response";
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/space/"+batchId+"/articles",opts);
    }
    getBatchAssignments(batchId,filter?:any,withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter}
        if(withHeaders){
            opts.observe="response";
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/space/"+batchId+"/assignments",opts);
    }
    getBatchQuizs(batchId,filter?:any,withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter}
        if(withHeaders){
            opts.observe="response";
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/space/"+batchId+"/quizzes",opts);
    }


    getBatchSubjectsAndFaculties(batchId:string): Observable<any>{
        return this.http.get(`${EdukitConfig.BASICS.API_URL}/admin/batch/${batchId}/subjects-and-faculties`)
    }
}
