import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StudentFullOrderComponent } from './full-order-form.component';
import { UserService } from '../../../../../lib/services/user.service';
import { FileinputModule } from '../../../../../lib/components/filemanager/fileinput.module';
import { UserSearchFormModule } from '../../../../../lib/components/user-search-form/user-search-form.module';
import { SessionService } from '../../../../../lib/services/session.service';
import { CoptionService } from '../../../../../lib/services/coption.service';
import { CenterService } from '../../../../../lib/services/center.service';
import { BatchService } from '../../../../../lib/services/batch.service';
import { ProductService } from '../../../../../lib/services/product.service';
import { StudentService } from '../../../../../lib/services/student.service';
import { OrderDataService } from '../../services/order-data.service';
import { StudentOrderPaymentModule } from '../order-payment/order-payment.module';
import { TabViewModule } from 'primeng/tabview';


@NgModule({
    declarations:[StudentFullOrderComponent],
    imports: [FileinputModule, TabViewModule, StudentOrderPaymentModule, UserSearchFormModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[StudentFullOrderComponent],
    providers: [UserService, SessionService, CoptionService, CenterService, BatchService, ProductService, StudentService, OrderDataService]
})
export class StudentFullOrderFormModule{

}
