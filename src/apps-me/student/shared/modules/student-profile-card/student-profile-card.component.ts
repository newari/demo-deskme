import { OnInit, Component, Input } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { Student } from '../../../../../lib/models/student.model';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';
import { StudentService } from '../../../../../lib/services/student.service';

@Component({
    selector:'ek-student-profile-card',
    templateUrl:'./student-profile-card.component.html'
})
export class StudentProfileCardComponent implements OnInit {
    panelLoader:string="none";
    student:Student;
    constructor(
        private notifier:NotifierService,
        private studentService:StudentService
    ){

    }

    ngOnInit():void{
        this.panelLoader="show";
        this.studentService.getSessionStudent({populateSession:true, populateCourse:true, populateBatch:true}).subscribe(
            res=>{
                this.panelLoader="none";
                this.student=res;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }

}