import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForumQuestionService } from '../../services/forum-question.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AskForumQuestion } from './ask-question.component';
import { SubjectService } from '../../../../../lib/services/subject.service';
import { TopicService } from '../../../../../lib/services/topic.service';
import { QuillEditorModule } from '../../../../../lib/components/ngx-quill-editor';
import { SettingsService } from '../../../../../lib/services/settings.service';

@NgModule({
    declarations:[AskForumQuestion],
    imports:[CommonModule, QuillEditorModule, FormsModule, ReactiveFormsModule],
    exports:[AskForumQuestion],
    providers:[ForumQuestionService, SubjectService, TopicService,SettingsService]
})
export class AskForumQuestionModule{

} 