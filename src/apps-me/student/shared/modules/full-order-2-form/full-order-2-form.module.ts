import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StudentRegForm2Component } from './full-order-2-form.component';
import { UserService } from '../../../../../lib/services/user.service';
import { FileinputModule } from '../../../../../lib/components/filemanager/fileinput.module';
import { UserSearchFormModule } from '../../../../../lib/components/user-search-form/user-search-form.module';
import { SessionService } from '../../../../../lib/services/session.service';
import { CoptionService } from '../../../../../lib/services/coption.service';
import { CenterService } from '../../../../../lib/services/center.service';
import { BatchService } from '../../../../../lib/services/batch.service';
import { ProductService } from '../../../../../lib/services/product.service';
import { StudentService } from '../../../../../lib/services/student.service';
import { OrderDataService } from '../../services/order-data.service';
import { StudentOrderPaymentModule } from '../order-payment/order-payment.module';
import { SettingsService } from '../../../../../lib/services/settings.service';
import { ClientService } from '../../../../../lib/services/client.service';
import { FieldsetModule } from 'primeng/fieldset';
import { TabViewModule } from 'primeng/tabview';
import { CalendarModule } from 'primeng/calendar';


@NgModule({
    declarations: [StudentRegForm2Component],
    imports: [FileinputModule,FieldsetModule, TabViewModule, StudentOrderPaymentModule, CalendarModule, UserSearchFormModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports: [StudentRegForm2Component],
    providers: [UserService, SettingsService, ClientService, SessionService, CoptionService, CenterService, BatchService, ProductService, StudentService, OrderDataService]
})
export class StudentFullOrder2FormModule {

}
