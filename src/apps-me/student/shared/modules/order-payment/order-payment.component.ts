import { OnInit, Component, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthService } from '../../../../../lib/services/auth.service';
import { User } from '../../../../../lib/models/user.model';
import { EdukitConfig } from '../../../../../ezukit.config';
import { Product } from '../../../../../lib/models/product.model';
import { OrderService } from '../../../../../lib/services/order.service';
import { ProductService } from '../../../../../lib/services/product.service';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';
import { ClientService } from '../../../../../lib/services/client.service';
import { PaymentService } from '../../../../../lib/services/payment.service';
import * as moment from 'moment';
@Component({
    selector:'ek-student-order-payment',
    templateUrl:'./order-payment.component.html'
})
export class StudentOrderPaymentComponent implements OnInit {
    panelLoader:string="none";
    user:User;
    paymentMethod:string=null;
    paymentOption:string=null;
    activePmtOptionIndex:number=null;
    pmtOptions:any[]=[];
    formStatus:string='Normal';
    errorMsg:string=null;
    orderProduct:Product;
    client:any;
    discountVoucherForm: FormGroup;
    discountDetail:any;
    discountModalDisplay:boolean
    @Input('order') order:any; //order_id or order model
    discountCouponForm: FormGroup;
    showDiscountModalForm: boolean;
    promocode:any;
    paid:boolean;
    fullPaymentDiscountIsApplicable:boolean;
    totalDiscount: any;
    constructor(
        private fb:FormBuilder,
        private orderService:OrderService,
        private authService:AuthService,
        private productService:ProductService,
        private notifier:NotifierService,
        private clientService:ClientService,
        private paymentService : PaymentService
    ){

    }
    ngOnInit():void{
        this.user=this.authService.student();
        if(!this.order){
            return;
        }
        this.discountVoucherForm= this.fb.group({
            discountCode:['', Validators.required],
            type:['PROMOCODE'],

        });
        this.discountCouponForm=this.fb.group({
			type:['COUPON'],
			discountCode:['', Validators.required]
		})
        this.loadClient();
        this.getPaymentOptions();
        this.getProduct(this.order.products[0]);

    }
    getProduct(productId) {
        this.productService.getOneProduct(productId).subscribe(
            res=>{
                this.orderProduct=res;

            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    loadClient(){
        this.clientService.getClientConfig().subscribe(
            res=>{
                this.client=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }
    getPaymentOptions(){
		let data={
            order : this.order.id,
            sort:"createdAt ASC"
		};
		this.paymentService.getOrderPaymentNodes(data).subscribe(
			(res)=>{
				this.order.availablePaymentOptions=res;
			},
			(err)=>{this.notifier.alert(err.code, err.message, "danger", 5000);}
		);
	}
    // ngOnChanges(changes: SimpleChanges): void {
    //     if(changes['order']) {
    //         if(this.order&&this.order.availablePaymentOptions){
    //             let disabled:boolean=false;
    //             for(let i=0; i<this.order.availablePaymentOptions.length; i++){
    //                 if(this.order.availablePaymentOptions[i].removed){
    //                     continue;
    //                 }else if(!this.order.availablePaymentOptions[i].paid||this.order.availablePaymentOptions[i].paid<this.order.availablePaymentOptions[i].amount){
    //                     this.pmtOptions.push({index:i,title:this.order.availablePaymentOptions[i].title,amount:this.order.availablePaymentOptions[i].amount, disabled:disabled});
    //                     disabled=true;
    //                 }else{
    //                     this.pmtOptions.push({index:i,title:this.order.availablePaymentOptions[i].title+" (Paid)", amount:this.order.availablePaymentOptions[i].amount, disabled:true});
    //                 }
    //             }
    //         }
    //     }
    // }
    processPayment(){
        if(this.orderProduct.status==false){
            this.notifier.alert("ERROR", "Order payment can not proceed!", "danger", 10000);
            return;
        }
        let storeId='';
        let payableAmount=0;
        let paymentNodeId='';
        let paymentOptionTitle='';
        if(this.order.availablePaymentOptions.length&&this.activePmtOptionIndex==this.order.availablePaymentOptions.length ){
            storeId=this.order.store.id;
            payableAmount= this.getTotal(this.order.availablePaymentOptions);
            paymentNodeId= 'FullPaymentOption';
            paymentOptionTitle="Full Amount"
        }else{
            if(!this.order.availablePaymentOptions[this.activePmtOptionIndex]){
                alert("Please select payment option first!");
                return;
            }
            this.formStatus="Processing";
             storeId=this.order.availablePaymentOptions[this.activePmtOptionIndex].store;
             payableAmount=(this.order.availablePaymentOptions[this.activePmtOptionIndex].amount+(this.order.availablePaymentOptions[this.activePmtOptionIndex].extraAmount||0))-(this.order.availablePaymentOptions[this.activePmtOptionIndex].discountTotal||0)-(this.order.availablePaymentOptions[this.activePmtOptionIndex].paid||0);
             paymentNodeId=this.order.availablePaymentOptions[this.activePmtOptionIndex].id;
             paymentOptionTitle=this.order.availablePaymentOptions[this.activePmtOptionIndex].title;
             // this.order.lastPmtOptIndex=this.activePmtOptionIndex;
        }
        if(this.totalDiscount){
            payableAmount= payableAmount-this.totalDiscount;
        }

        this.orderService.updateOrder(this.order.id, {lastPmtOptIndex:this.activePmtOptionIndex, paymentOptionAmt:payableAmount, paymentOption:paymentOptionTitle}).subscribe(
            res=>{
                if(payableAmount<0.1){
                    window.location.href=this.client.baseUrl+"/api/sales/order/online-free-payment-response/"+storeId+"?orderId="+this.order.id;
                    // window.location.href=this.client.baseUrl+"/api/api/sales/order/make-online-payment?storeId="+storeId+"&orderId="+this.order.id+"&orderNo="+this.order.orderNo+"&totalAmount="+payableAmount+"&redirectUrl="+this.client.baseUrl+"/api/api/sales/order/online-payment-response/"+storeId+"&cancelUrl="+this.client.baseUrl+"/api/api/sales/order/online-payment-cancel-response/"+storeId+"&responsePreviewUrl="+this.client.baseUrl+"/api/api/sales/order/online-payment-response/"+storeId;
                }
                else{
                    window.location.href=this.client.baseUrl+"/api/sales/order/make-online-payment?storeId="+storeId+"&orderId="+this.order.id+"&orderNo="+this.order.orderNo+"&totalAmount="+payableAmount+"&nodeId="+paymentNodeId+"&redirectUrl="+this.client.baseUrl+"/api/sales/order/online-payment-response/"+storeId+"&cancelUrl="+this.client.baseUrl+"/api/sales/order/online-payment-cancel-response/"+storeId+"&responsePreviewUrl="+this.client.baseUrl+"/api/sales/order/online-payment-response/"+storeId;
                }
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
                this.formStatus="Normal";
            }
        )

    }
    getTotal(availablePaymentOptions: any): number {
        let total=0;
        for (let index = 0; index < availablePaymentOptions.length; index++) {
            total+=availablePaymentOptions[index].amount-(availablePaymentOptions[index].paid||0);
        }
        return total;
    }
    applyVoucherDiscount(){
        if(!this.discountVoucherForm.valid){
            return;
        }
        this.paymentService.addDiscount(this.order.id, {discountDetail:this.discountVoucherForm.value}).subscribe(
            res=>{
                this.discountDetail=res;
                this.discountModalDisplay=false;
                this.getPaymentOptions();
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    addDiscount(){
		if(!this.discountCouponForm.valid) return;
		if(this.order.discountDetail&&this.order.discountDetail.discountId){
			this.notifier.alert("ERROR", "You have already used a discount code", 'danger', 5000);
			return;
		}
		this.paymentService.addDiscount(this.order.id,{discountDetail:this.discountCouponForm.value}).subscribe(
			res=>{
				this.notifier.alert("Success", "Discount applies successfully", 'success', 500)
				this.loadOrder(this.order.id);
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
    }
    loadOrder(orderId){
        this.orderService.getOneOrder(orderId).subscribe(
            res=>{
                this.order=res;
                if(this.order.total<1){

                }
                this.getPaymentOptions();
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    showDiscountModal(){
		this.showDiscountModalForm=true;
    }

    applyPromocode(){
        if(!this.promocode) return ;
        this.discountVoucherForm.patchValue({discountCode:this.promocode});
        if(!this.discountVoucherForm.valid) return;
        this.formStatus="Proccesing";
        this.paymentService.addDiscount(this.order.id, {discountDetail:this.discountVoucherForm.value}).subscribe(
            res=>{
                if(res&&res.done){
                    this.paid=true;
                }
                this.loadOrder(this.order.id);
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );

    }
    setDiscount(activePmtOptionIndex){

        if(this.order.availablePaymentOptions&&this.order.availablePaymentOptions[activePmtOptionIndex].title.trim()=='Tuition Fee'){
            if(this.orderProduct&& this.orderProduct.discount&&this.orderProduct.discount.fullPaymentDiscount){
                console.log("Product have discount");

                if (this.order&&this.order.relBatch && this.order.relBatch.startDate&&this.order.relBatch.startDate!=="Invalid date") {
                    console.log(this.order.relBatch.startDate);
                    console.log(moment(this.order.relBatch.startDate,"DD/MM/YYYY").format());

                    let batchStartDate = new Date(moment(this.order.relBatch.startDate,"DD/MM/YYYY").format());
                    // let orderApprovedDate = new Date(this.order.createdAt);
                    // if (this.order.approvedAt) {
                    //      orderApprovedDate = new Date(this.order.approvedAt);
                    // }else{
                    //      orderApprovedDate = new Date(this.order.createdAt);
                    // }
                    // console.log(orderApprovedDate);
                    console.log("Batch Start Date",batchStartDate);

                    let discountWorkDate = moment(batchStartDate).add(5, 'day').toDate();
                    // if (moment(orderApprovedDate).isAfter(batchStartDate)){
                    //     discountWorkDate = moment(orderApprovedDate).add(5, 'day').toDate();
                    // }
                    let today = new Date();
                    if (moment(today).isBefore(discountWorkDate)) {
                        console.log("Discount Applicable");

                        this.order.discount = this.orderProduct.discount.fullPaymentDiscount;
                        this.totalDiscount = this.orderProduct.discount.fullPaymentDiscount;
                        this.order.discountDetail = {
                            discountTotal: this.orderProduct.discount.fullPaymentDiscount,
                            discountCode: "",
                            type: "",
                            discountType: "Full Payment Discount",
                            discountVal: 0,
                            reqDoc: "",
                            reqDocNo: "",
                            relProduct: this.orderProduct.id
                        }
                    }
                }
            }
        }
    }
}
