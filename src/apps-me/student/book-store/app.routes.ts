import { Routes, CanActivateChild } from '@angular/router';

import { BookStoreAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  { path: '',
    component:BookStoreAppComponent,
    children: [
      { path: '', loadChildren: './activities/home/home.activity.sbsh#StudentBookStoreHomeActivity' },
    ]
  }
]
