import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { BookstoreService } from '../../../../../lib/services/bookstore.service';

@Component({
  templateUrl: './home.component.html'
})
export class StudentBookStoreHomeComponent {
    subscribedBooks:any[];
    panelLoader:string;
	sessionStd:any;
    constructor(
      	private authService: AuthService,
        private notifier: NotifierService,
      	private bsService:BookstoreService
    ){
        
    }

    ngOnInit(){
		this.sessionStd=this.authService.student();
		this.loadBooks(this.sessionStd.id);
	}
	loadBooks(userId){
		this.bsService.getUserBooks(userId).subscribe(
			res=>{
				this.subscribedBooks=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
	}

}
