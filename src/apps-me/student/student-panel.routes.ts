import {Routes} from "@angular/router";
import { StudentPanelComponent } from './student-panel.component';
import { StudentAuthGuard } from "./shared/services/student-auth-guard";

export const STUDENT_PANEL_ROUTES:Routes=[
  	{
	  	path: '',
		component:StudentPanelComponent,
		canActivate:[StudentAuthGuard],
		canActivateChild:[StudentAuthGuard],
    	children: [
              { path: '',
            //   loadChildren: './dashboard/student-dashboard-app.module#StudentDashboardAppModule'
              loadChildren: () => import('./dashboard/student-dashboard-app.module').then(m => m.StudentDashboardAppModule)
            },
              { path: 'forum',
            //   loadChildren: './forum/student-forum-app.module#StudentForumAppModule'
                loadChildren: () => import('./forum/student-forum-app.module').then(m => m.StudentForumAppModule)
            },
            // () => import('modulefile').then(m => m.StudentForumQuestionActivity)
      		// { path: 'course', loadChildren: './course/student-course-app.module#StudentCourseAppModule' },
      		{ path: 'profile', loadChildren: () => import('./profile/student-profile-app.module').then(m => m.StudentProfileAppModule) },
      		{ path: 'shop', loadChildren: () => import('./shop/student-shop-app.module').then(m => m.StudentShopAppModule) },
      		// { path: 'conventional-exam', loadChildren: './conventional-exam/conventional-exam-app.module#StudentConventionalExamAppModule' },
      		// { path: 'library', loadChildren: './book-store/book-store-app.module#StudentBookStoreAppModule' },
      		{ path: 'testment', loadChildren: () => import('./testment/testment-app.module').then(m => m.StudentTestMentAppModule) },
      		// { path: 'courses', loadChildren: './coursebox/coursebox-app.module#StudentCourseBoxAppModule' },
      		{ path: 'online-courses', loadChildren: () => import('./online-course/onlinecourse-app.module').then(m => m.StudentOnlineCourseAppModule) },
			// { path: 'class-test', loadChildren: './class-test/class-test-app.module#StudentClassTestAppModule' },
			// { path: 'resources', loadChildren: './resources/resources-app.module#StudentResourcesAppModule' },
			{ path:'batchfeeds' , loadChildren: () => import('./batchfeed/batchfeed-app.module').then(m => m.StudentBatchFeedAppModule)}

		]
  	}
]
