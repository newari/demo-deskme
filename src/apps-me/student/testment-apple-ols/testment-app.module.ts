import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";
import { TestMentAppComponent } from './app.component';

@NgModule({
    declarations: [TestMentAppComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[]
})
export class StudentTestMentAppModule { }
