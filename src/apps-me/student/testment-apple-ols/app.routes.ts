import { Routes, CanActivateChild } from '@angular/router';

import { TestMentAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  { path: '',
    component:TestMentAppComponent,
    children: [
      { path: '', loadChildren: './activities/home/home.activity.stmh#StudentTestMentHomeActivity' },
      // { path: '', loadChildren: './activities/testseries/testseries.activity.stmts#StudentTestMentTestSeriesActivity' },
      { path: 'dashboard', loadChildren: './activities/home/home.activity.stmh#StudentTestMentHomeActivity' },
      { path: 'test', loadChildren: './activities/test/test.activity.stmt#StudentTestMentTestActivity' },
      { path: 'report', loadChildren: './activities/report/report.activity.stmr#StudentTestMentReportActivity' },
      { path: 'overview', loadChildren: './activities/report/overview.activity.stmr#StudentTestMentOverViewActivity' },
      { path: 'bookmark', loadChildren: './activities/bookmark/questions.activity.stmq#StudentTestMentBookmarkActivity' },
      { path: 'testseries', loadChildren: './activities/testseries/testseries.activity.stmts#StudentTestMentTestSeriesActivity' },
      { path: 'feedback', loadChildren: './activities/feedback/list.activity.stmfb#StudentTestMentFeedbackActivity' },
      { path: 'product/:productId', loadChildren: './activities/product/product.activity.stmp#StudentTestMentProductActivity' }
    ]
  }
]
