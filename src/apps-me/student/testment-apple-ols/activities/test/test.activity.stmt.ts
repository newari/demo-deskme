import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TestMentTestContent } from './test';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { ConventionalExamService } from '../../../../../lib/services/conventional-exam.service';
import { DialogModule, AccordionModule, FieldsetModule, TabViewModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TestMentService } from "../../../../../lib/services/testment.service";
import { TmFeedbackService } from "../../../../../lib/services/tmfeedback.service";
export const ROUTES:Routes=[
    {path: '', component: TestMentTestContent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Test'}},
];
@NgModule({
    declarations: [TestMentTestContent],
    imports:[
        CommonModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ROUTES),
        AccordionModule,
        TabViewModule,
        FieldsetModule
    ],
    providers: [ConventionalExamService,TestMentService,TmFeedbackService]
})
export class StudentTestMentTestActivity { }