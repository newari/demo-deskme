import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { TestMentService } from '../../../../../lib/services/testment.service';
import { TestseriesService } from '../../../../../lib/services/testseries.service';
@Component({
  templateUrl: './testseries.html'
})
export class TestMentTestSeriesAppComponent{
	userTestSeries : any=[];
	panelLoader = "none";
	allTestSeries=[];
	courses:any;
	courseIds:string[];
	tsProducts : any=[];
	sessionStd;
    constructor(
      	private testmentService:TestMentService,
      	private authService: AuthService,
		private testseriesService: TestseriesService

    ){}
    ngOnInit(){
		this.sessionStd=this.authService.student();
		this.loadUserTestSeries(this.sessionStd.id);
		this.loadTSProduct();
    }
	loadUserTestSeries(userId){
		this.panelLoader = "show";
		this.testmentService.getUserTestSeries(userId).subscribe(
			res=>{							
				this.userTestSeries=res;
				this.panelLoader = "none";
			},
			err=>{
				this.panelLoader = "none";
				// this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	loadTSProduct(){
		this.panelLoader = "show";
		this.testseriesService.getTSProduct().subscribe(
			res=>{	
				this.tsProducts=res;
				this.panelLoader = "none";
			},
			err=>{
				this.panelLoader = "none";
				// this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
}
