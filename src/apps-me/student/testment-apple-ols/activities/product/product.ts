import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { FormGroup, FormBuilder } from '@angular/forms';
import { TestseriesService } from '../../../../../lib/services/testseries.service';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from '../../../../../lib/services/order.service';
import { Order } from '../../../../../lib/models/order.model';
import { ClientService } from '../../../../../lib/services/client.service';
import { PaymentService } from '../../../../../lib/services/payment.service';
import { StudentService } from '../../../../../lib/services/student.service';
import { Student } from '../../../../../lib/models/student.model';
@Component({
  templateUrl: './product.html'
})
export class TestMentProductContent{
	orderForm:FormGroup;
	order:any;
	panelLoader="none";
	tsProduct : any;
	productId;
	sessionStd;
	activeStep =1;
	testSeries=[];
	student:Student;
    constructor(
      	private authService: AuthService,
        private notifier: NotifierService,
		private fb:FormBuilder,
		private activatedRoute : ActivatedRoute,
		private testseriesService : TestseriesService,
		private orderService : OrderService,
		private clientService : ClientService,
		private paymentService : PaymentService,
		private studentService: StudentService

    ){}
    ngOnInit(){
		this.orderForm=this.fb.group({
			course:[null],
			product:[null],
			stream:[null],
			session:[null],
			store:[null],
			total :[],
			tracer:[{form:'STUDENT-TM-ORDER-ADD'}],
			discountDetail : [{}],
			relProductCategory:[''],
			relProductType:['']
		});
		this.activatedRoute.params.subscribe(params=>{
			this.productId = params['productId'];
		});
		this.sessionStd=this.authService.student();
		this.getSessionStudent();
		this.loadProduct();
		
    }
	getSessionStudent() {
		 this.studentService.getSessionStudent().subscribe(
			 res=>{
				this.student=res;
			 },
			 err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			 }
		 );
	}
	loadProduct(){
		this.testseriesService.getTsProductOne(this.productId).subscribe(
			res=>{	
				this.tsProduct=res;
				this.testSeries=this.tsProduct.sourceId;
				this.orderForm.patchValue(this.tsProduct);
				this.tsProduct.totalTests=0;
				for(var i = 0; i < this.testSeries.length; i++){
					this.tsProduct.totalTests += this.testSeries[i].totalTests;
				}
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	getTests(tsId,index){
		let data={
			testSeries : tsId
		};
		if(this.testSeries[index].tests){
			return;
		}
		this.testseriesService.getTsProductTest(data).subscribe(
			res=>{	
				this.testSeries[index].tests= res;
				console.log(this.testSeries[index]);
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	createOrder(){
		let stdDetails:any={};
		if(this.student&&(!this.student.majorCourse||this.student.majorCourse=='null')){
			stdDetails.majorCourse= this.tsProduct.course.id||this.tsProduct.course;
		}
		if(this.student&&(!this.student.majorStream||this.student.majorStream=='null')){
			stdDetails.majorStream= this.tsProduct.stream.id||this.tsProduct.stream;
		}
		if(this.student&&(!this.student.session||this.student.session=='null')){
			stdDetails.session= this.tsProduct.session.id||this.tsProduct.session.id
		}
		if(stdDetails=={}){
			this.placeOrder();
		}else{

			this.studentService.updateStudent(this.student.id, stdDetails).subscribe(
				res=>{
					this.placeOrder();
				},
				err=>{
					this.notifier.alert(err.code, err.message, 'danger', 5000);
				}
			);
		}

	}

	placeOrder(){
		let orderData=this.orderForm.value;
		orderData.product = this.tsProduct.id;
		orderData.total = parseFloat(this.tsProduct.cost);
		orderData.productCategory=this.tsProduct.category;
		orderData.productType=this.tsProduct.type;
		if(this.tsProduct.customParam&&this.tsProduct.customParam.autoAssigned){
			orderData.autoAssigned=true;
		}
		if(this.tsProduct&&!this.tsProduct.costIncludedTax&&this.tsProduct.showTaxInReceipt&&this.tsProduct.tax){
			let taxTotal=0;

			if(this.tsProduct.tax.cgst){
				taxTotal+= orderData.total*this.tsProduct.tax.cgst/100;
			}
			if(this.tsProduct.tax.sgst){
				taxTotal+= orderData.total*this.tsProduct.tax.sgst/100;
			}
			orderData.total =parseFloat((orderData.total + taxTotal).toFixed(2)) 
		}
		this.orderService.placeOrder(this.sessionStd, orderData).subscribe(
			res=>{
				this.order=res;
				this.activeStep=3;
			},
			err=>{
				this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
			}
		)
	}

	
}
