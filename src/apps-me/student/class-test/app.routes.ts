import { Routes, CanActivateChild } from '@angular/router';

import { ClassTestAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  { path: '',
    component: ClassTestAppComponent,
    children: [
      { path: '', loadChildren: './activities/home/home.activity.scth#StudentClassTestHomeActivity' },
      { path: 'report', loadChildren: './activities/report/overview.activity.sctr#StudentClassTestOverViewActivity' },
      // { path: 'offline-test', loadChildren: './activities/offline-test/list.activity.sceofl#StudentConventionalExamOfflineTestListActivity' },
      // { path: 'question', loadChildren: './activities/question/questions.activity.sfq#StudentForumQuestionsActivity' },
      // { path: 'question/ask', loadChildren: './activities/question/ask.activity.sfq#StudentForumQuestionAskActivity' },
      // { path: 'question/:questionId', loadChildren: './activities/question/question.activity.sfq#StudentForumQuestionActivity' },
    ]
  }
]
