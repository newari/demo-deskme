import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentClassTestHomeComponent } from './home.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { ConventionalExamService } from '../../../../../lib/services/conventional-exam.service';
import { DialogModule, AccordionModule, TabViewModule, FieldsetModule } from 'primeng/primeng';
import { FilemanagerService } from '../../../../../lib/components/filemanager/filemanager.service';
import { FeedbackService } from "../../../../../lib/services/feedback.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { VideoService } from "../../../../../lib/services/video.service";
import { SettingsService } from "../../../../../lib/services/settings.service";
import { ClasstestresultService } from "../../../../../lib/services/classtestresult.service";
export const ROUTES:Routes=[
    {path: '', component: StudentClassTestHomeComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentClassTestHomeComponent],
    imports:[
        CommonModule,
        DialogModule,
        FormsModule,
        TabViewModule,
        ReactiveFormsModule,
        RouterModule.forChild(ROUTES),
        AccordionModule,
        FieldsetModule
    ],
    providers: [ClasstestresultService,, FilemanagerService, FeedbackService, VideoService, SettingsService]
})
export class StudentClassTestHomeActivity { }