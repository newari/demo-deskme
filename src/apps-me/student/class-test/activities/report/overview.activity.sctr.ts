import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ClassTestOverViewContent } from './overview';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { ConventionalExamService } from '../../../../../lib/services/conventional-exam.service';
import { DialogModule, AccordionModule, TabViewModule, ChartModule, PaginatorModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { ClasstestService } from "../../../../../lib/services/classtest.service";
import { QsetService } from "../../../../../lib/services/qset.service";
import { PassageService } from "../../../../../lib/services/passage.service";
import { BookmarkQuestionService } from "../../../../../lib/services/bookmarkquestion.service";
import { ClasstestresultService } from "../../../../../lib/services/classtestresult.service";
export const ROUTES:Routes=[
    {path: '', component: ClassTestOverViewContent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Report'}},
];

@NgModule({
    declarations: [ClassTestOverViewContent],
    imports:[
        CommonModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ROUTES),
        AccordionModule,
        TabViewModule,
        ChartModule,
        PaginatorModule
       
    ],
    providers: [ConventionalExamService, ClasstestresultService, ClasstestService,QsetService,BookmarkQuestionService,PassageService]
})
export class StudentClassTestOverViewActivity { }