import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { ClasstestService } from '../../../../../lib/services/classtest.service';
import { StudentClassTestOfflineTestListComponent } from "./list.component";
import { DialogModule } from "primeng/primeng";
export const ROUTES:Routes=[
    {path: '', component: StudentClassTestOfflineTestListComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentClassTestOfflineTestListComponent],
    imports:[
        CommonModule,DialogModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[ClasstestService]
})
export class StudentClassTestOfflineTestListActivity { }