import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentDownloadsHomeComponent } from './home.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { DialogModule } from 'primeng/primeng';
import { DownloadcategoryService } from "../../../../../lib/services/downloadcategory.service";
import { StudentService } from '../../../../../lib/services/student.service';
export const ROUTES:Routes=[
    {path: '', component: StudentDownloadsHomeComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Downloads'}},
];

@NgModule({
    declarations: [StudentDownloadsHomeComponent],
    imports:[
        CommonModule,
        DialogModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[DownloadcategoryService, StudentService]
})
export class StudentResourcesDownloadsActivity { }