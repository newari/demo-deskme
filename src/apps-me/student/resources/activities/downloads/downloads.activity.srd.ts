import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentDownloadsComponent } from './downloads.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { DialogModule, PaginatorModule } from 'primeng/primeng';
import { DownloadcategoryService } from "../../../../../lib/services/downloadcategory.service";
import { DownloadService } from '../../../../../lib/services/download.service';
export const ROUTES:Routes=[
    {path: '', component: StudentDownloadsComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Downloads'}},
];

@NgModule({
    declarations: [StudentDownloadsComponent],
    imports:[
        CommonModule,
        DialogModule,
        PaginatorModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[DownloadcategoryService, DownloadService]
})
export class StudentResourcesDownloadsListActivity { }