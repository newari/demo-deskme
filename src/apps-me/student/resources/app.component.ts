import {Component} from "@angular/core";
import { EzukitAppService } from '../../../lib/services/ezukit-app.service';
@Component({
    templateUrl: './app.component.html'
})
export class ResourcesAppComponent { 
    constructor(private appService:EzukitAppService){

    }
    ngOnInit(){
        let _this=this;
        window.setTimeout(function(){
            _this.appService.setActiveApp({name:'Resources'});
        }, 0);
    }
}
