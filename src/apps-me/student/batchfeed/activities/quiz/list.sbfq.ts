import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StudentQuizListComponent } from "./list";
import { Routes, RouterModule } from "@angular/router";
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { QuizService } from "../../../../../lib/services/quiz.service";
import { FormsModule } from "@angular/forms";
import { DialogModule } from 'primeng/dialog';
import { FieldsetModule } from 'primeng/fieldset';
import { SidebarModule } from 'primeng/sidebar';

export const ROUTES:Routes=[
    {path: '', component: StudentQuizListComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Quizs'}},
];
@NgModule({
    declarations:[StudentQuizListComponent],
    imports:[CommonModule,DialogModule,FormsModule,FieldsetModule, SidebarModule, RouterModule.forChild(ROUTES)],
    exports:[],
    providers:[QuizService]
})

export class StudentQuizListActivity{}
