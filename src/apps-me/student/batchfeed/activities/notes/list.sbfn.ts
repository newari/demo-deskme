import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StudentNotesListComponent } from "./list";
import { Routes, RouterModule } from "@angular/router";
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { FormsModule } from "@angular/forms";
import { SidebarModule } from 'primeng/sidebar';

export const ROUTES:Routes=[
    {path: '', component: StudentNotesListComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Notes'}},
];
@NgModule({
    declarations:[StudentNotesListComponent],
    imports:[CommonModule, SidebarModule,FormsModule, RouterModule.forChild(ROUTES)],
    exports:[],
})

export class StudentNotesListActivity{}
