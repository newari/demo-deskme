import { Component, OnInit } from "@angular/core";
import { StudentPanelService } from "../../../shared/services/student-panel.service";
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { AuthService } from "../../../../../lib/services/auth.service";
import { DomSanitizer } from "@angular/platform-browser";


@Component({
    selector:'note-list',
    templateUrl:'./list.html'
})

export class StudentNotesListComponent implements OnInit{
    notes;
    student:any;
    displayArticleDetail: boolean;
    selectedArticle: any;
    activeBatch: any;
    subjects:any=[];
    selectedSubject: any=null;
    panelLoader: string;
    activeBatchIndex:number
    activeTab: any='StudyMaterial';
    assignments: any[];
    activeFilter:any;
    pdfViewer:any={show:false, link:""};
    constructor(
        private studentService: StudentPanelService,
        private notifier: NotifierService,
        // private
        private authService:AuthService ,
        private sanitizer: DomSanitizer
    ){}
    setActiveBatch(index?:number){
        this.activeBatch=this.student.batches[index];
        this.activeBatchIndex=index;
        this.loadSubjects();
    }
    ngOnInit(){
        this.student= this.authService.studentInfo();
        if(!this.student){
            return;
        }

        if(!this.student.batches||(this.student.batches&&this.student.batches.length<1)){
            return;
        }
        this.activeBatch= this.student.batches[0];
        this.activeBatchIndex=0;
        this.loadSubjects();
    }
    loadSubjects() {
        this.panelLoader='show';

       this.studentService.getBatchSubjectsAndFaculties(this.activeBatch.batch.id).subscribe(
           res=>{
                this.panelLoader='none';
                this.subjects = res.subjects;
           },
           err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message,'danger', 5000)
           }

       );
    }
    getBacthNotes(filter?:any){
        this.notes=[];
        if(!filter){
            filter ={};
        }
        this.panelLoader='show';
        this.studentService.getBatchNotes(this.activeBatch.batch.id,filter,true).subscribe(
            res=>{
                this.panelLoader='none';
                this.notes=res.body;
            },
            err=>{
                this.panelLoader='none';
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    getAssignmentFeeds(filter?:any) {
        this.assignments=[];
        this.panelLoader='show';
        if(!filter) {filter={}};
        this.studentService.getBatchAssignments(this.activeBatch.batch.id,filter,true).subscribe(
            res=>{
                this.assignments=res.body;
                this.panelLoader="none";
            },
            err=>{
                this.notifier.alert(err.code, err.message,'danger', 5000);
                this.panelLoader="none";
            }
        );
    }
    showDetails(i){
        this.displayArticleDetail=true;
        this.selectedArticle=this.notes[i];
    }

    getSafeUrl(url){
        if(url!=='null'){
            return this.sanitizer.bypassSecurityTrustResourceUrl(url);
        }
    }
    setActiveTab(tab){
        this.activeTab = tab;
        if(this.activeTab=='StudyMaterial'){
            this.getBacthNotes({subject:this.selectedSubject.id});
        }else if(this.activeTab=='WorkBook'){
            this.getAssignmentFeeds({subject:this.selectedSubject.id});
        }
    }
    showAnswerUploadModal(i:number){

    }

    confirmedToCheck(i:number){

    }
    displayPdf(pdfUrl:string){
        this.pdfViewer.link=pdfUrl;
        this.pdfViewer.show=true;
    }
}
