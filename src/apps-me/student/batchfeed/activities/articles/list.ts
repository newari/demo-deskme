import { Component, OnInit } from "@angular/core";
import { StudentPanelService } from "../../../shared/services/student-panel.service";
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { AuthService } from "../../../../../lib/services/auth.service";
import { DomSanitizer } from "@angular/platform-browser";


@Component({
    selector:'article-list',
    templateUrl:'./list.html'
})

export class StudentArtcleListComponent implements OnInit{
    articles:any=[];
    student:any;
    displayArticleDetail: boolean;
    selectedArticle: any;
    panelLoader='';
    activeBatch: any;
    activeBatchIndex:number;
    constructor(
        private studentService: StudentPanelService,
        private notifier: NotifierService,
        // private
        private authService:AuthService ,
        private sanitizer: DomSanitizer
    ){}
    setActiveBatch(index?:number){
        this.activeBatch=this.student.batches[index];
        this.getArticleFeeds();
    }
    ngOnInit(){
        this.student= this.authService.studentInfo();

        if(!this.student){
            return;
        }

        if(!this.student.batches||(this.student.batches&&this.student.batches.length<1)){

            return;
        }
        this.activeBatch= this.student.batches[0];
        // if(!this.activeBatch)
        // return;
         this.panelLoader="show";
        this.getArticleFeeds();
    }
    getArticles() {
        if(!this.student.batches||(this.student.batches&&this.student.batches.length<1))
        return;

        let batches:any= this.student.batches.map(
            (batch)=>{
                return batch.batch.id
            }
        )
        this.studentService.getBatchFeed({batch:batches,contentType:"ARTICLE"}).subscribe(
            res=>{
                this.articles=res;
                this.panelLoader="none";

            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
                this.panelLoader="none";

            }
        );
    }
    getArticleFeeds(){
        this.studentService.getBatchArticles(this.activeBatch.batch.id).subscribe(
            res=>{
                this.articles=res;
                this.panelLoader="none";
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
                this.panelLoader="none";
            }
        );
    }
    showDetails(i){
        this.panelLoader="show";
        this.displayArticleDetail=true;
        this.selectedArticle=this.articles[i];
        let self=this;
        setTimeout(()=>{
            self.panelLoader='none';
        },500)
    }

    getSafeUrl(url?:any,type?:any){
        if(url!=='null'){
            if(type=='VIDEO')
            return this.sanitizer.bypassSecurityTrustResourceUrl(url);
            else if(type=='IMG')
            return this.sanitizer.bypassSecurityTrustUrl(url)
            else
            return this.sanitizer.bypassSecurityTrustUrl(url)
        }
    }
    getUrl(url){
        return `url(${url})`;
    }
}
