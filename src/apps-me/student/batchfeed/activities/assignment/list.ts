import { Component, OnInit, ElementRef } from "@angular/core";
import { StudentPanelService } from "../../../shared/services/student-panel.service";
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { AuthService } from "../../../../../lib/services/auth.service";
import { DomSanitizer } from "@angular/platform-browser";
import { FilemanagerService } from "../../../../../lib/components/filemanager/filemanager.service";
import { ConventionalExamService } from "../../../../../lib/services/conventional-exam.service";
import { ClientService } from "../../../../../lib/services/client.service";


@Component({
    selector:'assignment-list',
    templateUrl:'./list.html'
})

export class StudentAssignmentListComponent implements OnInit{
    assignments:any=[];
    student:any;
    displayArticleDetail: boolean;
    selectedAssignment: any;
    answerUploadModal: boolean;
    panelLoader: string;
    activeIndex: number;
    client:any;
    activeBatch: any;
    activeBatchIndex:number;
    upldPrgrs:number;
    subjects:any=[];
    selectedSubject: null;
    totalRecords: number=0;
    countBase:number=1;
    activeFilter: any;

    constructor(
        private studentService: StudentPanelService,
        private notifier: NotifierService,
        private fmService: FilemanagerService,
        private authService:AuthService ,
        private sanitizer: DomSanitizer,
        private ceService: ConventionalExamService,
        private el: ElementRef,
        private clientService:ClientService
    ){}
    setActiveBatch(index?:number){
        this.activeBatchIndex=index;
        this.activeBatch=this.student.batches[index];
        this.loadSubjects();
    }
    loadSubjects() {
        this.panelLoader='show';

       this.studentService.getBatchSubjectsAndFaculties(this.activeBatch.batch.id).subscribe(
           res=>{
                this.panelLoader='none';
                this.subjects = res.subjects;
           },
           err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message,'danger', 5000)
           }

       );
    }
    getAssignmentFeeds(filter?:any) {
        this.assignments=[];
        this.panelLoader='show';
        if(!filter) {filter={}};

        this.activeFilter=filter;

        // filter.limit=100;
        this.studentService.getBatchAssignments(this.activeBatch.batch.id,filter,true).subscribe(
            res=>{
                this.assignments=res.body;
                this.totalRecords = res.headers.get('totalRecords')||0;
                this.panelLoader="none";
            },
            err=>{
                this.notifier.alert(err.code, err.message,'danger', 5000);
                this.panelLoader="none";
            }
        );
    }
    ngOnInit(){
        this.student= this.authService.studentInfo();

        if(!this.student){
            return;
        }
        this.client= this.clientService.getLocalClient();
        if(!this.student.batches||(this.student.batches&&this.student.batches.length<1)){
            return;
        }
        this.activeBatchIndex=0;
        this.activeBatch= this.student.batches[0];
        this.loadSubjects();
    }
    getAssignments() {
        if(!this.student.batches||(this.student.batches&&this.student.batches.length<1))
        return;

        let batches:any= this.student.batches.map(
            (batch)=>{
                return batch.batch.id
            }
        )
        this.studentService.getBatchFeed({batch:batches,contentType:"ASSIGNMENT"}).subscribe(
            res=>{
                this.assignments=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }
    showDetails(i){
        this.displayArticleDetail=true;
        this.selectedAssignment=this.assignments[i];
    }

    getSafeUrl(url,type){
        if(url!=='null'){
            if(type=='VIDEO')
            return this.sanitizer.bypassSecurityTrustResourceUrl(url);
            else if(type=='IMG')
            return this.sanitizer.bypassSecurityTrustUrl(url)
        }
    }

    showAnswerUploadModal(assignmentIndex?:number){
		this.selectedAssignment=this.assignments[assignmentIndex];
        this.answerUploadModal=true;
        this.activeIndex=assignmentIndex;
    }

    async uploadFile(cb) {

    	//locate the file element meant for the file upload.
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#fmFile');
    	//get the total amount of files attached to the file input.
        let fileCount: number = inputEl.files.length;
    	//create a new fromdata instance
        this.panelLoader="show";
        //check if the filecount is greater than zero, to be sure a file was selected.

        if (fileCount > 0) { // a file was selected
            let formData = new FormData();
            //append the key name 'photo' with the first file in the element
			      formData.append('fmFile', inputEl.files.item(0));
            //call the angular http method
            if(inputEl.files[0].type!=='application/pdf'){
                return  cb({code:"ERROR", message:"Please upload a PDF file!!"});
            }
            // try {
            //     var res:any=await this.fmService.uploadDirctToS3(inputEl.files[0], inputEl.files[0].name, this.client.s3Dir||this.client.publicDir);
            // } catch (error) {
            //     return cb(error)
            // }
            // if(!res){
            //    return cb({code:"ERROR", message:"file Not uploaded"})
            // }
            // console.log("res here",res);

            // if(res.filePath){
            //     this.createFileInEdukit(res.filePath,inputEl.files[0].name);
            //     return cb(null,{url:"https://edkt.net/"+res.filePath});
            // }

            this.fmService.getAwsTempCreds().subscribe(
                credRes=>{
                    this.fmService.uploadDirctToS3Obs(credRes.Credentials, inputEl.files[0], inputEl.files[0].name, this.client.s3Dir||this.client.publicDir).subscribe(
                        res=>{
                            if(res.done){
                                console.log(res.body)
                                if(res.body&&res.body.key){
                                    this.createFileInEdukit(res.body.key,inputEl.files[0].name);
                                    return cb(null,{url:"https://edkt.net/"+res.body.key});
                                }
                            }else{
                                this.upldPrgrs=Math.ceil(res.progress*100);
                                console.log("prgrs", res.progress)
                            }
                        },
                        err=>{
                            console.log(err)
                        }
                    );
                },
                credErr=>{
                    console.log(credErr)
                }
            )

    		// this.fmService.uploadDirctToS3(formData, inputEl.files[0].name, this.client.s3Dir||this.client.publicDir).subscribe(
            //     res=>{
            //         this.panelLoader="none";
			// 		return cb(null, res);

            //     },
            //     err=>{
            //         this.panelLoader="none";
            //         return cb(err);
            //         // this.notifier.alert(err.code, err.message, 'danger');
            //         // this.fh.upload1Status=err.message;

            //     }
            // )
    	}
    }
    createFileInEdukit(filePath: any,fileName) {
        console.log("Create in fileManager",filePath);
        let data={
            url: "https://edkt.net/"+filePath,
            path: filePath,
            name: fileName,
            title: fileName,
            altText: fileName,
            category:"PDF",
            type:"PDF",
            owner: this.student.user,
            group: "STUDENT",
            status: true,
            client:this.client.id
        }

        this.fmService.addFilem(data).subscribe(
            res=>{

            },
            err=>{
                console.log(err);

            }
        );
    }
    uploadAnswerSheet(){
		if(!this.selectedAssignment){
			this.answerUploadModal=false;
			return;
        }
		var self=this;
		this.panelLoader="show";
      	this.uploadFile(function(err, file){
	        if(err){
				self.panelLoader="none";
				return self.notifier.alert(err.code, err.message, "danger", 5000);
	        }
	        let data={
				stdResponseFile:file.url,
				sendSMS : 'CONVENTIONAL_TEST_STD_UPLOAD_SHEET',
				isPdfInvalid : false,
				reasonOfInvalid : '',
				confirmedToCheck : false,
			}
			self.ceService.updateStudentResponse(self.selectedAssignment.data.response.id, data).subscribe(
				res=>{
					self.panelLoader="none";
					self.answerUploadModal=false;
					self.assignments[self.activeIndex].data.response.stdResponseFile=res.stdResponseFile;
					self.assignments[self.activeIndex].data.response.isPdfInvalid=res.isPdfInvalid;
					self.assignments[self.activeIndex].data.response.confirmedToCheck=res.confirmedToCheck;
					self.notifier.alert('Uploaded', 'Your response uploded successfully!', "success", 5000);
				},
				err=>{
					self.panelLoader="none";
					self.notifier.alert(err.code, err.message, "danger", 5000);
				}
			)
      	});
    }

    confirmedToCheck(assignmentIndex){
		let confirmedAt=new Date();
		let data={confirmedToCheck:true, confirmedAt:confirmedAt};
		this.ceService.updateStudentResponse(this.assignments[assignmentIndex].data.response.id, data).subscribe(
			res=>{
				this.panelLoader="none";
				this.assignments[assignmentIndex].data.response.confirmedToCheck=true;
				this.notifier.alert('Confirmed', 'Confirmed to Check Successfully!', "success", 500);
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
    }
    
    paginate(e) {
        if (!this.activeFilter) { 
            this.activeFilter = {};
        }
        this.countBase = e.rows * e.page + 1;
        this.activeFilter.page = (e.page + 1);
        this.activeFilter.limit = e.rows;
        this.getAssignmentFeeds(this.activeFilter);
    }
}
