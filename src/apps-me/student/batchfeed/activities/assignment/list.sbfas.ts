import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StudentAssignmentListComponent } from "./list";
import { Routes, RouterModule } from "@angular/router";
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { FilemanagerService } from "../../../../../lib/components/filemanager/filemanager.service";
import { ConventionalExamService } from "../../../../../lib/services/conventional-exam.service";
import { FormsModule } from "@angular/forms";
// import { StudentPanelService } from "../../../shared/services/student-panel.service";
// import { StudentPanelService } from "../../../../../lib/services/student-panel.service";
import { DialogModule } from 'primeng/dialog';
import { PaginatorModule } from 'primeng/paginator';
import { SidebarModule } from 'primeng/sidebar';

export const ROUTES:Routes=[
    {path: '', component: StudentAssignmentListComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Assignments'}},
];
@NgModule({
    declarations:[StudentAssignmentListComponent],
    imports:[CommonModule,DialogModule,FormsModule,PaginatorModule, SidebarModule, RouterModule.forChild(ROUTES)],
    exports:[],
    providers:[FilemanagerService,ConventionalExamService]
})

export class StudentAssignmentListActivity{}
