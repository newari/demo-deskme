import {Component} from "@angular/core";
import { AuthService } from '../../../../../lib/services/auth.service';
import { ForumQuestionService } from '../../../shared/services/forum-question.service';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';

@Component({
	templateUrl: './questions.component.html'
})
export class StudentForumQuestionsComponent {
    questions:any[];
    panelLoader:string="none";
    constructor(
        private questionService:ForumQuestionService,
        private authService:AuthService,
        private notifier:NotifierService
    ){
            
    }

    ngOnInit(){
        this.panelLoader="show";
        this.questionService.getQuestion().subscribe(
            res=>{
                this.panelLoader="none";
                this.questions=res;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )

    }
}
