import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { ForumQuestionService } from '../../../shared/services/forum-question.service';
import { ActivatedRoute, Params } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Forumquestion } from '../../../../../lib/models/forumquestion.model';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';

@Component({
	templateUrl: './question.component.html'
})
export class StudentForumQuestionComponent {
    question:any;
    panelLoader:string="none";
    questionReplies:Forumquestion[];

    questionReplyForm:FormGroup;
    formStatus:string="Normal";
    fh:any={
        status:'Normal',
        file1:null,
        file2:null,
        file3:null,
        file1Status:'',
        file2Status:'',
        file3Status:'',
        error:null
    };
    constructor(
        private questionService:ForumQuestionService,
        private authService:AuthService,
        private fb:FormBuilder,
        private el: ElementRef,
        private notifier:NotifierService,
        private activatedRoute:ActivatedRoute,
    ){
            
    }

    ngOnInit(){
        this.activatedRoute.params.subscribe((params: Params) => {
	        let questionId = params['questionId'];
            this.loadQuestion(questionId);
            
            this.loadReply(questionId);
        });
        
        this.questionReplyForm=this.fb.group({
            content:['', Validators.required],
        });
        

    }

    loadQuestion(questionId){
        this.panelLoader="show";
        this.questionService.getOneQuestion(questionId).subscribe(
            res=>{
                this.panelLoader="none";
                this.question=res;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
    }
    loadReply(qid){
        if(this.questionReplies){
            return;
        }
        this.panelLoader="show";
        this.formStatus="Processing";
        this.questionService.getForumquestionReply(qid).subscribe(
            res=>{
                this.questionReplies=res;
                this.panelLoader="none";
                this.formStatus="Normal";
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
                this.panelLoader="none";
                this.formStatus="Normal";
            }
        )
    }

    addReply(){
        let thisUser=this.authService.student();
        let question=this.questionReplyForm.value;
        question.parentId=this.question.id;
        question.user=thisUser.id;
        question.status=true;
        question.stream=this.question.stream.id||null;
        question.subject=this.question.subject.id||null;
        question.topic=this.question.topic||"";
        question.attachments=[];
        if(this.fh.file1){
            question.attachments.push(this.fh.file1);
        }
        if(this.fh.file2){
            question.attachments.push(this.fh.file2);
        }
        if(this.fh.file3){
            question.attachments.push(this.fh.file3);
        }
        this.panelLoader="show";
        this.formStatus="Processing";
        this.questionService.addForumquestionReply(question).subscribe(
            res=>{
                res.user=thisUser;
                // this.questionReplies.unshift(res);
                if(this.questionReplies&&this.questionReplies.length>0){
                    this.questionReplies.unshift(res);
                }else{
                    this.questionReplies=[res];
                }
                this.questionReplyForm.reset();
                this.fh.file1=null;
                this.fh.file2=null;
                this.fh.file3=null;
                this.panelLoader="none";
                this.formStatus="Normal";
            },
            err=>{
                this.formStatus="Normal";
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )
    }

    upload(id:string) {
    	//locate the file element meant for the file upload.
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#'+id);
		console.log(inputEl);
    	//get the total amount of files attached to the file input.
        let fileCount: number = inputEl.files.length;
    	//create a new fromdata instance
        let formData = new FormData();
    	//check if the filecount is greater than zero, to be sure a file was selected.
        if (fileCount > 0) { // a file was selected
            //append the key name 'photo' with the first file in the element
			formData.append('ekFile', inputEl.files.item(0));
            //call the angular http method
			this.fh[id+"Status"]="Uploading...";
            this.questionService.uploadFile(formData).subscribe(
                res=>{
                    this.fh[id]=res.url;
                    
                    this.fh[id+"Status"]="";
                    // document.getElementById(id).value="";
                },
                err=>{
                    console.log(err);
                    this.fh[id+"Status"]="Error in uploading, Try again!";
                    // document.getElementById(id).value="";
                }
            );
			

    	}
    }
}
