import { Routes, CanActivateChild } from '@angular/router';

import { StudentForumAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  { path: '',
  component:StudentForumAppComponent,
  children: [
    { path: '', loadChildren: () => import('./activities/home/home.activity.sfh').then(m => m.StudentForumHomeActivity) },
    { path: 'question', loadChildren: () => import('./activities/question/questions.activity.sfq').then(m => m.StudentForumQuestionsActivity) },
    { path: 'question/ask', loadChildren: () => import('./activities/question/ask.activity.sfq').then(m => m.StudentForumQuestionAskActivity) },
    { path: 'question/:questionId', loadChildren: () => import('./activities/question/question.activity.sfq').then(m => m.StudentForumQuestionActivity) },
    // { path: 'question/:questionId', loadChildren: () => import('modulefile').then(m => m.StudentForumQuestionActivity) './activities/question/question.activity.sfq' },
  ]
  }
]
