import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";
import { StudentForumAppComponent } from './app.component';

@NgModule({
    declarations: [StudentForumAppComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[]
})
export class StudentForumAppModule { }
