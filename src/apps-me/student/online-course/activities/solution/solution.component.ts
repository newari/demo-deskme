import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { Router } from '@angular/router';
import { ProductService } from '../../../../../lib/services/product.service';
import { UserService } from '../../../../../lib/services/user.service';
import { StudentService } from '../../../../../lib/services/student.service';
import { Student } from '../../../../../lib/models/student.model';
import * as moment from 'moment'
@Component({
  templateUrl: './solution.component.html'
})
export class StudentOnlineCourseSolutionComponent {
	panelLoader:string;
	sessionStd:any;
	userProducts:any[];
	student: Student;
	freeProducts: any[];
    constructor(
      	private authService: AuthService,
        private notifier: NotifierService,
		private router:Router,
		private productService:ProductService,
		private userService:UserService,
		private studentService:StudentService
    ){

    }

	ngOnInit(){
		this.sessionStd=this.authService.student();
		if(this.sessionStd){
			this.loadStudentProducts();
			this.loadStudent();

		}
	}
	loadStudent(){
		this.panelLoader="show";
		this.studentService.getSessionStudent({populateUser:true, populateSession:true, populateCourse:true, populateBatch:true, populateProduct:true}).subscribe(
            res=>{
                this.panelLoader="none";
				this.student=res;
				// this.loadFreeProducts();
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
	}

	loadStudentProducts(){
		this.panelLoader='show';
		let fltr:any={
            user:this.sessionStd.id,
			productCategory:'5cf0cfc4e6636f4d815ccb0a',
			productType:'5e95852a7c62f0052cbdfcfc'
		}
		this.userService.getUserProduct(fltr).subscribe(
			res=>{
				this.panelLoader="none";
				this.userProducts=res;
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}

	loadFreeProducts(){
		let filter:any={cost:'0',
			stream:this.student.majorStream.id,
			course:this.student.majorCourse.id,
			category:'5cf0cfc4e6636f4d815ccb0a',
			type:'5e95852a7c62f0052cbdfcfc'
		}
		this.productService.getProducts(filter).subscribe(
			res=>{
				this.freeProducts=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}

	getValidity(date){

		let currentDate= new Date();

		let validTill = moment(date).format();

		if(moment(validTill).isAfter(currentDate)){
			return false
		}else{
			return true
		}


	}


}
