import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { Router } from '@angular/router';
import { ProductService } from '../../../../../lib/services/product.service';
import { UserService } from '../../../../../lib/services/user.service';
import { StudentService } from '../../../../../lib/services/student.service';
import { Student } from '../../../../../lib/models/student.model';
import * as moment from 'moment'
import { ClientService } from '../../../../../lib/services/client.service';
@Component({
  templateUrl: './course.component.html'
})
export class StudentOnlineCourseCourseComponent {
	panelLoader:string;
	sessionStd:any;
	userProducts:any[];
	student: Student;
	freeProducts: any[];
	selectedProduct:any;
	msgs:any[];
	clientDetails:any
	confirmationHeading:string;
	acceptVisible: boolean;
    constructor(
      	private authService: AuthService,
        private notifier: NotifierService,
		private router:Router,
		private productService:ProductService,
		private userService:UserService,
		private studentService:StudentService,
		private clientService: ClientService
    ){

    }

	ngOnInit(){
		this.sessionStd=this.authService.student();
		this.clientDetails=this.clientService.getLocalClient();
		if(this.sessionStd){
			this.loadStudentProducts();
			this.loadStudent();

		}
	}
	loadStudent(){
		this.studentService.getSessionStudent({populateUser:true, populateSession:true, populateCourse:true, populateBatch:true, populateProduct:true}).subscribe(
            res=>{
                this.panelLoader="none";
				this.student=res;
				// this.loadFreeProducts();
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
	}

	loadStudentProducts(){
		let fltr:any={
            user:this.sessionStd.id,
            productCategory:'5cf0cfc4e6636f4d815ccb0a'
        }
        this.panelLoader="show";
		this.userService.getUserProduct(fltr).subscribe(
			res=>{
                this.panelLoader="none";
				this.userProducts=res;
			},
			err=>{
                this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}

	loadFreeProducts(){
		let filter:any={cost:'0',
			stream:this.student.majorStream.id,
			course:this.student.majorCourse.id,
			productCategory:'5cf0cfc4e6636f4d815ccb0a',
			// type:'5d0b5ce7d721ff179f779fc4'
		}
        this.userService.getFreeProductForUser(filter).subscribe(
			res=>{
                this.freeProducts=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}

	getValidity(date){
		if(!date){
			return false;
		}
		let currentDate= new Date();

		let validTill = moment(date).format();

		if(moment(validTill).isAfter(currentDate)){
			return false
		}else{
			return true
		}


	}

	confirm(i){

	}
}
