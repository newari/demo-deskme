import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentOnlineCourseHomeComponent } from './home.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { DialogModule } from 'primeng/dialog';
export const ROUTES:Routes=[
    {path: '', component: StudentOnlineCourseHomeComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentOnlineCourseHomeComponent],
    imports:[
        CommonModule,
        DialogModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[]
})
export class StudentOnlineCourseHomeActivity { }
