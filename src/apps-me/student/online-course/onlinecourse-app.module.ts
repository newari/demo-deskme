import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";
import { OnlineCourseAppComponent } from './app.component';
import { LessonService } from "../../../lib/services/lesson.service";

@NgModule({
    declarations: [OnlineCourseAppComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[LessonService]
})
export class StudentOnlineCourseAppModule { }
 