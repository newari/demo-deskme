import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentShopHomeComponent } from './home.component';
import { StudentAuthGuard } from '../../../shared/services/student-auth-guard';
import { OrderService } from "../../../../../lib/services/order.service";

export const ROUTES:Routes=[
    {path: '', component: StudentShopHomeComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentShopHomeComponent],
    imports:[
        CommonModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[OrderService]
})
export class StudentShopHomeActivity { 
    
}