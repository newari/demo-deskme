import {Component} from "@angular/core";
import { AuthService } from '../../../../../lib/services/auth.service';
import { Order } from '../../../../../lib/models/order.model';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { OrderService } from '../../../../../lib/services/order.service';

@Component({
  	templateUrl: './home.component.html'
})
export class StudentShopHomeComponent {
	panelLoader:string;
	orders:Order[];
    constructor(
		private authService: AuthService,
		private notifier:NotifierService,
		private orderService:OrderService,
    ){
        
    }

    ngOnInit(){
		var thisStd=this.authService.student();
		this.loadOrders({limit:50});
	}
	
	loadOrders(filter?:any){
		this.panelLoader="show";
		this.orderService.getOrder(filter).subscribe(
			res=>{
				this.orders=res;
				this.panelLoader="none";
			},
			err=>{
				this.notifier.alert(err.code, err.mesaage, "danger", 10000);
				this.panelLoader="none";
			}
		)
	}


}
