import {Component} from "@angular/core";
import { AuthService } from '../../../../../lib/services/auth.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Order } from '../../../../../lib/models/order.model';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { OrderService } from "../../../../../lib/services/order.service";
import { PaymentService } from "../../../../../lib/services/payment.service";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  	templateUrl: './order-payment.component.html'
})
export class StudentShopOrderPaymentComponent {
	panelLoader:string;
	order:Order;
	showDiscountModalForm:boolean;
	discountForm:FormGroup;
	orderId:any
    constructor(
		private fb: FormBuilder,
		private authService: AuthService,
		private notifier:NotifierService,
		private orderService:OrderService,
		private paymentService:PaymentService,
		private activatedRoute:ActivatedRoute
    ){
        
    }

    ngOnInit(){
		var thisStd=this.authService.student();
		this.activatedRoute.params.subscribe((params: Params) => {
			this.orderId = params['orderId'];
			this.loadOrder(this.orderId);
		});
		
		this.discountForm=this.fb.group({
			type:['COUPON'],
			discountCode:['', Validators.required]
		})
		
	}
	
	loadOrder(orderId){
		this.panelLoader="show";
		this.orderService.getOneOrder(orderId).subscribe(
			res=>{
				this.order=res;
				this.panelLoader="none";
			},
			err=>{
				this.notifier.alert(err.code, err.mesaage, "danger", 10000);
				this.panelLoader="none";
			}
		)
	}
	showDiscountModal(){
		this.showDiscountModalForm=true;
	}

	addDiscount(){
		if(!this.discountForm.valid) return;
		if(this.order.discountDetail&&this.order.discountDetail.discountId){
			this.notifier.alert("ERROR", "You have already used a discount code", 'danger', 5000);
			return;
		}
		this.paymentService.addDiscount(this.orderId,{discountDetail:this.discountForm.value}).subscribe(
			res=>{
				this.notifier.alert("Success", "Discount applies successfully", 'success', 500)
				this.loadOrder(this.orderId);
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}

}
