import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentShopOrderPaymentComponent } from './order-payment.component';
import { StudentFullOrderFormModule } from '../../../shared/modules/full-order-form/full-order-form.module';
import { StudentOrderPaymentModule } from '../../../shared/modules/order-payment/order-payment.module';
import { StudentAuthGuard } from '../../../shared/services/student-auth-guard';
import { OrderService } from "../../../../../lib/services/order.service";
import { PaymentService } from "../../../../../lib/services/payment.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DialogModule } from 'primeng/dialog';

export const ROUTES:Routes=[
    {path: '', component: StudentShopOrderPaymentComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Admission/Order Payment'}},
];

@NgModule({
    declarations: [StudentShopOrderPaymentComponent],
    imports:[
        StudentOrderPaymentModule,
        CommonModule,
        StudentFullOrderFormModule,
        RouterModule.forChild(ROUTES),
        DialogModule,
         FormsModule,
         ReactiveFormsModule
    ],
    providers:[OrderService, PaymentService]
})
export class StudentShopOrderPaymentActivity {

}
