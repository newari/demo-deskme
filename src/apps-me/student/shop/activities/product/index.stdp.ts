import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

export const ROUTES:Routes=[
    {
        path: '',
        children: [
            { path: '', loadChildren: () => import('./list.activity.stdp').then(m => m.ProductListActivity) },
            // { path: 'add', loadChildren: './add.activity.stdp#TestAddActivity' },
            // { path: 'edit/:id', loadChildren: './edit.activity.stdp#TestEditActivity' },
            // { path: 'print/', loadChildren: './print.activity.stdp#TestPrintActivity' },
            { path: 'view', loadChildren: () => import('./view.activity.stdp').then(m => m.ProductViewActivity)  },

        ]
    },
];

@NgModule({
    declarations:[],
    imports: [RouterModule.forChild(ROUTES)]
})
export class StudentProductModule {

}
