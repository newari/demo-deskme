import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {  ProductListContent } from './list';
import { StudentPanelService } from '../../../../../lib/services/student-panel.service';
import { ProductService } from '../../../../../lib/services/product.service';
import { CommonModule } from '@angular/common';
import { SafeHtmlPipeModule } from '../../../../../lib/filters/safehtml.pipe';
import { FormsModule } from '@angular/forms';
import { TabViewModule } from 'primeng/tabview';
export const ROUTES:Routes=[
    {path: '', component: ProductListContent, pathMatch:'full',data : {title : "Products - Edukit"}},
];

@NgModule({
    declarations:[ProductListContent],
    imports:[RouterModule.forChild(ROUTES),CommonModule,TabViewModule,SafeHtmlPipeModule,FormsModule],
    providers:[StudentPanelService,ProductService]
})
export class ProductListActivity {

 }
