import { Component} from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { FormGroup, FormBuilder } from '@angular/forms';
import { TestseriesService } from '../../../../../lib/services/testseries.service';
import { ActivatedRoute ,Router } from '@angular/router';
import { OrderService } from '../../../../../lib/services/order.service';
import { StudentService } from '../../../../../lib/services/student.service';
import { Student } from '../../../../../lib/models/student.model';
import { EdukitConfig } from '../../../../../ezukit.config';
import { ProductService } from '../../../../../lib/services/product.service';
import { ConventionalExamTestseriesService } from '../../../../../lib/services/conventional-exam-test-series.service';
@Component({
  templateUrl: './view.html'
})
export class StudentProductContent{
	orderForm:FormGroup;
	order:any;
	panelLoader="none";
	testLoader = "none";
	tsProduct : any;
	productId;
	sessionStd;
	activeStep =1;
	testSeries=[];
	student:Student;
	freeProduct : boolean;
	testOpened : boolean;
	basicAPI;
	displayTestModal : boolean;
	test : any={};
	paymentOptKeys:any[];
	paymentOptions:any[];
	showEMIModal: boolean;
	plan: any;
    constructor(
      	private authService: AuthService,
        private notifier: NotifierService,
		private fb:FormBuilder,
		private activatedRoute : ActivatedRoute,
		private testseriesService : TestseriesService,
		private orderService : OrderService,
		private studentService: StudentService,
		private router : Router,
		private productService : ProductService,
		private CETestSeriesService : ConventionalExamTestseriesService
    ){}
    ngOnInit(){
		this.orderForm=this.fb.group({
			course:[null],
			product:[null],
			stream:[null],
			session:[null],
			store:[null],
			total :[],
			tracer:[{form:'STUDENT-TM-ORDER-ADD'}],
			discountDetail : [{}],
			relProductCategory:[''],
			relProductType:['']
		});
		this.activatedRoute.queryParams.subscribe(params=>{
			this.productId = params['productId'];
		});
		this.sessionStd=this.authService.student();
		this.getSessionStudent();
		this.loadProduct();
		this.basicAPI = EdukitConfig.BASICS.API_URL;

    }
	getSessionStudent() {
		 this.studentService.getSessionStudent().subscribe(
			 res=>{
				this.student=res;
			 },
			 err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			 }
		 );
	}
	loadProduct(){
		this.productService.getOneProductInfo(this.productId).subscribe(
			res=>{
				this.tsProduct=res;
				this.orderForm.patchValue(this.tsProduct);
				if(res.view == 'TESTMENT' && res.sourceId.length >0){
					this.testSeries=res.sourceId;
					this.tsProduct.totalTests=0;
					for(var i = 0; i < this.testSeries.length; i++){
						this.tsProduct.totalTests += this.testSeries[i].totalTests;
					}
					this.loadTests(this.testSeries[0].id,0);
				}
				if(this.tsProduct.cost <= 0){
					this.freeProduct=true;
				}
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}

	loadTests(tsId,index){
		let data={
			product : this.productId,
			testSeries : tsId
		};
		this.testLoader="show";
		this.testseriesService.getTsProductTest(data).subscribe(
			res=>{
				this.testSeries[index].tests= res;
				this.testOpened=false;
				this.testLoader="none";
			},
			err=>{
				this.testLoader="none";
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	viewTest(test){
		this.displayTestModal = true;
		this.test=test;
		console.log(test);
	}
	testWindow(ts,test) {
		let view='w';
        if(screen.width < 672){
            view='m';
		}
		let url = this.basicAPI +'/student/testment/testwindow2?view='+view+'&product='+this.productId+'&testSeries='+ts+'&test='+test+'&open=true';
		url =url.toString();
		let params  = 'width='+screen.width+',height='+screen.height+',top=0,left=0,fullscreen=yes,resizable=0';
		let newWindow=window.open(url,'EXAM',params);
		this.testOpened = true;
		if (window.focus) {newWindow.focus()}
		return false;
	}
	createOrder(){
		let stdDetails:any={};
		if(this.student&&(!this.student.majorCourse||this.student.majorCourse=='null')){
			stdDetails.majorCourse= this.tsProduct.course.id||this.tsProduct.course;
		}
		if(this.student&&(!this.student.majorStream||this.student.majorStream=='null')){
			stdDetails.majorStream= this.tsProduct.stream.id||this.tsProduct.stream;
		}
		if(this.student&&(!this.student.session||this.student.session=='null')){
			stdDetails.session= this.tsProduct.session.id||this.tsProduct.session.id
		}
		if(stdDetails=={}){
			this.placeOrder(true);
		}else{
			this.panelLoader="show";
			this.studentService.updateStudent(this.student.id, stdDetails).subscribe(
				res=>{
					this.placeOrder(true);
					this.panelLoader="none";
				},
				err=>{
					this.panelLoader="none";
					this.notifier.alert(err.code, err.message, 'danger', 5000);
				}
			);
		}

	}
	loadConventionalTest(ts,index) {
		let filter={
			tsId : ts,
			select : ['id','title','alias','totalQs','totalMarks','duration']
		};
		this.CETestSeriesService.getTestSeriesTest(filter).subscribe(
			res=>{
				this.tsProduct.sourceId[index].tests=[];
			   	this.tsProduct.sourceId[index].tests=res;
			},
			err=>{
			   this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
   }
	placeOrder(checkEMI?:boolean){

		if(checkEMI&&this.tsProduct){
			this.getProductPaymentOptions();
		}else{

			let orderData:any=this.orderForm.value;
			orderData.product = this.tsProduct.id;
			orderData.total = parseFloat(this.tsProduct.cost);
			orderData.productCategory=this.tsProduct.category;
			orderData.productType=this.tsProduct.type;
			if(this.tsProduct.customParam&&this.tsProduct.customParam.autoAssigned){
				orderData.autoAssigned=true;
			}
			if(this.student.customParams&&this.student.customParams.specialDiscount){
				orderData.discountDetail={
					discountCode:"SpecialDiscount",
					discountAmount:this.student.customParams.specialDiscount,
					discountType: 'Fix',
					studentSpecialDiscount:true
				};
				orderData.total = orderData.total-this.student.customParams.specialDiscount;
				if(!orderData.customParams) orderData.customParams={};
				orderData.customParams.studentSpecialDiscount=this.student.customParams.specialDiscount;
			}
			if(this.tsProduct&&!this.tsProduct.costIncludedTax&&this.tsProduct.showTaxInReceipt&&this.tsProduct.tax){
				let taxTotal=0;

				if(this.tsProduct.tax.cgst){
					taxTotal+= orderData.total*this.tsProduct.tax.cgst/100;
				}
				if(this.tsProduct.tax.sgst){
					taxTotal+= orderData.total*this.tsProduct.tax.sgst/100;
				}
				orderData.total =parseFloat((orderData.total + taxTotal).toFixed(2))
			}
			this.panelLoader="show";

			if(this.plan){
				if(!orderData.customParams){
					orderData.customParams = {};
				}

				orderData.customParams.emiPlan= this.plan;
			}
			this.orderService.placeOrder(this.sessionStd, orderData).subscribe(
				res=>{
					this.order=res;
					this.activeStep=3;
					console.log(this.activeStep);
					this.panelLoader="none";
				},
				err=>{
					this.panelLoader="none";
					this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
				}
			);
		}
	}

	enrollFreeProduct(){
		let data={
			user : this.student.user,
			product : this.productId
		};
		this.panelLoader="show";
		this.orderService.enrollFreeProduct(data).subscribe(
			res=>{
				this.notifier.alert('Success','Erolled Successfully', "success", 5000);
				this.panelLoader="none";
				// this.router.navigate(['/student/testment/testseries']);
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
			}
		)
	}


	getProductPaymentOptions(){
        this.productService.getProductPaymentOptions(this.productId).subscribe(
            res=>{
				this.paymentOptions= this.groupBy(res,'emiPlan');
				this.paymentOptKeys= Object.keys(this.paymentOptions);
				if(this.paymentOptKeys.length<1){
					this.placeOrder();
				}else
				if(this.paymentOptKeys[0]=='undefined'){
					this.placeOrder();
				}else{
					this.showEMIModal=true;
				}
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }

    groupBy(xs, key) {
        return xs.reduce(function(rv, x) {
          (rv[x[key]] = rv[x[key]] || []).push(x);
          return rv;
        }, {});
    };

    setEMIPlan(plan){
        this.plan = plan;
		this.placeOrder();
		this.showEMIModal=false;
    }

}
