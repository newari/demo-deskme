import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentCourseHomeComponent } from './home.component';
import { OrderService } from '../../../../../lib/services/order.service';
import { StudentAuthGuard } from '../../../shared/services/student-auth-guard';

export const ROUTES:Routes=[
    {path: '', component: StudentCourseHomeComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentCourseHomeComponent],
    imports:[
        CommonModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[OrderService]
})
export class StudentCourseHomeActivity { }