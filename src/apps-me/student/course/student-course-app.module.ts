import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";
import { StudentCourseAppComponent } from './app.component';

@NgModule({
    declarations: [StudentCourseAppComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[]
})
export class StudentCourseAppModule { }
