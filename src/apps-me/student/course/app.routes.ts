import { Routes, CanActivateChild } from '@angular/router';

import { StudentCourseAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  { 
    path: '',
    component:StudentCourseAppComponent,
    children: [
      	{ path: '', loadChildren: './activities/home/home.activity.sch#StudentCourseHomeActivity' },
    ]
  }
]
