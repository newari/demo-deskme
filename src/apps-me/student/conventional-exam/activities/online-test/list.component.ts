import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { ConventionalExamService } from '../../../../../lib/services/conventional-exam.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import * as moment from 'moment';
import { FilemanagerService } from '../../../../../lib/components/filemanager/filemanager.service';
import { FeedbackService } from '../../../../../lib/services/feedback.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { VideoService } from '../../../../../lib/services/video.service';
import { SettingsService } from '../../../../../lib/services/settings.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './list.component.html'
})
export class StudentConventionalExamListComponent {
	ceTestSeries:any[];
	videoSolutions:any;
    activeTest:any;
    activeRowIndex:number;
    answerUploadModal:boolean;
    panelLoader:string='none';
	sessionStd:any;
	feedbackList:any;
	viewPdfModal:boolean;
	viewTopSolutionModal:boolean;
	instructionModal:boolean;
	confirmToCheck:boolean;
	topSolutions:any;
	showFeedBackOption:boolean;
	feedBackForm:FormGroup;
	showVideo:boolean;
	videoLimit:number;
	videoTimerId:any;
	videoData:any;
	viewLimitMsg:string;
	showTestSolnAlways:boolean=false;
	activeTestSeriesIndex;
	displayVideoModal : boolean=false;
	evaluatedVideoURL :any;
	displayPreDisVideoModal :boolean=false;
	preDisVideoURL : any;
	currentDate : any;
	displaytop5SheetsModal :boolean = false;
	topStudents : any = [];
	activeTestSeries : number = 0;
	testSeriesId : any;
    constructor(
      	private ceService:ConventionalExamService,
      	private authService: AuthService,
        private el: ElementRef,
        private notifier: NotifierService,
		private fmService:FilemanagerService,
		private feedbackService:FeedbackService,
		private fb:FormBuilder,
		private videoService:VideoService,
		private sanitizer:DomSanitizer,
		private settingService: SettingsService,
		private activatedRoute : ActivatedRoute
    ){
        
    }

    ngOnInit(){
		this.sessionStd=this.authService.student();
		this.activatedRoute.queryParams.subscribe(
			params=>{
				this.testSeriesId = params['ts'];
			}
		);
		this.settingService.getSettingByKey('CONV_EXAM_SOLN_ALWAYS_DWNLD').subscribe(
			res=>{
				this.showTestSolnAlways=res.value;
				console.log(this.showTestSolnAlways);
				this.loadTests(this.sessionStd.id);
			},
			err=>{
				this.showTestSolnAlways=false;
				this.loadTests(this.sessionStd.id);
			}
		)
		this.feedBackForm=	this.fb.group({
			question:['null', Validators.required],
			response:['null', Validators.required]
		})
		this.currentDate = new Date();
    }
    loadTests(userId){
      	this.ceService.getUserTests(userId).subscribe(
	        res=>{				
				for (let i = 0; i < res.length; i++) {
					let testCount =res[i].tests.length;
					if(this.testSeriesId == res[i].tsDetail._id){
						this.activeTestSeries = i;
					}
					for (let testIndex = 0; testIndex < testCount; testIndex++) {
						res[i].tests[testIndex].enablePaperDownload = moment().isSameOrAfter(res[i].tests[testIndex].test.startDate);
						res[i].tests[testIndex].enablePaperUpload = moment().isBetween(res[i].tests[testIndex].test.startDate, moment(res[i].tests[testIndex].test.endDate).endOf("day"));
						res[i].tests[testIndex].enableSolnDownload =  moment().isSameOrAfter(res[i].tests[testIndex].test.endDate);
						res[i].tests[testIndex].enablePreDisVideo= moment().isSameOrAfter(res[i].tests[testIndex].test.startDate);
						
						if(res[i].tests[testIndex].enableSolnDownload){
							if(this.showTestSolnAlways){
								res[i].tests[testIndex].enableSolnDownload=true;
							}
							else{
								if(!res[i].confirmedToCheck){
									res[i].tests[testIndex].enableSolnDownload=false;
								}
							}
						}
						else{
							if(!this.showTestSolnAlways && res[i].confirmedToCheck ){
								res[i].tests[testIndex].enableSolnDownload=true;
							}
						}
						if(res[i].tests[testIndex].test.isSolnEnable){
							if(res[i].tests[testIndex].stdResponseFile && res[i].tests[testIndex].enablePaperUpload){
								res[i].tests[testIndex].enableSolnDownload=true;
							}
						}
					}								
				}				
				  this.ceTestSeries=res;
	        },
	        err=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
	        }
      	)
    }
    showAnswerUploadModal(testSeriesIndex,testIndex){
		this.activeTest=this.ceTestSeries[testSeriesIndex].tests[testIndex];
		this.activeRowIndex=testIndex;
		this.activeTestSeriesIndex=testSeriesIndex;
      	this.answerUploadModal=true;
	}
	showViewPdfModal(testSeriesIndex,testIndex){
		this.activeTest=this.ceTestSeries[testSeriesIndex].tests[testIndex];
		this.activeRowIndex=testIndex;
		this.activeTestSeriesIndex=testSeriesIndex;
		this.viewPdfModal=true;
	  }
	showInstructionModal(){
		this.instructionModal=true;
	}
    uploadFile(cb) {
    	//locate the file element meant for the file upload.
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#fmFile');
    	//get the total amount of files attached to the file input.
        let fileCount: number = inputEl.files.length;
    	//create a new fromdata instance
        this.panelLoader="show";
    	//check if the filecount is greater than zero, to be sure a file was selected.
        if (fileCount > 0) { // a file was selected
            let formData = new FormData();
            //append the key name 'photo' with the first file in the element
			      formData.append('fmFile', inputEl.files.item(0));
            //call the angular http method
    			  this.fmService.uploadFile(formData).subscribe(
                res=>{
                    this.panelLoader="none";
					return cb(null, res);
					
                },
                err=>{
                    this.panelLoader="none";
                    return cb(err);
                    // this.notifier.alert(err.code, err.message, 'danger');
                    // this.fh.upload1Status=err.message;
                    
                }
            )
    	}
    }
    uploadAnswerSheet(){
		if(!this.activeTest){
			this.answerUploadModal=false;
			return;
		}
		var self=this;
		this.panelLoader="show";		
      	this.uploadFile(function(err, file){
	        if(err){
				self.panelLoader="none";
				return self.notifier.alert(err.code, err.message, "danger", 5000);
	        }
	        let data={
				stdResponseFile:file.url,
				sendSMS : 'CONVENTIONAL_TEST_STD_UPLOAD_SHEET',
				isPdfInvalid : false,
				reasonOfInvalid : '',
				confirmedToCheck : false,
			}
			self.ceService.updateStudentResponse(self.activeTest._id, data).subscribe(
				res=>{
					self.panelLoader="none";
					self.answerUploadModal=false;
					self.ceTestSeries[self.activeTestSeriesIndex].tests[self.activeRowIndex].stdResponseFile=res.stdResponseFile;
					self.ceTestSeries[self.activeTestSeriesIndex].tests[self.activeRowIndex].isPdfInvalid=res.isPdfInvalid;
					self.ceTestSeries[self.activeTestSeriesIndex].tests[self.activeRowIndex].confirmedToCheck=res.confirmedToCheck;
					self.notifier.alert('Uploaded', 'Your response uploded successfully!', "success", 5000);
				},
				err=>{
					self.panelLoader="none";
					self.notifier.alert(err.code, err.message, "danger", 5000);
				}
			)
      	});
	}
	pageRendered(e: CustomEvent) {
		console.log('(page-rendered)', e);
	}
	confirmedToCheck(testSeriesIndex,testIndex){
		let confirmedAt=new Date();
		let data={confirmedToCheck:true, confirmedAt:confirmedAt};
		this.ceService.updateStudentResponse(this.ceTestSeries[testSeriesIndex].tests[testIndex]._id, data).subscribe(
			res=>{
				this.panelLoader="none";
				this.ceTestSeries[testSeriesIndex].tests[testIndex].confirmedToCheck=true;
				this.notifier.alert('Confirmed', 'Confirmed to Check Successfully!', "success", 500);
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
	}
	
	loadFeedBack(){
		this.feedbackService.getFeedback().subscribe(
			(res)=>{
				this.feedbackList=res;
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 500);
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}
	
	showFeedBackModal(testSeriesIndex,testIndex){
		this.activeTest = this.ceTestSeries[testSeriesIndex].tests[testIndex];
		this.activeRowIndex = testIndex;
		this.showFeedBackOption=true;
		this.loadFeedBack();
	}
	submitFeedBack(quesId){
		if (!this.feedBackForm.valid) {
			return;
		}

		let formData= this.feedBackForm.value;
		formData.question=quesId;
		formData.user=this.sessionStd.id;
		formData.test = this.activeTest.test._id;
		 this.feedbackService.addFeedbackResponse(formData).subscribe(
			 (res)=>{
				 this.notifier.alert('success','Added Success Fully', 'success', 500);
			 },
			 (err) => {
				 this.notifier.alert(err.code, err.message, 'danger', 5000);
			 }
		 );
	}
	// showVideoModal(testSeriesIndex,testIndex, videoIndex){
	// 	this.viewLimitMsg=null;
	// 	this.activeTest = this.ceTestSeries[testSeriesIndex].tests[testIndex];
	// 	this.videoSolutions=null;
	// 	this.showVideo = true;
	// 	this.panelLoader="show";
	// 	this.videoService.getOneVideo(this.activeTest.test.solVideos[videoIndex]).subscribe(
	// 		(res) => {
	// 			this.videoData = res;
	// 			if (this.videoData) {
	// 				this.videoService.getVideoViews(this.activeTest.user, this.activeTest.test.solVideos[videoIndex]).subscribe(
	// 					(res) => {
	// 						this.videoLimit = res;
	// 						if (!res.totalViews || (res.totalViews < this.activeTest.test.videoViewsLimit)) {

	// 							let self = this;
	// 							self.panelLoader = 'none';
	// 							self.videoSolutions = self.sanitizer.bypassSecurityTrustResourceUrl(self.videoData.videoSource);
	// 							self.videoTimerId = window.setTimeout(() => {
	// 								self.videoService.addVideoView({ user: self.activeTest.user, video: self.activeTest.test.solVideos[videoIndex] }).subscribe(
	// 									(res) => { },
	// 									(err) => { }
	// 								)
	// 							}, 500000);
	// 						} else {
	// 							this.showVideo = false;
	// 							this.viewLimitMsg = "Video view limt(" + this.activeTest.test.videoViewsLimit + " views) has been exceeded!";
	// 						}
	// 					},
	// 					(err) => {
	// 						this.notifier.alert(err.code, err.message, 'danger', 5000);
	// 					}
	// 				);
	// 			}
	// 		},
	// 		(err) => {
	// 			this.notifier.alert(err.code, err.message, 'danger', 5000);
	// 		}
	// 	)
				
		
	// 	// this.videoSolutions = this.sanitizer.bypassSecurityTrustResourceUrl(this.activeTest.solVideos[videoIndex]);
	// 	// this.showVideo=true;
	// 	// let self=this;
	// 	// let views=0;
	// 	// if (self.activeTest.views) {
	// 	// 	views = self.activeTest.views;
	// 	// }else{
	// 	// 	views= 0;
	// 	// }
	// 	// self.videoTimerId=window.setTimeout(() => { 			
	// 	// 	self.ceService.updateStudentResponse(self.activeTest.id, {views:views+1}).subscribe(
	// 	// 		(res)=>{},
	// 	// 		(err)=> {}
	// 	// 	)
	// 	// }, 5000);
	// }
	showVideoModal(videoId,source,tsIndex,tIndex){
		this.videoSolutions=null;
		this.activeTest = this.ceTestSeries[tsIndex].tests[tIndex];
		this.showVideo = true;
		this.panelLoader="show";
		this.viewLimitMsg='';
		this.videoService.getVideoViews(this.activeTest.user, videoId).subscribe(
			(res) => {
				this.videoLimit = res;
				if (!res.totalViews || (res.totalViews < this.activeTest.test.videoViewsLimit)) {
					let self = this;
					self.panelLoader = 'none';
					self.videoSolutions = this.getVideoURL(videoId,source);
					self.videoTimerId = window.setTimeout(() => {
						self.videoService.addVideoView({ user: self.activeTest.user, video: videoId }).subscribe(
							(res) => { },
							(err) => { }
						)
					}, 300000);
				} else {
					this.viewLimitMsg = "Video view limit(" + this.activeTest.test.videoViewsLimit + " views) has been exceeded!";
				}
			},
			(err) => {
				this.panelLoader="none";
				return this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}
	closeVideo(){
		clearTimeout(this.videoTimerId);	
	}
	getVideoURL(videoId,source){
		let url = '';
		if(source ==='youtube'){
			url = 'https://www.youtube.com/embed/' + videoId;
		}else if(source ==='vimeo'){
			url = 'https://player.vimeo.com/video/' + videoId;
		}
		return this.sanitizer.bypassSecurityTrustResourceUrl(url);
	}
	showEvaluatedVideoModal(url){
		this.displayVideoModal=true;
		this.evaluatedVideoURL= this.sanitizer.bypassSecurityTrustResourceUrl(url);
	}
	showPreDisVideosModal(ceTest){
		this.displayPreDisVideoModal = true;
		this.activeTest = ceTest;
		this.preDisVideoURL= this.getVideoURL(this.activeTest.test.preDiscussionVideos.ids[0],this.activeTest.test.preDiscussionVideos.source);
	}
	changeVideoURL(videoId,source){
		this.preDisVideoURL = this.getVideoURL(videoId,source);
	}
	showTop5SheetsModal(data){
		this.displaytop5SheetsModal = true;
		let filter={
			test : data.test._id,
			testSeries : data.testSeries,
		};
		this.ceService.getTopStudents(filter).subscribe(
			res=>{this.topStudents=res},
			err=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		);
	}
}
