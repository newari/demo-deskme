import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentConventionalExamListComponent } from './list.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { ConventionalExamService } from '../../../../../lib/services/conventional-exam.service';
import { DialogModule, AccordionModule, FieldsetModule } from 'primeng/primeng';
import { FilemanagerService } from '../../../../../lib/components/filemanager/filemanager.service';
import { FeedbackService } from "../../../../../lib/services/feedback.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { VideoService } from "../../../../../lib/services/video.service";
import { SettingsService } from "../../../../../lib/services/settings.service";
export const ROUTES:Routes=[
    {path: '', component: StudentConventionalExamListComponent, canActivate: [StudentAuthGuard],pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentConventionalExamListComponent],
    imports:[
        CommonModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ROUTES),
        AccordionModule,
        FieldsetModule
    ],
    providers: [ConventionalExamService, FilemanagerService, FeedbackService, VideoService, SettingsService]
})
export class StudentConventionalExamOnlineTestActivity { }