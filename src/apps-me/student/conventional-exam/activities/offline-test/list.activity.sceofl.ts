import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { ConventionalExamService } from '../../../../../lib/services/conventional-exam.service';
import { StudentConventionalExamOfflineTestListComponent } from "./list.component";
import { DialogModule, AccordionModule, TabViewModule, FieldsetModule } from "primeng/primeng";
export const ROUTES:Routes=[
    {path: '', component: StudentConventionalExamOfflineTestListComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Offline Test'}},
];

@NgModule({
    declarations: [StudentConventionalExamOfflineTestListComponent],
    imports:[
        CommonModule,DialogModule,
        RouterModule.forChild(ROUTES),
        AccordionModule,
        TabViewModule,
        FieldsetModule
    ],
    providers:[ConventionalExamService]
})
export class StudentConventionalExamOfflineTestListActivity { }