import { Component, ElementRef, OnInit } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { ConventionalExamService } from '../../../../../lib/services/conventional-exam.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import * as moment from 'moment';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  templateUrl: './list.component.html'
})
export class StudentConventionalExamOfflineTestListComponent implements OnInit {
    ceTests:any[];
    activeTest:any;
    panelLoader:string;
	sessionStd:any;
	ceTest;
	viewTopSolutionModal:boolean;
	topSolutions;
	videoSolution: any;
	showVideo: boolean;
	videoTimerId:any;
    constructor(
      	private ceService:ConventionalExamService,
      	private authService: AuthService,
        private el: ElementRef,
		private notifier: NotifierService,
		private sanitizer:DomSanitizer
    ){
        
    }

    ngOnInit(){
		this.sessionStd=this.authService.student();
		this.loadTests(this.sessionStd.id);
    }
    loadTests(userId){
      	this.ceService.getUserOfflineTests().subscribe(
	        (res)=>{
				let toDate=moment.now();
					this.ceTests=res;
				// let mapedRes=tests.map(function(record){
				// 	record.enablePaperDownload=moment().isAfter(record.startDate);
				// 	record.enableSolnDownload=moment().isAfter(record.startDate);
				// 	return record;
				// });
	          	// this.ceTests=mapedRes;
	        },
	        (err)=>{
				console.log(err);
				this.notifier.alert(err.code, err.message, "danger", 5000);
	        }
      	)
    }
    
	checkEnabled(date?:Date){
		return	moment().isAfter(date);
	}
	pageRendered(e: CustomEvent) {
		console.log('(page-rendered)', e);
	}
	showSolutionModal(productIndex,testSeriesIndex,testIndex){
		this.viewTopSolutionModal=true;
		if (this.ceTests[productIndex]&& this.ceTests[productIndex].testSerieses && this.ceTests[productIndex].testSerieses[testSeriesIndex] && this.ceTests[productIndex].testSerieses[testSeriesIndex].tests&&this.ceTests[productIndex].testSerieses[testSeriesIndex].tests[testIndex]) {
			this.ceTest = this.ceTests[productIndex].testSerieses[testSeriesIndex].tests[testIndex];
		}
	}
	showVideoModal(testIndex) {
		this.activeTest = this.ceTests[testIndex];
		this.videoSolution = this.sanitizer.bypassSecurityTrustResourceUrl(this.activeTest.test.solVideo);
		this.showVideo = true;
		let self = this;
		let views = 0;
		if (self.activeTest.views) {
			views = self.activeTest.views
		} else {
			views = 0;
		}
		self.videoTimerId = window.setTimeout(() => {
			self.ceService.updateStudentResponse(self.activeTest.id, { views: views + 1 }).subscribe(
				(res) => { },
				(err) => { }
			)
		}, 5000);
	}
	closeVideo() {
		clearTimeout(this.videoTimerId);
	}
}
