import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentTestMentDashboardComponent } from './dashboard';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { DialogModule } from 'primeng/primeng';
import { ConventionalExamService } from "../../../../../lib/services/conventional-exam.service";
export const ROUTES:Routes=[
    {path: '', component: StudentTestMentDashboardComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentTestMentDashboardComponent],
    imports:[
        CommonModule,
        DialogModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[ConventionalExamService]
})
export class StudentConventionalExamDashboardActivity { }