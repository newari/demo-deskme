import {Routes} from "@angular/router";

import {AppActivity} from "./app.activity";
import { StudentLoginContent } from './activities/login/login.component';

export const APP_ROUTES:Routes=[
  { path:'',
    component:AppActivity,
    children: [
      {path: 'login', component: StudentLoginContent, pathMatch:'full', data: {title: 'Login - Student Zone'}},
    ]
  }
]
