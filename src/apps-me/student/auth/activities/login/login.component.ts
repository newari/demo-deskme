import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
    selector: 'student-login',
    templateUrl:'./login.component.html'
})
export class StudentLoginContent implements OnInit { 
   loginWith: any;
    constructor(
        private activatedRoute:ActivatedRoute,
        private router: Router
    ){}

    ngOnInit(){
        this.activatedRoute.queryParams.subscribe(
            params=>{
               this.loginWith=params;
                
            }
        );
    }
}