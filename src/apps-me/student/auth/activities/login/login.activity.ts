import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { StudentLoginContent } from './login.component';
import { LoginboxModule } from '../../../../../lib/components/loginbox/loginbox.module';

@NgModule({
    declarations:[
        StudentLoginContent,
    ],
    imports:[
        LoginboxModule,

        FormsModule, 
        ReactiveFormsModule, 
        CommonModule
    ]
})
export class StudentLoginActivity { }