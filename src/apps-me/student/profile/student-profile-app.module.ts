import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";
import { StudentProfileAppComponent } from './app.component';

@NgModule({
    declarations: [StudentProfileAppComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[]
})
export class StudentProfileAppModule { }
