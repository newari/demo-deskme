import {Component} from "@angular/core";
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { UserService } from "../../../../../lib/services/user.service";
import { Student } from '../../../../../lib/models/student.model';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { StudentService } from "../../../../../lib/services/student.service";

@Component({
  templateUrl: './edit.component.html'
})
export class StudentProfileEditComponent {
    panelLoader:string="none";
    student:Student;
    changePasswordForm:FormGroup;
    cpfError:string=null;
    cpfStatus:string="Normal";
    addressForm:FormGroup;
    afError:string=null;
    afStatus:string="Normal";
    constructor(
        private userService:UserService,
      	private notifier:NotifierService,
        private studentService:StudentService,
        private fb:FormBuilder
    ){
        
    }

    ngOnInit(){
        this.changePasswordForm=this.fb.group({
            currentPassword:['', Validators.required],
            newPassword:['', Validators.required],
            newPasswordConf:['', Validators.required],
        })
        this.addressForm=this.fb.group({
            address:this.fb.group({
                address:['', Validators.required],
                landmark:['', Validators.required],
                city:['', Validators.required],
                state:['', Validators.required],
                postalCode:['', Validators.required],
                country:['India', Validators.required]
            }),
            permanantAddress:this.fb.group({
                address:['', Validators.required],
                landmark:['', Validators.required],
                city:['', Validators.required],
                state:['', Validators.required],
                postalCode:['', Validators.required],
                country:['India', Validators.required]
            })
        })


        this.panelLoader="show";
        this.studentService.getSessionStudent({populateUser:true, populateSession:true, populateCourse:true, populateBatch:true, populateProduct:true}).subscribe(
            res=>{
                this.panelLoader="none";
                this.student=res;
                this.addressForm.patchValue({address:res.address, permanantAddress:res.permanantAddress})
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }

    changePassword(){
        if(this.cpfStatus!="Normal"){
            return;
        }
        if(!this.changePasswordForm.valid){
            this.cpfError="Please fill all the fields!";
            return;
        }
        let frmData=this.changePasswordForm.value;
        if(frmData.newPassword!=frmData.newPasswordConf){
            this.cpfError="New Password and Confirm new Password value must be same!";
            return;
        }
        this.cpfStatus="Processing";
        this.userService.changeSessionPassword(frmData.currentPassword, frmData.newPassword).subscribe(
            res=>{
                this.cpfStatus="Normal";
                this.notifier.alert('Success', "Password has been changed!", "success", 10000);
            },
            err=>{
                this.cpfStatus="Normal";
                this.cpfError=err.message;
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )

    }

    updateAddress(){
        if(this.afStatus!="Normal"){
            return;
        }
        if(!this.addressForm.valid){
            this.afError="Please fill all the fields!";
            return;
        }
        let frmData=this.addressForm.value;
        
        this.afStatus="Processing";
        this.studentService.updateAddress(frmData.address, frmData.permanantAddress).subscribe(
            res=>{
                this.afStatus="Normal";
                this.notifier.alert('Success', 'Address updated successfully', "success", 5000);
                
            },
            err=>{
                this.afStatus="Normal";
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )
    }
}
