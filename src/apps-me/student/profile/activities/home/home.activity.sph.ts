import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentProfileHomeComponent } from './home.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { StudentService } from "../../../../../lib/services/student.service";

export const ROUTES:Routes=[
    {path: '', component: StudentProfileHomeComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'My Profile'}},
];

@NgModule({
    declarations: [StudentProfileHomeComponent],
    imports:[
        CommonModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[StudentService]
})
export class StudentProfileHomeActivity { }