import {Component} from "@angular/core";
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';
import { StudentService } from '../../../../../lib/services/student.service';
import { Student } from '../../../../../lib/models/student.model';

@Component({
  templateUrl: './home.component.html'
})
export class StudentProfileHomeComponent {
    panelLoader:string="none";
    student:Student;
    constructor(
      	private authService: AuthService,
      	private notifier:NotifierService,
      	private studentService:StudentService
    ){
        
    }

    ngOnInit(){
        var thisStd=this.authService.student();
        this.panelLoader="show";
        this.studentService.getSessionStudent({populateUser:true, populateSession:true, populateCourse:true, populateBatch:true, populateProduct:true, populateCenter:true}).subscribe(
            res=>{
                this.panelLoader="none";
                this.student=res;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }
    generateReferralCode(){
        if(this.student.user.referralCode){
            return;
        }
        this.panelLoader="show";
        this.studentService.generateReferralCode(this.student.user.id).subscribe(
            res=>{
                this.panelLoader="none";
                this.student.user.referralCode=res.referralCode;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }
}
