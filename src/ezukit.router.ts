import {Routes} from "@angular/router";

export const rootRouterConfig:Routes = [
  {path:'',
//   loadChildren: './apps/ims/ims.module#ImsModule'
redirectTo: 'student', pathMatch: 'full'
//   loadChildren: () => import('./apps/student/student.panel').then(m => m.StudentPanel)
},

  // {path: 'ims/auth', loadChildren: './apps/ims/auth/app.auth#AppModule'},
  // {path: 'ims', loadChildren: './apps/ims/ims.module#ImsModule'},
  // {path: 'faculty/auth', loadChildren: './apps/faculty/auth/faculty-login-app#FacultyLoginAppModule'},
  // {path: 'faculty', loadChildren: './apps/faculty/faculty.panel#FacultyPanel'},
  {path: 'student/auth',
//   loadChildren: './apps/student/auth/app#AppAuthModule',
  loadChildren: () => import('./apps/student/auth/app').then(m => m.AppAuthModule)
},
  {path: 'student',
//   loadChildren: './apps/student/student.panel#StudentPanel'
  loadChildren: () => import('./apps/student/student.panel').then(m => m.StudentPanel)

},

  // {path: 'staff/auth', loadChildren: './apps/staff/auth/app#AppModule'},
  // {path: 'staff', loadChildren: './apps/staff/staff.module#StaffModule'},

];
