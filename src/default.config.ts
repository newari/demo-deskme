export class DefaultConfig{
    basicsConfig:any;
    constructor(){
        if(process.env.NODE_ENV=='development'){
            this.basicsConfig={
                API_URL: 'https://mentorseduserv.online/api',
                INTEK_API_URL: 'http://localhost:1338',
                CDN_URL: 'https://mentorseduserv.online'
            }
        }else if(process.env.NODE_ENV!=='production'){
            this.basicsConfig={
                API_URL:'http://localhost:1337/api',
                INTEK_API_URL:'http://localhost:1338',
                CDN_URL:'http://localhost:1337'
            }
        }else{
            this.basicsConfig={
                API_URL:'https://mentorseduserv.online/api',
                INTEK_API_URL:'https://intek.edukitapp.com',
                // CDN_URL:'https://hub.iesmaster.org',
                CDN_URL:'/resources',
            }
        }
    }
    getBasics(){
        return this.basicsConfig;
    }
}
