import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Downloadcategory} from "../models/downloadcategory.model";

@Injectable() 
export class DownloadcategoryService{
    private downloadcategory: Downloadcategory;
    constructor(private http:HttpClient){ }

    addDownloadcategory(downloadcategory:Downloadcategory) : Observable<Downloadcategory>{
        return this.http.post<Downloadcategory>(EdukitConfig.BASICS.API_URL+"/webber/downloadcategory", downloadcategory);
    }

    getDownloadcategory(filter?: any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/downloadcategory", opts);
    }

    getOneDownloadcategory(downloadcategoryId) :Observable<Downloadcategory>{
        return this.http.get<Downloadcategory>(EdukitConfig.BASICS.API_URL+"/webber/downloadcategory/"+downloadcategoryId);
    }

    updateDownloadcategory(downloadcategoryId, downloadcategory:Downloadcategory) : Observable<Downloadcategory>{
        return this.http.put<Downloadcategory>(EdukitConfig.BASICS.API_URL+"/webber/downloadcategory/"+downloadcategoryId, downloadcategory);
    }

    deleteDownloadcategory(downloadcategoryId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/downloadcategory/"+downloadcategoryId);
    }
    getStdDownloadCategory(filter?: any): Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/student/downloadcategory", { params: filter });
    }
    getOneDownloadcategoryData(downloadcategoryId): Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/webber/download/category/" + downloadcategoryId);

    }

}
    