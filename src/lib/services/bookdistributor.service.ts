import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';
import { BookDistributor } from "../models/bookdistrubutor.model";

@Injectable() 
export class BookDistributorService{
    private bookdistributor: BookDistributor;
    constructor(private http:HttpClient){ }

    addBookDistributor(bookdistributor:any) : Observable<BookDistributor>{
        return this.http.post<BookDistributor>(EdukitConfig.BASICS.API_URL+"/book-store/bookdistributor", bookdistributor);
    }

    getBookDistributor(filter?:any) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/book-store/bookdistributor", {params:filter});
    }

    getOneBookDistributor(bookdistributorId) :Observable<BookDistributor>{
        return this.http.get<BookDistributor>(EdukitConfig.BASICS.API_URL+"/book-store/bookdistributor/"+bookdistributorId);
    }

    updateBookDistributor(bookdistributorId, bookdistributor:any) : Observable<BookDistributor>{
        return this.http.put<BookDistributor>(EdukitConfig.BASICS.API_URL+"/book-store/bookdistributor/"+bookdistributorId, bookdistributor);
    }

    deleteBookDistributor(bookdistributorId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/book-store/bookdistributor/"+bookdistributorId);
    }
}
