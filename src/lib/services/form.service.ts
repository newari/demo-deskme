import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Form} from "../models/form.model";

@Injectable() 
export class FormService{
    private form: Form;
    constructor(private http:HttpClient){ }

    addForm(form:Form) : Observable<Form>{
        return this.http.post<Form>(EdukitConfig.BASICS.API_URL+"/form/form", form);
    }

    getForm(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/form/form",opts);
    }

    getOneForm(formId) :Observable<Form>{
        return this.http.get<Form>(EdukitConfig.BASICS.API_URL+"/form/form/"+formId);
    }

    updateForm(formId, form:Form) : Observable<Form>{
        return this.http.put<Form>(EdukitConfig.BASICS.API_URL+"/form/form/"+formId, form);
    }

    deleteForm(formId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/form/form/"+formId);
    }
    getFormResponse(filter?: any, withHeaders?: boolean):Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/form/formresponse", opts);
    }
    updateFormResponse(id, status?:any):Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/form/formresponse/"+id, status);
    }
    export(formId, filter?:any):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL + "/form/formresponse/export-data/"+formId, {params:filter});
    }
}
    