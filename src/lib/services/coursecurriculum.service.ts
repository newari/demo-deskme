import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import {CourseCurriculum} from "../models/coursecurriculum.model";
import { EdukitConfig } from "../../../src/ezukit.config";

@Injectable()
export class CourseCurriculumService{
    private courseCurriculum: CourseCurriculum;
    constructor(private http:HttpClient){ }

    addCourseCurriculum(courseCurriculum:CourseCurriculum) : Observable<CourseCurriculum>{
        return this.http.post<CourseCurriculum>(EdukitConfig.BASICS.API_URL+"/course-creator/coursecurriculum", courseCurriculum);
    }

    getCourseCurriculum(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/course-creator/coursecurriculum", opts);
    }

    getOneCourseCurriculum(courseCurriculumId) :Observable<CourseCurriculum>{
        return this.http.get<CourseCurriculum>(EdukitConfig.BASICS.API_URL+"/course-creator/coursecurriculum/"+courseCurriculumId);
    }

    // with subject names
    getOneCurriculum(courseCurriculumId, filter?:any) :Observable<CourseCurriculum>{
        return this.http.get<CourseCurriculum>(EdukitConfig.BASICS.API_URL+"/course-creator/curriculum/"+courseCurriculumId, { params: filter });
    }

    getCurriculumSubjects(courseCurriculumId, filter?:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/course-creator/curriculum/"+courseCurriculumId+"/subjects", { params: filter });
    }

    updateCourseCurriculum(courseCurriculumId, courseCurriculum:any) : Observable<CourseCurriculum>{
        return this.http.put<CourseCurriculum>(EdukitConfig.BASICS.API_URL+"/course-creator/coursecurriculum/"+courseCurriculumId, courseCurriculum);
    }

    addCourseCurriculumSubject(courseCurriculumId, subject:string) : Observable<CourseCurriculum>{
        return this.http.put<CourseCurriculum>(EdukitConfig.BASICS.API_URL+"/course-creator/curriculum/"+courseCurriculumId+"/subjects", {subject:subject});
    }

    deleteCourseCurriculum(courseCurriculumId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/course-creator/coursecurriculum/"+courseCurriculumId);
    }

    getSubjectwiseCurriculums(curriculums:any):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/course-creator/course-curriculum/subject-wise-data", {params:curriculums});
    }
    getCurriculumProducts(courseCurriculumId) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/course-creator/curriculum/"+courseCurriculumId+"/products");
    }
    sendLessonNotification(data:any) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/notification/androidpush-campaign/send-to-topic", data);
    }
    swapLessonSequence(courseCurriculumId:string, data:any) : Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/course-creator/curriculum/"+courseCurriculumId+"/swap-lessons", data);
    }
    getCourseCurriculumLessonWiseReports(curriculumtId:string, filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/course-creator/curriculum/"+curriculumtId+"/lesson-wise-report", opts);
    }

    getProductCurriculum(productId) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/course-creator/product/"+productId+"/curriculums");
    }
}
