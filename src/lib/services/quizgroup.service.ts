import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { QuizGroup } from "../models/quizgroup.model";

@Injectable() 
export class QuizGroupService{
    private quizGroup: QuizGroup;
    constructor(private http:HttpClient){ }

    addQuizGroup(data:any) : Observable<QuizGroup>{
        return this.http.post<QuizGroup>(EdukitConfig.BASICS.API_URL+"/qbank/quizgroup", data);
    }
    getQuizGroup(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if(withHeaders){
            opts.observe = 'response';
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/quizgroup", opts);
    }
    getOneQuizGroup(quizGroupId) :Observable<QuizGroup>{
        return this.http.get<QuizGroup>(EdukitConfig.BASICS.API_URL+"/qbank/quizgroup/"+quizGroupId);
    }
    updateQuizGroup(quizGroupId, quiz:any) : Observable<QuizGroup>{
        return this.http.put<QuizGroup>(EdukitConfig.BASICS.API_URL+"/qbank/quizgroup/"+quizGroupId, quiz);
    }
    deleteQuizGroup(quizGroupId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/qbank/quizgroup/"+quizGroupId);
    }
    getQuizGroupQsets(quizGroupId):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/qbank/quizgroup/qsets/"+quizGroupId);
    }
    
}
    
