import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Firm} from "../models/firm.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class FirmService{
    private firm: Firm;
    constructor(private http:HttpClient){ }

    addFirm(firm:Firm) : Observable<Firm>{
        return this.http.post<Firm>(EdukitConfig.BASICS.API_URL+"/org/firm", firm);
    }

    getFirm() :Observable<Firm[]>{
        return this.http.get<Firm[]>(EdukitConfig.BASICS.API_URL+"/org/firm");
    }

    getOneFirm(firmId) :Observable<Firm>{
        return this.http.get<Firm>(EdukitConfig.BASICS.API_URL+"/org/firm/"+firmId);
    }

    updateFirm(firmId, firm:Firm) : Observable<Firm>{
        return this.http.put<Firm>(EdukitConfig.BASICS.API_URL+"/org/firm/"+firmId, firm);
    }

    deleteFirm(firmId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/org/firm/"+firmId);
    }
}
    
