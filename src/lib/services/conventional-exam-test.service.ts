import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { ConventionalExamTest } from "../models/conventionalExamTest.model";

@Injectable()
export class ConventionalExamTestService{
    private test: ConventionalExamTest;
    constructor(private http:HttpClient){ }

    addTest(test:any) : Observable<ConventionalExamTest>{
        return this.http.post<ConventionalExamTest>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltest", test);
    }

    getTest(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltest", opts);
    }

    getAsigneeTest(assignedUesrId:string) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/conventional-exam/assignee/"+assignedUesrId+"/tests");
    }

    getOneTest(testId) :Observable<ConventionalExamTest>{
        return this.http.get<ConventionalExamTest>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltest/"+testId);
    }

    updateTest(testId, test:ConventionalExamTest) : Observable<ConventionalExamTest>{
        return this.http.put<ConventionalExamTest>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltest/"+testId, test);
    }
    duplicateTest(testId:string) : Observable<ConventionalExamTest>{
        return this.http.post<ConventionalExamTest>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltest/"+testId+"/duplicate", {});
    }
    deleteTest(testId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltest/"+testId);
    }
}

