import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import {Bookcategory} from "../models/bookcategory.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class BookcategoryService{
    private bookcategory: Bookcategory;
    constructor(private http:HttpClient){ }

    addBookcategory(bookcategory:any) : Observable<Bookcategory>{
        return this.http.post<Bookcategory>(EdukitConfig.BASICS.API_URL+"/book-store/bookcategory", bookcategory);
    }

    getBookcategory() :Observable<Bookcategory[]>{
        return this.http.get<Bookcategory[]>(EdukitConfig.BASICS.API_URL+"/book-store/bookcategory");
    }

    getOneBookcategory(bookcategoryId) :Observable<Bookcategory>{
        return this.http.get<Bookcategory>(EdukitConfig.BASICS.API_URL+"/book-store/bookcategory/"+bookcategoryId);
    }

    updateBookcategory(bookcategoryId, bookcategory:any) : Observable<Bookcategory>{
        return this.http.put<Bookcategory>(EdukitConfig.BASICS.API_URL+"/book-store/bookcategory/"+bookcategoryId, bookcategory);
    }

    deleteBookcategory(bookcategoryId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/book-store/bookcategory/"+bookcategoryId);
    }
}
