import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import {Forumquestion} from "../models/forumquestion.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class ForumquestionService{
    private forumquestion: Forumquestion;
    constructor(private http:HttpClient){ }

    addForumquestion(forumquestion:any) : Observable<Forumquestion>{
        return this.http.post<Forumquestion>(EdukitConfig.BASICS.API_URL+"/forum/question", forumquestion);
    }

    addForumquestionReply(forumquestion:Forumquestion) : Observable<Forumquestion>{
        return this.http.post<Forumquestion>(EdukitConfig.BASICS.API_URL+"/forum/question/reply", forumquestion);
    }

    getForumquestion(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe="response";
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/forum/question", opts);
    }

    getForumquestionReply(questionId:string, filter?:any,withHeaders?:boolean) :Observable<any>{
        if(!filter){
            var filter:any={};
        }
        filter.parentId=questionId;
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe="response";
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/forum/question", opts);
    }

    getOneForumquestion(forumquestionId) :Observable<Forumquestion>{
        return this.http.get<Forumquestion>(EdukitConfig.BASICS.API_URL+"/forum/question/"+forumquestionId);
    }

    updateForumquestion(forumquestionId, forumquestion:any) : Observable<Forumquestion>{
        return this.http.put<Forumquestion>(EdukitConfig.BASICS.API_URL+"/forum/question/"+forumquestionId, forumquestion);
    }

    patchUpdateForumquestion(forumquestionId, forumquestion:any) : Observable<Forumquestion>{
        return this.http.put<Forumquestion>(EdukitConfig.BASICS.API_URL+"/forum/question/"+forumquestionId, forumquestion);
    }

    updateForumquestionSolStatus(forumquestionId, solStatus:boolean) : Observable<Forumquestion>{
        return this.http.put<Forumquestion>(EdukitConfig.BASICS.API_URL+"/forum/question/"+forumquestionId+"/sol-status", {solStatus:solStatus});
    }

    updateForumquestionReply(forumquestionId, forumquestion:Forumquestion) : Observable<Forumquestion>{
        return this.http.put<Forumquestion>(EdukitConfig.BASICS.API_URL+"/forum/question/reply/"+forumquestionId, forumquestion);
    }

    deleteForumquestion(forumquestionId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/forum/question/"+forumquestionId);
    }

    uploadFile(data:any) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/public/file/upload", data);
    }
    getForumSummary():Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/forum/summary-count");
    }

    getForumQuestionByLesson(threadId?:any, filter?:any):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/forum/quesiotn/by-thread/"+threadId, {params:filter});
    }
    getForumUsers(filter?:any, withHeaders?:boolean):Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe="response";
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/forum/users", opts);
    }

    blockUser(data:any) : Observable<Forumquestion>{
        return this.http.post<Forumquestion>(EdukitConfig.BASICS.API_URL+"/forum/block-user", data);
    }

    unBlockUser(data:any) : Observable<Forumquestion>{
        return this.http.post<Forumquestion>(EdukitConfig.BASICS.API_URL+"/forum/unblock-user", data);
    }
}

