import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";

import { Batch } from "../models/batch.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable()
export class BatchService {
    private batch: Batch;
    constructor(private http: HttpClient) { }

    addBatch(batch: Batch): Observable<Batch> {
        return this.http.post<Batch>(EdukitConfig.BASICS.API_URL + "/admin/batch", batch);
    }

    getBatch(filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/admin/batch", opts);
    }

    getOneBatch(batchId): Observable<Batch> {
        return this.http.get<Batch>(EdukitConfig.BASICS.API_URL + "/admin/batch/" + batchId);
    }

    updateBatch(batchId, batch: Batch): Observable<Batch> {
        return this.http.put<Batch>(EdukitConfig.BASICS.API_URL + "/admin/batch/" + batchId, batch);
    }

    deleteBatch(batchId): Observable<any> {
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL + "/admin/batch/" + batchId);
    }

    getUpcomingEvent(batchId): Observable<any> {
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/admin/batch/" + batchId + "/upcoming-event");
    }

    getFaculty(batchId): Observable<any> {
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/admin/batch/" + batchId + "/faculty");
    }

    addUserProduct(batchId, data): Observable<any> {
        return this.http.post<any>(EdukitConfig.BASICS.API_URL + "/admin/batch/" + batchId + "/user-product", data);
    }
    addBatchSeats(batch?: any): Observable<any> {
        return this.http.post(EdukitConfig.BASICS.API_URL + "/admin/batchseat/addbatchseats", batch);
    }
    getBatchSeats(batchId) : Observable<any> {
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/admin/batchseat/seat-by-batch/"+batchId);
    }

    getPerBatchSeat(batchId) : Observable<any> {
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/admin/batchseat/seat-by-batch/"+batchId);
    }

    deleteBatchSeats(batchseatId): Observable<any> {
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL + "/admin/batchseat" + batchseatId);
    }
    updateBatchSeat(batchSeatId, batchSeat: any): Observable<any> {
        return this.http.put<any>(EdukitConfig.BASICS.API_URL + "/admin/batchseat/" + batchSeatId, batchSeat);
    }
    getEnrollement(batchId:string, filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/admin/batch/"+batchId+"/enrollment", opts);

    }

    getIdCardTemplates():Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+ "/admin/idcardtemplate");
    }

    addIDCardTemplateToBatch(batchId, data?:any):Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+ "/admin/idcard-template/add-to-batch/"+batchId, data);

    }

    getBatchEnrolledStudents(bacthId): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL + "/admin/batch/"+bacthId+"/enrolled-student");
    }
    getBatchCount():Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/admin/batch/count");
    }

    getBatchSubjects(batchId): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/admin/batch/"+batchId+"/subjects")
    }

    addBatchSubject(batchId, data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+ `/admin/batch/${batchId}/add-subject`,data);
    }
    removeBatchSubject(batchSubjectId): Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+ `/admin/batch-subject/${batchSubjectId}`);
    }
}

