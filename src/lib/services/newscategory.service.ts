import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {NewsCategory} from "../models/newscategory.model";

@Injectable() 
export class NewsCategoryService{
    private newscategory: NewsCategory;
    constructor(private http:HttpClient){ }

    addNewscategory(newscategory:NewsCategory) : Observable<NewsCategory>{
        return this.http.post<NewsCategory>(EdukitConfig.BASICS.API_URL+"/webber/newscategory", newscategory);
    }

    getNewscategory(filter?: any, withHeaders?:boolean) :Observable<any>{
         let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/newscategory", opts);
    }

    getOneNewscategory(newscategoryId) :Observable<NewsCategory>{
        return this.http.get<NewsCategory>(EdukitConfig.BASICS.API_URL+"/webber/newscategory/"+newscategoryId);
    }

    updateNewscategory(newscategoryId, newscategory:NewsCategory) : Observable<NewsCategory>{
        return this.http.put<NewsCategory>(EdukitConfig.BASICS.API_URL+"/webber/newscategory/"+newscategoryId, newscategory);
    }

    deleteNewscategory(newscategoryId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/newscategory/"+newscategoryId);
    }
}
    