import { Injectable } from "@angular/core";

import "rxjs/Rx";
import { Observable } from "rxjs";


import {Freenote} from "../models/freenote.model";
import { EdukitConfig } from "../../ezukit.config";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class FreenoteService{
    private freenote: Freenote;
    constructor(private http:HttpClient){ }

    addFreenote(freenote:Freenote) : Observable<Freenote>{
        return this.http.post<Freenote>(EdukitConfig.BASICS.API_URL+"/webber/note", freenote);
    }

    getFreenote(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders) opts.observe="response";
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/note", opts) ;
    }

    getOneFreenote(freenoteId) :Observable<Freenote>{
        return this.http.get<Freenote>(EdukitConfig.BASICS.API_URL+"/webber/note/"+freenoteId)
    }

    updateFreenote(freenoteId, freenote:Freenote) : Observable<Freenote>{
        return this.http.put<Freenote>(EdukitConfig.BASICS.API_URL+"/webber/note/"+freenoteId, freenote)
    }

    deleteFreenote(freenoteId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/note/"+freenoteId);
    }
    duplicate(freenoteId): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+`/webber/note/${freenoteId}/make-duplicate`);
    }
}
