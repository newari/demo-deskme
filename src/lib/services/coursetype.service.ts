import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import {Coursetype} from "../models/coursetype.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class CoursetypeService{
    private coursetype: Coursetype;
    constructor(private http:HttpClient){ }

    addCoursetype(coursetype:any) : Observable<Coursetype>{
        return this.http.post<Coursetype>(EdukitConfig.BASICS.API_URL+"/admin/coursetype", coursetype);
    }

    getCoursetype() :Observable<Coursetype[]>{
        return this.http.get<Coursetype[]>(EdukitConfig.BASICS.API_URL+"/admin/coursetype");
    }

    getOneCoursetype(coursetypeId) :Observable<Coursetype>{
        return this.http.get<Coursetype>(EdukitConfig.BASICS.API_URL+"/admin/coursetype/"+coursetypeId);
    }

    updateCoursetype(coursetypeId, coursetype:Coursetype) : Observable<Coursetype>{
        return this.http.put<Coursetype>(EdukitConfig.BASICS.API_URL+"/admin/coursetype/"+coursetypeId, coursetype);
    }

    deleteCoursetype(coursetypeId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/admin/coursetype/"+coursetypeId);
    }
}
    
