import { Injectable } from "@angular/core";
// import "rxjs";
import { Observable } from "rxjs";
// import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';

import {Enquiry} from "../models/enquiry.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class EnquiryService{
    private enquiry: Enquiry;
    constructor(private http:HttpClient){ }

    addEnquiry(enquiryData:any) : Observable<Enquiry>{
        return this.http.post<Enquiry>(EdukitConfig.BASICS.API_URL+"/enquiry/enquiry", enquiryData);
    }

    registerEnquiry(enqData:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/register-enquiry", enqData);
    }

    quickRegisterEnquiry(enqData:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/enquiry/quick-register", enqData);
    }
    
    searchEnquiry(query) :Observable<Enquiry[]>{
        return this.http.get<Enquiry[]>(EdukitConfig.BASICS.API_URL+"/enquiry/enquiry/search", {params:query});
    }
    getEnquiry(filter?:any,withHeaders?:boolean) :Observable<any>{
        
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/enquiry/enquiry", opts);
    }

    getOneEnquiry(enquiryId) :Observable<Enquiry>{
        return this.http.get<Enquiry>(EdukitConfig.BASICS.API_URL+"/enquiry/enquiry/"+enquiryId);
    }

    getSessionEnquiry(filter?:any) :Observable<Enquiry>{
        return this.http.get<Enquiry>(EdukitConfig.BASICS.API_URL+"/enquiry/me", {params:filter});
    }

    updateEnquiry(enquiryId, enquiry:any) : Observable<Enquiry>{
        return this.http.put<Enquiry>(EdukitConfig.BASICS.API_URL+"/enquiry/enquiry/"+enquiryId, enquiry);
    }

    updateAddress(address, permanantAddress) : Observable<Enquiry>{
        return this.http.put<Enquiry>(EdukitConfig.BASICS.API_URL+"/enquiry/address", {address:address, permanantAddress:permanantAddress});
    }

    deleteEnquiry(enquiryId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/enquiry/enquiry/"+enquiryId);
    }
    
    exportEnquiries(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/enquiry/enquiry/export-enquiries", opts);
    }
    getEmployeeWiseReport(data:any){
        return this.http.get(EdukitConfig.BASICS.API_URL + "/enquiry/enquiry/report/employee", {params:data});
        
    }
    addActivity(data:any): Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL + "/enquiry/enquiry/add-activity", data);
    }
    getEnquiryActivity(enquiryId:string){
        return this.http.get(EdukitConfig.BASICS.API_URL + "/enquiry/enquiry/" + enquiryId+"/activity");
         
    }
    getEmployeeEnquiryReminder(employeeId, filter?:any, withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe='response';
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/enquiry/employee/" + employeeId + "/reminder", opts);    
    }
    getEnquiryReminder(filter?:any, withHeaders?:boolean):Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/enquiry/enquiry/reminders", opts);

    }
    exportEnquiryReminder(filter): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL + "/enquiry/enquiry/export-reminders", { params: filter });
        
    }

    exportEmployeeWiseEnquiryReminder(filter): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL + "/enquiry/enquiry/report/export-employee-report", {params:filter});

    }
    exportEmployeeEnquiryReminder(employeeId, filter): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/enquiry/employee/" + employeeId + "/export-reminder", { params: filter });

    }
    getEnquiryStats(filter?:any): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/enquiry/enquiry/stats", { params: filter });

    }
    getUserEnquiryStats(userEmail:string, filter?: any): Observable<any> {
        if(!filter){
            filter={};
        }
        filter.email=userEmail;
        return this.http.get(EdukitConfig.BASICS.API_URL + "/enquiry/enquiry/user-stats", { params: filter });

    }
    getEnquiryCountsByStatus(filter?:any): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/enquiry/enquiry-count-by-status",{params:filter});
    }
    convertEnquiryToStudent(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/enquiry/convert-enquiry-to-student", data);
    }
    getReminderCounts(): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/enquiry/enquiry/reminder-count");
    }
    updateEnquiryActivity(activityId, data?: any): Observable<any> {
        return this.http.put(EdukitConfig.BASICS.API_URL + "/enquiry/enquiryactivity/" + activityId, data);
    }
}
    
