import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";

import {Website} from "../models/website.model";
import { HttpClient } from "@angular/common/http";
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class WebsiteService{
    private website: Website;
    constructor(private http:HttpClient){ }

    addWebsite(website:any) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/webber/website", website);
    }

    getWebsite() :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/webber/website");
    }

    getOneWebsite(websiteId) :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/webber/website/"+websiteId);
    }



    updateWebsite(websiteId, website?:any) : Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/webber/website/"+websiteId, website);
    }

    getWebsiteTemplate(websiteId) :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/webber/website/"+websiteId+"/template");
    }

    updateWebsiteTemplate(websiteId:string, tmplt:any) : Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/webber/website/"+websiteId+"/template", tmplt);
    }
    addTemplateDefaultPage(websiteId:string, tmplt:any) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/webber/website/"+websiteId+"/template/default-page", tmplt);
    }

    deleteWebsite(websiteId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/website/"+websiteId);
    }
    getWebsiteCount(): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/webber/website/website-count");
    }
    templates(): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/webber/templates");
    }
    activateWebsiteTemplate(websiteId:string, tmplt:any): Observable<any> {
        return this.http.post(EdukitConfig.BASICS.API_URL + "/webber/website/"+websiteId+"/template/activate", tmplt);
    }
    downloadWebsiteTemplate(websiteId:string, tmplt:any): Observable<any> {
        return this.http.post(EdukitConfig.BASICS.API_URL + "/webber/website/"+websiteId+"/template/download", tmplt);
    }

    getWebsiteDownloadedTemplate(websiteId) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/webber/website/"+websiteId+"/downloaded-templates");
    }

    removeSiteMap(websiteId:any): Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/website/"+websiteId+"/remove-sitemap");
    }
}
