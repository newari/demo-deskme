import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import {BookMartCategory} from "../models/bookmartcategory.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class BookMartCategoryService{
    private bookMartCategory: BookMartCategory;
    constructor(private http:HttpClient){ }

    addBookMartCategory(bookMartCategory:any) : Observable<BookMartCategory>{
        return this.http.post<BookMartCategory>(EdukitConfig.BASICS.API_URL+"/book-mart/bookmartcategory", bookMartCategory);
    }

    getBookMartCategory() :Observable<BookMartCategory[]>{
        return this.http.get<BookMartCategory[]>(EdukitConfig.BASICS.API_URL+"/book-mart/bookmartcategory");
    }

    getOneBookMartCategory(bookMartCategoryId) :Observable<BookMartCategory>{
        return this.http.get<BookMartCategory>(EdukitConfig.BASICS.API_URL+"/book-mart/bookmartcategory/"+bookMartCategoryId);
    }

    updateBookMartCategory(bookMartCategoryId, bookMartCategory:any) : Observable<BookMartCategory>{
        return this.http.put<BookMartCategory>(EdukitConfig.BASICS.API_URL+"/book-mart/bookmartcategory/"+bookMartCategoryId, bookMartCategory);
    }

    deleteBookMartCategory(bookMartCategoryId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/book-mart/bookmartcategory/"+bookMartCategoryId);
    }
}
