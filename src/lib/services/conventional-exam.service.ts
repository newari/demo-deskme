import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class ConventionalExamService{
    constructor(private http:HttpClient){ }

    getUserTests(userId) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/conventional-exam/user/"+userId+"/tests");
    }

    getUserOfflineTests() :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/conventional-exam/user/offline-tests");
    }

    updateStudentResponse(id:string, data:any) :Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/test/user-response/"+id, data);
    }
    getTopStudents(filter?:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/conventional-exam/test/user/topstudents",{params:filter});
    }
    getOverAllReport(data): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/student/conventional-exam/dashboard/overallreport",{params: data});
    }
    getUpComingTests(filter?:any): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/student/conventional-exam/dashboard/upcomingtests",{params: filter});
    }
    // getLastAttemptedTest(userId): Observable<any[]> {
    //     return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/student/conventional-exam/dashboard/lastattempted",{params: {user : userId}});
    // }
    getDashboadTotals(): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/student/conventional-exam/dashboard/totals");
    }
}
    
