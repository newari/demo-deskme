import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Resident} from "../models/resident.model";

@Injectable() 
export class ResidentService{
    private resident: Resident;
    constructor(private http:HttpClient){ }

    addResident(resident:Resident) : Observable<Resident>{
        return this.http.post<Resident>(EdukitConfig.BASICS.API_URL+"/hostel/resident", resident);
    }

    getResident(filter?:any) :Observable<Resident[]>{
        return this.http.get<Resident[]>(EdukitConfig.BASICS.API_URL+"/hostel/resident", {params:filter});
    }

    getOneResident(residentId) :Observable<Resident>{
        return this.http.get<Resident>(EdukitConfig.BASICS.API_URL+"/hostel/resident/"+residentId);
    }

    updateResident(residentId, resident:Resident) : Observable<Resident>{
        return this.http.put<Resident>(EdukitConfig.BASICS.API_URL+"/hostel/resident/"+residentId, resident);
    }

    deleteResident(residentId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/hostel/resident/"+residentId);
    }
}
    