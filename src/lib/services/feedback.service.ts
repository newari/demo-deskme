import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Feedback} from "../models/feedback.model";

@Injectable() 
export class FeedbackService{
    private feedback: Feedback;
    constructor(private http:HttpClient){ }

    addFeedback(feedback:Feedback) : Observable<Feedback>{
        return this.http.post<Feedback>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexamfeedback", feedback);
    }

    getFeedback() :Observable<Feedback[]>{
        return this.http.get<Feedback[]>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexamfeedback");
    }

    getOneFeedback(feedbackId) :Observable<Feedback>{
        return this.http.get<Feedback>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexamfeedback/"+feedbackId);
    }

    updateFeedback(feedbackId, feedback:Feedback) : Observable<Feedback>{
        return this.http.put<Feedback>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexamfeedback/"+feedbackId, feedback);
    }

    deleteFeedback(feedbackId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexamfeedback/"+feedbackId);
    }

    addFeedbackQs(feedbackId,fbqsId){
        let body: any = { feedbackId: feedbackId, fbqsId: fbqsId };
        return this.http.put<Feedback>(EdukitConfig.BASICS.API_URL + "/conventional-exam/conventionalexamfeedback/add-qs/" , body);
    }
    removeFeedBackQs(feedbackId, fbqsId){
        let body: any = { feedbackId: feedbackId, fbqsId: fbqsId };
        return this.http.put<Feedback>(EdukitConfig.BASICS.API_URL + "/conventional-exam/conventionalexamfeedback/remove-qs/", body);

    }

    addFeedbackResponse(data): Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexamfeedbackresponse", data);
    }
    getFeedbackResponse(testId:string) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexam/feedback-response/"+testId);
    }
}
    