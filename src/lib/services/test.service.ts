import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { Test } from "../models/test.model";

@Injectable() 
export class TestService{
    private test: Test;
    constructor(private http:HttpClient){ }
    addTest(test:any) : Observable<Test>{
        return this.http.post<Test>(EdukitConfig.BASICS.API_URL+"/testment/test", test);
    }
    getTest(filter?:any,withHeaders?:any) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe = 'response';
        }
        return this.http.get<Test[]>(EdukitConfig.BASICS.API_URL+"/testment/test",opts);
    }
    getOneTest(testId) :Observable<Test>{
        return this.http.get<Test>(EdukitConfig.BASICS.API_URL+"/testment/test/"+testId);
    }
    updateTest(testId, test) : Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/testment/test/"+testId, test);
    }
    deleteTest(testId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/testment/test/"+testId);
    }
    getTotalTestPs(data):Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/ims/testment/test/totalparticipants",{params : data});
    }
    getTestParticipants(data):Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/ims/testment/test/participants",{params : data});
    }
    studentExport(data) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/ims/testment/test/studentexport",{params : data})
    }
    testQuestionwiseExport(data) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/ims/testment/test/report/questionwise/export",{params : data})
    }
    reEvaluateReport(testId) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/ims/testment/test/re-evaluate/"+testId);
    }
    getTestPrintQs(testId,filter?:any) :Observable<Test>{
        return this.http.get<Test>(EdukitConfig.BASICS.API_URL+"/testment/test/print/"+testId, {params:filter});
    }
    getAssignTests(filter) :Observable<Test[]>{
        return this.http.get<Test[]>(EdukitConfig.BASICS.API_URL+"/testment/test/assign",{params: filter});
    }
    getBonusMarkQs(qsetId) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/testment/test/bonusmarks/"+qsetId+"/qs",);
    }
    getTestsOrderSummary(filter):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/testment/test/ordersummary",{params:filter});
    }
    createQuickTest(test:any) : Observable<Test>{
        return this.http.post<Test>(EdukitConfig.BASICS.API_URL+"/testment/test/quick-test", test);
    }
}
    
