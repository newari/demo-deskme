import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";

import { EdukitConfig } from '../../ezukit.config';

import {Classtestresult} from "../models/classtestresult.model";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ClasstestresultService{
    private classtestresult: Classtestresult;
    constructor(private http:HttpClient){ }

    addClasstestresult(classtestresult:Classtestresult) : Observable<Classtestresult>{
        return this.http.post<Classtestresult>(EdukitConfig.BASICS.API_URL+"/class-test/classtestresponse", classtestresult)

    }

    getClasstestresult(filter?:any,withHeaders? : Boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<Classtestresult[]>(EdukitConfig.BASICS.API_URL+"/class-test/classtestresponse",opts);
    }

    getOneClasstestresult(classtestresultId) :Observable<Classtestresult>{
        return this.http.get<Classtestresult>(EdukitConfig.BASICS.API_URL+"/class-test/classtestresponse/"+classtestresultId)

    }

    updateClasstestresult(classtestresultId, classtestresult:Classtestresult) : Observable<Classtestresult>{
        return this.http.put<Classtestresult>(EdukitConfig.BASICS.API_URL+"/class-test/classtestresponse/"+classtestresultId, classtestresult)

    }

    deleteClasstestresult(classtestresultId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/class-test/classtestresponse/"+classtestresultId)

    }
    addMultipleResult(data?:any):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/class-test/classtestresponse/add-multi-result",data);
    }
    exportResult(testId): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/class-test/classtestresponse/export/"+testId);
    }
    getStudentAllClassTestsResult():Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+ "class-test/classtetresponse/student-class-test");
    }
    exportPerStudentWiseOverAllTestResult(studentId): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/class-test/classtestresponse/export-student-result/"+studentId);
    }
    generateRank(testId, rankStrategy): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/class-test/classtestresponse/generate-rank/" + testId, { params: { rankStrategy: rankStrategy}});
    }
    sendMessage(classTestId,classtestseriesId): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/class-test/classtestresponse/send-sms", {params:{testId:classTestId, testSeriesId:classtestseriesId}});
    }
    getUserTests(userId): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/class-test/user/" + userId + "/tests");

    }
    getPercentageOverview(testId): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/class-test/test/" + testId + "/percentage-overview");
    }
    addMenaulMultiResult(data?:any):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/class-test/classtest-response/add-menual-result", data);
    }
    clearTestResponse(filter):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/class-test/classtest-response/clear-response", {params:filter});
    }
}
