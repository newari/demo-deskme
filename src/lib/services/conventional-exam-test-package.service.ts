import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
// import { TestPackage } from "../models/testpackage.model";
import { ConventionalExamTestPackage } from "../models/conventionalExamTestPackage.model";

@Injectable() 
export class ConventionalExamTestPackageService{
    private testpackage: ConventionalExamTestPackage;
    constructor(private http:HttpClient){ }

    addTestpackage(testpackage:any) : Observable<ConventionalExamTestPackage>{
        return this.http.post<ConventionalExamTestPackage>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestpackage", testpackage);
    }

    getTestpackage() :Observable<ConventionalExamTestPackage[]>{
        return this.http.get<ConventionalExamTestPackage[]>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestpackage");
    }

    getOneTestpackage(testpackageId) :Observable<ConventionalExamTestPackage>{
        return this.http.get<ConventionalExamTestPackage>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestpackage/"+testpackageId);
    }

    updateTestpackage(testpackageId, testpackage:ConventionalExamTestPackage) : Observable<ConventionalExamTestPackage>{
        return this.http.put<ConventionalExamTestPackage>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestpackage/"+testpackageId, testpackage);
    }

    deleteTestpackage(testpackageId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestpackage/"+testpackageId);
    }

    addTestSeries(testPackageId, testSeriesId):Observable<any>{
        let body:any={testPackageId:testPackageId, testSeriesId:testSeriesId};
        
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestpackage/add-testseries", body);
    }
    removeTestSeries(testPackageId,testSeriesId):Observable<any>{
        let body:any={testPackageId:testPackageId, testSeriesId:testSeriesId};
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestpackage/remove-testseries", body);
    }
}
    
