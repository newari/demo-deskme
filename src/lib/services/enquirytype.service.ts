import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import {EnquiryType} from "../models/enquirytype.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class EnquiryTypeService{
    private enquirytype: EnquiryType;
    constructor(private http:HttpClient){ }

    addEnquiryType(enquiryTypeData:any) : Observable<EnquiryType>{
        return this.http.post<EnquiryType>(EdukitConfig.BASICS.API_URL+"/enquiry/enquirytype", enquiryTypeData);
    } 
 
     
    getEnquiryType(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/enquiry/enquirytype", opts);
    }

    getOneEnquiryType(enquiryTypeId) :Observable<EnquiryType>{
        return this.http.get<EnquiryType>(EdukitConfig.BASICS.API_URL+"/enquiry/enquirytype/"+enquiryTypeId);
    }
 

    updateEnquiryType(enquiryTypeId, enquiry:any) : Observable<EnquiryType>{
        return this.http.put<EnquiryType>(EdukitConfig.BASICS.API_URL+"/enquiry/enquirytype/"+enquiryTypeId, enquiry);
    }
 

    deleteEnquiryType(enquiryTypeId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/enquiry/enquirytype/"+enquiryTypeId);
    }
    
    // exportEnquiries(filter?:any, withHeaders?:boolean) :Observable<any>{
    //     let opts:any={params:filter};
    //     if(withHeaders){
    //         opts.observe= 'response'
    //     }
    //     return this.http.get(EdukitConfig.BASICS.API_URL+"/enquiry/enquiry/export-enquiries", opts);
    // }
}
    
