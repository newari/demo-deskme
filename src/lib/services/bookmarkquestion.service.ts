import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { BookmarkQuestion } from "../models/bookmarkquestion.model";

@Injectable() 
export class BookmarkQuestionService{
    private qBookmark: BookmarkQuestion;
    constructor(private http:HttpClient){ }
    addBookmarkQuestion(data): Observable<BookmarkQuestion> {
        return this.http.post<BookmarkQuestion>(EdukitConfig.BASICS.API_URL + "/testment/bookmarkquestion",data);
    } 
    getBookmarkQuestion(filter): Observable<BookmarkQuestion> {
        return this.http.get<BookmarkQuestion>(EdukitConfig.BASICS.API_URL + "/testment/bookmarkquestion",{params : filter});
    } 
    getReportBookmarkQuestion(data): Observable<BookmarkQuestion> {
        return this.http.get<BookmarkQuestion>(EdukitConfig.BASICS.API_URL + "/testment/bookmarkquestion/report",{params : data});
    } 
    deleteBookmarkQuestion(id): Observable<BookmarkQuestion> {
        return this.http.delete<BookmarkQuestion>(EdukitConfig.BASICS.API_URL + "/testment/bookmarkquestion/"+id);
    } 
}
    
