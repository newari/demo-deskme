import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Classroom} from "../models/classroom.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class ClassroomService{
    private classroom: Classroom;
    constructor(private http:HttpClient){ }

    addClassroom(classroom:any) : Observable<Classroom>{
        return this.http.post<Classroom>(EdukitConfig.BASICS.API_URL+"/admin/classroom", classroom);
    }

    getClassroom() :Observable<Classroom[]>{
        return this.http.get<Classroom[]>(EdukitConfig.BASICS.API_URL+"/admin/classroom");
    }

    getOneClassroom(classroomId) :Observable<Classroom>{
        return this.http.get<Classroom>(EdukitConfig.BASICS.API_URL+"/admin/classroom/"+classroomId);
    }

    updateClassroom(classroomId, classroom:Classroom) : Observable<Classroom>{
        return this.http.put<Classroom>(EdukitConfig.BASICS.API_URL+"/admin/classroom/"+classroomId, classroom);
    }

    deleteClassroom(classroomId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/admin/classroom/"+classroomId);
    }
    getUpcomingSchedule(classroomIds:string[], fltr?:any):Observable<any>{
        if(!fltr){
            fltr={};
        }
        fltr.classroomIds=classroomIds;
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/calendar/upcoming-schedule/classrooms", {params:fltr});
    }

    getCoordinatorClassrooms(coordinatorUserId:string):Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/classroom/coordinator-classrooms/"+coordinatorUserId);
    }

    getClassroomEventBatch(data:any):Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/calendar/classroom-events-batch", {params:data});
    }
}
    
