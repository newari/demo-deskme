import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";

import {Store} from "../models/store.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable()
export class StoreService{
    private store: Store;
    constructor(private http:HttpClient){ }

    addStore(store:any) : Observable<Store>{
        return this.http.post<Store>(EdukitConfig.BASICS.API_URL+"/cmn/store", store);
    }

    getStore(filter?:any) :Observable<Store[]>{
        return this.http.get<Store[]>(EdukitConfig.BASICS.API_URL+"/cmn/store", {params:filter});
    }

    getOneStore(storeId) :Observable<Store>{
        return this.http.get<Store>(EdukitConfig.BASICS.API_URL+"/cmn/store/"+storeId);
    }

    updateStore(storeId, store:any) : Observable<Store>{
        return this.http.put<Store>(EdukitConfig.BASICS.API_URL+"/cmn/store/"+storeId, store);
    }

    deleteStore(storeId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/cmn/store/"+storeId);
    }
}

