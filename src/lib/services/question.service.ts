import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { Question } from "../models/question.model";

@Injectable() 
export class QuestionService{
    private question: Question;
    constructor(private http:HttpClient){ }

    addQuestion(question:any) : Observable<Question>{        
        return this.http.post<Question>(EdukitConfig.BASICS.API_URL+"/qbank/question", question);
    }
    maxSequenceQuestion(qsetId):Observable<any>{        
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/qsetqs/?qset="+qsetId+"&sort=questionSeqId DESC&limit=1");
    }
    getQuestion(filter?: any, withHeaders?: boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<Question[]>(EdukitConfig.BASICS.API_URL + "/qbank/question", opts);
    }
    getPrintQuestions(filter?: any, withHeaders?: boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/qbank/question/print", opts);
    }
    getOneQuestion(questionId) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/question/"+questionId);
    }
    updateQuestion(questionId, question:Question) : Observable<Question>{
        return this.http.put<Question>(EdukitConfig.BASICS.API_URL+"/qbank/question/"+questionId, question);
    }
    deleteQuestion(questionId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/qbank/question/"+questionId);
    }
    uploadQuestionsDocx(data):Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/qbank/question/upload/docx",data);
    }
    readTableFormatQsDocx(data):Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/qbank/question/read/docx/tableformat",data);
    }
    getRandomQuestions(data):Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/qbank/question/maker/generate/random",data);
    }
    getQCode(data):Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/qbank/question/qcode",data);
    }
    addVariantQuestion(data,qsetId) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/qbank/question/variant/add/"+qsetId,data);
    }
    updateVariantQuestion(questionId,data) : Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/qbank/question/variant/edit/"+questionId,data);
    }
    getVariantQuestions(qsetId):Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/question/variant/"+qsetId);
    }
    getUserCreatedBy(filter?:any):Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/core/user/createdby",{params:filter});
    }
    saveQBFileQuestions(data):Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/qbank/question/save/qbfile",data);
    }
    getIMSDashboard(filter?: any):Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/question/ims/dashboard",{params: filter});
    }
}
    
