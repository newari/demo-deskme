    
import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import {Course} from "../models/course.model";
import { EdukitConfig } from "../../ezukit.config";
// import { Course } from "../ek-model/course.model";

@Injectable() 
export class CourseService{
    private course: Course;
    constructor(private http:HttpClient){ }

    addCourse(course:Course) : Observable<Course>{
        return this.http.post<Course>(EdukitConfig.BASICS.API_URL+"/webber/course", course);
    }

    getCourse(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/course", opts);
    }

    getOneCourse(courseId) :Observable<Course>{
        return this.http.get<Course>(EdukitConfig.BASICS.API_URL+"/webber/course/"+courseId);
    }

    updateCourse(courseId, course:Course) : Observable<Course>{
        return this.http.put<Course>(EdukitConfig.BASICS.API_URL+"/webber/course/"+courseId, course);
    }

    deleteCourse(courseId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/course/"+courseId);
    }
}
    