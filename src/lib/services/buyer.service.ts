import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class BuyerService{
    private buyer: any;
    constructor(private http:HttpClient){ }

   

    getBuyer(filter?:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/cmn/buyer", {params:filter});
    }

    getRecurringCustomer(filter?:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/cmn/recurring-customer", {params:filter});
    }

}
    
