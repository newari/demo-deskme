import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Testimonial} from "../models/testimonial.model";

@Injectable() 
export class TestimonialService{
    private testimonial: Testimonial;
    constructor(private http:HttpClient){ }

    addTestimonial(testimonial:Testimonial) : Observable<Testimonial>{
        return this.http.post<Testimonial>(EdukitConfig.BASICS.API_URL+"/webber/testimonial", testimonial);
    }

    getTestimonial(filter?: any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/testimonial", opts);
    }

    getOneTestimonial(testimonialId) :Observable<Testimonial>{
        return this.http.get<Testimonial>(EdukitConfig.BASICS.API_URL+"/webber/testimonial/"+testimonialId);
    }

    updateTestimonial(testimonialId, testimonial:Testimonial) : Observable<Testimonial>{
        return this.http.put<Testimonial>(EdukitConfig.BASICS.API_URL+"/webber/testimonial/"+testimonialId, testimonial);
    }

    deleteTestimonial(testimonialId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/testimonial/"+testimonialId);
    }
    getTestimonialCount():Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/webber/testimonial/count");
    }
}
    