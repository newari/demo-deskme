import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { Employee } from '../models/employee.model';
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class EmployeeService{
    private employee:Employee;
    constructor(private http:HttpClient){ }

    addEmployee(employee:any) : Observable<Employee>{
        return this.http.post<Employee>(EdukitConfig.BASICS.API_URL+"/hr/employee", employee);
    }

    getEmployee(filter?:any,withHeaders?:any) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/hr/employee", opts);
    }

    getEmployeeByAppAccess(filter?:any) :Observable<Employee[]>{
        return this.http.get<Employee[]>(EdukitConfig.BASICS.API_URL+"/hr/employee/user-by-app-access", {params:filter});
    }

    getOneEmployee(employeeId) :Observable<Employee>{
        return this.http.get<Employee>(EdukitConfig.BASICS.API_URL+"/hr/employee/"+employeeId);
    }

    updateEmployee(employeeId, employee:any) : Observable<Employee>{
        return this.http.put<Employee>(EdukitConfig.BASICS.API_URL+"/hr/employee/"+employeeId, employee);
    }

    deleteEmployee(employeeId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/hr/employee/"+employeeId);
    }
}
    
