import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import {PostCategory} from "../models/postcategory.model";
import { EdukitConfig } from "../../../src/ezukit.config";

@Injectable() 
export class PostCategoryService{
    private postCategory: PostCategory;
    constructor(private http:HttpClient){ }

    addPostCategory(postCategory:PostCategory) : Observable<PostCategory>{
        return this.http.post<PostCategory>(EdukitConfig.BASICS.API_URL+"/blog/blogpostcategory", postCategory);
    }

    getPostCategory(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/blog/blogpostcategory", opts);
    }

    getOnePostCategory(postCategoryId) :Observable<PostCategory>{
        return this.http.get<PostCategory>(EdukitConfig.BASICS.API_URL+"/blog/blogpostcategory/"+postCategoryId);
    }

    updatePostCategory(postCategoryId, postCategory:PostCategory) : Observable<PostCategory>{
        return this.http.put<PostCategory>(EdukitConfig.BASICS.API_URL+"/blog/blogpostcategory/"+postCategoryId, postCategory);
    }

    deletePostCategory(postCategoryId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/blog/blogpostcategory/"+postCategoryId);
    }
}

    