import { Injectable } from "@angular/core";

import "rxjs/Rx";
import { Observable } from "rxjs";

import { EdukitConfig } from "../../ezukit.config";

import { HttpClient } from '@angular/common/http';
import {Lesson} from "../models/lesson.model";

@Injectable()
export class LessonService{
    private lesson: Lesson;
    constructor(private http:HttpClient){ }

    addLesson(Lesson:Lesson) : Observable<Lesson>{
        return this.http.post<Lesson>(EdukitConfig.BASICS.API_URL+"/course-creator/lesson", Lesson);
    }

    getLesson(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe='response';
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/course-creator/lessons", opts);
    }

    getOneLesson(LessonId) :Observable<Lesson>{
        return this.http.get<Lesson>(EdukitConfig.BASICS.API_URL+"/course-creator/lesson/"+LessonId) ;
    }

    updateLesson(LessonId, data?:any) : Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/course-creator/lesson/"+LessonId, data);
    }

    deleteLesson(LessonId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/course-creator/lesson/"+LessonId);
    }

    // Lesson Content APIS
    getLessonContent(lessonId): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/course-creator/lessoncontent", {params:{lesson:lessonId}});
    }
    addLessonContent(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/course-creator/lessoncontent", data);
    }
    updateLessonContent(lesssonID,data?:any):Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/course-creator/lessoncontent/"+lesssonID,data );
    }
    getUserLessons(subjectId?:any,curriculumId?:any, filter?:any):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/course-creator/lessons/"+curriculumId+"/"+subjectId, {params:filter});
    }

    getSingleLessonContent(lessonId,productId?:any, filter?:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+ "/course-creator/lesson-content/"+lessonId+"/"+productId, {params:filter});
    }
    addOnLessonUsers(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/course-creator/userlesson", data);
    }

    updateLessonUsers(data?:any):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/course-creater/userlesson/complete-lesson", data);
    }

    getLessonUsers(lessonId, filter?:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+ "/course-creator/lesson/"+lessonId+"/users", {params:filter});
    }
    getSignleLessonOfUser(lessonId,productId): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+ "/course-creator/userlesson/"+productId+"/lesson/"+lessonId);
    }
    updateUserLesson(lessonId,productId, data?:any): Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+ "/course-creator/userlesson/product/"+productId+"/lesson/"+lessonId, data);
    }

    updateLessonViewsStats(lessonId, data): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+`/course-creator/userlesson/${lessonId}/update-stats`, data);
    }
    updateUserLessonViewData(accessKey, data): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/course-creator/userlesson/update-lesson/"+accessKey, data);
    }
    getDaywiseLesson(curriId, filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe='response';
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/course-creator/day-wise-lesson/"+curriId, opts);
    }
    getDaywiseLiveLesson(curriId, filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe='response';
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/course-creator/day-wise-live-lessons/"+curriId, opts);
    }

}

