import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import { Curriculum} from "../models/curriculum.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable()
export class CurriculumService{
    private curriculum: Curriculum;
    constructor(private http:HttpClient){ }

    addCurriculum(curriculum:any) : Observable<Curriculum>{
        return this.http.post<Curriculum>(EdukitConfig.BASICS.API_URL+"/admin/curriculum", curriculum);
    }

    getCurriculum() :Observable<Curriculum[]>{
        return this.http.get<Curriculum[]>(EdukitConfig.BASICS.API_URL+"/admin/curriculum");
    }

    getOneCurriculum(curriculumId) :Observable<Curriculum>{
        return this.http.get<Curriculum>(EdukitConfig.BASICS.API_URL+"/admin/curriculum/"+curriculumId);
    }

    updateCurriculum(curriculumId, curriculum:any) : Observable<Curriculum>{
        return this.http.put<Curriculum>(EdukitConfig.BASICS.API_URL+"/admin/curriculum/"+curriculumId, curriculum);
    }

    deleteCurriculum(curriculumId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/admin/curriculum/"+curriculumId);
    }

    addCurriculumSubject(curriculumId, subjectData:any) : Observable<Curriculum>{
        subjectData.curriculumId=curriculumId;
        return this.http.post<Curriculum>(EdukitConfig.BASICS.API_URL+"/admin/curriculum/addsubject", subjectData);
    }

    removeCurriculumSubject(curriculumId, subjectId) : Observable<Curriculum>{
        return this.http.post<Curriculum>(EdukitConfig.BASICS.API_URL+"/admin/curriculum/remove-subject", {curriculumId:curriculumId, subjectId:subjectId});
    }

    getCurriculumSubject(curriculumId) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/admin/curriculum/"+curriculumId+"/subject");
    }

    


}

