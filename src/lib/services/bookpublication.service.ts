import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { Observable } from "rxjs";


import {Bookpublication} from "../models/bookpublication.model";
import { HttpClient } from "@angular/common/http";
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class BookpublicationService{
    private bookpublication: Bookpublication;
    constructor(private http:HttpClient){ }

    addBookpublication(bookpublication:Bookpublication) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/book-store/bookpublication", bookpublication);
    }

    getBookpublication(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe="response";
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/book-store/bookpublication", opts)
    }

    getOneBookpublication(bookpublicationId,filter?:any) :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/book-store/bookpublication/"+bookpublicationId,{params:filter});
    }

    updateBookpublication(bookpublicationId, bookpublication:Bookpublication) : Observable<any>{
      
        return this.http.put(EdukitConfig.BASICS.API_URL+"/book-store/bookpublication/"+bookpublicationId, bookpublication)
    }

    deleteBookpublication(bookpublicationId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/book-store/bookpublication/"+bookpublicationId);
    }
}
    