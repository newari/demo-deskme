import { Injectable } from '@angular/core';
import "rxjs";
import { Observable } from "rxjs/Observable";
import { Observer } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable()
export class EzukitAppService {
    data:Observable<any>;
    dataObserver:Observer<any>;
    
    constructor(private http:HttpClient){
        let self=this;
        this.data = new Observable(observer => self.dataObserver = observer);
    }

    setActiveApp(AppConfig){
        if(this.dataObserver){
            this.dataObserver.next(AppConfig);
        }
        
    }

    getClientApp(appId:string) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/core/client/apps/"+appId);
    }
}