import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Videoseries} from "../models/videoseries.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class VideoseriesService{
   // private videoseries: Videoseries;
    constructor(private http:HttpClient){ }

    addVideoseries(videoseries:any) : Observable<Videoseries>{
        return this.http.post<Videoseries>(EdukitConfig.BASICS.API_URL+"/videovibe/videoseries", videoseries);
    }

    getVideoseries() :Observable<Videoseries[]>{
        return this.http.get<Videoseries[]>(EdukitConfig.BASICS.API_URL+"/videovibe/videoseries");
    }

    getOneVideoseries(videoseriesId) :Observable<Videoseries>{
        return this.http.get<Videoseries>(EdukitConfig.BASICS.API_URL+"/videovibe/videoseries/"+videoseriesId);
    }

    updateVideoseries(videoseriesId, videoseries:Videoseries) : Observable<Videoseries>{
        return this.http.put<Videoseries>(EdukitConfig.BASICS.API_URL+"/videovibe/videoseries/"+videoseriesId, videoseries);
    }

    deleteVideoseries(videoseriesId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/videovibe/videoseries/"+videoseriesId);
    }
    
    addVideoseriesVideo(videoData:any) : Observable<Videoseries>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/videovibe/videoseries/add-video", videoData);
    }
    removeVideoseriesVideo(videoData:any) : Observable<Videoseries>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/videovibe/videoseries/remove-video", videoData);
    }


}
    
