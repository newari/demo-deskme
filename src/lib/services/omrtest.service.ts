import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { Omrtest } from "../models/omrtest.model";

@Injectable() 
export class OmrtestService{
   // private Testpaper: Videoseries;
    constructor(private http:HttpClient){ }

    addOmrtest(omrtest:Omrtest) : Observable<Omrtest>{
        return this.http.post<Omrtest>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest", omrtest);
    }

    getOmrtest() :Observable<Omrtest[]>{
        return this.http.get<Omrtest[]>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest");
    }

    getTotalOmrtest() :Observable<Omrtest[]>{
        return this.http.get<Omrtest[]>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/totalomrtest");
    }

    getOneOmrtest(OmrtestId) :Observable<Omrtest>{
        return this.http.get<Omrtest>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/"+OmrtestId);
    }

    updateOmrtest(OmrtestId, omrtest:Omrtest) : Observable<Omrtest>{
        return this.http.put<Omrtest>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/"+OmrtestId, omrtest);
    }

    deleteOmrtest(OmrtestId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/"+OmrtestId);
    }
    addOmrtestQs(omrTestQsData):Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/addomrtestqs/", omrTestQsData);
    }
    addOmrtestStdQs(omrTestStdQsData):Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/addomrteststdqs/", omrTestStdQsData);
    }
    getOmrtestQs(omrtestId) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/getomrtestqs/"+omrtestId);
    }
    getOmrtestQsGroupBySection(omrtestId) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/getomrtestqs-by-section/"+omrtestId);
    }

    getOmrTestResult(filter?:any, withHeaders?:boolean) :Observable<any>{

        let opts:any={params:filter};
        console.log(opts);
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/gettestresult", opts);
    }
    getStdResult(omrtestId:string, rollNo:string) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/std-result/"+omrtestId, {params:{rollNo:rollNo}});
    }
    getStdReport(rollNo:string) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/std-report/"+rollNo);
    }
    getStdSectionWiseResult(omrtestId:string, rollNo:string) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/std-section-wise-result/"+omrtestId, {params:{rollNo:rollNo}});
    }
    updateStdResult(updatedStdQsData):Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/updatestdresult/", updatedStdQsData);
    }
    getOmrtestStdResponse(omrtestId) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/getomrteststdresponse/"+omrtestId);
    }
    reCheckStdResult(omrtestId, rollNo) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrtest/recheckstdresult?id="+omrtestId+"&rollNo="+rollNo);
    }

}
    
