import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import {Leave} from "../models/leave.model";
import { EdukitConfig } from "../../../src/ezukit.config";

@Injectable() 
export class LeaveService{
    private leave: Leave;
    constructor(private http:HttpClient){ }

    addLeave(leave:Leave) : Observable<Leave>{
        return this.http.post<Leave>(EdukitConfig.BASICS.API_URL+"/hr/leave", leave);
    }

    getLeave() :Observable<Leave[]>{
        return this.http.get<Leave[]>(EdukitConfig.BASICS.API_URL+"/hr/leave");
    }

    getOneLeave(leaveId) :Observable<Leave>{
        return this.http.get<Leave>(EdukitConfig.BASICS.API_URL+"/hr/leave/"+leaveId);
    }

    updateLeave(leaveId, leave:Leave) : Observable<Leave>{
        return this.http.put<Leave>(EdukitConfig.BASICS.API_URL+"/hr/leave/"+leaveId, leave);
    }

    deleteLeave(leaveId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/hr/leave/"+leaveId);
    }
}
    