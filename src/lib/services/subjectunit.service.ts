import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";


import {Subjectunit} from "../models/subjectunit.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable()
export class SubjectunitService{
    private subjectunit: Subjectunit;
    constructor(private http:HttpClient){ }

    addSubjectunit(subjectunit:any) : Observable<Subjectunit>{
        return this.http.post<Subjectunit>(EdukitConfig.BASICS.API_URL+"/admin/subjectunit", subjectunit);
    }

    getSubjectunit(filter?:any) :Observable<Subjectunit[]>{
        return this.http.get<Subjectunit[]>(EdukitConfig.BASICS.API_URL+"/admin/subjectunit",{params : filter});
    }

    getOneSubjectunit(subjectunitId) :Observable<Subjectunit>{
        return this.http.get<Subjectunit>(EdukitConfig.BASICS.API_URL+"/admin/subjectunit/"+subjectunitId)
    }

    updateSubjectunit(subjectunitId, subjectunit:Subjectunit) : Observable<Subjectunit>{
        return this.http.put<Subjectunit>(EdukitConfig.BASICS.API_URL+"/admin/subjectunit/"+subjectunitId, subjectunit);
    }

    deleteSubjectunit(subjectunitId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/admin/subjectunit/"+subjectunitId);
    }

}

