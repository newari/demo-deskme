import { Injectable } from "@angular/core";

import "rxjs/Rx";
import { Observable } from "rxjs";


import {Offer} from "../models/offer.model";
import { EdukitConfig } from "../../ezukit.config";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class OfferService{
    private offer: Offer;
    constructor(private http:HttpClient){ }

    addOffer(offer:Offer) : Observable<Offer>{
        return this.http.post<Offer>(EdukitConfig.BASICS.API_URL+"/webber/offer", offer);
    }

    getOffer(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders) opts.observe="response";
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/offer",opts);
    }

    getOneOffer(offerId) :Observable<Offer>{
        return this.http.get<Offer>(EdukitConfig.BASICS.API_URL+"/webber/offer/"+offerId);
    }

    updateOffer(offerId, offer:Offer) : Observable<Offer>{

        return this.http.put<Offer>(EdukitConfig.BASICS.API_URL+"/webber/offer/"+offerId, offer);
    }

    deleteOffer(offerId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/offer/"+offerId);
    }
}
