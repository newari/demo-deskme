import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Coption} from "../models/coption.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class CoptionService{
    private coption: Coption;
    constructor(private http:HttpClient){ }

    addCoption(coption:any) : Observable<Coption>{
        return this.http.post<Coption>(EdukitConfig.BASICS.API_URL+"/admin/coption", coption);
    }
    getCoption(filter?:any) :Observable<Coption[]>{
        if(!filter.sort) filter.sort="valueAlias ASC";
        return this.http.get<Coption[]>(EdukitConfig.BASICS.API_URL+"/admin/coption", {params:filter});
    }
    getCoptionCategories() :Observable<Coption[]>{
        let opts:any={option:'Option'};
       
        return this.http.get<Coption[]>(EdukitConfig.BASICS.API_URL+"/admin/coption", {params:opts});
    }

    getOneCoption(CoptionId) :Observable<Coption>{
        return this.http.get<Coption>(EdukitConfig.BASICS.API_URL+"/admin/coption/"+CoptionId);
    }

    getCoptionByName(CoptionName:string) :Observable<Coption[]>{
        return this.http.get<Coption[]>(EdukitConfig.BASICS.API_URL+"/admin/coption/by-name/"+CoptionName);
    }

    updateCoption(CoptionId, coption:any) : Observable<Coption>{
        return this.http.put<Coption>(EdukitConfig.BASICS.API_URL+"/admin/coption/"+CoptionId, coption);
    }

    deleteCoption(CoptionId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/admin/coption/"+CoptionId);
    }

    getCoptionName() :Observable<Coption[]>{
        return this.http.get<Coption[]>(EdukitConfig.BASICS.API_URL+"/admin/coption/name");
    }

    getCoptionValue(CoptionName) :Observable<Coption[]>{
        return this.http.get<Coption[]>(EdukitConfig.BASICS.API_URL+"/admin/coption/value/"+CoptionName);
    }

    getCoptionChild(CoptionId) :Observable<Coption[]>{
        return this.http.get<Coption[]>(EdukitConfig.BASICS.API_URL+"/admin/coption/child/"+CoptionId);
    }
    getCoptionProductCategory(filter?:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/admin/coption/category", {params:filter});
    }
    addWebPage(coptionId): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL +"/admin/coption/add-page/"+coptionId, {});
    }
}
    
