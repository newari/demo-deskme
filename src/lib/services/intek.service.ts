import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { ClientConfig } from "../models/clientconfig.model";

@Injectable() 
export class IntekService{
    constructor(private http:HttpClient){ }

    getClientConfig(filter?:any) :Observable<ClientConfig>{
        return this.http.get<ClientConfig>(EdukitConfig.BASICS.API_URL+"/client", {params:filter});
    }
    
    getApps(filter?:any) :Observable<any[]>{
        
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/intek/dataapi/getapps", {params:filter});
    }
    getOneApp(appId:string) :Observable<any>{
       
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/intek/dataapi/getoneapp/"+appId);
    }

    getAppProfile(appProfileId:string) :Observable<any>{
       
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/intek/dataapi/getappprofile/"+appProfileId);
    }
    addCartItem(data?:any) :Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/intek/dataapi/addcartitem", data);
    }

    deleteCartItem(id:string) :Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/intek/dataapi/deletecartitem/"+id);
    }
    
    getCartItems(clientId:string) :Observable<any[]>{
        
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/intek/dataapi/getcartitems", {params:{client:clientId}});
    }
    placeOrder(data:any) :Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/intek/dataapi/placeorder", data);
    }
    getAds(filter?:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/intek/dataapi/getslides", {params:filter});
    }


}
    
