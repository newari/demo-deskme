import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {CommonFeedback} from "../models/common-feedback.model";

@Injectable() 
export class CommonFeedbackService{
    private feedback: CommonFeedback;
    constructor(private http:HttpClient){ }

    addFeedback(feedback:CommonFeedback) : Observable<CommonFeedback>{
        return this.http.post<CommonFeedback>(EdukitConfig.BASICS.API_URL+"/cmn/feedback", feedback);
    }

    getFeedback(filter?:any) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/cmn/feedback",{params:filter});
    }

    getOneFeedback(feedbackId,filter?:any) :Observable<CommonFeedback>{
        return this.http.get<CommonFeedback>(EdukitConfig.BASICS.API_URL+"/cmn/feedback/"+feedbackId,{params:filter});
    }

    updateFeedback(feedbackId, feedback:CommonFeedback) : Observable<CommonFeedback>{
        return this.http.put<CommonFeedback>(EdukitConfig.BASICS.API_URL+"/cmn/feedback/"+feedbackId, feedback);
    }

    deleteFeedback(feedbackId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/cmn/feedback/"+feedbackId);
    }

    addFeedbackQs(feedbackId,fbqsId){
        let body: any = { feedbackId: feedbackId, fbqsId: fbqsId };
        return this.http.put<CommonFeedback>(EdukitConfig.BASICS.API_URL + "/cmn/feedback/add-qs/" , body);
    }
    removeFeedBackQs(feedbackId, fbqsId){
        let body: any = { feedbackId: feedbackId, fbqsId: fbqsId };
        return this.http.put<CommonFeedback>(EdukitConfig.BASICS.API_URL + "/cmn/feedback/remove-qs/", body);

    }

    addFeedbackResponse(data): Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/cmn/feedbackresponse", data);
    }
    getFeedbackResponse(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter}
        if(withHeaders){
            opts.observe="response";
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/cmn/feedbackresponse", opts);
    }

    addFeedbackReply(responseId, replyData?:any) :Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/cmn/feedbackresponse/"+responseId, replyData);
    }

    getOneFeedbackResponse(responseId,filter): Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/cmn/feedbackresponse/"+responseId,{params:filter});
    }
}
    