import { Injectable } from "@angular/core";

import "rxjs/Rx";
import { Observable } from "rxjs";


import {Rankpredictor} from "../models/rankpredictor.model";
import { HttpClient } from "@angular/common/http";
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class RankpredictorService{
    private rankpredictor: Rankpredictor;
    constructor(private http:HttpClient){ }

    addRankpredictor(rankpredictor) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/webber/rankpredictor", rankpredictor)
    }

    getRankpredictor(filter?:any) :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/webber/rankpredictor", {params:filter});
    }

    getOneRankpredictor(rankpredictorId) :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/webber/rankpredictor/"+rankpredictorId)
    }

    updateRankpredictor(rankpredictorId, rankpredictor:any) : Observable<any>{

        return this.http.put(EdukitConfig.BASICS.API_URL+"/webber/rankpredictor/"+rankpredictorId, rankpredictor);
    }

    deleteRankpredictor(rankpredictorId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/rankpredictor/"+rankpredictorId);
    }

    getRankpredictorResponse(filter, withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe="response"
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/webber/rankpredictorresponse",opts);
    }

    exportResponses(rankPredictorId,filter): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/webber/rankpredictor/"+rankPredictorId+"/export-response", {params:filter});
    }
    predictRank(data:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/public/data/generate-result",data);
    }
}
