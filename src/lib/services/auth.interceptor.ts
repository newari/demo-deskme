// src/app/auth/token.interceptor.ts
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { AuthService } from './auth.service';
import { Injector } from '@angular/core';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private injector: Injector) {}

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let authService=this.injector.get(AuthService);
        request=request.clone({setHeaders: {token: authService.getAuthToken()}, withCredentials: true})
        return next.handle(request).do(
            (event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    // do stuff with response if you want
                }
            }, 
            (err: any) => {
                if (err instanceof HttpErrorResponse) {
                    
                    if (err.status === 401) {
                        
                        authService.logout();
                        // redirect to the login route
                        // or show a modal
                    }


                }
            }
        ).catch((err: HttpErrorResponse) => {
            console.log(err);
            
            // let parsedError = Object.assign({}, err, { error: err.error });
            return Observable.throw(err.error);
        });;
    }
}