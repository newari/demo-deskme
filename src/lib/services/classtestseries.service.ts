import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import {Classtestseries} from "../models/classtestseries.model";
import { EdukitConfig } from "../../ezukit.config";
import { HttpClient } from "@angular/common/http";

@Injectable() 
export class ClasstestseriesService{
    private classtestseries: Classtestseries;
    constructor(private http:HttpClient){ }

    addClasstestseries(classtestseries:Classtestseries) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/class-test/classtestseries", classtestseries);
    }

    getClasstestseries() :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/class-test/classtestseries");
    }

    getOneClasstestseries(classtestseriesId) :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/class-test/classtestseries/"+classtestseriesId);
    }

    updateClasstestseries(classtestseriesId, classtestseries:Classtestseries) : Observable<any>{

        return this.http.put(EdukitConfig.BASICS.API_URL+"/class-test/classtestseries/"+classtestseriesId, classtestseries);
    }

    deleteClasstestseries(classtestseriesId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/class-test/classtestseries/"+classtestseriesId);
    }
    getClassTestSeriesCount(): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/class-test/classtestseries/count");
    }
}
    