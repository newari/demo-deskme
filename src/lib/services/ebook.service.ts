import { Injectable } from "@angular/core";

import "rxjs/Rx";
import { Observable } from "rxjs";

import { EdukitConfig } from '../../ezukit.config';

import {Ebook} from "../models/ebook.model";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class EbookService{
    private ebook: Ebook;
    constructor(private http:HttpClient){ }

    addEbook(ebook:Ebook) : Observable<Ebook>{
        return this.http.post<Ebook>(EdukitConfig.BASICS.API_URL+"/admin/ebook", ebook);
    }

    getEbook(filter?:any,withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders) opts.observe='response';
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/admin/ebook",opts);
    }

    getOneEbook(ebookId) :Observable<Ebook>{
        return this.http.get<Ebook>(EdukitConfig.BASICS.API_URL+"/admin/ebook/"+ebookId);
    }

    updateEbook(ebookId, ebook:Ebook) : Observable<Ebook>{
        return this.http.put<Ebook>(EdukitConfig.BASICS.API_URL+"/admin/ebook/"+ebookId, ebook);
    }

    deleteEbook(ebookId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/admin/ebook/"+ebookId);
    }
    getEbookStats(): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/admin/ebook/dashboard/dash-cards");
    }
}
