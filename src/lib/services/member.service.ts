import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";
import { HttpClient, HttpParams } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';
export class Member{

}

@Injectable()
export class MemberService{
    private member: Member;
    constructor(private http:HttpClient){ }

    addMember(member:Member) : Observable<Member>{
        return this.http.post<Member>(EdukitConfig.BASICS.API_URL+"/staff/member", member);
    }

    getMember(filter?:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/staff/member", {params:filter});
    }

    getOneMember(memberId) :Observable<Member>{
        return this.http.get<Member>(EdukitConfig.BASICS.API_URL+"/staff/member/"+memberId);
    }

    updateMember(memberId, member:Member) : Observable<Member>{
        return this.http.put<Member>(EdukitConfig.BASICS.API_URL+"/staff/member/"+memberId, member);
    }

    deleteMember(memberId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/staff/member/"+memberId);
    }
}

