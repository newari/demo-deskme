import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Notification} from "../models/notification.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class NotificationService{
    private notification: Notification;
    constructor(private http:HttpClient){ }

    addNotification(notification:any) : Observable<Notification>{
        return this.http.post<Notification>(EdukitConfig.BASICS.API_URL+"/cmn/notifier", notification);
    }

    getNotification(filter?:any) :Observable<Notification[]>{
        return this.http.get<Notification[]>(EdukitConfig.BASICS.API_URL+"/cmn/notifier", {params:filter});
    }

    getOneNotification(notificationId) :Observable<Notification>{
        return this.http.get<Notification>(EdukitConfig.BASICS.API_URL+"/cmn/notifier/"+notificationId);
    }

    updateNotification(notificationId, notification:any) : Observable<Notification>{
        return this.http.put<Notification>(EdukitConfig.BASICS.API_URL+"/cmn/notifier/"+notificationId, notification);
    }

    deleteNotification(notificationId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/cmn/notifier/"+notificationId);
    }
}
    
