import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {NotificationTemplate} from "../models/notification-template.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class NotificationTemplateService{
    private notificationTemplate: NotificationTemplate;
    constructor(private http:HttpClient){ }

    addNotificationTemplate(notificationTemplate:any) : Observable<NotificationTemplate>{
        return this.http.post<NotificationTemplate>(EdukitConfig.BASICS.API_URL+"/notification/notificationtemplate", notificationTemplate);
    }

    getNotificationTemplate(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe="response"
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/notification/notificationtemplate", opts);
    }

    getOneNotificationTemplate(notificationTemplateId) :Observable<NotificationTemplate>{
        return this.http.get<NotificationTemplate>(EdukitConfig.BASICS.API_URL+"/notification/notificationtemplate/"+notificationTemplateId);
    }

    updateNotificationTemplate(notificationTemplateId, notificationTemplate:any) : Observable<NotificationTemplate>{
        return this.http.put<NotificationTemplate>(EdukitConfig.BASICS.API_URL+"/notification/notificationtemplate/"+notificationTemplateId, notificationTemplate);
    }

    deleteNotificationTemplate(notificationTemplateId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/notification/notificationtemplate/"+notificationTemplateId);
    }
}
    
