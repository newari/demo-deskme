import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import {Designation} from "../models/designation.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class DesignationService{
    private designation: Designation;
    constructor(private http:HttpClient){ }

    addDesignation(designation:any) : Observable<Designation>{
        return this.http.post<Designation>(EdukitConfig.BASICS.API_URL+"/hr/designation", designation);
    }

    getDesignation(filter?:any) :Observable<Designation[]>{
        return this.http.get<Designation[]>(EdukitConfig.BASICS.API_URL+"/hr/designation",{params : filter});
    }

    getOneDesignation(designationId) :Observable<Designation>{
        return this.http.get<Designation>(EdukitConfig.BASICS.API_URL+"/hr/designation/"+designationId);
    }

    updateDesignation(designationId, designation:any) : Observable<Designation>{
        return this.http.put<Designation>(EdukitConfig.BASICS.API_URL+"/hr/designation/"+designationId, designation);
    }

    deleteDesignation(designationId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/hr/designation/"+designationId);
    }
}
    
