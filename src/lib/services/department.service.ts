import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { Department } from "../models/department.model";
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class DepartmentService{
    private department: Department;
    constructor(private http:HttpClient){ }

    addDepartment(department:any) : Observable<Department>{
        return this.http.post<Department>(EdukitConfig.BASICS.API_URL+"/org/department", department);
    }

    getDepartment() :Observable<Department[]>{
        return this.http.get<Department[]>(EdukitConfig.BASICS.API_URL+"/org/department");
    }

    getOneDepartment(departmentId) :Observable<Department>{
        return this.http.get<Department>(EdukitConfig.BASICS.API_URL+"/org/department/"+departmentId);
    }

    updateDepartment(departmentId, department:any) : Observable<Department>{
        return this.http.put<Department>(EdukitConfig.BASICS.API_URL+"/org/department/"+departmentId, department);
    }

    deleteDepartment(departmentId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/org/department/"+departmentId);
    }
}
    
