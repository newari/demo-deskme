import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';
import { Campaign } from '../models/campaign.model';

@Injectable()
export class CampaignService {
    private campaign: Campaign;
    constructor(private http: HttpClient) { }

    addCampaign(campaign: any): Observable<Campaign> {
        return this.http.post<Campaign>(EdukitConfig.BASICS.API_URL + "/notification/campaign", campaign);
    }

    getCampaign(filter?: any): Observable<Campaign[]> {
        return this.http.get<Campaign[]>(EdukitConfig.BASICS.API_URL + "/notification/campaign?sort=createdAt DESC", { params: filter });
    }

    getOneCampaign(campaignId): Observable<Campaign> {
        return this.http.get<Campaign>(EdukitConfig.BASICS.API_URL + "/notification/campaign/" + campaignId);
    }


    updateCampaign(campaignId, campaign: any): Observable<Campaign> {
        return this.http.put<Campaign>(EdukitConfig.BASICS.API_URL + "/notification/campaign/" + campaignId, campaign);
    }

    deleteCampaign(campaignId): Observable<any> {
        return this.http.delete(EdukitConfig.BASICS.API_URL + "/notification/campaign/" + campaignId);
    }
    sendNoticeBoardCampaign(data: any): Observable<Campaign> {
        return this.http.post<Campaign>(EdukitConfig.BASICS.API_URL + "/notification/noticeboard-campaign/send", data);
    }
    sendAndroidPushCampaign(data: any): Observable<Campaign> {
        return this.http.post<Campaign>(EdukitConfig.BASICS.API_URL + "/notification/androidpush-campaign/send", data);
    }
    testAndroidPushNotification(data: any): Observable<Campaign> {
        return this.http.post<Campaign>(EdukitConfig.BASICS.API_URL + "/notification/androidpush-campaign/test", data);
    }
}

