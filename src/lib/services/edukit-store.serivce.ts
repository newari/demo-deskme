import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../../src/ezukit.config";

@Injectable() 
export class EdukitStoreService{
    constructor(private http:HttpClient){ }

    placeOrder(order:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/edukit-store/esorder/place-order", order);
    }

    getESOrders(filter?:any, withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe='response';
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/edukit-store/esorder", opts);
    }
    
    getESSales(filter?:any, withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe='response';
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/edukit-store/estransaction", opts
        );
    }

    makePayment(order:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/edukit-store/esorder/"+order+"/make-payment", {});
    }
}
    