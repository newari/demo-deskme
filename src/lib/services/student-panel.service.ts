import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class StudentPanelService{
    constructor(private http:HttpClient){ }
    getDashboardSlider() :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-panel/config/getDashboardSlider");
    }
    getDirectDashboardSlider(code?:any) :Observable<any>{
        
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/slider/"+code);
    }
    setDashboardSlider(opt) :Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/student-panel/config/setDashboardSlider", opt);
    }
    getDashboardAdGroup() :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-panel/config/getDashboardAdGroup");
    }
    setDashboardAdGroup(opt) :Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/student-panel/config/setDashboardAdGroup", opt);
    }
    getESEAdmitCardDetails(userId, form): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/form/formresponse/ese-admit-card", { params: { userId: userId, form: form } });
    }
    uploadESEAdmitCard(data): Observable<any> {
        return this.http.post(EdukitConfig.BASICS.API_URL + "/form/formresponse", data);
    }
    sendSignupOtp(data): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL + "/public/user/signup-with-otp",data);
    }
    registerWithOtp(data): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL + "/public/user/register-student-with-otp",data);
    }
    varifyOtp(data): Observable<any> {
        return this.http.post(EdukitConfig.BASICS.API_URL + "/public/user/check-signup-otp", data);
    } 
    sendSigninOtp(data): Observable<any> {
        return this.http.post(EdukitConfig.BASICS.API_URL + "/public/send-otp", data);
    }
    varifySigninOtp(data?:any): Observable<any> {
        return this.http.post(EdukitConfig.BASICS.API_URL + "/public/varify", data);
    } 
    getDashCards():Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-panel/config/getStudentDashCards");
    }
    getRecentLessons(filter?:any):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/course-creator/userlesson?sort=createdAt DESC&limit=4", {params:filter});
    }

    getMyRecommandedProdcuts(filter?:any):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-manager/product/get-recommaneded-product", {params:filter});

    }
    getStudentBathces(filter?:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-manager/student/batches/ernolled", {params:filter})
    }

    getMyEbooks(filter?:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-manager/product/my-ebooks",{params:filter});
    }

    sendOtp(filter?:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-manager/product/my-ebooks",{params:filter});
    }
}