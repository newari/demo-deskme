import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
@Injectable() 
export class PaymentService{
    private payment: any;
    constructor(private http:HttpClient){ }

   

    addPayment(data) :Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/cmn/order/payment", data);
    }
    addCustomPayment(data) :Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/cmn/order/custom-payment", data);
    }
    updatePayment(paymentId,data?:any) :Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/sales/payment/"+paymentId, data);
    }

    
    getPayment(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/sales/payment", opts);
    }

    getPaymentCollectionByMode(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/sales/payment/collection-by-mode", opts);
    }

    approvePayment(paymentId) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/cmn/order/approve-payment", {paymentId:paymentId});
    }
    exportOrders(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/sales/payment/export-payments", opts);
    }
    exportPaymentsWithoutTax(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/sales/payment/export-payments-without-tax", opts);
    }
    exportPaymentsWithTax(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/sales/payment/export-payments-with-tax", opts);
    }

    getOrderPaymentNodes(filter?:any) :Observable<any>{
        let opts:any={params:filter};
        
        return this.http.get(EdukitConfig.BASICS.API_URL+"/cmn/order/payment-nodes", opts);
    }
    getPaymentNodes(filter?:any) :Observable<any>{
        let opts:any={params:filter};
        
        return this.http.get(EdukitConfig.BASICS.API_URL+"/sales/payment/nodes", opts);
    }
    cancelPayment(paymentId, reason?:any):Observable<any>{
         return this.http.get(EdukitConfig.BASICS.API_URL+"/sales/payment/"+paymentId+"/cancel", {params:{reason:reason}});
    }
    transferPayment(paymentId, data?:any ){
        return this.http.get(EdukitConfig.BASICS.API_URL + "/sales/payment/" + paymentId + "/transfer", { params:data });

    }
    
    addPaymentNode(data?:any):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL +"/sales/payment-nodes/add-due-payment-node",data);
    } 
    refundPayment(paymentId, data?: any) {
        return this.http.post(EdukitConfig.BASICS.API_URL + "/sales/payment/" + paymentId + "/refund", data);

    }
    addDiscount(orderId:any,discountDetail?:any):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL +"/cmn/order/"+orderId+"/add-discount",discountDetail);
        
    }

    exportPaymentWithExtraOption(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/sales/payment/export-payments-with-extra-option", opts);
    }
    exportPaymentWithTaxWithExtraOption(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/sales/payment/export-payments-with-text-and-extra-option", opts);
    }
    exportPaymentWithoutTaxWithExtraOption(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/sales/payment/export-payments-without-text-and-extra-option", opts);
    }
    exportRefundedPayments(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/sales/payment/export-refunded-payments", opts);
    }
}

