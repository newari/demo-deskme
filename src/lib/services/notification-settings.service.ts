import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';
import { NotificationSettings } from '../models/notificationSettings.model';

@Injectable()
export class NotificationSettingsService {
    private notificationSettings: NotificationSettings;
    constructor(private http: HttpClient) { }

    addNotificationSettings(notificationSettings: any): Observable<NotificationSettings> {
        return this.http.post<NotificationSettings>(EdukitConfig.BASICS.API_URL + "/notification/notificationSettings", notificationSettings);
    }

    getnotificationSettings(filter?: any): Observable<NotificationSettings[]> {
        return this.http.get<NotificationSettings[]>(EdukitConfig.BASICS.API_URL + "/notification/notificationSettings", { params: filter });
    }

    getOneNotificationSettings(notificationSettingsId): Observable<NotificationSettings> {
        return this.http.get<NotificationSettings>(EdukitConfig.BASICS.API_URL + "/notification/notificationSettings/" + notificationSettingsId);
    }

    addOrUpdateNotificationSettings(notificationSettings: any): Observable<NotificationSettings> {
        return this.http.put<NotificationSettings>(EdukitConfig.BASICS.API_URL + "/notification/add-update-notificationSettings", notificationSettings);
    }

    deleteNotificationSettings(notificationSettingsId): Observable<any> {
        return this.http.delete(EdukitConfig.BASICS.API_URL + "/notification/notificationSettings/" + notificationSettingsId);
    }
}

