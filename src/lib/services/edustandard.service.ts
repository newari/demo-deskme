import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../../src/ezukit.config";
import { EduStandard } from "../models/edustandard.model";

@Injectable() 
export class EduStandardService{
    private eduStandard: EduStandard;
    constructor(private http:HttpClient){ }

    addEduStandard(eduStandard:EduStandard) : Observable<EduStandard>{
        return this.http.post<EduStandard>(EdukitConfig.BASICS.API_URL+"/webber/edustandard", eduStandard);
    }

    getEduStandard(filter?:any) :Observable<EduStandard[]>{
        return this.http.get<EduStandard[]>(EdukitConfig.BASICS.API_URL+"/webber/edustandard", {params:filter});
    }

    getOneEduStandard(eduStandardId) :Observable<EduStandard>{
        return this.http.get<EduStandard>(EdukitConfig.BASICS.API_URL+"/webber/edustandard/"+eduStandardId);
    }

    updateEduStandard(eduStandardId, eduStandard:EduStandard) : Observable<EduStandard>{
        return this.http.put<EduStandard>(EdukitConfig.BASICS.API_URL+"/webber/edustandard/"+eduStandardId, eduStandard);
    }

    deleteEduStandard(eduStandardId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/edustandard/"+eduStandardId);
    }
}
    