import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';
import { Center } from '../models/center.model';

@Injectable() 
export class CenterService{
    private center: Center;
    constructor(private http:HttpClient){ }

    addCenter(center:any) : Observable<Center>{
        return this.http.post<Center>(EdukitConfig.BASICS.API_URL+"/org/center", center);
    }

    getCenter(filter?:any) :Observable<Center[]>{
        return this.http.get<Center[]>(EdukitConfig.BASICS.API_URL+"/org/center", {params:filter});
    }

    getOneCenter(centerId) :Observable<Center>{
        return this.http.get<Center>(EdukitConfig.BASICS.API_URL+"/org/center/"+centerId);
    }

    updateCenter(centerId, center:any) : Observable<Center>{
        return this.http.put<Center>(EdukitConfig.BASICS.API_URL+"/org/center/"+centerId, center);
    }

    deleteCenter(centerId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/org/center/"+centerId);
    }

    checkSession(dta) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/set-session", dta);
    }
    checkClient(dta) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/get-session", dta);
    }
}
    
