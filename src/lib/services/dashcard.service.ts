import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';
import { Dashcard } from '../models/dashcard.model';

@Injectable() 
export class DashcardService{
    constructor(private http:HttpClient){ }

    getOneCard(cardId:string) : Observable<Dashcard>{
        return this.http.get<Dashcard>(EdukitConfig.BASICS.API_URL+"/core/dashcard/"+cardId);
    }

   
}
    
