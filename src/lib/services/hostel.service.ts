import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Hostel} from "../models/hostel.model";

@Injectable() 
export class HostelService{
    private hostel: Hostel;
    constructor(private http:HttpClient){ }

    addHostel(hostel:Hostel) : Observable<Hostel>{
        return this.http.post<Hostel>(EdukitConfig.BASICS.API_URL+"/hostel/hostel", hostel);
    }

    getHostel() :Observable<Hostel[]>{
        return this.http.get<Hostel[]>(EdukitConfig.BASICS.API_URL+"/hostel/hostel");
    }

    getOneHostel(hostelId) :Observable<Hostel>{
        return this.http.get<Hostel>(EdukitConfig.BASICS.API_URL+"/hostel/hostel/"+hostelId);
    }

    updateHostel(hostelId, hostel:Hostel) : Observable<Hostel>{
        return this.http.put<Hostel>(EdukitConfig.BASICS.API_URL+"/hostel/hostel/"+hostelId, hostel);
    }

    deleteHostel(hostelId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/hostel/hostel/"+hostelId);
    }
    getHostelSummary():Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/hostel/hostel-summary");
    }
}
    