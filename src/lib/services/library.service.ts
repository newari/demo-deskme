import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class LibraryService{
    constructor(private http:HttpClient){ }

    getSummaryData(params) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/library/summary", {params:params});
    }

    getStudentById(idNumber:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/library/student/"+idNumber);
    }
    getBookById(idNumber:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/library/book/"+idNumber);
    }
    issueBook(data) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/library/issue-book", data);
    }
    getUserBooks(userId) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/library/user/"+userId+"/books");
    }

}

