import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { Passage } from "../models/passage.model";

@Injectable() 
export class PassageService{
    private passage: Passage;
    constructor(private http:HttpClient){ }

    addPassage(passage: any): Observable<Passage> {
        return this.http.post<Passage>(EdukitConfig.BASICS.API_URL + "/qbank/passage", passage);
    }
    addPassageQuestion(passageQuestion): Observable<any> {
        return this.http.post<any>(EdukitConfig.BASICS.API_URL + "/qbank/passagequestion", passageQuestion);
    }
    getPassage(filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) opts.observe = 'response';
        return this.http.get<Passage[]>(EdukitConfig.BASICS.API_URL + "/qbank/passage",opts);
    }
    getPassageQuestions(passage): Observable<any> {
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/qbank/passage/questions", { params: { passage: passage } });
    }
    getOnePassage(passageId): Observable<Passage> {
        return this.http.get<Passage>(EdukitConfig.BASICS.API_URL + "/qbank/passage/" + passageId);
    }

    updatePassage(passageId, passage: Passage): Observable<Passage> {
        return this.http.put<Passage>(EdukitConfig.BASICS.API_URL + "/qbank/passage/" + passageId, passage);
    }

    deletePassage(passageId): Observable<any> {
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL + "/qbank/passage/" + passageId);
    }
    deletePassageQuestion(passageQuestionId): Observable<any> {
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL + "/qbank/question/" + passageQuestionId);
    }
}
    
