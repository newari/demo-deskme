import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { Testseries } from "../models/testseries.model";

@Injectable() 
export class TestseriesService{
    private testseries: Testseries;
    constructor(private http:HttpClient){ }

    addTestseries(testseries:any) : Observable<Testseries>{
        return this.http.post<Testseries>(EdukitConfig.BASICS.API_URL+"/testment/testseries", testseries);
    }
    getTestseries(filter?:any,withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe = 'response';
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/testment/testseries",opts);
    }
    getTestSeriesTest(data):Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/testment/testseriestest/",{params:data});
    }
    getTestSeriesTestOne(tsTestId) :Observable<Testseries>{
        return this.http.get<Testseries>(EdukitConfig.BASICS.API_URL+"/testment/testseriestest/"+tsTestId);
    }
    getOneTestseries(testSeriesId) :Observable<Testseries>{
        return this.http.get<Testseries>(EdukitConfig.BASICS.API_URL+"/testment/testseries/"+testSeriesId);
    }
    updateTestseries(testSeriesId, testseries) : Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/testment/testseries/"+testSeriesId, testseries);
    }
    deleteTestseries(testSeriesId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/testment/testseries/"+testSeriesId);
    }

    addTestSeriesTest(testSeriesTest):Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/testment/testseriestest", testSeriesTest);
    }
    removeTestSeriesTest(testSeriesTestId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/testment/testseriestest/"+testSeriesTestId);
    }
    updateStatus(data) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/ims/testment/testseries/updatestatus",{params : data});
    }
    productTestSeries(data) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/testment/testseries",{params : data});
    }
    getTSProduct(): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/testseries/tsproducts");
    }
    getTsProductOne(productId):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/testment/testseries/tsproduct/"+productId);
    }
    getTsProductTest(data):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/testment/testseries/tsproduct/tests",{params:data});
    }
    getLinkedProducts(tsId):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/testment/testseries/linkedproduct/"+tsId);
    }
    addTestAndAssign(data):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/testment/testseriestest/assign",data);
    }
    deleteTestAndAssign(testSeriesTestId):Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/testment/testseriestest/deleteassign/"+testSeriesTestId);
    }
    getTakeTest(data) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/testment/testseries/taketest",{params:data});
    }
    testAssignToFaculty(data) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/testment/testseries/testassign/faculty",data);
    }
    testTestWindow(data) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/student/testment/testwindow-2/app",{params:data});
    }
    getUserProductTestSeries(filter? : any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/testment/userproduct/testseries",{params : filter});
    }
    getUserProducts(filter? : any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/testment/userproduct",{params : filter});
    }
}
    
