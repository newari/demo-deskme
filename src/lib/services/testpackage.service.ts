import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { TestPackage } from "../models/testpackage.model";

@Injectable() 
export class TestPackageService{
    private testpackage: TestPackage;
    constructor(private http:HttpClient){ }

    addTestpackage(testpackage:any) : Observable<TestPackage>{
        return this.http.post<TestPackage>(EdukitConfig.BASICS.API_URL+"/testment/testpackage", testpackage);
    }

    getTestpackage() :Observable<TestPackage[]>{
        return this.http.get<TestPackage[]>(EdukitConfig.BASICS.API_URL+"/testment/testpackage");
    }

    getOneTestpackage(testpackageId) :Observable<TestPackage>{
        return this.http.get<TestPackage>(EdukitConfig.BASICS.API_URL+"/testment/testpackage/"+testpackageId);
    }

    updateTestpackage(testpackageId, testpackage:TestPackage) : Observable<TestPackage>{
        return this.http.put<TestPackage>(EdukitConfig.BASICS.API_URL+"/testment/testpackage/"+testpackageId, testpackage);
    }

    deleteTestpackage(testpackageId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/testment/testpackage/"+testpackageId);
    }

    addTestSeries(testPackageId, testSeriesId):Observable<any>{
        let body:any={testPackageId:testPackageId, testSeriesId:testSeriesId};
        
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/testment/testpackage/add-testseries", body);
    }
    removeTestSeries(testPackageId,testSeriesId):Observable<any>{
        let body:any={testPackageId:testPackageId, testSeriesId:testSeriesId};
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/testment/testpackage/remove-testseries", body);
    }
}
    
