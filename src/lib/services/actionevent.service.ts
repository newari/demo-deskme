import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { EdukitConfig } from "../../ezukit.config";
import { Actionevent } from "../models/actionevent.model";
import { HttpClient } from '@angular/common/http';


@Injectable() 
export class ActioneventService{
    private Actionevent: Actionevent;
    constructor(private http:HttpClient){ }

    getActionevent(filter?) :Observable<Actionevent[]>{
        return this.http.get<Actionevent[]>(EdukitConfig.BASICS.API_URL+"/core/actionevent", {params:filter});
    }

    

}
    
