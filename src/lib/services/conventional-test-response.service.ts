import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class ConventionalTestResponseService{
    private test: any;
    constructor(private http:HttpClient){ }

    addTestResponse(test:any) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestresponse", test);
    }

    getTestParticipants(testId:string, filter?:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/conventional-exam/test/"+testId+"/participants", {params:filter});
    }

    getAssignedCopies():Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/conventional-exam/test/assigned-participants");

    }

    getOneTestResponse(testId) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestresponse/"+testId);
    }
    
    updateTestResponse(testId, data:any) : Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestresponse/"+testId, data);
    }

    deleteTestResponse(testId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestresponse/"+testId);
    }
}
    
