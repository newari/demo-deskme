import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {News} from "../models/news.model";

@Injectable()
export class NoticeService{
    private news: News;
    constructor(private http:HttpClient){ }

    addNews(news:News) : Observable<News>{
        return this.http.post<News>(EdukitConfig.BASICS.API_URL+"/webber/news", news);
    }

    getNews(filter?: any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/news", opts);
    }

    getOneNews(newsId) :Observable<News>{
        return this.http.get<News>(EdukitConfig.BASICS.API_URL+"/webber/news/"+newsId);
    }

    updateNews(newsId, news?:any) : Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/webber/news/"+newsId, news);
    }

    deleteNews(newsId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/news/"+newsId);
    }

    getNewsCount(): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/webber/newses/news-count");
    }
}
