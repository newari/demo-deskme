import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Video} from "../models/video.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class VideovibeService{
    private video: Video;
    // private reqHeaders:Headers;
    private vvUrl:string;
    constructor(private http:HttpClient){ 
        //  this.reqHeaders=new Headers({'SproutVideo-Api-Key':'78f5c4b17191907ea33db78775358378'});
         this.vvUrl='https://api.sproutvideo.com/v1';
    }

    addSubscriber(videoData:any) : Observable<any>{
        

        return this.http.post(this.vvUrl+"/logins", videoData);
    }

    getSubscriber() :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/videovibe/subscriber");
    }

    getOneSubscriber(videoId) :Observable<any>{
        return this.http.get(this.vvUrl+"/logins");
    }

    
}
    
