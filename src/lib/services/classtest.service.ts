import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";

import { EdukitConfig } from '../../ezukit.config';

import {Classtest} from "../models/classtest.model";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ClasstestService{
    private classtest: Classtest;
    constructor(private http:HttpClient){ }

    addClasstest(classtest:Classtest) : Observable<Classtest>{

        return this.http.post<Classtest>(EdukitConfig.BASICS.API_URL+"/class-test/classtest", classtest);

    }

    getClasstest(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe='response';
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/class-test/classtest", opts);

    }

    getOneClasstest(classtestId) :Observable<Classtest>{
        return this.http.get<Classtest>(EdukitConfig.BASICS.API_URL+"/class-test/classtest/"+classtestId);
    }

    updateClasstest(classtestId, classtest:Classtest) : Observable<Classtest>{

        return this.http.put<Classtest>(EdukitConfig.BASICS.API_URL+"/class-test/classtest/"+classtestId, classtest)
    }

    deleteClasstest(classtestId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/class-test/classtest/"+classtestId)
    }
    getClassTestCount(): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/class-test/classtest/count");
    }
}
