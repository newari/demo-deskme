import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import {Frontpage} from "../models/frontpage.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class FrontpageService{
    private frontpage: Frontpage;
    constructor(private http:HttpClient){ }

    addFrontpage(frontpage:any) : Observable<Frontpage>{
        return this.http.post<Frontpage>(EdukitConfig.BASICS.API_URL+"/testment/webfront/page", frontpage);
    }

    addSection(pageName, section:any) : Observable<Frontpage>{
        section.pageName=pageName;
        return this.http.post<Frontpage>(EdukitConfig.BASICS.API_URL+"/testment/webfront/page/add-section", section);
    }

    getFrontpage() :Observable<Frontpage[]>{
        return this.http.get<Frontpage[]>(EdukitConfig.BASICS.API_URL+"/testment/webfront/page");
    }

    getOneFrontpage(frontpageId) :Observable<Frontpage>{
        return this.http.get<Frontpage>(EdukitConfig.BASICS.API_URL+"/testment/webfront/page/"+frontpageId);
    }

    updateFrontpage(frontpageId, frontpage:any) : Observable<Frontpage>{
        return this.http.put<Frontpage>(EdukitConfig.BASICS.API_URL+"/testment/webfront/page/"+frontpageId, frontpage);
    }

    deleteFrontpage(frontpageId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/testment/webfront/page/"+frontpageId);
    }
}
    
