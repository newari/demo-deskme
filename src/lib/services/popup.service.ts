import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Popup} from "../models/popup.model";

@Injectable() 
export class PopupService{
    private popup: Popup;
    constructor(private http:HttpClient){ }

    addPopup(popup:Popup) : Observable<Popup>{
        return this.http.post<Popup>(EdukitConfig.BASICS.API_URL+"/webber/popup", popup);
    }

    getPopup(filter?: any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/popup", opts);
    }

    getOnePopup(popupId) :Observable<Popup>{
        return this.http.get<Popup>(EdukitConfig.BASICS.API_URL+"/webber/popup/"+popupId);
    }

    updatePopup(popupId, popup:Popup) : Observable<Popup>{
        return this.http.put<Popup>(EdukitConfig.BASICS.API_URL+"/webber/popup/"+popupId, popup);
    }

    deletePopup(popupId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/popup/"+popupId);
    }
    getPopupCount(): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/webber/popups/popup-count");
    }
}
    