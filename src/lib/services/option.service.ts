import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Option} from "../models/option.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class OptionService{
    private option: Option;
    constructor(private http:HttpClient){ }

    addOption(option:any) : Observable<Option>{
        return this.http.post<Option>(EdukitConfig.BASICS.API_URL+"/cmn/option", option);
    }

    getOption(filter?:any) :Observable<Option[]>{
        return this.http.get<Option[]>(EdukitConfig.BASICS.API_URL+"/cmn/option", {params:filter});
    }

    getOneOption(optionId) :Observable<Option>{
        return this.http.get<Option>(EdukitConfig.BASICS.API_URL+"/cmn/option/"+optionId);
    }

    getOptionByName(optionName:string) :Observable<Option>{
        return this.http.get<Option>(EdukitConfig.BASICS.API_URL+"/admin/option/by-name/"+optionName);
    }

    updateOption(optionId, option:Option) : Observable<Option>{
        return this.http.put<Option>(EdukitConfig.BASICS.API_URL+"/cmn/option/"+optionId, option);
    }

    deleteOption(optionId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/cmn/option/"+optionId);
    }

    getOptionName() :Observable<Option[]>{
        return this.http.get<Option[]>(EdukitConfig.BASICS.API_URL+"/admin/option/name");
    }

    getOptionValue(optionName) :Observable<Option[]>{
        return this.http.get<Option[]>(EdukitConfig.BASICS.API_URL+"/admin/option/value/"+optionName);
    }

    getOptionChild(optionId) :Observable<Option[]>{
        return this.http.get<Option[]>(EdukitConfig.BASICS.API_URL+"/admin/option/child/"+optionId);
    }

}
    
