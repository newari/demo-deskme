import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate, CanActivateChild } from '@angular/router';
// Import our authentication service
import { AuthService } from './auth.service';
import { NotifierService } from '../components/notifier/notifier.service';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {

  constructor(private authService: AuthService, private router: Router, private notifier:NotifierService) {}

  canActivate() {
    // this.notifier.start();
    // If user is not logged in we'll send them to the homepage
    // this.notifier.start();
    // If user is not logged in we'll send them to the homepage
    if (!this.authService.loggedIn()) {
        this.authService.logout();
        this.router.navigate(['/student/auth/login']);
        this.notifier.alert('Permission denied!', "Login Again!", "danger", 100000);
        return false;
    }

    var admin=this.authService.admin();
    if (admin==null||(admin.type!="ADMIN"&&admin.type!="STAFF")) {
        this.authService.logout();
        this.router.navigate(['/student/auth/login']);
        this.notifier.alert('Permission denied!', "Login Again!", "danger", 100000);

        return false;
    }
    return true;
  }

  canActivateChild() {
    // If user is not logged in we'll send them to the homepage
    if (!this.authService.loggedIn()) {
        this.authService.logout();

        this.router.navigate(['/student/auth/login']);
        return false;
    }
    var admin=this.authService.admin();
    if (admin==null||(admin.type!="ADMIN"&&admin.type!="STAFF")) {
        this.authService.logout();

        this.router.navigate(['/student/auth/login']);
        return false;
    }

    return true;
  }

}
