import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Topic} from "../models/topic.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class TopicService{
    private topic: Topic;
    constructor(private http:HttpClient){ }

    addTopic(topic:any) : Observable<Topic>{
        return this.http.post<Topic>(EdukitConfig.BASICS.API_URL+"/admin/topic", topic);
    }

    getTopic(filter?:any) :Observable<Topic[]>{
        return this.http.get<Topic[]>(EdukitConfig.BASICS.API_URL+"/admin/topic", {params:filter});
    }

    getOneTopic(topicId) :Observable<Topic>{
        return this.http.get<Topic>(EdukitConfig.BASICS.API_URL+"/admin/topic/"+topicId);
    }

    updateTopic(topicId, topic:any) : Observable<Topic>{

        return this.http.put<Topic>(EdukitConfig.BASICS.API_URL+"/admin/topic/"+topicId, topic);
    }

    deleteTopic(topicId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/admin/topic/"+topicId);
    }
    getMultipleUnitTopic(unitId) :Observable<Topic[]>{
        return this.http.get<Topic[]>(EdukitConfig.BASICS.API_URL+"/admin/topic", {params:{unit : unitId}});
    }
}

