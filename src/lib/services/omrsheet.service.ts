import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { Omrsheet } from "../models/omrsheet.model";

@Injectable() 
export class OmrsheetService{
   // private Testpaper: Videoseries;
    constructor(private http:HttpClient){ }

    addOmrsheet(omrsheet:Omrsheet) : Observable<Omrsheet>{
        return this.http.post<Omrsheet>(EdukitConfig.BASICS.API_URL+"/omreasy/omrsheet",omrsheet);
    }

    getOmrsheet() :Observable<Omrsheet[]>{
        return this.http.get<Omrsheet[]>(EdukitConfig.BASICS.API_URL+"/omreasy/omrsheet");
    }

    getTotalOmrsheet() :Observable<Omrsheet[]>{
        return this.http.get<Omrsheet[]>(EdukitConfig.BASICS.API_URL+"/omreasy/Omrsheet/totalomrsheet");
    }

    getOneOmrsheet(omrsheetId) :Observable<Omrsheet>{
        return this.http.get<Omrsheet>(EdukitConfig.BASICS.API_URL+"/omreasy/omrsheet/"+omrsheetId);
    }

    updateOmrsheet(omrsheetId, omrsheet:Omrsheet) : Observable<Omrsheet>{
        return this.http.put<Omrsheet>(EdukitConfig.BASICS.API_URL+"/omreasy/omrsheet/"+omrsheetId, omrsheet);
    }

    deleteOmrsheet(omrsheetId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/omreasy/omrsheet/"+omrsheetId);
    }
    addOmrsheetQuestions(questions:any) : Observable<Omrsheet>{
        return this.http.put<Omrsheet>(EdukitConfig.BASICS.API_URL+"/omreasy/omrsheet/addQuestions", questions);
    }
    removeOmrsheetQuestions(questions:any) : Observable<Omrsheet>{
        return this.http.put<Omrsheet>(EdukitConfig.BASICS.API_URL+"/omreasy/omrsheet/removeQuestion", questions);
    }
    

}
    
