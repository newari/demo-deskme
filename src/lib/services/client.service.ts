import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { ClientConfig } from "../models/clientconfig.model";

@Injectable() 
export class ClientService{
    private buyer: any;
    constructor(private http:HttpClient){ }
    static getClient(){
        let clientStr=window.localStorage.getItem("client");
        if(!clientStr){
            return null;
        }
        let client=JSON.parse(clientStr);
        return client;
    }
    getLocalClient(){
        let clientStr=window.localStorage.getItem("client");
        if(!clientStr){
            return null;
        }
        let client=JSON.parse(clientStr);
        return client;
    }
    getClientConfig(filter?:any) :Observable<ClientConfig>{
       return this.http.get<ClientConfig>(EdukitConfig.BASICS.API_URL+"/core/client", {params:filter});
    }
    // getClientConfig(filter?:any) :Observable<ClientConfig>{
    //     let clientStr=window.localStorage.getItem("client");
    //     if(!clientStr){
    //         let data = this.http.get<ClientConfig>(EdukitConfig.BASICS.API_URL+"/core/client", {params:filter});
    //         if(data){
    //             window.localStorage.setItem("client", JSON.stringify(data));
    //             return data;
    //         }
    //     }
    //     let client=JSON.parse(clientStr);
    //     return client;
    // }
    getApps(filter?:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/core/client/apps", {params:filter});
    }
    getFacultyApps() :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/core/client/facultyapps");
    }
    getClientOneApp(appId:string) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/core/client/apps/"+appId);
    }
    getOneApp(appId:string) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/edukit-store/app/"+appId);
    }
    getClients(filter?:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/core/clients", {params:filter});
    }
    getAppInfo(filter?:any) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/app/WIN_APP/app-basics", {params:filter});
    }
}
    
