import { Injectable } from "@angular/core";

import "rxjs/Rx";
import { Observable } from "rxjs";


import {Blogsubscriber} from "../models/blogsubscriber.model";
import { HttpClient } from "@angular/common/http";
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class BlogsubscriberService{
    private blogsubscriber: Blogsubscriber;
    constructor(private http:HttpClient){ }

    addBlogsubscriber(blogsubscriber:Blogsubscriber) : Observable<Blogsubscriber>{
        return this.http.post<Blogsubscriber>(EdukitConfig.BASICS.API_URL+"/blog/blogsubscriber", blogsubscriber);
    }

    getBlogsubscriber(filter, withHeaders?:boolean) :Observable<any>{
        let opts :any = {params:filter};
        if(withHeaders){
            opts.observe="response";
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/blog/blogsubscriber",opts)
    }

    getOneBlogsubscriber(blogsubscriberId) :Observable<Blogsubscriber>{
        return this.http.get<Blogsubscriber>(EdukitConfig.BASICS.API_URL+"/blog/blogsubscriber/"+blogsubscriberId);
    }

    updateBlogsubscriber(blogsubscriberId, blogsubscriber?:any) : Observable<any>{

        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/blog/blogsubscriber/"+blogsubscriberId, blogsubscriber);
    }

    deleteBlogsubscriber(blogsubscriberId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/blog/blogsubscriber/"+blogsubscriberId);
    }
}
