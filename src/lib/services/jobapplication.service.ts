import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import {Jobapplication} from "../models/jobapplication.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class JobapplicationService{
    private jobapplication: Jobapplication;
    constructor(private http:HttpClient){ }

    addJobapplication(jobapplication:any) : Observable<Jobapplication>{
        return this.http.post<Jobapplication>(EdukitConfig.BASICS.API_URL+"/hr/jobapplication", jobapplication);
    }

    getJobapplication(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/hr/jobapplication", opts);
    }
    exportJobapplications(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/hr/export-jobapplication", opts);
    }
    getOneJobapplication(jobapplicationId) :Observable<Jobapplication>{
        return this.http.get<Jobapplication>(EdukitConfig.BASICS.API_URL+"/hr/jobapplication/"+jobapplicationId);
    }

    updateJobapplication(jobapplicationId, jobapplication:any) : Observable<Jobapplication>{
        return this.http.put<Jobapplication>(EdukitConfig.BASICS.API_URL+"/hr/jobapplication/"+jobapplicationId, jobapplication);
    }

    deleteJobapplication(jobapplicationId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/hr/jobapplication/"+jobapplicationId);
    }
}
    
