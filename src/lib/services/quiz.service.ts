import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { Quiz } from "../models/quiz.model";

@Injectable() 
export class QuizService{
    private quiz: Quiz;
    constructor(private http:HttpClient){ }

    addQuiz(data:any) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/qbank/quiz", data);
    }
    getQuiz(filter?:any,withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/quiz", opts);
    }
    getOneQuiz(quizId) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/quiz/"+quizId);
    }
    updateQuiz(quizId, quiz:any) : Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/qbank/quiz/"+quizId, quiz);
    }
    deleteQuiz(quizId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/qbank/quiz/"+quizId);
    }
    assignQuiz(data):Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/qbank/quiz/assign", data);
    }
    getUserQuizs() :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/quizparticipant/quiz");
    }
    getAllQuizs() :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/quiz/all");
    }
    getAllQuizGroup(filter?:any) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/student/all/quizgroup",{params:filter});
    }
    getQuizGroupQuiz(quizGroup,filter?:any) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/student/quizgroup/quiz/"+quizGroup,{params:filter});
    }
    getQuizReport(filter) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/student/quiz/report",{params : filter});
    }
}
    
