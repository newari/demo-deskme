import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class AccountingService{
    constructor(private http:HttpClient){ }

    getSummaryData(params) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/accounting/summary", {params:params});
    }
    getStudentWisePayments(filter?:any,withHeaders?:boolean) :Observable<any>{

        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/accounting/report/student-wise-payments", opts);
    }

}

