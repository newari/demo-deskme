import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Download} from "../models/download.model";

@Injectable() 
export class DownloadService{
    private download: Download;
    constructor(private http:HttpClient){ }

    addDownload(download:Download) : Observable<Download>{
        return this.http.post<Download>(EdukitConfig.BASICS.API_URL+"/webber/download", download);
    }

    getDownload(filter?: any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<Download[]>(EdukitConfig.BASICS.API_URL+"/webber/download",opts);
    }

    getOneDownload(downloadId) :Observable<Download>{
        return this.http.get<Download>(EdukitConfig.BASICS.API_URL+"/webber/download/"+downloadId);
    }

    updateDownload(downloadId, download:Download) : Observable<Download>{
        return this.http.put<Download>(EdukitConfig.BASICS.API_URL+"/webber/download/"+downloadId, download);
    }

    deleteDownload(downloadId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/download/"+downloadId);
    }
}
    