import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Webpage} from "../models/webpage.model";

@Injectable() 
export class WebpageService{
    private webpage: Webpage;
    constructor(private http:HttpClient){ }

    addWebpage(webpage:Webpage) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/webfront/page", webpage);
    }

    getWebpage(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe ='response';
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webfront/page", opts);
    }

    getOneWebpage(webpageId) :Observable<Webpage>{
        return this.http.get<Webpage>(EdukitConfig.BASICS.API_URL+"/webfront/page/"+webpageId);
    }

    updateWebpage(webpageId, webpage:Webpage) : Observable<Webpage>{
        return this.http.put<Webpage>(EdukitConfig.BASICS.API_URL+"/webfront/page/"+webpageId, webpage);
    }

    deleteWebpage(webpageId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webfront/page/"+webpageId);
    }
    getWebSiteWebpages(websiteId):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/webfront/page/"+websiteId+"/webpages");
    }
    getWebsitePageCount(): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/webfront/webpage-count");
    }
    getContentBlock(pageId:string): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/webber/webpage/"+pageId+"/contentblocks");
    }
    addContentBlock(blk:any): Observable<any> {
        return this.http.post(EdukitConfig.BASICS.API_URL + "/webber/contentblock", blk);
    }
    updateContentBlock(blkId:string, blk:any): Observable<any> {
        return this.http.put(EdukitConfig.BASICS.API_URL + "/webber/contentblock/"+blkId, blk);
    }
    createDuplicatePage(pageId:string): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/webfront/page/"+pageId+"/create-duplicate", {})
    }
}
    