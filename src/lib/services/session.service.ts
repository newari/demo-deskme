import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import {Session} from "../models/session.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class SessionService{
    private session: Session;
    constructor(private http:HttpClient){ }

    addSession(session:any) : Observable<Session>{
        return this.http.post<Session>(EdukitConfig.BASICS.API_URL+"/admin/session", session);
    }

    getSession(filter?:any) :Observable<Session[]>{
        return this.http.get<Session[]>(EdukitConfig.BASICS.API_URL+"/admin/session", {params:filter});
    }

    getOneSession(sessionId) :Observable<Session>{
        return this.http.get<Session>(EdukitConfig.BASICS.API_URL+"/admin/session/"+sessionId);
    }

    updateSession(sessionId, session:any) : Observable<Session>{
        return this.http.put<Session>(EdukitConfig.BASICS.API_URL+"/admin/session/"+sessionId, session);
    }

    deleteSession(sessionId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/admin/session/"+sessionId);
    }
}
    
