import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { TmFeedbackSubjects } from "../models/tmfeedbacksubjects.model";
import { TmFeedbackQs } from "../models/tmfeedbackqs.model";

@Injectable() 
export class TmFeedbackService{
    private TmfeedbackSubjects: TmFeedbackSubjects;
    constructor(private http:HttpClient){ }
    addFbSubject(data:TmFeedbackSubjects) : Observable<TmFeedbackSubjects>{
        return this.http.post<TmFeedbackSubjects>(EdukitConfig.BASICS.API_URL+"/testment/tmfeedbacksubjects", data);
    }
    getFbSubjects(filter?:any, withHeaders? : boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/testment/tmfeedbacksubjects",opts);
    }
    updateFbSubject(id, data:TmFeedbackSubjects) : Observable<TmFeedbackSubjects>{
        return this.http.put<TmFeedbackSubjects>(EdukitConfig.BASICS.API_URL+"/testment/tmfeedbacksubjects/"+id, data);
    }
    getOneFbSubject(id) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/testment/tmfeedbacksubjects/"+id);
    }
    deleteFbSubject(id):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/testment/tmfeedbacksubjects/"+id);
    }
    addFeedbackQs(feedback:TmFeedbackQs) : Observable<TmFeedbackQs>{
        return this.http.post<TmFeedbackQs>(EdukitConfig.BASICS.API_URL+"/testment/tmfeedbackqs", feedback);
    }
    getFeedbackQs(filter?:any, withHeaders? : boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/testment/tmfeedbackqs",opts);
    }
    updateFeedbackQs(id, data:TmFeedbackQs) : Observable<TmFeedbackQs>{
        return this.http.put<TmFeedbackQs>(EdukitConfig.BASICS.API_URL+"/testment/tmfeedbackqs/"+id, data);
    }
    getStudentFeedbackQs(filter?:any) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/student/testment/tmfeedbackqs",{params : filter});
    }
    exportFeedbacks(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/testment/tmfeedbackqs/export-feedbacks", opts);
    }
    // addFeedbackQs(feedbackId,fbqsId){
    //     let body: any = { feedbackId: feedbackId, fbqsId: fbqsId };
    //     return this.http.put<Feedback>(EdukitConfig.BASICS.API_URL + "/conventional-exam/conventionalexamfeedback/add-qs/" , body);
    // }
    // removeFeedBackQs(feedbackId, fbqsId){
    //     let body: any = { feedbackId: feedbackId, fbqsId: fbqsId };
    //     return this.http.put<Feedback>(EdukitConfig.BASICS.API_URL + "/conventional-exam/conventionalexamfeedback/remove-qs/", body);

    // }

    // addFeedbackResponse(data): Observable<any>{
    //     return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexamfeedbackresponse", data);
    // }
    // getFeedbackResponse(testId:string) :Observable<any[]>{
    //     return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexam/feedback-response/"+testId);
    // }
}
    