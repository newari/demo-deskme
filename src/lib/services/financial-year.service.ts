import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';
import { FinancialYear } from '../models/financial-year.model';

@Injectable() 
export class FinancialYearService{
    private financialYear: FinancialYear;
    constructor(private http:HttpClient){ }

    addFinancialYear(financialYear:any) : Observable<FinancialYear>{
        return this.http.post<FinancialYear>(EdukitConfig.BASICS.API_URL+"/sales/financialyear", financialYear);
    }

    getFinancialYear(filter?:any) :Observable<FinancialYear[]>{
        return this.http.get<FinancialYear[]>(EdukitConfig.BASICS.API_URL+"/sales/financialyear", {params:filter});
    }

    getOneFinancialYear(financialYearId) :Observable<FinancialYear>{
        return this.http.get<FinancialYear>(EdukitConfig.BASICS.API_URL+"/sales/financialyear/"+financialYearId);
    }

    updateFinancialYear(financialYearId, financialYear:any) : Observable<FinancialYear>{
        return this.http.put<FinancialYear>(EdukitConfig.BASICS.API_URL+"/sales/financialyear/"+financialYearId, financialYear);
    }

    deleteFinancialYear(financialYearId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/sales/financialyear/"+financialYearId);
    }
}
    
