import { Injectable } from "@angular/core";

import "rxjs/Rx";
import { Observable } from "rxjs";
import {Qset} from "../models/qset.model";
import { HttpClient } from "@angular/common/http";
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class QsetService{
    private qset: Qset;
    constructor(private http:HttpClient){ }

    addQset(qset:Qset) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/qbank/qset", qset);
    }
    getQset(filter:any,withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/qbank/qset",opts);
    }
    getQsetQuestion(data) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/qsetqs/byqset-id", {params:data});
    }
    getSalesQsetQuestions(data) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/qsetqs/sale", {params:data});
    }
    addQsetQuestion(data):Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/qbank/qsetqs",data);
    }
    deleteQsetQuestion(qsetId): Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/qbank/qsetqs/deleteqsetqs", {params:{qsetId:qsetId}});
    }
    deleteOneQsetQuestion(qsetQs): Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/qbank/qsetqs/"+qsetQs);
    }
    getOneQset(qsetId) :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/qbank/qset/"+qsetId);
    }
    updateQset(qsetId, qset) : Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/qbank/qset/"+qsetId, qset);
    }
    updateQbankQuestion(qsetId,data) : Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/qbank/qsetqs/update/qbankqs/"+qsetId, data);
    }
    getQsetTitles(filter?:any):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/qbank/qset",{params:filter});
    }
    deleteQset(qsetId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/qbank/qset/"+qsetId);
    }

    saveFileQuestions(data,fieldSet):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/qbank/qsetqs/savefile/"+fieldSet,data);
    }
    assignGenQs(data,qsetId):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/qbank/qsetqs/maker/assign/"+qsetId+"/genqs",data);
    }
    convertGenData(data):Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/qbank/qsetqs/maker/convert",data);
    }
    assignFaculty(id,data):Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/qbank/qset/assignfaculty/"+id,data);
    }
    linkedQsets(questionId) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/qbank/qsetqs/question/link/"+questionId);
    }
    getQsetVariantQs(qsetId):Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/qsetqs/variant", {params:{qsetId:qsetId}});
    }
    assignQset(filter):Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/qset/assign", {params:filter});
    }

    getQuiz(quizId?:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student/testment/testwindow/qsetqs/question/"+quizId)
    }
    addResponse(data?:any):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/testment/useronlinetestqsresponse",data);
    }

    checkIsQuizFinished(filter:any):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/testment/useronlinetestqsresponse",{params:filter});
    }
    getQuestionsOrderSummary(qsetId):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/qbank/qset/questions/ordersummary/"+qsetId);
    }
    getQsetsOrderSummary(filter):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/qbank/qset/ordersummary",{params:filter});
    }
    getQuizData(quizId?:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/qbank/quizwindow/qset/"+quizId);
    }
    submitQuiz(quizId?:any,userId?:any, data?:any): Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/qbank/quizwindow/"+userId+"/submit/"+quizId, data);
    }

    getOneQuizReport(filter?:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/qbank/quizwindow/quiz/report",{params : filter});
    }
}
