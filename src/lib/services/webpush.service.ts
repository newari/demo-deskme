import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class WebPushService { 
    // webPushConfig:any;
    constructor(private http: HttpClient) {
        
     }

    addCampaign(campaign: any): Observable<any> {
        
        return this.http.post("https://onesignal.com/api/v1/notifications?useEdukitAuthHeader=false", campaign, { headers: { Authorization:'Basic YjQzYjAwYzctYTUyZi00OGY0LTlkYzYtMTcxYjkyMTg2NGIx'}});
       
    }

    getCampaign(): Observable<any>{
    //    return this.http.get(EdukitConfig.BASICS.API_URL + "/notification/webpush");
       return this.http.get("https://onesignal.com/api/v1/notifications", { headers: { Authorization: 'Basic YjQzYjAwYzctYTUyZi00OGY0LTlkYzYtMTcxYjkyMTg2NGIx' }, params: { useEdukitAuthHeader:'no', app_id:'c8595b3a-c1fc-4c9e-9dfd-ce450748897d'}});

   }
    getCampaignUsers(): Observable<any> {
        //    return this.http.get(EdukitConfig.BASICS.API_URL + "/notification/webpush");
        return this.http.get(EdukitConfig.BASICS.API_URL +"/notification/webpush-campaign/");

    }
  
//     getOneCampaign(capmapignId): Observable<any>{
//        return this.http.get("https://onesignal.com/api/v1/notifications/" + capmapignId + "?useEdukitAuthHeader=false", { headers: { Authorization: 'Basic YjQzYjAwYzctYTUyZi00OGY0LTlkYzYtMTcxYjkyMTg2NGIx' }, params: { useEdukitAuthHeader: 'no', app_id: 'c8595b3a-c1fc-4c9e-9dfd-ce450748897d' } });
//    }
    getOneCampaign(capmapignId): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL +'/notification/webpush-campaign/'+capmapignId);
    }
    
//     sendCampaign(data: any): Observable<any>{
//        return this.http.post("https://onesignal.com/api/v1/notifications", data,{ headers: { Authorization: 'Basic YjQzYjAwYzctYTUyZi00OGY0LTlkYzYtMTcxYjkyMTg2NGIx' }, params: { useEdukitAuthHeader: 'no', app_id: 'c8595b3a-c1fc-4c9e-9dfd-ce450748897d' } });

//    }
    sendCampaign(data: any): Observable<any> {
        return this.http.post(EdukitConfig.BASICS.API_URL + '/notification/webpush-campaign/send', data);

    }

}

