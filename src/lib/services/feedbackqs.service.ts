import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Feedbackqs} from "../models/feedbackqs.model";

@Injectable() 
export class FeedbackqsService{
    private feedbackqs: Feedbackqs;
    constructor(private http:HttpClient){ }

    addFeedbackqs(feedbackqs:Feedbackqs) : Observable<Feedbackqs>{
        return this.http.post<Feedbackqs>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexamfeedbackqs", feedbackqs);
    }

    getFeedbackqs() :Observable<Feedbackqs[]>{
        return this.http.get<Feedbackqs[]>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexamfeedbackqs");
    }

    getOneFeedbackqs(feedbackqsId) :Observable<Feedbackqs>{
        return this.http.get<Feedbackqs>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexamfeedbackqs/"+feedbackqsId);
    }

    updateFeedbackqs(feedbackqsId, feedbackqs:Feedbackqs) : Observable<Feedbackqs>{
        return this.http.put<Feedbackqs>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexamfeedbackqs/"+feedbackqsId, feedbackqs);
    }

    deleteFeedbackqs(feedbackqsId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionalexamfeedbackqs/"+feedbackqsId);
    }
}
    