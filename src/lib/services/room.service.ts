import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Room} from "../models/room.model";

@Injectable() 
export class RoomService{
    private room: Room;
    constructor(private http:HttpClient){ }

    addRoom(room:Room) : Observable<Room>{
        return this.http.post<Room>(EdukitConfig.BASICS.API_URL+"/hostel/hostelroom", room);
    }

    getRoom(filter?:any) :Observable<Room[]>{
        return this.http.get<Room[]>(EdukitConfig.BASICS.API_URL+"/hostel/hostelroom", {params:filter});
    }

    getOneRoom(roomId) :Observable<Room>{
        return this.http.get<Room>(EdukitConfig.BASICS.API_URL+"/hostel/hostelroom/"+roomId);
    }

    updateRoom(roomId, room:Room) : Observable<Room>{
        return this.http.put<Room>(EdukitConfig.BASICS.API_URL+"/hostel/hostelroom/"+roomId, room);
    }

    deleteRoom(roomId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/hostel/hostelroom/"+roomId);
    }
}
    