import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import {CourseProduct} from "../models/courseproduct.model";
import { EdukitConfig } from "../../../src/ezukit.config";

@Injectable() 
export class CourseProductService{
    private courseProduct: CourseProduct;
    constructor(private http:HttpClient){ }

    addCourseProduct(courseProduct:CourseProduct) : Observable<CourseProduct>{
        return this.http.post<CourseProduct>(EdukitConfig.BASICS.API_URL+"/course-creator/courseproduct", courseProduct);
    }

    getCourseProduct() :Observable<CourseProduct[]>{
        return this.http.get<CourseProduct[]>(EdukitConfig.BASICS.API_URL+"/course-creator/courseproduct");
    }

    getOneCourseProduct(courseProductId) :Observable<CourseProduct>{
        return this.http.get<CourseProduct>(EdukitConfig.BASICS.API_URL+"/course-creator/courseproduct/"+courseProductId);
    }

    updateCourseProduct(courseProductId, courseProduct:CourseProduct) : Observable<CourseProduct>{
        return this.http.put<CourseProduct>(EdukitConfig.BASICS.API_URL+"/course-creator/courseproduct/"+courseProductId, courseProduct);
    }

    deleteCourseProduct(courseProductId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/course-creator/courseproduct/"+courseProductId);
    }
}
    