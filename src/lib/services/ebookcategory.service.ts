import { Injectable } from "@angular/core";

import "rxjs/Rx";
import { Observable } from "rxjs";

import {Ebookcategory} from "../models/ebookcategory.model";
import { HttpClient } from "@angular/common/http";
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class EbookcategoryService{
    private ebookcategory: Ebookcategory;
    constructor(private http:HttpClient){ }

    addEbookcategory(ebookcategory:Ebookcategory) : Observable<Ebookcategory>{
        return this.http.post<Ebookcategory>(EdukitConfig.BASICS.API_URL+"/admin/ebookcategory", ebookcategory);
    }

    getEbookcategory(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders)
        opts.observe='response';

        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/admin/ebookcategory",opts);
    }

    getOneEbookcategory(ebookcategoryId) :Observable<Ebookcategory>{
        return this.http.get<Ebookcategory>(EdukitConfig.BASICS.API_URL+"/admin/ebookcategory/"+ebookcategoryId);
    }

    updateEbookcategory(ebookcategoryId, ebookcategory:Ebookcategory) : Observable<Ebookcategory>{
        return this.http.put<Ebookcategory>(EdukitConfig.BASICS.API_URL+"/admin/ebookcategory/"+ebookcategoryId, ebookcategory);
    }

    deleteEbookcategory(ebookcategoryId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/admin/ebookcategory/"+ebookcategoryId);
    }
}
