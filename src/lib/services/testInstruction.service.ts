import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { TestInstruction } from "../models/testInstruction.model";

@Injectable() 
export class TestInstructionService{
    private testInstruction: TestInstruction;
    constructor(private http:HttpClient){ }

    addInstruction(instruction:any) : Observable<TestInstruction>{
        return this.http.post<TestInstruction>(EdukitConfig.BASICS.API_URL+"/testment/testinstruction", instruction);
    }
    
    getInstruction() :Observable<TestInstruction[]>{
        return this.http.get<TestInstruction[]>(EdukitConfig.BASICS.API_URL+"/testment/testinstruction",{params: {sort :'createdAt DESC'}});
    }
    getOneInstruction(instructionId) :Observable<TestInstruction>{
        return this.http.get<TestInstruction>(EdukitConfig.BASICS.API_URL+"/testment/testinstruction/"+instructionId);
    }
    updateInstruction(instructionId, data) : Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/testment/testinstruction/"+instructionId, data);
    }
    deleteInstruction(instructionId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/testment/testinstruction/"+instructionId);
    }
}
    
