import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { Attendance } from '../models/attendance.model';

@Injectable() 
export class AttendanceService{
    private attendance: Attendance;
    constructor(private http:HttpClient){ }

    addAttendance(attendance:any) : Observable<Attendance>{
        return this.http.post<Attendance>(EdukitConfig.BASICS.API_URL+"/student-manager/studentattendance", attendance);
    }

    getAttendance(filter) :Observable<Attendance[]>{
        return this.http.get<Attendance[]>(EdukitConfig.BASICS.API_URL+"/student-manager/studentattendance", {params:filter});
    }

    getOneAttendance(attendanceId) :Observable<Attendance>{
        return this.http.get<Attendance>(EdukitConfig.BASICS.API_URL+"/student-manager/studentattendance/"+attendanceId);
    }

    updateAttendance(attendanceId, attendance:Attendance) : Observable<Attendance>{
        return this.http.put<Attendance>(EdukitConfig.BASICS.API_URL+"/student-manager/studentattendance/"+attendanceId, attendance);
    }

    deleteAttendance(attendanceId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/student-manager/studentattendance/"+attendanceId);
    }

    getBatchWiseAttendanceReport(filter?: any): Observable<any> {
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/student-manager/studentattendance/batch-wise-report", {params:filter});
    }
    addAttendanceManually(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+ "/student-manager/studentattendance/add-manually", data);
    }
    sendAbsentSms(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+ "/student-manager/studentattendance/send-absent-sms", data);
    }
    sendPresentSms(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+ "/student-manager/studentattendance/send-present-sms", data);
    }
    sendLeaveSms(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+ "/student-manager/studentattendance/send-leave-sms", data);
    }
}
    
