import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Menu} from "../models/Menu.model";

@Injectable() 
export class MenuService{
    constructor(private http:HttpClient){ }

    addMenu(menu:Menu) : Observable<Menu>{
        return this.http.post<Menu>(EdukitConfig.BASICS.API_URL+"/webber/menu", menu);
    }

    getMenu(filter?:any) :Observable<Menu[]>{
        return this.http.get<Menu[]>(EdukitConfig.BASICS.API_URL+"/webber/menu");
    }

    getOneMenu(menuId) :Observable<Menu>{
        return this.http.get<Menu>(EdukitConfig.BASICS.API_URL+"/webber/menu/"+menuId);
    }

    updateMenu(menuId, menu:Menu) : Observable<Menu>{
        return this.http.put<Menu>(EdukitConfig.BASICS.API_URL+"/webber/menu/"+menuId, menu);
    }

    deleteMenu(menuId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/menu/"+menuId);
    }
}
    