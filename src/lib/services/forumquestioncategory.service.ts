import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { Observable } from "rxjs";

import {ForumQuestionCategory} from "../models/forumquestioncategory.model";
import { HttpClient } from "@angular/common/http";
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class ForumQuestionCategoryService{
    private forumQuestionCategory: ForumQuestionCategory;
    constructor(private http:HttpClient){ }

    addForumQuestionCategory(forumQuestionCategory:ForumQuestionCategory) : Observable<ForumQuestionCategory>{
        return this.http.post<ForumQuestionCategory>(EdukitConfig.BASICS.API_URL+"/forum/forumquestioncategory", forumQuestionCategory);
    }

    getForumQuestionCategory(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any= {params:filter};
        if(withHeaders) opts.observe='response';
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/forum/forumquestioncategory", opts);
    }

    getOneForumQuestionCategory(categoryId) :Observable<ForumQuestionCategory>{
        return this.http.get<ForumQuestionCategory>(EdukitConfig.BASICS.API_URL+"/forum/forumquestioncategory/"+categoryId);
    }

    updateForumQuestionCategory(categoryId, forumQuestionCategory:ForumQuestionCategory) : Observable<ForumQuestionCategory>{ 
        return this.http.put<ForumQuestionCategory>(EdukitConfig.BASICS.API_URL+"/forum/forumquestioncategory/"+categoryId, forumQuestionCategory);
    }

    deleteForumQuestionCategory(categoryId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/forum/forumquestioncategory/"+categoryId);
    }
}