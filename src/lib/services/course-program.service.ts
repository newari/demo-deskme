import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import {CourseProgram} from "../models/course-program.model";
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class CourseProgramService{
    private courseProgram: CourseProgram;
    constructor(private http:HttpClient){ }

    addCourseProgram(courseProgram: CourseProgram) : Observable<CourseProgram>{
        return this.http.post<CourseProgram>(EdukitConfig.BASICS.API_URL+"/webber/courseprogram", courseProgram);
    }

    getCourseProgram(filter?: any, withHeaders?:boolean) :Observable<any>{
         let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/courseprogram",opts);
    }

    getOneCourseProgram(courseProgramId) :Observable<CourseProgram>{
        return this.http.get<CourseProgram>(EdukitConfig.BASICS.API_URL+"/webber/courseprogram/"+courseProgramId);
    }

    updateCourseProgram(courseProgramId, courseProgram:CourseProgram) : Observable<CourseProgram>{
        return this.http.put<CourseProgram>(EdukitConfig.BASICS.API_URL+"/webber/courseprogram/"+courseProgramId, courseProgram);
    }

    deleteCourseProgram(courseProgramId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/courseprogram/"+courseProgramId);
    }
}
    