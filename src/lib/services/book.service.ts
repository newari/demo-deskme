import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import {Book} from "../models/book.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class BookService{
    private book: Book;
    constructor(private http:HttpClient){ }

    addBook(book:any) : Observable<Book>{
        return this.http.post<Book>(EdukitConfig.BASICS.API_URL+"/book-store/book", book);
    }

    getBook(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe='response';
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/book-store/book", opts);
    }

    getOneBook(bookId) :Observable<Book>{
        return this.http.get<Book>(EdukitConfig.BASICS.API_URL+"/book-store/book/"+bookId);
    }

    updateBook(bookId, book:any) : Observable<Book>{
        return this.http.put<Book>(EdukitConfig.BASICS.API_URL+"/book-store/book/"+bookId, book);
    }

    deleteBook(bookId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/book-store/book/"+bookId);
    }
    launchAsProduct(bookId, data:any) : Observable<Book>{
        return this.http.post<Book>(EdukitConfig.BASICS.API_URL+"/book-store/book/launch-as-product/"+bookId, data);
    }

    addStockSegment(bookId, data:any) : Observable<Book>{
        return this.http.post<Book>(EdukitConfig.BASICS.API_URL+"/book-store/book/add-stock-segment/"+bookId, data);
    }

    distributeBook(bookId, data): Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/book-store/book/distribute/"+bookId,data);
    }
    addStock(bookId, data): Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/book-store/book/"+bookId+"/add-stock", data);
    }

    getStockHistory(bookId): Observable<any> {
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/book-store/book/"+bookId+"/stock-history");
    }

}
    
