import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {VideoSubscriber} from "../models/videosubscriber.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class VideoSubscriberService{
   // private subscriber: subscriber;
    constructor(private http:HttpClient){ }

    addSubscriber(subscriber:any) : Observable<VideoSubscriber>{
        return this.http.post<VideoSubscriber>(EdukitConfig.BASICS.API_URL+"/videovibe/subscriber", subscriber);
    }

    getSubscriber() :Observable<VideoSubscriber[]>{
        return this.http.get<VideoSubscriber[]>(EdukitConfig.BASICS.API_URL+"/videovibe/subscriber");
    }

    findSubscriber() :Observable<VideoSubscriber[]>{
        return this.http.get<VideoSubscriber[]>(EdukitConfig.BASICS.API_URL+"/videovibe/subscriber/find-subscriber");
    }

    getOneSubscriber(subscriberId) :Observable<VideoSubscriber>{
        return this.http.get<VideoSubscriber>(EdukitConfig.BASICS.API_URL+"/videovibe/subscriber/"+subscriberId);
    }

    updateSubscriber(subscriberId, subscriber:VideoSubscriber) : Observable<VideoSubscriber>{
        return this.http.put<VideoSubscriber>(EdukitConfig.BASICS.API_URL+"/videovibe/subscriber/"+subscriberId, subscriber);
    }

    deleteSubscriber(subscriberId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/videovibe/subscriber/"+subscriberId);
    }
    
    getSubscriberVideosSeries(userIdData:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/videovibe/subscriber/subscriber-videoseries", {params:userIdData});
    }
    addSubscriberVideosSeries(subscriberId, videoSeriesData:any) : Observable<VideoSubscriber>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/videovibe/subscriber/"+subscriberId, videoSeriesData);
    }

    removeSubscriberVideosSeries(videoSeriesId) : Observable<VideoSubscriber>{
        return this.http.delete<VideoSubscriber>(EdukitConfig.BASICS.API_URL+"/videovibe/subscriber/subscriber-remove-videoseries/"+videoSeriesId);
    }

}
    
