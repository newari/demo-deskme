import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Album} from "../models/album.model";

@Injectable() 
export class AlbumService{
    private album: Album;
    constructor(private http:HttpClient){ }

    addAlbum(album:Album) : Observable<Album>{
        return this.http.post<Album>(EdukitConfig.BASICS.API_URL+"/webber/album", album);
    }

    getAlbum(filter?: any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/album", opts);
    }

    getOneAlbum(albumId) :Observable<Album>{
        return this.http.get<Album>(EdukitConfig.BASICS.API_URL+"/webber/album/"+albumId);
    }

    updateAlbum(albumId, album:Album) : Observable<Album>{
        return this.http.put<Album>(EdukitConfig.BASICS.API_URL+"/webber/album/"+albumId, album);
    }

    deleteAlbum(albumId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/album/"+albumId);
    }
    getAlbumPhotoOrVideo(filter?: any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL +"/webber/album/data", {params:filter})
    }
}
    