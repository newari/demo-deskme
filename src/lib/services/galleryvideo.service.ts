import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Galleryvideo} from "../models/galleryvideo.model";

@Injectable() 
export class GalleryvideoService{
    private galleryvideo: Galleryvideo;
    constructor(private http:HttpClient){ }

    addGalleryvideo(galleryvideo:Galleryvideo) : Observable<Galleryvideo>{
        return this.http.post<Galleryvideo>(EdukitConfig.BASICS.API_URL+"/webber/galleryvideo", galleryvideo);
    }

    getGalleryvideo(filter?: any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/galleryvideo", opts);
    }

    getOneGalleryvideo(galleryvideoId) :Observable<Galleryvideo>{
        return this.http.get<Galleryvideo>(EdukitConfig.BASICS.API_URL+"/webber/galleryvideo/"+galleryvideoId);
    }

    updateGalleryvideo(galleryvideoId, galleryvideo:Galleryvideo) : Observable<Galleryvideo>{
        return this.http.put<Galleryvideo>(EdukitConfig.BASICS.API_URL+"/webber/galleryvideo/"+galleryvideoId, galleryvideo);
    }

    deleteGalleryvideo(galleryvideoId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/galleryvideo/"+galleryvideoId);
    }
    getVideoCount(): Observable<any> {
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/webber/galleryvideo/count");
    }
}
    