import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Organization} from "../models/organization.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class OrganizationService{
    private organization: Organization;
    constructor(private http:HttpClient){ }

    addOrganization(organization:Organization) : Observable<Organization>{
        return this.http.post<Organization>(EdukitConfig.BASICS.API_URL+"/admin/organization", organization);
    }

    getOrganization() :Observable<Organization[]>{
        return this.http.get<Organization[]>(EdukitConfig.BASICS.API_URL+"/admin/organization");
    }

    getOneOrganization(organizationId) :Observable<Organization>{
        return this.http.get<Organization>(EdukitConfig.BASICS.API_URL+"/admin/organization/"+organizationId);
    }

    updateOrganization(organizationId, organization:Organization) : Observable<Organization>{
        return this.http.put<Organization>(EdukitConfig.BASICS.API_URL+"/admin/organization/"+organizationId, organization);
    }

    deleteOrganization(organizationId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/admin/organization/"+organizationId);
    }
}
    
