import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Subject} from "../models/subject.model";
import { HttpClient } from '@angular/common/http';
import { Subjectunit } from '../models/subjectunit.model';
import { Topic } from '../models/topic.model';
import { EdukitConfig } from '../../ezukit.config';

@Injectable()
export class SubjectService{
    private subject: Subject;
    constructor(private http:HttpClient){ }

    addSubject(subject:Subject) : Observable<Subject>{
        return this.http.post<Subject>(EdukitConfig.BASICS.API_URL+"/admin/subject", subject);
    }
    getSubject(filter?:any) :Observable<Subject[]>{
        return this.http.get<Subject[]>(EdukitConfig.BASICS.API_URL+"/admin/subject", {params:filter});
    }
    getOneSubject(subjectId) :Observable<Subject>{
        return this.http.get<Subject>(EdukitConfig.BASICS.API_URL+"/admin/subject/"+subjectId);
    }
    updateSubject(subjectId, subject:any) : Observable<Subject>{
        return this.http.put<Subject>(EdukitConfig.BASICS.API_URL+"/admin/subject/"+subjectId, subject);
    }
    deleteSubject(subjectId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/admin/subject/"+subjectId);
    }
    getUnit(subjectId) :Observable<Subjectunit[]>{
        return this.http.get<Subjectunit[]>(EdukitConfig.BASICS.API_URL+"/admin/subject/"+subjectId+"/unit");
    }
    getTopic(subjectId,filter?:any) :Observable<Topic[]>{
        return this.http.get<Topic[]>(EdukitConfig.BASICS.API_URL+"/admin/subject/"+subjectId+"/topic",{params:filter});
    }
    getMultipleSubjectUnit(subjectId) :Observable<Subjectunit[]>{
        return this.http.get<Subjectunit[]>(EdukitConfig.BASICS.API_URL+"/admin/subjectunit" ,{params:{subject:subjectId}});
    }
    addSubjectUnit(subjectunit:any) : Observable<Subjectunit>{
        return this.http.post<Subjectunit>(EdukitConfig.BASICS.API_URL+"/admin/subjectunit", subjectunit);
    }

    addSubjectTopic(topic:any) : Observable<Topic>{
        return this.http.post<Topic>(EdukitConfig.BASICS.API_URL+"/admin/topic", topic);
    }
}

