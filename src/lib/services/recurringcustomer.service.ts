import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import {Recurringcustomer} from "../models/recurringcustomer.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class RecurringcustomerService{
    private recurringcustomer: Recurringcustomer;
    constructor(private http:HttpClient){ }

    addRecurringcustomer(recurringcustomer:any) : Observable<Recurringcustomer>{
        return this.http.post<Recurringcustomer>(EdukitConfig.BASICS.API_URL+"/book-store/recurringcustomer", recurringcustomer);
    }

    getRecurringcustomer(filter?:any) :Observable<Recurringcustomer[]>{
        return this.http.get<Recurringcustomer[]>(EdukitConfig.BASICS.API_URL+"/sales/recurring-customer", {params:filter});
    }

    getOneRecurringcustomer(recurringcustomerId) :Observable<Recurringcustomer>{
        return this.http.get<Recurringcustomer>(EdukitConfig.BASICS.API_URL+"/book-store/recurringcustomer/"+recurringcustomerId);
    }

    updateRecurringcustomer(recurringcustomerId, recurringcustomer:any) : Observable<Recurringcustomer>{
        return this.http.put<Recurringcustomer>(EdukitConfig.BASICS.API_URL+"/book-store/recurringcustomer/"+recurringcustomerId, recurringcustomer);
    }

    deleteRecurringcustomer(recurringcustomerId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/book-store/recurringcustomer/"+recurringcustomerId);
    }
}
    
