import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';
import { Classroom } from "../models/classroom.model";


@Injectable()
export class ClassroomCoordinatorService{
    constructor(private http:HttpClient){ }



    getClassroom(memberUserId) :Observable<Classroom[]>{
        return this.http.get<Classroom[]>(EdukitConfig.BASICS.API_URL+"/staff/classroom-coordinator/"+memberUserId+"/classroom");
    }


}

