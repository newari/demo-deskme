import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import {Jobopening} from "../models/jobopening.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class JobopeningService{
    private jobopening: Jobopening;
    constructor(private http:HttpClient){ }

    addJobopening(jobopening:Jobopening) : Observable<Jobopening>{
        return this.http.post<Jobopening>(EdukitConfig.BASICS.API_URL+"/hr/jobopening", jobopening);
    }

    getJobopening() :Observable<Jobopening[]>{
        return this.http.get<Jobopening[]>(EdukitConfig.BASICS.API_URL+"/hr/jobopening");
    }

    getOneJobopening(jobopeningId) :Observable<Jobopening>{
        return this.http.get<Jobopening>(EdukitConfig.BASICS.API_URL+"/hr/jobopening/"+jobopeningId);
    }

    updateJobopening(jobopeningId, jobopening:any) : Observable<Jobopening>{
        return this.http.put<Jobopening>(EdukitConfig.BASICS.API_URL+"/hr/jobopening/"+jobopeningId, jobopening);
    }

    deleteJobopening(jobopeningId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/hr/jobopening/"+jobopeningId);
    }
}
    
