import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class SalesReportService{
    constructor(private http:HttpClient){ }

    salesSummary(filter?:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/sales/report/summary", {params:filter});
    }

    productWiseSales(filter?:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/sales/report/product-wise-sales", {params:filter});
    }


}

