import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class EmailPushService {
    // webPushConfig:any;
    constructor(private http: HttpClient) {

    }

    addCampaign(campaign: any): Observable<any> {

        return this.http.post("https://onesignal.com/api/v1/notifications?useEdukitAuthHeader=false", campaign, { headers: { Authorization: 'Basic YjQzYjAwYzctYTUyZi00OGY0LTlkYzYtMTcxYjkyMTg2NGIx' } });

    }

    getCampaign(): Observable<any> {
        //    return this.http.get(EdukitConfig.BASICS.API_URL + "/notification/webpush");
        return this.http.get("https://onesignal.com/api/v1/notifications", { headers: { Authorization: 'Basic YjQzYjAwYzctYTUyZi00OGY0LTlkYzYtMTcxYjkyMTg2NGIx' }, params: { useEdukitAuthHeader: 'no', app_id: 'c8595b3a-c1fc-4c9e-9dfd-ce450748897d' } });

    }
    getCampaignUsers(filter?:any): Observable<any> {
        //    return this.http.get(EdukitConfig.BASICS.API_URL + "/notification/webpush");
        return this.http.get(EdukitConfig.BASICS.API_URL + "/notification/emailpush-lists-contacts",{params:filter});

    }

    getAllContactList(): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL + "/notification/emailpush-contact-list");
    }
    getOneCampaign(capmapignId): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + '/notification/webpush-campaign/' + capmapignId);
    }

    sendCampaign(data: any): Observable<any> {
        return this.http.post(EdukitConfig.BASICS.API_URL + '/notification/emailpush-campaign/send', data);

    }
    // sendCampaign(data: any): Observable<any>  {
    //     console.log(data);
    //     return this.http.post("https://api.elasticemail.com/v2/campaign/add?apikey=47978b1e-a0cc-4abb-b559-6ecf80df3dd0&useEdukitAuthHeader=no&campaign=" + JSON.stringify(data), { headers: { "Contetn-Type": "application/x-www-form-urlencoded" }, params: {campaign: 'as'}});

    // }

}

