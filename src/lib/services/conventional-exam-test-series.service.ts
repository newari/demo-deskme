import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
// import { Testseries } from "../models/testseries.model";
import { ConventionalExamTestSeries } from "../models/conventionalExamTestSeries.model";

@Injectable() 
export class ConventionalExamTestseriesService{
    private testseries: ConventionalExamTestSeries;
    constructor(private http:HttpClient){ }

    addTestseries(testseries:any) : Observable<ConventionalExamTestSeries>{
        return this.http.post<ConventionalExamTestSeries>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestseries", testseries);
    }

    getTestseries(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts :any={params:filter};
        if(withHeaders){
            opts.observe = 'response';
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestseries", opts);
    }

    getOneTestseries(testseriesId) :Observable<ConventionalExamTestSeries>{
        return this.http.get<ConventionalExamTestSeries>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestseries/"+testseriesId);
    }

    updateTestseries(testseriesId, testseries:ConventionalExamTestSeries) : Observable<ConventionalExamTestSeries>{
        return this.http.put<ConventionalExamTestSeries>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestseries/"+testseriesId, testseries);
    }

    deleteTestseries(testseriesId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestseries/"+testseriesId);
    }
    addTest(data):Observable<any>{
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestseries/add-test",data);
    }
    removeTest(data):Observable<any>{
        
        return this.http.put<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/conventionaltestseries/remove-test",data);
    }
    getTestSeriesTest(filter): Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/conventional-exam/testseries/test",{params : filter});
    }
}
    
