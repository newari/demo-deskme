import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Galleryphoto} from "../models/galleryphoto.model";

@Injectable() 
export class GalleryphotoService{
    private galleryphoto: Galleryphoto;
    constructor(private http:HttpClient){ }

    addGalleryphoto(galleryphoto:Galleryphoto) : Observable<Galleryphoto>{
        return this.http.post<Galleryphoto>(EdukitConfig.BASICS.API_URL+"/webber/galleryphoto", galleryphoto);
    }

    getGalleryphoto(filter?: any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/galleryphoto", opts);
    }

    getOneGalleryphoto(galleryphotoId) :Observable<Galleryphoto>{
        return this.http.get<Galleryphoto>(EdukitConfig.BASICS.API_URL+"/webber/galleryphoto/"+galleryphotoId);
    }

    updateGalleryphoto(galleryphotoId, galleryphoto:Galleryphoto) : Observable<Galleryphoto>{
        return this.http.put<Galleryphoto>(EdukitConfig.BASICS.API_URL+"/webber/galleryphoto/"+galleryphotoId, galleryphoto);
    }

    deleteGalleryphoto(galleryphotoId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/galleryphoto/"+galleryphotoId);
    }
    getPhotoCount():Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/webber/galleryphoto/count");
    }
}
    