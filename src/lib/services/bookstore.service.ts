import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable() 
export class BookstoreService{
    constructor(private http:HttpClient){ }

    getSummaryData(params) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/book-store/summary", {params:params});
    }
    getStudentById(idNumber:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/book-store/student/"+idNumber);
    }
    getBookById(idNumber:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/book-store/book/"+idNumber);
    }
    issueBook(data) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/book-store/issue-book", data);
    }
    issueBookBySRN(data) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/book-store/issue-book-by-srn", data);
    }
    getUserBooks(userId) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/book-store/user/"+userId+"/books");
    }
    getProductWiseStock(filter?:any, withHeaders?:boolean) : Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/book-store/report/product-wise-stock", opts);
    }
    exportProductWiseStock(filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/book-store/report/export-product-wise-stock", opts);
    }
    getProductWiseDispatch(filter?:any, withHeaders?:boolean) : Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/book-store/report/product-wise-dispatch", opts);
    }
    exportProductWiseDispatch(filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/book-store/report/export-product-wise-dispatch", opts);
    }
    getDispatchHistory(filter?:any, withHeaders?:boolean) : Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/book-store/report/dispatch-history", opts);
    }
    exportDispatchHistory(filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/book-store/report/export-dispatch-history", opts);
    }
    getBookDispatchHistory(bookId:string, filter?:any, withHeaders?:boolean) : Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/book-store/report/dispatch-history/"+bookId, opts);
    }
    exportBookDispatchHistory(bookId: string, filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/book-store/report/export-dispatch-history/" + bookId, opts);
    }
    getBookSubscribers(bookId:string, filter?:any, withHeaders?:boolean) : Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/book-store/report/subscribers/"+bookId, opts);
    }
    exportBookSubscribers(bookId: string, filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/book-store/report/export-subscribers/" + bookId, opts);
    }
    
    getSubscriberWiseDispatchReport(filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/book-store/report/subscriber-wise-dispatch", opts);
    }
    exportSubscriberBookDispatchReport(filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/book-store/report/export-subscriber-wise-dispatch", opts);
    }
    getSubscriberBook(subscriberId): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/book-store/report/subscriber-book/"+subscriberId);
    }
    exportSubscriberBook(subscriberId): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/book-store/report/export-subscriber-book/"+subscriberId);
    }
}
    
