import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { User } from '../models/user.model';
import { EdukitConfig } from '../../ezukit.config';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class UserService{
    private user: User;
    constructor(private http:HttpClient){ }

    addUser(user:User) : Observable<User>{
        return this.http.post<User>(EdukitConfig.BASICS.API_URL+"/core/user", user);
    }

    getUser(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any= {params:filter};
        if(withHeaders){
            opts.observe='response';
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/core/user",opts);
    }

    getOneUser(userId) :Observable<User>{
        return this.http.get<User>(EdukitConfig.BASICS.API_URL+"/core/user/"+userId);
    }

    updateUser(userId, user:any) : Observable<User>{
        return this.http.put<User>(EdukitConfig.BASICS.API_URL+"/core/user/"+userId, user);
    }

    resetPassword(userId, newPassword) : Observable<User>{
        return this.http.put<User>(EdukitConfig.BASICS.API_URL+"/core/user/reset-password", {newPassword:newPassword, userId:userId, clcStdUpdate:true});
    }

    changeSessionPassword(currentPassword, newPassword) : Observable<User>{
        return this.http.put<User>(EdukitConfig.BASICS.API_URL+"/core/user/change-session-password", {currentPassword:currentPassword, newPassword:newPassword, clcStdUpdate:true});
    }

    changeMobile(userId, userType, mobile) : Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/core/user/change-mobile", {userId:userId, userType:userType, mobile:mobile});
    }

    changeEmail(userId, userType, email) : Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/core/user/change-email", {userId:userId, userType:userType, email:email, clcStdUpdate:true});
    }

    deleteUser(userId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/core/user/"+userId);
    }

    searchUser(query) :Observable<User[]>{
        return this.http.get<User[]>(EdukitConfig.BASICS.API_URL+"/core/user/search", {params:query});
    }
    searchUser2(query) :Observable<User[]>{
        return this.http.get<User[]>(EdukitConfig.BASICS.API_URL+"/core/user/search2", {params:query});
    }
    getUserProduct(filter) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/core/user/product", {params:filter});
    }
    addUserProduct(data) :Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/core/user/product", data);
    }
    getAppActivity(filter?:any) : Observable<any>{

        return this.http.get(EdukitConfig.BASICS.API_URL+"/core/user/app-activities", {params:filter});
    }

    getAppWiseActivity(filter?:any) : Observable<any>{

        return this.http.get(EdukitConfig.BASICS.API_URL+"/core/user/app-wise-activities", {params:filter});
    }

    updateUserActivityPermission(userId, perms) : Observable<any>{

        return this.http.put(EdukitConfig.BASICS.API_URL+"/core/user/activity-permission", {userId:userId,permissions:perms});
    }
    addFavLink(linkData): Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/core/user/favlink", linkData);
    }
    removeFavLink(flId): Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/core/user/favlink/"+flId);
    }
    getFavLink(): Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/core/user/favlink");
    }

    updateUserProduct(userProduct?:any, validTill?:any): Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/core/user/product/"+userProduct, {validTill:validTill});
    }
    getFreeProductForUser(filter?:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/core/user/free-product",{params:filter});
    }

    removeUserProduct(userProductId): Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/core/user/product/"+userProductId+"/remove");
    }
    resetDeviceLimit(userId:string):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/core/user/"+userId+"/reset-device-limit");
    }
}
