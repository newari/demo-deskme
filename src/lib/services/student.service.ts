import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import {Student} from "../models/student.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';

@Injectable()
export class StudentService{
    private student: Student;
    constructor(private http:HttpClient){ }

    addStudent(studentData:any) : Observable<Student>{
        return this.http.post<Student>(EdukitConfig.BASICS.API_URL+"/student-manager/student", studentData);
    }

    registerStudent(stdData:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/student-manager/register-student", stdData);
    }

    quickRegisterStudent(stdData:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/student-manager/student/quick-register", stdData);
    }
    getStudent(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-manager/student", opts);
    }
    getOneStudent(studentId) :Observable<Student>{
        return this.http.get<Student>(EdukitConfig.BASICS.API_URL+"/student-manager/student/"+studentId);
    }

    getStudentBySRN(srn) :Observable<Student>{
        return this.http.get<Student>(EdukitConfig.BASICS.API_URL+"/student-manager/student/srn/"+srn);
    }

    getSessionStudent(filter?:any) :Observable<Student>{
        return this.http.get<Student>(EdukitConfig.BASICS.API_URL+"/student-manager/student/me", {params:filter});
    }

    updateStudent(studentId, student:any) : Observable<Student>{
        return this.http.put<Student>(EdukitConfig.BASICS.API_URL+"/student-manager/student/"+studentId, student);
    }

    updateAddress(address, permanantAddress) : Observable<Student>{
        return this.http.put<Student>(EdukitConfig.BASICS.API_URL+"/student-manager/student/address", {address:address, permanantAddress:permanantAddress});
    }

    deleteStudent(studentId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/student-manager/student/"+studentId);
    }
    downloadStudentProfilePdf(studentId, recreatePdf?:boolean):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-manager/student/"+studentId+"/download-profile-pdf?recreatePdf="+recreatePdf);
    }
    createStudentProfilePdf(studentId, recreatePdf?:boolean):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-manager/student/"+studentId+"/create-profile-pdf");
    }
    createStudentIdCard(studentId):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/student-manager/student/id-card", {stdId:studentId});
    }
    setBatchRollNo(studentId:string,batchId?:any):Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/student-manager/student/"+studentId+"/batch/"+batchId+"/set-batch-rollno", {});
    }
    exportStudents(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-manager/student/export-students", opts);
    }
    fullfillOldOrder(srn:string):Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/student-manager/student/fullfill-old-order", {srn:srn});
    }
    unFullfillOldOrder(srn:string):Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/student-manager/student/unfullfill-order", {srn:srn});
    }

    getStudentByBatchEnrollment(filter?:any):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-manager/student/by-batch-enrollment", {params:filter});
    }

    getIdCardCount():Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-manager/student/id-card-count");
    }

    getStudentByProductCategory(filter?:any,withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe='response';
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/student-manager/student/by-product-category" , opts);

    }
    exportStudentsByProductCategory(filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/student-manager/student/export-product-category", opts);
    }
    createParentProfile(parentData):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL + "/student-manager/student/create-parent", parentData);
    }
    getStudentIdCard(studentId): Observable<any> {
        return this.http.get(EdukitConfig.BASICS.API_URL + "/student-manager/student/" + studentId + "/check-idcard");
    }
    updateStdIdCatd(idCardId, data?: any): Observable<any> {
        return this.http.put(EdukitConfig.BASICS.API_URL + "/student-manager/stdidcard/" + idCardId, data);
    }

    blockIdCard(idCardId, data?: any): Observable<any> {
        return this.http.post(EdukitConfig.BASICS.API_URL + '/student-manager/stdidcard/block-idcard/' + idCardId, data);
    }
    unblockIdCard(idCardId, data?: any): Observable<any> {
        return this.http.post(EdukitConfig.BASICS.API_URL + '/student-manager/stdidcard/unblock-idcard/' + idCardId, data);
    }
    getNotifications(filter?:any, withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe = 'response';
        }

        return this.http.get(EdukitConfig.BASICS.API_URL + '/student-manager/student/notification/get-student-notification', opts);
    }

    getNoticeBoard(filter?:any, withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe = 'response';
        }

        return this.http.get(EdukitConfig.BASICS.API_URL + '/notification/user-notice-board', opts);
    }

    setStundetSRNForceFully(studentId?:any, data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/student-manager/student/set-srn-forcefully/"+studentId,data);
    }
    enrollToBatch(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/student-manager/student/enroll-to-batch",data);
    }
    generateReferralCode(userId): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-manager/student/create-referralcode/"+userId);
    }
    getFreeLesson(filter): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/student-manager/student/lessons/free-lessons",{params:filter});
    }
    resetDeviceLimit(userId:string):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/core/user/"+userId+"/reset-device-limit");
    }
}

