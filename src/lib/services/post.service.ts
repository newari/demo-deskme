import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import {Post} from "../models/post.model";
import { EdukitConfig } from "../../../src/ezukit.config";

@Injectable() 
export class PostService{
    private post: Post;
    constructor(private http:HttpClient){ }

    addPost(post:Post) : Observable<Post>{
        return this.http.post<Post>(EdukitConfig.BASICS.API_URL+"/blog/blogpost", post);
    }

    getPost(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe='response';
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/blog/blogpost", opts);
    }

    getOnePost(postId) :Observable<Post>{
        return this.http.get<Post>(EdukitConfig.BASICS.API_URL+"/blog/blogpost/"+postId);
    }

    updatePost(postId, post?:any) : Observable<Post>{
        return this.http.put<Post>(EdukitConfig.BASICS.API_URL+"/blog/blogpost/"+postId, post);
    }

    deletePost(postId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/blog/blogpost/"+postId);
    }
    getPostCount(): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+ "/blog/blogpost/post-count");
    }
}
    