import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Video} from "../models/video.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class VideoService{
    private video: Video;
    // private reqHeaders:Headers;
    constructor(private http:HttpClient){ 
        //  this.reqHeaders=new Headers({'SproutVideo-Api-Key':'78f5c4b17191907ea33db78775358378'});
    }

    addVideo(videoData:any) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/videovibe/video", videoData);
    }
    
    uploadVideo(videoData:any) : Observable<any>{
        

        return this.http.post("https://api.sproutvideo.com/v1/videos", videoData);
    }

    getVideo(filter?:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/videovibe/video", {params:filter});
    }

    getVideoProducts(filter:any) :Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/cmn/product", {params:filter});
    }

    getOneVideo(videoId) :Observable<Video>{
        return this.http.get<Video>(EdukitConfig.BASICS.API_URL+"/videovibe/video/"+videoId);
    }

    getProviderToken() :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/videovibe/video/provider-token");
    }

    updateVideo(videoId, video:any) : Observable<Video>{
        return this.http.put<Video>(EdukitConfig.BASICS.API_URL+"/videovibe/video/"+videoId, video);
    }

    deleteVideo(videoId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/videovibe/video/"+videoId);
    }

    getVideoViews(userId,videoId):Observable<any>{
        let opts={
            user:userId,
            video:videoId
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/videovibe/videoviews/",{params:opts});
    }
    addVideoView(data:any):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/videovibe/videoviews", data);
    }
}
    
