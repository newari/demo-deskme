import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import { User } from "../models/user.model";

@Injectable()
export class TestMentService {
    constructor(private http: HttpClient) { }

    getPortalLink(): Observable<any> {
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/testment/portal-link");
    }
    getUserOnlineTests(userId, filter?:any): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/useronline/" + userId + "/tests", {params:filter});
    }
    getUserTestSeries(userId): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/useronline/" + userId + "/testseries");
    }
    getUserOfflineTests(): Observable<any> {
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/conventional-exam/user/offline-tests");
    }
    updateStudentResponse(id: string, data: any): Observable<any[]> {
        return this.http.put<any>(EdukitConfig.BASICS.API_URL + "/conventional-exam/test/user-response/" + id, data);
    }
    getUserReportTests(userId): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/report/" + userId + "/tests");
    }
    getUserReportOverview(uotId): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/report/overview/" + uotId);
    }
    getReportQuestionWise(data, qsetId): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/report/questionwise/" + qsetId, { params: data });
    }
    getReportTopicWise(data, qsetId): Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/report/topicwise/" + qsetId, { params: data });
    }
    getReportSolution(data, qsetId,fieldSet): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/report/solution/"+fieldSet+'/'+ qsetId, { params: data });
    }
    getCompareWithToppers(data): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/report/comparetopper", { params: data });
    }
    generateTestRank(data, userId): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/report/test/user-rank/" + userId, { params: data });
    }
    getReportDifficultLevel(data): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/report/difficultlevel", { params: data });
    }
    getTestParticipants(filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/testment/report/test/participants", opts);
    }
    getOverAllReport(data): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/student/testment/dashboard/overallreport",{params: data});
    }
    getTestSeriesSummary(userId): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/report/testseries/summary/"+userId);
    } 
    getUpComingTests(data): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/student/testment/dashboard/recommendedtests",{params: data});
    }

    // getUpTests(): Observable<any[]> {
    //     return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/student/testment/dashboard/overview'",{params: {user : '5981a83c82b5e3275df1db9c'}});
    // }
    getLastAttemptedTest(userId): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/student/testment/dashboard/lastattempted",{params: {user : userId}});
    }
    getTestmentDashboard(filter?:any): Observable<any> {
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/ims/testment/test/dashboard",{params: filter});
    }
    getDailyAttemptedTests(filter?:any): Observable<any> {
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/ims/testment/test/dashboard/dailytests",{params: filter});
    }
    getAssignedTestSeries(filter?:any,withHeaders?:any): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) opts.observe = 'response';
        return this.http.get<any>(EdukitConfig.BASICS.API_URL + "/ims/testment/dashboard/testseries",opts);
    }
    getUserTests(userId): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/ims/testment/student/"+userId);
    }
    getUserDetails(user): Observable<User> {
        return this.http.get<User>(EdukitConfig.BASICS.API_URL + "/core/user/"+user);
    }
    getTmStudents(filter?: any, withHeaders?: boolean): Observable<any> {
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL + "/testment/useronline/students",opts);
    }
    assignProductToMultiUser(data): Observable<User>{
        return this.http.post<User>(EdukitConfig.BASICS.API_URL + "/core/user/product/multiuser",data);
    }
    getFacultyTests(userId): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/useronline/" + userId + "/facultytests");
    }
    getTestAttempts(data): Observable<any[]> {
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/testment/useronlinetestattempt",{params : data});
    }
    getTestReportQuestionWise(data) : Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL + "/ims/testment/test/report/questionwise",{params : data});
    }
   
}