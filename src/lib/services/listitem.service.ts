import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import {Listitem} from "../models/listitem.model";
import { EdukitConfig } from "../../ezukit.config";
// import { Listitem } from "../ek-model/listitem.model";

@Injectable() 
export class ListitemService{
    private listitem: Listitem;
    constructor(private http:HttpClient){ }

    addListitem(listitem:Listitem) : Observable<Listitem>{
        return this.http.post<Listitem>(EdukitConfig.BASICS.API_URL+"/webber/webberlistitem", listitem);
    }

    getListitem(filter?: any, withHeaders?:boolean) :Observable<any>{
         let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/webberlistitem",opts);
    }

    getOneListitem(listitemId) :Observable<Listitem>{
        return this.http.get<Listitem>(EdukitConfig.BASICS.API_URL+"/webber/webberlistitem/"+listitemId);
    }

    updateListitem(listitemId, listitem:Listitem) : Observable<Listitem>{
        return this.http.put<Listitem>(EdukitConfig.BASICS.API_URL+"/webber/webberlistitem/"+listitemId, listitem);
    }

    deleteListitem(listitemId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/webberlistitem/"+listitemId);
    }
}
    