import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { EdukitConfig } from '../../ezukit.config';
import { HttpClient } from '@angular/common/http';
@Injectable()
export class SMSService {
    private SENDERID:string="EXAMPR";
    private route='transactional';
    protected smstype='normal';
    protected priority='ndnd';
    protected contacts:number[];
    protected username:string;
    protected password:string;
    protected apiURL:string;
    constructor(private http:HttpClient){
        
    }

    setProvider(){
        this.username="pess";
        this.password="newari@pess";
    }

    sendTransactionalSMS(mobile, msg): Observable<any>{
        this.setProvider();
        const reqBody={
            apiUrl:'http://api.mVaayoo.com/mvaayooapi/MessageCompose',
            apiPath: '/api/sendmsg.php?user='+this.username+'&pass='+this.password+'&sender='+this.SENDERID+'&phone='+mobile+'&text='+msg+'&priority='+this.priority+'&stype='+this.smstype,
            apiMethod:'POST',
            apiBody:{
                'user':'sunil.newari@gmail.com:newari@339',
                'senderID':'EDUKIT',
                'receipientno':mobile,
                'msgtxt':msg
            },
            apiHeaders:{},
        }
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/core/send-sms", reqBody);
    }


}