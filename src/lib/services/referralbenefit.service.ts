import { Injectable } from "@angular/core";
import "rxjs/Rx";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {ReferralBenefit} from "../models/referralbenefit.model";

@Injectable() 
export class ReferralBenefitService{
    private referralBenefit: ReferralBenefit;
    constructor(private http:HttpClient){ }

    addReferralBenefit(referralBenefit:ReferralBenefit) : Observable<ReferralBenefit>{
        return this.http.post<ReferralBenefit>(EdukitConfig.BASICS.API_URL+"/cmn/referralbenefit", referralBenefit);
    }

    getReferralBenefit(filter?:any, withHeaders?:boolean) :Observable<any>{

        let opts:any={params:filter};
        if(withHeaders)
        opts.observe="response";

        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/cmn/referralbenefit", opts);
    }

    getOneReferralBenefit(referralBenefitId) :Observable<ReferralBenefit>{
        return this.http.get<ReferralBenefit>(EdukitConfig.BASICS.API_URL+"/cmn/referralbenefit/"+referralBenefitId);
    }

    updateReferralBenefit(referralBenefitId, referralBenefit:ReferralBenefit) : Observable<ReferralBenefit>{
        return this.http.put<ReferralBenefit>(EdukitConfig.BASICS.API_URL+"/cmn/referralbenefit/"+referralBenefitId, referralBenefit);
    }

    deleteReferralBenefit(referralBenefitId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/cmn/referralbenefit/"+referralBenefitId);
    }
}
    