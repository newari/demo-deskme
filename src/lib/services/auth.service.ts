import { Injectable } from '@angular/core';
// import { Headers, Response, Http } from '@angular/http';
import { EdukitConfig } from '../../ezukit.config';
import { Subject } from 'rxjs';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
    authObservable :Subject<any>;

    constructor(private http:HttpClient, private router: Router){
        this.authObservable=new Subject();
     }

    login(data:any) : Observable<any>{
        let headrs=new HttpHeaders().set('withCredentials', 'true');
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/auth/signin", data, { withCredentials: true});
    }

    processForgotPass(email) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/auth/forgot-password", {email:email});
    }
    requestResetPasswordOTP(username) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/public/user/sent-reset-password-otp", {username:username});
    }
    resetPasswordRequest(data?:any):Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/public/user/reset-password",data);
    }

    loggedIn(){
        var token=window.localStorage.getItem("authToken");
        var user=window.localStorage.getItem("user");
        if(token&&token!='undefined'&&token!==null&&user&&user!='undefined'&&user!==null){
            return true;
        }
        return false;
    }

    logout(){
        window.localStorage.removeItem("authToken");
        // this.authObservable.next({loggedOut:true});
        //^For use in LoginWindow
        let urlArr=window.location.pathname.split("/");
        if(urlArr[1]&&urlArr[1]=="student"){
            this.router.navigate(["/student/auth/login"]);
            // let clientStr=window.localStorage.getItem("client");
            // let client;
            // if(clientStr){
            //     client=JSON.parse(clientStr);
            // }
            // if(client&&client.defaultLogoutPage){
            //     window.location.href=client.defaultLogoutPage
            // }else{
            //     this.router.navigate(["/student/auth/login"]);
            // }
        }else{
            this.router.navigate(["/student/auth/login"]);
        }
        return true;
    }

    user(){
        let user=window.localStorage.getItem("user");
        if(user){
            user=JSON.parse(user);
            return user;
        }
        return null;
    }

    admin(){
        let u=window.localStorage.getItem("user");
        if(u){
            var user=JSON.parse(u);
        }
        if(user&&(user.type=="ADMIN"||user.type=="STAFF")){
            return user;
        }
        return null;
    }

    faculty(){
        let u=window.localStorage.getItem("user");
        if(u){
            var user=JSON.parse(u);
        }
        if(user&&user.type=="FACULTY"){
            return user;
        }
        return null;
    }

    student(){
        let u=window.localStorage.getItem("user");
        if(u){
            var user=JSON.parse(u);
        }
        if(user&&user.type=="STUDENT"){
            return user;
        }
        return null;
    }
    studentInfo(){
        let std=window.localStorage.getItem("student");
        if(std){
            var student=JSON.parse(std);
            return student;
        }
        return null;
    }
    staff(){
        let u=window.localStorage.getItem("user");
        if(u){
            var user=JSON.parse(u);
        }
        if(user&&user.type=="STAFF"){
            return user;
        }
        return null;
    }

    getAuthToken(){
        return window.localStorage.getItem("authToken");
    }
    getDefaultSession(){
        return 'null';
    }
    getDefaultCenter(){
        return 'null';
    }
    getUserPerms(userPemrs, app, activity, reqPerms){
        let perms:any={};
        if(!userPemrs){
            return perms;
        }
        if(userPemrs[0]=="ALL"||userPemrs.all || userPemrs['ALL']){
            console.log("working");

            for(let i=0; i<reqPerms.length; i++){
                perms[reqPerms[i]]=true;
            }
        }
        console.log("===userpe==",userPemrs);
        console.log("=== = ===",perms);
        if(!userPemrs[app]){
            console.log("APP NOT FOUND");

            return perms;
        }
        if(!userPemrs[app][activity]){
            console.log("ACTIVITY NOT FOUND");
            return perms;
        }
        for(let i=0; i<reqPerms.length; i++){
            if(userPemrs[app][activity][reqPerms[i]]){
                perms[reqPerms[i]]=true;
            }
        }
        console.log("===",perms);
        return perms;
    }

    getDefaultParams(){
        let u=window.localStorage.getItem("user");
        if(u){
            let user=JSON.parse(u);
            return user.defaultParams;
        }else{
            return null;
        }
    }
}
