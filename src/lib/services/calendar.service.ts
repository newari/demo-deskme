import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class CalendarService{
    constructor(private http:HttpClient){ }

    downloadBatchGroupCalendar(filter?:any, withHeaders?:boolean) :Observable<any>{
        var from=null;
        var to=null;
        var batch=null;
        var subject=null;
        var user=null;
        var faculty=null;
        var status=null;
        var type=null;

        if(filter&&filter.from){
            var from=filter.from;
        }
        if(filter&&filter.to){
            var to=filter.to;
        }
        if(filter&&filter.subject){
            var subject=filter.subject;
        }
        if(filter&&filter.user){
            var user=filter.user;
        }
        if(filter&&filter.faculty){
            var faculty=filter.faculty;
        }

        if(filter&&filter.batch){
            var batch=filter.batch;
        }
        if(filter&&filter.status){
            var status=filter.status;
        }
        if(filter&&filter.type){
            var type=filter.type;
        }
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get(EdukitConfig.BASICS.API_URL+"/calendar/download-batch-group-schedule", opts);
    }

    shiftEvent(data:any):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/calendar/shift-event", data);
    }

    updateEventTiming(data:any):Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/calendar/event-timing", data);
    }

    mergeEvent(event1, event2):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/calendar/merge-event", {event1:event1, event2:event2});
    }

    startEvent(eventId, time):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/calendar/event/start/"+eventId, {eventId:eventId, time:time});
    }

    finishEvent(eventId, time, breakDuration):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/calendar/event/finish/"+eventId, {eventId:eventId, time:time, breakDuration:breakDuration});
    }

    
}
    
