import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
import {Testimonialcategory} from "../models/testimonialcategory.model";

@Injectable() 
export class TestimonialcategoryService{
    private testimonialcategory: Testimonialcategory;
    constructor(private http:HttpClient){ }

    addTestimonialcategory(testimonialcategory:Testimonialcategory) : Observable<Testimonialcategory>{
        return this.http.post<Testimonialcategory>(EdukitConfig.BASICS.API_URL+"/webber/testimonialcategory", testimonialcategory);
    }

    getTestimonialcategory(filter?: any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/testimonialcategory", opts);
    }

    getOneTestimonialcategory(testimonialcategoryId) :Observable<Testimonialcategory>{
        return this.http.get<Testimonialcategory>(EdukitConfig.BASICS.API_URL+"/webber/testimonialcategory/"+testimonialcategoryId);
    }

    updateTestimonialcategory(testimonialcategoryId, testimonialcategory:Testimonialcategory) : Observable<Testimonialcategory>{
        return this.http.put<Testimonialcategory>(EdukitConfig.BASICS.API_URL+"/webber/testimonialcategory/"+testimonialcategoryId, testimonialcategory);
    }

    deleteTestimonialcategory(testimonialcategoryId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/testimonialcategory/"+testimonialcategoryId);
    }
}
    