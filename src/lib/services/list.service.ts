import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import {List} from "../models/list.model";
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class ListService{
    private list: List;
    constructor(private http:HttpClient){ }

    addList(list:List) : Observable<List>{
        return this.http.post<List>(EdukitConfig.BASICS.API_URL+"/webber/webberlist", list);
    }

    getList(filter?: any, withHeaders?:boolean) :Observable<any>{
         let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/webber/webberlist", opts);
    }

    getOneList(listId) :Observable<List>{
        return this.http.get<List>(EdukitConfig.BASICS.API_URL+"/webber/webberlist/"+listId);
    }

    updateList(listId, list:List) : Observable<List>{
        return this.http.put<List>(EdukitConfig.BASICS.API_URL+"/webber/webberlist/"+listId, list);
    }

    deleteList(listId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/webberlist/"+listId);
    }
}
    