import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';
import { Contact } from '../models/contact.model';

@Injectable()
export class ContactService{
    constructor(private http:HttpClient){ }

    addContact(contact:any) : Observable<Contact>{
        return this.http.post<Contact>(EdukitConfig.BASICS.API_URL+"/notification/contact", contact);
    }

    getContact(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/notification/contact", opts);
    }

    getContactCount(filter?:any) :Observable<any>{
        let opts:any={params:filter};

        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/notification/contact/count", opts);
    }

    getOneContact(centerId) :Observable<Contact>{
        return this.http.get<Contact>(EdukitConfig.BASICS.API_URL+"/notification/contact/"+centerId);
    }

    updateContact(centerId, contact:any) : Observable<Contact>{
        return this.http.put<Contact>(EdukitConfig.BASICS.API_URL+"/notification/contact/"+centerId, contact);
    }
    refreshContactData(contactId:string) : Observable<Contact>{
        return this.http.put<Contact>(EdukitConfig.BASICS.API_URL+"/notification/contact/refresh", {contact:contactId});
    }
    deleteContact(centerId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/notification/contact/"+centerId);
    }
    sendEmail(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+ "/notification/contact/send-email", data);
    }
    sendSMS(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+ "/notification/contact/send-sms", data);
    }
    importContacts(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/notificaion/contact/import", data)
    }
    getContactWithAdvanceFilter(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/notification/contact/advance-filter", data, {observe:'response'})
    }
    exportContacts(data?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/notification/contact/export-contact", data);
    }
    sendVerificationEmail(contactId?:any): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/notification/contact/"+contactId+"/send-email-verification",{});
    }

    sendBulkEmail(data): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/notification/contact/send-bulk-email",data)
    }
    sendBulkSMS(data): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/notification/contact/send-bulk-sms",data)
    }


    // Send Android Push Notification to selected user
    sendAPNToOneUser(data): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/notification/contact/send-apn",data);
    }
    sendAPNToSelectedUser(data): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/notification/contact/send-apn-to-selected-contacts",data);
    }

    getProduct(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/cmn/product", opts);
    }
}

