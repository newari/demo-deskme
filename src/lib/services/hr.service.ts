import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
@Injectable() 
export class HrService{
    constructor(private http:HttpClient){ }

    getSummaryData(params) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/hr/summary", {params:params});
    }

}
    
