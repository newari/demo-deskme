import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";
@Injectable() 
export class SettingsService{
    constructor(private http:HttpClient){ }

    addSettings(data) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/admin/settings", data);
    }
    getSettingByKey(key) :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/admin/settings-by-key/"+key);
    }
    getSettings(filter?:any) :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/admin/settings", {params:filter});
    }

    updateSettings(settingKey, data) : Observable<any>{
        return this.http.put(EdukitConfig.BASICS.API_URL+"/admin/settings/"+settingKey, data);
    }


}
    
