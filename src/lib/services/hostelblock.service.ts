import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import { HttpClient } from '@angular/common/http';
import {Hostelblock} from "../models/hostelblock.model";
import { EdukitConfig } from "../../../src/ezukit.config";

@Injectable() 
export class HostelblockService{
    private hostelblock: Hostelblock;
    constructor(private http:HttpClient){ }

    addHostelblock(hostelblock:Hostelblock) : Observable<Hostelblock>{
        return this.http.post<Hostelblock>(EdukitConfig.BASICS.API_URL+"/hostel/hostelblock", hostelblock);
    }

    getHostelblock(filter?:any) :Observable<Hostelblock[]>{
        return this.http.get<Hostelblock[]>(EdukitConfig.BASICS.API_URL+"/hostel/hostelblock", {params:filter});
    }

    getOneHostelblock(hostelblockId) :Observable<Hostelblock>{
        return this.http.get<Hostelblock>(EdukitConfig.BASICS.API_URL+"/hostel/hostelblock/"+hostelblockId);
    }

    updateHostelblock(hostelblockId, hostelblock:Hostelblock) : Observable<Hostelblock>{
        return this.http.put<Hostelblock>(EdukitConfig.BASICS.API_URL+"/hostel/hostelblock/"+hostelblockId, hostelblock);
    }

    deleteHostelblock(hostelblockId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/hostel/hostelblock/"+hostelblockId);
    }
}
    