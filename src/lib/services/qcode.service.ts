import { Injectable } from "@angular/core";

import "rxjs/Rx";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { EdukitConfig } from "../../ezukit.config";
import { QalphaCode } from "../models/qalphacode.model";
import { QbetaCode } from "../models/qbetacode.model";
import { QgammaCode } from "../models/qgammacode.model";
@Injectable()
export class QcodeService{
    constructor(private http:HttpClient){ }
    getAllCodes(data) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/allcodes",{params : data});
    }
    // ----------------------------- Alpha Methods --------------------------------------
    addAlpha(data) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/qbank/qalphacode",data);
    }
    getAlpha(alphaFilter,withHeaders : boolean):Observable<any>{
        let filter: any = { params: alphaFilter };
        if (withHeaders) {
            filter.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/qalphacode/getalpha",filter);
    }
    getOneAlpha(alphaCodeId) :Observable<QalphaCode>{
        return this.http.get<QalphaCode>(EdukitConfig.BASICS.API_URL+"/qbank/qalphacode/"+alphaCodeId);
    }
    updateAlpha(questionId, data : QalphaCode) : Observable<QalphaCode>{
        return this.http.put<QalphaCode>(EdukitConfig.BASICS.API_URL+"/qbank/qalphacode/"+questionId, data);
    }
    deleteAlpha(alphaCodeId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/qbank/qalphacode/"+alphaCodeId);
    }

    // ---------------------------------- Beta Methods -----------------------------------------------

    addBeta(data) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/qbank/qbetacode",data);
    }
    getBeta(betaFilter,withHeaders : boolean):Observable<any>{
        let filter: any = { params: betaFilter };
        if (withHeaders) {
            filter.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/qbetacode/getbeta",filter);
    }
    getOneBeta(betaCodeId) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/qbetacode/"+betaCodeId);
    }
    updateBeta(questionId, data : QbetaCode) : Observable<QbetaCode>{
        return this.http.put<QbetaCode>(EdukitConfig.BASICS.API_URL+"/qbank/qbetacode/"+questionId, data);
    }
    deleteBeta(betaCodeId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/qbank/qbetacode/"+betaCodeId);
    }
    // ---------------------------------- Gamma Methods -----------------------------------------------

    addGamma(data) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/qbank/qgammacode",data);
    }
    getGamma(gammaFilter,withHeaders : boolean):Observable<any>{
        let filter: any = { params: gammaFilter };
        if (withHeaders) {
            filter.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/qbank/qgammacode/getgamma",filter);
    }
    getOneGamma(gammaCodeId) :Observable<QgammaCode>{
        return this.http.get<QgammaCode>(EdukitConfig.BASICS.API_URL+"/qbank/qgammacode/"+gammaCodeId);
    }
    updateGamma(questionId, data : QgammaCode) : Observable<QgammaCode>{
        return this.http.put<QgammaCode>(EdukitConfig.BASICS.API_URL+"/qbank/qgammacode/"+questionId, data);
    }
    deleteGamma(gammaCodeId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/qbank/qgammacode/"+gammaCodeId);
    }
}
