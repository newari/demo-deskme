import { Injectable } from "@angular/core";
import { Headers, Response, BaseRequestOptions, RequestOptions } from '@angular/http';
import "rxjs";
import { Observable } from "rxjs";

import { HttpClient } from '@angular/common/http';
import { Faculty } from "../models/faculty.model";
import { EdukitConfig } from "../../ezukit.config";
import { Event } from "../models/event.model";

@Injectable()
export class FacultyService{
    private faculty: Faculty;
    constructor(private http:HttpClient){ }

    addFaculty(faculty:any) : Observable<Faculty>{
        return this.http.post<Faculty>(EdukitConfig.BASICS.API_URL+"/admin/faculty", faculty);
    }

    getFaculty(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe= 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/admin/faculty", opts);
    }

    getOneFaculty(facultyId) :Observable<Faculty>{
        return this.http.get<Faculty>(EdukitConfig.BASICS.API_URL+"/admin/faculty/"+facultyId);
    }

    updateFaculty(facultyId, faculty) : Observable<Faculty>{
        return this.http.put<Faculty>(EdukitConfig.BASICS.API_URL+"/admin/faculty/"+facultyId, faculty);
    }

    deleteFaculty(facultyId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/admin/faculty/"+facultyId);
    }

    getSubjectFaculty(subjectId) :Observable<Faculty>{
        return this.http.get<Faculty>(EdukitConfig.BASICS.API_URL+"/admin/faculty/ofsubject/"+subjectId);
    }

    getFacultyScheduledBatch(facultyId) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/faculty/member/"+facultyId+"/scheduled-batch");
    }

    getFacultyFinishedHours(facultyId, fltrData) :Observable<Event[]>{
        return this.http.get<Event[]>(EdukitConfig.BASICS.API_URL+"/faculty/member/"+facultyId+"/finished-hours", {params:fltrData});
    }

    getBatchWithSubject(facultyId) :Observable<Event[]>{
        return this.http.get<Event[]>(EdukitConfig.BASICS.API_URL+"/faculty/member/"+facultyId+"/batch-subject");
    }

    createBatchFeed(data,spaceId): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/space/"+spaceId+"/create-feed",data);
    }
    getSharedWith(sourceId?:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/space/"+sourceId+"/shared-with");
    }

    getFacultyBatchArticles(batchId?:any, filter?:any,withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders)
        opts.observe='response';

        return this.http.get(EdukitConfig.BASICS.API_URL+"/space/"+batchId+"/articles",opts);
    }

    getFacultyBatchNotes(batchId?:any, filter?:any,withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders)
        opts.observe='response';

        return this.http.get(EdukitConfig.BASICS.API_URL+"/space/"+batchId+"/notes",opts);
    }

    getFacultyBatchAssignments(batchId?:any, filter?:any,withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders)
        opts.observe='response';

        return this.http.get(EdukitConfig.BASICS.API_URL+"/space/"+batchId+"/assignments",opts);
    }

    getFacultyBatchQuizzs(batchId?:any, filter?:any,withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders)
        opts.observe='response';

        return this.http.get(EdukitConfig.BASICS.API_URL+"/space/"+batchId+"/quizzes",opts);
    }

    getFacultyBatchForumQs(batchId?:any, filter?:any,withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders)
        opts.observe='response';

        return this.http.get(EdukitConfig.BASICS.API_URL+"/space/"+batchId+"/forum",opts);
    }
    addBatchToFaculty(facultyId, data): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+`/faculty/member/${facultyId}/add-batch`,data);
    }

    getFacultyBatches(filter?:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/faculty/member/batches", {params:filter});
    }
    removeFacultyBatch(id): Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/faculty/member/"+id+"/remove-faculty-batch");
    }
}

