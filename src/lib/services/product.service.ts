import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";

import {Product} from "../models/product.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../ezukit.config';
import { ProductPaymentOption } from '../models/payment-option.model';

@Injectable()
export class ProductService{
    private product: Product;
    constructor(private http:HttpClient){ }

    addProduct(product:any) : Observable<Product>{
        return this.http.post<Product>(EdukitConfig.BASICS.API_URL+"/cmn/product", product);
    }
    getProduct(filter?:any, withHeaders?:boolean) :Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/cmn/product", opts);
    }
    searchProduct(query:any) :Observable<Product[]>{
        return this.http.get<Product[]>(EdukitConfig.BASICS.API_URL+"/cmn/product/search", {params:query});
    }
    getOneProduct(productId, filter?:any) :Observable<Product>{
        return this.http.get<Product>(EdukitConfig.BASICS.API_URL+"/cmn/product/"+productId, {params:filter});
    }

    updateProduct(productId, product:any) : Observable<Product>{
        return this.http.put<Product>(EdukitConfig.BASICS.API_URL+"/cmn/product/"+productId, product);
    }

    deleteProduct(productId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/cmn/product/"+productId);
    }


    addStock(productId, data:any) : Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/cmn/product/add-stock/"+productId, data);
    }
    getStockHistory(productId, filter?:any) : Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/cmn/product/stock-history/"+productId, {params:filter});
    }
    getCenters(filter?:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/centers", {params:filter});
    }

    getCourses(filter?:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/courses", {params:filter});
    }
    getAllCourses(filter?:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/all-courses", {params:filter});
    }
    getBoards(filter?:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/boards", {params:filter});
    }
    getPrograms(filter?:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/programs", {params:filter});
    }
    getProductCategories(filter?:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/product-categories", {params:filter});
    }
    getLiveProductsCategories(filter?:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/get-live-product-categies", {params:filter});
    }
    getProductTypes(filter?:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/product-types", {params:filter});
    }

    getStreams(filter?:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/streams", {params:filter});
    }
    getAllStreams(filter?:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/all-stream", {params:filter});
    }
    getSessions(filter?:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/sessions", {params:filter});
    }
    getProducts(filter?:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/products", {params:filter});
    }
    getBatches(filter?:any) : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/data/batches", {params:filter});
    }
    getProductComboItems(productId:string) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/product/"+productId+"/combo-items")
    }

    // Date fetched from Orders
    getEnrolledUsers(productId:string, filter?:any, withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/cmn/product/"+productId+"/enrolled-users", opts)
    }

    // data fetched from UserProduct
    getEnrolledProductUsers(productId:string, filter?:any, withHeaders?:boolean): Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/cmn/product/"+productId+"/enrolled-product-users", opts)
    }
    addUserProduct(productId, data):Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/cmn/product/"+productId+"/enrolled-users/add-product", data);
    }

    getProductPaymentOptions(productId:string) : Observable<ProductPaymentOption[]>{
        return this.http.get<ProductPaymentOption[]>(EdukitConfig.BASICS.API_URL+"/public/data/product/"+productId+"/payment-options")
    }
    addProductPaymentOption(productId:string, data:ProductPaymentOption) : Observable<ProductPaymentOption>{
        return this.http.post<ProductPaymentOption>(EdukitConfig.BASICS.API_URL+"/public/data/product/"+productId+"/payment-options", data)
    }
    updateProductPaymentOpt(productId:string,pmtOptId:string, data:ProductPaymentOption) : Observable<ProductPaymentOption>{
        return this.http.put<ProductPaymentOption>(EdukitConfig.BASICS.API_URL+"/public/data/product/"+productId+"/payment-options/"+pmtOptId, data);
    }
    deleteProductPaymentOption(productId:string, optId:string) : Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/public/data/product/"+productId+"/payment-options/"+optId)
    }
    getProgramCount():Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/count");
    }
    createDupProduct(data?:any):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL + "/cmn/product/create-duplicate", data);
    }
    getComboProducts(cProductIds?:any):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL + "/cmn/product/combo",{params:{comboProducts:cProductIds}});
    }

    getSourceIdTS(ids?:any):Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL + "/cmn/product/sourceId/ts",{params:{testSeries:ids}});
    }
    deleteProductBatch(productId?:any, batchId?:any):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/cmn/product/remove-batch/"+productId+"/"+batchId)
    }
    getCourseCreatorDashBoardData(): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/cmn/product/get-course-creator-db-count");
    }
    exportEnrolledUsers(productId:string): Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/cmn/product/"+productId+"/enrolled-users/export")
    }
    getOneProductInfo(productId:string): Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/cmn/product/info/"+productId)
    }

    addUserProductToEnrolledUsers(productId, data):Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/cmn/product/"+productId+"/enrolled-product-users/add-product", data);
    }
}

