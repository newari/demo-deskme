import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Slider} from "../models/slider.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable() 
export class SliderService{
    private slider: Slider;
    constructor(private http:HttpClient){ }

    addSlider(slider:any) : Observable<Slider>{
        return this.http.post<Slider>(EdukitConfig.BASICS.API_URL+"/cmn/slider", slider);
    }

    getSlider(filter?: any, withHeaders?:boolean) :Observable<any>{
        let opts: any = { params: filter };
        if (withHeaders) {
            opts.observe = 'response'
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/cmn/slider", opts);
    }

    getOneSlider(sliderId) :Observable<Slider>{
        return this.http.get<Slider>(EdukitConfig.BASICS.API_URL+"/cmn/slider/"+sliderId);
    }

    updateSlider(sliderId, slider:Slider) : Observable<Slider>{
        return this.http.put<Slider>(EdukitConfig.BASICS.API_URL+"/cmn/slider/"+sliderId, slider);
    }

    deleteSlider(sliderId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/cmn/slider/"+sliderId);
    }

    addSlide(slide) : Observable<any>{

        return this.http.post(EdukitConfig.BASICS.API_URL+"/cmn/slider/add-slide", slide);
    }

    updateSlides(sliderId, slides) : Observable<any>{
        let data={sliderId:sliderId, slides:slides};
        return this.http.post(EdukitConfig.BASICS.API_URL+"/cmn/slider/update-slides", data);
    }
}
    
