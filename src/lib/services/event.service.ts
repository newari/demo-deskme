import { Injectable } from "@angular/core";
import "rxjs";
import { Observable } from "rxjs";


import {Event} from "../models/event.model";
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class EventService{
    private event: Event;
    constructor(private http:HttpClient){ }

    addEvent(event:Event) : Observable<Event>{
        return this.http.post<Event>(EdukitConfig.BASICS.API_URL+"/calendar/event", event);
    }

    addEvents(event:Event[], batchToMerge?:string) : Observable<Event>{
        return this.http.post<Event>(EdukitConfig.BASICS.API_URL+"/calendar/events", event);
    }

    getEventStats(fltr) :Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/calendar/event/status-stats", fltr);
    }

    getEvent() :Observable<Event[]>{
        return this.http.get<Event[]>(EdukitConfig.BASICS.API_URL+"/calendar/event");
    }

    getEvents(filter?) :Observable<Event[]>{
        var from=null;
            var to=null;
            var batch=null;
            var subject=null;
            var user=null;
            var faculty=null;
            var status=null;
            var type=null;

        if(filter&&filter.from){
            var from=filter.from;
        }
        if(filter&&filter.to){
            var to=filter.to;
        }
        if(filter&&filter.subject){
            var subject=filter.subject;
        }
        if(filter&&filter.user){
            var user=filter.user;
        }
        if(filter&&filter.faculty){
            var faculty=filter.faculty;
        }

        if(filter&&filter.batch){
            var batch=filter.batch;
        }
        if(filter&&filter.status){
            var status=filter.status;
        }
        if(filter&&filter.type){
            var type=filter.type;
        }
        return this.http.get<Event[]>(EdukitConfig.BASICS.API_URL+"/calendar/events?from="+from+"&to="+to+"&batch="+batch+"&user="+user+"&faculty="+faculty+"&type="+type+"&status="+status+"&subject="+subject);
    }

    loadEvent(from, to, batch?, subject?, user?) :Observable<Event[]>{
        return this.http.get<Event[]>(EdukitConfig.BASICS.API_URL+"/calendar/event/filter?from="+from+"&to="+to+"&batch="+batch+"&user="+user+"&subject="+subject);
    }

    getOneEvent(eventId) :Observable<Event>{
        return this.http.get<Event>(EdukitConfig.BASICS.API_URL+"/calendar/event/"+eventId);
    }

    updateEvent(eventId, event:Event) : Observable<Event>{
        return this.http.put<Event>(EdukitConfig.BASICS.API_URL+"/calendar/event/"+eventId, event);
    }

    mergeBatchEvent(eventId, batch:string) : Observable<Event>{
        return this.http.put<Event>(EdukitConfig.BASICS.API_URL+"/calendar/event/"+eventId+"/merge", batch);
    }

    mergeBatchEvents(data:any) : Observable<Event>{
        return this.http.put<Event>(EdukitConfig.BASICS.API_URL+"/calendar/event/merge-in-batch", data);
    }

    updateEventFinishTiming(eventId, data) : Observable<Event>{
        return this.http.put<Event>(EdukitConfig.BASICS.API_URL+"/calendar/event/finish-data/"+eventId, data);
    }

    deleteEvent(eventId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/calendar/event/"+eventId);
    }
    getBatchEvent(batchId?:any, from?:any, to?:any, faculty?:any, subject?:any, status?:any) :Observable<Event[]>{
        return this.http.get<Event[]>(EdukitConfig.BASICS.API_URL+"/calendar/event/batch/"+batchId+"?from="+from+"&to="+to+"&faculty="+faculty+"&subject="+subject+"&status="+status);
    }

    getFacultyEvent(facultyId) :Observable<Event[]>{
        return this.http.get<Event[]>(EdukitConfig.BASICS.API_URL+"/calendar/event/faculty/"+facultyId);
    }
    getUserEvent(userId) :Observable<Event[]>{
        return this.http.get<Event[]>(EdukitConfig.BASICS.API_URL+"/calendar/event/user/"+userId);
    }
    getBatchSubjectEvent(batchId, subjectId) :Observable<Event[]>{
        return this.http.get<Event[]>(EdukitConfig.BASICS.API_URL+"/calendar/event/batchsubject/"+batchId+"/"+subjectId);
    }
    getClassroomEvent(crId) :Observable<Event[]>{
        return this.http.get<Event[]>(EdukitConfig.BASICS.API_URL+"/calendar/event/classroom/"+crId);
    }
    getBatchScheduledFaculties(batchId):Observable<any[]>{
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/calendar/event/batchfaculty/"+batchId);
    }
    startEvent(eventId, time):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/calendar/event/start/"+eventId, {eventId:eventId, time:time});
    }

    finishEvent(eventId, time, breakDuration):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/calendar/event/finish/"+eventId, {eventId:eventId, time:time, breakDuration:breakDuration});
    }

    startBreak(eventId, time):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/calendar/event/start-break/"+eventId, {eventId:eventId, time:time});
    }

    finishBreak(eventId, time, breakDuration):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/calendar/event/finish-break/"+eventId, {eventId:eventId, time:time, breakDuration:breakDuration});
    }

    askAsigneeApproval(eventIds):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/calendar/event/ask-asignee-approval", {eventIds:eventIds});
    }

    deleteMultiEvent(eventIds):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/calendar/event/multi-delete", {eventIds:eventIds});
    }

    changeStatus(eventId, newStatus):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/calendar/event/change-status", {eventId:eventId, status:newStatus});
    }

    changeStatusMulti(eventIds, newStatus, notify):Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/calendar/event/change-status-multi", {eventIds:eventIds, status:newStatus, notify:notify});
    }


}

