import { Injectable } from "@angular/core";

import "rxjs/Rx";
import { Observable } from "rxjs";


import {Freenotecategory} from "../models/freenotecategory.model";
import { HttpClient } from "@angular/common/http";
import { EdukitConfig } from "../../ezukit.config";

@Injectable()
export class FreenotecategoryService{
    private freenotecategory: Freenotecategory;
    constructor(private http:HttpClient){ }

    addFreenotecategory(freenotecategory:Freenotecategory) : Observable<Freenotecategory>{
        return this.http.post<Freenotecategory>(EdukitConfig.BASICS.API_URL+"/webber/notecategory", freenotecategory);
    }

    getFreenotecategory(filter?:any) :Observable<Freenotecategory[]>{
        return this.http.get<Freenotecategory[]>(EdukitConfig.BASICS.API_URL+"/webber/notecategory", {params:filter});
            ;
    }

    getOneFreenotecategory(freenotecategoryId) :Observable<Freenotecategory>{
        return this.http.get<Freenotecategory>(EdukitConfig.BASICS.API_URL+"/webber/notecategory/"+freenotecategoryId);
    }

    updateFreenotecategory(freenotecategoryId, freenotecategory:Freenotecategory) : Observable<Freenotecategory>{
        return this.http.put<Freenotecategory>(EdukitConfig.BASICS.API_URL+"/webber/notecategory/"+freenotecategoryId, freenotecategory);
    }

    deleteFreenotecategory(freenotecategoryId):Observable<any>{
        return this.http.delete(EdukitConfig.BASICS.API_URL+"/webber/notecategory/"+freenotecategoryId);
    }

    uploadPutFile(url:string, fileObj:any) : Observable<any>{

        return this.http.put<any>(url, fileObj);
    }
}
