export class Omrsheet {
    constructor(
        public id:string,
        public title:string,
        public totalQuestion:string,
        public totalSection:string,
        public questions: any[],
        public srnCount:string,
        public status:boolean
    ){}
      
}
