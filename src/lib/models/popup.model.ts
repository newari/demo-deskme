export class Popup {
  constructor(
      public id:string,
      public displayOn:string,
      public position:string,
      public image:string,
      public text:string,
      public link:string,
      public status:boolean){ }
}
