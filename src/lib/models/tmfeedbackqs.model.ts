export class TmFeedbackQs  {
    constructor(
        public id:string,
        public subject:any,
        public type : string,
        public testSeries : any,
        public test : any,
        public question : any,
        public stream : any,
        public response : string,
        public isResponsed : boolean,
        public qSeq : number,
        public status:boolean,
        public repliedAt : any,
        public repliedBy : any
      ){ }
  }