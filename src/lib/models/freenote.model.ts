export class Freenote {
  constructor(
      public id:string,
      public title:string,
      public type:string,
      public category:any,
      public stream:any,
      public course:any,
      public validTill:any,
      public client:any,
      public thumb:any,
      public link:string,
      public status:boolean,
      public customParams:any,
      public needCustomParam?:boolean
      ){ }
}
