export class Passage{
    constructor(
    public id :string,
    public title:string, // Optional
    public passageContent:string, 
    public status:boolean
    
    ){}
}