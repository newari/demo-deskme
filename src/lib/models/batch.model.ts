import { Curriculum } from './curriculum.model';
import { Time } from '@angular/common';
export class Batch {
  constructor(
      public id:string,
      public name:string,
      public code:string,
      public daySlots:Object,
      public dateSlots:Object,
      public offDays:string[],
      public offDates:string[],
      public status:boolean,
      public faculties:any[],
      public totalSeats:number,
      public filledSeats:number,
      public curriculum?: any,
      public idCardTemplate?: string,
      public session?:any,
      public center?:any,
      public idCardColor?:string,
      public admissionStatus?:boolean,
      public classEntryMinFee?:any,
      public course?: any,
      public stream?: any,
      public startTime?: Time,
      public endTime?:Time,
      public startDate?:string,
      public endDate?:string){ }
}
