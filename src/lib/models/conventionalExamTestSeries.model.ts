export class ConventionalExamTestSeries {
    constructor(
        public id:string,
        public title:string,
        public alias:string,
        public tests:any,
        public isEvaluatedVideo : boolean,
        public isQCAB : boolean,
        public totalTests:number,
        public status:boolean,
    ){}
}