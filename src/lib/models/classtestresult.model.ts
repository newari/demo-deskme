export class Classtestresult {
  constructor(
      public id:string,
      public correctQs:number,
      public inCorrectQs:number,
      public maxScore?:number,
      public stdScore?:number,
      public totalQs?:number,
      public rank?:number,
      public user?:any,
      public test?:any,
      public testSeries?:any,
      public product?:any,
      public percentage?:number,
      public qwise?:any,
      public sectionScore?:any,//name:{score,correctQs,incorrectQs,totalQs}

      ){ }
}
