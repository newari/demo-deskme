export class Option {
  constructor(
      public id:string,
      public name:string,
      public value:string, //array[{ "value": "GATE", "valueAlias": "GATE", "category": "Engineering" }]
      public valueAlias:string, //array[{ "value": "GATE", "valueAlias": "GATE", "category": "Engineering" }]
      public valueCode:string,  
      public isChild?:boolean,  
      public childOf?:any,  
      public more?:any,  
    ){ }
}
