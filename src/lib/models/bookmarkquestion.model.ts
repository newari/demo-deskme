export class BookmarkQuestion {
    constructor(
        public id: string,
        public question: any,
        public user: any,
        public test: any,
    ) { }
}