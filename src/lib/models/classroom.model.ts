export class Classroom {
    constructor(
        public id:string,
        public name:string,
        public status:boolean,
        public coordinator?:any,
        public landmark?:string,
        public city?:string,
        public address?: string,
        public lat?:string,
        public lon?:string){ }
}
