export class BacthSeat {
    constructor(
        public id: string,
        public title: string,
        public status: boolean,
        public bacth?:any,
        public admissionStatus?:any
    ) { }
}