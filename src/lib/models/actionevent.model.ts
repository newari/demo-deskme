export class Actionevent {
  constructor(
      public id:string,
      public handler:string,
      public activity:string,
      public value:any,
      public valueAlias:any, //array[{ "value": "GATE", "valueAlias": "GATE", "category": "Engineering" }]
      public tags:string[],
      public status:boolean,
      public params?:any,
      public more?:any){ }
}
