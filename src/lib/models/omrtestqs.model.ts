export class OmrtestQs {

    constructor(

        public id: string,
        public test: any,
        public answer: string,
        public questionType: string,
        public sectionIndex: number,
        public questionIndex: number,
        public markP: number,
        public markN: number
    ){}
}