export class ReferralBenefit {
    constructor(
        public id:string,
        public title:string,
        public referrerBenefit?:any,
        public referralBenefit?:any,
        public perUserLimit?:number,
        public client?:any,
        public status?:boolean,
        public validity?:any){ }
  }