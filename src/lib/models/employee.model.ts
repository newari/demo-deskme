export class Employee {
    constructor(
        public id:string,
        public name:string,
        public user:any,
        public designation:any,
        public jobRole:string,
        public status:boolean,
        public phone?:number,
        public description?:string
        ){ }
  }
  