export class Slider {
  constructor(
      public id:string,
      public title:string,
      public status:boolean,
      public slides?:any[]
      ){ }
}
