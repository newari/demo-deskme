export class Course {
  constructor(
      public id:string,
      public title:string,
      public name?:string,
      public code?:string,
      public eduStandard?:any,
      public insty?:string,
      public status?:boolean
      ){ }
}
