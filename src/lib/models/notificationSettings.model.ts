export class NotificationSettings {
    constructor(
        webpushProvider?:any,
        webpushCredentials?:any,
        smsProvider?: any,
        smsCredentials?: any,
        emailProvider?: any,
        emailCredentials?: any,
        androidPushProvider?: any,
        androidPushCredentials?: any,
        iosPushProvider?: any,
        iosPushCredentials?: any,
        status?:boolean
    ) {
        
    }
}