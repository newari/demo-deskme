export class Feedback {
  constructor(
      public id:string,
      public title:string,
      public totalQs:number,
      public status:boolean,
      public fbqs?:any
    ){ }
}
