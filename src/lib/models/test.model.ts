export class Test {
    constructor(
        public id: string,
        public title: string,
        public alias: string,
        public testType: string,
        public description: string,
        public totalQs: number,
        public duration: number,
        public course: any,
        public stream: any,
        public sections: any,
        public startDate: Date,
        public theme: string,
        public resultDate : Date,
        public endDate: Date,
        public maxScore: any,
        public defaultLang: string,
        public status: boolean,
        public instruction:any,
        public isCalculator:boolean,
        public oneTimeSubmission : boolean,
        public preDiscussionVideos : any,
        public solVideos : any,
        public totalQuestionLevel:any,
        
    ) { }
}