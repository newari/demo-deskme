export class Ebookcategory {
  constructor(
      public id:string,
      public title:string,
      public status:boolean,
      public parent?:string
      ){ }
}
