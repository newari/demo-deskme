export class ClientConfig {
    constructor(
        public title: string,
        public logo:any,
        public studentZoneTheme:string,
        public defaultLogoutPage?:string,
        public isESSeller? : boolean,
        public isESBuyer? : boolean,
        public favicon?:any
    ) { }
}
