export class EduStandard {
  constructor(
      public id:string,
      public name:string,
      public code:string,
      public status:boolean){ }
}
