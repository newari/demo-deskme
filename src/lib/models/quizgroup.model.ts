export class QuizGroup {
    constructor (
        public title : string,
        public alias : string,
        public status : boolean
    ){}
};