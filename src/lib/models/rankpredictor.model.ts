export class Rankpredictor {
  constructor(
      public id:string,
      public title:string,
      public stream:any,
      public course:any,
      public examSession:string,
      public session:any,
      public status:boolean){ }
}
