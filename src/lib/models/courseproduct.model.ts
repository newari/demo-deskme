export class CourseProduct {
  constructor(
      public id:string,
      public title:string,
      public status:boolean){ }
}
