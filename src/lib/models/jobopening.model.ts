export class Jobopening {
  constructor(
      public id:string,
      public title:string,
      public designation:any,
      public location:any,
      public status:boolean,
      public ctc?:string,
      public description?:string,
      public minExperience?:string,  //in year
      public stream?:string,
      public cv?:string,
      public videoCv?:string,
      public qualification?:string,
      public collegeType?:string,
      public collegeName?:string,
      public experience?:string,
      public jobType?:string,
      public availability?:string,
      public nfEmail?:string,
      public nfMobile?:string
    ){ }
}
