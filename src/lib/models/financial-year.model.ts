export class FinancialYear {
    constructor(
        public id:string,
        public title:string,
        public startDate:any,
        public endDate:any,
        public status:boolean,
        public isDefault?:boolean
        
    ){ }
    }
    