export class Coursetype {
  constructor(
              public id:number,
              public name:string,
              public email:string,
              public status:boolean,
              public mobile?:string,
              public landmark?:string,
              public city?:string,
              public address?: string,
              public lat?:string,
              public lon?:string){ }
}
