export class Subject {
  constructor(
              public name:string,
              public status:boolean,
              public id?:string,
              public stream?:any,
              public faculties?:Object[]){ }
}
