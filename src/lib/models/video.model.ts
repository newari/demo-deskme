export class Video {
  constructor(
      public id:string,
      public title:string,
      public status:boolean,
      public course?:any,
      public stream?:any,
      public videoSourceId?:any
    ){ }
}
