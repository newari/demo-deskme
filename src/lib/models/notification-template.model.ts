export class NotificationTemplate {
    constructor(
        public id:string,
        public title?:string,
        public subject?:string,
        public message?:string,
      ){ }
  }
  