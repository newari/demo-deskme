export class Forumquestion {
  constructor(
      public id:string,
      public user:any,
      public type:string,
      public title:string,
      public content:string,
      public status:boolean,
      public stream?:any,
      public attachments?:string[],
      public subject?:any,
      public group?:any,
      public createdAt?:any,
      public topic?:any,
      public tags?:any,
      public category?:any,
      public officiallyReplied?:boolean,
      public totalReplies?:number,
      public thread?:any,
      public typeParams?:any,
      public uri?:string,
      ){ }
}
