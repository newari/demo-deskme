export class Post {
  constructor(
      public id:string,
      public title:string,
      public description:string,
      public createdAt:string,
      public postCategories?:any,
      public featureImg?:string,
      public content?:string,
      public tags?:any,
      public uri?:string,
      public status?:boolean,
      public isDisqusEnbl?:boolean,
      public publishedAt?:any,
      public isPublished?:boolean,
      public thread?:string,
      public replies?:any[],
      public attachments?:any[],
      public stream?:any,
      public topic?:any,
      public subject?:any,
      public user?:any,
      public totalReplies?:number
      ){ }
}
