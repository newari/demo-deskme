export class Contact {
    constructor(
        public id?:string,
        public firstName?:string,
        public lastName?:string,
        public email?:string,
        public mobile?:string,
        public emailVerified?:boolean,
        public mobileVerified?:boolean,
        public meta?:any,
        public user?:any,
        public tags?:any,
        public client?:string,
        public fcmToken?:string,
        public status?:boolean,
        public androidApps?:any){ }
  }
