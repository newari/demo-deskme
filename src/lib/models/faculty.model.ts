export class Faculty {
  constructor(public id:string,
              public name:string,
              public lastName : string,
              public userDetails:any,
              public status:boolean,
              public createdAt:any,
              public mobile?:string,
              public daySlots?:Object,
              public dateSlots?:Object,
              public offDays?:string[],
              public offDates?:string[],
              public subjects?:any[],
              public batches?:any[],
              public city?:string,
              public address?: string,
              public lat?:string,
              public lon?:string,
              public course?:any[],
              public stream?:any[],
              public profileImage?:any
            ){ }
}
