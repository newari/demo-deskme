export class Blogsubscriber {
  constructor(
      public id:string,
      public name:string,
      public email:string,
      public status:boolean,
      public subscribed?:boolean,
      public blog?:any,
      public client?:any
      ){ }
}
