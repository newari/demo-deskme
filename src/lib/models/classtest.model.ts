export class Classtest {
  constructor(
      public id:string,
      public title:string,
      public status:boolean,
       public maxScore:number,
       public timeDuration:number,
       public totalQs:number,
       public type:string,
       public date:Date,
       public sections?:any,
       public testSeries?:any,
       public rankStrategy?:any
    ){ }
}
