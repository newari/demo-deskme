export class Offer {
  constructor(
      public id:string,
      public title:string,
      public image:string,
      public link:string,
      public createdBy:any,
      public status:boolean){ }
}
