export class Qset {
  constructor(
    public id: string,
    public title: string,
    public course: any,
    public stream: any,
    public paperType: any,
		public	type:any,
		public	description:string,
		public	duration:any,
		public	totalMarks:any,
		public	defaultLang:string,
		public	totalQs:any,
    public createdBy: any,
    public assignedTo: any,
    public assignedBy: any,
    public oShip : any,
    public customParam : any,
    public forSell : boolean,
		public cost : any,
		public mrp : any,
		public discount : any,
    public isPublished : boolean,
    public status: boolean
    ) { }
}