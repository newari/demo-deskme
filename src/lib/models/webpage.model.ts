export class Webpage {
  constructor(
      public id:string,
      public title:string,
      public name:string,
      public description:string,
      public uri:string,
      public featureImg:string,
      public viewFile:string,
      public webfront:string,
      public website:any,
      public status:boolean,
      public client:any,
      public metaTags?:any,
      public customParams?:any
    ){ }
}
