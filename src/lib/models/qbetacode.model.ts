export class QbetaCode {
  constructor(
    public id:string,
    public code:string,
    public course?: string,
    public qType? : string,
    public extraQType? : string,
    public status?:boolean
  ){ }
}
