export class Downloadcategory {
  constructor(
      public id:string,
      public title:string,
      public stream : any,
      public session : any,
      public productType : any,
      public shortCode : string,
      public status:boolean){ }
}
