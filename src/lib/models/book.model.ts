export class Book {
  constructor(
      public id:string,
      public title:string,
      public titleAlias:string,
      public description:string,
      public type:string, //book Type: Academic, Question Bank, Magzine, Diary, Notebook, Register, Novel, Dictionary, 
      public stream: any,
      public course: any,
      public mrp:string,
      public cost:string,
      public thumb:string,
      public status:boolean,
      public quantity:number,
      public fullDescription?:string,
      public idNumber?:string, //barCode
      public model?:string, //unique id for inter relation of multiple editions
      public edition?:number, //edition like 1, 2 as 1st, 2nd
      public isbn?:string,
      public featureImg?:string,
      public images?:any,
      public tags?:any, //tags also include bookCategoryNames
      public availableFrom?:any,
      public stockHistory?:any[],
      public dimension?:any, //Dimensions [L, W, H] in cm
      public weight?:number, //in gram
      public sortOrder?:number,
      public uri?:string,
      public url?:string,
      public sample?:string, //url of pdf 
      public attributes?:any, //custom attributes according demand of organisation?: like total pages, topics, subjects stackes in groups etc...
      public seo?:any, //pageTitle, metad, metak, og:image, and more
      public product?:any,
      public bookCategory?:any,
      public author?:any,
){ }
}
