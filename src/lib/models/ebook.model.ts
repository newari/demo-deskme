export class Ebook {
  constructor(
      public id:string,
      public title:string,
      public stream:any,
      public course:any,
      public category:any,
      public subCategory:any,
      public file:string,
      public thumb:string,
      public status:boolean,
      public publication?:any,
      public createdBy?:any,
      public viewLimit?:number,
      public order?:number
      ){ }
}
