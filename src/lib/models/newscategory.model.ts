export class NewsCategory {
  constructor(
      public id:string,
      public title:string,
      public status:boolean){ }
}
