export class Testseries {
    constructor(
        public id: string,
        public title: string,
        public alias: string,
        public course: any,
        public stream: any,
        public type: string,
        public tests: any,
        public featureImage: any,
        public totalTests: number,
        public linkedProduct: number,
        public startDate: any,
        public endDate: any,
        public status: boolean,

    ) { }
}