export class Event {
  constructor(
      public title:any,
      public start:any,
      public end:any,
      public type:string,
      public status:string,
      public id?:string,
      public createdBy?:string,  //User ID
      public duration?:number,
      public startedAt?:any,
      public endedAt?:any,
      public breakDuration?:number,
      public breaks?:any,
      public user?:any,
      public faculty?:any,
      public venue?:string,
      public classroom?:any,
      public batch?:any,
      public subject?:any,
      public attendees?:number,
      public forced?:boolean,
      public more?:string,
      ){ }
}
