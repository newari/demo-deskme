export class Coption {
  constructor(
      public id:string,
      public code:string,
      public option:string,
      public value:any,
      public valueAlias:any, //array[{ "value": "GATE", "valueAlias": "GATE", "category": "Engineering" }]
      public parentOption:string,
      public status:boolean,
      public more?:any,
      public havePage?:boolean,
      public isCommon?:boolean,
      public client?:any,
      public relatedApp?:any
      ){ }
}
