export class Department {
  constructor(
      public id:string,
      public title:string,
      public status:boolean){ }
}
