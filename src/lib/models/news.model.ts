export class News {
  constructor(
      public id:string,
      public title:string,
      public category: any,
      public type: string,
      public centre:any,
      public stream:any,
      public featured:boolean,
      public fileUrl:string,
      public status:boolean,
      public product?:any,
      public course?:any,
      public session?:any,
      public batch?:any,
      public startDate?:string,
      public endDate?:string,
      public needStream?:any,
      public needCenter?:any,
      public needSession?:any,
      public needProduct?:any,
      public needBatch?:any,
      public needCourse?:any

      ){

       }
}
