export class Lesson {
  constructor(
      public id:string,
      public title:string,
      public status:boolean,
      public curriculum?:any,
      public subject?:any,
      public unit?:any,
      public session?:any,
      public topic?:any,
      public description?:any,
      public stream?:any,
      public course?:any,
      public isDisscussionEnabled?:boolean,
      public quiz?:any,
      public educator?:any,
      public type?:any,
      public assignment?:any,
      public isFeedBackEnabled?:boolean,
      public viewLimit?:number
      ){ }
}
