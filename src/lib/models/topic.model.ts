export class Topic {
  constructor(
    public id:string,
    public name:string,
    public subject:any,
    public unit:any,
    public status:boolean){ }
}
