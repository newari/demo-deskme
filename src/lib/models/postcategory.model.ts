export class PostCategory {
  constructor(
      public id:string,
      public title:string,
      public status:boolean,
      public slug?:string,
      public parent?:any,

      ){ }
}
