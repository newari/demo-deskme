export class Frontpage {
  constructor(
      public id:string,
      public title:string,
      public name:string,   ///its type:home, about, products
      public status:boolean,
      public sections?:any){ }
}
