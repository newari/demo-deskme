export class Galleryphoto {
  constructor(
      public id:string,
      public title:string,
      public url:string,
      public link:string,
      public order:number,
      public status:boolean,
      public album?:any){ }
}
