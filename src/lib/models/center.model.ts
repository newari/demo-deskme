export class Center {
    constructor(
        public id:string,
        public title:string,
        public studentIdCard:string,
        public status:boolean,
        public coordinator?:string,
        public landmark?:string,
        public city?:string,
        public address?: string,
        public lat?:string,
        public lon?:string){ }
}
