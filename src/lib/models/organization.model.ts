export class Organization {
  constructor(
      public id:string,
      public title:string,
      public status:boolean){ }
}
