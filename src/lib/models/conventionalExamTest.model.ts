export class ConventionalExamTest{
   
        constructor(
            public id:string,
            public title:string,
            public alias:string,
            public type:string,
            public description:string,
            public totalQs:number,
            public totalMarks:number,
            public duration:number,
            public course:any,
            public lang:string,
            public status:boolean,
            public quesPdf:string,
            public solPdf:string,
            public startDate:any,
            public endDate:any,
            public solVideos:any,
            public preDiscussionVideo : any,
            public topSheetEnable : boolean,
            public modalAnsSheet : any,
            public isSolnEnable : boolean,
            public facultyAssignedTo ?: any,
            public createdBy ?: any
        ){}
}