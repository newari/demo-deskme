export class Firm {
constructor(
    public id:string,
    public name:string,
    public type:string,
    public allowStdReg:boolean,
    public address:string,
    public city:string,
    public state:string,
    public country:string,
    public signature:string,
    public email:string,
    public phone:string,
    public logo:string,
    public directorSign:string,
    public logoPath:string,
    public status:boolean,
    public srnPrefix?:string,
    public gstin?:string,
){ }
}
