export class Listitem {
  constructor(
      public id:string,
      public title:string,
      public desci:string,
      public link:string,
      public thumb:string,
      public icon:string,
      public status:boolean,
      public list?:any
    ){ }
}
