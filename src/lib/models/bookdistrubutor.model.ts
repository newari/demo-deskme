export class BookDistributor{
    constructor(
    public id:string,
    public title:string,
    public status:boolean,
    public store?:any,
    public firm?:any,
    public isEdukitModule?:boolean
 
    ){}
}