export class Classtestseries {
  constructor(
      public id:string,
      public title:string,
      public status:boolean,
      public program?:any,
      public session?:any,
      public batch?:any
    ){ }
}
