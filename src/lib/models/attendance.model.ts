export class Attendance {
    constructor(
        public date: string,
        public batch:any,
        public period:number,
        public periodTiming: any,
        public classroom:any,
        public student: any,
        public present: number, //0:Not Allowed, 1: Present, 2:simply entry not allowed due to fee condition or different batch but forcefully accepted.
        public invigilator?: any, 
        public more?: any,
        public createdAt?:string,
        public id?: string,
        
    ) { }
}
