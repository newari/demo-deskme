export class Enquiry {
    constructor(
        public firstName:string,
        public lastName:string,
        public email:string,
        public mobile:number,
        public message:string,
        public source?:string,
        public id?:string,
        public course?:any,
        public enquiryType?:any,
        public stream?:any,
        public address?:any, // public address:string, // public landmark:string, // public city:string, // public state:string, // public country:string, // public postalCode:string,
        public currentStatus?:string,
        public nextReminderTime?:Date,
        public nextReminderMessage?:string,
        public activities?:any[],//public createdAt:date, // public comment:string, // public reminder : string, // public reminderTime:string
        public createdAt?: any,
        public createdBy?:any,
        public assignedTo?:any,
        public assignedBy?:any,
        public assignedAt?:Date,
        public closed?: string,
        public user?:any,
        public lastActivity?:any

    ){ }
  }