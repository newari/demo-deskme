export class Designation {
  constructor(
      public id:string,
      public title:string,
      public status:boolean){ }
}
