export class Session {
  constructor(public id:number,
              public name:string,
              public status:boolean,
              public isCurrent?:boolean){ }
}
