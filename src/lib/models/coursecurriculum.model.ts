export class CourseCurriculum {
  constructor(
      public id:string,
      public title:string,
      public status:boolean,
      public course?:any,
      public stream?:any,
      public subjects?:any,
      public educators?:any,
      public isDeleted?:boolean,
      public session?:any,
      public products?:any[]

      ){ }
}
