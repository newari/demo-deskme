export class QgammaCode {
  constructor(
    public id:string,
    public code:string,
    public level?: string,
    public status?:boolean
  ){ }
}
