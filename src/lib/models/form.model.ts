export class Form {
  constructor(
      public id:string,
      public title:string,
      public status:boolean,
      public code:string,
      public fields:any,
      public needOTPVerification:boolean
    ){ }
}
