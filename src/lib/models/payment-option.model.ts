export class ProductPaymentOption {
    constructor(
        public ptoduct:any,
        public title:string,
        public amount:number,
        public billingDate:any,
        public dueDate:any,
        public storeId:string,
        public id?:string,
        public extraAmount?:number,
        public applyDiscount?:boolean,
        public linkedProduct?:any,
        public client?:any
        ){ }
  }
  