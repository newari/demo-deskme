export class Favlink {
    constructor(
        public id:string,
        public title:string,
        public url:string,
        public user?:any
    ){ }
  }
  