export class Store {
  constructor(
      public id:string,
      public title:string,
      public firm:any,
      public status:boolean,
      public receiptPrefix:string,
      public tiPrefix:string,
      public bosPrefix:string,
      public sgst:number,
      public cgst:number,
      public igst:number,
      public activeFinancialYear:any,
      public paymentGateway?:any){ }
}
