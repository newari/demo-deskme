export class Testimonial {
  constructor(
      public id:string,
      public studentName:string,
      public course:any,
      public category:any,
      public stream:any,
      public status:boolean){ }
}
