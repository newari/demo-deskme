export class Dashcard {
    constructor(
        public id:string,
        public title:string,
        public src:string,
        public moduleName:string,
        public params:any,
        public status:boolean){ }
  }
  