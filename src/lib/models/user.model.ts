export class User {
  constructor(
      public id:string,
      public firstName:string,
      public lastName:string,
      public email:string,
      public mobile:string,
      public username:string,
      public type:string,
      public status:boolean,
      public apps?:any,
      public activityPermission?:any,
      public operationType?:string,
      public isRecurringCustomer?:boolean,
      public recurringCustomerOf?:any[],
      public profileImg?:string,
      public address?:any,
      public socialProfile?:any,
      public company?:any,
      public settings?:any,
      public regSrc?:string,
      public lastLogin?:any,
      public profile?:any,
      public password?:any,
      public defaultParams?:any,
      public mobileVerified?:boolean,
      public emailVerified?:boolean

      ){ }
}
