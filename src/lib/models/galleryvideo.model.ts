export class Galleryvideo {
  constructor(
      public id:string,
      public title:string,
      public album:string,
      public url:string,
      public link:string,
      public order:number,
      public status:boolean){ }
}
