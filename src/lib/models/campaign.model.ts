export class Campaign {
    constructor(
        public id: string,
        public title: string,
        public status: boolean,
        public msg?: any,
        public type?: string,
        public contentHtml?: string,
        public contentText?: string,
        public params?: string,
        public defaultRecipient?: string,
        public srcId?:string,
        public payload?:any,
        public sentBy?:any,
        public template?:any,
        public isSent?:boolean,
    ) { }
}
