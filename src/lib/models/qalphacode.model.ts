export class QalphaCode {
  constructor(
    public id:string,
    public code:string,
    public stream?: string,
    public subject?: string,
    public topic?: string,
    public unit?: string,
    public status?:boolean
  ){ }
}
