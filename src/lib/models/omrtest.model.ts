export class Omrtest {
    constructor(
        public id:string,
        public title:string,
        public alias:string,
        public type:string,
        public paperType:string,
        public totalQs:string,
        public totalMarks:string,
        public omrSheet:any,
        public course:any,
        public lang:string,
        public checkedStatus: boolean,
        public status:boolean
    ){}
      
}
