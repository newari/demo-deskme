export class Menu {
    constructor(
        public id:string,
        public title:string,
        public website:any,
        public status:boolean,
        public items?:any[]){ }
  }
  