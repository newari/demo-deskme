export class Bookcategory {
  constructor(
      public id:string,
      public title:string,
      public slug:string,
      public status:boolean){ }
}
