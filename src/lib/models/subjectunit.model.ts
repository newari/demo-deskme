export class Subjectunit {
  constructor(
    public id:string,
    public name:string,
    public subject:any,
    public status:boolean
    ){ }
}
