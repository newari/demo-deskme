export class Videoseries {
    constructor(
        public id:string,
        public title:string,
        public alias:string,
        public status:boolean,
        public course: any,
        public tags:any[],
        public videos:any[]
    ){}
      
}
