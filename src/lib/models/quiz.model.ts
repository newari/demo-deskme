export class Quiz {
    constructor (
        public quizGroup : any,
        public qset : any,
        public startDate : any,
        public endDate : any,
        public status : boolean
    ){}
};