export class Notification {
  constructor(
      public id:string,
      public title:string,
      public type:string,
      public actionEvent:any,
      public actionEventValue:string,
      public template:string,
      public params:any,
      public status:boolean,
      public subjectTemplate?:string,
      public defaultNotifications?:any[],
    ){ }
}
