export class Website {
  constructor(
      public id:string,
      public title:string,
      public status:boolean,
      public domain:string,
      public code:string,
      public host:string,
      public email?:any,
      public contactNumber?:any,
      public address?:string,
      public socialLinks?:string,
      public favicon?:string,
      public gcSiteKey?:string,
      public gcSecreteKey?:string,
      public type?:string,
      public blogUri?:string,
      public showDisqus?:boolean,
      public noIndex?:boolean,
      public siteMap?:string,
      public haveSiteMap?:boolean,
      public enableRSSFeed?:boolean,
      public rssFeedLimit?:number
    ){ }
}
