export class Album {
  constructor(
      public id:string,
      public title:string,
      public type:string,
      public status:boolean){ }
}
