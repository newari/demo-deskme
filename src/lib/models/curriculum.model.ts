export class Curriculum {
  constructor(
      public id:string,
      public name:string,
      public course:any,
      public status:boolean){ }
}
