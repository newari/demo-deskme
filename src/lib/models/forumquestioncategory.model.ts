
export class ForumQuestionCategory {
    constructor(
        public id:string,
        public title:string,
        public status:boolean,
        public createdBy?:any,
        public updatedBy?:any,
        public client?:any
        )
        { }
  }