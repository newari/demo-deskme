export class Room {
  constructor(
      public id:string,
      public title:string,
      public filledSeats:string,
      public seatPerRoom:string,
      public roomNoPrefix:string,
      public status:boolean){ }
}
