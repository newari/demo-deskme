export class Download {
  constructor(
      public id:string,
      public title:string,
      public fileUrl:string,
      public category:any,
      public status:boolean){ }
}
