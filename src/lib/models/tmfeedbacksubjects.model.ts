export class TmFeedbackSubjects  {
    constructor(
        public id:string,
        public title:string,
        public type:number,
        public status:boolean
      ){ }
  }