import { Component, ViewContainerRef, ViewChild, ReflectiveInjector, ComponentFactoryResolver, Input, EventEmitter, Output } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
// import { ExportBatchScheduleForm } from '../../../../apps/ims/admin/activities/batch/export-schedule';
import { RightSideWindowData } from './rsw-data.interface';

@Component({
    selector:'ims-rswindow',
    // entryComponents:[ExportBatchScheduleForm],
    templateUrl:'./rswindow.html'
})
export class ImsRswindow{
    windowState:boolean;
    windowPos=-400;
    currentComponent=null;
    windowTitle:string;
    scrolledBottom:boolean=false;
    @ViewChild('rsWindow', { read: ViewContainerRef }) rsWindow: ViewContainerRef;
    @Input() windowWidth:number=400;

    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
    @Output() onError:EventEmitter<any>=new EventEmitter<any>();
    @Output() onClose:EventEmitter<any>=new EventEmitter<any>();
    @Output() onScrolledToBottom:EventEmitter<any>=new EventEmitter<any>();
    // @ViewChild('dynamicComponentContainer', { read: ViewContainerRef }) dynamicComponentContainer: ViewContainerRef;
    constructor(private resolver: ComponentFactoryResolver, private router:Router) {
        this.windowPos=this.windowWidth;
    }

    @Input() set rswComponentData(data: {component: any, inputs: any, title?:any, params?:any }) {
        if (!data) {
            this.windowPos=-this.windowWidth;
            return;
        }
        this.windowPos=0;
        this.windowTitle=data.title;
        // Inputs need to be in the following format to be resolved properly
        let inputProviders = Object.keys(data.inputs).map((inputName) => {return {provide: inputName, useValue: data.inputs[inputName]};});
        let resolvedInputs = ReflectiveInjector.resolve(inputProviders);

        // We create an injector out of the data we want to pass down and this components injector
        let injector = ReflectiveInjector.fromResolvedProviders(resolvedInputs, this.rsWindow.parentInjector);

        // We create a factory out of the component we want to create
        let factory = this.resolver.resolveComponentFactory(data.component);

        // We create the component using the factory and the injector
        let component = factory.create(injector);
        if(component.instance['onSuccess']){
            component.instance['onSuccess'].subscribe(evt=>this.onSuccess.emit(evt));
        }

        if(component.instance['onError']){
            component.instance['onError'].subscribe(evt=>this.onError.emit(evt));
        }

        // component.instance['batchId'] = '111';
        // We insert the component into the dom container
        this.rsWindow.insert(component.hostView);
        let inptKeys=Object.keys(data.inputs);
        for(let ik=0; ik<inptKeys.length; ik++){
            console.log(inptKeys[ik]);

            component.instance[inptKeys[ik]]=data.inputs[inptKeys[ik]];
        }
        // .map((inputName) => {return {provide: inputName, useValue: data.inputs[inputName]};})
        // Destroy the previously created component
        if (this.currentComponent) {
           this.currentComponent.destroy();
        }

        this.currentComponent = component;
    }

    closeRsw(){
        this.onClose.emit(true);
        this.currentComponent.destroy();

        this.windowPos=-this.windowWidth;
        // this.router.navigate(['/ims/admin/insty'], {queryParams: { 'session_id': '111' }})
    }

    onScroll(e){
        if((e.target.scrollTop>(e.target.scrollHeight-e.target.offsetHeight-80))&&!this.scrolledBottom){
            this.onScrolledToBottom.emit(true);
            this.scrolledBottom=true;
        }else if((e.target.scrollTop<(e.target.scrollHeight-e.target.offsetHeight-80))&&this.scrolledBottom){
            this.onScrolledToBottom.emit(false);
            this.scrolledBottom=false;
        }
    }


}
