import { Component } from '@angular/core';
export interface RightSideWindowData{
    component:any,
    inputs:any,
    title?:any,
    params?:any
}