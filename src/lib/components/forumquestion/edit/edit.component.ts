import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Forumquestion } from '../../../models/forumquestion.model';
import { Coption } from '../../../models/coption.model';
import { Subject } from '../../../models/subject.model';
import { ForumquestionService } from '../../../services/forumquestion.service';
import { CoptionService } from '../../../services/coption.service';
import { SubjectService } from '../../../services/subject.service';
import { NotifierService } from '../../notifier/notifier.service';
import { ForumQuestionCategoryService } from '../../../services/forumquestioncategory.service';
import { ForumQuestionCategory } from '../../../models/forumquestioncategory.model';
import { Topic } from '../../../models/topic.model';


@Component({
    selector:'ek-forumquestion-edit-form',
    templateUrl:'./edit.component.html'
})
export class ForumquestionEditFormComponent{
    forumquestionForm:FormGroup;
	forumquestion:Forumquestion;
	formStatus="Normal";
	panelLoader:string="none";
	streams:Coption[];
    subjects:Subject[];
	@Input() forumquestionId:any;
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
	categories: ForumQuestionCategory[];
	topics: Topic[];
	
	constructor(
		private fb:FormBuilder,
		private forumquestionService:ForumquestionService,
        private coptionServise:CoptionService,
        private subjectService:SubjectService,
		private notifier: NotifierService,
		private questionCategoryService:ForumQuestionCategoryService
	){ }
	
	getForumquestion(forumquestionId){
		this.panelLoader="show;"
		this.forumquestionService.getOneForumquestion(forumquestionId).subscribe(
			(res)=>{
				if(res.stream){
					res.stream=res.stream.id||null;
					this.loadSubjects(res.stream);
				}
				if(res.subject){
					res.subject=res.subject.id||null;
					this.loadTopics(res.subject);
				}
				if(res.topic)
				res.topic=res.topic.id||null;
				if(res.category)
				res.category=res.category.id||null;

				this.forumquestion=res;
				this.forumquestionForm.patchValue(res);
				this.panelLoader="none";
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
			}
		)
	}
		
	updateForumquestion(): void {
		if(!this.forumquestionForm.dirty){
			return;
		}
		this.formStatus="Processing";
		this.forumquestion=this.forumquestionForm.value;
		this.forumquestionService.updateForumquestion(this.forumquestionId, this.forumquestion).subscribe(
			(res)=>{
				this.onSuccess.emit(res);
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.forumquestionForm=this.fb.group({
			title:['', Validators.required],
			stream:[null, Validators.required],
			subject:[null, Validators.required],
			topic:["", Validators.required],
			status:[true, Validators.required],
			category:[null, Validators.required],
			tags:[[]]
		});

		let self=this;
		window.setTimeout(function(){
			self.getForumquestion(self.forumquestionId);
			self.loadStreams();
			self.loadQsCategoies();
		}, 0);
	}
	loadStreams(){
        this.coptionServise.getCoption({option:'STREAM'}).subscribe(
            res=>{
                this.streams=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
    }

    loadSubjects(stream?:any){
		this.forumquestionForm.patchValue({topic:null,subject:null});
        this.subjectService.getSubject({stream:stream,limit:'all'}).subscribe(
            res=>{
                this.subjects=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
	}
	

	loadQsCategoies(){
		this.questionCategoryService.getForumQuestionCategory().subscribe(
			res=>{
				this.categories=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}


	loadTopics(subId){
		this.forumquestionForm.patchValue({topic:null});
		this.subjectService.getTopic(subId).subscribe(
			res=>{
				this.topics=res;
			},
			err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		);
	}
}
