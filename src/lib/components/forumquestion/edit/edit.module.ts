import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ForumquestionEditFormComponent } from './edit.component';
import { FileinputModule } from '../../filemanager/fileinput.module';
import { SubjectService } from '../../../services/subject.service';
import { ChipsModule } from 'primeng/primeng';

@NgModule({
    declarations:[ForumquestionEditFormComponent],
    imports:[FileinputModule, CommonModule, FormsModule, ReactiveFormsModule, ChipsModule],
    exports:[ForumquestionEditFormComponent],
    providers:[SubjectService]
})
export class ForumquestionEditFormModule{
}
