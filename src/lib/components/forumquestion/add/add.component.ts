import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Forumquestion } from '../../../models/forumquestion.model';
import { ForumquestionService } from '../../../services/forumquestion.service';
import { NotifierService } from '../../notifier/notifier.service';
import { ForumQuestionCategoryService } from '../../../services/forumquestioncategory.service';
import { Coption } from '../../../models/coption.model';
import { Subject } from '../../../models/subject.model';
import { CoptionService } from '../../../services/coption.service';
import { SubjectService } from '../../../services/subject.service';
import { Topic } from '../../../models/topic.model';
import { AuthService } from '../../../services/auth.service';


@Component({
    selector:'ek-forumquestion-add-form',
    templateUrl:'./add.component.html'
})
export class ForumquestionAddFormComponent{
    forumquestionForm:FormGroup;
	forumquestionData:Forumquestion;
	formStatus="Normal";
	categories:any;
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
	streams:Coption[];
	subjects: Subject[];
	topics: Topic[];
	faculty:any;
    constructor(
		private fb:FormBuilder,
		private forumquestionService:ForumquestionService,
		private notifier: NotifierService,
		private questionCategoryService:ForumQuestionCategoryService,
		private coptionService: CoptionService,
		private subjectService: SubjectService,
		private authService: AuthService

    ){ }
	
	addForumquestion(): void {
		this.formStatus="Processing";
		this.forumquestionData=this.forumquestionForm.value;
		this.forumquestionService.addForumquestion(this.forumquestionData).subscribe(
			res=>{
                this.onSuccess.emit({event:'ItemAdded', item:res});
				this.forumquestionForm.reset(); 
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.faculty= this.authService.faculty();
		
		this.forumquestionForm=this.fb.group({
			title:['', Validators.required],
			status:[true, Validators.required],
			category:[null, Validators.required],
			tags:[[]],
			stream:[null],
			subject:[null],
			topic:[null],
			space:['']
		});
		this.loadQsCategoies();
		this.loadStreams();
		this.loadSubjects();
    }

	loadQsCategoies(){
		this.questionCategoryService.getForumQuestionCategory().subscribe(
			res=>{
				this.categories=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}
    
	loadStreams(){
        this.coptionService.getCoption({option:'STREAM'}).subscribe(
            res=>{
                this.streams=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
    }

    loadSubjects(stream?:any){
		this.forumquestionForm.patchValue({subject:null,topic:null});
        this.subjectService.getSubject({stream:stream, limit:'all'}).subscribe(
            res=>{
                this.subjects=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
	}  
	loadTopics(subId){
		this.forumquestionForm.patchValue({topic:null});
		this.subjectService.getTopic(subId).subscribe(
			res=>{
				this.topics=res;
			},
			err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		);
	} 
}
