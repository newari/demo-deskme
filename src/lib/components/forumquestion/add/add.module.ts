import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ForumquestionAddFormComponent } from './add.component';
import { FileinputModule } from '../../filemanager/fileinput.module';
import { ChipsModule } from 'primeng/primeng';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations:[ForumquestionAddFormComponent],
    imports:[FileinputModule, FormsModule,CommonModule, ReactiveFormsModule, ChipsModule],
    exports:[ForumquestionAddFormComponent]
})
export class ForumquestionAddFormModule{
}
