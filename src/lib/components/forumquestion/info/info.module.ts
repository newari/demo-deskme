import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForumquestionInfoComponent } from './info.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { QuillEditorModule } from '../../ngx-quill-editor';

@NgModule({
    declarations:[ForumquestionInfoComponent],
    imports:[CommonModule, QuillEditorModule, FormsModule, ReactiveFormsModule],
    exports:[ForumquestionInfoComponent]
})
export class ForumquestionInfoModule {
    
}
