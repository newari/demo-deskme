import { Component, OnInit, Input, ElementRef } from '@angular/core';
import {NgIf} from '@angular/common';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Forumquestion } from '../../../models/forumquestion.model';
import { ForumquestionService } from '../../../services/forumquestion.service';
import { AuthService } from '../../../services/auth.service';
import { NotifierService } from '../../notifier/notifier.service';

@Component({
    selector:'ek-forumquestion-info',
    templateUrl:'./info.component.html'
})
export class ForumquestionInfoComponent implements OnInit{
    forumquestion:Forumquestion;
    panelLoader="none";
    activeTab:number=0;
    questionReplies:any[];

    @Input() forumquestionId:any;

    questionReplyForm:FormGroup;
    formStatus:string="Normal";
    fh:any={
        status:'Normal',
        file1:null,
        file2:null,
        file3:null,
        file1Status:'',
        file2Status:'',
        file3Status:'',
        error:null
    };

    constructor(
        private forumquestionService:ForumquestionService,
        private fb:FormBuilder,
        private authService:AuthService,
        private el: ElementRef,
        private notifier: NotifierService
    ){}


    ngOnInit(): void{
        this.questionReplyForm=this.fb.group({
            content:['', Validators.required],
        });

        let self=this;
		window.setTimeout(function(){
            self.getForumquestion(self.forumquestionId);

		}, 0);
    }

    getForumquestion(forumquestionId){
        this.panelLoader="show";
        this.forumquestionService.getOneForumquestion(forumquestionId).subscribe( (data)=>{
                this.forumquestion=data;
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }

    deleteForumquestion(forumquestionId){
        let conforumquestion=window.confirm("Are you sure to delete this?");
        if(!conforumquestion){
            return;
        }
        this.panelLoader="show";
        this.forumquestionService.deleteForumquestion(forumquestionId).subscribe(
            (res)=>{
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }

    loadReply(){
        this.activeTab=1;
        if(this.questionReplies){
            return;
        }
        this.panelLoader="show";
        this.formStatus="Processing";
        this.forumquestionService.getForumquestionReply(this.forumquestion.id).subscribe(
            res=>{
                this.questionReplies=res;
                this.panelLoader="none";
                this.formStatus="Normal";
            },
            err=>{
                // this.notifier.alert(err.code, err.message, "danger", 10000);
                this.panelLoader="none";
                this.formStatus="Normal";
            }
        )
    }

    addReply(){
        let thisUser=this.authService.admin();
        let question=this.questionReplyForm.value;
        question.parentId=this.forumquestion.id;
        question.user=thisUser.id;
        question.status=true;
        question.stream=this.forumquestion.stream?this.forumquestion.stream.id:null;
        question.subject=this.forumquestion.subject?this.forumquestion.subject.id:null;
        question.topic=this.forumquestion.topic||"";
        question.attachments=[];
        if(this.fh.file1){
            question.attachments.push(this.fh.file1);
        }
        if(this.fh.file2){
            question.attachments.push(this.fh.file2);
        }
        if(this.fh.file3){
            question.attachments.push(this.fh.file3);
        }
        this.panelLoader="show";
        this.formStatus="Processing";
        this.forumquestionService.addForumquestionReply(question).subscribe(
            res=>{
                res.user=thisUser;
                if(this.questionReplies&&this.questionReplies.length>0){
                    this.questionReplies.unshift(res);
                }else{
                    this.questionReplies=[res];
                }
                this.questionReplyForm.reset();
                this.fh.file1=null;
                this.fh.file2=null;
                this.fh.file3=null;
                this.panelLoader="none";
                this.formStatus="Normal";
            },
            err=>{
                this.formStatus="Normal";
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )
    }

    upload(id:string) {
    	//locate the file element meant for the file upload.
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#'+id);
		console.log(inputEl);
    	//get the total amount of files attached to the file input.
        let fileCount: number = inputEl.files.length;
    	//create a new fromdata instance
        let formData = new FormData();
    	//check if the filecount is greater than zero, to be sure a file was selected.
        if (fileCount > 0) { // a file was selected
            //append the key name 'photo' with the first file in the element
			formData.append('ekFile', inputEl.files.item(0));
            //call the angular http method
			this.fh[id+"Status"]="Uploading...";
            this.forumquestionService.uploadFile(formData).subscribe(
                res=>{
                    this.fh[id]=res.url;

                    this.fh[id+"Status"]="";
                    // document.getElementById(id).value="";
                },
                err=>{
                    console.log(err);
                    this.fh[id+"Status"]="Error in uploading, Try again!";
                    // document.getElementById(id).value="";
                }
            );


    	}
    }
}
