import { Component, Input, OnChanges, ElementRef } from '@angular/core';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Post } from '../../models/post.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ForumquestionService } from '../../services/forumquestion.service';
import { NotifierService } from '../notifier/notifier.service';
import { AuthService } from '../../services/auth.service';
import { Lesson } from '../../models/lesson.model';
import { LessonService } from '../../services/lesson.service';

@Component({
    selector: 'forum-thread',
    templateUrl: './ft.component.html',
})
export class ForumThreadComponent implements OnChanges{
    @Input('thread') thread:string;
    qPosts:any;
    activeQPost:Post;
    replyForm:FormGroup;
    user:any;
    lesson:Lesson;
    questionForm: FormGroup;
    activateAddQsForm:boolean;
    formStatus: string;
    panelLoader:string;
    activePostLoader:string;
    fh:any={
        status:'Normal',
        file1:null,
        file2:null,
        file3:null,
        file1Status:'',
        file2Status:'',
        file3Status:'',
        error:null
    };
    constructor(private fb:FormBuilder,
        private forumService:ForumquestionService,
        private notifier:NotifierService,
        private authService:AuthService,
        private lessonService:LessonService,
        private el: ElementRef,
        ) {

    }
    ngOnChanges(){
        let self=this;
        this.panelLoader="show";
        setTimeout(() => {
            self.loadQPosts(this.thread);
        }, 1);
        this.user =this.authService.admin();
        if(!this.user){
            this.user=this.authService.faculty();
        }
        if(!this.user){
            this.user= this.authService.student();
        }

        this.loadLesson();
    }

    // Load Lesson Details {{thread is basically a lesson}}
    loadLesson() {
        this.lessonService.getOneLesson(this.thread).subscribe(
            res=>{
                this.lesson=res;
                if(res.stream){
                    res.stream=res.stream.id
                }
                if(res.subject){
                    res.subject=res.subject.id
                }
                if(res.topic){
                    res.topic=res.topic.id
                }
                this.questionForm.patchValue({stream:res.stream, subject:res.subject, topic:res.topic, thread:this.thread});
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    ngOnInit(): void {
        if(!this.thread||this.thread.trim()=='') return;
        let self=this;
        this.panelLoader="show";
        setTimeout(() => {
            self.loadQPosts(this.thread);
        }, 1);

        this.replyForm=this.fb.group({
            title:[''],
            description:['', Validators.required]
        })
        this.questionForm= this.fb.group({
            title:['',Validators.required],
            content:['', Validators.required],
            subject:[''],
            stream:[''],
            topic:[''],
            status:[true],
            thread:['']

        });
    }
    loadQPosts(thread?: string) {
        let filter:any= {
            sort: "createdAt",
            orderBy: "DESC"
        }
         this.forumService.getForumQuestionByLesson(thread, filter).subscribe(
             res=>{
                this.qPosts=res;
                if(res&&res.length<1){
                    this.activateAddQsForm=true;
                }
                this.panelLoader="none";
             },err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
             }
         );
    }
    viewQReplies(i?:number){
        this.activePostLoader="show";
        this.activeQPost=this.qPosts[i];
        this.forumService.getForumquestionReply(this.activeQPost.id, {orderBy:'ASC'}).subscribe(
            res=>{
                this.activeQPost.replies=res;
                this.activePostLoader="none";
                this.notifier.alert("Success", "Replies loaded Successfully", 'success', 500);
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);

                this.activePostLoader="none";
            }
        );
    }
    addReply(){
        let formData:any={
            content: this.replyForm.value.description,
            title:"Reply On"+ this.activeQPost.title,
            thread:this.thread,
            parentId:this.activeQPost.id,
            subject:this.activeQPost.subject,
            stream:this.activeQPost.stream,
            topic:this.activeQPost.topic,
            type : "ForumQuestionReply",
            user:this.user.id
        }
        formData.attachments=[];
        if(this.fh.file1){
            formData.attachments.push(this.fh.file1);
        }
        if(this.fh.file2){
            formData.attachments.push(this.fh.file2);
        }
        if(this.fh.file3){
            formData.attachments.push(this.fh.file3);
        }
        this.forumService.addForumquestionReply(formData).subscribe(
            res=>{
                if(!this.activeQPost.replies&&((this.activeQPost.replies&&this.activeQPost.replies.length<1)||!Array.isArray(this.activeQPost.replies))){
                    this.activeQPost.replies=[];
                    this.activeQPost.totalReplies=0;
                }
                this.activeQPost.replies.push(res);
                this.activeQPost.totalReplies++;
                this.replyForm.reset();
                this.fh={
                    status:'Normal',
                    file1:null,
                    file2:null,
                    file3:null,
                    file1Status:'',
                    file2Status:'',
                    file3Status:'',
                    error:null
                };
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );

    }


    addNewQuestion(){
        if(!this.questionForm.valid){
            return;
        }
        this.formStatus="Processing";

        let frmData=this.questionForm.value;
        frmData.user=this.user.id;
        frmData.attachments=[];
        if(this.fh.file1){
            frmData.attachments.push(this.fh.file1);
        }
        if(this.fh.file2){
            frmData.attachments.push(this.fh.file2);
        }
        if(this.fh.file3){
            frmData.attachments.push(this.fh.file3);
        }
        this.forumService.addForumquestion(this.questionForm.value).subscribe(
            res=>{
                res.user=this.user;
                if(this.qPosts.length>0){
                    this.qPosts.unshift(res);
                }else{
                    this.qPosts.push(res);
                }
                this.activateAddQsForm=false;
                this.formStatus="";
                this.questionForm.patchValue({title:'', content:''});
                this.fh={
                    status:'Normal',
                    file1:null,
                    file2:null,
                    file3:null,
                    file1Status:'',
                    file2Status:'',
                    file3Status:'',
                    error:null
                };
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )

    }
    upload(id:string) {
    	//locate the file element meant for the file upload.
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#'+id);
		console.log(inputEl);
    	//get the total amount of files attached to the file input.
        let fileCount: number = inputEl.files.length;
    	//create a new fromdata instance
        let formData = new FormData();
    	//check if the filecount is greater than zero, to be sure a file was selected.
        if (fileCount > 0) { // a file was selected
            //append the key name 'photo' with the first file in the element
			formData.append('ekFile', inputEl.files.item(0));
            //call the angular http method
			this.fh[id+"Status"]="Uploading...";
            this.forumService.uploadFile(formData).subscribe(
                res=>{
                    this.fh[id]=res.url;

                    this.fh[id+"Status"]="";
                    // document.getElementById(id).value="";
                },
                err=>{
                    console.log(err);
                    this.fh[id+"Status"]="Error in uploading, Try again!";
                    // document.getElementById(id).value="";
                }
            );


    	}
    }

}
