import { NgModule }      from '@angular/core';
import { ForumThreadComponent } from './ft.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ForumquestionService } from '../../services/forumquestion.service';
import { AuthService } from '../../services/auth.service';
import { LessonService } from '../../services/lesson.service';
import { CKEditor4Module } from '../ckeditor4/ckeditor4.module';
import { DialogModule } from 'primeng/dialog';


@NgModule({
    declarations: [ ForumThreadComponent ],
    imports:[CommonModule, FormsModule, ReactiveFormsModule, CKEditor4Module, DialogModule],
    exports:[ForumThreadComponent],
    providers:[ForumquestionService,AuthService, LessonService]
})
export class ForumThreadModule {  }
