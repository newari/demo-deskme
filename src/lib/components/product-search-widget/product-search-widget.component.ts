import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Product } from '../../models/product.model';
import { ProductService } from '../../services/product.service';
import { NotifierService } from '../notifier/notifier.service';
@Component({
    selector:'ek-product-search-widget',
    templateUrl:'./product-search-widget.component.html'
})
export class ProductSearchWidgetComponent implements OnInit{
    
    productData:Product;
    products:any[]=[];
    selectedProduct:any;
    @Input() store:any;
    @Output() onProductSelect:EventEmitter<any>=new EventEmitter<any>();
    constructor(
		
		private productService:ProductService,
		private notifier: NotifierService
		){ 
			
    }
    ngOnInit(): void {
		
    }
    
    set query(q:string){
        let fltr:any={alias:q};
        if(this.store){
            fltr.store=this.store;
        }
        this.productService.searchProduct(fltr).subscribe(
            res=>{
                this.products=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
    selectProduct(i){
        this.selectedProduct=this.products[i];
        this.onProductSelect.emit(this.products[i]);
        this.products=[];
    }
    
}