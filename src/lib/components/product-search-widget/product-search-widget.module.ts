import { NgModule } from '@angular/core';
import { TabViewModule } from 'primeng/primeng';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductSearchWidgetComponent } from './product-search-widget.component';
import { ProductService } from '../../services/product.service';

@NgModule({
    declarations:[ProductSearchWidgetComponent],
    imports:[TabViewModule, CommonModule, FormsModule, ReactiveFormsModule],
    providers:[ProductService],
    exports:[ProductSearchWidgetComponent]
})
export class ProductSerachWidgetModule{

}