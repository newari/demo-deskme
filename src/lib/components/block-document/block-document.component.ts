import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NotifierService } from '../notifier/notifier.service';



@Component({
    selector:'block-document',
    templateUrl:'./block-document.component.html'
})
export class BlockDocumentComponent implements OnInit{
    @Input() isBlocked:boolean;
    @Input() reason:string;

    constructor(
        private notifier: NotifierService){}
        
    ngOnInit(){
         
    }

    
}