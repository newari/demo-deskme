import { NgModule } from '@angular/core';
import { BlockDocumentComponent } from './block-document.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookService } from '../../services/book.service';

@NgModule({
    declarations:[BlockDocumentComponent],
    exports:[BlockDocumentComponent],
    providers:[BookService],
    imports:[CommonModule, FormsModule, ReactiveFormsModule]
})
export class BlockDocumentModule{
    
}