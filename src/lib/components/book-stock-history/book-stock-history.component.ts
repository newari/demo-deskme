import { Component, OnInit, Input } from '@angular/core';
import { BookService } from '../../services/book.service';
import { NotifierService } from '../notifier/notifier.service';

@Component({
    selector:'ek-book-stock-history',
    templateUrl:'./book-stock-history.component.html'
})
export class BookStockHistoryComponent implements OnInit{
    @Input() bookId:string;
    stocks:any[];
    panelLoader:string="none";
    
    constructor(
        private bookService:BookService,
        private notifier: NotifierService){}
        
    ngOnInit(){
        this.loadStockHistory();
    }

    loadStockHistory(){
        this.panelLoader="show";
        this.bookService.getStockHistory(this.bookId).subscribe(
            data=>{
                this.stocks=data;
                this.panelLoader="none";
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
}