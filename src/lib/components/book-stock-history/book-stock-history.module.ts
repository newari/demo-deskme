import { NgModule } from '@angular/core';
import { BookStockHistoryComponent } from './book-stock-history.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookService } from '../../services/book.service';

@NgModule({
    declarations:[BookStockHistoryComponent],
    exports:[BookStockHistoryComponent],
    providers:[BookService],
    imports:[CommonModule]
})
export class BookStockHistoryModule{
    
}