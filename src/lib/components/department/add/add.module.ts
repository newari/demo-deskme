import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DepartmentAddFormComponent } from './add.component';
import { FileinputModule } from '../../filemanager/fileinput.module';

@NgModule({
    declarations:[DepartmentAddFormComponent],
    imports:[FileinputModule, FormsModule, ReactiveFormsModule],
    exports:[DepartmentAddFormComponent]
})
export class DepartmentAddFormModule{
}
