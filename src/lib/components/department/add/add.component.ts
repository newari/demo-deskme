import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotifierService } from '../../notifier/notifier.service';
import { DepartmentService } from '../../../services/department.service';
import { Department } from '../../../models/department.model';


@Component({
    selector:'ek-department-add-form',
    templateUrl:'./add.component.html'
})
export class DepartmentAddFormComponent{
    departmentForm:FormGroup;
	departmentData:Department;
    formStatus="Normal";
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
    
    constructor(
		private fb:FormBuilder,
		private departmentService:DepartmentService,
		private notifier: NotifierService
    ){ }
	
	addDepartment(): void {
		this.formStatus="Processing";
		this.departmentData=this.departmentForm.value;
		this.departmentService.addDepartment(this.departmentData).subscribe(
			res=>{
                this.onSuccess.emit({event:'ItemAdded', item:res});
				this.departmentForm.reset(); 
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.departmentForm=this.fb.group({
			title:['', Validators.required],
			status:[true, Validators.required],
        });
        
    }

   
    
    
}
