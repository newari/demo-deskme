import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DepartmentService } from '../../../services/department.service';
import { NotifierService } from '../../notifier/notifier.service';
import { Department } from '../../../models/department.model';


@Component({
    selector:'ek-department-edit-form',
    templateUrl:'./edit.component.html'
})
export class DepartmentEditFormComponent{
    departmentForm:FormGroup;
	department:Department;
	formStatus="Normal";
	panelLoader:string="none";

	@Input() departmentId:any;
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
	
	constructor(
		private fb:FormBuilder,
		private departmentService:DepartmentService,
		private notifier: NotifierService
	){ }
	
	getDepartment(departmentId){
		this.panelLoader="show;"
		this.departmentService.getOneDepartment(departmentId).subscribe(
			(res)=>{
				this.department=res;
				this.departmentForm.patchValue(res);
				this.panelLoader="none";
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
			}
		)
	}
		
	updateDepartment(): void {
		if(!this.departmentForm.dirty){
			return;
		}
		this.formStatus="Processing";
		this.department=this.departmentForm.value;
		this.departmentService.updateDepartment(this.departmentId, this.department).subscribe(
			(res)=>{
				this.onSuccess.emit(res);
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.departmentForm=this.fb.group({
			title:['', Validators.required],
			status:[true, Validators.required],
		});

		let self=this;
		window.setTimeout(function(){
			self.getDepartment(self.departmentId);
		}, 0);
    }
}
