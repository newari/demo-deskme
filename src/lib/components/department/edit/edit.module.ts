import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DepartmentEditFormComponent } from './edit.component';
import { FileinputModule } from '../../filemanager/fileinput.module';

@NgModule({
    declarations:[DepartmentEditFormComponent],
    imports:[FileinputModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[DepartmentEditFormComponent]
})
export class DepartmentEditFormModule{
}
