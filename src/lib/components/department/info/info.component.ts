import { Component, OnInit, Input } from '@angular/core';
import {NgIf} from '@angular/common';
import { ActivatedRoute, Params } from '@angular/router';
import { DepartmentService } from '../../../services/department.service';
import { NotifierService } from '../../notifier/notifier.service';
import { Department } from '../../../models/department.model';


@Component({
    selector:'ek-department-info',
    templateUrl:'./info.component.html'
})
export class DepartmentInfoComponent implements OnInit{ 
    department:Department;
    panelLoader="none";
    activeTab:number=0;
    
    @Input() departmentId:any;

    constructor(
        private departmentService:DepartmentService,
        private activatedRoute:ActivatedRoute,
        private notifier: NotifierService
    ){}


    ngOnInit(): void{
        let self=this;
		window.setTimeout(function(){
			self.getDepartment(self.departmentId);
		}, 0);
    }

    getDepartment(departmentId){
        this.panelLoader="show";
        this.departmentService.getOneDepartment(departmentId).subscribe( (data)=>{
                this.department=data;
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }

    deleteDepartment(departmentId){
        let condepartment=window.confirm("Are you sure to delete this?");
        if(!condepartment){
            return;
        }
        this.panelLoader="show";
        this.departmentService.deleteDepartment(departmentId).subscribe(
            (res)=>{
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }
}
