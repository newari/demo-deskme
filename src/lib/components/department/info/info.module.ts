import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DepartmentInfoComponent } from './info.component';

@NgModule({
    declarations:[DepartmentInfoComponent],
    imports:[CommonModule],
    exports:[DepartmentInfoComponent]
})
export class DepartmentInfoModule {
    
}
