import { Component, OnInit, Input } from '@angular/core';
import { Student } from '../../../models/student.model';
import { StudentService } from '../../../services/student.service';
import { NotifierService } from '../../notifier/notifier.service';
import { EdukitConfig } from '../../../../ezukit.config';


@Component({
    selector:'ek-student-info',
    templateUrl:'./info.component.html'
})
export class StudentInfoComponent implements OnInit{
    student:Student;
    panelLoader="none";
    BASIC_CONFIG:any;
    @Input() userId:any;
    @Input() studentId:any;
    @Input() profileDownloadOpt:boolean;

    constructor(
        private studentService:StudentService,
        private notifier: NotifierService
    ){
        this.BASIC_CONFIG=EdukitConfig.BASICS;
    }


    ngOnInit(): void{
        let self=this;
        // if(this)
        window.setTimeout(function(){
            self.getStudent(self.studentId);
        }, 1);
    }

    getStudent(studentId?:string){

        let filter:any={limit:1};
        if(studentId){
            filter.id=studentId;
        }else if(this.userId){
            filter.user=this.userId;
        }else{
            return;

            // this.notifier.alert("Missing User", "Student ID is missing!", 'danger', 10000 );
        }
        this.panelLoader="show";
        this.studentService.getStudent(filter).subscribe(
            (data)=>{
                this.student=data[0];
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }
    createProfilePdf(){
        this.studentService.createStudentProfilePdf(this.student.id).subscribe(
            res=>{
                this.student.profilePdf=res;
                this.notifier.alert('Done!', "Profile PDF Created Successfully!", 'success', 10000 );
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000 );
            }
        )
    }
    createIdCard(){
        this.studentService.createStudentIdCard(this.student.id).subscribe(
            res=>{
                this.student.idCard=res;
                this.notifier.alert('Done!', "ID Card Created Successfully!", 'success', 10000 );
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000 );
            }
        )
    }
    resetDeviceLimit(userId:string){
        let conf= window.confirm("Are you sure to remove all linked device limit?");
        if(!conf){
            return;
        }

        this.studentService.resetDeviceLimit(userId).subscribe(
            res=>{
                this.panelLoader="none";
                this.notifier.alert("Done!", "Device reseted successfully!", 'success', 3000 );
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 3000 );
            }
        )
    }
}
