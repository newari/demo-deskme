import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { StudentInfoComponent } from './info.component';
import { StudentService } from '../../../services/student.service';

@NgModule({
    declarations:[StudentInfoComponent],
    imports:[CommonModule, RouterModule],
    exports:[StudentInfoComponent],
    providers:[StudentService]
})
export class StudentInfoModule {
    
}
