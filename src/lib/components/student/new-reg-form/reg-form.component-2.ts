import { Component, OnInit, Input, EventEmitter, Output, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';
import { User } from '../../../models/user.model';
import { Product } from '../../../models/product.model';
import { UserService } from '../../../services/user.service';
import { NotifierService } from '../../notifier/notifier.service';
import { SessionService } from '../../../services/session.service';
import { CoptionService } from '../../../services/coption.service';
import { CenterService } from '../../../services/center.service';
import { ProductService } from '../../../services/product.service';
import { StudentService } from '../../../services/student.service';
import { SettingsService } from '../../../services/settings.service';

@Component({
    selector:'ek-student-reg-form-2',
    templateUrl:'./reg-form.component-2.html'
})
export class StudentRegForm2Component implements OnInit{
    studentRegForm:FormGroup;
    userData:User;
    formStatus:string="Normal";
    resetPassword:boolean=false;
    courses:any[]=[];
    productTypes:any[]=[];
    products:Product[];
    selectedProduct:Product;
    validationError:string;
    paymentMethod:string="";
    paymentOption:string="";
    fh:any={
        personalImg:'https://iesmaster.org/public/images/dummy-photo.png',
        signImg:'https://iesmaster.org/public/images/dummy-photo-sign.png',
        status:'Normal',
        error:null
    };
    formSettings:any;
    // myDatePickerOptions: IMyDpOptions = {
    //     // other options...
    //     dateFormat: 'dd/mm/yyyy',
    // };
    stdHavePastExams:boolean=false;
    isExStd:boolean=false;
    pastCrses:number[]=[0,1,2];
    @Output() onStudentRegistered:EventEmitter<any>=new EventEmitter<any>();
    @Input() submitLabel:string='Save';
    @Input() userSearchOpt:boolean=false;
    @Input() defaultUserValue:any;
    @Input() formCourses:any[];//=["ESE+GATE"];  //Course Coptio Values
    @Input() formProductTypes:any[];//=["Postal Book Program"];  //ProductType Coptio Values
    @Input() streams:any[]//=['CE', 'ME']; //'EE', 'EC'
    @Input() centers:any[]//=['centerId'];
    @Input() sessions:any[]//=['sessionId'];
    siblings: FormArray;
    @Input('user')
    set user(u){
        if(this.studentRegForm){
            if(u){
                let uData:any={};
                uData.firstName=u.firstName;
                uData.lastName=u.lastName;
                uData.email=u.email;
                uData.mobile=u.mobile;
                uData.personalImg=u.profileImg;
                if(!u.address){
                    uData.address={};
                    uData.permanantAddress={};
                }else{
                    if(!u.address.shipping){
                        uData.address={address:'', landmark:'', city:'', state:'', postalCode:'', country:'India'};
                    }else{
                        uData.address=u.address.shipping
                    }
                    if(!u.address.billing){
                        uData.permanantAddress={address:'', landmark:'', city:'', state:'', postalCode:'', country:'India'};
                    }else{
                        uData.permanantAddress=u.address.billing
                    }
                }
                this.studentRegForm.patchValue({user:uData});
                this.userData=u;
                this.resetPassword=false;
            }else{
                this.studentRegForm.patchValue({user:{}});
                this.userData=null;
                this.resetPassword=true;
            }
        }
            
    }
    constructor(
		private fb:FormBuilder,
		private userService:UserService,
        private notifier: NotifierService,
        private el: ElementRef,
        private sessionService:SessionService,
        private coptionService:CoptionService,
        private centerService:CenterService,
        private productService:ProductService,
        private studentService:StudentService,
        private settingService:SettingsService
	){ 
			
    }
    ngOnInit():void{
        let self=this;
        window.setTimeout(function(){
            self.setInit();
        }, 1);
    }

    setInit(){
        
        this.loadCenters();
        this.loadCourse();
        this.loadProductTypes();
        this.loadStreams();
        this.loadSessions();
        this.loadSettings();
    }
    loadSettings(){
        this.settingService.getSettingByKey('ADMISSION_SETTING').subscribe(
            (res)=>{
                
                if (res&&res.more) {
                    this.formSettings=res.more;
                    this.studentRegForm = this.fb.group({
                        center: ['', Validators.required],
                        course: ['', Validators.required],
                        productType: ['', Validators.required],
                        stream: ['', Validators.required],
                        session: ['', Validators.required],
                        product: ['', Validators.required],
                        batch: [''],
                        user: this.fb.group({
                            firstName: ['', Validators.required],
                            lastName: ['', (this.formSettings.lastName.mandatory ? Validators.required  : '')],
                            middleName: ['', (this.formSettings.middleName.mandatory ? Validators.required  : '')],
                            mobile: ['', (this.formSettings.mobile.mandatory ? Validators.required : '')],
                            email: ['', Validators.required],
                            category: ['null', (this.formSettings.category.mandatory ?Validators.required: '')],
                            gender: ['null', (this.formSettings.gender.mandatory ?Validators.required: '')],
                            dob: [null, (this.formSettings.dob.mandatory ?Validators.required: '')],
                            personalImg: ['', (this.formSettings.personalImg.mandatory ?Validators.required: '')],
                            signImg: ['', (this.formSettings.signImg.mandatory ?Validators.required: '')],
                            father: this.fb.group({
                                name: ['', (this.formSettings.father.fields.name.mandatory ?Validators.required: '')],
                                mobile: ['', (this.formSettings.father.fields.mobile.mandatory ? Validators.required : '')],
                                email: ['', (this.formSettings.father.fields.email.mandatory ? Validators.required : '')],
                                qualification: ['', (this.formSettings.father.fields.qualification.mandatory ? Validators.required : '')],
                                designation: ['', (this.formSettings.father.fields.designation.mandatory ? Validators.required : '')],
                                occupation: ['', (this.formSettings.father.fields.occupation.mandatory ? Validators.required : '')],
                                officeAddress: ['', (this.formSettings.father.fields.officeAddress.mandatory ? Validators.required : '')],
                                age: ['', (this.formSettings.father.fields.age.mandatory ? Validators.required : '')],
                                photo: ['', (this.formSettings.father.fields.photo.mandatory ? Validators.required : '')],
                                nationality: ['INDIAN', (this.formSettings.father.fields.nationality.mandatory ? Validators.required : '')],
                                aadhaarNo: ['', (this.formSettings.father.fields.aadhaarNo.mandatory ? Validators.required : '')],
                                annualIncome: ['', (this.formSettings.father.fields.annualIncome.mandatory ? Validators.required : '')]

                            }),
                            mother: this.fb.group({
                                name: ['', (this.formSettings.mother.fields.name.mandatory ? Validators.required : '')],
                                mobile: ['', (this.formSettings.mother.fields.mobile.mandatory ? Validators.required : '')],
                                email: ['', (this.formSettings.mother.fields.email.mandatory ? Validators.required : '')],
                                qualification: ['', (this.formSettings.mother.fields.qualification.mandatory ? Validators.required : '')],
                                designation: ['', (this.formSettings.mother.fields.designation.mandatory ? Validators.required : '')],
                                occupation: ['', (this.formSettings.mother.fields.occupation.mandatory ? Validators.required : '')],
                                officeAddress: ['', (this.formSettings.mother.fields.officeAddress.mandatory ? Validators.required : '')],
                                age: ['', (this.formSettings.mother.fields.age.mandatory ? Validators.required : '')],
                                photo: ['', (this.formSettings.mother.fields.photo.mandatory ? Validators.required : '')],
                                nationality: ['INDIAN', (this.formSettings.mother.fields.nationality.mandatory ? Validators.required : '')],
                                aadhaarNo: ['', (this.formSettings.mother.fields.aadhaarNo.mandatory ? Validators.required : '')],
                                annualIncome: ['', (this.formSettings.mother.fields.annualIncome.mandatory ? Validators.required : '')]
                            }),
                            urbanOrRural: ['URBAN', (this.formSettings.urbanOrRural.mandatory ? Validators.required : '')],
                            motherTongue: ['HINDI', (this.formSettings.motherTongue.mandatory ? Validators.required : '')],
                            nationality: ['INDIAN', (this.formSettings.nationality.mandatory ? Validators.required : '')],
                            languageKnown: ['HINDI', (this.formSettings.languageKnown.mandatory ? Validators.required : '')],
                            aadhaarNo: ['', (this.formSettings.aadhaarNo.mandatory ? Validators.required : '')],
                            religion: ['HINDU', (this.formSettings.religion.mandatory ? Validators.required : '')],
                            bloodGroup: ['null', (this.formSettings.bloodGroup.mandatory ? Validators.required : '')],
                            address: this.fb.group({
                                address: ['', (this.formSettings.address.fields.address.mandatory ? Validators.required : '')],
                                landmark: [''],
                                city: [''],
                                state: [''],
                                postalCode: [''],
                                country: ['India'],
                            }),
                            permanantAddress: this.fb.group({
                                address: ['', (this.formSettings.address.fields.permanentAddress.mandatory ? Validators.required : '')],
                                landmark: [''],
                                city: [''],
                                state: [''],
                                postalCode: [''],
                                country: ['India']
                            }),
                            siblings: this.fb.array([]),
                            exStudent: this.fb.group({
                                srn: ['', (this.formSettings.exStudent.fields.srn.mandatory ? Validators.required : '')],
                                session: ['', (this.formSettings.exStudent.fields.session.mandatory ? Validators.required : '')],
                                idProof: ['', (this.formSettings.exStudent.fields.idProof.mandatory ? Validators.required : '')],
                                program: ['', (this.formSettings.exStudent.fields.program.mandatory ? Validators.required : '')]
                            }),
                            pastCourses: this.fb.array([
                                this.initPastCourseFrm("", this.formSettings.pastCourses&&this.formSettings.pastCourses.display&&this.formSettings.pastCourses.fields&&this.formSettings.pastCourses.fields.college&&this.formSettings.pastCourses.fields.college.mandatory?true:false),
                                this.initPastCourseFrm(""),
                                this.initPastCourseFrm("Other")
                            ]),
                            pastExams: this.fb.array([
                                this.initPastExamFrm("GATE"),
                                this.initPastExamFrm("ESE")
                            ]),
                            emergencyContact: this.fb.group({
                                name: ['', (this.formSettings.emergencyContact.fields.name.mandatory ? Validators.required : '')],
                                mobile: ['', (this.formSettings.emergencyContact.fields.mobile.mandatory ? Validators.required : '')],
                                relationship: ['', (this.formSettings.emergencyContact.fields.relationship.mandatory ? Validators.required : '')],
                            }),
                            medicalHistory: this.fb.group({
                                birthHistory: this.fb.group({
                                    birthDetail: ['', (this.formSettings.medicalHistory.birthHistory.fields.birthDetail.mandatory ? Validators.required : '')],
                                    birthCry: ['', (this.formSettings.medicalHistory.birthHistory.fields.birthCry.mandatory ? Validators.required : '')],
                                    dischargeInDays: ['', (this.formSettings.medicalHistory.birthHistory.fields.dischargeInDays.mandatory ? Validators.required : '')],
                                    specialCare: ['', (this.formSettings.medicalHistory.birthHistory.fields.specialCare.mandatory ? Validators.required : '')],
                                    ifNICU: ['', ],
                                    explaination:['',],
                                }),
                                hearing: this.fb.group({
                                    difficulty :['',(this.formSettings.medicalHistory.hearing.fields.difficulty.mandatory?Validators.required:false)] ,
                                    consultation: ['', (this.formSettings.medicalHistory.hearing.fields.consultation.mandatory ? Validators.required : false)],
                                    explaination:['']
                                }),
                                vision: this.fb.group({
                                    useLenses: ['', (this.formSettings.medicalHistory.vision.fields.useLenses.mandatory ? Validators.required : false)],
                                    consultation: ['', (this.formSettings.medicalHistory.vision.fields.consultation.mandatory ? Validators.required : false)],
                                    explaination: ['']

                                }),
                                motorMileStone: this.fb.group({
                                    sitting: ['', (this.formSettings.medicalHistory.motorMileStone.fields.sitting.mandatory ? Validators.required : false)],
                                    standing: ['', (this.formSettings.medicalHistory.motorMileStone.fields.standing.mandatory ? Validators.required : false)],
                                    walking: ['', (this.formSettings.medicalHistory.motorMileStone.fields.walking.mandatory ? Validators.required : false)],
                                    speech: ['', (this.formSettings.medicalHistory.motorMileStone.fields.speech.mandatory ? Validators.required : false)],
                                    medicationForMedicalCondition: ['', (this.formSettings.medicalHistory.motorMileStone.fields.medicationForMedicalCondition.mandatory ? Validators.required : false)],
                                    medicationForWellBeing: ['', (this.formSettings.medicalHistory.motorMileStone.fields.medicationForWellBeing.mandatory ? Validators.required : false)],
                                    medicationForAlergy: ['', (this.formSettings.medicalHistory.motorMileStone.fields.medicationForAlergy.mandatory ? Validators.required : false)],
                                })
                            }),
                            miscellaneous: this.fb.group({
                                howDidHearAboutInstitue: this.fb.group({
                                    website: [''],
                                    magzine: [''],
                                    newsPaper: [''],
                                    others: ['']
                                })
                            }),
                            distanceFromSchool:['', (this.formSettings.distanceFromSchool.mandatory ? Validators.required : '') ]
                        }),
                        cart: this.fb.group({
                            discountCode: ['']
                        })
                    });

                    this.createSiblingByInput(2);
                }
                
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    createSiblings(): FormGroup {
        return this.fb.group({
            name: [''],
            age:[''],
            institute: [''],
            standard: ['']
        })
    }

    createSiblingByInput(totalSibling) {
        if (totalSibling != '') {
           let user =this.studentRegForm.get('user') as FormGroup; 
            this.siblings = user.get('siblings') as FormArray;
            for (let index = 0; index < totalSibling; index++) {
                this.siblings.push(this.createSiblings())
            };
            
        }
    }
    removeSibling(index){
        this.siblings.removeAt(index);
    }
    initPastCourseFrm(crsName:string, required?:boolean){
        return this.fb.group({
            course:[crsName, (required?Validators.required:null)],
            courseDuration:[''],
            college:['', (required?Validators.required:null)],
            year:['', (required?Validators.required:null)],
            marks:['', (required?Validators.required:null)],
            remarks:[''],
        })
    }

    initPastExamFrm(examName:string){
        return this.fb.group({
            name:[examName],
            year:['2016-17'],
            rank:[''],
            remarks:[''],
        })
    }
    
    loadSessions(){
        let filter:any={status:true};
        if(this.sessions){
            filter.id=this.sessions;
        }
        this.sessionService.getSession(filter).subscribe(
            res=>{
                this.sessions=res;
            },
            err=>{
                console.log(err);
            }
        );
    }

    loadCenters(){
        let filter:any={status:true};
        if(this.centers){
            filter.id=this.centers;
        }
        this.centerService.getCenter(filter).subscribe(
            res=>{
                this.centers=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    loadCourse(){
        this.coptionService.getCoption({option:'COURSE', status:true}).subscribe(
            res=>{
                if(this.formCourses){
                    var self=this;
                    res=res.filter(function(crs:any){
                        for(let ci=0; ci<self.formCourses.length; ci++){
                            if(crs.value==self.formCourses[ci]){
                                return true;
                            }
                        } 
                        return false;
                    })
                }
                this.courses=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    loadProductTypes(){
        this.coptionService.getCoption({option:'PRODUCTTYPE', status:true}).subscribe(
            res=>{
                if(this.formProductTypes){
                    var self=this;
                    res=res.filter(function(pt:any){
                        for(let ci=0; ci<self.formProductTypes.length; ci++){
                            if(pt.value==self.formProductTypes[ci]){
                                return true;
                            }
                        } 
                        return false;
                    })
                }
                this.productTypes=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    loadStreams(){
        this.coptionService.getCoption({option:'STREAM', status:true}).subscribe(
            res=>{
                if(this.streams){
                    var self=this;
                    res=res.filter(function(strm:any){
                        for(let ci=0; ci<self.streams.length; ci++){
                            if(strm.value==self.streams[ci]){
                                return true;
                            }
                        } 
                        return false;
                    })
                }
                this.streams=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    checkProductDepParams(){
        let frmData=this.studentRegForm.value;
        if(frmData.course==""||frmData.productType==""||frmData.stream==""||frmData.session==""||frmData.center==""){
            alert("Please select Center, Course, Program, Session & Stream First!");
            return;
        }
    }
    setProductDepParams(){
        this.selectedProduct=this.products[this.studentRegForm.value.product];
    }
    loadProducts(){
        let frmData=this.studentRegForm.value;
        if(frmData.course==""||frmData.productType==""||frmData.stream==""||frmData.center==""||frmData.session==""){
            return;
        }
        let filter={
            center:frmData.center,
            course:frmData.course,
            productType:frmData.productType,
            stream:frmData.stream,
            session:frmData.session,
            populateBatches:true,
            status:true
        };

        this.productService.getProduct(filter).subscribe(
            res=>{
                this.products=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    setAddressSimilarity(el:any){
        if(el.target.checked){
            let corsAdrs=this.studentRegForm.value.user.address;
            this.studentRegForm.patchValue({user:{permanantAddress:corsAdrs}});
        }
    }

    getValue(arr:any[], id:string){
        arr.filter(function(el){
            if(el.id==id){
                return true;
            }
            return false;
        });
        return arr[0];
    }

    generatePassword(){
        // this.regForm.patchValue({password:Math.floor((Math.random() * 100000) + 1)});
    }

    setPasswordOpt(state){
        this.resetPassword=state;
    }

    setBillingAddress(sameAsShipping){
        if(sameAsShipping){
            // this.regForm.patchValue({address:{billing:this.regForm.value.address.shipping}});
        }else{
            // this.regForm.patchValue({address:{billing:((this.userData&&this.userData.address&&this.userData.address.billing)?this.userData.address.billing:{address:'', landmark:'', city:'', state:'', postalCode:'', country:'India'})}});
        }
    }

    onUserSelect(user){
		this.userService.getOneUser(user.id).subscribe(
			res=>{
                this.user=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
    }
    
    addUpdateStudent(placeOrder?:boolean){
        let userData=this.studentRegForm.value.user;
        if (!this.studentRegForm.valid) {
            this.validationError='show-error';
            this.fh.error="Please Fill All Mandatory fields";
            return;
        }
        
        if (this.formSettings) {
            if ((userData.personalImg == "" &&this.formSettings.personalImg&&this.formSettings.personalImg.mandatory) || (userData.signImg == "" &&this.formSettings.signImg&&this.formSettings.signImg.mandatory)) {
                this.fh.error = "Personal Image and Sign Image are mandatory!";
                return;
            }
            if (this.formSettings.father && this.formSettings.father.fields && this.formSettings.father.fields.photo && this.formSettings.father.fields.photo.mandatory&&!userData.father.photo) {
                this.fh.error = "Father Photo Required!";
                return;
            }
            if (this.formSettings.mother && this.formSettings.mother.fields && this.formSettings.mother.fields.photo && this.formSettings.mother.fields.photo.mandatory && !userData.mother.photo) {
                this.fh.error = "Mother Photo Required!";
                return;
            }
        }
        userData.customParams={
            urbanOrRural: userData.urbanOrRural,
            motherTongue: userData.motherTongue,
            nationality: userData.nationality,
            languageKnown: userData.languageKnown,
            aadhaarNo: userData.aadhaarNo,
            religion: userData.religion,
            bloodGroup: userData.bloodGroup,
            emergencyContact: userData.emergencyContact,
            medicalHistory: userData.medicalHistory,
        }
        userData.stream=this.studentRegForm.value.stream;
        userData.course=this.studentRegForm.value.course;
        userData.session=this.studentRegForm.value.session;
        userData.center=this.studentRegForm.value.center;
        userData.productCategory=this.selectedProduct.category.id;
        if(this.selectedProduct.haveBatch){
            if(!this.studentRegForm.value.batch||this.studentRegForm.value.batch==null){
                this.fh.error="Please select Batch first!";
                return;
            }
            userData.batch=this.studentRegForm.value.batch;
        }
        userData.dob=moment(userData.dob).format("DD/MM/YYYY");
        let restOrderData: any = {
            stream: userData.stream,
            course: userData.course,
            center: userData.center,
            session: userData.session,
            productCategory: userData.productCategory,
            productType: this.selectedProduct.type.id
        };
        if (userData.batch) {
            restOrderData.batch = this.selectedProduct.batches[userData.batch].id
        }
        if (this.selectedProduct&&this.selectedProduct.store) {
            restOrderData.store =this.selectedProduct.store;
        }
        let stdData:any={
            createLogin:true,
            student:userData,
            user:{
                password:Math.random()*100000|0
            }
        }
        this.studentService.addStudent(stdData).subscribe(
            res=>{
                this.user=res.user;
                let evtData={
                    student:res,
                    restOrderData: restOrderData,
                    selectedProduct:this.selectedProduct
                }
                this.onStudentRegistered.emit(evtData);
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
    }
    
}