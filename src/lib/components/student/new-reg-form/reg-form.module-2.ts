import { NgModule } from '@angular/core';
import { TabViewModule, CalendarModule, FieldsetModule } from 'primeng/primeng';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StudentRegForm2Component } from './reg-form.component-2';
import { UserSearchFormModule } from '../../user-search-form/user-search-form.module';
import { FileinputModule } from '../../filemanager/fileinput.module';
import { UserService } from '../../../services/user.service';
import { SessionService } from '../../../services/session.service';
import { CoptionService } from '../../../services/coption.service';
import { CenterService } from '../../../services/center.service';
import { BatchService } from '../../../services/batch.service';
import { ProductService } from '../../../services/product.service';
import { StudentService } from '../../../services/student.service';
import { SettingsService } from '../../../services/settings.service';


@NgModule({
    declarations:[StudentRegForm2Component],
    imports: [FileinputModule, TabViewModule, CalendarModule, UserSearchFormModule, CommonModule, FormsModule, ReactiveFormsModule, FieldsetModule],
    exports:[StudentRegForm2Component],
    providers:[UserService, SessionService, CoptionService, CenterService, BatchService, ProductService, StudentService, SettingsService]
})
export class StudentRegForm2Module{

}