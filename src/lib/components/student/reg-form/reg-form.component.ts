import { Component, OnInit, Input, EventEmitter, Output, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { User } from '../../../models/user.model';
import { Product } from '../../../models/product.model';
import { UserService } from '../../../services/user.service';
import { NotifierService } from '../../notifier/notifier.service';
import { SessionService } from '../../../services/session.service';
import { CoptionService } from '../../../services/coption.service';
import { CenterService } from '../../../services/center.service';
import { ProductService } from '../../../services/product.service';
import { StudentService } from '../../../services/student.service';
import { SettingsService } from '../../../services/settings.service';

@Component({
    selector:'ek-student-reg-form',
    templateUrl:'./reg-form.component.html'
})
export class StudentRegFormComponent implements OnInit{
    studentRegForm:FormGroup;
    userData:User;
    formStatus:string="Normal";
    resetPassword:boolean=false;
    courses:any[]=[];
    productTypes:any[]=[];
    products:Product[];
    selectedProduct:Product;
    validationError:string;
    paymentMethod:string="";
    paymentOption:string="";
    fh:any={
        personalImg:'https://iesmaster.org/public/images/dummy-photo.png',
        signImg:'https://iesmaster.org/public/images/dummy-photo-sign.png',
        status:'Normal',
        error:null
    };
    formSettings:any;
    // myDatePickerOptions: IMyDpOptions = {
    //     // other options...
    //     dateFormat: 'dd/mm/yyyy',
    // };
    stdHavePastExams:boolean=false;
    isExStd:boolean=false;
    pastCrses:number[]=[0,1,2];
    @Output() onStudentRegistered:EventEmitter<any>=new EventEmitter<any>();
    @Input() submitLabel:string='Save';
    @Input() userSearchOpt:boolean=false;
    @Input() defaultUserValue:any;
    @Input() formCourses:any[];//=["ESE+GATE"];  //Course Coptio Values
    @Input() formProductTypes:any[];//=["Postal Book Program"];  //ProductType Coptio Values
    @Input() streams:any[]//=['CE', 'ME']; //'EE', 'EC'
    @Input() centers:any[]//=['centerId'];
    @Input() sessions:any[]//=['sessionId'];
    @Input('user')
    set user(u){
        if(this.studentRegForm){
            if(u){
                let uData:any={};
                uData.firstName=u.firstName;
                uData.lastName=u.lastName;
                uData.email=u.email;
                uData.mobile=u.mobile;
                uData.personalImg=u.profileImg;
                if(!u.address){
                    uData.address={};
                    uData.permanantAddress={};
                }else{
                    if(!u.address.shipping){
                        uData.address={address:'', landmark:'', city:'', state:'', postalCode:'', country:'India'};
                    }else{
                        uData.address=u.address.shipping
                    }
                    if(!u.address.billing){
                        uData.permanantAddress={address:'', landmark:'', city:'', state:'', postalCode:'', country:'India'};
                    }else{
                        uData.permanantAddress=u.address.billing
                    }
                }
                this.studentRegForm.patchValue({user:uData});
                this.userData=u;
                this.resetPassword=false;
            }else{
                this.studentRegForm.patchValue({user:{}});
                this.userData=null;
                this.resetPassword=true;
            }
        }
            
    }
    constructor(
		private fb:FormBuilder,
		private userService:UserService,
        private notifier: NotifierService,
        private el: ElementRef,
        private sessionService:SessionService,
        private coptionService:CoptionService,
        private centerService:CenterService,
        private productService:ProductService,
        private studentService:StudentService,
        private settingService:SettingsService
	){ 
			
    }
    ngOnInit():void{
        this.studentRegForm=this.fb.group({
            center:['', Validators.required],
            course:['', Validators.required],
            productType:['', Validators.required],
            stream:['', Validators.required],
            session:['', Validators.required],
            product:['', Validators.required],
            batch:[''],
            user:this.fb.group({
                firstName:[''], 
                lastName:[''],
                mobile:[''],
                email:[''],
                category:[''],
                gender:[''],
                dob:[null],
                personalImg:[''],
                signImg:[''],
                father:this.fb.group({
                    name:[''],
                    mobile:[''],
                }),
                mother:this.fb.group({
                    name:['']
                }),
                address:this.fb.group({
                    address:[''],
                    landmark:[''],
                    city:[''],
                    state:[''],
                    postalCode:[''],
                    country:['India'],
                }),
                permanantAddress:this.fb.group({
                    address:[''],
                    landmark:[''],
                    city:[''],
                    state:[''],
                    postalCode:[''],
                    country:['India']
                }),
                exStudent:this.fb.group({
                    srn:[''],
                    session:[''],
                    idProof:['']
                }),
                pastCourses:this.fb.array([
                    this.initPastCourseFrm("B.E./B.Tech.", true),
                    this.initPastCourseFrm("M.E./M.Tech."),
                    this.initPastCourseFrm("Other")
                ]),
                pastExams:this.fb.array([
                    this.initPastExamFrm("GATE"),
                    this.initPastExamFrm("ESE")
                ])
            }),
            cart:this.fb.group({
                discountCode:['']
            })
        });
        let self=this;
        window.setTimeout(function(){
            self.setInit();
        }, 1);
    }

    setInit(){
        
        this.loadCenters();
        this.loadCourse();
        this.loadProductTypes();
        this.loadStreams();
        this.loadSessions();
        this.loadSettings();
    }
    loadSettings(){
        this.settingService.getSettingByKey('ADMISSION_SETTING').subscribe(
            (res)=>{
                this.formSettings=res.more;

            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    initPastCourseFrm(crsName:string, required?:boolean){
        return this.fb.group({
            course:[crsName, (required?Validators.required:null)],
            courseDuration:[''],
            college:['', (required?Validators.required:null)],
            year:['', (required?Validators.required:null)],
            marks:['', (required?Validators.required:null)],
            remarks:[''],
        })
    }

    initPastExamFrm(examName:string){
        return this.fb.group({
            name:[examName],
            year:['2016-17'],
            rank:[''],
            remarks:[''],
        })
    }
    
    loadSessions(){
        let filter:any={status:true};
        if(this.sessions){
            filter.id=this.sessions;
        }
        this.sessionService.getSession(filter).subscribe(
            res=>{
                
                this.sessions=res;
            },
            err=>{
                console.log(err);
            }
        );
    }

    loadCenters(){
        let filter:any={status:true};
        if(this.centers){
            filter.id=this.centers;
        }
        this.centerService.getCenter(filter).subscribe(
            res=>{
                
                this.centers=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    loadCourse(){
        this.coptionService.getCoption({option:'COURSE', status:true}).subscribe(
            res=>{
                if(this.formCourses){
                    var self=this;
                    res=res.filter(function(crs:any){
                        for(let ci=0; ci<self.formCourses.length; ci++){
                            if(crs.value==self.formCourses[ci]){
                                return true;
                            }
                        } 
                        return false;
                    })
                }
                this.courses=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    loadProductTypes(){
        this.coptionService.getCoption({option:'PRODUCTTYPE', status:true}).subscribe(
            res=>{
                if(this.formProductTypes){
                    var self=this;
                    res=res.filter(function(pt:any){
                        for(let ci=0; ci<self.formProductTypes.length; ci++){
                            if(pt.value==self.formProductTypes[ci]){
                                return true;
                            }
                        } 
                        return false;
                    })
                }
                this.productTypes=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    loadStreams(){
        this.coptionService.getCoption({option:'STREAM', status:true}).subscribe(
            res=>{
                if(this.streams){
                    var self=this;
                    res=res.filter(function(strm:any){
                        for(let ci=0; ci<self.streams.length; ci++){
                            if(strm.value==self.streams[ci]){
                                return true;
                            }
                        } 
                        return false;
                    })
                }
                this.streams=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    checkProductDepParams(){
        let frmData=this.studentRegForm.value;
        if(frmData.course==""||frmData.productType==""||frmData.stream==""||frmData.session==""||frmData.center==""){
            alert("Please select Center, Course, Program, Session & Stream First!");
            return;
        }
    }
    setProductDepParams(){
        this.selectedProduct=this.products[this.studentRegForm.value.product];
    }
    loadProducts(){
        let frmData=this.studentRegForm.value;
        if(frmData.course==""||frmData.productType==""||frmData.stream==""||frmData.center==""||frmData.session==""){
            return;
        }
        let filter={
            center:frmData.center,
            course:frmData.course,
            productType:frmData.productType,
            stream:frmData.stream,
            session:frmData.session,
            populateBatches:true,
            status:true
        };

        this.productService.getProduct(filter).subscribe(
            res=>{
                this.products=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    setAddressSimilarity(el:any){
        if(el.target.checked){
            let corsAdrs=this.studentRegForm.value.user.address;
            this.studentRegForm.patchValue({user:{permanantAddress:corsAdrs}});
        }
    }

    getValue(arr:any[], id:string){
        arr.filter(function(el){
            if(el.id==id){
                return true;
            }
            return false;
        });
        return arr[0];
    }

    generatePassword(){
        // this.regForm.patchValue({password:Math.floor((Math.random() * 100000) + 1)});
    }

    setPasswordOpt(state){
        this.resetPassword=state;
    }

    setBillingAddress(sameAsShipping){
        if(sameAsShipping){
            // this.regForm.patchValue({address:{billing:this.regForm.value.address.shipping}});
        }else{
            // this.regForm.patchValue({address:{billing:((this.userData&&this.userData.address&&this.userData.address.billing)?this.userData.address.billing:{address:'', landmark:'', city:'', state:'', postalCode:'', country:'India'})}});
        }
    }

    onUserSelect(user){
		this.userService.getOneUser(user.id).subscribe(
			res=>{
                this.user=res;
                
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
    }
    
    addUpdateStudent(placeOrder?:boolean){
        let userData=this.studentRegForm.value.user;
        if(userData.personalImg==""||userData.signImg==""){
            this.fh.error="Personal Image and Sign Image are mandatory!";
            return;
        }
        if (!userData.firstName && this.formSettings && this.formSettings.firstName.mandatory) {
            this.fh.error = "Please Enter First name!";
            return;
        }
        if (!userData.lastName && this.formSettings && this.formSettings.lastName.mandatory) {
            this.fh.error = "Please Enter last name!";
            return;
        }
        if (!userData.email && this.formSettings && this.formSettings.email.mandatory) {
            this.fh.error = "Please Enter email address!";
            return;
        }
        if (!userData.mobile && this.formSettings && this.formSettings.mobile.mandatory) {
            this.fh.error = "Please Enter Mobile  Number!";
            return;
        }
        if (!userData.dob && this.formSettings && this.formSettings.dob.mandatory) {
            this.fh.error = "Please Enter Date of Birth!";
            return;
        }
        if (!userData.category && this.formSettings && this.formSettings.category.mandatory) {
            this.fh.error = "Please choose your category!";
            return;
        }
        if (userData.father&&!userData.father.name && this.formSettings && this.formSettings.father && this.formSettings.father.mandatory && this.formSettings.father.fields&&this.formSettings.father.fields.name&&this.formSettings.father.fields.name.mandatory) {
            this.fh.error = "Please Enter Father's Name!";
            return;
        }
        if (userData.father && !userData.father.mobile && this.formSettings && this.formSettings.father && this.formSettings.father.mandatory && this.formSettings.father.fields && this.formSettings.father.fields.mobile && this.formSettings.father.fields.mobile.mandatory) {
            this.fh.error = "Please Enter Father's Mobile Number!";
            return;
        }


        userData.stream=this.studentRegForm.value.stream;
        userData.course=this.studentRegForm.value.course;
        userData.session=this.studentRegForm.value.session;
        userData.center=this.studentRegForm.value.center;
        if(this.selectedProduct.haveBatch){
            if(!this.studentRegForm.value.batch||this.studentRegForm.value.batch==null){
                this.fh.error="Please select Batch first!";
                return;
            }
            userData.batch=this.studentRegForm.value.batch;
        }
        userData.dob=moment(userData.dob).format("DD/MM/YYYY");
        let stdData:any={
            createLogin:true,
            student:userData,
            user:{
                password:Math.random()*100000|0
            }
        }

        this.studentService.addStudent(stdData).subscribe(
            res=>{
                this.user=res.user;
                let evtData={
                    student:res,
                    selectedProduct:this.selectedProduct
                }
                this.onStudentRegistered.emit(evtData);
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
    }
    
    checkValidation(key, ){
        if (key) {
            
        }
    }
}