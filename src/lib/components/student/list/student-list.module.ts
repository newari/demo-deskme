import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { StudentListComponent } from './student-list.component';
import { PaginatorModule } from 'primeng/components/paginator/paginator';
import { StudentService } from '../../../services/student.service';
import { SessionService } from '../../../services/session.service';
import { CoptionService } from '../../../services/coption.service';
import { CenterService } from '../../../services/center.service';
import { ProductService } from '../../../services/product.service';
import { ProductInputFieldModule } from '../../product-inputfield/product-inputfield.module';
import { DialogModule } from 'primeng/primeng';
import { TestMentService } from '../../../services/testment.service';


@NgModule({
    declarations:[StudentListComponent],
    imports:[CommonModule, PaginatorModule,DialogModule, ProductInputFieldModule, RouterModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[StudentListComponent],
    providers:[StudentService, SessionService, CoptionService, CenterService, ProductService,TestMentService]
})
export class StudentListModule { 
    
}
