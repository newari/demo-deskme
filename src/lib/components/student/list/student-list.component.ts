import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Coption } from '../../../models/coption.model';
import { Product } from '../../../models/product.model';
import { StudentService } from '../../../services/student.service';
import { NotifierService } from '../../notifier/notifier.service';
import { SessionService } from '../../../services/session.service';
import { CoptionService } from '../../../services/coption.service';
import { CenterService } from '../../../services/center.service';
import { ProductService } from '../../../services/product.service';
import { EdukitConfig } from '../../../../ezukit.config';
import { AuthService } from '../../../services/auth.service';
import { TestMentService } from '../../../services/testment.service';

@Component({
    selector:'ek-student-list',
    templateUrl:'./student-list.component.html'
})
export class StudentListComponent implements OnInit{ 
    rows;
    columns; 
    productFilterForm : FormGroup;
    panelLoader="none";
    activeFilter:any;
    countBase:number=1;
    totalRecords:number;

    advanceFilterForm:FormGroup;
    courses:Coption[];
    streams:Coption[];
    products:Product[];
    selectedProduct:Product;

    stdSearchForm:FormGroup;
    selectedStds:number[]=[]; //arr of index;
    multiActionType:string;
    FCB={  //FooterControlsBox
        status:'hide',
        activeControls:'none'
    };
    userPerms:any;

    @Input() APP:string;
    @Input() ACTIVITY:string;
    @Input() appUri:string="student-manager";
    @Input() userSearchOpt:boolean=false;
    @Input() defaultUserValue:any;
    @Input() formCourses:any[];//=["ESE+GATE"];  //Course Coptio Values
    @Input() formProductTypes:any[];//=["Postal Book Program"];  //ProductType Coptio Values
    @Input() formStreams:any[];//=['CE', 'ME']; //'EE', 'EC'
    @Input() centers:any[];//=['centerId'];
    @Input() sessions:any[];//=['sessionId'];
    @Input() showAdvanceFilter:boolean
    showAssignModal: boolean;
    set(val){
        
    }
    constructor(
        private studentService:StudentService,
        private fb: FormBuilder,
        private notifier: NotifierService,
        private sessionService:SessionService,
        private coptionService:CoptionService,
        private centerService:CenterService,
        private productService:ProductService,
        private authService:AuthService,
        private testmentService:TestMentService
    ){}

    ngOnInit(): void{

        this.stdSearchForm=this.fb.group({
            srn:[""],
            email:[""],
            firstName:[""],
            mobile:[""]
        });

        this.advanceFilterForm=this.fb.group({
            from:[""],
            to:[""],
            session:[null],
            center:[null],
            course:[null],
            stream:[null],
            product:[null],
            batch:[null]
        });
        this.productFilterForm = this.fb.group({
            course: [null],
            stream: [null],
            product: [''],
        });
        this.loadStudents();

        let self=this;
        window.setTimeout(function(){
            self.setInit();
        }, 1);
        let sessionUser=this.authService.admin();
        this.userPerms=this.authService.getUserPerms(sessionUser.activityPermission, this.APP, this.ACTIVITY, ['EXPORT']);
        
    }

    setInit(){
        
        this.loadCenters();
        this.loadCourse();
        this.loadStreams();
        this.loadSessions();
    }

    loadSessions(){
        let filter:any={status:true};
        if(this.sessions){
            filter.id=this.sessions;
        }
        this.sessionService.getSession(filter).subscribe(
            res=>{
                
                this.sessions=res;
            },
            err=>{
                console.log(err);
            }
        );
    }

    loadCenters(){
        let filter:any={status:true};
        if(this.centers){
            filter.id=this.centers;
        }
        this.centerService.getCenter(filter).subscribe(
            res=>{
                
                this.centers=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    loadCourse(){
        this.coptionService.getCoption({option:'COURSE', status:true}).subscribe(
            res=>{
                if(this.formCourses){
                    var self=this;
                    res=res.filter(function(crs:any){
                        for(let ci=0; ci<self.formCourses.length; ci++){
                            if(crs.value==self.formCourses[ci]){
                                return true;
                            }
                        } 
                        return false;
                    })
                }
                this.courses=res;
            },
            err=>{
                console.log(err);
            }
        );
    }

    loadStreams(){
        this.coptionService.getCoption({option:'STREAM', status:true}).subscribe(
            res=>{
                if(this.formStreams){
                    var self=this;
                    res=res.filter(function(strm:any){
                        for(let ci=0; ci<self.formStreams.length; ci++){
                            if(strm.value==self.formStreams[ci]){
                                return true;
                            }
                        } 
                        return false;
                    })
                }
                this.streams=res;
            },
            err=>{
                console.log(err);
            }
        );
    }

    loadProducts(){
        let frmData=this.advanceFilterForm.value;
        if(!frmData.stream){
            return;
        }
        let filter:any={
            stream:frmData.stream,
            populateBatches:true,
            status:true
        };

        if(frmData.course){
            filter.course=frmData.course;
        }
        
        if(frmData.session){
            filter.session=frmData.session;
        }

        
        if(frmData.center){
            filter.center=frmData.center;
        }

        this.productService.getProduct(filter).subscribe(
            res=>{
                this.products=res;
            },
            err=>{
                console.log(err);
            }
        );
    }

    checkProductDepParams(){
        let frmData=this.advanceFilterForm.value;
        if(frmData.stream==""){
            alert("Please select Stream First!");
            return;
        }
    }

    // setProductDepParams(){
    //     this.selectedProduct=this.products[this.advanceFilterForm.value.mainProduct];
    // }

    loadStudents(filter?:any):void{
        this.panelLoader="Processing";
        this.selectedStds=[];
        this.studentService.getStudent(filter, true).subscribe(
            (data)=>{
                this.rows=data.body;
                this.totalRecords = data.headers.get('totalRecords')||0;
                this.panelLoader="none";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000);
                this.panelLoader="none";
                
            }
        );
        
    }

    loadStudentsWithAdvanceFilter(){
        let fltr=this.advanceFilterForm.value;
        
        this.activeFilter=fltr;
        this.countBase=1;
        this.loadStudents(fltr);
    }

    deleteStudent(student){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete "+student.name+"?");
        if(!confirm){
            return;
        }
        this.studentService.deleteStudent(student.id).subscribe(
            (res)=>{
                this.rows.splice(this.rows.indexOf(student), 1);
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );

            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }

    searchStudent(){
        let data=this.stdSearchForm.value;
        // if(data.srn.trim()==""&&data.email.trim()==""&&data.mobile.trim()==""&&data.firstName.trim()==""){
        //     return;
        // }
        let filter:any={};
        if(data.srn.trim()!=""){
            filter.srn=data.srn.trim();
        }
        if(data.email.trim()!=""){
            filter.email=data.email.trim();
        }
        if(data.mobile.trim()!=""){
            filter.mobile=data.mobile.trim();
        }
        if(data.firstName.trim()!=""){
            filter.firstName=data.firstName.trim();
        }
        this.activeFilter=filter;
        this.loadStudents(filter);

    }

    exportData(){
        let fltr:any={};
        if(this.activeFilter){
            fltr=this.activeFilter;
        }
        
        fltr.limit='all';
        this.studentService.exportStudents(fltr).subscribe(
            res=>{
                this.notifier.alert("Done!", "Data exported successfully.", "success", 5000);
                window.location.href=res.fileUrl;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'daner', 10000);
            }
        )
    }

    paginate(e){
        if(!this.activeFilter){
            this.activeFilter={};
        }
        this.countBase=e.rows*e.page+1;
        this.activeFilter.page=(e.page+1);
        this.activeFilter.limit=e.rows;
        this.loadStudents(this.activeFilter);
    }
    toggleStdSelection(stdIndex, e){
        if(e.target.checked){
            this.selectedStds.push(stdIndex);
            this.FCB.activeControls='MULTISELECT';
            this.FCB.status="show";
        }else{
            let ind=this.selectedStds.indexOf(stdIndex);
            this.selectedStds.splice(ind, 1);
            if(this.selectedStds.length<1){
                this.FCB.status="hide";
            }
        }

    }
    toggleStdSelectionAll(e){
        if(e.target.checked){
            this.selectedStds=[];
            for(let si=0; si<this.rows.length; si++){
                this.selectedStds.push(si);
            }
            this.FCB.activeControls='MULTISELECT';
            this.FCB.status="show";
        }else{
            this.selectedStds=[];
            this.FCB.status="hide";
        }

    }
    unSelectAll(){
        this.selectedStds=[];
        this.FCB.status="hide";
        
    }

    getSelectedStdIds(){
        let ids=[];
        for(let si=0; si<this.selectedStds.length; si++){
            ids.push(this.rows[this.selectedStds[si]].id);
        }
        return ids;
    }
    getSelectedStdUserIds(){
        let ids=[];
        for(let si=0; si<this.selectedStds.length; si++){
            ids.push(this.rows[this.selectedStds[si]].user);
        }
        return ids;
    }

    bulkAction(actionType){
        if(actionType=="DOWNLOAD-ID-CARD"){
            this.downloadIDCardBulk();
        }
        if (actionType == "Assign-Product") {
            this.showAssignModal=true;
        }
    }

    downloadIDCardBulk(){
        if(this.selectedStds.length<1){
            this.notifier.alert("Ooops!", "No student is selected!", "danger", 10000);
            return;
        }
        let stdIds=this.getSelectedStdIds();
        let idsStr=stdIds.join(",");
        window.open(EdukitConfig.BASICS.API_URL+"/student-manager/student/id-cards?sids="+idsStr);
        
    }
    onProductSelect(prdct){
        this.productService.getOneProduct(prdct.id).subscribe(
            res=>{
                this.selectedProduct=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }
    setBatchRollNo(stdIndex){
        this.panelLoader="show";
        this.studentService.setBatchRollNo(this.rows[stdIndex].id,this.rows[stdIndex].mainBtach.id).subscribe(
            res=>{
                this.panelLoader="none";
                this.rows[stdIndex].batchRollNo=res.batchRollNo;
                this.notifier.alert('Done!', "Batch Roll Number created successfully!", 'success', 500 );
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 10000 );
            }
        )
    }

    setBlockStatus(stdIndex, status){
        let blocked:boolean= status==='BLOCK'?true:false;
        this.studentService.updateStudent(this.rows[stdIndex].id, {isBlocked:blocked}).subscribe(
            res=>{
                if(res)
                this.rows[stdIndex].isBlocked=res.isBlocked
            }
        );
    }

    assignProduct(){
        if(this.selectedStds.length < 1){
            return this.notifier.alert('danger','No Student Selected','danger',5000);
        }
        if(!this.productFilterForm.value.product){
            return this.notifier.alert('danger','No Product Selected','danger',5000);
        }
        let stdIds = this.getSelectedStdUserIds();
        let data={
            user : stdIds,
            product : this.productFilterForm.value.product
        }
        this.testmentService.assignProductToMultiUser(data).subscribe(
            (res)=>{
                this.showAssignModal=false;
                this.notifier.alert('success','Product Assigned Successfully','success', 10000);
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000);
                this.panelLoader="none";
            }
        );
    }
}
