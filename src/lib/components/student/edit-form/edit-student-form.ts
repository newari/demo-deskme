import { Component, OnInit, Input, EventEmitter, Output, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import * as moment from 'moment';
import { User } from '../../../models/user.model';
import { Product } from '../../../models/product.model';
import { UserService } from '../../../services/user.service';
import { NotifierService } from '../../notifier/notifier.service';
import { SessionService } from '../../../services/session.service';
import { CoptionService } from '../../../services/coption.service';
import { CenterService } from '../../../services/center.service';
import { ProductService } from '../../../services/product.service';
import { StudentService } from '../../../services/student.service';
import { SettingsService } from '../../../services/settings.service';

@Component({
    selector: 'ek-student-edit-form',
    templateUrl: './edit-student-form.html'
})
export class StudentEditFormComponent implements OnInit {
    studentEditForm: FormGroup;
    userData: User;
    studentData;
    formStatus: string = "Normal";
    resetPassword: boolean = false;
    validationError: string;
    paymentMethod: string = "";
    paymentOption: string = "";
    fh: any = {
        personalImg: 'https://iesmaster.org/public/images/dummy-photo.png',
        signImg: 'https://iesmaster.org/public/images/dummy-photo-sign.png',
        status: 'Normal',
        error: null
    };
    formSettings: any;
    // myDatePickerOptions: IMyDpOptions = {
    //     // other options...
    //     dateFormat: 'dd/mm/yyyy',
    // };
    stdHavePastExams: boolean = false;
    isExStd: boolean = false;
    pastCrses: number[] = [0, 1, 2];
    @Input() submitLabel: string = 'Save';
    @Input() userSearchOpt: boolean = false;
    @Input() defaultUserValue: any;
     
    @Input() studentId:any
    siblings: any;
     
    constructor(
        private fb: FormBuilder,
        private userService: UserService,
        private notifier: NotifierService,
        private el: ElementRef,
        private sessionService: SessionService,
        private coptionService: CoptionService,
        private centerService: CenterService,
        private productService: ProductService,
        private studentService: StudentService,
        private settingService: SettingsService
    ) {

    }
    ngOnInit(): void {
        let self = this;
        window.setTimeout(function () {
            self.setInit();
        }, 1);
    }
    setInit() {
        this.loadSettings();
    }
    loadSettings() {
        this.settingService.getSettingByKey('ADMISSION_SETTING').subscribe(
            (res) => {
                this.formSettings = res.more;
                if (this.formSettings) {
                    this.studentEditForm = this.fb.group({
                        firstName: ['', Validators.required],
                        lastName: ['', (this.formSettings.lastName.mandatory ? Validators.required : '')],
                        middleName: ['', (this.formSettings.middleName.mandatory ? Validators.required : '')],
                        mobile: ['', (this.formSettings.mobile.mandatory ? Validators.required : '')],
                        email: ['', Validators.required],
                        category: ['null', (this.formSettings.category.mandatory ? Validators.required : '')],
                        gender: ['null', (this.formSettings.gender.mandatory ? Validators.required : '')],
                        dob: [null, (this.formSettings.dob.mandatory ? Validators.required : '')],
                        personalImg: ['', (this.formSettings.personalImg.mandatory ? Validators.required : '')],
                        signImg: ['', (this.formSettings.signImg.mandatory ? Validators.required : '')],
                        father: this.fb.group({
                            name: ['', (this.formSettings.father.fields.name.mandatory ? Validators.required : '')],
                            mobile: ['', (this.formSettings.father.fields.mobile.mandatory ? Validators.required : '')],
                            email: ['', (this.formSettings.father.fields.email.mandatory ? Validators.required : '')],
                            qualification: ['', (this.formSettings.father.fields.qualification.mandatory ? Validators.required : '')],
                            designation: ['', (this.formSettings.father.fields.designation.mandatory ? Validators.required : '')],
                            occupation: ['', (this.formSettings.father.fields.occupation.mandatory ? Validators.required : '')],
                            officeAddress: ['', (this.formSettings.father.fields.officeAddress.mandatory ? Validators.required : '')],
                            age: ['', (this.formSettings.father.fields.age.mandatory ? Validators.required : '')],
                            photo: ['', (this.formSettings.father.fields.photo.mandatory ? Validators.required : '')],
                            nationality: ['INDIAN', (this.formSettings.father.fields.nationality.mandatory ? Validators.required : '')],
                            aadhaarNo: ['', (this.formSettings.father.fields.aadhaarNo.mandatory ? Validators.required : '')],
                            annualIncome: ['', (this.formSettings.father.fields.annualIncome.mandatory ? Validators.required : '')]

                        }),
                        mother: this.fb.group({
                            name: ['', (this.formSettings.mother.fields.name.mandatory ? Validators.required : '')],
                            mobile: ['', (this.formSettings.mother.fields.mobile.mandatory ? Validators.required : '')],
                            email: ['', (this.formSettings.mother.fields.email.mandatory ? Validators.required : '')],
                            qualification: ['', (this.formSettings.mother.fields.qualification.mandatory ? Validators.required : '')],
                            designation: ['', (this.formSettings.mother.fields.designation.mandatory ? Validators.required : '')],
                            occupation: ['', (this.formSettings.mother.fields.occupation.mandatory ? Validators.required : '')],
                            officeAddress: ['', (this.formSettings.mother.fields.officeAddress.mandatory ? Validators.required : '')],
                            age: ['', (this.formSettings.mother.fields.age.mandatory ? Validators.required : '')],
                            photo: ['', (this.formSettings.mother.fields.photo.mandatory ? Validators.required : '')],
                            nationality: ['INDIAN', (this.formSettings.mother.fields.nationality.mandatory ? Validators.required : '')],
                            aadhaarNo: ['', (this.formSettings.mother.fields.aadhaarNo.mandatory ? Validators.required : '')],
                            annualIncome: ['', (this.formSettings.mother.fields.annualIncome.mandatory ? Validators.required : '')]
                        }),
                        siblings: this.fb.array([]),
                        urbanOrRural: ['URBAN', (this.formSettings.urbanOrRural.mandatory ? Validators.required : '')],
                        motherTongue: ['HINDI', (this.formSettings.motherTongue.mandatory ? Validators.required : '')],
                        nationality: ['INDIAN', (this.formSettings.nationality.mandatory ? Validators.required : '')],
                        languageKnown: ['HINDI', (this.formSettings.languageKnown.mandatory ? Validators.required : '')],
                        aadhaarNo: ['', (this.formSettings.aadhaarNo.mandatory ? Validators.required : '')],
                        religion: ['HINDU', (this.formSettings.religion.mandatory ? Validators.required : '')],
                        bloodGroup: ['null', (this.formSettings.bloodGroup.mandatory ? Validators.required : '')],
                        address: this.fb.group({
                            address: ['', (this.formSettings.address.fields.address.mandatory ? Validators.required : '')],
                            landmark: [''],
                            city: [''],
                            state: [''],
                            postalCode: [''],
                            country: ['India'],
                        }),
                        permanantAddress: this.fb.group({
                            address: ['', (this.formSettings.address.fields.permanentAddress.mandatory ? Validators.required : '')],
                            landmark: [''],
                            city: [''],
                            state: [''],
                            postalCode: [''],
                            country: ['India']
                        }),
                        exStudent: this.fb.group({
                            srn: ['', (this.formSettings.exStudent.fields.srn.mandatory ? Validators.required : '')],
                            session: ['', (this.formSettings.exStudent.fields.session.mandatory ? Validators.required : '')],
                            idProof: ['', (this.formSettings.exStudent.fields.idProof.mandatory ? Validators.required : '')],
                            program: ['', (this.formSettings.exStudent.fields.program.mandatory ? Validators.required : '')]
                        }),
                        pastCourses: this.fb.array([
                            this.initPastCourseFrm("", true),
                            this.initPastCourseFrm(""),
                            this.initPastCourseFrm("Other")
                        ]),
                        pastExams: this.fb.array([
                            this.initPastExamFrm("GATE"),
                            this.initPastExamFrm("ESE")
                        ]),
                        emergencyContact: this.fb.group({
                            name: ['', (this.formSettings.emergencyContact.fields.name.mandatory ? Validators.required : '')],
                            mobile: ['', (this.formSettings.emergencyContact.fields.mobile.mandatory ? Validators.required : '')],
                            relationship: ['', (this.formSettings.emergencyContact.fields.relationship.mandatory ? Validators.required : '')],
                        }),
                        medicalHistory: this.fb.group({
                            birthHistory: this.fb.group({
                                birthDetail: ['', (this.formSettings.medicalHistory.birthHistory.fields.birthDetail.mandatory ? Validators.required : '')],
                                birthCry: ['', (this.formSettings.medicalHistory.birthHistory.fields.birthCry.mandatory ? Validators.required : '')],
                                dischargeInDays: ['', (this.formSettings.medicalHistory.birthHistory.fields.dischargeInDays.mandatory ? Validators.required : '')],
                                specialCare: ['', (this.formSettings.medicalHistory.birthHistory.fields.specialCare.mandatory ? Validators.required : '')],
                                ifNICU: ['',],
                                explaination: ['',],
                            }),
                            hearing: this.fb.group({
                                difficulty: ['', (this.formSettings.medicalHistory.hearing.fields.difficulty.mandatory ? Validators.required : false)],
                                consultation: ['', (this.formSettings.medicalHistory.hearing.fields.consultation.mandatory ? Validators.required : false)],
                                explaination: ['']
                            }),
                            vision: this.fb.group({
                                useLenses: ['', (this.formSettings.medicalHistory.vision.fields.useLenses.mandatory ? Validators.required : false)],
                                consultation: ['', (this.formSettings.medicalHistory.vision.fields.consultation.mandatory ? Validators.required : false)],
                                explaination: ['']

                            }),
                            motorMileStone: this.fb.group({
                                sitting: ['', (this.formSettings.medicalHistory.motorMileStone.fields.sitting.mandatory ? Validators.required : false)],
                                standing: ['', (this.formSettings.medicalHistory.motorMileStone.fields.standing.mandatory ? Validators.required : false)],
                                walking: ['', (this.formSettings.medicalHistory.motorMileStone.fields.walking.mandatory ? Validators.required : false)],
                                speech: ['', (this.formSettings.medicalHistory.motorMileStone.fields.speech.mandatory ? Validators.required : false)],
                                medicationForMedicalCondition: ['', (this.formSettings.medicalHistory.motorMileStone.fields.medicationForMedicalCondition.mandatory ? Validators.required : false)],
                                medicationForWellBeing: ['', (this.formSettings.medicalHistory.motorMileStone.fields.medicationForWellBeing.mandatory ? Validators.required : false)],
                                medicationForAlergy: ['', (this.formSettings.medicalHistory.motorMileStone.fields.medicationForAlergy.mandatory ? Validators.required : false)],
                            })
                        }),
                        miscellaneous: this.fb.group({
                            howDidHearAboutInstitue:this.fb.group({
                                website:[''],
                                magzine:[''],
                                newsPaper:[''],
                                others:['']
                            })
                        }),
                        distanceFromSchool: ['', (this.formSettings.distanceFromSchool.mandatory ? Validators.required : '')]

                    }) 
                }
                if (this.studentId) {
                    this.loadStudent();
                }
            },
            (err) => {
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    loadStudent(){
        this.studentService.getOneStudent(this.studentId).subscribe(
            (res)=>{
                this.studentData=res;
                let data:any={}= res;
                if (data.customParams) {
                    if (data.customParams.emergencyContact) {
                        data.emergencyContact = data.customParams.emergencyContact
                    }
                    if (data.customParams.medicalHistory) {
                        data.medicalHistory = data.customParams.medicalHistory;
                    }
                    if (data.customParams.urbanOrRural) {
                         data.urbanOrRural=data.customParams.urbanOrRural;
                    }
                    if (data.customParams.motherTongue) {
                         data.motherTongue=data.customParams.motherTongue;
                    }
                    if (data.customParams.nationality) {
                         data.nationality=data.customParams.nationality;
                    }
                    if (data.customParams.languageKnown) {
                         data.languageKnown=data.customParams.languageKnown;
                    }
                    if (data.customParams.aadhaarNo) {
                         data.aadhaarNo=data.customParams.aadhaarNo;
                    }
                    if (data.customParams.religion) {
                         data.religion=data.customParams.religion;
                    }                
                    if (data.customParams.bloodGroup) {
                         data.bloodGroup=data.customParams.bloodGroup;
                    }                 
                }
                if (res.siblings) {
                    this.createSiblingByInput(res.siblings.length);
                    
                }
                this.studentEditForm.patchValue(res);
            },
            (err) => {
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    initPastCourseFrm(crsName: string, required?: boolean) {
        return this.fb.group({
            course: [crsName, (required ? Validators.required : null)],
            courseDuration: [''],
            college: ['', (required ? Validators.required : null)],
            year: ['', (required ? Validators.required : null)],
            marks: ['', (required ? Validators.required : null)],
            remarks: [''],
        })
    }

    initPastExamFrm(examName: string) {
        return this.fb.group({
            name: [examName],
            year: ['2016-17'],
            rank: [''],
            remarks: [''],
        })
    }

    setAddressSimilarity(el: any) {
        if (el.target.checked) {
            let corsAdrs = this.studentEditForm.value.user.address;
            this.studentEditForm.patchValue({ user: { permanantAddress: corsAdrs } });
        }
    }

    getValue(arr: any[], id: string) {
        arr.filter(function (el) {
            if (el.id == id) {
                return true;
            }
            return false;
        });
        return arr[0];
    }

    generatePassword() {
        // this.regForm.patchValue({password:Math.floor((Math.random() * 100000) + 1)});
    }

    setPasswordOpt(state) {
        this.resetPassword = state;
    } 

    addUpdateStudent() {
        let userData = this.studentEditForm.value;
        if (!this.studentEditForm.valid) {
            this.validationError = 'show-error';
            this.fh.error = "Please Fill All Mandatory fields";
            return;
        }
        if (userData.personalImg == "" || userData.signImg == "") {
            this.fh.error = "Personal Image and Sign Image are mandatory!";
            return;
        }
        if (this.formSettings) {
            if (this.formSettings.father && this.formSettings.father.fields && this.formSettings.father.fields.photo && this.formSettings.father.fields.photo.mandatory && !userData.father.photo) {
                this.fh.error = "Father Photo Required!";
                return;
            }
            if (this.formSettings.mother && this.formSettings.mother.fields && this.formSettings.mother.fields.photo && this.formSettings.mother.fields.photo.mandatory && !userData.mother.photo) {
                this.fh.error = "Mother Photo Required!";
                return;
            }
        }
        userData.customParams = {
            urbanOrRural: userData.urbanOrRural,
            motherTongue: userData.motherTongue,
            nationality: userData.nationality,
            languageKnown: userData.languageKnown,
            aadhaarNo: userData.aadhaarNo,
            religion: userData.religion,
            bloodGroup: userData.bloodGroup,
            emergencyContact: userData.emergencyContact,
            medicalHistory: userData.medicalHistory,
        }
        delete userData.urbanOrRural;
        delete userData.motherTongue;
        delete userData.nationality;
        delete userData.languageKnown;
        delete userData.aadhaarNo;
        delete userData.religion;
        delete userData.bloodGroup;
        delete userData.emergencyContact;
        delete userData.medicalHistory
         if (userData.dob) {
             userData.dob = moment(userData.dob).format("DD/MM/YYYY");
         }else{
             userData.dob="";
         }
        this.studentService.updateStudent(this.studentId, userData).subscribe(
            res => {
                this.studentData = res;
                this.notifier.alert('Success', 'Successfully Updated!!', 'success',500);
            },
            err => {
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
    }
    createSiblings(): FormGroup {
        return this.fb.group({
            name: [''],
            age: [''],
            institute: [''],
            standard: ['']
        })
    }

    createSiblingByInput(totalSibling) {
        if (totalSibling != '') {
            this.siblings = this.studentEditForm.get('siblings') as FormGroup;
            for (let index = 0; index < totalSibling; index++) {
                this.siblings.push(this.createSiblings())
            };
        }
    }
    removeSibling(index) {
        this.siblings.removeAt(index);
    }
}