import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BuyerService } from '../../services/buyer.service';
import { NotifierService } from '../notifier/notifier.service';

@Component({
    selector:'ek-buyers-component',
    templateUrl:'./buyers.component.html'
})
export class BuyersComponent implements OnInit{
    buyers:any[];
    panelLoader:string="none";
    @Input() store:string;
    @Input() viewOpt:string;
    @Output() onItemInfoClick:EventEmitter<any>=new EventEmitter<any>();
    constructor(
        private buyerService:BuyerService,
        private notifier:NotifierService
    ){

    }

    ngOnInit(){
        this.panelLoader="show";
        let filter:any={};
        if(this.store){
            filter.store=this.store;
        }
        this.buyerService.getBuyer(filter).subscribe(
            res=>{
                this.buyers=res;
                this.panelLoader="none";
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger");
            }
        )
    }

    showInfo(buyer){
        this.onItemInfoClick.emit(buyer);
    }
}