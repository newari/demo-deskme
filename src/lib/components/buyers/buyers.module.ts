import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BuyersComponent } from './buyers.component';
import { BuyerService } from '../../services/buyer.service';


@NgModule({
    declarations:[BuyersComponent],
    exports:[BuyersComponent],
    providers:[ BuyerService  ],
    imports:[CommonModule, RouterModule]
})
export class BuyersModule {
    
}
