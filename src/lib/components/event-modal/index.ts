import { NgModule } from '@angular/core';
import { EventModalComponent } from './component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EventService } from '../../services/event.service';

@NgModule({
    declarations:[EventModalComponent],
    imports:[CommonModule, FormsModule, ReactiveFormsModule],
    providers:[EventService],
    exports:[EventModalComponent]
})
export class EventModalModule {}