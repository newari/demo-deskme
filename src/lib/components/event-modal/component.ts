import { Component, Input, Output, EventEmitter, SimpleChange } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { EventService } from '../../services/event.service';
import { NotifierService } from '../notifier/notifier.service';
import { Event } from '../../models/event.model';
@Component({
    selector:'ek-event-modal',
    templateUrl:'./component.html'
})
export class EventModalComponent{
    panelLoader='none';
    eventTimingForm:FormGroup;
    mergeEventForm:FormGroup;

    @Input('eventId') eventId:string;
    @Input('ekModal') ekModal={status:'hide'};
    @Input('ekModalStatus') ekModalStatus='h';///to detect hide show changes
    
    
    @Output('done') done=new EventEmitter();
    event:Event;
    // ekModalStatus=this.ekModal.status;
    constructor(
        private eventService:EventService,
        private notifier:NotifierService,
        private fb:FormBuilder
    ){
    }

    ngOnInit():void{
        this.eventTimingForm=this.fb.group({
            startedAt:['', Validators.required],
            endedAt:['', Validators.required],
            breakStart:[''],
            breakEnd:[''],
            attendees:['']
        })
        this.mergeEventForm=this.fb.group({
            batchCode:['', Validators.required]
        })
    }

    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
        for (let propName in changes) {
          let changedProp = changes[propName];
        //   let to = JSON.stringify(changedProp.currentValue);
            console.log(changedProp)
        
          if (propName=='ekModalStatus'&&changedProp.currentValue=='show') {
              var _this=this;
              setTimeout(function(){
                _this.loadEvent();
              }, 50);
            
          } 
        }
    }
    
    loadEvent(){
        this.panelLoader='show';
        this.eventService.getOneEvent(this.eventId).subscribe(
            (res)=>{
                
                this.event=res;
                var frmdata:any={};
                if(this.event.startedAt){
                    console.log(this.event.startedAt);
                   frmdata.startedAt= moment(this.event.startedAt).format("HH:mm");
                }
                if(this.event.endedAt){
                   frmdata.endedAt= moment(this.event.endedAt).format("HH:mm");
                }
                if(this.event.attendees){
                   frmdata.attendees= this.event.attendees;
                }
                if(this.event.breaks&&this.event.breaks.length>0){
                   frmdata.breakStart= moment(this.event.breaks[0].start).format("HH:mm");
                   frmdata.breakEnd= moment(this.event.breaks[0].end).format("HH:mm");
                }
                this.panelLoader='none';
                this.eventTimingForm.patchValue(frmdata);

            },
            (err)=>{
                this.panelLoader='none';
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }

    updateEventTiming(){
        let data=this.eventTimingForm.value;
        if(!data.startedAt||!data.endedAt){
            return;
        }
        let dt=moment(this.event.start).format("YYYY-MM-DD");
        data.startedAt=moment(dt+"T"+data.startedAt+"+05:30");
        data.endedAt=moment(dt+"T"+data.endedAt+"+05:30");
        if(data.breakStart){
            data.breakStart=moment(dt+"T"+data.breakStart+"+05:30");
        }
        if(data.breakEnd){
            data.breakEnd=moment(dt+"T"+data.breakEnd+"+05:30");
        }
        this.panelLoader='show';
        this.eventService.updateEventFinishTiming(this.event.id, data).subscribe(
            res=>{
                this.panelLoader='none';
            },
            err=>{
                this.panelLoader='none';
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }

    mergeBatchEvent(){
        this.panelLoader='show';
        let batchData=this.mergeEventForm.value;
        this.eventService.mergeBatchEvent(this.event.id, batchData).subscribe(
            res=>{
                this.notifier.alert("Done", 'Event merged successfully', 'success', 5000);
                this.panelLoader='none';
            },
            err=>{
                this.panelLoader='none';
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
    
    
    
}