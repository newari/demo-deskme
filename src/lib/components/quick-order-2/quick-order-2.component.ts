import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {NgIf} from '@angular/common';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { User } from '../../models/user.model';
import { Order } from '../../models/order.model';
import { Session } from '../../models/session.model';
import { Coption } from '../../models/coption.model';
import { Product } from '../../models/product.model';
import { OrderService } from '../../services/order.service';
import { CenterService } from '../../services/center.service';
import { ProductService } from '../../services/product.service';
import { SessionService } from '../../services/session.service';
import { CoptionService } from '../../services/coption.service';
import { StudentService } from '../../services/student.service';
import { NotifierService } from '../notifier/notifier.service';
import { Center } from '../../models/center.model';
import { AuthService } from '../../services/auth.service';
import { Student } from '../../models/student.model';

@Component({
    selector:'ek-quick-order-2',
    templateUrl:'./quick-order-2.component.html'
})
export class QuickOrder2Component implements OnInit{ 
    quickOrderForm:FormGroup;
    activeStep:number=1;
    selectedUser:User;
    userOldOrders:Order[];
    theOrder:Order;
    centers:Center[];
    sessions:Session[];
    courses:Coption[];
    streams:Coption[];
    productTypes:Coption[];
    products:Product[];
    selectedProduct:Product;
    formStatus:string;
    userDefaultParams:any;
    selectedProductType:any;
    allRelatedProducts:any;
    fh:any={
        personalImg:'https://iesmaster.org/public/images/dummy-photo.png',
        signImg:'https://iesmaster.org/public/images/dummy-photo-sign.png',
        status:'Normal',
        error:null
    };
    isExStd: boolean = false;
    @Input() windowState:string;
    @Output() onWindowClose:EventEmitter<any>=new EventEmitter<any>();
    @Output() onQuickOrderCompelete:EventEmitter<any>=new EventEmitter<any>();
    @Input() productCategory:string[];//=[Classroom, Book];  //Category Coptio Ids
    @Input() email:string;
    addedProducts: any[]=[];
    cartTotal: number;
    pastCrses:number[]=[0,1,2];
    selectedStudent:Student;

    constructor(
        private fb:FormBuilder,
        private orderService:OrderService,
        private centerService:CenterService,
        private sessionService:SessionService,
        private productService:ProductService,
        private coptionService:CoptionService,
        private studentService:StudentService,
        private notifier: NotifierService,
        private authService: AuthService
    ){

    }

    ngOnInit(): void{

        this.quickOrderForm=this.fb.group({
            center:[null, Validators.required],
            course:[null, Validators.required],
            productType:[null, Validators.required],
            stream:[null, Validators.required],
            session:[null, Validators.required],
            product:[null, Validators.required],
            batch:[null],
            user:this.fb.group({
                firstName:['', Validators.required],
                lastName:['', Validators.required],
                email:['', Validators.required],
                mobile:['', Validators.required],
                gender:['', Validators.required],
                dob:['', Validators.required],
                address:this.fb.group({
                    address:[''],
                    landmark:[''],
                    city:[''],
                    state:[''],
                    postalCode:[''],
                    country:['India'],
                }),
                permanantAddress:this.fb.group({
                    address:[''],
                    landmark:[''],
                    city:[''],
                    state:[''],
                    postalCode:[''],
                    country:['India']
                }),
                exStudent: this.fb.group({
                    srn: [''],
                    session: ['']
                }),
                pastCourses: this.fb.array([
                    this.initPastCourseFrm("", true),
                    this.initPastCourseFrm(""),
                    this.initPastCourseFrm("Other")
                ]),
                father:this.fb.group({
                    name:['', Validators.required],
                    mobile:['']
                }),
                mother:this.fb.group({
                    name:['', Validators.required],
                    mobile:['']
                })
            })
        });
        let self=this;
        window.setTimeout(function(){
            self.setInit();
        }, 1);
        this.userDefaultParams=this.authService.getDefaultParams();
        
    }

    setInit(){
        
        this.loadCenters();
        this.loadCourse();
        this.loadProductTypes();
        this.loadStreams();
        this.loadSessions();

        if (this.email) {
            
        }
    }
    initPastCourseFrm(crsName:string, required?:boolean){
        return this.fb.group({
            course:[crsName, (required?Validators.required:null)],
            courseDuration:[''],
            college:['', (required?Validators.required:null)],
            year:['', (required?Validators.required:null)],
            marks:['', (required?Validators.required:null)],
            remarks:[''],
        })
    }
    onUserSelect(user){
        this.selectedUser=user;

        this.orderService.getOrder({user:user.id, populateCenter:true, populateCourse:true, populateStream:true, populateSession:true, populateBatch:true}).subscribe(
            res=>{
                this.userOldOrders=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000);
            }
        )
    }

    resetSelectedUser(){
        this.selectedUser=null;
        this.userOldOrders=null;
        this.quickOrderForm.reset();
    }
    onCenterChanged(){
        this.loadCourse();
        this.quickOrderForm.patchValue({course:null});
        this.unSelectProduct();
        
    }

    onCourseChanged(){
        this.loadProductTypes();
        this.quickOrderForm.patchValue({productType:null});
        this.unSelectProduct();
        
    }

    onProgramTypeChanged(){
        this.loadStreams();
        this.quickOrderForm.patchValue({stream:null});
        this.unSelectProduct();
        
    }

    onStreamChanged(){
        this.loadSessions();
        this.quickOrderForm.patchValue({session:null});
        this.unSelectProduct();
    }

    onSessionChanged(){
        this.unSelectProduct();
        
        this.loadProducts();
    }
    onProductChanged(){
        this.quickOrderForm.patchValue({batch:null});
        this.setProductDepParams();
       
    }
    loadRelatedProducts(): any {
        let filter:any={
            productCategory:this.selectedProduct.category,
            customParam:"relProduct",
            productType:this.selectedProduct.type
        }
        this.productService.getProducts(filter).subscribe(
            (res)=>{
                this.allRelatedProducts=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }

    checkProductDepParams(){
        let frmData=this.quickOrderForm.value;
        if(frmData.course==""||frmData.productType==""||frmData.stream==""){
            alert("Please select Course, Program & Stream First!");
            return;
        }
    }
    setProductDepParams(){
        if(this.quickOrderForm.value.product){
            this.selectedProduct=this.products[this.quickOrderForm.value.product];
            if(this.selectedProduct.customParam&&this.selectedProduct.customParam.haveRelatedProduct){
                this.loadRelatedProducts();
            }
        }else{
            this.selectedProduct=null;
        }
    }
    unSelectProduct(){
        this.quickOrderForm.patchValue({product:null});
        this.selectedProduct=null;
    }

    loadCenters(){
        let filter:any={};
        if(this.userDefaultParams&&this.userDefaultParams.center){
            filter.id=this.userDefaultParams.center.id;
        }
        if(this.centers){
            filter.id=this.centers;
        }
        
        
        this.productService.getCenters(filter).subscribe(
            res=>{
                
                this.centers=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    loadCourse(){
        let filter:any={};
        let frmData=this.quickOrderForm.value;
        if(frmData.center){
            filter.center=frmData.center;
        }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        this.productService.getCourses(filter).subscribe(
            res=>{
                
                this.courses=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    loadProductTypes(){
        let filter:any={};
        let frmData=this.quickOrderForm.value;
        if(frmData.center){
            filter.center=frmData.center;
        }
        if(frmData.course){
            filter.course=frmData.course
        }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        this.productService.getProductTypes(filter).subscribe(
            res=>{
                
                this.productTypes=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    loadStreams(){
        let filter:any={};
        let frmData=this.quickOrderForm.value;
        if(frmData.center){
            filter.center=frmData.center;
        }
        if(frmData.course){
            filter.course=frmData.course;
        }
        if(frmData.productType){
            filter.type=frmData.productType;
        }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        this.productService.getStreams(filter).subscribe(
            res=>{
                
                this.streams=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    loadSessions(){
        let filter:any={};
        if(this.userDefaultParams&&this.userDefaultParams.session){
            filter.id=this.userDefaultParams.session.id;
        }
        if(this.sessions){
            filter.id=this.sessions;
        }
        
        let frmData=this.quickOrderForm.value;
        if(frmData.center){
            filter.center=frmData.center;
            
        }
        if(frmData.course){
            filter.course=frmData.course;
        }
        if(frmData.productType){
            filter.type=frmData.productType;
            
        }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        this.productService.getSessions(filter).subscribe(
            res=>{
                this.sessions=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    loadProducts(){
        let frmData=this.quickOrderForm.value;
        if(frmData.course==""||frmData.productType==""||frmData.stream==""||frmData.center==""||frmData.session==""){
            return;
        }
        let filter={
            center:frmData.center,
            course:frmData.course,
            productType:frmData.productType,
            stream:frmData.stream,
            session:frmData.session,
            populateBatches:true,
            status:true
        };

        this.productService.getProducts(filter).subscribe(
            res=>{
                this.products=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    addUpdateStudent(placeOrder?:boolean){
        this.fh.error=null;
        if(!this.quickOrderForm.valid ){
            this.fh.error="Please fill all the mandatory fields!";
            return;
        }
        let userData:any=this.quickOrderForm.value.user;
        if(userData.personalImg==""||userData.signImg==""){
            userData.personalImg=this.fh.personalImg;
            userData.signImg=this.fh.signImg;
        }
        
        userData.stream=this.quickOrderForm.value.stream;
        userData.center=this.quickOrderForm.value.center;
        userData.course=this.quickOrderForm.value.course;
        userData.session=this.quickOrderForm.value.session;
        userData.productCategory = this.selectedProduct.category;
        if (this.selectedProduct&&this.selectedProduct.stdRegInsty) {
            userData.insty = this.selectedProduct.stdRegInsty;
        }
        if(this.selectedProduct.haveBatch){
            if(!this.quickOrderForm.value.batch||this.quickOrderForm.value.batch==null){
                this.fh.error="Please select Batch first!";
                return;
            }
            userData.batch=this.selectedProduct.batches[this.quickOrderForm.value.batch].id;
        }
        userData.dob=moment(userData.dob).format("DD/MM/YYYY");
        let stdData:any={student:userData};
        if(this.selectedUser){
            stdData.user={
                id:this.selectedUser.id
            }
        }else{
            stdData.createLogin=true;
            stdData.user={
                password:Math.random()*100000|0
            }
        }
        this.formStatus="Processing";
        this.studentService.quickRegisterStudent(stdData).subscribe(
            res=>{
                this.selectedUser=res.user;
                if(placeOrder){
                    this.placeOrder();
                }else{
                    this.formStatus="Normal";
                }
            },
            err=>{
                this.formStatus="Normal";
                this.fh.error=err.message;
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }
    placeOrder(){
		if(!this.selectedUser){
			this.notifier.alert("Warning", "Please add user first!", "danger", 10000);
			return;
		}
		let orderData:any={};
		orderData.store=this.selectedProduct.store.id||this.selectedProduct.store; //BOOKSTORE ID
		orderData.products=[];
        orderData.isShipping=this.selectedProduct.shipping;
        let prdctData:any={id:this.selectedProduct.id,  quantity:1, discountDetail:{}, taxDetail:{}};
        orderData.products.push(this.selectedProduct.id);
        orderData.total=parseFloat(this.selectedProduct.cost);
        orderData.total=orderData.total;
        console.log(orderData.total);
        
        if (this.cartTotal) {
            orderData.total= parseFloat(orderData.total+this.cartTotal);
        }
        if(this.addedProducts&&this.addedProducts.length>0){
            for (let index = 0; index < this.addedProducts.length; index++) {
                orderData.products.push(this.allRelatedProducts[this.addedProducts[index]].id)
            }
        }
        orderData.discountDetail={};
        orderData.shippingAddress={};
		orderData.shippingAddress.name=this.selectedUser.firstName+" "+this.selectedUser.lastName;
        orderData.center=this.quickOrderForm.value.center;
        orderData.course=this.quickOrderForm.value.course;
        orderData.stream=this.quickOrderForm.value.stream;
        orderData.session=this.quickOrderForm.value.session;
        orderData.productCategory = this.selectedProduct ? this.selectedProduct.category : null;
        orderData.productType = this.selectedProduct?this.selectedProduct.type:null;

        if(this.selectedProduct.batches&&this.quickOrderForm.value.batch){
            orderData.batch=this.selectedProduct.batches[this.quickOrderForm.value.batch].id||null;
        }else{
            orderData.batch=null;
        }
		orderData.tracer={
			form:'IMS-QUICK-ORDER-ADD-2'
        };
        if(!this.selectedProduct.costIncludedTax&&this.selectedProduct.tax){
            if(this.selectedProduct.tax.cgst){
                orderData.total+=parseFloat(((parseFloat(this.selectedProduct.cost)*parseInt(this.selectedProduct.tax.cgst))/100).toFixed(2));
            }
            if(this.selectedProduct.tax.sgst){
                orderData.total+=parseFloat(((parseFloat(this.selectedProduct.cost)*parseInt(this.selectedProduct.tax.sgst))/100).toFixed(2));
            }
        }
        orderData.total= parseFloat((orderData.total).toFixed(2))
        this.formStatus="Processing";
		this.orderService.placeOrder(this.selectedUser, orderData).subscribe(
			res=>{
				this.theOrder=res;
                this.activeStep=3;
			},
			err=>{
                this.formStatus="Normal";
                this.fh.error=err.message;

				this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
			}
		)
	}

    proceedToStep2(){
        this.activeStep=2;
        if(this.selectedUser){
            this.getSessionStudent();
            // this.quickOrderForm.patchValue({user:this.selectedUser});
        }
    }
    getSessionStudent() {
        this.studentService.getStudent({user:this.selectedUser.id}).subscribe(
            res=>{
                if(res.length>0){
                    this.selectedStudent= res[0];
                    if(!this.selectedStudent.dob||this.selectedStudent.dob=='') {
					console.log("HERE");
                        
                        this.selectedStudent.dob=null;
                    }else if(this.selectedStudent.dob&&(this.selectedStudent.dob=='null'||this.selectedStudent.dob=='Invalid date'||this.selectedStudent.dob=='Invalid Date')){
                        this.selectedStudent.dob=null;
                    } else{
                        this.selectedStudent.dob=moment(this.selectedStudent.dob, "DD-MM-YYYY").format("DD/MM/YYYY")
                    }
                    this.quickOrderForm.patchValue({user:this.selectedStudent});
                }
            }
        );
        // throw new Error("Method not implemented.");
    }
    
    proceedToStep3(oldOrderIndex){
        this.theOrder=this.userOldOrders[oldOrderIndex];
        this.activeStep=3;
    }
    
    onPaymentAdd(pmt){
        this.notifier.alert("Success", "Payment added successfully!", "success", 5000);
        this.onQuickOrderCompelete.emit(this.theOrder);
	}

    closeWindow(){
        this.activeStep=1;
        this.onWindowClose.emit(true)
    }

    addProductToCart(productIndex: number) {
        // let isSelectedProductExist= this.addedProducts.indexOf(this.selectedProduct.id);
        // if(isSelectedProductExist==-1) this.addedProducts.push(this.selectedProduct.id);
        if (this.allRelatedProducts[productIndex].isCombo) {
            this.addedProducts = [];
            console.log("here");
            
        } else if (this.addedProducts.length > 0 && this.allRelatedProducts[this.addedProducts[0]].isCombo) {
            this.addedProducts = [];
            console.log("heres");
        }
        this.addedProducts.push(productIndex);

        this.setCartTotal();

    }
    removeCartProduct(productIndex: number) {
        let ind = this.addedProducts.indexOf(productIndex);
        if (ind > -1) {
            this.addedProducts.splice(ind, 1);
        }
        this.setCartTotal();
    }

    setCartTotal() {
        console.log(this.quickOrderForm.value);

        let ttl = 0;
        for (let i = 0; i < this.addedProducts.length; i++) {
            ttl += parseFloat(this.allRelatedProducts[this.addedProducts[i]].cost);
        }

        this.cartTotal = ttl;
        
        // if (this.paymentOptions && this.activePmtOptionIndex > -1 && this.selectedProduct && this.selectedProduct.disableFullPriceOptOnOrder) {
        //     this.pmtOptAmount = this.paymentOptions[this.activePmtOptionIndex].amount;
        // } else {
        //     this.pmtOptAmount = this.cartTotal;
        // }
    }

    convertToFloat(number){
        return parseFloat(number);
    }
    setTotal(cartTotal, cost){
        console.log(cartTotal + parseFloat(cost));
        
        return cartTotal + parseFloat(cost);
    }
}
