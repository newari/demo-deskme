import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuickOrder2Component } from './quick-order-2.component';
import { UserSearchFormModule } from '../user-search-form/user-search-form.module';
import { CalendarModule } from 'primeng/primeng';
import { OrderService } from '../../services/order.service';
import { OrderPayment2Module } from '../order-payment-2/order-payment-2.module';


@NgModule({
    declarations:[QuickOrder2Component],
    exports:[QuickOrder2Component],
    providers:[OrderService],
    imports:[CommonModule,UserSearchFormModule, CalendarModule, FormsModule, OrderPayment2Module, ReactiveFormsModule, RouterModule]
})
export class QuickOrder2Module {
    
}