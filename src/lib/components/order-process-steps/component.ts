import { Component, Input, OnInit } from '@angular/core';
@Component({
    selector:"ek-order-process-steps",
    templateUrl:'./component.html'
})
export class OrderProcessStepsComponent implements OnInit{
    @Input() maxSteps:string[];
    @Input() compltedSteps:any[];
    @Input() productId:string;
    @Input() orderStatusHistory:any[]=[];
    steps:any[]=[];;
    ngOnInit(){
        if(!this.maxSteps){
            this.maxSteps=['Processed', 'Completed'];
        }
        console.log(this.orderStatusHistory);
        let productCmpltdSteps=[];
        for(let ci=0; ci<this.orderStatusHistory.length; ci++){
            if(this.orderStatusHistory[ci].product==this.productId){
                productCmpltdSteps.push(this.orderStatusHistory[ci].status);
            }
        }
        for(let i=0; i<this.maxSteps.length; i++){
            let compeleted=0;
            if(productCmpltdSteps.indexOf(this.maxSteps[i])>-1){
                compeleted=1;
            }
            this.steps.push({
                text:this.maxSteps[i],
                compeleted:compeleted
            });
        }

    }
}