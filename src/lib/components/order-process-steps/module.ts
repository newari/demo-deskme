import { NgModule } from '@angular/core';
import { OrderProcessStepsComponent } from './component';
import { CommonModule } from '@angular/common';
@NgModule({
    declarations:[OrderProcessStepsComponent],
    imports:[CommonModule],
    exports:[OrderProcessStepsComponent]
})
export class OrderProcessStepsModule{

}