import { Component, Input } from '@angular/core';
import { EdukitConfig } from '../../../ezukit.config';
import { ActivatedRoute } from '@angular/router';
import { NotifierService } from '../notifier/notifier.service';
import { AuthService } from '../../services/auth.service';
import { TestMentService } from '../../services/testment.service';
@Component({
	selector : 'take-test-list',
  	templateUrl: './test.html'
})
export class TakeTestContent{
	userTestSeries;
	testseriesId = null;
	activeCourse;
	activeTestSeries;
	courses:any;
	courseIds:string[]=[];
	sessionStd;
	testOpened = false;
	basicAPI;
	currentDate = new Date();
	@Input() routes:string;
    constructor(
      	private testmentService:TestMentService,
      	private authService: AuthService,
        private notifier: NotifierService,
    ){}
    ngOnInit(){
		this.sessionStd=this.authService.faculty();
		this.basicAPI = EdukitConfig.BASICS.API_URL;
		this.loadTests();
    }
	loadTests(){
		this.testmentService.getFacultyTests(this.sessionStd.id).subscribe(
		  res=>{			
				this.courseIds = [];
				this.userTestSeries=[];				
				this.userTestSeries=res;
				this.courses={};
				for(let ci=0; ci<res.length; ci++){
					if(this.testseriesId == res[ci].tsDetail._id){
						this.activeCourse = res[ci].course._id;
						this.activeTestSeries = res[ci].tsDetail._id;	
					}
					if(!this.courses[res[ci].course._id]){
						this.courses[res[ci].course._id]=res[ci].course;
						this.courseIds.push(res[ci].course._id);
					}
				}
				if(this.testseriesId == null && this.userTestSeries.length >0){
					this.activeCourse = this.courseIds[0];
					this.activeTestSeries = this.userTestSeries[0].tsDetail._id;
				}
				this.testOpened=false;
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	testWindow(data){
		let view='w';
        if(screen.width < 672){
            view='m';
        }
		let url = this.basicAPI +'/student/testment/testwindow/'+data.test.theme+'/'+view+'/'+data._id;
		let params  = 'width='+screen.width+',height='+screen.height+',top=0,left=0,fullscreen=yes,resizable=0';
		let newWindow=window.open(url,'EXAM',params);
		this.testOpened = true;
		if (window.focus) {newWindow.focus()}
		return false;
	}
}
