import {NgModule} from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TakeTestContent } from './test';
import { DialogModule, AccordionModule, FieldsetModule, TabViewModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TestMentService } from "../../services/testment.service";
@NgModule({
    declarations: [TakeTestContent],
    imports:[
        CommonModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule,
        AccordionModule,
        TabViewModule,
        FieldsetModule
    ],
    providers: [TestMentService],
    exports:[TakeTestContent]
})
export class TakeTestModule { }