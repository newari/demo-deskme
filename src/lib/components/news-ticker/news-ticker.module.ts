import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { NoticeService } from "../../services/notice.service";

import {NewsTickerComponent} from './news-tiker.component'
import { RouterModule } from "@angular/router";

@NgModule({
    declarations:[NewsTickerComponent] ,
    imports:[CommonModule,RouterModule],
    exports:[NewsTickerComponent],
    providers:[NoticeService]
})

export class NewsTickerModule{

}
