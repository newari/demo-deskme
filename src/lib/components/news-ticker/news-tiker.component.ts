import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'ek-news-ticker',
    templateUrl: './news-ticker.html',
    styleUrls: ['./news-ticker.css']
})
export class NewsTickerComponent implements OnInit {
    @Input() newses: any[];
    @Input() app: string;
    activeIndex: number;
    constructor() { }

    ngOnInit() {
        if (!this.newses) {
            this.newses = [
                {
                    id: 1,
                    title: 'Hello this is first News'
                },
                {
                    id: 2,
                    title: 'Hello Coronovirus spreading rapidly in india'
                },
                {
                    id: 3,
                    title: 'Breaking News'
                },
                {
                    id: 4,
                    title: 'Hello this Hello Coronovirus spreading rapidly in india is last News'
                }
            ];
        }
    }


}
