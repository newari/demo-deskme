import { Component, OnInit, Input  } from '@angular/core';
import { NotificationService } from '../../services/notification.service';
import { ActivatedRoute, Params } from '@angular/router';
import { NotifierService } from '../notifier/notifier.service';
import { Notification } from '../../models/notification.model';

@Component({
    selector:'ek-notification-view',
    templateUrl:'./view.html'
})
export class NotificationViewContent implements OnInit{ 
    notification:Notification;
    notificationId;
    panelLoader="none";
    @Input() routes:string;
    constructor(
        private notificationService:NotificationService,
        private activatedRoute:ActivatedRoute,
		private notifier: NotifierService){}


    ngOnInit(): void{
        this.activatedRoute.params.subscribe((params: Params) => {
	        this.notificationId = params['notificationId'];
			this.getNotification(this.notificationId);
	    });
    }

    getNotification(notificationId){
        this.notificationService.getOneNotification(notificationId).subscribe( (data)=>{
                this.notification=data;
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader="deleted";
            }
        );
    }

    deleteNotification(notificationId){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete this?");
        if(!confirm){
            return;
        }
        this.notificationService.deleteNotification(notificationId).subscribe(
            (res)=>{
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
}
