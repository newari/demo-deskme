import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Actionevent } from '../../models/actionevent.model';
import { NotificationService } from '../../services/notification.service';
import { ActioneventService } from '../../services/actionevent.service';
import { NotifierService } from '../notifier/notifier.service';

@Component({
    selector:'ek-notification-edit',
    templateUrl:'./edit.html'
})
export class NotificationEditContent implements OnInit{
    notificationForm:FormGroup;
	notification:any;
	notificationId;	
	actionEvents:Actionevent[];
	formStatus="Normal";
	selectedActionEvent:Actionevent;
	totalDefaultNotifications:number[]=[];
	defaultNotifications:FormArray;
	@Input() routes : string;
	constructor(
		private fb:FormBuilder,
		private activatedRoute: ActivatedRoute,
		private notificationService:NotificationService,
		private actionEventService:ActioneventService,
		private notifier: NotifierService){ 
			
	}
	getNotification(notificationId){
		this.notificationService.getOneNotification(notificationId).subscribe(
			(res)=>{
				this.notification=res;
				res.params=res.params.join(",");
				res.actionEvent=res.actionEvent.id;
				this.notificationForm.patchValue(res);
				// this.defaultNotifications.patchValue(res.defaultNotifications);
				if(res.defaultNotifications){
					for(let i=0; i<res.defaultNotifications.length; i++){
						let dobj=res.defaultNotifications[i];
						this.defaultNotifications.push(this.initDefaultNotificationFrm(dobj))
						this.totalDefaultNotifications.push(i);
					}
				}
				let filter :any={handler:'notifier'};
				if(this.routes === '/ims/conventional-exam/notification'){
					filter.value='CONVENTIONAL';
				}
				this.actionEventService.getActionevent(filter).subscribe(
					res=>{
						this.actionEvents=res;
						if(this.notification){
							this.setHelp(this.notification.actionEvent);
						}
					},
					err=>{
						this.notifier.alert(err.code, err.message, "danger");
					}
				)
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		);
		
	}

	updateNotification(): void {
		if(!this.notificationForm.dirty){
			return;
		}
		this.formStatus="Processing";
		let notification=this.notificationForm.value;
		this.setHelp(notification.actionEvent);
		notification.params=notification.params.split(",");
		notification.actionEventValue=this.selectedActionEvent.value;
		this.notificationService.updateNotification(this.notificationId, notification).subscribe(
			(res)=>{
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.defaultNotifications=this.fb.array([]);
		this.notificationForm=this.fb.group({
			title:['', Validators.required],
			type:['', Validators.required],
			actionEvent:['', Validators.required],
			actionEventValue:[''],
			subjectTemplate:[''],
			template:['', Validators.required],
			params:[''],
			status:[true, Validators.required],
			senderName:[''],
			sender:[''],
			defaultNotifications:this.defaultNotifications
		});

		this.activatedRoute.params.subscribe((params: Params) => {
	        this.notificationId = params['notificationId'];
			this.getNotification(this.notificationId);
		});
		
	}

	initDefaultNotificationFrm(dobj?){
		return this.fb.group({
			type:[dobj.type],
			to:[dobj.to],
			subjectTemplate:[dobj.subjectTemplate],
			template:[dobj.template]
		})
	}
	setHelp(actionEventId){
		if(actionEventId==""){
			this.selectedActionEvent=null;
			return;
		}
		for(let i=0; i<this.actionEvents.length; i++){
			if(this.actionEvents[i].id==actionEventId){
				this.selectedActionEvent=this.actionEvents[i];
				break;
			}
		}
	}

	addDefaultReceipent(){
		this.defaultNotifications.push(this.initDefaultNotificationFrm({}));
		this.totalDefaultNotifications.push(this.totalDefaultNotifications.length);
	}
	removeDefaultReceipent(index){
		this.defaultNotifications.removeAt(index);
		this.totalDefaultNotifications.splice(index,1);
	}
}
