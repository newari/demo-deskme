import { Component, OnInit, Input  } from '@angular/core';
import { NotificationService } from '../../services/notification.service';
import { NotifierService } from '../notifier/notifier.service';

@Component({
    selector:'ek-notification-list',
    templateUrl:'./list.html'
})
export class NotificationListContent implements OnInit{ 
    rows;
    columns; 
    panelLoader="none";
    @Input() routes : string;
    filter : any={};
    constructor(
        private notificationService:NotificationService,
		private notifier: NotifierService
    ){}

    ngOnInit(): void{
        // if(this.routes == '/ims/admin/notification'){

        // }
        if(this.routes == '/ims/conventional-exam/notification'){
            this.filter.actionEventValue = 'CONVENTIONAL';
        }
        console.log(this.routes);
        this.getNotifications();
    }

    getNotifications(){
        this.notificationService.getNotification(this.filter).subscribe(
            (data)=>this.rows = data,
            (err)=>console.log(err)
        );
    }
    deleteNotification(notification){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete "+notification.name+"?");
        if(!confirm){
            return;
        }
        this.notificationService.deleteNotification(notification.id).subscribe(
            (res)=>{
                this.rows.splice(this.rows.indexOf(notification), 1);
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );

            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
}
