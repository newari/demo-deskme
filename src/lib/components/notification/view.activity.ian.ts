import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { NotificationViewContent } from './view';
import { NotificationService } from '../../services/notification.service';

@NgModule({
    declarations:[NotificationViewContent],
    imports:[CommonModule, RouterModule],
    providers : [NotificationService],
    exports : [NotificationViewContent]
})
export class NotificationViewModule {}
