import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotificationListContent } from './list';
import { CommonModule } from '@angular/common';
import { NotificationService } from '../../services/notification.service';
@NgModule({
    declarations:[NotificationListContent],
    imports:[CommonModule, RouterModule],
    providers:[NotificationService],
    exports : [NotificationListContent]
})
export class NotificationListModule {
}
