import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { Actionevent } from '../../models/actionevent.model';
import { NotificationService } from '../../services/notification.service';
import { NotifierService } from '../notifier/notifier.service';
import { ActioneventService } from '../../services/actionevent.service';

@Component({
    selector:'ek-notification-add',
    templateUrl:'./add.html'
})
export class NotificationAddContent implements OnInit{
    notificationForm:FormGroup;
	notificationData:any;
	actionEvents:Actionevent[];
	formStatus="Normal";
	selectedActionEvent:Actionevent;
	totalDefaultNotifications:number[]=[];
	defaultNotifications:FormArray;
	@Input() routes : string;
	constructor(
		private fb:FormBuilder,
		private notificationService:NotificationService,
		private notifier: NotifierService,
		private actionEventService:ActioneventService,
	){}

	ngOnInit(): void {
		this.defaultNotifications=this.fb.array([]);
		this.notificationForm=this.fb.group({
			title:['', Validators.required],
			type:['', Validators.required],
			actionEvent:['', Validators.required],
			actionEventValue:[''],
			subjectTemplate:[''],
			template:['', Validators.required],
			params:[''],
			status:[true, Validators.required],
			senderName:[''],
			sender:[''],
			defaultNotifications:this.defaultNotifications
		});
		let filter :any={handler:'notifier'};
		if(this.routes === '/ims/conventional-exam/notification'){
			filter.value='CONVENTIONAL';
		}
		this.actionEventService.getActionevent(filter).subscribe(
			res=>{
				this.actionEvents=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger");
			}
		);
	}
	addNotification(): void {
		this.formStatus="Processing";
		this.notificationData=this.notificationForm.value;
		this.notificationData.params=this.notificationData.params.split(",");
		this.notificationData.actionEventValue=this.selectedActionEvent.value;
		this.notificationService.addNotification(this.notificationData).subscribe(
			res=>{
				this.notificationForm.reset(); 
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
	setValidation(type){
		if(type=='EMAIL'){
			let sender= this.notificationForm.get('sender') as FormControl;
			sender.setValidators(Validators.required)
			let senderName= this.notificationForm.get('senderName') as FormControl;
			senderName.setValidators(Validators.required);
		}else{
			let sender= this.notificationForm.get('sender') as FormControl;
			sender.setValidators(null)
			let senderName= this.notificationForm.get('senderName') as FormControl;
			senderName.setValidators(null);
		}
	}

	initDefaultNotificationFrm(){
		return this.fb.group({
			type:[''],
			to:[''],
			subjectTemplate:[''],
			template:['']
		})
	}
	setHelp(actionEvent){
		if(actionEvent==""){
			this.selectedActionEvent=null;
			return;
		}
		for(let i=0; i<this.actionEvents.length; i++){
			if(this.actionEvents[i].id==actionEvent){
				this.selectedActionEvent=this.actionEvents[i];
				break;
			}
		}
	}

	addDefaultReceipent(){
		this.defaultNotifications.push(this.initDefaultNotificationFrm());
		this.totalDefaultNotifications.push(this.totalDefaultNotifications.length);
	}
	removeDefaultReceipent(index){
		this.defaultNotifications.removeAt(index);
		this.totalDefaultNotifications.splice(index,1);
	}
}
