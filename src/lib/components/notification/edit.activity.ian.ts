import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotificationEditContent } from './edit';
import { ActioneventService } from '../../services/actionevent.service';
import { NotificationService } from '../../services/notification.service';
import { QuillEditorModule } from '../ngx-quill-editor';
@NgModule({
    declarations:[NotificationEditContent],
    imports:[RouterModule, FormsModule, QuillEditorModule, CommonModule, ReactiveFormsModule],
    providers:[ActioneventService,NotificationService],
    exports : [NotificationEditContent]
})
export class NotificationEditModule {}
