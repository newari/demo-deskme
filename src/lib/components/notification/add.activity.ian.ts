import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NotificationAddContent } from './add';
import { NotificationService } from '../../services/notification.service';
import { QuillEditorModule } from '../ngx-quill-editor';
import { ActioneventService } from '../../services/actionevent.service';
@NgModule({
    declarations:[NotificationAddContent],
    imports:[RouterModule, FormsModule, QuillEditorModule, CommonModule, ReactiveFormsModule],
    providers:[ActioneventService,NotificationService],
    exports : [NotificationAddContent]
})
export class NotificationAddModule {}
