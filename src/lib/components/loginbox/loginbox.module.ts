import { NgModule } from '@angular/core';
import { LoginboxComponent } from './loginbox.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { StudentPanelService } from '../../services/student-panel.service';
import { UserService } from '../../services/user.service';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations:[
        LoginboxComponent
    ],
    imports:[
        CommonModule,
        FormsModule, 
        ReactiveFormsModule,
        RouterModule
    ],
    exports:[LoginboxComponent],
    providers: [AuthService, StudentPanelService, UserService]
})
export class LoginboxModule { }