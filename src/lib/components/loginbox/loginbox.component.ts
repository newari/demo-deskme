import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotifierService } from '../notifier/notifier.service';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { StudentPanelService } from '../../services/student-panel.service';
import { UserService } from '../../services/user.service';

try {
    var macaddress = window.require('macaddress');
} catch (error) {
    console.log(error)
}
@Component({
    selector:"ek-loginbox",
    templateUrl:'./loginbox.component.html'
})
export class LoginboxComponent implements OnInit {
    @Input() authenticated:Boolean=true;
    @Input() userType:string[]=['GUEST'];
    @Input() logoutTarget:String='same';
    @Input() loginTarget:String='same';
    @Input() title:String='Login';
    @Input() activeBox:string="login";
    @Input() loginWith:any={};
    loginForm:FormGroup;
    signupForm:FormGroup;
    signupOtpForm:FormGroup;
    btnText="Login";
    signupBtnText:string="Register";
    authMsg:string;
    forgotPassForm:FormGroup;
    fpBtnText="Submit";
    loginWithOTPForm: FormGroup;
    otpData: any;
    otpVarification: FormGroup;
    std: any;
    needLoginWith:string;
    resetPasswordForm: FormGroup;
    constructor(
        private authService:AuthService,
        private fb:FormBuilder,
        private notifier:NotifierService,
        private router: Router,
        private userService:UserService,
        private spService: StudentPanelService,

    ){
        this.authService.authObservable.subscribe(
            (res)=>{
                if(res.loggedOut){
                    if(this.logoutTarget!="same"){
                        this.router.navigate([this.logoutTarget]);
                    }else{
                        this.authenticated=false;
                    }
                }
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )
    }



    ngOnInit(){
        if (this.loginWith && this.loginWith.email && this.loginWith.email!='') {
            this.sendOtp();
        }
            this.notifier.clearAlert();
            this.loginForm=this.fb.group({
                username:['', Validators.required],
                password:['', Validators.required],

            });
            this.signupForm=this.fb.group({
                firstName:['', Validators.required],
                lastName:['', Validators.required],
                dob:['', Validators.required],
                email:['', Validators.required],
                mobile:['', Validators.required],
                pass:['', Validators.required],
                cPass:['', Validators.required],

            });
            this.otpVarification = this.fb.group({
                otp: ['', Validators.required]
            });
            this.signupOtpForm = this.fb.group({
                otp: ['', Validators.required]
            });
            this.forgotPassForm=this.fb.group({
                email:['', Validators.required]
            });
            this.resetPasswordForm=this.fb.group({
                otp:['',Validators.required],
                newPassword:['', [Validators.required, Validators.minLength(6)]],
                confirmPassword:['', Validators.required]
            })
    }

    async login(){
        if(this.btnText!="Login"){
            return;
        }
        if(this.userType.indexOf('STUDENT')!==-1&&(this.loginForm.value.username==''||this.loginForm.value.password=='')){
            this.notifier.alert("Warning",'Please enter valid credentials!','danger', 5000);
            this.authMsg="Please enter valid credentials!";
            return;
        }
        this.btnText='Processing...';
        let data:any={username:this.loginForm.value.username.toLowerCase(), password:this.loginForm.value.password}
        try {
            data.did=await macaddress.one();
        } catch (error) {
            console.log(error)
        }
        data.deviceType="DESKTOP";
        this.authService.login(data).subscribe(
            (res)=>{
                if(!res.user.type||this.userType.indexOf(res.user.type)===-1){
                    this.notifier.alert('Not Authorised!', 'Permission not allowed to this login id!', 'danger', 5000);
                    this.authMsg="Permission not allowed to this login id!";
                    this.btnText="Login";
                    return false;
                }
                window.localStorage.setItem("authToken", res.token);
                this.notifier.alert('Success', '', 'Logged in successfully!', 5000);
                this.authenticated=true;
                let user=JSON.stringify(res.user);
                window.localStorage.setItem("user", user);
                this.notifier.clearAlert();
                if(res.user.defaultLandingPage&&res.user.defaultLandingPage!=""){
                    try {
                        return this.router.navigate([res.user.defaultLandingPage]);
                    } catch (error) {
                        console.log(error);
                    }
                }else if(this.loginTarget!="same"){
                    this.router.navigate([this.loginTarget]);
                }
                this.btnText="Login";

            },
            (err)=>{
                this.btnText="Login";
                this.authMsg=err.error.message;
                this.notifier.alert(err.error.code, err.error.message, 'danger', 5000);
            }
        );

    }

    processForgotPass(){
        if(!this.forgotPassForm.valid) return
        if(this.fpBtnText!="Submit"){
            return;
        }
        this.fpBtnText='Processing...';
        this.authService.requestResetPasswordOTP(this.forgotPassForm.value.email).subscribe(
            res=>{
                this.fpBtnText="Reset Password";
                this.activeBox='resetPassword';
                this.otpData=res;
                this.notifier.alert("Successfully sent", 'Check your mobile for SMS of reset OTP!', 'success', 15000);
            },
            err=>{
                this.notifier.alert(err.error.code, err.error.message, 'danger', 10000);
                this.fpBtnText="Submit";
                return false;
            }
        );
    }
    requsetResetPassword(){
        if(!this.resetPasswordForm.valid) return;
        let data:any=this.resetPasswordForm.value;
        data.id=this.otpData.id;
        data.clcStdUpdate=true;
        this.authService.resetPasswordRequest(data).subscribe(
            res=>{
                this.resetPasswordForm.reset();
                this.notifier.alert("Success", "Password successfully changed!", 'success', 10000);
                this.activeBox='login';
                this.fpBtnText="Password";
            },
            err=>{
                this.notifier.alert(err.error.code, err.error.message||'OTP mismatch or expired. Request another one.', 'danger', 15000);
            }
        );
    }
    setLoginWith(value?:any){
        if (!this.loginForm.value.username || this.loginForm.value.username=='') {
            this.notifier.alert("Error", "Please Enter Username!!", 'danger', 1000);
            return
        }
        this.needLoginWith=value;
    }
    sendOtp() {
        this.spService.sendSigninOtp({ username: this.loginWith.email }).subscribe(
            (res) => {
                this.otpData = res;
                this.notifier.alert("Successful", 'Sent Successfully!!', 'success', 500);
                this.activeBox ="verifyOTP";
                this.std=res.user;
            },
            (err) => {
                this.notifier.alert(err.error.code, err.error.message, 'danger', 5000);
            }
        );
    }
    verifyOTP() {
        if (!this.otpVarification.valid) {
            return;
        }
        let data: any = {};
        data.user = this.std.id;
        data.id = this.otpData.id;
        data.otp = this.otpVarification.value.otp;
        data.verifyOption = "mobile";
        this.spService.varifySigninOtp(data).subscribe(
            (res) => {
                if (res.token&&res.user) {
                    window.localStorage.setItem("authToken", res.token);
                    let userStr = JSON.stringify(res.user);
                    window.localStorage.setItem("user", userStr);
                    this.router.navigate(['/student/dashboard']);
                    this.notifier.alert("Successful", 'Mobile Number Varified Successfully!!', 'success', 500);

                } else {
                    this.notifier.alert("ERROR", 'Something Wrong. Try Again!!', 'danger', 500);
                }
            },
            (err) => {
                this.notifier.alert("ERROR", "Wrong OTP or Expired!!!", 'danger', 5000);
            }
        );
    }
    getUser(){
        this.userService.getOneUser(this.loginWith.id).subscribe(
            (res) => {
                this.std = res;
            },
            (err)=>{
                this.notifier.alert(err.error.code, err.error.message, 'danger', 5000);
            }
        )
    }

    requestOTP(){
        if(!this.loginForm.value.username||this.loginForm.value.username.trim()=='') {
            this.notifier.alert("Error", "Please Enter Username!!", 'danger', 1000);
            return;
        }
        this.loginWith={};
        this.loginWith.email = this.loginForm.value.username;
        this.sendOtp();
    }

    requestRegOTP(){
        if(this.signupBtnText!="Register"){
            return;
        }
        if(!this.signupForm.valid){
            this.notifier.alert("Warning",'Please enter all the mandatory values!','danger', 5000);
            this.authMsg="Please enter all the mandatory values!";
            return;
        }
        if(this.signupForm.value.pass!=this.signupForm.value.cPass){
            this.authMsg="Password and Confirm password must be same!";
            return;
        }
        this.signupBtnText="Processing...";
        this.spService.sendSignupOtp({ mobile: this.signupForm.value.mobile }).subscribe(
            (res) => {
                this.otpData = res;
                this.notifier.alert("Successful", 'Sent Successfully!!', 'success', 500);
                this.activeBox ="verifySignupOTP";
                this.signupBtnText="Complete Registration";
            },
            (err) => {
                this.signupBtnText="Register";
                this.notifier.alert(err.error.code, err.error.message, 'danger', 5000);
            }
        );
    }

    verifySignupOTP() {
        if (!this.signupOtpForm.valid) {
            return;
        }
        if(this.signupBtnText!="Complete Registration"){
            return;
        }
        let data: any = {};
        
        data.id = this.otpData.id;
        data.otp = this.signupOtpForm.value.otp;
        data.student=this.signupForm.value;
        console.log(data.student)
        data.password = data.student.pass;
        data.clcStdUpdate=true;
        this.signupBtnText="Processing...";
        this.spService.registerWithOtp(data).subscribe(
            (res) => {
                console.log(res);
                if(!res.token){
                    this.notifier.alert('Not Authorised!', 'Permission not allowed to this login id!', 'danger', 5000);
                    this.authMsg="Permission not allowed to this login id!";
                    this.btnText="Login";
                    return false;
                }
                window.localStorage.setItem("authToken", res.token);
                this.notifier.alert('Success', '', 'Logged in successfully!', 5000);
                this.authenticated=true;
                let user=JSON.stringify(res.user);
                window.localStorage.setItem("user", user);
                this.notifier.clearAlert();
                this.router.navigate([this.loginTarget]);
            },
            (err) => {
                this.signupBtnText="Complete Registration";
                this.notifier.alert("ERROR", "Wrong OTP or Expired!!!", 'danger', 5000);
            }
        );
    }

    register(){
        if(this.btnText!="Register"){
            return;
        }
        if(!this.signupForm.valid){
            this.notifier.alert("Warning",'Please enter all the mandatory values!','danger', 5000);
            this.authMsg="Please enter all the mandatory values!";
            return;
        }
        this.btnText='Processing...';
        this.authService.login({username:this.loginForm.value.username.toLowerCase(), password:this.loginForm.value.password}).subscribe(
            (res)=>{
                if(!res.user.type||this.userType.indexOf(res.user.type)===-1){
                    this.notifier.alert('Not Authorised!', 'Permission not allowed to this login id!', 'danger', 5000);
                    this.authMsg="Permission not allowed to this login id!";
                    this.btnText="Login";
                    return false;
                }
                window.localStorage.setItem("authToken", res.token);
                this.notifier.alert('Success', '', 'Logged in successfully!', 5000);
                this.authenticated=true;
                let user=JSON.stringify(res.user);
                window.localStorage.setItem("user", user);
                this.notifier.clearAlert();
                if(res.user.defaultLandingPage&&res.user.defaultLandingPage!=""){
                    try {
                        return this.router.navigate([res.user.defaultLandingPage]);
                    } catch (error) {
                        console.log(error);
                    }
                }else if(this.loginTarget!="same"){
                    this.router.navigate([this.loginTarget]);
                }
                this.btnText="Login";

            },
            (err)=>{
                this.btnText="Login";
                this.authMsg=err.error.message;
                this.notifier.alert(err.error.code, err.error.message, 'danger', 5000);
            }
        );
    }
}
