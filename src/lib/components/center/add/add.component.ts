import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CenterService } from '../../../services/center.service';
import { NotifierService } from '../../notifier/notifier.service';
import { Center } from '../../../models/center.model';


@Component({
    selector:'ek-center-add-form',
    templateUrl:'./add.component.html'
})
export class CenterAddFormComponent{
    centerForm:FormGroup;
	centerData:Center;
    formStatus="Normal";
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
    
    constructor(
		private fb:FormBuilder,
		private centerService:CenterService,
		private notifier: NotifierService
    ){ }
	
	addCenter(): void {
		this.formStatus="Processing";
		this.centerData=this.centerForm.value;
		this.centerService.addCenter(this.centerData).subscribe(
			res=>{
                this.onSuccess.emit({event:'ItemAdded', item:res});
				this.centerForm.reset(); 
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.centerForm=this.fb.group({
			title:['', Validators.required],
			studentIdCard:[''],
			status:[true, Validators.required],
        });
        
    }

   
    
    
}
