import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CenterAddFormComponent } from './add.component';
import { FileinputModule } from '../../filemanager/fileinput.module';

@NgModule({
    declarations:[CenterAddFormComponent],
    imports:[FileinputModule, FormsModule, ReactiveFormsModule],
    exports:[CenterAddFormComponent]
})
export class CenterAddFormModule{
}
