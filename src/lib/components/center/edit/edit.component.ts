import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CenterService } from '../../../services/center.service';
import { NotifierService } from '../../notifier/notifier.service';
import { Center } from '../../../models/center.model';


@Component({
    selector:'ek-center-edit-form',
    templateUrl:'./edit.component.html'
})
export class CenterEditFormComponent{
    centerForm:FormGroup;
	center:Center;
	formStatus="Normal";
	panelLoader:string="none";

	@Input() centerId:any;
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
	
	constructor(
		private fb:FormBuilder,
		private centerService:CenterService,
		private notifier: NotifierService
	){ }
	
	getCenter(centerId){
		this.panelLoader="show;"
		this.centerService.getOneCenter(centerId).subscribe(
			(res)=>{
				this.center=res;
				this.centerForm.patchValue(res);
				this.panelLoader="none";
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
			}
		)
	}
		
	updateCenter(): void {
		if(!this.centerForm.dirty){
			return;
		}
		this.formStatus="Processing";
		this.center=this.centerForm.value;
		this.centerService.updateCenter(this.centerId, this.center).subscribe(
			(res)=>{
				this.onSuccess.emit(res);
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.centerForm=this.fb.group({
			title:['', Validators.required],
			studentIdCard:[''],
			status:[true, Validators.required]
		});

		let self=this;
		window.setTimeout(function(){
			self.getCenter(self.centerId);
		}, 0);
    }
}
