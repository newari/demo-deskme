import { Component, OnInit, Input } from '@angular/core';
import {NgIf} from '@angular/common';
import { ActivatedRoute, Params } from '@angular/router';
import { CenterService } from '../../../services/center.service';
import { NotifierService } from '../../notifier/notifier.service';
import { Center } from '../../../models/center.model';


@Component({
    selector:'ek-center-info',
    templateUrl:'./info.component.html'
})
export class CenterInfoComponent implements OnInit{ 
    center:Center;
    panelLoader="none";
    activeTab:number=0;
    
    @Input() centerId:any;

    constructor(
        private centerService:CenterService,
        private activatedRoute:ActivatedRoute,
        private notifier: NotifierService
    ){}


    ngOnInit(): void{
        let self=this;
		window.setTimeout(function(){
			self.getCenter(self.centerId);
		}, 0);
    }

    getCenter(centerId){
        this.panelLoader="show";
        this.centerService.getOneCenter(centerId).subscribe( (data)=>{
                this.center=data;
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }

    deleteCenter(centerId){
        let concenter=window.confirm("Are you sure to delete this?");
        if(!concenter){
            return;
        }
        this.panelLoader="show";
        this.centerService.deleteCenter(centerId).subscribe(
            (res)=>{
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }
}
