import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CenterInfoComponent } from './info.component';

@NgModule({
    declarations:[CenterInfoComponent],
    imports:[CommonModule],
    exports:[CenterInfoComponent]
})
export class CenterInfoModule {
    
}
