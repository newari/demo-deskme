import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from '../../services/product.service';
import { NotifierService } from '../notifier/notifier.service';
import { StudentInfoComponent } from '../student/info/info.component';
import { RightSideWindowData } from '../rswindow/rsw-data.interface';
import * as moment from 'moment';
import { UserService } from '../../services/user.service';
import { CoptionService } from '../../services/coption.service';
import { Coption } from '../../models/coption.model';

@Component({
    selector:'ek-product-users',
    templateUrl:'./product-users.component.html',
    entryComponents:[StudentInfoComponent]

})
export class ProductUsersComponent implements OnInit{
    @Input() productId:string;

    productUsers:any[];
    panelLoader:string;
    totalRecords:number;
    activeFilter:any={};
    countBase:number=1;
    rswComponentData:RightSideWindowData;
    rswFooter:any;
    rswBottomed:boolean;
    rswItem:any;
    validTill:any;
    selectedIndex:number;
    minDateValue:Date=new Date(moment().add(2,'days').format());
    productAddForm:FormGroup;
    showAddProductModal:boolean;
    products:any;
    productCategory:any;
    prod
    productCategories: any;
    productTypes: Coption[];
    streams: Coption[];
    courses:Coption[];
    formStatus: string;
    constructor(
        private productService:ProductService,
        private userService:UserService,
        private fb:FormBuilder,
        private notifier: NotifierService,
        private coptionService: CoptionService
        ){}

    ngOnInit(){
        this.activeFilter={populateUser:true, populateOrder:true,paginate:true}
        this.loadProductUsers(this.activeFilter);
        this.productAddForm=this.fb.group({
            productCategory:[null, Validators.required],
            productType:[null, Validators.required],
            product:[null, Validators.required],
            stream:[null, Validators.required],
            course:[null, Validators.required]
        });
    }

    loadProductUsers(filter?:any){
        this.panelLoader="show";
        this.productService.getEnrolledProductUsers(this.productId, filter, true).subscribe(
            res=>{
                this.productUsers=res.body;
                this.totalRecords = res.headers.get('totalRecords')||0;
                this.panelLoader="none";
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 3000);
            }
        )
    }

    paginate(e){
        if(!this.activeFilter){
            this.activeFilter={};
        }
        this.countBase=e.rows*e.page+1;
        this.activeFilter.page=(e.page+1);
        this.activeFilter.limit=e.rows;
        this.loadProductUsers(this.activeFilter);
    }
    loadUserInfo(userId:string){
        this.rswComponentData={
            component:StudentInfoComponent,
            inputs:{userId:userId},
            title:"Forum Question "
        }
        this.rswItem=userId;
        this.rswBottomed=true;
    }
    onRSWClose(e){
        this.rswFooter=false;
        this.rswBottomed=false;
    }

    onRSWScrolledToBottom(rswBottomed){
        this.rswBottomed=rswBottomed;
    }

    extendValidity(){
        console.log("ok")
        if(this.productUsers[this.selectedIndex]){
            let userProductId= this.productUsers[this.selectedIndex].id;
            this.userService.updateUserProduct(userProductId, moment(this.validTill).endOf('day')).subscribe(
                res=>{
                    this.productUsers[this.selectedIndex].validTill= moment(this.validTill).endOf('day')
                    this.selectedIndex=null;
                },
                err=>{

                }
            );
        }else{
            return;
        }
    }
    removeProduct(i:number){
        let conf=window.confirm("Are you sure to remove this student from this product?");
        if(!conf){
            return;
        }

        if(!this.productUsers[i]){
            return;
        }
        this.panelLoader="show";
        this.userService.removeUserProduct(this.productUsers[i].id).subscribe(
            res=>{
                this.panelLoader="none";
                this.productUsers.splice(i, 1);
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 3000);
            }
        )

    }
    filterByUser(user){
        this.activeFilter.user=user.id;
        this.loadProductUsers(this.activeFilter);

    }
    clearUserFilter(){
        delete this.activeFilter.user;
        this.loadProductUsers(this.activeFilter);
    }

    showAddForm(){
        this.showAddProductModal=true;
        this.loadProductCats();
        // this.loadCourses();
        // this.loadStreams();
    }
    loadProductCats(){
        this.coptionService.getCoptionProductCategory().subscribe(
            res=>{
                this.productCategories=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }
    loadProductTypes(postCategoryId){
        if(postCategoryId==null||postCategoryId=='null'){
            return;
        }
        this.coptionService.getCoption({option:'PRODUCTTYPE',parentOption:postCategoryId,status:true}).subscribe(
            res=>{
                this.productTypes=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }
    loadStreams(){
         
        this.coptionService.getCoption({option:'STREAM', status:true}).subscribe(
            res=>{
                this.streams=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }

    loadCourses(){
        this.coptionService.getCoption({option:'COURSE',status:true}).subscribe(
            res=>{
                this.courses=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }

    loadProducts(){
        let frm=this.productAddForm.value;
        if(!frm.productCategory){
            this.notifier.alert("Error", "Product Category required!", "danger", 1000);
            return;
        }
        if(!frm.productType){
            this.notifier.alert("Error", "Product Type required!", "danger", 1000);
            return;
        }
        if(!frm.stream){
            this.notifier.alert("Error", "Stream required!", "danger", 1000);
            return;
        }
        if(!frm.course){
            this.notifier.alert("Error", "Course required!", "danger", 1000);
            return;
        }
        
        this.productService.getProduct({productCategory:frm.productCategory, productType:frm.productType,stream:frm.stream,course:frm.course, limit:100,status:true}).subscribe(
            res=>{
                this.products=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }

    setCourseAndStream(){
        this.productAddForm.patchValue({course:null, stream:null,product:null});
        this.loadCourses();
        this.loadStreams();
    }

    addUserProductAndFulfill(){
        this.formStatus="Processing";
        let frm=this.productAddForm.value;
        // let data:any=this.activeFilter;
        // data.product=frm.product;
        this.productService.addUserProductToEnrolledUsers(this.productId, frm).subscribe(
            res=>{
                this.notifier.alert('Done', "Product/Program added and fulfilled successfully!", "success", 2000);
                this.formStatus="none";
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
                this.formStatus="none";
            }
        )
    }

}
