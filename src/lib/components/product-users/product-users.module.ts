import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductService } from '../../services/product.service';
import { ProductUsersComponent } from './product-users.component';
import { PaginatorModule, CalendarModule, DialogModule } from 'primeng/primeng';
import { RouterModule } from '@angular/router';
import { StudentInfoComponent } from '../student/info/info.component';
import { RSWindowModule } from '../rswindow';
import { StudentService } from '../../services/student.service';
import { UserService } from '../../services/user.service';
import { UserSearchFormModule } from '../user-search-form/user-search-form.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoptionService } from '../../services/coption.service';

@NgModule({
    declarations:[ProductUsersComponent, StudentInfoComponent],
    exports:[ProductUsersComponent],
    providers:[ProductService, StudentService, UserService,CoptionService],
    imports:[CommonModule, PaginatorModule, RouterModule, RSWindowModule, UserSearchFormModule, DialogModule, ReactiveFormsModule, FormsModule]
})
export class ProductUsersModule{

}
