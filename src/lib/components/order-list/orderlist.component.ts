import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Angular2Csv } from 'angular2-csv';
import { Coption } from '../../models/coption.model';
import { OrderService } from '../../services/order.service';
import { NotifierService } from '../notifier/notifier.service';
import { CoptionService } from '../../services/coption.service';
import { ProductService } from '../../services/product.service';
import { StoreService } from '../../services/store.service';
import { CenterService } from '../../services/center.service';
import { AuthService } from '../../services/auth.service';
import { Settings } from '../../../apps/ims/admin/activities/settings/settings';
import { SettingsService } from '../../services/settings.service';

@Component({
    selector:'ek-order-list-component',
    templateUrl:'./orderlist.component.html'
})
export class OrderListComponent implements OnInit, OnChanges{
    ngOnChanges(changes:SimpleChanges): void {
        // console.log(changes);
        
        let filter:any={};
        if(this.filter){
            filter=this.filter;
        }
        if(this.store){
            filter.store=this.store;
        }
        // console.log(this.store);
        
        if(this.populateStore){
            filter.populateStore=this.populateStore;
        }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        this.activeFilter=filter;
        // console.log(filter);
        
        this.loadOrders(filter);
    } 
    orders;
    columns; 
    panelLoader="none";
    filterOpen:boolean=false;
    filterForm:FormGroup;
    orderStatus:Coption[];
    filterRowForm:FormGroup;
    filteredTags:any;
    filterData:any={};
    activeFilter:any;
    countBase:number=1;
    totalRecords:number;
    userPerms:any;
    regSources : any[];

    @Input() APP:string;
    @Input() ACTIVITY:string;
    @Input() store:string;
    @Input() viewOpt:string;
    @Input() invoiceOpt:boolean;
    @Input() deleteOpt:boolean;
    @Input() filter:any;
    @Input() populateStore:boolean;
    @Input() productCategory:string;
    constructor(
        private orderService:OrderService,
		private notifier: NotifierService,
        private coptionService: CoptionService,
        private productService:ProductService,
        private storeService:StoreService,
        private centerService:CenterService,
        private fb: FormBuilder,
        private authService:AuthService,
        private settingsService : SettingsService
    ){}

    ngOnInit(): void{
        
        this.filterForm=this.fb.group({
            from:[""],
            to:[""],
            status:[""],
            store:[null],
            orderNo:[""],
            invoiceNo:[""],
            customerEmail:[""]
        });
        this.filterRowForm=this.fb.group({
            orderNo:[""],
            isApproved:[null],
            customerEmail:[""],
            srn:[''],
            paymentStatus:[null],
            product:[null],
            createdAt:[""],
            regSource:[""],
            relCenter:[null],
            status:[null]
        });
        this.setFilterRow();
        this.loadRegSource();
        let sessionUser=this.authService.admin();
        this.userPerms=this.authService.getUserPerms(sessionUser.activityPermission, this.APP, this.ACTIVITY, ['EXPORT', 'LIST','VIEW_EMAIL','VIEW']);
    }

    loadOrders(filter?:any):void{
        this.panelLoader="Processing";
        if(!filter){
            filter={};
        }
        filter.populateCenter=true;
        this.orderService.getOrder(filter, true).subscribe(
            (res)=>{

                this.orders=res.body;
                this.totalRecords = res.headers.get('totalRecords')||0;
                this.panelLoader="none";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 2000);
                this.panelLoader="none";
                
            }
        );
        
    }

    deleteOrder(order){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete "+order.name+"?");
        if(!confirm){
            return;
        }
        this.orderService.deleteOrder(order.id).subscribe(
            (res)=>{
                this.orders.splice(this.orders.indexOf(order), 1);
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 2000 );
            }
        );
    }
    approveOrder(orderIndex){
        if(this.orders[orderIndex].status!=="Placed"){
            let conf=window.confirm("This order is not placed yet properly. Are you sure to generate Invoice for this order");
            if(!conf){
                return;
            }
        }
        this.panelLoader="show";
        this.orderService.approveOrder(this.orders[orderIndex].id).subscribe(
            res=>{
                this.panelLoader="none";
                this.orders[orderIndex].isApproved=res.isApproved;
                this.orders[orderIndex].status=res.status;
                this.notifier.alert("Done", 'Approved Successfully!', "success", 1000);

            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 2000);
                this.panelLoader="none";
            }
        )
    }
    setFilterBox(){
        
        this.coptionService.getCoption({option:'ORDERSTATUS'}).subscribe(
            res=>{
                this.orderStatus=res;
            },
            err=>{
                this.notifier.alert(err.code, err.details||err.message, "danger", 1000);
            }
        );
        this.storeService.getStore({status:true}).subscribe(
            res=>{
                this.filterData.stores=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 2000);
            }
        )

        this.filterOpen=true;
    }
    setFilterRow(){
        this.productService.getProduct({status:true}).subscribe(
            res=>{
                this.filterData.products=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 2000);
            }
        );
        this.centerService.getCenter({status:true}).subscribe(
            res=>{
                this.filterData.centers=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 2000);
            }
        )
        
    }
    loadOrdersWithFilter(){
        let fltr=this.filterForm.value;
        
        this.filteredTags=fltr;
        if(this.populateStore){
            fltr.populateStore=this.populateStore;
        }
        if(this.productCategory){
            fltr.productCategory=this.productCategory;
        }
        this.activeFilter=fltr;
        this.countBase=1;
        this.loadOrders(fltr);
    }
    loadOrdersWithRowFilter(){
        let fltr=this.filterRowForm.value;
        if(this.populateStore){
            fltr.populateStore=this.populateStore;
        }
        if(this.productCategory){
            fltr.productCategory=this.productCategory;
        }
        this.activeFilter=fltr;
        this.countBase=1;
        this.loadOrders(fltr);
    }
    clearFilter(){
        this.filterOpen=false;
        this.filterForm.reset();
        this.filteredTags=null;
        if(this.productCategory){
            this.filter.productCategory=this.productCategory;
        }
        this.loadOrders();
    }
    exportData(){
        let fltr:any={};
        if(this.activeFilter){
            fltr=this.activeFilter;
        }
        
        fltr.limit='all';
        this.orderService.exportOrders(fltr).subscribe(
            res=>{
                this.notifier.alert("Done!", "Data exported successfully.", "success", 5000);
                window.location.href=res.fileUrl;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'daner', 2000);
            }
        )
    }
    exportDataOld(){
        var data=[];
        this.orders.forEach(function(order){
            data.push({
                "Order No.":order.orderNo,
                "Invoice No.":order.invoiceNo||'NA',
                "Customer Name":order.buyerDetail.name,
                "Email":order.buyerDetail.email,
                "Mobile":order.buyerDetail.mobile,
                "Amount(INR)":order.total,
                "Discount":order.discount||'',
                "Date":order.createdAt,
                "Store":(order.store&&order.store.title?order.store.title:''),
                "Payment Status":order.paymentStatus||'',
                "Order Status":order.status
            })
        })    

        new Angular2Csv(data, 'Orders List', { 
                fieldSeparator: ',',
                quoteStrings: '"',
                decimalseparator: '.',
                showLabels: true, 
                showTitle: true 
              });
    }
    loadRegSource(){
        let filter={
            key : 'REG_SOURCE'
        };
        this.settingsService.getSettings(filter).subscribe(
            res=>{
                if(res&&res.length>0&&res[0].more){
                    this.regSources=res[0].more.codes;
                }else{
                    this.regSources=["IMS-SALES-ORDER-ADD","STUDENT-ZONE-ORDER-ADD",'ADMISSION-ONLINE-REG'];
                }
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 2000);
        });
    }
    paginate(e){
        if(!this.activeFilter){
            this.activeFilter={populateStore:true};
        }
        this.countBase=e.rows*e.page+1;
        this.activeFilter.page=(e.page+1);
        this.activeFilter.limit=e.rows;
        this.loadOrders(this.activeFilter);
    }
}
