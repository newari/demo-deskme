import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { OrderListComponent } from './orderlist.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { PaginatorModule } from 'primeng/components/paginator/paginator';
import { OrderService } from '../../services/order.service';
import { ProductService } from '../../services/product.service';
import { StoreService } from '../../services/store.service';
import { CenterService } from '../../services/center.service';
import { ProductInputFieldModule } from '../product-inputfield/product-inputfield.module';
import { SettingsService } from '../../services/settings.service'; 

@NgModule({
    declarations:[OrderListComponent], 
    exports:[OrderListComponent],
    providers:[OrderService, ProductService, StoreService, CenterService,SettingsService],
    imports:[CommonModule, FormsModule, ProductInputFieldModule,
        ReactiveFormsModule, RouterModule, PaginatorModule]
})
export class OrderListModule {
    
}
