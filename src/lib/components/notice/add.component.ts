import { Component, OnInit } from '@angular/core';
import { NoticeService } from '../../services/notice.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { News } from '../../models/news.model';
import { Coption } from '../../models/coption.model';
import { Session } from '../../models/session.model';
import { Product } from '../../models/product.model';
import { Batch } from '../../models/batch.model';
import { NotifierService } from '../notifier/notifier.service';
import { CoptionService } from '../../services/coption.service';
import { CenterService } from '../../services/center.service';
import { NewsCategoryService } from '../../services/newscategory.service';
import { ActivatedRoute, Params } from '@angular/router';
import { SessionService } from '../../services/session.service';
import { ProductService } from '../../services/product.service';
import * as moment from 'moment';

@Component({
    selector:'ek-notice-form',
    templateUrl:'./add.component.html'
})
export class NoticeAddComponent implements OnInit{
    newsForm:FormGroup;
	newsData:News;
	formStatus="Normal";
	streams:Coption[];
	courses:Coption[];
	sessions:Session[];
	products:Product[];
	batches:Batch[];
	centres;
	newsCategories:any;
	categoryId;
	activeFilter: any;
	minDateValue= new Date();
	constructor(
		private fb:FormBuilder,
		private noticeService:NoticeService,
		private notifier: NotifierService,
		private coptionService:CoptionService,
		private centreService:CenterService,
		private newsCategoryService:NewsCategoryService,
		private activatedRoute: ActivatedRoute,
		private sessionService: SessionService,
		private productService: ProductService,
		){}

	addNews(): void {
		if (!this.newsForm.valid) {
			return;
		}
		this.formStatus="Processing";
		this.newsForm.value.startDate= new Date(moment(this.newsForm.value.startDate).startOf('date').format());
		this.newsForm.value.endDate= new Date(moment(this.newsForm.value.endDate).endOf('date').format());
		this.newsData=this.newsForm.value;
		if(this.newsData.product&&this.products&&this.products[this.newsData.product]){
			this.newsData.product= this.products[this.newsData.product].id;
		}
		this.noticeService.addNews(this.newsData).subscribe(
			res=>{
				this.newsForm.reset();
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 500);
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		// this.loadCentre();
		// this.loadStream();
		this.loadNewsCategory();
		this.newsForm=this.fb.group({
			title: ['', Validators.required],
			featuredImage:[''],
			htmlContent:[''],
			type: [null, Validators.required],
			category: [null],
			centre: [null,],
			featured: [true,],
			targetUrl: ['', ],
			stream: [null,],
			needStream:[false],
			status:[true, Validators.required],
			order:[0],
			needCenter:[false],
			needSession:[false],
			session:[null],
			needProduct:[false],
			product:[null],
			needBatch:[false],
			batch:[null],
			needCourse:[false],
			course:[null],
			startDate:['', Validators.required],
			endDate:['', Validators.required],
			isPrivate:[false,Validators.required]
		});
		this.activatedRoute.queryParams.subscribe((params: Params) => {
			this.categoryId = params['categoryId'];
			if (this.categoryId && this.categoryId != "undefined") {
				this.newsForm.patchValue({ category: this.categoryId });
			}
		});
	}

	loadStream(){
		this.streams=[];
		if(!this.newsForm.value.needStream||this.newsForm.value.needStream=='false'){
			this.newsForm.value.patchValue({stream:null,status:true});
			return;
		}
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			(response)=>{
				this.streams=response;
				this.notifier.alert('Success', 'Successfully Loaded!', 'success', 500);
			},
			(err)=>{
				this.notifier.alert(err.code, err.message,'danger', 5000);
			}
		);
	}
	loadCourses(){
		this.courses=[];
		if(!this.newsForm.value.needCourse||this.newsForm.value.needCourse=='false'){
			this.newsForm.value.patchValue({course:null});
			return;
		}
		this.coptionService.getCoption({option:'COURSE',status:true}).subscribe(
			(response)=>{
				this.courses=response;
				this.notifier.alert('Success', 'Successfully Loaded!', 'success', 500);
			},
			(err)=>{
				this.notifier.alert(err.code, err.message,'danger', 5000);
			}
		);
	}
	loadCentre(){
		this.centres=[];
		if(!this.newsForm.value.needCenter||this.newsForm.value.needCenter=='false'){
			this.newsForm.value.patchValue({centre:null});
			return;
		}
		this.centreService.getCenter({status:true}).subscribe(
			(response)=>{
				this.centres=response;
				this.notifier.alert('Success', 'Successfully Loaded!', 'success', 500);

			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}
	loadSessions() {
		this.sessions=[];
		if(!this.newsForm.value.needSession||this.newsForm.value.needSession=='false'){
			this.newsForm.value.patchValue({session:null});
			return;
		}
		this.sessionService.getSession({status:true}).subscribe(
			res=>{
				this.sessions= res;
			},err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
   	}
	loadProducts(filter?:any){
		if(!filter){
			filter={};
		}
		filter.limit='all';
		filter.status=true;
		this.products=[];
		this.productService.getProduct(filter).subscribe(
			res=>{
				this.products=res
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		)
	}
	loadNewsCategory(){
		this.newsCategoryService.getNewscategory({limit:'all',status:true}).subscribe(
			(res)=>{
				this.newsCategories=res;
				this.notifier.alert("Success", "Successfully Loaded!!", 'success', 500);
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}

	setFilter(value){
		if(!value||value=='false'){
			return;
		}
		let filter:any={
		}

		if(this.newsForm.value.stream&&this.newsForm.value.stream!=='null'){
			filter.stream= this.newsForm.value.stream;
		}

		if(this.newsForm.value.course&&this.newsForm.value.course!=='null'){
			filter.course= this.newsForm.value.course;
		}

		if(this.newsForm.value.center&&this.newsForm.value.center!='null'){
			filter.center= this.newsForm.value.center;
		}
		if(this.newsForm.value.session&&this.newsForm.value.session!=='null'){
			filter.session= this.newsForm.value.session;
		}
		this.activeFilter= filter;
		this.loadProducts(filter);
	}
	setActiveFilter(){
		let filter:any={};
		if(this.newsForm.value.stream){
			filter.stream= this.newsForm.value.stream;
		}
		if(this.newsForm.value.course){
			filter.course= this.newsForm.value.course;
		}
		if(this.newsForm.value.center){
			filter.center= this.newsForm.value.center;
		}
		if(this.newsForm.value.session){
			filter.session= this.newsForm.value.session;
		}
		this.activeFilter= filter;
		this.loadProducts(filter);
	}
}
