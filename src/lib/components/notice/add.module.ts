import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NoticeAddComponent } from './add.component';
import { NoticeService } from '../../services/notice.service';
import { RouterModule } from '@angular/router';
import { CalendarModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CKEditor4Module } from '../ckeditor4/ckeditor4.module';
import { FileinputModule } from '../filemanager/fileinput.module';
import { CenterService } from '../../services/center.service';
import { NewsCategoryService } from '../../services/newscategory.service';
import { SessionService } from '../../services/session.service';
import { ProductService } from '../../services/product.service';


@NgModule({
    declarations:[
        NoticeAddComponent
    ],
    imports:[RouterModule, CommonModule, CalendarModule, FormsModule, ReactiveFormsModule, CKEditor4Module, FileinputModule],
    exports:[NoticeAddComponent],
    providers:[NoticeService, CenterService, NewsCategoryService, SessionService, ProductService]

})
export class NoticeFormModule { }
