import { NgModule } from '@angular/core';
import { EnquirySearchFormComponent } from './enquiry-search-form.component';
import { TabViewModule } from 'primeng/primeng';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileinputModule } from '../filemanager/fileinput.module';
@NgModule({
    declarations:[EnquirySearchFormComponent],
    imports:[FileinputModule, TabViewModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[EnquirySearchFormComponent]
})
export class EnquirySearchFormModule{

}