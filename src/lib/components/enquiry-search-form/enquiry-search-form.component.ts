import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Enquiry } from '../../models/enquiry.model';
import { EnquiryService } from '../../services/enquiry.service';
import { NotifierService } from '../notifier/notifier.service';
@Component({
    selector:'ek-enquiry-search-form',
    templateUrl:'./enquiry-search-form.component.html'
})
export class EnquirySearchFormComponent implements OnInit{
    
    enquiryData:Enquiry;
    enquiries:any[]=[];
    selectedUser:any;

    @Output() onUserSelect:EventEmitter<any>=new EventEmitter<any>();
    constructor(
		
		private enquiryService:EnquiryService,
		private notifier: NotifierService
		){ 
			
    }
    ngOnInit(): void {
		
    }
    
    set query(q){
        this.enquiryService.searchEnquiry({query:q}).subscribe(
            res=>{
                this.enquiries=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
    selectUser(i){
        this.selectedUser=this.enquiries[i];
        this.onUserSelect.emit(this.enquiries[i]);
        this.enquiries=[];
    }
    hello(){

    }
}