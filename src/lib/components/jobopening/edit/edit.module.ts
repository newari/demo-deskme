import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { JobopeningEditFormComponent } from './edit.component';
import { FileinputModule } from '../../filemanager/fileinput.module';
import { QuillEditorModule } from '../../ngx-quill-editor';
import { CenterService } from '../../../services/center.service';
import { DesignationService } from '../../../services/designation.service';


@NgModule({
    declarations:[JobopeningEditFormComponent],
    imports:[FileinputModule, QuillEditorModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[JobopeningEditFormComponent],
    providers:[CenterService, DesignationService]
})
export class JobopeningEditFormModule{

}