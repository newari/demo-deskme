import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Jobopening } from '../../../models/jobopening.model';
import { Designation } from '../../../models/designation.model';
import { JobopeningService } from '../../../services/jobopening.service';
import { CenterService } from '../../../services/center.service';
import { DesignationService } from '../../../services/designation.service';
import { NotifierService } from '../../notifier/notifier.service';
import { Center } from '../../../models/center.model';


@Component({
    selector:'ek-jobopening-edit-form',
    templateUrl:'./edit.component.html'
})
export class JobopeningEditFormComponent{
    jobopeningForm:FormGroup;
	jobopening:Jobopening;
	centers:Center[];
	designations:Designation[];
	formStatus="Normal";
	panelLoader:string="none";

	@Input() jobopeningId:any;
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
	
	constructor(
		private fb:FormBuilder,
		private jobopeningService:JobopeningService,
		private centerService:CenterService,
		private designationService:DesignationService,
		private notifier: NotifierService
	){ }
	
	getJobopening(jobopeningId){
		this.panelLoader="show;"
		this.jobopeningService.getOneJobopening(jobopeningId).subscribe(
			(res)=>{
				this.jobopening=res;
				this.jobopening.designation=res.designation.id;
				this.jobopening.location=res.location.id;
				this.jobopeningForm.patchValue(res);
				this.panelLoader="none";
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
			}
		)
	}
		
	updateJobopening(): void {
		if(!this.jobopeningForm.dirty){
			return;
		}
		this.formStatus="Processing";
		this.jobopening=this.jobopeningForm.value;
		this.jobopeningService.updateJobopening(this.jobopeningId, this.jobopening).subscribe(
			(res)=>{
				this.onSuccess.emit(res);
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.jobopeningForm=this.fb.group({
			title:['', Validators.required],
			designation:[null, Validators.required],
			location:[null, Validators.required],
			description:[''],
			ctc:[''],
			minExperience:[''],
			stream:[null],
			cv:[null],
			videoCv:[null],
			qualification:[null],
			collegeType:[null],
			collegeName:[null],
			experience:[null],
			jobType:[null],
			availability:[null],
			nfEmail:[''],
			nfMobile:[''],
			status:[true, Validators.required],
		});

		let self=this;
		window.setTimeout(function(){
			self.getJobopening(self.jobopeningId);
			self.loadCenters();
			self.loadDesignations();
		}, 0);
	}
	
	loadCenters(){
		this.centerService.getCenter().subscribe(
			res=>{
				this.centers=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		)
	}

	loadDesignations(){
		this.designationService.getDesignation().subscribe(
			res=>{
				this.designations=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		)
	}
}
