import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { JobopeningAddFormComponent } from './add.component';
import { FileinputModule } from '../../filemanager/fileinput.module';
import { QuillEditorModule } from '../../ngx-quill-editor';
import { CenterService } from '../../../services/center.service';
import { DesignationService } from '../../../services/designation.service';

@NgModule({
    declarations:[JobopeningAddFormComponent],
    imports:[FileinputModule, QuillEditorModule, FormsModule, CommonModule, ReactiveFormsModule],
    exports:[JobopeningAddFormComponent],
    providers:[CenterService, DesignationService]
})
export class JobopeningAddFormModule{
}
