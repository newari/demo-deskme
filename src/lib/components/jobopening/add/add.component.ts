import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Jobopening } from '../../../models/jobopening.model';
import { Designation } from '../../../models/designation.model';
import { JobopeningService } from '../../../services/jobopening.service';
import { CenterService } from '../../../services/center.service';
import { DesignationService } from '../../../services/designation.service';
import { NotifierService } from '../../notifier/notifier.service';
import { Center } from '../../../models/center.model';


@Component({
    selector:'ek-jobopening-add-form',
    templateUrl:'./add.component.html'
})
export class JobopeningAddFormComponent{
    jobopeningForm:FormGroup;
	jobopeningData:Jobopening;
	centers:Center[];
	designations:Designation[];
    formStatus="Normal";
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
    
    constructor(
		private fb:FormBuilder,
		private jobopeningService:JobopeningService,
		private centerService:CenterService,
		private designationService:DesignationService,
		private notifier: NotifierService
    ){ }
	
	addJobopening(): void {
		this.formStatus="Processing";
		this.jobopeningData=this.jobopeningForm.value;
		this.jobopeningService.addJobopening(this.jobopeningData).subscribe(
			res=>{
                this.onSuccess.emit({event:'ItemAdded', item:res});
				this.jobopeningForm.reset(); 
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.jobopeningForm=this.fb.group({
			title:['', Validators.required],
			designation:[null, Validators.required],
			location:[null, Validators.required],
			description:[''],
			ctc:[''],
			minExperience:[''],
			stream:[null],
			cv:[null],
			videoCv:[null],
			qualification:[null],
			collegeType:[null],
			collegeName:[null],
			experience:[null],
			jobType:[null],
			availability:[null],
			nfEmail:[''],
			nfMobile:[''],
			status:[true, Validators.required],
		});
		let self=this;
		window.setTimeout(function(){
			self.loadCenters();
			self.loadDesignations();
		}, 0);
        
	}
	
	loadCenters(){
		this.centerService.getCenter().subscribe(
			res=>{
				this.centers=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		)
	}

	loadDesignations(){
		this.designationService.getDesignation().subscribe(
			res=>{
				this.designations=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		)
	}

    
}