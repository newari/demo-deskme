import { Component, OnInit, Input } from '@angular/core';
import {NgIf} from '@angular/common';
import { ActivatedRoute, Params } from '@angular/router';
import { Jobopening } from '../../../models/jobopening.model';
import { JobopeningService } from '../../../services/jobopening.service';
import { NotifierService } from '../../notifier/notifier.service';


@Component({
    selector:'ek-jobopening-info',
    templateUrl:'./info.component.html'
})
export class JobopeningInfoComponent implements OnInit{ 
    jobopening:Jobopening;
    panelLoader="none";
    activeTab:number=0;
    
    @Input() jobopeningId:any;

    constructor(
        private jobopeningService:JobopeningService,
        private activatedRoute:ActivatedRoute,
        private notifier: NotifierService
    ){}


    ngOnInit(): void{
        let self=this;
		window.setTimeout(function(){
			self.getJobopening(self.jobopeningId);
		}, 0);
    }

    getJobopening(jobopeningId){
        this.panelLoader="show";
        this.jobopeningService.getOneJobopening(jobopeningId).subscribe( (data)=>{
                this.jobopening=data;
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }

    deleteJobopening(jobopeningId){
        let conjobopening=window.confirm("Are you sure to delete this?");
        if(!conjobopening){
            return;
        }
        this.panelLoader="show";
        this.jobopeningService.deleteJobopening(jobopeningId).subscribe(
            (res)=>{
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }
}
