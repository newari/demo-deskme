import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobopeningInfoComponent } from './info.component';

@NgModule({
    declarations:[JobopeningInfoComponent],
    imports:[CommonModule],
    exports:[JobopeningInfoComponent]
})
export class JobopeningInfoModule {
    
}
