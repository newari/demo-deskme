import { Component, Input, Output, EventEmitter, SimpleChange } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { OrderService } from '../../services/order.service';
import { NotifierService } from '../notifier/notifier.service';
@Component({
    selector:'ek-order-status',
    templateUrl:'./component.html'
})
export class OrderStatusComponent{
    panelLoader='none';
    eventTimingForm:FormGroup;

    @Input('eventId') eventId:string;
    @Input('ekModal') ekModal={status:'hide'};
    @Input('ekModalStatus') ekModalStatus='h';///to detect hide show changes
    
    
    @Output('done') done=new EventEmitter();
    event:Event;
    // ekModalStatus=this.ekModal.status;
    constructor(
        private orderService:OrderService,
        private notifier:NotifierService,
        private fb:FormBuilder
    ){
    }

    ngOnInit():void{
        this.eventTimingForm=this.fb.group({
            startedAt:['', Validators.required],
            endedAt:['', Validators.required],
            breakStart:[''],
            breakEnd:[''],
            attendees:['']
        })
    }

    ngOnChanges(changes: {[propKey: string]: SimpleChange}) {
        for (let propName in changes) {
          let changedProp = changes[propName];
        //   let to = JSON.stringify(changedProp.currentValue);
            console.log(changedProp)
        
          if (propName=='ekModalStatus'&&changedProp.currentValue=='show') {
              var _this=this;
              setTimeout(function(){
                _this.loadEvent();
              }, 50);
            
          } 
        }
    }

    loadEvent(){
        console.log("Why this colavery di!");
    }
    
    
    
}