import { NgModule } from '@angular/core';
import { OrderStatusComponent } from './component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OrderService } from '../../services/order.service';

@NgModule({
    declarations:[OrderStatusComponent],
    imports:[CommonModule, FormsModule, ReactiveFormsModule],
    providers:[OrderService],
    exports:[OrderStatusComponent]
})
export class OrderStatusModule {}