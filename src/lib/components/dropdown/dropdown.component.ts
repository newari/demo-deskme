import { Component, ElementRef, Input } from '@angular/core';
 
 @Component({
    selector: 'ek-dropdown',
    host: {
        '(document:click)': 'handleClick($event)',
    },
    template: `
        <div class="ek-dropdown">
            <div class="dropdown">
                <span (click)="changeState()" class="ek-dd-toggle">
                    <ng-content select=".dropdown-toggle"></ng-content>
                </span>
                <ul class="dropdown-menu dropdown-menu-{{direction}}" [ngClass]="state">
                    <ng-content select=".dropdown-menu" ></ng-content>
                </ul>
            </div>
        </div>
    	`
})
export class EkDropdown {
    public elementRef;
    public state='hide';
    @Input() direction:string="left";
    constructor(myElement: ElementRef) {
        this.elementRef = myElement;
    }
  
    handleClick(event){
        var clickedComponent = event.target;
        var inside = false;
        do {
            if (clickedComponent === this.elementRef.nativeElement) {
                inside = true;
            }
            clickedComponent = clickedComponent.parentNode;
        } while (clickedComponent);
        if(inside){
            // console.log('inside');
        }else{
            this.state='hide';
            // console.log('outside');
        }
    }
    changeState(){
        if(this.state=="hide"){
            this.state="show";
        }else{
            this.state="hide";
        }
    }
 }