import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EkDropdown } from './dropdown.component';

@NgModule({
    declarations: [EkDropdown],
    imports: [ CommonModule ],
    exports: [EkDropdown],
    providers: [],
})
export class EkDropdownModule {

}