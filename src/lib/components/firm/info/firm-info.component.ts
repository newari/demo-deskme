import { Component, OnInit, Input } from '@angular/core';
import {NgIf} from '@angular/common';
import { ActivatedRoute, Params } from '@angular/router';
import { Firm } from '../../../models/firm.model';
import { FirmService } from '../../../services/firm.service';
import { NotifierService } from '../../notifier/notifier.service';


@Component({
    selector:'ek-firm-info',
    templateUrl:'./firm-info.component.html'
})
export class FirmInfoComponent implements OnInit{ 
    firm:Firm;
    panelLoader="none";
    activeTab:number=0;
    
    @Input() firmId:any;

    constructor(
        private firmService:FirmService,
        private activatedRoute:ActivatedRoute,
        private notifier: NotifierService
    ){}


    ngOnInit(): void{
        let self=this;
		window.setTimeout(function(){
			self.getFirm(self.firmId);
		}, 0);
    }

    getFirm(firmId){
        this.panelLoader="show";
        this.firmService.getOneFirm(firmId).subscribe( (data)=>{
                this.firm=data;
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }

    deleteFirm(firmId){
        let confirm=window.confirm("Are you sure to delete this?");
        if(!confirm){
            return;
        }
        this.panelLoader="show";
        this.firmService.deleteFirm(firmId).subscribe(
            (res)=>{
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }
}
