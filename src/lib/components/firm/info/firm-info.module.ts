import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirmInfoComponent } from './firm-info.component';

@NgModule({
    declarations:[FirmInfoComponent],
    imports:[CommonModule],
    exports:[FirmInfoComponent]
})
export class FirmInfoModule {
    
}