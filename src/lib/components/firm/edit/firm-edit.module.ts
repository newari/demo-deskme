import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FirmEditFormComponent } from './firm-edit.component';
import { FileinputModule } from '../../filemanager/fileinput.module';

@NgModule({
    declarations:[FirmEditFormComponent],
    imports:[FileinputModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[FirmEditFormComponent]
})
export class FirmEditFormModule{
}