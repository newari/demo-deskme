import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Firm } from '../../../models/firm.model';
import { FirmService } from '../../../services/firm.service';
import { NotifierService } from '../../notifier/notifier.service';


@Component({
    selector:'ek-firm-edit-form',
    templateUrl:'./firm-edit.component.html'
})
export class FirmEditFormComponent{
    firmForm:FormGroup;
	firm:Firm;
	formStatus="Normal";
	panelLoader:string="none";

	@Input() firmId:any;
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
	
	constructor(
		private fb:FormBuilder,
		private firmService:FirmService,
		private notifier: NotifierService
	){ }
	
	getFirm(firmId){
		this.panelLoader="show;"
		this.firmService.getOneFirm(firmId).subscribe(
			(res)=>{
				this.firm=res;
				this.firmForm.patchValue(res);
				this.panelLoader="none";
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
			}
		)
	}
		
	updateFirm(): void {
		if(!this.firmForm.dirty){
			return;
		}
		this.formStatus="Processing";
		this.firm=this.firmForm.value;
		this.firmService.updateFirm(this.firmId, this.firm).subscribe(
			(res)=>{
				this.onSuccess.emit(res);
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.firmForm=this.fb.group({
			name:['', Validators.required],
			type:['', Validators.required],
			allowStdReg:[true, Validators.required],
			srnPrefix:[''],
			gstin:[''],
			email:['', Validators.required],
			phone:['', Validators.required],
			logo:[''],
			directorSign:[''],
			logoPath:[''],
			address:['', Validators.required],
			city:['', Validators.required],
			state:['', Validators.required],
			country:['India', Validators.required],
			signature:['', Validators.required],
			status:[true, Validators.required],
		});

		let self=this;
		window.setTimeout(function(){
			self.getFirm(self.firmId);
		}, 0);
	}
	onLogoSelect(file){
		this.firmForm.patchValue({logoPath:file.path});
		console.log(file);
	}
}