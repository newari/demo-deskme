import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Firm } from '../../../models/firm.model';
import { FirmService } from '../../../services/firm.service';
import { NotifierService } from '../../notifier/notifier.service';


@Component({
    selector:'ek-firm-add-form',
    templateUrl:'./firm-add.component.html'
})
export class FirmAddFormComponent{
    firmForm:FormGroup;
	firmData:Firm;
    formStatus="Normal";
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
    
    constructor(
		private fb:FormBuilder,
		private firmService:FirmService,
		private notifier: NotifierService
    ){ }
	
	addFirm(): void {
		this.formStatus="Processing";
		this.firmData=this.firmForm.value;
		this.firmService.addFirm(this.firmData).subscribe(
			res=>{
                this.onSuccess.emit({event:'ItemAdded', item:res});
				this.firmForm.reset(); 
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.firmForm=this.fb.group({
			name:['', Validators.required],
			type:['', Validators.required],
			allowStdReg:[true, Validators.required],
			srnPrefix:[''],
			gstin:[''],
			email:['', Validators.required],
			phone:['', Validators.required],
			logo:['', Validators.required],
			directorSign:[''],
			logoPath:[''],
			address:['', Validators.required],
			city:['', Validators.required],
			state:['', Validators.required],
			country:['India', Validators.required],
			signature:[''],
			status:[true, Validators.required],
        });
        
	}
	
	onLogoSelect(file){
		this.firmForm.patchValue({logoPath:file.path});
	}
	

   
    
    
}