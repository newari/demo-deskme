import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FirmAddFormComponent } from './firm-add.component';
import { CommonModule } from '@angular/common';
import { FileinputModule } from '../../filemanager/fileinput.module';

@NgModule({
    declarations:[FirmAddFormComponent],
    imports:[FileinputModule, FormsModule, ReactiveFormsModule, CommonModule],
    exports:[FirmAddFormComponent]
})
export class FirmAddFormModule{
}