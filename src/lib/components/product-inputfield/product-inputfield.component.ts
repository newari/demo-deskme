import { Component, OnInit, EventEmitter, Output, Input, forwardRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { NotifierService } from '../notifier/notifier.service';
import { ProductService } from '../../services/product.service';

@Component({
    selector:'ek-product-input',
    templateUrl:'./product-inputfield.component.html',
    providers: [
        { 
          provide: NG_VALUE_ACCESSOR,
          useExisting: forwardRef(() => ProductInputFiledComponent),
          multi: true
        }
      ]
})
export class ProductInputFiledComponent implements ControlValueAccessor{
    
    products:any[]=[];
    selectedProduct:any;
    selectedProductId:string;
    timer:any;

    @Output() onProductSelect:EventEmitter<any>=new EventEmitter<any>();
    @Input('session') session:any;
    @Input('center') center:any;
    @Input("elmClass") elmClass:string;
    @Input() filter:any; 
    constructor(
		
		private productService:ProductService,
		private notifier: NotifierService
		){ 
			
    }

    writeValue(value: any) {
        if (value !== undefined) {
            this.selectedProductId = value;
        }
        
    }
    
    propagateChange = (_: any) => {};

    registerOnChange(fn) {
        this.propagateChange = fn;
    }
    
    registerOnTouched() {}
    
    
    set query(alias){
        if(this.timer){
            clearTimeout(this.timer);
        }
        let self=this;
        let filter:any={alias:alias, status:'all'};
        if (self.filter&&self.filter!=undefined) {
            if (self.filter.category&&self.filter.category!=null) {
                filter.category  = self.filter.category;
            }
            if (self.filter.type&&self.filter.type!=null) {
                filter.type  = self.filter.type;
            }
            if (self.filter.session&&self.filter.session!=null) {
                filter.session = self.filter.session;
            }
            if (self.filter.center&&self.filter.center!=null) {
                filter.center  = self.filter.center;
            }
            if (self.filter.stream&&self.filter.stream!=null) {
                filter.stream  = self.filter.stream;
            }
            if (self.filter.course&&self.filter.course!=null) {
                filter.course  = self.filter.course;
            }
        }
        
        this.timer = setTimeout(function() {
            self.propagateChange(null);
            self.search(filter);

            // console.log(q)
        }, 400);
    }
    search(q){
        this.productService.searchProduct(q).subscribe(
            res=>{
                this.products=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
    selectProduct(i){
        this.selectedProduct=this.products[i];
        this.onProductSelect.emit(this.selectedProduct);
        this.propagateChange(this.products[i].id);
        this.products=[];
    }
    
}