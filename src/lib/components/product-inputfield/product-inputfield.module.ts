import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductInputFiledComponent } from './product-inputfield.component';

@NgModule({
    declarations:[ProductInputFiledComponent],
    imports:[CommonModule, FormsModule, ReactiveFormsModule],
    exports:[ProductInputFiledComponent]
})
export class ProductInputFieldModule{

}