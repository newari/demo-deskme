import { NgModule } from '@angular/core';
import { BookStockAddComponent } from './book-stock-add.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookService } from '../../services/book.service';

@NgModule({
    declarations:[BookStockAddComponent],
    exports:[BookStockAddComponent],
    providers:[BookService],
    imports:[CommonModule, FormsModule, ReactiveFormsModule]
})
export class BookStockAddModule{
    
}