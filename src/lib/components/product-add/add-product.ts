import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Product } from '../../models/product.model';
import { Coption } from '../../models/coption.model';
import { Session } from '../../models/session.model';
import { Batch } from '../../models/batch.model';
import { Firm } from '../../models/firm.model';
import { Store } from '../../models/store.model';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { NotifierService } from '../../components/notifier/notifier.service';
import { StoreService } from '../../services/store.service';
import { BatchService } from '../../services/batch.service';
import { CenterService } from '../../services/center.service';
import { SessionService } from '../../services/session.service';
import { FirmService } from '../../services/firm.service';
import { CoptionService } from '../../services/coption.service';
import { Center } from '../../models/center.model';

@Component({
    selector:'ek-product-add',
    templateUrl:'./add-product.html'
})
export class ProductAddContent implements OnInit{
    productForm:FormGroup;
	frmLoader="none";
	productData:Product;
	formStatus="Normal";
	courses:Coption[];
	productCategories:Coption[];
	productTypes:Coption[];
	streams:Coption[];
	sessions:Session[];
	products:Product[];
	batches:Batch[];
	addedBatches:any[];
	batchBoxStatus:boolean;
	centers:Center[];
	firms:Firm[];

	emiForm:FormGroup;
	addedEMIes:any[];
	EMIBoxStatus:boolean;
	emiCounts:number[];

	baseAmtBoxStatus:boolean;
	baseBkgAmt:any;
	isCombo:boolean;

	customParamsBoxStatus:boolean;
	customParamFrom:FormGroup;
	addedCustomParams:any={};
	addedCustomParamsKeys:string[];
	needCourseProgram:boolean;
	cmbPanelLoader:string="none";
	addedProducts:any[]=[];
	queryParams:any;
	batchFilter?:any={
		course:'',
		session:'',
		center:'',
		limit:'all'
	};
	totalRecords:number;
	availableStores:Store[];
	constructor(
		private fb:FormBuilder,
		private activatedRoute:ActivatedRoute,
		private productService:ProductService,
		private notifier: NotifierService,
		private storeService:StoreService,
		private batchService:BatchService,
		private centerService:CenterService,
		private sessionService:SessionService,
		private firmService:FirmService,
		private coptionService:CoptionService
	){

	}

	loadCourses(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'COURSE'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.courses=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}

	loadStores(){
		this.storeService.getStore().subscribe(
			res=>{
				this.availableStores=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger");
			}
		)
	}

	loadProductCategories(){
		this.frmLoader="show";
		this.coptionService.getCoptionProductCategory().subscribe(
			res=>{
				this.frmLoader="none";
				this.productCategories=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadProductTypes(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'PRODUCTTYPE'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.productTypes=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
	}

	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
	}

	loadSessions(){
		this.frmLoader="show";
		this.sessionService.getSession({status:true}).subscribe(
			res=>{
				this.frmLoader="none";
				this.sessions=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
	}

	addProduct(): void {
		this.productData=this.productForm.value;
		if(this.isCombo){
			if(this.addedProducts.length<2){
				this.notifier.alert("Combo Error", "Please add atleast 2 products in this combo!", "danger", 5000);
				return;
			}
			this.productData.comboProducts=this.addedProducts.map((prdct)=>{
				return prdct.id;
			});
		}else{
			this.productData.sourceModel=this.getSourceModel(this.productData.type);
		}
		this.formStatus="Processing";
		if(this.productData.shipping){
			this.productData.orderProcessSteps=['Processed', 'Shipped', 'Delivered', 'Completed'];
		}else{
			this.productData.orderProcessSteps=['Processed', 'Completed'];
		}

		if(this.productData.manageStock){
			this.productData.allTimeAccountingStock=this.productData.accountingStock;
			this.productData.allTimePhysicalStock=this.productData.physicalStock;
		}

		if(this.batchBoxStatus){
			this.productData.haveBatch=true;
			this.productData.batches=this.addedBatches;
		}else{
			this.productData.haveBatch=false;
			this.productData.batches=null;
		}

		if(this.EMIBoxStatus){
			this.productData.haveInstallement=true;
			this.productData.installementOptions=this.addedEMIes;
		}else{
			this.productData.haveInstallement=false;
			this.productData.installementOptions=null;
		}
		if(this.baseAmtBoxStatus&&this.baseBkgAmt.amount>0&&this.baseBkgAmt.title.trim()!=""){
			this.productData.basicBookingOpt={title:this.baseBkgAmt.title, amount:this.baseBkgAmt.amount};
		}else{
			this.productData.basicBookingOpt=null;
		}

		this.productData.isCombo=this.isCombo;

		if(this.customParamsBoxStatus&&this.addedCustomParamsKeys.length>0){
			this.productData.customParam=this.addedCustomParams;
		}
		if (this.needCourseProgram) {
			this.productData.needCourseProgram = this.needCourseProgram
		}
		this.productService.addProduct(this.productData).subscribe(
			res=>{
				this.productForm.reset();
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {

		this.productForm=this.fb.group({
			title:['', Validators.required],
			alias:['', Validators.required],
			course:['', Validators.required],
			stream:['', Validators.required],
			session:['', Validators.required],
			category:['', Validators.required],
			type:['', Validators.required],
			description:['', Validators.required],
			fullDescription:['', Validators.required],
			mrp:['', Validators.required],
			cost:['', Validators.required],
			disableFullPriceOptOnOrder:[null],
			pmtOptAsComboItems:[null],
			manageStock:[false, Validators.required],
			allTimeAccountingStock:[0],
			allTimePhysicalStock:[0],
			accountingStock:[0],
			physicalStock:[0],
			thumb:['', Validators.required],
			featureImg:['', Validators.required],
			uri:['', Validators.required],
			store:[null],
			sourceId:[''],
			shipping:[false],
			needStdReg:[null],
			stdRegInsty:[null],
			centerDependent:[null],
			center:[null],
			mandatoryFor:['All'],
			status:[true, Validators.required],
			discount:this.fb.group({
				currentStudentDiscount:[0],
				exStudentDiscount:[0],
				fullPaymentDiscount:[0],
			}),
			needCourseProgram:[false]
		});
		this.customParamFrom=this.fb.group({
			key:['', Validators.required],
			value:['', Validators.required],
		});
		this.loadCourses();
		this.loadProductCategories();
		this.loadProductTypes();
		this.loadStreams();
		this.loadSessions();
		this.loadStores();
		this.activatedRoute.queryParams.subscribe((qp) => {
            this.queryParams=qp;
			let defaultProductData:any={};

			if("title" in qp){
				defaultProductData.title=qp['title'];
				defaultProductData.alias=qp['title'];
			}

			if("course" in qp){
				defaultProductData.course=qp['course'];
			}
			if("stream" in qp){
				defaultProductData.stream=qp['stream'];
			}
			if("productCategory" in qp){
				defaultProductData.category=qp['productCategory'];
			}
			if("productType" in qp){
				defaultProductData.type=qp['productType'];
			}
			if("sourceId" in qp){
				defaultProductData.sourceId=qp['sourceId'];
			}
			this.productForm.patchValue(defaultProductData);
	    });
	}

	setProductType(){
		if(this.productForm.value.type!='30707264637474636f6d626f'&&this.productForm.value.type!="30707264637474707374626b"){ //here 30707264637474636f6d626f=Combo,30707264637474707374626b=Postal
			return;
		}
		this.cmbPanelLoader="show";
		this.productService.getProduct().subscribe(
			res=>{
				this.products=res.filter(function(p){
					return (p.type&&p.type.id!='30707264637474636f6d626f'&&p.type.id!='30707264637474707374626b');
				});
				this.cmbPanelLoader="none";
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger");
				this.cmbPanelLoader="none";
			}
		)
	}

	addComboProduct(selectedPrdct):void{
		this.cmbPanelLoader="show";
		this.productService.getOneProduct(selectedPrdct.id).subscribe(
			res=>{
				let product=res;
				let psteps=['Processed', 'Completed'];
				if(product.shipping){
					psteps=['Processed', 'Shipped', 'Delivered', 'Completed'];
				}
				if(!this.addedProducts){
					this.addedProducts=[];
				}
				this.addedProducts.push({
					id:product.id,
					title:product.title,
					alias:product.alias
					// orderProcessSteps:psteps,
					// type:product.type,
					// total:product.cost,
					// store:(product.store.id||product.store)
				});
				this.cmbPanelLoader="none";
			},
			err=>{
				this.cmbPanelLoader="none";
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)

	}

	removeComboProduct(cpIndex):void{
		this.addedProducts.splice(cpIndex, 1);
	}

	getSourceModel(type:string){
		let sourceModels = {
            'Test': 'test',
            'Test Series': 'testseries',
            'Test Package': 'testpackage',
            'Book': 'book'
        };
		return sourceModels[type];
	}

	setBatchBox(el){
		if(el.target.checked){
			this.batchBoxStatus=true;
			let filter: any = { limit: 'all'};
			if(this.productForm.value.session){
				filter.session = this.productForm.value.session;
			}
			if (this.productForm.value.center) {
				filter.center = this.productForm.value.center;
			}
			this.loadBatches(filter);
		}else{
			this.batchBoxStatus=false;
		}
	}
	loadBatches(filter: any) {
		if(!filter){filter.limit='all'}
		this.batchService.getBatch(filter).subscribe(
			res => {
				this.batches = res;
			},
			err => {
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}

	addBatch(batchIndex){
		if(!this.addedBatches){
			this.addedBatches=[];
		}
		for(let i=0; i<this.addedBatches.length; i++){
			if(this.addedBatches[i].id==this.batches[batchIndex].id){
				return;
			}
		}
		this.addedBatches.push({id:this.batches[batchIndex].id, totalSeats:this.batches[batchIndex].totalSeats, filledSeats:this.batches[batchIndex].filledSeats, name:this.batches[batchIndex].name, code:this.batches[batchIndex].code});
	}

	removeAddedBatch(index){
		this.addedBatches.splice(index, 1);
	}
	loadFirms(){

		if(this.productForm.value.needStdReg==false){
			return
		}
		this.firmService.getFirm().subscribe(
			res=>{
				this.firms=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)

	}

	loadCenters(){

		if(this.productForm.value.centerDependent==false){
			return
		}
		this.centerService.getCenter().subscribe(
			res=>{
				this.centers=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)

	}
	setEMIBox(el){
		if((typeof(el)=="boolean"&&el==true)||el.target.checked){
			this.EMIBoxStatus=true;
			this.emiForm=this.fb.group({
				title:["", Validators.required],
				remark:[""],
				installements:this.fb.array([]),
				status:[true]
			});
		}else{
			this.EMIBoxStatus=false;
		}
	}

	setInstallementAmtField(ei){
		return this.fb.group({
            amount:[0],
            title:['I-'+(ei+1)]
        });
	}

	setEMICounts(emiCounts){
		if(emiCounts>60){
			emiCounts=60;
		}

		let insts=[];
		for(let ei=0; ei<emiCounts; ei++){
			insts.push(this.setInstallementAmtField(ei));
		}
		this.emiForm.setControl("installements", this.fb.array(insts));
		if(emiCounts==0){
			this.emiCounts=[];
		}else{
			this.emiCounts=new Array(parseInt(emiCounts));
		}

	}


	addEMI(){
		if(!this.addedEMIes){
			this.addedEMIes=[];
		}
		let emiData=this.emiForm.value;
		this.addedEMIes.push(emiData);
		this.emiForm.reset();
	}

	removeAddedEMI(index){
		this.addedBatches.splice(index, 1);
	}


	setBaseAmtBox(el){
		if((typeof(el)=="boolean"&&el==true)||el.target.checked){
			this.baseAmtBoxStatus=true;
			this.baseBkgAmt={
				title:'',
				amount:0
			}
		}else{
			this.baseBkgAmt=null;
			this.baseAmtBoxStatus=false;
		}
	}
	setCustomParamsBox(el){
		if((typeof(el)=="boolean"&&el==true)||el.target.checked){
			this.customParamsBoxStatus=true;
		}else{
			this.customParamsBoxStatus=false;
		}
	}
	setComboBox(el){
		if((typeof(el)=="boolean"&&el==true)||el.target.checked){
			this.isCombo=true;
			this.loadProductsForCombo();

		}else{
			this.isCombo=null;
		}
	}

	loadProductsForCombo(){
		this.cmbPanelLoader="show";
		this.productService.getProduct({limit:100}).subscribe(
			res=>{
				this.cmbPanelLoader="none";
				this.products=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger");
				this.cmbPanelLoader="none";
			}
		)
	}

	addCustomParam(){
		if(!this.customParamFrom.valid){
			return;
		}
		let frm=this.customParamFrom.value;
		if(!this.addedCustomParamsKeys){
			this.addedCustomParamsKeys=[];
		}
		if(!this.addedCustomParams[frm.key]){
			this.addedCustomParams[frm.key]=frm.value;
		}

		this.addedCustomParamsKeys.push(frm.key);
		this.customParamFrom.reset();

	}

	addCourseProgram(el){
		if ((typeof (el) == "boolean" && el == true) || el.target.checked) {
			this.needCourseProgram = true;
		} else {
			this.needCourseProgram = null;
		}
	}
	setBatches(data?:any){
		if (!this.batchBoxStatus) {
			return;
		}
		if(data){
			if (data.center) this.batchFilter.center = data.center;
			if (data.course) this.batchFilter.course = data.course;
			if (data.session) this.batchFilter.session = data.session;
			this.loadBatches(this.batchFilter);
		}
	}
}
