import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductAddContent } from './add-product';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MultiSelectModule } from 'primeng/primeng';
import { AuthGuard } from '../../services/auth-guard.service';
import { ProductInputFieldModule } from '../product-inputfield/product-inputfield.module';
import { FileinputModule } from '../filemanager/fileinput.module';
import { SessionService } from '../../services/session.service';
import { StoreService } from '../../services/store.service';
import { BatchService } from '../../../apps/faculty/batch/services/batch.service';
import { CenterService } from '../../services/center.service';
import { FirmService } from '../../services/firm.service';

export const ROUTES:Routes=[
    {path: '', component: ProductAddContent, canActivate: [AuthGuard], pathMatch:'full', data: {title: 'Add new Product/Program'}},
];
@NgModule({
    declarations:[ProductAddContent],
    imports: [FileinputModule , MultiSelectModule, FormsModule, ReactiveFormsModule, CommonModule, ProductInputFieldModule, RouterModule.forChild(ROUTES)],
    providers:[SessionService, StoreService, BatchService, CenterService, FirmService]
})
export class ProductAddComponentModule { }

