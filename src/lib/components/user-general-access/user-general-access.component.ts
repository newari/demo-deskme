import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { NotifierService } from '../notifier/notifier.service';
import { SessionService } from '../../services/session.service';
import { CenterService } from '../../services/center.service';
import { FirmService } from '../../services/firm.service';
import { StoreService } from '../../services/store.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { Session } from '../../models/session.model';
import { Center } from '../../models/center.model';
import { Firm } from '../../models/firm.model';
import { Store } from '../../models/store.model';
import { FinancialYear } from '../../models/financial-year.model';

@Component({
    selector:'ek-user-general-access-form',
    templateUrl:'./user-general-access.component.html'
})
export class UserGeneralAccessComponent implements OnInit{
    
    userData:User;
    users:any[]=[];
    selectedUser:any;
    defaultParamsForm:FormGroup;
    dfFormStatus:string="Normal";
    sessions:Session[];
    centers:Center[];
    firms:Firm[];
    stores:Store[];
    fys:FinancialYear[];
    @Input() userId:string;
    constructor(
        private fb:FormBuilder,
		private userService:UserService,
        private notifier: NotifierService,
        private sessionService:SessionService,
        private centerService:CenterService,
        private firmService:FirmService,
        private storeService:StoreService,
        private fyService:FinancialYearService
    ){ 
			
    }
    ngOnInit(): void {
        // this.userService.getOneUser(this.userId).subscribe(
        //     res=>{

        //     },
        //     err=>{

        //     }
        // )
		this.defaultParamsForm=this.fb.group({
			session:[null],
			center:[null],
			firm:[null],
			store:[null],
			financialYear:[null]
        });
        this.loadSession();
        this.loadCenter();
        this.loadFirm();
        this.loadStore();
        this.loadFy();
        
    }
    

    loadSession(){
        this.sessionService.getSession().subscribe(
            res=>{
                this.sessions=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }
    loadCenter(){
        this.centerService.getCenter().subscribe(
            res=>{
                this.centers=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }
    loadFirm(){
        this.firmService.getFirm().subscribe(
            res=>{
                this.firms=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }
    loadStore(){
        this.storeService.getStore().subscribe(
            res=>{
                this.stores=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }
    loadFy(){
        this.fyService.getFinancialYear().subscribe(
            res=>{
                this.fys=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }

    updateDFParams(){
        let frmData=this.defaultParamsForm.value;
        let data:any={};
        if(this.sessions[frmData.session]){
            data.session={id:this.sessions[frmData.session].id, name:this.sessions[frmData.session].name};
        }
        if(this.centers[frmData.center]){
            data.center={id:this.centers[frmData.center].id, title:this.centers[frmData.center].title};
        }
        if(this.firms[frmData.firm]){
            data.firm={id:this.firms[frmData.firm].id, name:this.firms[frmData.firm].name};
        }
        if(this.stores[frmData.store]){
            data.store={id:this.stores[frmData.store].id, title:this.stores[frmData.store].title};
        }
        if(this.fys[frmData.financialYear]){
            data.financialYear={id:this.fys[frmData.financialYear].id, title:this.fys[frmData.financialYear].title};
        }

        this.userService.updateUser(this.userId, {defaultParams:data}).subscribe(
            res=>{
                this.notifier.alert("Success", "Updated successfully", "success", 5000);
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }

    
    
}