import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserGeneralAccessComponent } from './user-general-access.component';
import { SessionService } from '../../services/session.service';
import { CenterService } from '../../services/center.service';
import { FirmService } from '../../services/firm.service';
import { StoreService } from '../../services/store.service';
import { FinancialYearService } from '../../services/financial-year.service';
@NgModule({
    declarations:[UserGeneralAccessComponent],
    imports:[CommonModule, FormsModule, ReactiveFormsModule],
    exports:[UserGeneralAccessComponent],
    providers:[SessionService, CenterService, FirmService, StoreService, FinancialYearService]
})
export class UserGeneralAccessModule{

}