import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { JobapplicationAddFormComponent } from './add.component';
import { FileinputModule } from '../../filemanager/fileinput.module';
import { JobopeningService } from '../../../services/jobopening.service';

@NgModule({
    declarations:[JobapplicationAddFormComponent],
    imports:[FileinputModule, FormsModule, CommonModule, ReactiveFormsModule],
    exports:[JobapplicationAddFormComponent],
    providers:[JobopeningService]
})
export class JobapplicationAddFormModule{
}
