import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Jobapplication } from '../../../models/jobapplication.model';
import { Jobopening } from '../../../models/jobopening.model';
import { JobapplicationService } from '../../../services/jobapplication.service';
import { JobopeningService } from '../../../services/jobopening.service';
import { NotifierService } from '../../notifier/notifier.service';


@Component({
    selector:'ek-jobapplication-add-form',
    templateUrl:'./add.component.html'
})
export class JobapplicationAddFormComponent{
    jobapplicationForm:FormGroup;
	jobapplicationData:Jobapplication;
	formStatus="Normal";
	jobOpenings:Jobopening[];
	selectedOpening:Jobopening;
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
    
    constructor(
		private fb:FormBuilder,
		private jobapplicationService:JobapplicationService,
		private openingService:JobopeningService,
		private notifier: NotifierService
    ){ }
	
	addJobapplication(): void {
		this.formStatus="Processing";
		this.jobapplicationData=this.jobapplicationForm.value;
		this.jobapplicationService.addJobapplication(this.jobapplicationData).subscribe(
			res=>{
                this.onSuccess.emit({event:'ItemAdded', item:res});
				this.jobapplicationForm.reset(); 
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.jobapplicationForm=this.fb.group({
			jobopening:[null, Validators.required],
			firstName:['', Validators.required],
			lastName:['', Validators.required],
			email:['', Validators.required],
			mobile:['', Validators.required],
			dob:['', Validators.required],
			gender:[null, Validators.required],
			address:this.fb.group({
				address:['', Validators.required],
				landmark:[''],
				city:['', Validators.required],
				state:['', Validators.required],
				postalCode:['', Validators.required],
				country:['India', Validators.required],
			}),
			personalImg:[''],
			signImg:[''],
			cv:[''],
			cvVideo:[''],
			stream:[null],
			status:[true, Validators.required],
		});
		let self=this;
		window.setTimeout(function(){
			self.loadOpenings();
		}, 0);
        
    }
	
	loadOpenings(){
		this.openingService.getJobopening().subscribe(
			res=>{
				this.jobOpenings=res;
			},
			err=>{
				this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
			}
		)
	}

	setOpeningParams(i){
		this.selectedOpening=this.jobOpenings[i];
	}
   
    
    
}
