import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { JobapplicationEditFormComponent } from './edit.component';
import { FileinputModule } from '../../filemanager/fileinput.module';

@NgModule({
    declarations:[JobapplicationEditFormComponent],
    imports:[FileinputModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[JobapplicationEditFormComponent]
})
export class JobapplicationEditFormModule{
}
