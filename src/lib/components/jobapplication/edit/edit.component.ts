import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Jobapplication } from '../../../models/jobapplication.model';
import { Jobopening } from '../../../models/jobopening.model';
import { JobapplicationService } from '../../../services/jobapplication.service';
import { JobopeningService } from '../../../services/jobopening.service';
import { NotifierService } from '../../notifier/notifier.service';


@Component({
    selector:'ek-jobapplication-edit-form',
    templateUrl:'./edit.component.html'
})
export class JobapplicationEditFormComponent{
    jobapplicationForm:FormGroup;
	jobapplication:Jobapplication;
	formStatus="Normal";
	jobOpenings:Jobopening[];
	selectedOpening:Jobopening;
	panelLoader:string="none";

	@Input() jobapplicationId:any;
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
	
	constructor(
		private fb:FormBuilder,
		private jobapplicationService:JobapplicationService,
		private openingService:JobopeningService,
		private notifier: NotifierService
	){ }
	
	getJobapplication(jobapplicationId){
		this.panelLoader="show;"
		this.jobapplicationService.getOneJobapplication(jobapplicationId).subscribe(
			(res)=>{
				this.jobapplication=res;

				this.jobapplication.jobopening=res.jobopening.id;
				this.jobapplicationForm.patchValue(res);
				this.panelLoader="none";
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
			}
		)
	}
		
	updateJobapplication(): void {
		if(!this.jobapplicationForm.dirty){
			return;
		}
		this.formStatus="Processing";
		this.jobapplication=this.jobapplicationForm.value;
		this.jobapplicationService.updateJobapplication(this.jobapplicationId, this.jobapplication).subscribe(
			(res)=>{
				this.onSuccess.emit(res);
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.jobapplicationForm=this.fb.group({
			jobopening:[null, Validators.required],
			firstName:['', Validators.required],
			lastName:['', Validators.required],
			email:['', Validators.required],
			mobile:['', Validators.required],
			dob:['', Validators.required],
			gender:[null, Validators.required],
			address:this.fb.group({
				address:['', Validators.required],
				landmark:[''],
				city:['', Validators.required],
				state:['', Validators.required],
				postalCode:['', Validators.required],
				country:['India', Validators.required],
			}),
			personalImg:[''],
			signImg:[''],
			cv:[''],
			cvVideo:[''],
			stream:[null],
			status:[true, Validators.required],
		});

		let self=this;
		window.setTimeout(function(){
			self.getJobapplication(self.jobapplicationId);
			self.loadOpenings();
		}, 0);
	}
	
	loadOpenings(){
		this.openingService.getJobopening().subscribe(
			res=>{
				this.jobOpenings=res;
			},
			err=>{
				this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
			}
		)
	}

	setOpeningParams(i){
		this.selectedOpening=this.jobOpenings[i];
	}
}
