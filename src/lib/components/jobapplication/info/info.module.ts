import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobapplicationInfoComponent } from './info.component';

@NgModule({
    declarations:[JobapplicationInfoComponent],
    imports:[CommonModule],
    exports:[JobapplicationInfoComponent]
})
export class JobapplicationInfoModule {
    
}
