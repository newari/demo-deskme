import { Component, OnInit, Input } from '@angular/core';
import {NgIf} from '@angular/common';
import { ActivatedRoute, Params } from '@angular/router';
import { Jobapplication } from '../../../models/jobapplication.model';
import { JobapplicationService } from '../../../services/jobapplication.service';
import { NotifierService } from '../../notifier/notifier.service';


@Component({
    selector:'ek-jobapplication-info',
    templateUrl:'./info.component.html'
})
export class JobapplicationInfoComponent implements OnInit{ 
    jobapplication:Jobapplication;
    panelLoader="none";
    activeTab:number=0;
    
    @Input() jobapplicationId:any;

    constructor(
        private jobapplicationService:JobapplicationService,
        private activatedRoute:ActivatedRoute,
        private notifier: NotifierService
    ){}


    ngOnInit(): void{
        let self=this;
		window.setTimeout(function(){
			self.getJobapplication(self.jobapplicationId);
		}, 0);
    }

    getJobapplication(jobapplicationId){
        this.panelLoader="show";
        this.jobapplicationService.getOneJobapplication(jobapplicationId).subscribe( (data)=>{
                this.jobapplication=data;
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }

    deleteJobapplication(jobapplicationId){
        let conjobapplication=window.confirm("Are you sure to delete this?");
        if(!conjobapplication){
            return;
        }
        this.panelLoader="show";
        this.jobapplicationService.deleteJobapplication(jobapplicationId).subscribe(
            (res)=>{
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }
}
