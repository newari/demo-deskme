import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProductListComponent } from './productlist.component';
import { CommonModule } from '@angular/common';
import { PaginatorModule } from 'primeng/components/paginator/paginator';
import { ProductService } from '../../services/product.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { EkDropdownModule } from '../dropdown/dropdown.module';
import { DialogModule, InputSwitchModule } from 'primeng/primeng';
import { SessionService } from '../../services/session.service';

@NgModule({
    declarations:[ProductListComponent],
    imports:[ CommonModule, FormsModule,
        ReactiveFormsModule, RouterModule, PaginatorModule, EkDropdownModule, DialogModule, InputSwitchModule],
    exports:[ProductListComponent],
    providers: [ProductService, SessionService]
})
export class ProductListModule {
    
}