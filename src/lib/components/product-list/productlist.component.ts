import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { NotifierService } from '../notifier/notifier.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { CoptionService } from '../../services/coption.service';
import { Coption } from '../../models/coption.model';
import { Session } from '../../models/session.model';
import { SessionService } from '../../services/session.service';
import { AuthService } from '../../services/auth.service';

@Component({
    selector:'ek-product-list-component',
    templateUrl:'./productlist.component.html'
})
export class ProductListComponent implements OnInit{
    rows;
    columns;
    panelLoader="none";
    activeFilter:any={};
    countBase:number=1;
    totalRecords:number;
    filterRowForm:FormGroup;
    streams:Coption[];
    productTypes:Coption[];

    @Input() store:string;
    @Input() viewOpt:string;
    @Input() enrollementsOpt:string;
    @Input() editOpt:string;
    @Input() deleteOpt:boolean;
    @Input() filter:any;
    @Input() freeProducts: boolean = false;
    @Input() APP:string;
    @Input() ACTIVITY:string;
    showDupForm: boolean;
    duplicateProductForm: FormGroup;
    sessions :Session[];
    selectedProduct:any={};
    formStatus;
    checked:boolean=true;
    userPerms:any;
    constructor(
        private productService:ProductService,
		private notifier: NotifierService,
		private fb: FormBuilder,
        private coptionService: CoptionService,
        private sessionService:SessionService,
        private authService:AuthService
    ){}

    ngOnInit(): void{
        let sessionUser=this.authService.admin();
        this.userPerms=this.authService.getUserPerms(sessionUser.activityPermission, this.APP, this.ACTIVITY, ['LIST','EDIT','VIEW','DELETE','DUPLICATE','ENROLLED_STUDENT']);
        let filter:any={};
        if(this.filter){
            filter=this.filter;
            this.activeFilter=filter;
        }

        if(this.store){
            filter.store=this.store;
        }
        this.loadProducts(filter);
        this.filterRowForm=this.fb.group({
            title:[""],
            stream:[null],
            type:[null],
            cost:[""],
            status:[filter.status||null]
        });
        this.duplicateProductForm = this.fb.group({
            session: [null],
            mrp: [""],
            cost: [""],
            status: [false]
        });
        this.loadStreams();
        this.loadProductTypes();

    }

    loadStreams(){
        this.coptionService.getCoption({option:'STREAM'}).subscribe(
            res=>{
                this.streams=res;
            },
            err=>{
                this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
            }
        );
    }

    loadProductTypes(){
        let filter: any = { option: 'PRODUCTTYPE'};
        if (this.activeFilter && this.activeFilter.productCategory ) {
            filter.parentOption = this.activeFilter.productCategory ;
        }
        this.coptionService.getCoption(filter).subscribe(
            res=>{
                this.productTypes=res;
            },
            err=>{
                this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
            }
        );
    }

    loadProducts(filter){
        this.panelLoader="Processing";
        this.productService.getProduct(filter, true).subscribe(
            (res)=>{
                this.rows=res.body;
                this.totalRecords = res.headers.get('totalRecords')||0;
                this.panelLoader="none";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000);
                this.panelLoader="none";

            }
        );
    }

    loadProductsWithRowFilter(){
        let fltr=this.filterRowForm.value;
        if (!this.activeFilter) {
            this.activeFilter={};
        }
        if (fltr&&fltr.type!=null) {
            this.activeFilter.type=fltr.type;
        }
        if (fltr && fltr.title != null && fltr.title!='') {
            this.activeFilter.title = fltr.title;
        }
        if (fltr && fltr.stream != null) {
            this.activeFilter.stream = fltr.stream;
        }
        if (fltr && fltr.cost != null && fltr.cost!= '') {
            this.activeFilter.cost = fltr.cost;
        }
        if (fltr && fltr.status != null&&fltr.status!='') {
            this.activeFilter.status = fltr.status;
        }
        this.countBase=1;
        this.loadProducts(this.activeFilter);
    }
    loadSessions(){
        this.sessionService.getSession().subscribe(
            (res)=>{
                this.sessions=res;
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }

    deleteProduct(product){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete "+product.name+"?");
        if(!confirm){
            this.panelLoader = "none";
            return;
        }
        this.productService.deleteProduct(product.id).subscribe(
            (res)=>{
                this.rows.splice(this.rows.indexOf(product), 1);
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );

            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    loadFreeProducts(){
        this.activeFilter.customParam = {"freeProduct" : "testSeries"}
        this.loadProducts(this.activeFilter);
    }
    paginate(e){
        if(!this.activeFilter){
            this.activeFilter={};
        }
        this.countBase=e.rows*e.page+1;
        this.activeFilter.page=(e.page+1);
        this.activeFilter.limit=e.rows;
        this.loadProducts(this.activeFilter);
    }
    showDupFormOption(productIndex){
        this.selectedProduct = this.rows[productIndex];
        this.showDupForm=true;
        this.loadSessions();
    }

    createDuplicateProduct(){
        if(!this.duplicateProductForm.valid){
            return
        };
        let confirm = window.confirm("Are You Sure to create Duplicate product of " + this.selectedProduct.title);
        if (!confirm) return;
        this.formStatus='Processing';
        console.log(this.duplicateProductForm.value);
        let dupProductData:any={};
        dupProductData = this.duplicateProductForm.value;
        dupProductData.oldProduct=this.selectedProduct.id;
        // return;
        this.productService.createDupProduct(dupProductData).subscribe(
            (res)=>{
                this.loadProducts(this.activeFilter);
                this.duplicateProductForm.reset();
                this.showDupForm = false;

                this.selectedProduct={};
                this.formStatus = '';

            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );

    }
    updateProductStatus(event, productIndex){
        if (!event.target) return;
        let status = event.target.checked;
        this.productService.updateProduct(this.rows[productIndex].id, {status:status}).subscribe(
            (res)=>{
                this.rows[productIndex].status = status;
                this.notifier.alert("Success", "Successfully Updated!!", 'success', 500);
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );


    }
}
