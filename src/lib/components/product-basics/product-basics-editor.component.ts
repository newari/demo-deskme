import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Product } from '../../models/product.model';
import { Coption } from '../../models/coption.model';
import { ProductService } from '../../services/product.service';
import { CoptionService } from '../../services/coption.service';
import { NotifierService } from '../notifier/notifier.service';
import { FirmService } from '../../services/firm.service';
import { Firm } from '../../models/firm.model';
import { CenterService } from '../../services/center.service';
import { StoreService } from '../../services/store.service';
import { Store } from '../../models/store.model';
import { Center } from '../../models/center.model';


@Component({
    selector:'ek-product-basics-editor-component',
    templateUrl:'./product-basics-editor.component.html'
})
export class ProductBasicsEditorComponent implements OnInit{
    productForm:FormGroup;
	product:Product;
	@Input() productId;
	formStatus="Normal";
	panelLoader="none";
	frmLoader="none";

	courses:Coption[];
	productCategories:Coption[];
	productTypes:Coption[];
	streams:Coption[];
	products:Product[];
	centers:Center[];
	availableStores:Store[];
	firms:Firm[];

	cmbPanelLoader:string="none";
	addedProducts:any[]=[];
	constructor(
		private fb:FormBuilder,
		private productService:ProductService,
		private firmService:FirmService,
		private coptionService:CoptionService,
		private storeService:StoreService,
		private centerService:CenterService,
		private notifier: NotifierService){ 
			
	}

	getProduct(productId){
		this.panelLoader="show";
		this.productService.getOneProduct(productId).subscribe(
			(res)=>{
				let product=res;
				this.product=res;
				if(res.category&&res.category.id){
					product.category=res.category.id;
				}
				if(res.stream&&res.stream.id){
					product.stream=res.stream.id;
				}
				if(res.course&&res.course.id){
					product.course=res.course.id;
				}
				
				if(res.type&&res.type.id){
					product.type=res.type.id;
				}
				if(res.store&&res.store.id){
					product.store=res.store.id;
				}
				if(res.needStdReg&&res.stdRegInsty){
					res.stdRegInsty=res.stdRegInsty.id||res.stdRegInsty;
					this.loadFirms();
				}else{
					res.needStdReg=false;
				}
				if(res.centerDependent){
					res.center=res.center.id;
					this.loadCenters();
				}
				
				this.productForm.patchValue(product);
				this.loadStores();
				
				this.panelLoader="none";
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		)
	}

	loadCourses(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'COURSE'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.courses=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}

	loadProductCategories(){
		this.frmLoader="show";
		this.coptionService.getCoptionProductCategory({option:'PRODUCTCATEGORY'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.productCategories=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadProductTypes(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'PRODUCTTYPE'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.productTypes=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
	}

	loadCenters(){
		
		if(this.productForm.value.centerDependent==false){
			return
		}
		this.centerService.getCenter().subscribe(
			res=>{
				this.centers=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
		
	}
	loadStores(){
		this.storeService.getStore().subscribe(
			res=>{
				this.availableStores=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger");
			}
		)
	}

	loadFirms(){
		console.log(this.productForm.value.needStdReg);
		if(this.productForm.value.needStdReg=='false'){
			return
		}
		this.firmService.getFirm().subscribe(
			res=>{
				this.firms=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
		
	}

	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
	}
	
	updateProduct(): void {
		
		this.product=this.productForm.value;
		this.formStatus="Processing";
		this.productService.updateProduct(this.productId, this.product).subscribe(
			(res)=>{
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 500 );
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.productForm=this.fb.group({
			course:['', Validators.required],
			stream:['', Validators.required],
			category:['', Validators.required],
			type:['', Validators.required], 
			mrp:['', Validators.required],
			cost:['', Validators.required],
			storeFronts:[[]],
			store:[[]],
			shipping:[false],
			needStdReg:[null],
			stdRegInsty:[null],
			centerDependent:[null],
			center:[null],
			status:[true, Validators.required],
			discount:this.fb.group({
				currentStudentDiscount:[0],
				exStudentDiscount:[0],
				fullPaymentDiscount:[0],
			})
		});
		this.getProduct(this.productId);
		this.loadCourses();
		this.loadProductCategories();
		this.loadProductTypes();
		this.loadStreams();
	}

	setProductType(){
		 
		this.cmbPanelLoader="show";
		this.productService.getProduct().subscribe(
			res=>{
				this.products=res;
				this.cmbPanelLoader="none";
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger");
				this.cmbPanelLoader="none";
			}
		)
	}
	linkStoreFront(storeCode){
		if(!this.product.storeFronts){
			this.product.storeFronts=[storeCode];
			
		}else if(this.product.storeFronts&&this.product.storeFronts.indexOf(storeCode)==-1){
			this.product.storeFronts.push(storeCode);
		}else{
			this.product.storeFronts.splice(this.product.storeFronts.indexOf(storeCode), 1);
		}
		this.productForm.value.storeFronts=this.product.storeFronts;
	}
}
