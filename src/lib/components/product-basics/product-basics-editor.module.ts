import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FileinputModule } from '../filemanager/fileinput.module';
import { ProductService } from '../../services/product.service';
import { ProductBasicsEditorComponent } from './product-basics-editor.component';
import { StoreService } from '../../services/store.service';
import { CenterService } from '../../services/center.service';
import { FirmService } from '../../services/firm.service';

@NgModule({
    declarations:[ProductBasicsEditorComponent],
    imports:[FileinputModule, RouterModule, FormsModule, ReactiveFormsModule, CommonModule],
    providers:[ProductService, StoreService, CenterService, FirmService],
    exports:[ProductBasicsEditorComponent]
})
export class ProductBasicsEditorModule {
    
 }
