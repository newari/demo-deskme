import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from '../../services/product.service';
import { NotifierService } from '../notifier/notifier.service';



@Component({
    selector:'ek-product-stock-add-form',
    templateUrl:'./product-stock-add.component.html'
})
export class ProductStockAddComponent implements OnInit{
    @Input() productId:string;

    @Output('onStockAdded') onStockAdded:EventEmitter<any>=new EventEmitter();

    stockForm:FormGroup;
    stockFormStatus="Normal";

    constructor(
        private productService:ProductService,
        private fb:FormBuilder,
        private notifier: NotifierService){}

    ngOnInit(){
        this.stockForm=this.fb.group({
            accountingStock:[0, Validators.required],
            physicalStock:[0, Validators.required]
        })
    }

    addStock(){
        let data=this.stockForm.value;
        data.date=new Date();
        this.stockFormStatus="Processing";
        this.productService.addStock(this.productId, data).subscribe(
            stock=>{

                this.stockFormStatus="Normal";
                this.stockForm.reset();
                this.onStockAdded.emit(stock);
            },
            err=>{
                this.stockFormStatus="Normal";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
}
