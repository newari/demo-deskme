import { NgModule } from '@angular/core';
import { ProductStockAddComponent } from './product-stock-add.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductService } from '../../services/product.service';

@NgModule({
    declarations:[ProductStockAddComponent],
    exports:[ProductStockAddComponent],
    providers:[ProductService],
    imports:[CommonModule, FormsModule, ReactiveFormsModule]
})
export class ProductStockAddModule{
    
}