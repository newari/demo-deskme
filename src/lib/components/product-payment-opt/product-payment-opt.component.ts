import { Component, OnInit, Input} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ProductService } from '../../services/product.service';
import { NotifierService } from '../notifier/notifier.service';
import { Store } from '../../models/store.model';
import { StoreService } from '../../services/store.service';
import { ProductPaymentOption } from '../../models/payment-option.model';

@Component({
    selector:'ek-product-payment-opt',
    templateUrl:'./product-payment-opt.component.html'
})
export class ProductPaymentOptComponent implements OnInit{
    @Input() productId:string;
    @Input() stores:Store[];
    paymentOpts:ProductPaymentOption[];
    optForm:FormGroup;
    optFormStatus="Normal";
    panelLoader:string="Normal";
    displayForm:boolean=false;
    selectedOption: ProductPaymentOption;
    displayEditForm: boolean;
    constructor(
        private productService:ProductService,
        private fb:FormBuilder,
        private storeService:StoreService,
        private notifier: NotifierService){}
        
    ngOnInit(){
        this.optForm=this.fb.group({
            title:["", Validators.required],
            amount:[0, Validators.required],
            extraAmount:[0],
            storeId:[null, Validators.required],
            billingDate:[null, Validators.required],
            dueDate:[null, Validators.required],
            invoiceType:[null],
            linkedProduct:[''],
            applyDiscount:[false]
        });
        this.loadStores(); 
        this.loadPaymentOpts(); 
    }

    loadStores(){
        if(this.stores){
            return;
        }
        this.storeService.getStore().subscribe(
            res=>{
                this.stores=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 2000);
            }
        )
    }

    loadPaymentOpts(){
        this.panelLoader="show";
        this.productService.getProductPaymentOptions(this.productId).subscribe(
            res=>{
                this.panelLoader="none";
                this.paymentOpts=res;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 2000);
            }
        )
    }

    addPaymentOpt(){
        let data=this.optForm.value;
        data.product=this.productId;
        this.optFormStatus="Processing";
        this.productService.addProductPaymentOption(this.productId, data).subscribe(
            res=>{
                this.paymentOpts.push(res);
                this.optFormStatus="Normal";
                this.optForm.reset();
            },
            err=>{
                this.optFormStatus="Normal";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
    removeOpt(i){
        let conf=confirm("Are you sure to remove this option?");
        if(!conf){
            return;
        }
        this.panelLoader="show";
        this.productService.deleteProductPaymentOption(this.productId, this.paymentOpts[i].id).subscribe(
            res=>{
                this.panelLoader="none";
                this.paymentOpts.splice(i, 1);
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 2000);
            }
        )
    }

    showEditForm(i){
        this.displayEditForm=true;
        this.selectedOption=this.paymentOpts[i];
        this.optForm.patchValue( this.selectedOption);

    }
    updatePaymentOpt(){
        if(!this.optForm.dirty) return;

        this.productService.updateProductPaymentOpt(this.productId,this.selectedOption.id, this.optForm.value).subscribe(
            res=>{
                
                this.optForm.reset();
                this.displayEditForm=false;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000)
            }
        );
    }
}