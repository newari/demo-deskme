import { NgModule } from '@angular/core';
import { ProductPaymentOptComponent } from './product-payment-opt.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductService } from '../../services/product.service';

@NgModule({
    declarations:[ProductPaymentOptComponent],
    exports:[ProductPaymentOptComponent],
    providers:[ProductService],
    imports:[CommonModule, FormsModule, ReactiveFormsModule]
})
export class ProductPaymentOptModule{
    
}