import { NgModule } from '@angular/core';
import { UserSearchFormComponent } from './user-search-form.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FileinputModule } from '../filemanager/fileinput.module';
import { TabViewModule } from 'primeng/tabview';
@NgModule({
    declarations:[UserSearchFormComponent],
    imports:[FileinputModule, TabViewModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[UserSearchFormComponent]
})
export class UserSearchFormModule{

}
