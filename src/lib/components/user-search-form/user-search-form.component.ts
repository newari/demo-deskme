import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { NotifierService } from '../notifier/notifier.service';
import { FormControl } from '@angular/forms';
import { Subject } from 'rxjs';
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import { Observable } from "rxjs/Observable";
@Component({
    selector:'ek-user-search-form',
    templateUrl:'./user-search-form.component.html'
})
export class UserSearchFormComponent implements OnInit{

    userData:User;
    users:any[]=[];
    selectedUser:any;
    query:string;
    searchTextChanged = new Subject<string>();
    req:any;
    @Output() onUserSelect:EventEmitter<any>=new EventEmitter<any>();
    @Output() onSearchClear:EventEmitter<any>=new EventEmitter<any>();
    @Input() email: string;
    @Input() type:string;
    @Input() field:string="mobile";
    constructor(

		private userService:UserService,
		private notifier: NotifierService
		){

    }
    ngOnInit(): void {
		if (this.email) {
            this.hello(this.email);
        }
        this.searchTextChanged
        .debounceTime(500)
        .distinctUntilChanged()
        .subscribe((val) =>{
            console.log(val)
            this.searchUser(val);
        });
    }

    searchUser(term){
        if(this.req){
            console.log(term)
            this.req.unsubscribe();
        }
        this.req=this.userService.searchUser({ query: term, field:this.field, type: this.type ? this.type:null}).subscribe(
            res=>{
                this.users=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
    selectUser(i){
        this.selectedUser=this.users[i];
        this.onUserSelect.emit(this.users[i]);
        this.users=[];
    }
    hello(email?:any){
        this.userService.searchUser({ query: email }).subscribe(
            res => {
                this.users = res;
            },
            err => {
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
    search($event) {
        this.searchTextChanged.next($event.target.value);
    }
    clearSearch(){
        this.query="";
        this.onSearchClear.emit(this.selectedUser);
        this.selectedUser=null;
    }
}
