import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CnvertEnquiryFormComponent } from './convert-enquiry.component';
import { FileinputModule } from '../filemanager/fileinput.module';
import { EmployeeService } from '../../services/employee.service';
import { EnquiryTypeService } from '../../services/enquirytype.service';
import { AuthService } from '../../services/auth.service';
import { CalendarModule, DialogModule } from 'primeng/primeng';
import { CommonModule } from '@angular/common';
import { EnquiryService } from '../../services/enquiry.service';

@NgModule({
    declarations: [CnvertEnquiryFormComponent],
    imports: [FileinputModule,CalendarModule, DialogModule,CommonModule, FormsModule, ReactiveFormsModule],
    exports: [CnvertEnquiryFormComponent],
    providers: [EnquiryService,  EmployeeService,AuthService, EnquiryTypeService]
})
export class ConvertEnquiryFormModule {
}
