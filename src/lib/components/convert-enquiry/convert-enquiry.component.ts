import { Component, EventEmitter, Output, Input, OnInit, AfterViewInit, AfterContentChecked, AfterContentInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EnquiryService } from '../../services/enquiry.service';
import { NotifierService } from '../notifier/notifier.service';
import { Center } from '../../models/center.model';
import { Order } from '../../models/order.model';
import { EmployeeService } from '../../services/employee.service';
import { Employee } from '../../models/employee.model';
import { AuthService } from '../../services/auth.service';
import { EnquiryTypeService } from '../../services/enquirytype.service';
import { Enquiry } from '../../models/enquiry.model';
@Component({
    selector: 'ek-convert-enquiry-form',
    templateUrl: './convert-enquiry.component.html'
})
export class CnvertEnquiryFormComponent implements OnInit, AfterContentInit {
    ngAfterContentInit(): void {
        if (this.order) {
            this.loadEnquiryTypes();
            this.loadEmployees();
        }
    }
    
    convertEnquiryForm: FormGroup;
    enquiryData: Enquiry;
    formStatus = "Normal";
    @Input() order:any
    @Output() onSuccess: EventEmitter<any> = new EventEmitter<any>();
    employees: Employee[];
    convertEnquiryModal: boolean;
    enquiryTypes: any;

    constructor(
        private fb: FormBuilder,
        private enquiryService: EnquiryService,
        private notifier: NotifierService,
        private employeeService:EmployeeService,
        private authService:AuthService,
        private enquiryTypeService:EnquiryTypeService
    ) { }

    convertToEnquiry(): void {
        this.formStatus = "Processing";
        this.enquiryData = this.convertEnquiryForm.value;
        this.enquiryService.addEnquiry(this.enquiryData).subscribe(
            res => {
                this.onSuccess.emit({ event: 'ItemAdded', item: res });
                this.convertEnquiryForm.reset();
                this.formStatus = "Normal";
                this.notifier.alert('Success', 'Added Successfully', 'success', 5000);
            },
            err => {
                this.notifier.alert(err.code, err.message, 'danger', 5000);
                this.formStatus = "Normal";
            }
        );
    }
    ngOnInit(): void {
        this.convertEnquiryForm = this.fb.group({
            enquiryType: ['', Validators.required],
            nextReminderMessage: [''],
            nextReminderTime: [''],
            assignedTo:[''],
        });
    }
    loadEmployees(filter?: any) {
        if (!filter) {
            filter = {};
        }
        filter["activityPermission"] = 'ACADEMICS.ENQUIRY.LIST';
        this.employeeService.getEmployeeByAppAccess(filter).subscribe(
            (data) => {
                this.employees = data;
            },
            (err) => {
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    } 
    convertEnquiry() {
        this.formStatus = "Processing";
        if (!this.convertEnquiryForm.valid) {
            this.formStatus = "None";
            return;
        }
        let admin = this.authService.admin();
        let enquiry: any = {
            firstName: this.order.user.firstName,
            lastName: this.order.user.lastName,
            email: this.order.user.email,
            mobile: this.order.user.mobile,
            message: "Interested to buy " + this.order.products[0].title,
            source: "from Student Order No. " + this.order.orderNo + " Record",
            course: this.order.relCourse.id,
            enquiryType: this.convertEnquiryForm.value.enquiryType,
            nextReminderMessage: this.convertEnquiryForm.value.nextReminderMessage,
            nextReminderTime: new Date(this.convertEnquiryForm.value.nextReminderTime),
            status: true,
            stream: this.order.relStream.id,
            currentStatus: "Interested",
            createdBy: admin.id,
            assignedTo: this.convertEnquiryForm.value.assignedTo,
            assignedAt: new Date(),
            assignedBy: admin.id
        } 

        this.enquiryService.addEnquiry(enquiry).subscribe(
            (res) => {
                this.formStatus = "None";
                this.onSuccess.emit({ event: 'ItemAdded', item: res });
                this.convertEnquiryForm.reset();
                this.convertEnquiryModal = false;
                this.notifier.alert("Success", "Successfully Converted!!", 'success', 500)
            },
            (err) => {
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }

    loadEnquiryTypes() {
        this.enquiryTypeService.getEnquiryType({ limit: 50 }).subscribe(
            (res) => {
                this.notifier.alert("Success", "Successfully Loaded!!", 'success', 500)
                this.enquiryTypes = res;
            },
            (err) => {
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
}
