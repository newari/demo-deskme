import { NgModule } from '@angular/core';
import { ProductStockHistoryComponent } from './product-stock-history.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductService } from '../../services/product.service';

@NgModule({
    declarations:[ProductStockHistoryComponent],
    exports:[ProductStockHistoryComponent],
    providers:[ProductService],
    imports:[CommonModule]
})
export class ProductStockHistoryModule{
    
}