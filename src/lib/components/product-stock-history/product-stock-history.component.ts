import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { NotifierService } from '../notifier/notifier.service';

@Component({
    selector:'ek-product-stock-history',
    templateUrl:'./product-stock-history.component.html'
})
export class ProductStockHistoryComponent implements OnInit{
    @Input() productId:string;
    stocks:any[];
    panelLoader:string="none";
    
    constructor(
        private productService:ProductService,
        private notifier: NotifierService){}
        
    ngOnInit(){
        this.loadStockHistory();
    }

    loadStockHistory(){
        this.panelLoader="show";
        this.productService.getStockHistory(this.productId).subscribe(
            data=>{
                this.stocks=data;
                this.panelLoader="none";
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
}