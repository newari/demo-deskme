import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { PaymentCollectionDashbox } from './pcdb.component';
import { PaymentService } from '../../services/payment.service';

@NgModule({
    declarations:[PaymentCollectionDashbox],
    exports:[PaymentCollectionDashbox],
    providers:[PaymentService],
    imports:[CommonModule, FormsModule,
        ReactiveFormsModule]
})
export class PaymentCollectionDashboxModule {
    
}
