import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Angular2Csv } from 'angular2-csv';
import { PaymentService } from '../../services/payment.service';
import { NotifierService } from '../notifier/notifier.service';
import { StoreService } from '../../services/store.service';
import { CenterService } from '../../services/center.service';
import { Store } from '../../models/store.model';
import { Center } from '../../models/center.model';

@Component({
    selector:'ek-payment-collection-dashbox',
    templateUrl:'./pcdb.component.html'
})
export class PaymentCollectionDashbox implements OnInit{ 
    payments:any[];
    total:number;
    panelLoader="none";
    filterForm:FormGroup;
    filterFormState:boolean=false;
    stores:Store[];
    centers:Center[];
    constructor(
        private paymentService:PaymentService,
        private notifier: NotifierService,
        private centerService:CenterService,
        private storeService:StoreService,
		private fb: FormBuilder
    ){}

    ngOnInit(): void{
        let filter:any={};
       
        this.filterForm=this.fb.group({
            approvedDateFrom:[""],
            approvedDateTo:[""],
            store:[null],
            center:[null]
        });
        this.loadPayments(filter);
        
    }

    loadPayments(filter?:any):void{
        this.panelLoader="show";
        if(!filter){
            filter={};
        }
        this.paymentService.getPaymentCollectionByMode(filter).subscribe(
            (data)=>{
                let ttl=0;
                for(let i=0; i<data.length; i++){
                    ttl+=(data[i].extraCollection+data[i].totalCollection);
                }
                this.payments=data;
                this.total=ttl;
                this.panelLoader="none";
                this.filterFormState=false;
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000);
                this.panelLoader="none";
            }
        );
        
    }

    filterPayments(){
        let data=this.filterForm.value;
        this.loadPayments(data);
    }

    displayFilterForm(){
        if(!this.stores){
            this.loadStores();
        }
        if(!this.centers){
            this.loadCenters();
        }
        this.filterFormState=true;
    }

    loadStores(){
        this.storeService.getStore({status:true}).subscribe(
            res=>{
                this.stores=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000);
            }
        )
    }
    loadCenters(){
        this.centerService.getCenter({status:true}).subscribe(
            res=>{
                this.centers=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000);
            }
        )
    }
    exportRecords(){
        var data=[];
        this.payments.forEach(function(pmt){
            data.push({
                "Mode":pmt._id,
                "Total Collection":pmt.totalCollection,
                "Extra Collection":pmt.extraCollection,
                "Overall Collection":(pmt.totalCollection+pmt.extraCollection),
            })
        })    

        new Angular2Csv(data, 'Payment Mode wise Collection Report', { 
                fieldSeparator: ',',
                quoteStrings: '"',
                decimalseparator: '.',
                showLabels: true, 
                showTitle: true 
              });
    }
}