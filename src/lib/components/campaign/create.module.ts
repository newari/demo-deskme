import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { QuillEditorModule } from '../ngx-quill-editor/quillEditor.module';
import { FileinputModule } from '../filemanager/fileinput.module';
import { ContactService } from '../../services/contact.service';
import { CampaignService } from '../../services/campaign.service';
import { CampaignCreateComponent } from './create.component';
import { NotificationTemplateService } from '../../services/notification-template.service';
import { RouterModule } from '@angular/router';


@NgModule({
    declarations:[CampaignCreateComponent],
    imports: [ CommonModule, FormsModule, ReactiveFormsModule, QuillEditorModule, FileinputModule, RouterModule],
    providers:[ContactService, CampaignService, NotificationTemplateService],
    exports:[CampaignCreateComponent]
})
export class CampaignCreateModule { }
