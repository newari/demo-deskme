import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Campaign } from '../../models/campaign.model';
import { CampaignService } from '../../services/campaign.service';
import { NotifierService } from '../notifier/notifier.service';
import { NotificationTemplateService } from '../../services/notification-template.service';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { ContactService } from '../../services/contact.service';

@Component({
    selector:'ek-new-campaign',
    templateUrl:'./create.component.html'
})
export class CampaignCreateComponent implements OnInit{
    campaignForm:FormGroup;
	campaignData:Campaign;
	formStatus="Normal";
	frmLoader="none";
	@Input() campaignPreData:any;
	@Input() campaignType:string;
	@Input() editCampaignType:boolean=true;
	selectedTemplate:any;
    templates: any;
    activeStep:number=1;
    createdCampaign:any;
    contactFilter:string=null;
    contactFilterType:string=null;
    contactFilterTypeVal:string=null;
    cfItems:any[]; //contact filter items
    selectedFilterItem:string;
    contactLoader:string;
    contactCount:number=0;
    campaignSent:any;
    additionalParamsOpt:boolean;
    keyValSets:any[]=[];
    testFcmToken:string;
    testPushModal:string="hide";
    @Input() inappData:any;
	constructor(
		private fb:FormBuilder,
		private campaignService:CampaignService,
		private notifier: NotifierService,
        private templateService: NotificationTemplateService,
        private sanitizer:DomSanitizer,
        private contactService: ContactService
	){

    }

	ngOnInit(): void {
		this.campaignForm=this.fb.group({
			title:['', Validators.required],
			type:[this.campaignType, Validators.required],
			thumb:['',],
			source:['',],
			sourceId:['',],
			params:[[]],
			defaultRecipient:[''],
			status:[true],
			payLoad:this.fb.group({
				key:['type'],
				value:['null']
			}),
			template:this.fb.group({
				fromname:[''],
				fromemail:[''],
				name:[''],
            }),
            msg:this.fb.group({
				contentType:[null],  //for notice board:[html,link,pdf,img,video,inapp]
				heading:[''],
				html:[''],
				text:[''],
				link:[''],
				pdf:[''],
				image:[''],
				video:[''],
				inapp:['']

			})

        });

        if(this.campaignPreData){
            this.campaignForm.patchValue(this.campaignPreData)
        }
        if(this.campaignType=="ANDROIDPUSH"){
            this.campaignForm.controls['msg'].patchValue({contentType:"text"});
        }
    }


    setCampaignType(type){
        this.campaignType=type;
        this.additionalParamsOpt=false;
        if(this.campaignType=='EMAILPUSH'){
            this.loadTemplates();
        }

        if(this.campaignType=='ANDROIDPUSH'){
            this.campaignForm.controls['msg'].patchValue({contentType:"text"});
            this.additionalParamsOpt=true;
        }
    }
    removeKeyValSet(i){
        this.keyValSets.splice(i, 1);
    }

    nextStep2(){
        if(this.keyValSets.length>0){
            let params={};
            for(let i=0; i<this.keyValSets.length; i++){
                params[this.keyValSets[i].key]=this.keyValSets[i].val;
            }
            this.campaignForm.controls['msg'].patchValue({inapp:{actionData:params}});
        }
        console.log(this.campaignForm.value);
        this.activeStep=2;
    }

    nextStep3(){
        this.activeStep=3;

    }
    directToStep(stepNo){
        if(stepNo<this.activeStep&&!this.createdCampaign){
            this.activeStep=stepNo;
        }
        return;
    }
    resetCampaign(){
        this.keyValSets=[];
        this.campaignForm.reset();

        if(this.campaignType){
            this.campaignForm.patchValue({type:this.campaignType})
        }
        this.activeStep=1;
        this.createdCampaign=null;
        this.contactFilter=null;
        this.campaignSent=false;
    }
    cleanURL(oldURL ): SafeUrl {
        return   this.sanitizer.bypassSecurityTrustResourceUrl(oldURL);
    }
    setContactFilter(e){
        this.contactFilter=e.target.value;
        if(this.contactFilter=="all"){
            this.getContactCount(null, true);
        }
    }
    setContactFilterType(e){
        this.contactFilterType=e.target.value;
        if(this.contactFilterType=="product"){
            this.contactLoader="show"
            this.contactService.getProduct({limit:200}).subscribe(
                res=>{
                    this.cfItems=res;
                    this.contactLoader="none";
                },
                err=>{
                    this.notifier.alert(err.code, err.message, 'danger', 5000 );
				    this.contactLoader="none";
                }
            )
        }
    }
    getContactCount(e:any, forAll?:boolean){
        let filter:any={};
        if(!forAll) {
             if(!e.target.value){
                this.contactFilterTypeVal=null;
                return;
            }

            if(this.contactFilterType=="product"){
                filter.product=e.target.value;
                this.contactFilterTypeVal=e.target.value;
            }
        }

        this.contactLoader="show";
        this.contactService.getContactCount(filter).subscribe(
            res=>{
                this.contactCount=res;
                this.contactLoader="none";
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.contactLoader="none";
            }
        )
    }

    addCampaign(): void {
        if(this.formStatus=="Processing"){
            return;
        }
        let data:any=this.campaignForm.value;
        if(this.inappData){
            data.msg.inapp=this.inappData;
        }else if(data.msg&&data.msg.inapp&&data.msg.inapp.actionData){
            if(data.msg.inapp.actionData.page){
                data.msg.inapp.page=data.msg.inapp.actionData.page;
                delete data.msg.inapp.actionData.page;
            }
            if(data.msg.inapp.actionData.openDirect){
                data.msg.inapp.openDirect=Boolean(data.msg.inapp.actionData.openDirect);
                delete data.msg.inapp.actionData.openDirect;
            }

        }

        data.contactFilter={};
        if(this.contactFilter=="all"){
            data.contactFilter.all=true;
        }else if(this.contactFilter=="filter"){
            if(!this.contactFilterType||!this.contactFilterTypeVal){
                this.notifier.alert("Error", "Please select contacts first!", 'danger', 1000 );
                return;
            }
            data.contactFilter[this.contactFilterType]=this.contactFilterTypeVal;
        }else{
            this.notifier.alert("Error", "Please select contacts first!!", 'danger', 1000 );
            return;
        }
        data.totalContacts=this.contactCount;
        this.formStatus="Processing";

		this.campaignService.addCampaign(data).subscribe(
			res=>{
                this.createdCampaign=res;
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	loadTemplates(){
		this.templateService.getNotificationTemplate({limit:'all', status:true}).subscribe(
			res=>{
				this.templates= res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		)
	}

	setTemplate(index){
		this.selectedTemplate = this.templates[index];
		this.campaignForm.patchValue({subject:this.selectedTemplate.subject, contentHtml:this.selectedTemplate.message, params:this.selectedTemplate.params});
    }
    setNoticeBoardTargetContent(target){

    }

    sendCampaign(){
        if(!this.createdCampaign){
            return;
        }
        if(!this.contactFilter){
            this.notifier.alert("Failed", "Please select contacts first", "warning", 2000)
            return;
        }
        if(this.formStatus=="Processing"){
            return;
        }
        let contacts:any;
        if(this.contactFilter=="all"){
            contacts="all";
        }else if(this.contactFilter=="filter"){
            contacts={};
            contacts["meta."+this.contactFilterType+"_"+this.contactFilterTypeVal]=true
        }
        if(this.createdCampaign.type=="NOTICEBOARD"){
            this.sendNoticeCampaign(contacts);
        }else if(this.createdCampaign.type=="ANDROIDPUSH"){
            this.sendAndroidPushCampaign(contacts);
        }

    }
    sendNoticeCampaign(contacts){
        this.formStatus="Processing";
        this.campaignService.sendNoticeBoardCampaign({contacts:contacts, campaign:this.createdCampaign.id}).subscribe(
            res=>{
                this.campaignSent=res;
                this.formStatus="Normal";
                this.notifier.alert('Done!', 'Notification sent successfully.', 'success', 2000 );
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.formStatus="Normal";
            }
        )
    }
    sendAndroidPushCampaign(contacts){
        this.formStatus="Processing";
        this.campaignService.sendAndroidPushCampaign({contacts:contacts, campaign:this.createdCampaign.id}).subscribe(
            res=>{
                this.campaignSent=res;
                this.formStatus="Normal";
                this.notifier.alert('Done!', 'Notification sent successfully.', 'success', 2000 );
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.formStatus="Normal";
            }
        )
    }
    sendTestAndroidPushNotification(){
        if(this.campaignType!="ANDROIDPUSH"){
            return;
        }
        if(!this.testFcmToken||this.testFcmToken.trim()==""){
            return;
        }
        let formData:any=this.campaignForm.value;
        let data:any={
            title:formData.msg.heading,
            message:formData.msg.text,
            fcmToken:this.testFcmToken
        }
        if(formData.thumb){
            data.image=formData.thumb;
        }
        let additionalData:any={};
        if(this.inappData){
            additionalData=this.inappData;
        }else if(formData.msg&&formData.msg.inapp&&formData.msg.inapp.actionData){
            additionalData.actionData=formData.msg.inapp.actionData;
            if(formData.msg.inapp.actionData.page){
                additionalData.page=formData.msg.inapp.actionData.page;
            }
            if(formData.msg.inapp.actionData.openDirect){
                additionalData.openDirect=formData.msg.inapp.actionData.openDirect;
            }

        }
        data.additionalData=additionalData;
        this.formStatus="Processing";
        this.campaignService.testAndroidPushNotification(data).subscribe(
            res=>{
                this.testFcmToken="";
                this.formStatus="Normal";
                this.notifier.alert('Done!', 'Test Notification sent successfully.', 'success', 2000 );
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.formStatus="Normal";
            }
        )

    }
}
