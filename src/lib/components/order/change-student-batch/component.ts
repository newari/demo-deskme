import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms/src/model';
import { Product } from '../../../models/product.model';
import { OrderService } from '../../../services/order.service';
import { ProductService } from '../../../services/product.service';
import { NotifierService } from '../../notifier/notifier.service';

@Component({
    selector:'ek-order-change-student-batch',
    templateUrl:'./component.html'
})
export class ChangeOrderStudentBatch implements OnInit{
    batchForm:FormGroup;
    product:Product;
    formStatus:string="Normal";
    @Input() orderId:string;
    @Input() batchId:string;
    @Input() productId:string;
    constructor(
        private fb:FormBuilder,
        private orderService:OrderService,
        private productService:ProductService,
        private notifier: NotifierService){}
    
    ngOnInit(){
        this.batchForm=this.fb.group({
            order:[this.orderId, Validators.required],
            product:[this.productId, Validators.required],
            batch:[this.batchId, Validators.required],
            updateStd:[true]
        });
        let self=this;
        setTimeout(function(){
            self.productService.getOneProduct(self.productId).subscribe(
                res=>{
                    self.product=res;
                    self.batchForm.patchValue({batch:self.batchId, product:self.productId, order:self.orderId});
                },
                err=>{
                    self.notifier.alert(err.code, err.message, "danger", 10000);
                }
            )
        }, 0);
        
    }
    changeBatch(){
        let frmData=this.batchForm.value;
        if(frmData.batch==this.batchId){
            return;
        }
        if(!this.batchForm.valid||frmData.batch=="null"){
            this.notifier.alert("Error", "Please fill all the fields!", "danger", 10000);
            return;
        }
        this.formStatus="Processing";
        this.orderService.changeOrderBatch(frmData).subscribe(
            res=>{
                this.formStatus="Normal";
                this.notifier.alert('Success', "Changed successfully", "success", 10000);
            },
            err=>{
                this.formStatus="Normal";
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )
    }
}