import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChangeOrderStudentBatch } from './component';
import { OrderService } from '../../../services/order.service';
import { ProductService } from '../../../services/product.service';



@NgModule({
    declarations:[ChangeOrderStudentBatch],
    exports:[ChangeOrderStudentBatch],
    providers:[OrderService, ProductService],
    imports:[CommonModule, FormsModule, ReactiveFormsModule, RouterModule]
})
export class ChangeOrderStudentBatchModule {
    
}
