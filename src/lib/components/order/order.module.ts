import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { TabViewModule, DialogModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OrderComponent } from './order.component';
import { OrderProcessStepsModule } from '../order-process-steps/module';
import { OrderPaymentModule } from '../order-payment/order-payment.module';
import { StudentInfoModule } from '../student/info/info.module';
import { ChangeOrderStudentBatchModule } from './change-student-batch/module';
import { OrderService } from '../../services/order.service';
import { EkDropdownModule } from '../dropdown/dropdown.module';
import { OrderPayment2Module } from '../order-payment-2/order-payment-2.module';


@NgModule({
    declarations:[OrderComponent],
    exports:[OrderComponent],
    providers:[OrderService],
    imports:[CommonModule,DialogModule,EkDropdownModule, ChangeOrderStudentBatchModule,  TabViewModule, OrderPaymentModule, OrderPayment2Module, StudentInfoModule, FormsModule, OrderProcessStepsModule, ReactiveFormsModule, RouterModule]
})
export class OrderModule {
    
 }
