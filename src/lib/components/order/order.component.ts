import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Order } from '../../models/order.model';
import { Coption } from '../../models/coption.model';
import { User } from '../../models/user.model';
import { OrderService } from '../../services/order.service';
import { CoptionService } from '../../services/coption.service';
import { AuthService } from '../../services/auth.service';
import { NotifierService } from '../notifier/notifier.service';
import { EdukitConfig } from '../../../ezukit.config';
import { PaymentService } from '../../services/payment.service';

@Component({
    selector:'ek-order-component',
    templateUrl:'./order.component.html'
})
export class OrderComponent implements OnInit{ 
    order:Order;
    API_URL:string;
    orderPayments:any[];
    orderHistory:any[];
    @Input() APP:string;
    @Input() ACTIVITY:string;
    @Input() orderId:string;
    @Input() invoiceOpt:boolean;
    @Input() studentProfileDownloadOpt:boolean;
    @Output() onOrderLoad:EventEmitter<Order>=new EventEmitter<Order>();
    statusForm: FormGroup;
    orderDiscounts:any[];
    discountForm:FormGroup;
    formStatus = "Normal";
    disFormStatus="Normal";
    panelLoader="none";
    statusOptions:Coption[];
    orderProducts:any[];
    showStdInfo:boolean=false;
    hfp:string=""; //HistoryFilteredProductid
    showBatchChangeWindow:boolean=false;
    sessionUser:User;
    userPerms:any;
    customParamsKeys:string[];
    paymentOptions:any[];
    showPaymentCancelWindow:boolean;
    showPaymentTransferModal:boolean;
    paymentId;
    selectedPayment;
    userOrders;
    paymentNodes;
    fh: any = { 
        status: 'Normal',
        error: null
    };
    cancelPaymentForm:FormGroup;
    transferPaymentForm: FormGroup;
    showPaymentRefundModal: boolean;
    refundPaymentForm: FormGroup;
    editPaymentModeForm: FormGroup;
    showEditPaymentModal: boolean;
    selectedPaymentIndex: number;
    constructor(
        private fb:FormBuilder,
        private orderService:OrderService,
        private paymentService:PaymentService,
        private coptionService:CoptionService,
        private authService:AuthService,
		private notifier: NotifierService){
            this.API_URL=EdukitConfig.BASICS.API_URL;
        }


    ngOnInit(): void{
        this.sessionUser=this.authService.admin();
        this.userPerms=this.authService.getUserPerms(this.sessionUser.activityPermission, this.APP, this.ACTIVITY, ['CHANGE_BATCH','VIEW', 'VIEW_EMAIL','VIEW_MOBILE']);
        this.statusForm=this.fb.group({
            productId:['', Validators.required],
            status:['', Validators.required],
            comment:[''],
            notify:[true, Validators.required]
        });
        this.discountForm = this.fb.group({
            paymentOption: [null, Validators.required],
            type: [null, Validators.required],
            discountCode: [''],
            discountType: [null],
            discountVal: ['']
        });
        this.getOrder(this.orderId);
        this.cancelPaymentForm= this.fb.group({
            reason:['', Validators.required]
        });
        this.transferPaymentForm = this.fb.group({
            newOrderNo:[, Validators.required],
            paymentNode:[null, Validators.required]
        });
        this.refundPaymentForm  = this.fb.group({
            refundAmount: [, Validators.required],
            refundReason: ['', Validators.required],
            paymentNode:[null, Validators.required]
        });
        this.editPaymentModeForm= this.fb.group({
            primaryMode:[null, Validators.required],
            trackingId:['']
        })

    }

    getOrder(orderId){
        this.orderService.getOneOrder(orderId).subscribe( (data)=>{
                this.order=data;
                this.onOrderLoad.emit(data);
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
                
                this.loadOrderStatusCoptions();
                if(this.order.customParams){
                    this.customParamsKeys=Object.keys(this.order.customParams);    
                }
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader="deleted";
            }
        );
    }

    deleteOrder(orderId){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete this?");
        if(!confirm){
            return;
        }
        this.orderService.deleteOrder(orderId).subscribe(
            (res)=>{
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }

    loadOrderStatusCoptions(){
        this.coptionService.getCoption({option:'ORDERSTATUS'}).subscribe(
            res=>{
                this.statusOptions=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 2000);
            }
        )
    }

    updateOrderStatus(cb?:Function){
        this.formStatus="Processing";
        var statusData=this.statusForm.value;
        statusData.orderId=this.order.id;
        this.orderService.updateOrderStatus(statusData).subscribe(
            res=>{
                this.formStatus="Normal";
                // this.order.history=res;
                if(cb){
                    cb();
                }
            },
            err=>{
                this.formStatus="Normal";
                this.notifier.alert(err.code, err.message, "danger", 5000);

            }
        )
    }
    quickUpdateOrderStatus(productIndex, productId, status, comboProductIndex?:number){
        let frmData={
            productId:productId,
            status:status,
            comment:'',
            notify:true
        };
        this.statusForm.patchValue(frmData);
        let self=this;
        this.updateOrderStatus(function(){
            if(comboProductIndex){
                self.order.products[productIndex].comboProducts[comboProductIndex].status=status;
            }else{
                self.order.products[productIndex].status=status;
            }
        });
    }
    approvePayment(paymentIndex){
        if(this.orderPayments[paymentIndex].transactionStatus!=="Success"){
            let conf=window.confirm("This payment is not completed yet properly. Are you sure to generate Invoice for this payment.");
            if(!conf){
                return;
            }
        }
        this.panelLoader="show";
        this.orderService.approvePayment(this.orderPayments[paymentIndex].id).subscribe(
            res=>{
                this.panelLoader="none";
                console.log(res);
                this.orderPayments[paymentIndex]=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 2000);
                this.panelLoader="none";
            }
        )
    }
    approveOrder(){
        if(this.order.status!=="Placed"){
            let conf=window.confirm("This order is not placed yet properly. Are you sure to generate Invoice for this order");
            if(!conf){
                return;
            }
        }
        this.panelLoader="show";
        this.orderService.approveOrder(this.order.id).subscribe(
            res=>{
                this.panelLoader="none";
                this.order.isApproved=res.isApproved;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 2000);
                this.panelLoader="none";
            }
        )
    }
    createReceiptPdf(paymentIndex){
        this.orderService.createReceiptPdf(this.orderPayments[paymentIndex].id).subscribe(
            res=>{
                this.orderPayments[paymentIndex]=res;
                this.panelLoader="none";
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 2000);
                this.panelLoader="none";
            }
        )
    }
    reCreateReceiptPdf(paymentIndex){
        this.orderService.reCreateReceiptPdf(this.orderPayments[paymentIndex].id).subscribe(
            res=>{
                this.orderPayments[paymentIndex]=res;
                this.panelLoader="none";
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 2000);
                this.panelLoader="none";
            }
        )
    }
    onNewPaymentAdd(payment){
        if(!this.orderPayments){
            this.orderPayments=[];
        }
        this.order.paid=this.order.paid+payment.amount;
        this.orderPayments.push(payment);
    }

    removeOrderProduct(index:number, comboIndex?:number){
        this.panelLoader="show";
        this.orderService.removeOrderProduct(this.order.id, index, comboIndex).subscribe(
            res=>{
                this.panelLoader="none";
                this.order.total=res.total;
                this.order.subTotal=res.subTotal;
                if(comboIndex!=null){
                    this.order.products[index].comboProducts[comboIndex].removed=true;
                }else{
                    this.order.products[index].removed=true;
                }
                this.notifier.alert('Done!', 'Updated successfully!', "success", 10000);
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )
    }

    processFullfillStep(){
        this.panelLoader="show";
        this.orderService.processOrderFullfillStep({orderId:this.order.id}).subscribe(
            res=>{
                this.panelLoader="none";
                this.notifier.alert("Done", "Fullfilled!", "success", 2000);
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }
    addOrderDiscount(){
        let data=this.discountForm.value;
        if(data.type=="CUSTOM"){
            if(data.discountType=="null"||data.discountVal==""){
                return this.notifier.alert("Error!", "please fill all the fields", "danger", 5000);
            }
            data.discountCode="CUSTOM-"+data.discountType;
            
        }else{
            if (data.discountCode == "") {
                return this.notifier.alert("Error!", "please fill all the fields", "danger", 5000);
            }
        }
        data.orderId = this.order.id;
        this.disFormStatus="Processing";
        if(!this.paymentOptions[data.paymentOption]){
            return this.notifier.alert("Error!", "Please select payment option/emi.", "danger", 2000);
        }
        data.paymentNode=this.paymentOptions[data.paymentOption].id;
        this.orderService.addOrderDiscount(data).subscribe(
            res=>{
                this.discountForm.reset();
                if(this.orderDiscounts){
                    this.orderDiscounts.push(res);
                }
                
                this.order.discountTotal=this.order.discountTotal+res.amount;
                this.order.total = this.order.total - res.amount;
                this.disFormStatus = "Normal";
                this.notifier.alert('Success!', 'Discount applied successfully!', "success", 5000);
            },
            err=>{
                this.disFormStatus = "Normal";
                
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }
    loadPayments(load?:any){
        if(!this.order){
            return;
        }
        if(!load&&this.orderPayments){
            return;
        }
        this.panelLoader="show";
        this.paymentService.getPayment({order:this.order.id, populateStore:true}).subscribe(
            res=>{
                this.panelLoader="none";
                this.orderPayments=res;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 2000);
            }
        )
    }
    loadProducts(){
        if(!this.order){
            return;
        }
        if(this.orderProducts){
            return;
        }
        this.panelLoader="show";
        this.orderService.getOrderProduct(this.order.id).subscribe(
            res=>{
                this.panelLoader="none";
                this.orderProducts=res;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 2000);
            }
        )
    }
    loadOrderDiscounts(){
        if(!this.order){
            return;
        }
        if(this.orderDiscounts){
            return;
        }
        this.panelLoader="show";
        this.loadOrderPaymentOption();
        this.orderService.getOrderDiscount(this.order.id).subscribe(
            res=>{
                this.panelLoader="none";
                this.orderDiscounts=res;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 2000);
            }
        )
    }
    loadOrderPaymentOption(){
        this.paymentService.getOrderPaymentNodes({order:this.order.id}).subscribe(
            res=>{
                this.paymentOptions=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 2000);
            }
        )
    }
    onTabChange(e){
        if(e.index===1){
            this.loadPayments();
        }
        if(e.index===2){
            this.loadOrderDiscounts();
        }
        if(e.index===4){
            this.loadProducts();
        }
    }
    showCancelPayment(paymentId){
        this.paymentId=paymentId;
        this.showPaymentCancelWindow=true;

    }
    cancelPayment(){
        this.formStatus="Processing";
        let confirm = window.confirm("Are you sure to Cancel Payment!!");
        if (!confirm) {
            this.showPaymentCancelWindow=false;
            return;
        }
        if (!this.cancelPaymentForm.valid) {
            return;
        }
        if (!this.paymentId) {
            return;
        }
        this.paymentService.cancelPayment(this.paymentId, this.cancelPaymentForm.value.reason).subscribe(
            (res)=>{
                this.showPaymentCancelWindow = false;
                this.notifier.alert('Success', 'Successfully Canceled!!', 'success', 500);
                this.loadPayments('load');
                this.getOrder(this.orderId);
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    showTransferPayment(paymentIndex) {
        this.paymentId = this.orderPayments[paymentIndex].id;
        this.selectedPayment = this.orderPayments[paymentIndex];
        this.showPaymentTransferModal = true;
        // this.loadOrders();
    }
    transferPayment(){
        if (!this.transferPaymentForm.valid) {
            return;
        }
        this.formStatus = "Processing";
        let confirm = window.confirm("Are you sure to Cancel Payment!!");
        if (!confirm) {
            this.showPaymentTransferModal=false;
            this.formStatus = "none";
            return;
        }
        if (!this.paymentId){
            return;
        }
        let formData:any={}= this.transferPaymentForm.value;
        this.paymentService.transferPayment(this.paymentId, formData ).subscribe(
            (res)=>{
                this.notifier.alert('Success', "Successfully Transfered", 'success', 500);
                this.showPaymentTransferModal=false;
                this.formStatus = "none";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }

    loadPaymentNode(orderNo){
        this.paymentService.getOrderPaymentNodes({ orderNo: orderNo}).subscribe(
            (res)=>{
                this.paymentNodes= res;
                this.notifier.alert('Success', "Successfully Loaded", 'success', 500);

            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    showRefundPayment(paymentIndex) {
        this.paymentId = this.orderPayments[paymentIndex].id;
        this.selectedPayment = this.orderPayments[paymentIndex];
        this.showPaymentRefundModal = true;
        this.loadPaymentNode(this.order.orderNo);
        // this.loadOrders();
    }
    refundPayment() {
        if (!this.refundPaymentForm.valid) {
            return;
        }
        if (this.refundPaymentForm.value.refundAmount<1) {
            this.notifier.alert('Error', "Amount can't be zero", 'danger', 5000);
        }
        this.formStatus = "Processing";
        let confirm = window.confirm("Are you sure to Refund Payment!!");
        if (!confirm) {
            this.showPaymentRefundModal = false;
            this.formStatus = "none";
            return;
        }
        if (!this.paymentId) {
            return;
        }
        let formData: any = {} = this.refundPaymentForm.value;
        formData.paymentNode= this.paymentNodes[formData.paymentNode].id;
        this.paymentService.refundPayment(this.paymentId, formData).subscribe(
            (res) => {
                this.notifier.alert('Success', "Successfully Refunded", 'success', 500);
                this.showPaymentRefundModal = false;
                this.formStatus = "none";
                this.getOrder(this.orderId);
                this.loadPayments('load');
            },
            (err) => {
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    checkPaymentAmount(amount){
        if (amount>this.paymentNodes[this.refundPaymentForm.value.paymentNode].paid) {
            this.notifier.alert('Error', 'Amount is too large then payment node paid amount', 'danger', 5000);
            this.refundPaymentForm.patchValue({refundAmount:0});
            return;
        }
        
    }
    // loadOrders(){
    //     this.orderService.getOrder({user:this.order.user.id}).subscribe(
    //         (res)=>{
    //             this.userOrders=res;
    //         },
    //         (err)=>{
    //             this.notifier.alert(err.code, err.message, 'danger', 5000);
    //         }
    //     )
    // }
    unfullfillProduct(){
        this.panelLoader="show";
        this.orderService.unfullfillProduct(this.order.id).subscribe(
            res=>{
                this.panelLoader="none";
                this.notifier.alert('Done!', 'Un Fullfill Order Processed successfully!', "success", 10000);
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )
    }

    showEditPmt(paymentIndex?:number){
        this.selectedPaymentIndex= paymentIndex;
        if( this.orderPayments[paymentIndex]){
            this.selectedPayment = this.orderPayments[paymentIndex];
            this.editPaymentModeForm.patchValue(this.selectedPayment);
            this.showEditPaymentModal=true;
        }else{
            return;
        }
    }

    updatePaymentMode(){
        if(!this.editPaymentModeForm.valid) return;
        if(!this.editPaymentModeForm.dirty) return;

        let confirm = window.confirm("Are you sure to update Payment mode and tracking ID? Please check  first and  then confirm..");
        if(!confirm) return;

        let data:any =this.editPaymentModeForm.value;
        if((data.primaryMode=='Online'||data.primaryMode=='DD'||data.primaryMode=='NEFT'||data.primaryMode=='CHEQUE')&&(!data.trackingId||data.trackingId==''||data.trackingId=='null')){
            this.notifier.alert('Error', "Please Enter TrackingID", 'danger', 500);
            return;
        }

        if(data.trackingId!==this.selectedPayment.trackingId)
        data.trackingIdChanedBy=this.sessionUser.id;

        this.formStatus="Processing";

        this.paymentService.updatePayment(this.selectedPayment.id, data).subscribe(
            res=>{
                this.formStatus="normal";
                if(res&&res.length>0)
                this.orderPayments[this.selectedPaymentIndex].primaryMode= res[0].primaryMode;
                this.editPaymentModeForm.reset();
                this.showEditPaymentModal=false;
                this.selectedPayment=null;
                this.selectedPaymentIndex=null;
            },
            err=>{
                this.formStatus="normal";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
}
