import { NgModule } from "@angular/core";
import { DemoDashcard } from './demo.component';

@NgModule({
    declarations:[DemoDashcard],
    exports:[DemoDashcard]
})
export class DemoDashcardModule {
    
}