import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { NotifierService } from '../../notifier/notifier.service';

@Component({
    selector:'ek-user-change-password',
    templateUrl:'./change-password.component.html'
})
export class UserChangePasswordComponent implements OnInit{ 
    formStatus="Normal";
    changePasswordFrm:FormGroup;
    frmError:string;

    @Input() title:string;
    @Input() userId:string;

    constructor(
        private fb:FormBuilder,
        private userService:UserService,
        private notifier: NotifierService,

    ){}
    ngOnInit(): void{
        this.changePasswordFrm=this.fb.group({
            passA:['', Validators.required],
			passB:['', Validators.required]
        })
        
    }
    changePassword(){
        if(!this.changePasswordFrm.dirty){
			return;
		}
        this.frmError=null;
        let frmData=this.changePasswordFrm.value;
        
        if(frmData.passA!=frmData.passB){
			this.frmError="New Password & Confirm Password must be same";
			return;
        }
        if(!this.validatePassword(frmData.passA)){
            this.frmError="Please enter valid password (atleast 5 chracter long)!";
            return; 
        }
        this.formStatus="Processing";
        this.userService.resetPassword(this.userId, frmData.passA).subscribe(
            res=>{
                this.notifier.alert('Done!', 'Updated successfully!', "success", 5000);
                this.formStatus="Normal";
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
                this.formStatus="Normal";
            }
        )
    }

    validatePassword(password:string){
        let pass=password.trim();
        return pass.length>4;
    }
    
}
