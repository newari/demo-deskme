import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserChangePasswordComponent } from './change-password.component';
import { UserService } from '../../../services/user.service';

@NgModule({
    declarations:[UserChangePasswordComponent],
    imports:[CommonModule, FormsModule, ReactiveFormsModule],
    exports:[UserChangePasswordComponent],
    providers:[UserService]
})
export class UserChangePasswordModule {
    
}
