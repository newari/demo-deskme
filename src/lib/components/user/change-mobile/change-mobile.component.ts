import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { NotifierService } from '../../notifier/notifier.service';

@Component({
    selector:'ek-user-change-mobile',
    templateUrl:'./change-mobile.component.html'
})
export class UserChangeMobileComponent implements OnInit{ 
    formStatus="Normal";
    changeMobileFrm:FormGroup;
    frmError:string;

    @Input() title:string;
    @Input() userId:string;
    @Input() userType:string;

    constructor(
        private fb:FormBuilder,
        private userService:UserService,
        private notifier: NotifierService,

    ){}
    ngOnInit(): void{
        this.changeMobileFrm=this.fb.group({
            mobile:['', Validators.required]
        })
    }
    changeMobile(){
        this.frmError=null;
        let frmData=this.changeMobileFrm.value;
        if(!this.validateMobile(frmData.mobile)){
            this.frmError="Please enter 10 digit mobile number!";
            return; 
        }
        this.formStatus="Processing";
        this.userService.changeMobile(this.userId, this.userType, frmData.mobile).subscribe(
            res=>{
                this.notifier.alert('Done!', 'Updated successfully!', "success", 5000);
                this.formStatus="Normal";
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
                this.formStatus="Normal";
            }
        )
    }

    validateMobile(mobileNo:string){
        var re=/^[0-9]{10}$/;
        return re.test(mobileNo);
    }
    
}
