import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';


import { UserChangeMobileComponent } from './change-mobile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../../../services/user.service';

@NgModule({
    declarations:[UserChangeMobileComponent],
    imports:[CommonModule, FormsModule, ReactiveFormsModule],
    exports:[UserChangeMobileComponent],
    providers:[UserService]
})
export class UserChangeMobileModule {
    
}
