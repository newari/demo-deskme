import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';


import { UserChangePhotoComponent } from './change-photo.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { StudentService } from '../../../services/student.service';
import { FileinputModule } from '../../filemanager/fileinput.module';

@NgModule({
    declarations: [UserChangePhotoComponent],
    imports: [CommonModule, FormsModule, ReactiveFormsModule, FileinputModule],
    exports: [UserChangePhotoComponent],
    providers: [UserService, StudentService]
})
export class UserChangePhotoModule {

}
