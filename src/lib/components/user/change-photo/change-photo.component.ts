import { Component, OnInit, Input, AfterViewInit, AfterContentChecked, AfterContentInit, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { NotifierService } from '../../notifier/notifier.service';
import { StudentService } from '../../../services/student.service';
import { Student } from '../../../models/student.model';

@Component({
    selector: 'ek-user-change-photo',
    templateUrl: './change-photo.component.html'
})
export class UserChangePhotoComponent implements OnInit, OnChanges {
     
     
        
    formStatus = "Normal";
    changeStudentPhotoForm: FormGroup;
    frmError: string;
    student: Student;
    @Input() title: string;
    @Input() studentId: string;
    @Input() userType: string;

    constructor(
        private fb: FormBuilder,
        private userService: UserService,
        private studentService:StudentService,
        private notifier: NotifierService,

    ) { }
    ngOnInit(): void {
        this.changeStudentPhotoForm = this.fb.group({
            personalImg: ['', Validators.required],
            signImg: ['', Validators.required]
        });
    }
    getStudent(){
        this.studentService.getOneStudent(this.studentId).subscribe(
            (res)=>{
                this.student=res;
                console.log(res);
                
                this.changeStudentPhotoForm.patchValue(res);
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    updateStudentPhotos() {
        this.frmError = null;
        if (!this.changeStudentPhotoForm.dirty){
            return;
        }
        let frmData = this.changeStudentPhotoForm.value;
        if (frmData.personalImg=='') {
            this.frmError = "Please select a Personal Image!";
            return;
        }
        if (frmData.signImg=='') {
            this.frmError = "Please select a Signature Image!";
            return;
        }
        this.formStatus = "Processing";
        this.studentService.updateStudent(this.student.id, frmData).subscribe(
            (res) => {
                this.formStatus = "Normal";
                this.notifier.alert('Success', 'Saved Successfully', 'success', 500);
            },
            (err) => {
                this.notifier.alert(err.code, err.message, 'danger', 5000);
                this.formStatus = "Normal";
            }
        );
        // this.userService.up(this.userId, this.userType, frmData.mobile).subscribe(
        //     res => {
        //         this.notifier.alert('Done!', 'Updated successfully!', "success", 5000);
        //         this.formStatus = "Normal";
        //     },
        //     err => {
        //         this.notifier.alert(err.code, err.message, "danger", 10000);
        //         this.formStatus = "Normal";
        //     }
        // )
    }

    ngOnChanges(): void {
        if (this.studentId) {
            this.getStudent();
        }

    }

}
