import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { UserInfoComponent } from './info.component';
import { UserService } from '../../../services/user.service';
import { OrderService } from '../../../services/order.service';

@NgModule({
    declarations:[UserInfoComponent],
    imports:[CommonModule, RouterModule],
    exports:[UserInfoComponent],
    providers:[UserService, OrderService]
})
export class UserInfoModule {
    
}
