import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../../models/user.model';
import { Order } from '../../../models/order.model';
import { UserService } from '../../../services/user.service';
import { OrderService } from '../../../services/order.service';
import { NotifierService } from '../../notifier/notifier.service';

@Component({
    selector:'ek-user-info',
    templateUrl:'./info.component.html'
})
export class UserInfoComponent implements OnInit{ 
    user:User;
    panelLoader="none";
    activeTab:number=0;

    userOrders:Order[];
    
    @Input() userId:any;
    @Input() orderViewOpt:string;
    @Input() orderInvoiceOpt:boolean;

    constructor(
        private userService:UserService,
        private orderService:OrderService,
        private notifier: NotifierService
    ){}


    ngOnInit(): void{
        let self=this;
		window.setTimeout(function(){
			self.getUser(self.userId);
		}, 0);
    }

    getUser(userId){
        this.panelLoader="show";
        this.userService.getOneUser(userId).subscribe( (data)=>{
                this.user=data;
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }

    loadOrders(){
        this.activeTab=1;
        // if(this.userOrders){
        //     return;
        // }
        this.panelLoader="show";
        this.orderService.getOrder({user:this.user.id}).subscribe(
            res=>{
                this.panelLoader="none";
                this.userOrders=res;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        )
    }
}
