import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { UserService } from '../../../services/user.service';
import { NotifierService } from '../../notifier/notifier.service';

@Component({
    selector:'ek-user-change-email',
    templateUrl:'./change-email.component.html'
})
export class UserChangeEmailComponent implements OnInit{ 
    formStatus="Normal";
    changeEmailFrm:FormGroup;
    frmError:string;

    @Input() title:string;
    @Input() userId:string;
    @Input() userType:string;

    constructor(
        private fb:FormBuilder,
        private userService:UserService,
        private notifier: NotifierService,

    ){}
    ngOnInit(): void{
        this.changeEmailFrm=this.fb.group({
            email:['', Validators.required]
        })
    }
    changeEmail(){
        this.frmError=null;
        let frmData=this.changeEmailFrm.value;
        if(!this.validateEmail(frmData.email)){
            this.frmError="Please enter valid email address!";
            return; 
        }
        this.formStatus="Processing";
        this.userService.changeEmail(this.userId, this.userType, frmData.email).subscribe(
            res=>{
                this.notifier.alert('Done!', 'Updated successfully!', "success", 5000);
                this.formStatus="Normal";
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
                this.formStatus="Normal";
            }
        )
    }

    validateEmail(email:string) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
}
