import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { UserChangeEmailComponent } from './change-email.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../../../services/user.service';

@NgModule({
    declarations:[UserChangeEmailComponent],
    imports:[CommonModule, FormsModule, ReactiveFormsModule],
    exports:[UserChangeEmailComponent],
    providers:[UserService]
})
export class UserChangeEmailModule {
    
}
