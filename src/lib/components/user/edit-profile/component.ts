import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../../models/user.model';
import { UserService } from '../../../services/user.service';
import { NotifierService } from '../../notifier/notifier.service';
import { ClientService } from '../../../services/client.service';

@Component({
    selector:'ek-user-edit-form',
    templateUrl:'./component.html'
})
export class UserEditProfileComponent implements OnInit{
    userForm:FormGroup;
    userData:User;
    formStatus:string="Normal";
    resetPassword:boolean=false;
    imsApps:any[]=[];
    facultyApps:string[];
    userApps:any[]=[];
    addedApps:any[]=[];
    @Output() onUserSaved:EventEmitter<any>=new EventEmitter<any>();
    @Input() submitLabel:string='Save';
    @Input() userType:string;
    @Input() userId:any;
    constructor(
		private fb:FormBuilder,
        private userService:UserService,
        private clientService: ClientService,
		private notifier: NotifierService
    ){ 
			
    }
    ngOnInit(): void {
		this.userForm=this.fb.group({
			firstName:['', Validators.required],
			lastName:['', Validators.required],
			email:['', Validators.required],
			mobile:['', Validators.required],
            profileImg:[''],
            isRecurringCustomer:[false],
            recurringCustomerOf:[[]],
			status:[null, Validators.required],
			address:this.fb.group({
                shipping:this.fb.group({
                    address:[''],
                    landmark:[''],
                    city:[''],
                    state:[''],
                    postalCode:[''],
                    country:['India']
                }),
                billing:this.fb.group({
                    address:[''],
                    landmark:[''],
                    city:[''],
                    state:[''],
                    postalCode:[''],
                    country:['India']
                })
            }),
            username:[''],
            password:[''],
        });
        
        // this.imsApps=["ACADEMICS", "ADMINISTRATION", "HR", "BOOK_STORE",  "TESTMENT", "VIDEOVIBE", "ACCOUNTING", "FORUM", "MORE"];
        this.facultyApps=["CONVENTIONAL_EXAM"];
        this.clientService.getApps({ isInstalled: true }).subscribe(
            res => {
                console.log("here77");
                if (res.length > 0) {
                    for (let index = 0; index < res.length; index++) {
                        if (res[index].app && res[index].app.code) {
                            this.imsApps.push(res[index].app.code);

                        }
                    }
                } else {
                    this.imsApps = ["ACADEMICS", "ADMINISTRATION", "HR", "BOOK_STORE", "TESTMENT", "VIDEOVIBE", "ACCOUNTING", "FORUM", "MORE"];
                }
            })
        let self=this;
        setTimeout(function(){
            self.loadUser();
        }, 1);
        
    }

    loadUser(){
        this.userService.getOneUser(this.userId).subscribe(
            res=>{
                this.userData=res;
                this.userForm.patchValue(res);
                if(!res.apps){
                    res.apps={};
                }
                if(res.type=="ADMIN"&&this.userType=="ADMIN"){
                    this.addedApps=res.apps.ims||[];
                    this.userApps=this.imsApps;
                }else if(res.type=="FACULTY"&&this.userType=="FACULTY"){
                    this.addedApps=res.apps.faculty||[];
                    this.userApps=this.facultyApps;
                    
                }
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )
    }

    updateUser(){
        
        let userData=this.userForm.value;
        if(!this.userData&&userData.password.toString().trim()==""){
            userData.password=Math.floor((Math.random() * 100000) + 1);;
        }
        if((!this.resetPassword&&this.userData)||userData.password.toString().trim()==""){
            delete userData.password;
        }

        if(this.userType){
            if(this.userType=="ADMIN"){
                userData.type="ADMIN";
                userData.apps={ims:this.addedApps};
            }else if(this.userType=="FACULTY"){
                userData.type="FACULTY";
                userData.apps={faculty:this.addedApps};
            }
            let perms: any = {};
            for (let index = 0; index < this.addedApps.length; index++) {
                perms[this.addedApps[index]] = { all: true }
            }
            userData.activityPermission = perms;
        }
        
        
        this.formStatus="Processing";
        this.userService.updateUser(this.userData.id, userData).subscribe(
            res=>{
                this.formStatus="Normal";
                userData.password="";
                this.onUserSaved.emit(res);
            },
            err=>{
                this.formStatus="Normal";
                this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
            }
        )
        
    }

    generatePassword(){
        this.userForm.patchValue({password:Math.floor((Math.random() * 100000) + 1)});
    }

    setPasswordOpt(state){
        this.resetPassword=state;
    }

    setBillingAddress(sameAsShipping){
        if(sameAsShipping){
            this.userForm.patchValue({address:{billing:this.userForm.value.address.shipping}});
        }else{
            this.userForm.patchValue({address:{billing:((this.userData&&this.userData.address&&this.userData.address.billing)?this.userData.address.billing:{address:'', landmark:'', city:'', state:'', postalCode:'', country:'India'})}});
        }
    }

    addAppAccess(appIndex){
        console.log(appIndex)
        if(this.userType=='ADMIN'){
            let appExist = this.addedApps.indexOf(this.userApps[appIndex]);
            if (appExist==-1) {
                this.addedApps.push(this.userApps[appIndex]);
            }
        }else if(this.userType=="FACULTY"){
            let appExist = this.addedApps.indexOf(this.userApps[appIndex]);
            if (appExist == -1) {
                this.addedApps.push(this.userApps[appIndex]);
            }
        }
        
    }

    removeAddedApp(i){
        this.addedApps.splice(i, 1);
    }
    
}