import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserEditProfileComponent } from './component';
import { FileinputModule } from '../../filemanager/fileinput.module';
import { UserService } from '../../../services/user.service';
import { ClientService } from '../../../services/client.service';
 

@NgModule({
    declarations:[UserEditProfileComponent],
    imports:[FileinputModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[UserEditProfileComponent],
    providers: [UserService, ClientService]
})
export class UserEditProfileModule{

}