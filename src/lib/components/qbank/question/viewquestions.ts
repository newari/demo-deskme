import { Component, OnInit, Input, ElementRef, ViewChild  } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Coption } from '../../../models/coption.model';
import { Subject } from '../../../models/subject.model';
import { Subjectunit } from '../../../models/subjectunit.model';
import { Topic } from '../../../models/topic.model';
import { QuestionService } from '../../../services/question.service';
import { NotifierService } from '../../notifier/notifier.service';
import { QsetService } from '../../../services/qset.service';
import { CoptionService } from '../../../services/coption.service';
import { OptionService } from '../../../services/option.service';
import { SubjectService } from '../../../services/subject.service';
import { TopicService } from '../../../services/topic.service';
import { SessionService } from '../../../services/session.service';
import { Option } from '../../../models/option.model';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector:'ek-full-view-questions',
    templateUrl:'./viewquestions.html',
})
export class FullViewQuestionsContent implements OnInit{ 
    rows;
    editQuesForm : FormGroup;
    columns; 
    courses: any;
    streams:Coption[];
    subjects: Subject[];
    subjectUnit: Subjectunit[];
    topics: Topic[];
    filter : any={};
    totalRecords;
    countBase:number=1;
    panelLoader="none";
    searchForm:FormGroup;
    index : number ;
    frmLoader='normal';
    options = ["A","B","C","D","E"]; 
    questionLevel = [null,"Easy","Average","Medium","Difficult","Very Difficult","Tough"];
    createdBy : any=[];
    linkQsets : any=[];
    displayQEdit = false;
    answers =[];
    questionType;
    questionId;
    qbAdvFilterActive : boolean=false;
    showLinkedQs: boolean= false;
    sessions = [];
    sourceList : Option[];
    user : any={};
    mtqAns : any={};
    @Input() routes:string;
    @Input() courseId : string;
    @Input() streamId : any;
    printButtons = [
        {label: 'Current Page', icon: 'fa-refresh', command: () => {this.printQuestion();}},
        // {label: 'Print All', command: () => {this.printAllQuestion();}},
        // {label: 'Angular.io', icon: 'fa-link', url: 'http://angular.io'},
        // {label: 'Theming', icon: 'fa-paint-brush', routerLink: ['/theming']}
    ];
    @ViewChild('mathload', { read: ElementRef }) mathload: ElementRef;
    constructor(
        private questionService:QuestionService,
        private notifier: NotifierService,
        private fb:FormBuilder,
        private qsetService : QsetService,
        private coptionService: CoptionService,
        private optionService : OptionService,
        private subjectService: SubjectService,
        private topicService: TopicService,
        private sessionService : SessionService,
        private authService : AuthService,
        private el : ElementRef,
        private router : Router
        ){}

    ngOnInit(): void{
        this.user = this.authService.user();
        this.searchForm = this.fb.group({
            id : [''],
            title:[''],
            qType:[''],
            extraQType : [''],
            course:[''],
            stream:[''],
            unit : [''],
            topic:[''],
            status:[''],
            level:[''],
            linkCount : [''],
            subject:[''],
            mp : [''],
            mn : [''],
            source : [''],
            tags : [''],
            session:[''],
            defaultLang : [''],
            stage : [''],
            createdBy : [''],
            createdAt : ['']
        })
        this.editQuesForm=this.fb.group({
            qContent:['', Validators.required],
            qType : [],
			qOption:this.fb.group({
				optionA:[''],
				optionB:[''],
				optionC:[''],
				optionD:[''],
				optionE:[''],
				optionP:[''],
				optionQ:[''],
				optionR:[''],
				optionS:[''],
				optionT:[''],
			}),
			ans:['', Validators.required],
            qSolution:['', Validators.required],
            defaultFont:['']
        });
        
        this.loadCourses();
        this.loadStreams();
        let filter:any={};
        this.loadTopics(filter);
        this.loadSubjects(filter);
        this.loadQuestions();
        if(this.user.type != 'STAFF'){
            this.loadCreatedBy();
        }
        
        this.loadSessions();
        this.loadSourceOption();
    }
    loadQuestions(filter?:any):void{
        this.panelLoader="show";
        this.filter.populatePassage=true;
        if(this.user.type == 'STAFF'){
            this.filter.createdBy = this.user.id;
        }
        this.questionService.getQuestion(this.filter,true).subscribe(
            (data) =>{ 
                this.totalRecords = data.headers.get('totalRecords') || 0;
                this.rows=data.body;
                this.panelLoader="none";
                this.loadMathJax();
            },
            (err) => {
                this.panelLoader="none";
                console.log(err)
            }
        );
    }
   
    deleteQuestion(question){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete "+question.title+" ?");
        if(!confirm){
            this.panelLoader="none";
            return;
        }
        this.questionService.deleteQuestion(question.id).subscribe(
            (res)=>{
                this.rows.splice(this.rows.indexOf(question), 1);
                this.panelLoader="none";
                this.notifier.alert('Success', 'deleted Successfully', 'success', 1000 );
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    searchData(){
        if (!this.searchForm.valid) {
            return;
        }
        this.filter = Object.assign(this.filter, this.searchForm.value);
        this.loadQuestions();
    }
    loadCourses(){
		this.frmLoader="show";
		let filter={};
		filter ={option:'COURSE'};
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				this.frmLoader="none";
				if(!res[0]){
					this.courses=[res];
                    this.searchForm.patchValue({course : this.courses[0].id});
                    
				}else{
					this.courses=res;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
    loadSubjects(streamId) {
        this.frmLoader="show";
        this.subjectService.getSubject(streamId).subscribe(
            (res) => {
                this.frmLoader = "none";
                this.subjects = res;
            },
            (err) => { 
                this.frmLoader = "none"; 
                this.notifier.alert(err.code, err.message, 'danger', 5000); 
            }
        )
    }
    loadSubjectUnit(subjectid){
		this.subjectService.getUnit(subjectid).subscribe(
			(res)=>{
				this.frmLoader="none";
				this.subjectUnit=res;
			},
			(err)=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
	}
    loadTopics(filter) {
        this.frmLoader="show";
        this.topicService.getTopic(filter).subscribe(
            (res) => {
                this.frmLoader = "none";
                this.topics = res;
            },
            (err) => { 
                this.frmLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
    loadCreatedBy(){
        this.frmLoader="show";
        this.questionService.getUserCreatedBy().subscribe(
            (res) => {
                this.createdBy = res;
                this.frmLoader="none";
            },
            (err) => {
                this.frmLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000); }
        )
    }
    loadSessions(){
		this.frmLoader="show";
		this.sessionService.getSession({status:true}).subscribe(
			res=>{
				this.frmLoader="none";
				this.sessions=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
    }
    setSubjects(streamId) {
        let filter: any = {};
        filter.stream = streamId;
        this.loadSubjects(filter);

    }
    setUnits(subjectId){
		this.loadSubjectUnit(subjectId);
	}
    setTopics(unitId){
		let filter:any={};
		filter.unit=unitId;
		this.loadTopics(filter);
	}
    paginate(e) {
        if (!this.filter) { 
            this.filter = {};
        }
        this.countBase = e.rows * e.page + 1;
        this.filter.page = (e.page + 1);
        this.filter.limit = e.rows;
        this.loadQuestions();
    }
    qLink(qId){
        this.qsetService.linkedQsets(qId).subscribe(
            res=>{
                this.linkQsets = res;
                this.showLinkedQs=true;
            },
            err=>{
                this.notifier.alert(err.code,err.message,'danger',1000);
            }
        );
    }
    sortBy(value){
        this.filter.sortBy = value;
        this.loadQuestions();
    }
    showQuickEdit(index){
        this.displayQEdit = true;
        this.index = index;
        this.questionId = this.rows[index].id;
        this.questionType = this.rows[index].qType;
        let ansExist = this.rows[index].ans;
        if(ansExist){
            if(this.questionType == 'MAQ'){
                var ans=ansExist.split(',');
                this.answers = ans;
            }
            else if(this.questionType == 'MTQ'){
                var ans=ansExist.split(',');
                for (let i = 0; i < ans.length; i++) {
                    let element = ans[i];
                    element = element.split('-');
                    if(element[0]&&element[1]){
                        this.mtqAns[element[0]]= element[1].split('');
                    }
                }
            }
        }
        this.editQuesForm.patchValue(this.rows[index]);
    }
    insertCheckbxValue(event){		
		if(event.target.checked){
			this.answers.push(event.target.value);
		}else{
			let index = this.answers.indexOf(event.target.value);
			this.answers.splice(index,1);
		}
    }
    mtqAnsChange(event,option){
		if(event.target.checked){
			if(!this.mtqAns[option]){
				this.mtqAns[option]=[];
			}
			this.mtqAns[option].push(event.target.value);
		}else{
			if(this.mtqAns[option]){
				let index = this.mtqAns[option].indexOf(event.target.value);
				this.mtqAns[option].splice(index,1);
			}
		}
	}
    updateEditQuestion(){
        let data=this.editQuesForm.value;
        if(data.qType == 'MAQ' && this.answers.length > 0){
			data.ans=this.answers;
		}
		else if(data.qType == 'MTQ'){
			data.ans='';
			for(let key in this.mtqAns){
				if(this.mtqAns[key].length>0){
					let ans = key+'-';
					ans+= this.mtqAns[key].sort().join('');
					data.ans+= ans+',';
				}
			}
			data.ans=data.ans.slice(0, -1);
        }
        this.questionService.updateQuestion(this.questionId,data).subscribe(
			(res)=>{
                this.notifier.alert('Success', 'Saved changes Successfully', 'success', 1000 );
                this.loadQuestions();
                this.displayQEdit = false;
                this.answers=[];
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 1000 );
			}
		);
    }
    loadSourceOption() {
        this.optionService.getOption({ name: "SOURCE", limit:120, sort:'createdAt DESC'}).subscribe(
            (data) => {
				this.sourceList = data;
            },
            (err) => {
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
	}
    loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub,this.mathload]);
        }, 10);
    }
    printQuestion(){
        console.log(this.totalRecords);
        
        if(this.totalRecords < 500){
            this.filter.limit='all';
        }
        this.router.navigate(['/'+this.routes+'/qbank/question/print'],{queryParams: this.filter});
    }
}
