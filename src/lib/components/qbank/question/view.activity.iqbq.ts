import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { QuestionViewContent } from './view';
import { QuestionService } from '../../../services/question.service';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';

@NgModule({
    declarations:[QuestionViewContent],
    imports:[RouterModule, CommonModule,SafeHtmlPipeModule],
    providers:[QuestionService],
    exports:[QuestionViewContent]
}) 
export class QuestionViewModule {} 
