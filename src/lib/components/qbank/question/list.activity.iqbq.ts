import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {  QuestionListContent } from './list';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PaginatorModule, DialogModule } from 'primeng/primeng';
import { EkDropdownModule } from '../../dropdown/dropdown.module';
import { QuestionService } from '../../../services/question.service';
import { CoptionService } from '../../../services/coption.service';
import { SubjectService } from '../../../services/subject.service';
import { TopicService } from '../../../services/topic.service';
import { QsetService } from '../../../services/qset.service';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';
@NgModule({
    declarations:[QuestionListContent],
    imports: [CommonModule, RouterModule, ReactiveFormsModule, FormsModule, PaginatorModule,EkDropdownModule,DialogModule,SafeHtmlPipeModule], 
    providers:[QuestionService,CoptionService,SubjectService,TopicService,QsetService],
    exports:[QuestionListContent]
})
export class QuestionListModule {}
