import { Component, OnInit, Input  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Coption } from '../../../models/coption.model';
import { Subject } from '../../../models/subject.model';
import { Subjectunit } from '../../../models/subjectunit.model';
import { Topic } from '../../../models/topic.model';
import { QuestionService } from '../../../services/question.service';
import { CoptionService } from '../../../services/coption.service';
import { SubjectService } from '../../../services/subject.service';
import { TopicService } from '../../../services/topic.service';
import { QsetService } from '../../../services/qset.service';
import { PassageService } from '../../../services/passage.service';
import { NotifierService } from '../../notifier/notifier.service';
import { SessionService } from '../../../services/session.service';
import { Session } from '../../../models/session.model';
import { OptionService } from '../../../services/option.service';
import { Option } from '../../../models/option.model';

@Component({
    selector:'ek-question-studio',
    templateUrl:'./studio.html'
})
export class QuestionStudioContent implements OnInit{ 
    searchForm:FormGroup;
    codeForm : FormGroup;
    qsetFilterForm : FormGroup;
    rows;
    columns; 
    qsetId;
    passageId;
    courses: any;
    streams:Coption[];
    subjects: Subject[];
    subjectUnit: Subjectunit[];
    topics: Topic[];
    activeFilter;
    totalRecords;
    countBase:number;
    panelLoader="none";
    frmLoader='normal';
    showLinkedQs : boolean=false;
    questionBankList=[];
    questionList=[];
    qbFilterActive;
    examFilterActive;
    qsetQuestion;
    seq;
    qsetTitles;
    passageTitles;
    passageQuestion;
    linkQsets :any=[];
    questionLevel = [null,"Easy","Average","Medium","Difficult","Very Difficult","Tough"];
    lang;
    sessions : Session[];
    createdBy : any=[];
    sources : Option[];
    @Input() routes:string;
    @Input() courseId : string;
	@Input() streamId : any;
    constructor(
        private notifier: NotifierService,
        private fb:FormBuilder,
        private questionService:QuestionService,
        private coptionService: CoptionService,
        private subjectService: SubjectService,
        private topicService: TopicService,
        private activatedRoute: ActivatedRoute,
        private qsetService: QsetService,
        private passageService : PassageService,
        private sessionService : SessionService,
        private optionService : OptionService,
        private router : Router
    ){}
    ngOnInit(): void{
        this.activatedRoute.queryParams.subscribe(params=>{
            this.qsetId = params['qsetId'];
            this.passageId = params['passageId'];
            this.lang = params['lang'];
        });
        this.searchForm = this.fb.group({
            id:[''],
            title:[''],
            qType:[''],
            stream:[''],
            course:[''],
            status:[''],
            level:[''],
            subject:[''],
            topic:[''],
            unit : [''],
            mn : [''],
            mp : [''],
            defaultLang : [''],
            tags : [''],
            stage : [''],
            source : [''],
            createdBy :[''],
            session : ['']  
        });
        this.codeForm = this.fb.group({
			alpha : [''],
			beta : [''],
            gamma : [''],
            stage : ['']
        });
        this.qsetFilterForm = this.fb.group({
			course : [''],
			stream : [''],
			qset : ['']
		});
        if(this.qsetId){
            this.loadQsetQuestions();
        }
        if(this.passageId){
            this.loadPassageQuestions();
        }
        this.loadCourses();
        this.loadStreams();
        this.loadSessions();
        let filter:any={};
        this.loadSubjects(filter);
        this.loadTopics(filter);
        this.loadSubjectUnit(filter);
        if(this.courseId){
            filter.course=this.courseId;
        }
        if(this.routes == 'ims'){
            this.loadCreatedBy();
            this.loadSourceOption();
        }
        this.loadQuestions(filter);
    }
    loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub]);
        }, 10);
    }
    questionBankFilters(value){
        if(value == 'question'){
            this.qbFilterActive = 'question';
            this.searchForm.reset(); 
            if(this.courseId){
                this.searchForm.patchValue({course:this.courseId});
            }
        }
        if(value == 'qset'){
            this.qbFilterActive = 'qset';
        }
        if(value == 'passage'){
            this.qbFilterActive = 'passage';
            this.loadPassage();
        }
        if(value == 'advance'){
            this.qbFilterActive = 'advance';
            this.searchForm.reset(); 
            if(this.courseId){
                this.searchForm.patchValue({course:this.courseId});
            }
        }
        if(value == 'code'){
            this.qbFilterActive = 'code';
        }
        if(value == ''){ 
            this.qbFilterActive = '';
        }
    }
    
    examFilters(value){
        if(value == 'qset'){
            this.examFilterActive = 'qset';  
        }
        else if(value == 'passage'){
            this.examFilterActive = 'passage';
            this.loadPassage();
        }
        else if(value == 'null'){ 
            this.examFilterActive = null;
        }
    }
    searchQset(qsetId){
        this.qsetId = qsetId;
        this.passageId = null;
        this.loadQsetQuestions();
    }
    searchPassage(passageId){
        this.passageId = passageId;
        this.qsetId = null;
        this.loadPassageQuestions();
    }
    loadQbQsetQuestion(qsetId){
        let data={
            id : qsetId
        };
        this.qsetService.getQsetQuestion(data).subscribe(
            (data) =>{
                this.questionBankList = data;
                this.loadMathJax();
            },
            (err) =>{this.notifier.alert(err.code, err.message, 'danger', 5000 );}
        );
    }
    loadQbPassageFilter(passageId){
        this.passageService.getPassageQuestions(passageId).subscribe(
            (data) =>{ 
            
                this.questionBankList  = data;},
            (err) => { this.notifier.alert(err.code, err.message, 'danger', 5000); }
        );
    }
    loadPassage(){
        this.passageService.getPassage().subscribe(
            (data)=>this.passageTitles = data,
            (err)=>{ this.notifier.alert(err.code, err.message, 'danger', 5000);}
        );
    } 
    loadQuestions(filter?:any):void{
        this.questionService.getQuestion(filter,true).subscribe(
            (data) =>{ 
                this.totalRecords = data.headers.get('totalRecords') || 0;
                this.questionBankList=data.body;
                this.loadMathJax();
            },
            (err) => console.log(err)
        );
    }
    loadQsetQuestions():void{
        let data={
            id : this.qsetId
        };
        this.qsetService.getQsetQuestion(data).subscribe(
            (data) =>{ 
                this.questionList = data;  
                this.loadMathJax();
            },
            (err) =>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader="deleted";
            }
        );
    }
    loadPassageQuestions():void{
        this.passageService.getPassageQuestions(this.passageId).subscribe(
            (data) =>{ 
                this.questionList  = data;
                this.loadMathJax();
            },
            (err) => console.log(err)
        );
    }
    getCodeQuestions(){
        this.questionService.getQCode(this.codeForm.value).subscribe(
            (data) =>{
                let qCode = data;
                this.activeFilter={};
                if(qCode.alpha){
                    this.activeFilter.alpha = qCode.alpha.id;
                }
                if(qCode.beta){
                    this.activeFilter.beta = qCode.beta.id;
                }
                if(qCode.gamma){
                    this.activeFilter.gamma = qCode.gamma.id;
                }
                this.activeFilter.stage = this.codeForm.value.stage;
                this.loadQuestions(this.activeFilter);
            },
            (err) =>{this.notifier.alert(err.code, err.message, 'danger', 5000 );}
        );
    }
    onMoveToSource(event){
        let question = event.items[0];
        if(this.lang && question.defaultLang){
            if(this.lang != question.defaultLang){
                for(var i= this.questionList.length-1; i >=0; i--){
                    if(question.id == this.questionList[i].id){
                        this.questionBankList.push(this.questionList[i]);
                        delete this.questionList[i];
                        break;
                    }
                }
                this.loadMathJax();
                return this.notifier.alert('Danger','Question Language is diff of Qset Language','danger',4000);
            }
        }
    }
    changePositiveMarks(value,index,id){
        let qData = this.questionList[index];
        if(qData.question._id == id || qData.id == id){
            this.questionList[index].mp = value;
        }
    }
    changeNegativeMarks(value,index,id){
        let qData = this.questionList[index];
        if(qData.question._id == id || qData.id == id){
            this.questionList[index].mn = value;
        }
    }
    saveExamQuestion(){
        if(this.qsetId){ 
            this.qsetService.updateQbankQuestion(this.qsetId,this.questionList).subscribe(
                (res)=>{
                    this.notifier.alert('Success', 'Question Added Successfully', 'success', 1000 );
                },
                (err)=>{
                    this.notifier.alert(err.code, err.message, 'danger', 5000 );
                }
            );
        }
        // if(this.passageId){
        //     for(var i = 0; i < this.questionList.length; i++){
        //         if(!this.questionList[i].question){
        //             this.passageQuestion={
        //                 "question":this.questionList[i].id,
        //                 "passage":this.passageId
        //              }
        //             this.passageService.addPassageQuestion(this.passageQuestion).subscribe(
        //                 res=>{
        //                 this.notifier.alert('Success', 'Passage Question Added Successfully', 'success', 1000 );
        //                 },
        //                 err=>{
        //                     this.notifier.alert(err.code, err.message, 'danger', 5000 );
        //                 }
        //             );
        //         }                     
        //     }
        // }
        if(this.qsetId == null && this.passageId == null){
            this.notifier.alert('danger','Select Qset or Passage First', 'danger', 5000 );
            this.examFilterActive = 'qset'; 
        }
    }

    deleteQuestion(question){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete "+question.title+"?");
        if(!confirm){
            this.panelLoader="none";
            return;
        }
        this.questionService.deleteQuestion(question.id).subscribe(
            (res)=>{
                this.rows.splice(this.rows.indexOf(question), 1);
                this.panelLoader="none";
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );

            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    searchData(){
        if (!this.searchForm.valid) {
            return;
        }       
        this.activeFilter={};
        if (this.searchForm.value.id && this.searchForm.value.id!='null') {
            this.activeFilter.id = this.searchForm.value.id;
        }
        if (this.searchForm.value.title && this.searchForm.value.title!='null') {
            this.activeFilter.title = this.searchForm.value.title;
        }
        if (this.searchForm.value.course && this.searchForm.value.course != 'null') {
            this.activeFilter.course = this.searchForm.value.course;
        }
        if (this.searchForm.value.stream && this.searchForm.value.stream != 'null') {
            this.activeFilter.stream = this.searchForm.value.stream;
        }
        if (this.searchForm.value.qType && this.searchForm.value.qType != 'null') {
            this.activeFilter.qType = this.searchForm.value.qType;
        }
        if (this.searchForm.value.level && this.searchForm.value.level != 'null') {
            this.activeFilter.level = this.searchForm.value.level;
        }
        if (this.searchForm.value.unit && this.searchForm.value.unit != 'null') {
            this.activeFilter.unit = this.searchForm.value.unit;
        }
        if (this.searchForm.value.topic && this.searchForm.value.topic != 'null') {
            this.activeFilter.topic = this.searchForm.value.topic;
        }
        if (this.searchForm.value.subject && this.searchForm.value.subject != 'null') {
            this.activeFilter.subject = this.searchForm.value.subject;
        }
        if (this.searchForm.value.mp && this.searchForm.value.mp != 'null') {
            this.activeFilter.mp = this.searchForm.value.mp;
        }
        if (this.searchForm.value.mn && this.searchForm.value.mn != 'null') {
            this.activeFilter.mn = this.searchForm.value.mn;
        }
        if (this.searchForm.value.dafalutLang && this.searchForm.value.dafalutLang != 'null') {
            this.activeFilter.dafalutLang = this.searchForm.value.dafalutLang;
        }
        if (this.searchForm.value.tags && this.searchForm.value.tags != 'null') {
            this.activeFilter.tags = [this.searchForm.value.tags];
        }
        if (this.searchForm.value.status && this.searchForm.value.status != 'null') {
            this.activeFilter.status = this.searchForm.value.status;
        }
        if (this.searchForm.value.stage && this.searchForm.value.stage != 'null') {
            this.activeFilter.stage = this.searchForm.value.stage;
        }
        if (this.searchForm.value.session && this.searchForm.value.session != 'null') {
            this.activeFilter.session = this.searchForm.value.session;
        }
        if (this.searchForm.value.source && this.searchForm.value.source != 'null') {
            this.activeFilter.source = this.searchForm.value.source;
        }
        if (this.searchForm.value.createdBy && this.searchForm.value.createdBy != 'null') {
            this.activeFilter.createdBy = this.searchForm.value.createdBy;
        }
        this.loadQuestions(this.activeFilter);
    }
    loadSessions(){
		this.frmLoader="show";
		this.sessionService.getSession({status:true}).subscribe(
			res=>{
				this.frmLoader="none";
				this.sessions=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
	}
    loadCourses(){
		this.frmLoader="show";
		let filter={};
		if(this.courseId){
			filter ={id : this.courseId}; 
		}
		else{
			filter ={option:'COURSE'};
		}
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				this.frmLoader="none";
				if(!res[0]){
					this.courses=[res];
					this.searchForm.patchValue({course : this.courses[0].id});
				}else{
					this.courses=res;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
				if(this.streamId && this.streams.length >0){
					let newStream = [];
					for(var i=0;i<this.streams.length;i++){
						for(var j=0;j<this.streamId.length;j++){
							if( this.streams[i].id == this.streamId[j]){
								newStream.push(this.streams[i]);
							}
						}
					}
					this.streams = newStream;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
    loadSubjects(streamId?:any) {
        this.subjectService.getSubject(streamId).subscribe(
            (res) => {
                this.frmLoader = "none";
                this.subjects = res;
            },
            (err) => { this.notifier.alert(err.code, err.message, 'danger', 5000); }
        )
    }
    loadSubjectUnit(subjectid?:any){
		this.subjectService.getUnit(subjectid).subscribe(
			(res)=>{
				this.frmLoader="none";
				this.subjectUnit=res;
			},
			(err)=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
	}
	loadTopics(filter?:any){
		this.topicService.getTopic(filter).subscribe(
			(res)=>{
				this.frmLoader="none";
				this.topics=res;
			},
			(err)=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
    }
    loadCreatedBy(){
        this.frmLoader="show";
        this.questionService.getUserCreatedBy().subscribe(
            (res) => {
                this.createdBy = res;
                this.frmLoader="none";
            },
            (err) => {
                this.frmLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000); }
        )
    }
    loadTitles(filter?:any){
        this.qsetService.getQsetTitles(filter).subscribe(
            (res) => {
                this.qsetTitles = res;
                if(this.qsetTitles == ''){
                    this.notifier.alert('danger',"No Qset Exist with Course and Stream", 'danger', 5000);
                }
            },
            (err) => { this.notifier.alert(err.code, err.message, 'danger', 5000); }
        )
    }
    setSubjects(streamId) {
        let filter: any = {};
        filter.stream = streamId;
        this.loadSubjects(filter);
    }
    setUnits(subjectId){	
        let filter: any = {};
        filter.subject = subjectId;	 
		this.loadSubjectUnit(subjectId);
	}
	setTopics(unitId){
		let filter:any={};
		filter.unit=unitId;
		this.loadTopics(filter);
	}
    setTitle(streamId,courseId){
        let filter: any = {};
        filter.stream = streamId;
        if (courseId && courseId!='') {
            filter.course = courseId;
        }
        this.loadTitles(filter);
    }
    qLink(qId){
        this.qsetService.linkedQsets(qId).subscribe(
            res=>{
                this.linkQsets = res;
                this.showLinkedQs=true;
            },
            err=>{
                this.notifier.alert(err.code,err.message,'danger',1000);
            }
        );
    }
    setQueryParams(){
		if(this.qsetId){
			this.router.navigate(['/'+this.routes+'/qbank/qset/view/'],{queryParams : {qsetId : this.qsetId}});
		}else if(this.passageId){
			this.router.navigate(['/'+this.routes+'/qbank/passage/view/'],{queryParams : {passageId : this.passageId}});
		}else{
			this.router.navigate(['/'+this.routes+'/qbank/question']);
		}
	}
    paginate(e) {
        if (!this.activeFilter) {
            this.activeFilter = {};
        }
        this.countBase = e.rows * e.page + 1;
        this.activeFilter.page = (e.page + 1);
        this.activeFilter.limit = e.rows;
        this.loadQuestions(this.activeFilter);
    }
    loadSourceOption() {
        // this.frmLoader = "show";
        this.optionService.getOption({ name: "SOURCE" }).subscribe(
            (data) => {
				this.sources = data;
                // this.frmLoader = "none";
            },
            (err) => {
                // this.frmLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
	}
}
