import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {  QuestionPrintContent } from './print';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { QuestionService } from '../../../services/question.service';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';

@NgModule({ 
    declarations:[QuestionPrintContent],
    imports: [CommonModule, RouterModule, ReactiveFormsModule,SafeHtmlPipeModule],
    providers:[QuestionService], 
    exports:[QuestionPrintContent]
})
export class QuestionPrintModule {}
