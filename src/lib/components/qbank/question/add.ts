import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Passage } from '../../../models/passage.model';
import { Question } from '../../../models/question.model';
import { Coption } from '../../../models/coption.model';
import { Subject } from '../../../models/subject.model';
import { Subjectunit } from '../../../models/subjectunit.model';
import { Topic } from '../../../models/topic.model';
import { QuestionService } from '../../../services/question.service';
import { NotifierService } from '../../notifier/notifier.service';
import { CoptionService } from '../../../services/coption.service';
import { SubjectService } from '../../../services/subject.service';
import { TopicService } from '../../../services/topic.service';
import { PassageService } from '../../../services/passage.service';
import { QsetService } from '../../../services/qset.service';
import { AuthService } from '../../../services/auth.service';
import { SessionService } from '../../../services/session.service';
import { Session } from '../../../models/session.model';
import { IfStmt } from '@angular/compiler';
import { OptionService } from '../../../services/option.service';
import { Option } from '../../../models/option.model';
import { ClientService } from '../../../services/client.service';
declare var CKEDITOR:any;
@Component({
    selector:'ek-question-add',
    templateUrl:'./add.html'
})
export class QuestionAddContent implements OnInit,AfterViewInit{
	codeForm : FormGroup;
	quesForm:FormGroup;
	passageForm:FormGroup;
	passageData:Passage;
	qData:Question;
	questionType:string;
	qsetId;
	config;
	passageId;
	questionId;
	qsetQuestion;
	passageQuestion;
	questionSeqId:number;
	formStatus="Normal";
	frmLoader="none";
	courses:any;
	streams:Coption[];
	subjects:Subject[];
	subjectUnit:Subjectunit[];
	topics:Topic[];
	passages:any=[];
	sessions=[];
	answers =[];
	options = ["A","B","C","D","E"];
	displayPassage: boolean = false;
	variantEnable : boolean=false;
	addedPassage:any;
	passageBoxStatus:boolean;
	qCode;
	user;
	showCodeModal : boolean=false;
	qsetLang;
	examList:Option[];
	sourceList : Option[];
	qContent:string;
	isClientSeller : boolean=false;
	mtqAns : any={};
	@Input() routes : string;
	@Input() courseId : string;
	@Input() streamId : any;
	constructor(
		private fb:FormBuilder,
		private questionService:QuestionService,
		private notifier: NotifierService,
		private coptionService:CoptionService,
		private optionService : OptionService,
		private subjectService:SubjectService,
		private topicService:TopicService,
		private sessionService:SessionService,
		private router:Router,
		private activatedRoutes:ActivatedRoute,
		private passageService:PassageService,
		private qsetService: QsetService,
		private authService: AuthService,
		private clientService : ClientService,
	){}
	ngOnInit(): void {
		this.user= this.authService.user();
		if(this.user){
			this.clientService.getClientConfig({client : this.user.client, allowedApps:'303030303030307162616e6b'}).subscribe(
				res=>{
				this.isClientSeller = res.isESSeller;
				},err=>{console.log(err);}
			);
		}
		this.activatedRoutes.queryParams.subscribe(params => {
			this.questionType = params['questionType'];
			this.qsetId = params['qsetId'];
			this.passageId = params['passageId'];
			this.qsetLang = params['lang'];
		});
		this.quesForm=this.fb.group({
			title:[''],
			qContent:['', Validators.required],
			qOption:this.fb.group({
				optionA:[''],
				optionB:[''],
				optionC:[''],
				optionD:[''],
				optionE:[''],
				optionP:[''],
				optionQ:[''],
				optionR:[''],
				optionS:[''],
				optionT:[''],
			}),
			ans:'',
			qSolution:[''],
			stream:[''],
			course:[''],
			duration:[0],
			mp:[0],
			mn:[0],
			qType:['', Validators.required],
			extraQType:[''],
			subject:[''],
			topic:[''],
			unit:[''],
			tags:[],
			exam:[''],
			source : [''],
			defaultLang:[this.qsetLang || '',Validators.required],
			defaultFont:[''],
			alpha:[''],
			beta:[''],
			gamma:[''],
			partialMark:[false],
			status:[true, Validators.required],
			level:[],
			havePassage:[false],
			passage:[''],
			linkCount : [0],
			session:[],
			isVariant :[false],
			variant : [''],
			forSell : [false],
			price : [0],
			stage : ['NOT_REVIEWED'],
			createdBy:[this.user.id]
		});
		this.passageForm=this.fb.group({
			title:['', Validators.required],
			passageContent:['', Validators.required],
			status:[false,Validators.required]
		});
		this.codeForm = this.fb.group({
			alpha : ['',Validators.required],
			beta : ['',Validators.required],
			gamma : ['',Validators.required]
		});
		if(this.passageId){
			this.addedPassage = this.passageId;
		}
		if(this.questionType){
			this.quesForm.patchValue({qType:this.questionType});
		}
		this.loadCourses();
		this.loadStreams();
		this.loadSessions();
		this.loadExamOption();
		// CKEDITOR.plugins.addExternal('ckeditor_wiris', 'https://www.wiris.net/demo/plugins/ckeditor/', 'plugin.js');
	}
	ngAfterViewInit(){
		var mathFieldSpan = document.getElementById('math-field');
		var latexSpan = document.getElementById('latex');
		var MQ=(window as any).MathQuill.getInterface(2);
		var mathField = MQ.MathField(mathFieldSpan, {
			spaceBehavesLikeTab: true,
			handlers: {
				edit: function() {
					latexSpan.textContent = mathField.latex();
				}
			}
		});
	}

	insertCheckbxValue(event){
		if(event.target.checked){
			this.answers.push(event.target.value);
		}else{
			let index = this.answers.indexOf(event.target.value);
			this.answers.splice(index,1);
		}
	}
	mtqAnsChange(event,option){
		if(event.target.checked){
			if(!this.mtqAns[option]){
				this.mtqAns[option]=[];
			}
			this.mtqAns[option].push(event.target.value);
		}else{
			if(this.mtqAns[option]){
				let index = this.mtqAns[option].indexOf(event.target.value);
				this.mtqAns[option].splice(index,1);
			}
		}
	}
	addQuestion(variant?:any): void {
		let data:any={};
		data=this.quesForm.value;
		
		if(data.qType == '' || data.qType == 'null'){
			return this.notifier.alert('Danger', 'Question Type is Mandatory', 'danger', 5000 );
		}
		if(this.qsetLang && this.qsetLang != data.defaultLang){
			return this.notifier.alert('Danger', 'Default Language must be same with Qset Language', 'danger', 5000 )
		}

		if(data.qType == 'MAQ' && this.answers.length > 0){
			data.ans=this.answers;
		}
		else if(data.qType == 'MTQ' && !data.ans){
			data.ans='';
			for(let key in this.mtqAns){
				if(this.mtqAns[key].length>0){
					let ans = key+'-';
					ans+= this.mtqAns[key].sort().join('');
					data.ans+= ans+',';
				}
			}
			data.ans=data.ans.slice(0, -1);
		}
		if(data.qType!="STQ" && data.qType!="FUQ"){
			if(data.ans == ''|| !data.ans){
				return this.notifier.alert('Danger', 'Answer is Mandatory', 'danger', 5000 );
			}
		}
		if(!data.title){
			let content = $(data.qContent).text();
			data.title = content.substr(0, 50);
		}
		if(this.addedPassage && this.addedPassage!=''){
			data.havePassage=true;
			data.passage=this.addedPassage;
		}else{
			data.havePassage=false;
			data.passage=null;
		}
		if(data.mn!=''){
			data.mn = parseFloat(data.mn);
		}
		if(this.qsetId){
			data.linkCount = 1;
		}
		this.quesForm.patchValue(data);
		this.formStatus="Processing";
		this.questionService.addQuestion(data).subscribe(
			res=>{
				this.questionId = res.id;
				if(this.qsetId){
					let qData = res;
					this.questionService.maxSequenceQuestion(this.qsetId).subscribe(
						res=>{
							if(res != null && res != ''){
								this.questionSeqId = res[0].questionSeqId;
								this.questionSeqId +=1;
							}else{this.questionSeqId = 1;}
							this.qsetQuestion={
								"question":this.questionId,
								"questionSeqId":this.questionSeqId,
								"qset":this.qsetId,
								"mp" : qData.mp,
								"mn" : qData.mn,
							}
							this.qsetService.addQsetQuestion(this.qsetQuestion).subscribe(
								res=>{
									this.notifier.alert('Success', 'Question Added Successfully', 'success', 5000 );
								},
								err=>{
									this.notifier.alert(err.code, err.message, 'danger', 5000 );
								}
							);
						}
					);
				}
				if(variant){
					this.variantEnable = true;
				}else{
					this.resetQuesForm();
				}
				this.formStatus="Normal";
				this.notifier.alert('Success', ' Question Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
	variantQuestion(){
		if(this.qsetLang && this.qsetLang != this.quesForm.value.defaultLang){
			return this.notifier.alert('Danger', 'Default Language must be same with Qset Language', 'danger', 5000 )
		}
		this.addQuestion(true);
		// this.variantEnable = true;
	}
	addVariantQuestion(){
		this.formStatus = "show";
		this.quesForm.value.isVariant = true;
		this.quesForm.value.variant = this.questionId;
		if(this.addedPassage){
			this.quesForm.value.havePassage=true;
			this.quesForm.value.passage=this.addedPassage;
		}
		if(!this.quesForm.value.title){
			let content = $(this.quesForm.value.qContent).text();
			this.quesForm.value.title = content.substr(0, 50);
		}
		this.questionService.addVariantQuestion(this.quesForm.value,this.qsetId).subscribe(
			(res)=>{
				this.formStatus="none";
				this.variantEnable = false;
				this.resetQuesForm();
				this.notifier.alert('Success', 'Variant Question Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
	resetQuesForm(){
		this.quesForm.controls['qContent'].reset();
		this.quesForm.controls['qSolution'].reset();
		if(this.quesForm.value.qType !='ARQ'){
			this.quesForm.controls['qOption'].reset();
		}
		// this.quesForm.controls['defaultLang'].reset();
		this.quesForm.controls['title'].reset();
		if(!this.variantEnable){
			this.quesForm.controls['ans'].reset();
		}
		this.answers=[];
		this.mtqAns={A : [],B : [],C : [],D : [],E : []};
	}
	loadCourses(){
		this.frmLoader="show";
		let filter={};
		if(this.courseId){
			filter ={id : this.courseId};
		}
		else{
			filter ={option:'COURSE'};
		}
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				this.frmLoader="none";
				if(!res[0]){
					this.courses=[res];
					this.quesForm.patchValue({course : this.courses[0].id});
				}else{
					this.courses=res;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
				if(this.streamId && this.streams.length >0){
					let newStream = [];
					for(var i=0;i<this.streams.length;i++){
						for(var j=0;j<this.streamId.length;j++){
							if( this.streams[i].id == this.streamId[j]){
								newStream.push(this.streams[i]);
							}
						}
					}
					this.streams = newStream;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadSubjects(filter){
		this.subjectService.getSubject(filter).subscribe(
			(res)=>{
				this.frmLoader="none";
				this.subjects=res;
			},
			(err)=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
	}
	loadSubjectUnit(subjectid){
		this.subjectService.getUnit(subjectid).subscribe(
			(res)=>{
				this.frmLoader="none";
				this.subjectUnit=res;
			},
			(err)=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
	}
	loadTopics(filter){
		this.topicService.getTopic(filter).subscribe(
			(res)=>{
				this.frmLoader="none";
				this.topics=res;
			},
			(err)=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
	}
  	setSubjects(streamId){
		let filter:any={};
		filter.stream=streamId;
		this.loadSubjects(filter);

	}
	setUnits(subjectId){
		this.loadSubjectUnit(subjectId);
		// this.loadTopics(subjectId);
	}
	setTopics(unitId){
		let filter:any={};
		filter.unit=unitId;
		this.loadTopics(filter);
	}

	loadPassages(){
		this.passageService.getPassage({limit : 'all'}).subscribe(
			(res)=>{
				this.passages=res;
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 10000);
			}
		);
	}
	loadSessions(){
		this.frmLoader="show";
		this.sessionService.getSession({status:true}).subscribe(
			res=>{
				this.frmLoader="none";
				for (let index = 0; index < res.length; index++) {
					let obj= {label: res[index].name, value: res[index].id};
					this.sessions.push(obj);
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
	}
	setPassageBox(el){
		if(el.target.checked){
			this.passageBoxStatus=true;
		}else{
			this.passageBoxStatus=false;
			this.addedPassage='';
		}
	}
	addPassage(id){
		this.addedPassage = id.trim();
	}
	removePassage(){
		this.addedPassage=null;
		this.quesForm.patchValue({havePassage : false,passage:null});
	}
	addNewPassage(): void {
		this.formStatus="Processing";
		let data:any={};
		data=this.passageForm.value;
		this.passageService.addPassage(data).subscribe(
			res=>{
				this.passageForm.reset();
				this.displayPassage = false;
				this.addedPassage = res.id;
				this.passages.push(res);
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
	addCode(){
		this.formStatus="Processing";
		this.questionService.getQCode(this.codeForm.value).subscribe(
			res=>{
				this.qCode=res;
				this.setCode();
				this.showCodeModal=false;
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Codes Added', 'success', 1000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	setCode(){
		let frmData = this.quesForm.value;
		frmData.stream=this.qCode.alpha.stream;
		frmData.subject=this.qCode.alpha.subject;
		frmData.unit=this.qCode.alpha.unit;
		frmData.topic=this.qCode.alpha.topic;
		frmData.course=this.qCode.beta.course;
		frmData.qType=this.qCode.beta.qType;
		frmData.extraQType = this.qCode.beta.extraQType;
		frmData.level=this.qCode.gamma.level;
		frmData.alpha=this.qCode.alpha.id;
		frmData.beta=this.qCode.beta.id;
		frmData.gamma=this.qCode.gamma.id;
		this.quesForm.patchValue(frmData);
		this.setQueryParams(frmData.qType);
	}
	showDialog() {
        this.displayPassage = true;
    }
	setQueryParams(qType:string){
		if(this.qsetId){
			this.router.navigate(['/'+this.routes+'/qbank/question/add/'], { queryParams: {qsetId: this.qsetId, lang:this.qsetLang, questionType: qType }});
		}else if(this.passageId){
			this.router.navigate(['/'+this.routes+'/qbank/question/add/'], { queryParams: {passageId: this.passageId, lang:this.qsetLang, questionType: qType }});
		}else{
			this.router.navigate(['/'+this.routes+'/qbank/question/add/'], { queryParams: {questionType: qType }});
		}
		this.answers=[];
		let data = this.quesForm.value;
		if(qType == "ARQ"){
			data.qOption.optionA = "Both A & R are true and R is the correct explanation of A";
			data.qOption.optionB = "Both A & R are true but R is NOT the correct explanation of A";
			data.qOption.optionC = "A is true but R is false";
			data.qOption.optionD = "A is false but R is true";
			this.quesForm.patchValue(data);
		}
		else{
			this.quesForm.controls['qOption'].reset();
		}
	}
	loadExamOption() {
        this.frmLoader = "show";
        this.optionService.getOption({ name: "EXAM", limit:120, sort:'createdAt DESC' }).subscribe(
            (data) => {
                this.examList = data;
                this.frmLoader = "none";
            },
            (err) => {
                this.frmLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
	refershExam(){
        this.loadExamOption();
	}
	loadSourceOption() {
        // this.frmLoader = "show";
        this.optionService.getOption({ name: "SOURCE", limit:120, sort:'createdAt DESC'}).subscribe(
            (data) => {
				this.sourceList = data;
                // this.frmLoader = "none";
            },
            (err) => {
                // this.frmLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
	}
	refershSource(){
        this.loadSourceOption();
	}
}
