import { Component, OnInit, Input  } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Coption } from '../../../models/coption.model';
import { Subject } from '../../../models/subject.model';
import { Subjectunit } from '../../../models/subjectunit.model';
import { Topic } from '../../../models/topic.model';
import { QuestionService } from '../../../services/question.service';
import { NotifierService } from '../../notifier/notifier.service';
import { CoptionService } from '../../../services/coption.service';
import { SubjectService } from '../../../services/subject.service';
import { TopicService } from '../../../services/topic.service';
import { QsetService } from '../../../services/qset.service';
import { AuthService } from '../../../services/auth.service';

@Component({
    selector:'ek-question-list',
    templateUrl:'./list.html',
})
export class QuestionListContent implements OnInit{ 
    rows;
    columns; 
    courses: any;
    streams:Coption[];
    subjects: Subject[];
    subjectUnit: Subjectunit[];
    topics: Topic[];
    filter : any={};
    totalRecords;
    countBase:number=1;
    panelLoader="none";
    searchForm:FormGroup;
    questionLevel = [null,"Easy","Average","Medium","Difficult","Very Difficult","Tough"];
    createdBy : any=[];
    linkQsets : any=[];
    user : any={};
    showLinkedQs: boolean= false;
    @Input() routes:string;
    @Input() courseId : string;
	@Input() streamId : any;
    constructor(
        private questionService:QuestionService,
        private notifier: NotifierService,
        private fb:FormBuilder,
        private qsetService : QsetService,
        private coptionService: CoptionService,
        private subjectService: SubjectService,
        private topicService: TopicService,
        private router : Router,
        private activatedRoute : ActivatedRoute,
        private authService : AuthService,
        ){}

    ngOnInit(): void{
        this.user= this.authService.user();
        this.searchForm = this.fb.group({
            title:[''],
            qType:[''],
            extraQType : [''],
            stream:[''],
            course:[''],
            status:[''],
            level:[''],
            linkCount : [''],
            subject:[''],
            createdBy : [''],
            topic:[''],
            stage : ['']
        })
        if(this.courseId){
            this.filter.course= this.courseId;
            this.searchForm.patchValue({course : this.courseId});
        }else{
            this.loadCourses();
        }
        if(this.streamId){
            this.filter.stream= this.streamId;
        }
        
        this.loadStreams();
        let filter:any={};
        this.loadTopics(filter);
        this.loadSubjects(filter);
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            if(params['alpha']){
               this.filter.alpha = params['alpha'];
            }
            if(params['beta']){
                this.filter.beta = params['beta'];
            }
            if(params['gamma']){
                this.filter.gamma = params['gamma'];
            }
        });
       if(this.routes == 'ims' && this.user.type != 'STAFF'){
        this.loadCreatedBy();
       }
        this.loadQuestions(this.filter);
    }
    loadQuestions(filter?:any):void{
        this.panelLoader="show";
        if(this.user.type == 'STAFF'){
            filter.createdBy = this.user.id;
        }
        this.questionService.getQuestion(filter,true).subscribe(
            (data) =>{ 
                this.totalRecords = data.headers.get('totalRecords') || 0;
                this.rows=data.body;
                this.panelLoader="none";
            },
            (err) => {
                this.panelLoader="none";
                console.log(err)
            }
        );
    }
    deleteQuestion(question){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete "+question.title+" ?");
        if(!confirm){
            this.panelLoader="none";
            return;
        }
        this.questionService.deleteQuestion(question.id).subscribe(
            (res)=>{
                this.rows.splice(this.rows.indexOf(question), 1);
                this.panelLoader="none";
                this.notifier.alert('Success', 'deleted Successfully', 'success', 1000 );
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    printQuestion(){
        this.router.navigate(['/'+this.routes+'/qbank/question/print'],{queryParams: this.filter});
    }
    searchData(){
        if (!this.searchForm.valid) {
            return;
        }
        let filter = Object.assign(this.filter, this.searchForm.value);
        this.loadQuestions(filter);
    }
    loadCourses(){
		this.panelLoader="show";
		let filter={};
		if(this.courseId){
			filter ={id : this.courseId}; 
		}
		else{
			filter ={option:'COURSE'};
		}
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				this.panelLoader="none";
				if(!res[0]){
					this.courses=[res];
                    this.searchForm.patchValue({course : this.courses[0].id});
                    
				}else{
					this.courses=res;
				}
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.panelLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.panelLoader="none";
				this.streams=res;
				if(this.streamId && this.streams.length >0){
					let newStream = [];
					for(var i=0;i<this.streams.length;i++){
						for(var j=0;j<this.streamId.length;j++){
							if( this.streams[i].id == this.streamId[j]){
								newStream.push(this.streams[i]);
							}
						}
					}
					this.streams = newStream;
				}
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
    loadSubjects(streamId) {
        this.panelLoader="show";
        this.subjectService.getSubject(streamId).subscribe(
            (res) => {
                this.panelLoader = "none";
                this.subjects = res;
            },
            (err) => { 
                this.panelLoader = "none"; 
                this.notifier.alert(err.code, err.message, 'danger', 5000); 
            }
        )
    }
     
    loadTopics(subjectId) {
        this.panelLoader="show";
        this.topicService.getTopic(subjectId).subscribe(
            (res) => {
                this.panelLoader = "none";
                this.topics = res;
            },
            (err) => { 
                this.panelLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
    loadCreatedBy(){
        this.panelLoader="show";
        this.questionService.getUserCreatedBy().subscribe(
            (res) => {
                this.createdBy = res;
                this.panelLoader="none";
            },
            (err) => {
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000); }
        )
    }

    setSubjects(streamId) {
        let filter: any = {};
        filter.stream = streamId;
        this.loadSubjects(filter);

    }
     
    setTopics(subjectId) {
        let filter: any = {};
        filter.subject = subjectId;
        this.loadTopics(filter);
    }
    paginate(e) {
        if (!this.filter) { 
            this.filter = {};
        }
        this.countBase = e.rows * e.page + 1;
        this.filter.page = (e.page + 1);
        this.filter.limit = e.rows;
        this.loadQuestions(this.filter);
    }
    qLink(qId){
        this.qsetService.linkedQsets(qId).subscribe(
            res=>{
                this.linkQsets = res;
                this.showLinkedQs=true;
            },
            err=>{
                this.notifier.alert(err.code,err.message,'danger',1000);
            }
        );
    }
    sortBy(value){
        this.filter.sortBy = value;
        this.loadQuestions(this.filter);
    }
}
