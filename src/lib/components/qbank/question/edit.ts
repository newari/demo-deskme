import { Component, OnInit, Input, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, RequiredValidator } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Passage } from '../../../models/passage.model';
import { Coption } from '../../../models/coption.model';
import { Subject } from '../../../models/subject.model';
import { Subjectunit } from '../../../models/subjectunit.model';
import { Topic } from '../../../models/topic.model';
import { QuestionService } from '../../../services/question.service';
import { NotifierService } from '../../notifier/notifier.service';
import { CoptionService } from '../../../services/coption.service';
import { SubjectService } from '../../../services/subject.service';
import { TopicService } from '../../../services/topic.service';
import { PassageService } from '../../../services/passage.service';
import { AuthService } from '../../../services/auth.service';
import { Session } from '../../../models/session.model';
import { SessionService } from '../../../services/session.service';
import { OptionService } from '../../../services/option.service';
import { Option } from '../../../models/option.model';
import { ClientService } from '../../../services/client.service';

@Component({
    selector:'ek-question-edit',
    templateUrl:'./edit.html'
})
export class QuestionEditContent implements OnInit,AfterViewInit{
	quesForm:FormGroup;
	passageForm : FormGroup;
	question;
	questionType;
	questionId;
	passageQuestion;
	answers =[];
	options = ["A","B","C","D","E"];
	courses:any;
	streams:Coption[];
	subjects:Subject[];
	subjectUnit:Subjectunit[];
	topics:Topic[];
	passages:Passage[];
	sessions =[];
	addedPassage:any;
	passageBoxStatus:boolean=false;
	frmLoader="none";
	formStatus="Normal";
	panelLoader="none";
	qCode;
	user;
	showCodeModal : boolean=false;
	codeForm : FormGroup;
	variantEnable : boolean=false;
	variantEvent="none";
	qsetLang;
	qsetId;
	examList:Option[];
	sourceList : Option[];
	isClientSeller : boolean=false;
	displayPassage : boolean= false;
	mtqAns : any={};
	@Input() routes : string;
	@Input() courseId : string;
	@Input() streamId : any;
	constructor(
		private fb:FormBuilder,
		private activatedRoute: ActivatedRoute,
		private questionService:QuestionService,
		private coptionService:CoptionService,
		private optionService : OptionService,
		private subjectService:SubjectService,
		private topicService:TopicService,
		private passageService:PassageService,
		private sessionService : SessionService,
		private router:Router,
		private authService : AuthService,
		private clientService : ClientService,
		private notifier: NotifierService){ 
	}
	ngOnInit(): void {
		this.user=this.authService.user();
		if(this.user){
			this.clientService.getClientConfig({client : this.user.client, allowedApps:'303030303030307162616e6b'}).subscribe(
				res=>{
				this.isClientSeller = res.isESSeller;
				},err=>{this.notifier.alert(err.code,err.message,'danger',1000)}
			);
		}
		this.quesForm=this.fb.group({
			title:[''],
			qContent:[''],
			qOption:this.fb.group({
				optionA:[''],
				optionB:[''],
				optionC:[''],
				optionD:[''],
				optionE:[''],
				optionP:[''],
				optionQ:[''],
				optionR:[''],
				optionS:[''],
				optionT:[''],
			}),
			ans:[''],
			qSolution:[''],
			stream:['',Validators.required],
			course:['',Validators.required],
			duration:[],
			mp:[],
			mn:[],
			qType:[''],
			subject:[''],
			topic:[''],
			unit:[''],
			tags:[],
			exam:[''],
			alpha:[''],
			beta:[''],
			gamma:[''],
			partialMark : [''],
			defaultLang : ['',Validators.required],
			defaultFont:[''],
			status:['',Validators.required],
			level:[],
			havePassage:[''],
			passage:[''],
			linkCount:[],
			source:[''],
			session:[],
			isVariant : [],
			variant:[],
			stage : [''],
			forSell:[false],
			price:[],
			createdAt : [this.user.id]
		});
		this.passageForm=this.fb.group({
			title:['', Validators.required],
			passageContent:['', Validators.required],
			status:[true,Validators.required]
		});
		this.codeForm = this.fb.group({
			alpha : ['',Validators.required],
			beta : ['',Validators.required],
			gamma : ['',Validators.required]
		});
		this.loadCourses();
		this.loadStreams();
		this.loadSessions();
		this.loadExamOption();
		this.activatedRoute.params.subscribe((params: Params) => {
	        this.questionId = params['id'];
			this.getQuestion();
		}); 
		this.activatedRoute.queryParams.subscribe(params => {      
			this.questionType = params['questionType'];
			this.qsetId = params['qsetId'];
			this.qsetLang = params['lang'];
			if(!this.questionType){
				this.quesForm.patchValue({qType:'null'});				
			}
			else{
				this.quesForm.patchValue({qType:this.questionType});	
			}					
		});
	}
	ngAfterViewInit(){
		var mathFieldSpan = document.getElementById('math-field');
		var latexSpan = document.getElementById('latex');
		var MQ=(window as any).MathQuill.getInterface(2);
		var mathField = MQ.MathField(mathFieldSpan, {
			spaceBehavesLikeTab: true, 
			handlers: {
				edit: function() { 
					latexSpan.textContent = mathField.latex(); 
				}
			}
		});
	}
	insertCheckbxValue(event){		
		if(event.target.checked){
			this.answers.push(event.target.value);
		}else{
			let index = this.answers.indexOf(event.target.value);
			this.answers.splice(index,1);
		}
	}
	mtqAnsChange(event,option){
		if(event.target.checked){
			if(!this.mtqAns[option]){
				this.mtqAns[option]=[];
			}
			this.mtqAns[option].push(event.target.value);
		}else{
			if(this.mtqAns[option]){
				let index = this.mtqAns[option].indexOf(event.target.value);
				this.mtqAns[option].splice(index,1);
			}
		}
	}
	getQuestion(){
		this.panelLoader="show";
		this.questionService.getOneQuestion(this.questionId).subscribe(
			(res)=>{
				this.question=res;
				this.questionType=this.question.qType;
				let code : any ={};
				if(this.question.ans){
					if(this.questionType == 'MAQ'){
						var ans=this.question.ans.split(',');
						this.answers = ans;
					}
					else if(this.questionType == 'MTQ'){
						var ans=this.question.ans.split(',');
						for (let i = 0; i < ans.length; i++) {
							let element = ans[i];
							element = element.split('-');
							if(element[0]&&element[1]){
								this.mtqAns[element[0]]= element[1].split('');
							}
						}
					}
				}
				if(res.course&&res.course.id){
					this.question.course=res.course.id;
				}
				if(res.stream&&res.stream.id){
					this.question.stream=res.stream.id;
					this.setSubjects(this.question.stream);
				}

				if(res.subject&&res.subject.id){
					this.question.subject=res.subject.id;
					this.setUnits(this.question.subject);
				}
				
				if(res.unit&&res.unit.id){
					this.question.unit=res.unit.id;
					this.setTopics(this.question.unit);
				}
				if(res.topic&&res.topic.id){
					this.question.topic=res.topic.id;
				}
				if(res.exam&&res.exam.id){
					this.question.exam=res.exam.id;
				}
				if(res.source&&res.source.id){
					this.question.source=res.source.id;
					this.loadSourceOption();
				}
				if(res.isVariant && res.variant){
					this.question.variant = res.variant.id;
				}
				if(res.alpha&&res.alpha.id){
					code.alpha=res.alpha;
					this.question.alpha=res.alpha.id;
				}
				if(res.beta&&res.beta.id){
					code.beta=res.beta;
					this.question.beta=res.beta.id;
				}
				if(res.gamma&&res.gamma.id){
					code.gamma=res.gamma;
					this.question.gamma=res.gamma.id;
				}
				if(this.question.passage != null){
					this.addedPassage = this.question.passage.id;
				}
				// if(res.session&&res.session.id){
				// 	this.question.session=res.session.id;
				// }
				
				this.quesForm.patchValue(this.question);
				if(code.alpha || code.beta|| code.gamma  ){
					this.qCode=code;
				}
				this.panelLoader="none";
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		)
	}
	variantQuestion(event){
		if(this.qsetLang && this.qsetLang != this.quesForm.value.defaultLang){
			return this.notifier.alert('Danger', 'Default Language must be same with Qset Language', 'danger', 5000 )
		}
		this.variantEvent= event;
		this.updateQuestion();
		this.variantEnable=true;
	}
	getVariantQuestion(){
		this.panelLoader="show";
		this.questionService.getOneQuestion(this.quesForm.value.variant).subscribe(
			(res)=>{
				let data :any={
					qContent : res.qContent,
					qOption : res.qOption,
					qSolution : res.qSolution,
					title : res.title,
					variant : res.variant.id,
					isVariant : res.isVariant,
					defaultLang : res.defaultLang
				};
				if(res.havePassage){
					data.havePassage=res.havePassage;
					data.passageContent = res.passage.id;
					this.addedPassage= res.passage.id;
				}
				this.addedPassage = this.question.passage.id;
				this.questionId = res.id;
				this.quesForm.patchValue(data);
				this.panelLoader="none";
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		)
	}
	resetQuesForm(){
		this.quesForm.controls['qContent'].reset();
		this.quesForm.controls['qSolution'].reset();
		this.quesForm.controls['qOption'].reset();
		this.quesForm.controls['title'].reset();
		// this.quesForm.controls['defaultLang'].reset();
	}
	updateQuestion(): void {
		// if(!this.quesForm.dirty){
		// 	return;
		// }
		this.question=this.quesForm.value;
		if(this.qsetLang && this.qsetLang != this.question.defaultLang){
			return this.notifier.alert('Danger', 'Default Language must be same with Qset Language', 'danger', 5000 )
		}
		this.formStatus="Processing";
		if(this.question.qType == 'MAQ' && this.answers.length > 0){
			this.question.ans=this.answers;
		}
		else if(this.question.qType == 'MTQ'){
			this.question.ans='';
			for(let key in this.mtqAns){
				if(this.mtqAns[key].length>0){
					let ans = key+'-';
					ans+= this.mtqAns[key].sort().join('');
					this.question.ans+= ans+',';
				}
			}
			this.question.ans=this.question.ans.slice(0, -1);
		}
		delete this.question.answers;
		if(this.addedPassage){
			this.question.havePassage=true;
			this.question.passage=this.addedPassage;
		}else{
			this.question.havePassage=false;
			this.question.passage=null;
		}
		this.questionService.updateQuestion(this.questionId, this.question).subscribe(
			(res)=>{			
				this.formStatus="Normal";
				if(this.variantEvent == 'EDIT'){
					this.getVariantQuestion();
				}else if(this.variantEvent == 'ADD'){
					this.resetQuesForm();
				}
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
	updateVariantQuestion(): void {
		if(!this.quesForm.dirty){
			return;
		}
		this.formStatus="Processing";
		if(this.addedPassage){
			this.quesForm.value.havePassage=true;
			this.quesForm.value.passage=this.addedPassage;
		}
		if(this.variantEvent == 'EDIT'){
			this.questionService.updateVariantQuestion(this.questionId,this.quesForm.value).subscribe(
				(res)=>{		
					this.formStatus="Normal";
					this.notifier.alert('Success', 'Variant Question Saved Successfully', 'success', 5000 );
				},
				(err)=>{
					this.notifier.alert(err.code, err.message, 'danger', 5000 );
					this.formStatus="Normal";
				}
			);
		}
		else{
			this.quesForm.value.isVariant=true;
			this.quesForm.value.variant=this.questionId;
			this.questionService.addVariantQuestion(this.quesForm.value,this.qsetId).subscribe(
				(res)=>{			
					this.formStatus="Normal";
					this.notifier.alert('Success', 'Variant Question Added Successfully', 'success', 5000 );
				},
				(err)=>{
					this.notifier.alert(err.code, err.message, 'danger', 5000 );
					this.formStatus="Normal";
				}
			);
		}
	}
	loadCourses(){
		this.frmLoader="show";
		let filter={};
		if(this.courseId){
			filter ={id : this.courseId}; 
		}
		else{
			filter ={option:'COURSE'};
		}
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				this.frmLoader="none";
				if(!res[0]){
					this.courses=[res];
					this.quesForm.patchValue({course : this.courses[0].id});
				}else{
					this.courses=res;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
				if(this.streamId && this.streams.length >0){
					let newStream = [];
					for(var i=0;i<this.streams.length;i++){
						for(var j=0;j<this.streamId.length;j++){
							if( this.streams[i].id == this.streamId[j]){
								newStream.push(this.streams[i]);
							}
						}
					}
					this.streams = newStream;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadSubjects(filter){
		this.subjectService.getSubject(filter).subscribe(
			(res)=>{
				this.frmLoader="none";
				this.subjects=res;
			},
			(err)=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
	}
	loadSubjectUnit(subjectid){
		this.subjectService.getUnit(subjectid).subscribe(
			(res)=>{
				this.frmLoader="none";
				this.subjectUnit=res;
			},
			(err)=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
	}
	loadTopics(filter){
		this.topicService.getTopic(filter).subscribe(
			(res)=>{
				this.frmLoader="none";
				this.topics=res;
			},
			(err)=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
	}
	loadSessions(){
		this.frmLoader="show";
		this.sessionService.getSession({status:true}).subscribe(
			res=>{
				this.frmLoader="none";
				for (let index = 0; index < res.length; index++) {
					let obj= {label: res[index].name, value: res[index].id};
					this.sessions.push(obj);
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
	}
	addCode(){
		this.formStatus="Processing";
		this.questionService.getQCode(this.codeForm.value).subscribe(
			res=>{
				this.qCode=res;
				this.setCode();
				this.showCodeModal=false;
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Codes Added', 'success', 1000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
	setCode(){
		let frmData = this.quesForm.value;
		frmData.stream=this.qCode.alpha.stream;
		frmData.subject=this.qCode.alpha.subject;
		frmData.unit=this.qCode.alpha.unit;
		frmData.topic=this.qCode.alpha.topic;
		frmData.course=this.qCode.beta.course;
		frmData.qType=this.qCode.beta.qType;
		frmData.extraQType = this.qCode.beta.extraQType;
		frmData.level=this.qCode.gamma.level;
		frmData.alpha=this.qCode.alpha.id;
		frmData.beta=this.qCode.beta.id;
		frmData.gamma=this.qCode.gamma.id;
		this.quesForm.patchValue(frmData);
		this.setQueryParams(frmData.qType);
	}
	
	loadPassages(){
		this.passageService.getPassage({limit : 'all'}).subscribe(
			(res)=>{
				this.passages=res;
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 10000);
			}
		);
	}
	setPassageBox(el){
		if(el.target.checked){
			this.passageBoxStatus=true;
			// this.loadPassages();
		}else{
			this.passageBoxStatus=false;
		}
	}
	removePassage(){
		this.addedPassage=null;
		this.quesForm.patchValue({havePassage : false,passage:null});
	}
	setSubjects(streamId){
		let filter:any={};
		filter.stream=streamId;
		this.loadSubjects(filter);
	}
	setUnits(subjectId){		 
		this.loadSubjectUnit(subjectId);
	}
	setTopics(unitId){
		let filter:any={};
		filter.unit=unitId;
		this.loadTopics(filter);
	}
	setQueryParams(qType:string){
		this.router.navigate(['/'+this.routes+'/qbank/question/edit/'+this.questionId], { queryParams: {questionType: qType }});
		this.answers=[];
		
	}	
	loadExamOption() {
        this.frmLoader = "show";
        this.optionService.getOption({ name: "EXAM", limit:120, sort:'createdAt DESC' }).subscribe(
            (data) => {
                this.examList = data;
                this.frmLoader = "none";
            },
            (err) => {
                this.frmLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
	refershExam(){
        this.loadExamOption();
	}
	loadSourceOption() {
        // this.frmLoader = "show";
        this.optionService.getOption({ name: "SOURCE", limit:120, sort:'createdAt DESC' }).subscribe(
            (data) => {
				this.sourceList = data;
                // this.frmLoader = "none";
            },
            (err) => {
                // this.frmLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
	}
	refershSource(){
        this.loadSourceOption();
	}
	addPassage(id){
		this.addedPassage = id.trim();
	}
	addNewPassage(): void {
		this.formStatus="Processing";
		let data:any={};
		data=this.passageForm.value;
		this.passageService.addPassage(data).subscribe(
			res=>{
				this.passageForm.reset();
				this.displayPassage = false;
				this.addedPassage = res.id;
				// this.passages.push(res);
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
}
