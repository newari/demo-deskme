import { Component, OnInit, Input  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QuestionService } from '../../../services/question.service';

@Component({
    selector:'ek-question-print',
    templateUrl:'./print.html'
})
export class QuestionPrintContent implements OnInit{ 
    rows;
    activeFilter;
    totalRecords;
    panelLoader="none";
    questionLevel = [null,"Easy","Average","Medium","Difficult","Very Difficult","Tough"];
    @Input() routes:string;
    constructor(
        private questionService:QuestionService,
        private activatedRoute : ActivatedRoute
    ){}
    ngOnInit(): void{
        let filter : any={};
        this.activatedRoute.queryParams.subscribe(params=>{
            if(params['alpha']){
                filter.alpha= params['alpha'];
            }
            if(params['beta']){
                filter.beta= params['beta'];
            }
            if(params['gamma']){
                filter.gamma= params['gamma'];
            }
            if(params['title']){
                filter.title= params['title'];
            }
            if(params['course']){
                filter.course= params['course'];
            }
            if(params['stream']){
                filter.stream= params['stream'];
            }
            if(params['type']){
                filter.type= params['type'];
            }
            if(params['subject']){
                filter.subject= params['subject'];
            }
            if(params['topic']){
                filter.topic= params['topic'];
            }
            if(params['level']){
                filter.level= params['level'];
            }
            if(params['status']){
                filter.status= params['status'];
            }
            if(params['qType']){
                filter.qType= params['qType'];
            }
            if(params['page']){
                filter.page= params['page'];
                // filter.limit= params['limit'];
            }
            if(params['limit']){
                filter.limit= params['limit'];
            }
        });
        this.loadQuestions(filter);
    }
    
    loadQuestions(filter?:any):void{
        this.panelLoader="show";
        filter.populatePassage = true;
        this.questionService.getPrintQuestions(filter,true).subscribe(
            (data) =>{ 
                this.rows=data.body;
                this.panelLoader="none";
                this.loadMathJax();
            },
            (err) => {
                console.log(err);
                this.panelLoader="none";
            }
        );
    }
    loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub]);
        }, 10);
    }
}
