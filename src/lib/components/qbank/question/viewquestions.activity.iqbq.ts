import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PaginatorModule, DialogModule, CalendarModule, SplitButtonModule } from 'primeng/primeng';
import { FullViewQuestionsContent } from './viewquestions';
import { CoptionService } from '../../../services/coption.service';
import { QuestionService } from '../../../services/question.service';
import { SubjectService } from '../../../services/subject.service';
import { TopicService } from '../../../services/topic.service';
import { QsetService } from '../../../services/qset.service';
import { SessionService } from '../../../services/session.service';
import { OptionService } from '../../../services/option.service';
import { EkDropdownModule } from '../../dropdown/dropdown.module';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';

@NgModule({
    declarations:[FullViewQuestionsContent],
    imports: [CommonModule, RouterModule, ReactiveFormsModule, FormsModule, PaginatorModule,EkDropdownModule,DialogModule,SafeHtmlPipeModule,CKEditor4Module,CalendarModule,SplitButtonModule], 
    providers:[QuestionService,CoptionService,SubjectService,TopicService,QsetService,SessionService,OptionService],
    exports:[FullViewQuestionsContent]
})
export class FullViewQuestionsModule{}
