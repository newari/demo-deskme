import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QuestionAddContent } from './add';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DialogModule, ChipsModule, MultiSelectModule } from 'primeng/primeng';
import { QsetService } from '../../../services/qset.service';
import { QuestionService } from '../../../services/question.service';
import { CoptionService } from '../../../services/coption.service';
import { SubjectService } from '../../../services/subject.service';
import { TopicService } from '../../../services/topic.service';
import { PassageService } from '../../../services/passage.service';
import { SessionService } from '../../../services/session.service';
import { FileinputModule } from '../../filemanager/fileinput.module';
import { OptionService } from '../../../services/option.service';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';
@NgModule({  
    declarations:[QuestionAddContent],
    imports:[RouterModule,CommonModule,DialogModule,CKEditor4Module, FormsModule, ReactiveFormsModule,FileinputModule,ChipsModule,MultiSelectModule],
    providers:[QsetService,QuestionService,CoptionService,SubjectService,TopicService,PassageService,SessionService,OptionService],
    exports:[QuestionAddContent]
})   

export class QuestionAddModule { }

