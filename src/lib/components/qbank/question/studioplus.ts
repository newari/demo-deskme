import { Component, OnInit, Input  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Coption } from '../../../models/coption.model';
import { Subject } from '../../../models/subject.model';
import { Subjectunit } from '../../../models/subjectunit.model';
import { Topic } from '../../../models/topic.model';
import { QuestionService } from '../../../services/question.service';
import { CoptionService } from '../../../services/coption.service';
import { SubjectService } from '../../../services/subject.service';
import { TopicService } from '../../../services/topic.service';
import { QsetService } from '../../../services/qset.service';
import { PassageService } from '../../../services/passage.service';
import { NotifierService } from '../../notifier/notifier.service';
import { SessionService } from '../../../services/session.service';
import { Session } from '../../../models/session.model';
import { OptionService } from '../../../services/option.service';
import { Option } from '../../../models/option.model';
import { ClientService } from '../../../services/client.service';
import { AuthService } from '../../../services/auth.service';
import { SubjectunitService } from '../../../services/subjectunit.service';

@Component({
    selector:'ek-question-studioplus',
    templateUrl:'./studioplus.html'
})
export class QuestionStudioPlusContent implements OnInit{ 
    searchForm:FormGroup;
    codeForm : FormGroup;
    qsetFilterForm : FormGroup;
    rows;
    columns;
    qsetId;
    passageId;
    courses: any;
    qsetCourses : any;
    streams:Coption[];
    qsetStreams:Coption[];
    subjects: Subject[];
    subjectUnit: Subjectunit[];
    topics: Topic[];
    activeFilter : any={};
    totalRecords;
    countBase:number;
    panelLoader="none";
    frmLoader='normal';
    showLinkedQs : boolean=false;
    questionBankList=[];
    questionList=[];
    qbFilterActive;
    examFilterActive;
    qsetQuestion;
    seq;
    qsetTitles;
    passageTitles;
    passageQuestion;
    linkQsets :any=[];
    questionLevel = [null,"Easy","Average","Medium","Difficult","Very Difficult","Tough"];
    lang;
    sessions : Session[];
    createdBy : any=[];
    sources : Option[];
    clients : any=[];
    activeClient : any;
    selectedClient : any;
    user : any={};
    mixedPending : boolean = false;
    @Input() routes:string;
    @Input() courseId : string;
	@Input() streamId : any;
    clientConfigs: any[];
    isFinalised : boolean = false;
    isPurchased : boolean = false;
    isESBuyer : boolean = false;
    constructor(
        private notifier: NotifierService,
        private fb:FormBuilder,
        private questionService:QuestionService,
        private coptionService: CoptionService,
        private subjectService: SubjectService,
        private subjectUnitService : SubjectunitService,
        private topicService: TopicService,
        private activatedRoute: ActivatedRoute,
        private qsetService: QsetService,
        private passageService : PassageService,
        private sessionService : SessionService,
        private optionService : OptionService,
        private clientService : ClientService,
        private authService : AuthService,
        private router : Router
    ){}
    ngOnInit(): void{
        this.user= this.authService.user();
		if(this.user){
            this.activeClient= this.user.client;
            this.selectedClient = this.user.client;
			this.clientService.getClientConfig({client : this.user.client}).subscribe(
				res=>{
                this.isESBuyer = res.isESBuyer;
                if(this.isESBuyer){
                    this.loadClients();
                }
                },err=>{
                    console.log(err);
                    this.notifier.alert(err.code,err.message,'danger',4000);
                }
			);
		}
        this.activatedRoute.queryParams.subscribe(params=>{
            this.qsetId = params['qsetId'];
            this.passageId = params['passageId'];
            this.lang = params['lang'];
        });
        this.searchForm = this.fb.group({
            id:[''],
            title:[''],
            qType:[''],
            stream:[''],
            course:[''],
            status:[''],
            level:[''],
            subject:[''],
            topic:[''],
            unit : [''],
            mn : [''],
            mp : [''],
            defaultLang : [''],
            tags : [''],
            stage : [''],
            source : [''],
            createdBy :[''],
            session : ['']  
        });
        this.codeForm = this.fb.group({
			alpha : [''],
			beta : [''],
            gamma : [''],
            stage : ['']
        });
        this.qsetFilterForm = this.fb.group({
			course : [''],
			stream : [''],
			qset : ['']
        });
        if(this.qsetId){
            this.getOneQset();
            this.loadQsetQuestions();
        }
        if(this.passageId){
            this.loadPassageQuestions();
        }
        this.loadFormData();
    }
    loadFormData(){
        this.loadCourses();
        this.loadStreams();
        this.loadSessions();
        this.loadSubjects();
        this.loadTopics();

        this.loadSubjectUnit();
        if(this.routes == 'ims'){
            this.loadCreatedBy();
            this.loadSourceOption();
        }
        let filter:any={};
        if(this.courseId){
            filter.course=this.courseId;
        }
        this.loadQuestions(filter);
    }
    loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub]);
        }, 10);
    }
    questionBankFilters(value){
        if(value == 'question'){
            this.qbFilterActive = 'question';
            this.searchForm.reset(); 
            if(this.courseId){
                this.searchForm.patchValue({course:this.courseId});
            }
        }
        if(value == 'qset'){
            this.qbFilterActive = 'qset';
        }
        if(value == 'passage'){
            this.qbFilterActive = 'passage';
            this.loadPassage();
        }
        if(value == 'advance'){
            this.qbFilterActive = 'advance';
            this.searchForm.reset(); 
            if(this.courseId){
                this.searchForm.patchValue({course:this.courseId});
            }
        }
        if(value == 'code'){
            this.qbFilterActive = 'code';
        }
        if(value == ''){ 
            this.qbFilterActive = '';
        }
    }
    examFilters(value){
        if(value == 'qset'){
            this.examFilterActive = 'qset';  
        }
        else if(value == 'passage'){
            this.examFilterActive = 'passage';
            this.loadPassage();
        }
        else if(value == 'null'){ 
            this.examFilterActive = null;
        }
    }
    searchQset(qsetId){
        this.qsetId = qsetId;
        this.passageId = null;
        this.loadQsetQuestions();
    }
    searchPassage(passageId){
        this.passageId = passageId;
        this.qsetId = null;
        this.loadPassageQuestions();
    }
    loadQbQsetQuestion(qsetId){
        let data={
            id : qsetId
        };
        this.qsetService.getQsetQuestion(data).subscribe(
            (data) =>{
                this.questionBankList = data;
                this.loadMathJax();
            },
            (err) =>{this.notifier.alert(err.code, err.message, 'danger', 5000 );}
        );
    }
    loadQbPassageFilter(passageId){
        this.passageService.getPassageQuestions(passageId).subscribe(
            (data) =>{ 
            
                this.questionBankList  = data;},
            (err) => { this.notifier.alert(err.code, err.message, 'danger', 5000); }
        );
    }
    loadPassage(){
        this.passageService.getPassage().subscribe(
            (data)=>this.passageTitles = data,
            (err)=>{ this.notifier.alert(err.code, err.message, 'danger', 5000);}
        );
    } 
    loadQuestions(filter?:any):void{
        this.questionBankList=[];
        this.frmLoader="show";
        let filter2 = Object.assign({client:this.selectedClient},filter);
        if(this.user.type == 'STAFF'){
            filter2.createdBy = this.user.id;
        }
        this.questionService.getQuestion(filter2,true).subscribe(
            (data) =>{ 
                this.totalRecords = data.headers.get('totalRecords') || 0;
                this.questionBankList=data.body;
                this.loadMathJax();
                this.frmLoader="none";
            },
            (err) => {console.log(err); this.frmLoader="none";}
        );
    }
    getOneQset(){
        this.qsetService.getOneQset(this.qsetId).subscribe(
            res=>{
                if(res.oShip && res.oShip == '3'){
                    this.isFinalised = true;
                }
                this.isPurchased = res.isPurchased;
            },
            err=>{this.notifier.alert(err.code,err.message,'danger',2000);}
        );
    }
    loadQsetQuestions():void{
        let data={
            id : this.qsetId
        };
        this.qsetService.getQsetQuestion(data).subscribe(
            (data) =>{ 
                this.questionList = data;  
                this.loadMathJax();
            },
            (err) =>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader="deleted";
            }
        );
    }
    loadPassageQuestions():void{
        this.passageService.getPassageQuestions(this.passageId).subscribe(
            (data) =>{ 
                this.questionList  = data;
                this.loadMathJax();
            },
            (err) => console.log(err)
        );
    }
    getCodeQuestions(){
        this.questionService.getQCode(this.codeForm.value).subscribe(
            (data) =>{
                let qCode = data;
                this.activeFilter={};
                if(qCode.alpha){
                    this.activeFilter.alpha = qCode.alpha.id;
                }
                if(qCode.beta){
                    this.activeFilter.beta = qCode.beta.id;
                }
                if(qCode.gamma){
                    this.activeFilter.gamma = qCode.gamma.id;
                }
                this.activeFilter.stage = this.codeForm.value.stage;
                this.loadQuestions(this.activeFilter);
            },
            (err) =>{this.notifier.alert(err.code, err.message, 'danger', 5000 );}
        );
    }
    onMoveToSource(event){
      
        let question = event.items[0];
        if(this.lang && question.defaultLang){
            let count = 0;
            let index = null;
            for(var i= this.questionList.length-1; i >=0; i--){
                let diffLang = false;
                if(this.questionList[i].question){
                    if(question.id == this.questionList[i].question._id){
                        count++;
                    }
                    if(this.lang !== this.questionList[i].question.defaultLang){
                        diffLang = true;
                     }
                }
                else if(this.questionList[i].id){
                    if(question.id == this.questionList[i].id){
                        count++;
                        index= i;
                    }
                    if(this.lang !== this.questionList[i].defaultLang){
                       diffLang = true;
                    }
                } 
                if(diffLang){
                    this.questionBankList.push(this.questionList[i]);
                    this.questionList.splice(i,1);
                    this.notifier.alert('Warning','Question Language is diff of Qset Language','danger',4000);
                    index= null;
                    break;
                }
                if(count >1 && index){
                    this.questionBankList.push(this.questionList[index]);
                    this.questionList.splice(index,1);
                    index= null;
                    this.notifier.alert('Warning',' Question Already Exist !! ','danger',4000);
                    break;
                }
            }
        }
    }
    onMoveToTarget(event){
        let question = event.items[0];
    }
    changePositiveMarks(value,index,id){
        let qData = this.questionList[index];
        if(qData.id == id || qData.question._id == id){
            this.questionList[index].mp = value;
        }
    }
    changeNegativeMarks(value,index,id){
        let qData = this.questionList[index];
        if(qData.id == id || qData.question._id == id){
            this.questionList[index].mn = value;
        }
    }
    saveExamQuestion(){
        if(this.qsetId){
            this.qsetService.updateQbankQuestion(this.qsetId,this.questionList).subscribe(
                (res)=>{
                    this.notifier.alert('Success', 'Question Added Successfully', 'success', 1000 );
                },
                (err)=>{
                    this.notifier.alert(err.code, err.message, 'danger', 5000 );
                }
            );
        }
        // if(this.passageId){
        //     for(var i = 0; i < this.questionList.length; i++){
        //         if(!this.questionList[i].question){
        //             this.passageQuestion={
        //                 "question":this.questionList[i].id,
        //                 "passage":this.passageId
        //              }
        //             this.passageService.addPassageQuestion(this.passageQuestion).subscribe(
        //                 res=>{
        //                 this.notifier.alert('Success', 'Passage Question Added Successfully', 'success', 1000 );
        //                 },
        //                 err=>{
        //                     this.notifier.alert(err.code, err.message, 'danger', 5000 );
        //                 }
        //             );
        //         }                     
        //     }
        // }
        if(this.qsetId == null && this.passageId == null){
            this.notifier.alert('danger','Select Qset or Passage First', 'danger', 5000 );
            this.examFilterActive = 'qset'; 
        }
    }
    deleteQuestion(question){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete "+question.title+"?");
        if(!confirm){
            this.panelLoader="none";
            return;
        }
        this.questionService.deleteQuestion(question.id).subscribe(
            (res)=>{
                this.rows.splice(this.rows.indexOf(question), 1);
                this.panelLoader="none";
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );

            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    searchData(){
        if (!this.searchForm.valid) {
            return;
        }       
        this.activeFilter={};
        if (this.searchForm.value.id && this.searchForm.value.id!='null') {
            this.activeFilter.id = this.searchForm.value.id;
        }
        if (this.searchForm.value.title && this.searchForm.value.title!='null') {
            this.activeFilter.title = this.searchForm.value.title;
        }
        if (this.searchForm.value.course && this.searchForm.value.course != 'null') {
            this.activeFilter.course = this.searchForm.value.course;
        }
        if (this.searchForm.value.stream && this.searchForm.value.stream != 'null') {
            this.activeFilter.stream = this.searchForm.value.stream;
        }
        if (this.searchForm.value.qType && this.searchForm.value.qType != 'null') {
            this.activeFilter.qType = this.searchForm.value.qType;
        }
        if (this.searchForm.value.level && this.searchForm.value.level != 'null') {
            this.activeFilter.level = this.searchForm.value.level;
        }
        if (this.searchForm.value.unit && this.searchForm.value.unit != 'null') {
            this.activeFilter.unit = this.searchForm.value.unit;
        }
        if (this.searchForm.value.topic && this.searchForm.value.topic != 'null') {
            this.activeFilter.topic = this.searchForm.value.topic;
        }
        if (this.searchForm.value.subject && this.searchForm.value.subject != 'null') {
            this.activeFilter.subject = this.searchForm.value.subject;
        }
        if (this.searchForm.value.mp && this.searchForm.value.mp != 'null') {
            this.activeFilter.mp = this.searchForm.value.mp;
        }
        if (this.searchForm.value.mn && this.searchForm.value.mn != 'null') {
            this.activeFilter.mn = this.searchForm.value.mn;
        }
        if (this.searchForm.value.defaultLang && this.searchForm.value.defaultLang != 'null') {
            this.activeFilter.defaultLang = this.searchForm.value.defaultLang;
        }
        if (this.searchForm.value.tags && this.searchForm.value.tags != 'null') {
            this.activeFilter.tags = [this.searchForm.value.tags];
        }
        if (this.searchForm.value.status && this.searchForm.value.status != 'null') {
            this.activeFilter.status = this.searchForm.value.status;
        }
        if (this.searchForm.value.stage && this.searchForm.value.stage != 'null') {
            this.activeFilter.stage = this.searchForm.value.stage;
        }
        if (this.searchForm.value.session && this.searchForm.value.session != 'null') {
            this.activeFilter.session = this.searchForm.value.session;
        }
        if (this.searchForm.value.source && this.searchForm.value.source != 'null') {
            this.activeFilter.source = this.searchForm.value.source;
        }
        if (this.searchForm.value.createdBy && this.searchForm.value.createdBy != 'null') {
            this.activeFilter.createdBy = this.searchForm.value.createdBy;
        }
        this.loadQuestions(this.activeFilter);
    }
    loadSessions(){
		this.frmLoader="show";
		this.sessionService.getSession({status:true}).subscribe(
			res=>{
				this.frmLoader="none";
				this.sessions=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
	}
    loadCourses(){
		this.frmLoader="show";
		let filter:any={client:this.selectedClient};
		if(this.courseId){
			filter ={id : this.courseId}; 
		}
		else{
			filter.option='COURSE';
        }
        this.coptionService.getCoption(filter).subscribe(
			res=>{
                this.frmLoader="none";
                
                let data : any;
                if(!res[0]){
					data=[res];
					this.searchForm.patchValue({course : data[0].id});
				}else{
					data=res;
                }
                if(this.activeClient == this.selectedClient){
                    this.qsetCourses = data;
                }
                
                this.courses=data; 
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
        this.frmLoader="show";
        let filter={
            client:this.selectedClient,
            option : 'STREAM'
        };
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
				if(this.streamId && this.streams.length >0){
                    let newStream = [];
                   
					for(var i=0;i<this.streams.length;i++){
						for(var j=0;j<this.streamId.length;j++){
							if( this.streams[i].id == this.streamId[j]){
								newStream.push(this.streams[i]);
							}
						}
					}
					this.streams = newStream;
                }
                if(this.activeClient == this.selectedClient){
                    this.qsetStreams = this.streams;
                }
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
    loadSubjects(filter?:any) {
        this.frmLoader="show";
        let filter2 = Object.assign({client:this.selectedClient},filter);
        this.subjectService.getSubject(filter2).subscribe(
            (res) => {
                this.frmLoader = "none";
                this.subjects = res;
            },
            (err) => { this.frmLoader="none"; this.notifier.alert(err.code, err.message, 'danger', 5000); }
        )
    }
    loadSubjectUnit(filter?:any){
        this.frmLoader="show";
        let filter2 = Object.assign({client:this.selectedClient},filter);
		this.subjectUnitService.getSubjectunit(filter2).subscribe(
			(res)=>{
				this.frmLoader="none";
				this.subjectUnit=res;
			},
			(err)=>{this.frmLoader="none"; this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
	}
	loadTopics(filter?:any){
        this.frmLoader="show";
        let filter2 = Object.assign({client:this.selectedClient},filter);
		this.topicService.getTopic(filter2).subscribe(
			(res)=>{
				this.frmLoader="none";
				this.topics=res;
			},
			(err)=>{this.frmLoader="none";this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
    }
    loadCreatedBy(){
        this.frmLoader="show";
        let filter={client:this.selectedClient};
        this.questionService.getUserCreatedBy(filter).subscribe(
            (res) => {
                this.createdBy = res;
                this.frmLoader="none";
            },
            (err) => {
                this.frmLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000); }
        )
    }
    loadTitles(filter?:any){
        this.frmLoader="show";
        let filter2 = Object.assign({limit : 'all', client:this.activeClient},filter);
        this.qsetService.getQsetTitles(filter2).subscribe(
            (res) => {
                this.qsetTitles = res;
                if(this.qsetTitles == ''){
                    this.notifier.alert('danger',"No Qset Exist with Course and Stream", 'danger', 5000);
                }
                this.frmLoader="none";
            },
            (err) => {  
                this.frmLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000); }
        )
    }
    setSubjects(streamId) {
        let filter: any = {};
        filter.stream = streamId;
        this.loadSubjects(filter);
    }
    setUnits(subjectId){
        let filter: any = {};
        filter.subject = subjectId;	 
		this.loadSubjectUnit(filter);
	}
	setTopics(unitId){
		let filter:any={};
		filter.unit=unitId;
		this.loadTopics(filter);
	}
    setTitle(streamId,courseId){
        let filter: any = {};
        filter.stream = streamId;
        if (courseId && courseId!='') {
            filter.course = courseId;
        }
        this.loadTitles(filter);
    }
    qLink(qId){
        this.qsetService.linkedQsets(qId).subscribe(
            res=>{
                this.linkQsets = res;
                this.showLinkedQs=true;
            },
            err=>{
                this.notifier.alert(err.code,err.message,'danger',1000);
            }
        );
    }
    setQueryParams(){
		if(this.qsetId){
			this.router.navigate(['/'+this.routes+'/qbank/qset/view/'],{queryParams : {qsetId : this.qsetId}});
		}else if(this.passageId){
			this.router.navigate(['/'+this.routes+'/qbank/passage/view/'],{queryParams : {passageId : this.passageId}});
		}else{
			this.router.navigate(['/'+this.routes+'/qbank/question']);
		}
	}
    paginate(e) {
        if (!this.activeFilter) {
            this.activeFilter = {};
        }
        this.countBase = e.rows * e.page + 1;
        this.activeFilter.page = (e.page + 1);
        this.activeFilter.limit = e.rows;
        this.loadQuestions(this.activeFilter);
    }
    loadSourceOption() {
        // this.frmLoader = "show";
        let filter = {
            name: "SOURCE",
            client : this.selectedClient
        };
        this.optionService.getOption().subscribe(
            (data) => {
				this.sources = data;
                // this.frmLoader = "none";
            },
            (err) => {
                // this.frmLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    loadClients(filter?:any) {
        this.frmLoader = "show";
        if(!filter){
            filter={};
        }
        filter.isESSeller=true;
        this.clientService.getClients(filter).subscribe(
            (data) => {
                this.clientConfigs = data;
                this.clientConfigs.push(
                    {client : {
                        id : this.activeClient,
                        title : 'Your Question Bank'
                    }
                })
                this.frmLoader = "none";
            },
            (err) => {
                this.frmLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    selectClient(id){
        this.selectedClient = id;
        this.qbFilterActive = '';
        this.loadFormData();
    }
    sortBy(value){
        console.log(value);
        this.activeFilter.sortBy = value;
        this.loadQuestions(this.activeFilter);
    }
}
