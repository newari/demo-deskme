import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QuestionStudioContent } from './studio';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PaginatorModule, PickListModule, MenubarModule, SplitButtonModule, DialogModule } from 'primeng/primeng';
import { QuestionService } from '../../../services/question.service';
import { CoptionService } from '../../../services/coption.service';
import { SubjectService } from '../../../services/subject.service'; 
import { TopicService } from '../../../services/topic.service';
import { QsetService } from '../../../services/qset.service';
import { PassageService } from '../../../services/passage.service';
import { SessionService } from '../../../services/session.service';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';
import { OptionService } from '../../../services/option.service';

@NgModule({
    declarations:[QuestionStudioContent], 
    imports: [CommonModule, RouterModule,SplitButtonModule, MenubarModule, PickListModule, ReactiveFormsModule, FormsModule, PaginatorModule,DialogModule,SafeHtmlPipeModule],
    providers:[QuestionService,SubjectService,CoptionService,TopicService,QsetService,PassageService,SessionService,OptionService],
    exports:[QuestionStudioContent]  
})
export class QuestionStudioModule {
    
 }