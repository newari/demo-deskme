import { Component, OnInit, Input, ElementRef, PipeTransform, Pipe  } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Question } from '../../../models/question.model';
import { QuestionService } from '../../../services/question.service';
import { NotifierService } from '../../notifier/notifier.service';

@Component({
    selector:'ek-question-view',
    templateUrl:'./view.html'
})
export class QuestionViewContent implements OnInit { 
    question:Question;
    questionId;
    panelLoader="none";
    questionLevel = [null,"Easy","Average","Medium","Difficult","Very Difficult","Tough"];
    @Input() routes:string;
    constructor(
        private questionService:QuestionService,
        private activatedRoute:ActivatedRoute,
        private notifier: NotifierService,
        private el : ElementRef,
    ){}

    ngOnInit(): void{
        this.activatedRoute.params.subscribe((params: Params) => {
	        this.questionId = params['questionId'];
            this.getQuestion(this.questionId);
	    });
    }
    getQuestion(questionId){
        this.questionService.getOneQuestion(questionId).subscribe( (data)=>{
                this.question=data;
                
                this.loadMathJax();
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader="deleted";
            }
        );
    }
    loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub]);
        }, 10);
    }
    deleteQuestion(questionId){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete this?");
        if(!confirm){
            this.panelLoader="none";
            return;
        }
        this.questionService.deleteQuestion(questionId).subscribe(
            (res)=>{
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
}
