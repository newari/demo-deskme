
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QuestionEditContent } from './edit';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { DialogModule, ChipsModule, MultiSelectModule } from 'primeng/primeng';
import { QsetService } from '../../../services/qset.service';
import { QuestionService } from '../../../services/question.service';
import { CoptionService } from '../../../services/coption.service';
import { SubjectService } from '../../../services/subject.service';
import { TopicService } from '../../../services/topic.service';
import { PassageService } from '../../../services/passage.service';
import { SessionService } from '../../../services/session.service';
import { FileinputModule } from '../../filemanager/fileinput.module';
import { OptionService } from '../../../services/option.service';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';
@NgModule({   
    declarations:[QuestionEditContent],
    imports:[RouterModule, FormsModule,DialogModule, CKEditor4Module, ReactiveFormsModule, CommonModule,DialogModule,FileinputModule,ChipsModule,MultiSelectModule],
    providers:[QsetService,QuestionService,CoptionService,SubjectService,TopicService,PassageService,SessionService,OptionService],
    exports:[QuestionEditContent] 
})
export class QuestionEditModule {}
