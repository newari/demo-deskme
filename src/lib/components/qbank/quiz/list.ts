import { Component, OnInit, Input,} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuizGroup } from '../../../models/quizgroup.model';
import { QuizService } from '../../../services/quiz.service';
import { NotifierService } from '../../notifier/notifier.service';
import { QuizGroupService } from '../../../services/quizgroup.service';
import { QsetService } from '../../../services/qset.service';
import { ActivatedRoute } from '@angular/router';
import { BatchService } from '../../../services/batch.service';

@Component({
    selector : 'ek-quiz-list',
    templateUrl:'./list.html'
})
export class QuizListContent implements OnInit{ 
    searchForm:FormGroup;
    assignQuizForm : FormGroup;
    rows;
    panelLoader="none";
    formStatus="Normal";
    totalRecords : number = 0;
    filter : any={};
    qsets;
    countBase : number=1;
    quizGroups : QuizGroup[];
    displayAssignQuizModal : boolean;
    @Input() routes:string;
    batches : any=[];
    selectedQuiz : any={};
    selectCount : number = 0;
    constructor(
        private fb : FormBuilder,
        private quizService:QuizService,
        private notifier: NotifierService,
        private quizGroupService : QuizGroupService,
        private qsetService : QsetService,
        private activatedRoute : ActivatedRoute,
        private batchService : BatchService
        ){}
    ngOnInit(): void{
        let quizGroup = '';
        this.activatedRoute.queryParams.subscribe((params)=>{
            if(params['quizGroup']){
                quizGroup = params['quizGroup'];
            }
        })
        
        if(quizGroup){
            this.filter.quizGroup=quizGroup;
        }
        this.searchForm = this.fb.group({
            qset : [''],
            quizGroup : [quizGroup],
            stream:[''],
            course:[''],
            startDate:[''],
			endDate:[''],
            status:[''],
            isPublic:[''],
            isFree : [''],
            oneTimeSubmission : [''],
            createdBy:[''],
        })
        this.assignQuizForm = this.fb.group({
            quizGroup : [''],
            batch : ['',Validators.required],
            quizs : []
        })
        this.loadQuiz();
        this.loadQuizGroups();
    }
    loadQuiz(){
        this.panelLoader="show";
        this.quizService.getQuiz(this.filter,true).subscribe(
            (data)=>{
                this.totalRecords=data.headers.get('totalRecords')|| 0;
                this.rows = data.body;
                this.panelLoader="none";
            },
            (err)=>{this.panelLoader="none"; console.log(err)}
        );
    }

    deleteQuiz(id,index){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure you want to delete this Quiz?");
        if(!confirm){
            this.panelLoader="none";
            return ;
        }
        this.quizService.deleteQuiz(id).subscribe(
            (res)=>{
                this.rows.splice(this.rows.indexOf(index), 1);
                this.panelLoader="none";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    updateStatus(event,index){
        this.panelLoader="show";
        let status = !this.rows[index].status;
        if(status){var statusName= "Active";}
        else{var statusName= "InActive";}
        let confirm=window.confirm("Are you sure you want to "+statusName+" this Quiz?");
        if(!confirm){
            this.panelLoader="none";
            if(event.target.checked){event.target.checked=false;}
            else{event.target.checked=true;}
            return;
        }
        this.quizService.updateQuiz(this.rows[index].id,{status : status}).subscribe(
            (res)=>{
                this.rows[index].status=res.status;
                this.panelLoader = "none";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader = "none";
            }
        )
    }
    // loadQsets(){
	// 	let filter={limit:'all'};
    //     this.qsetService.getQsetTitles(filter).subscribe(
    //         (res) => {
	// 			this.qsets = res;
    //             if(this.qsets == ''){
    //                 this.notifier.alert('danger',"No Qset Exist with Course and Stream", 'danger', 2000);
    //             }
    //         },
    //         (err) => {  
    //             this.notifier.alert(err.code, err.message, 'danger', 2000); }
    //     )
    // }	
    loadQuizGroupQsets(quizGroup){
        console.log(quizGroup);
        this.quizGroupService.getQuizGroupQsets(quizGroup).subscribe(
            (res) => {
				this.qsets = res;
                if(this.qsets == ''){
                    this.notifier.alert('danger',"No Qset Exist with Course and Stream", 'danger', 2000);
                }
            },
            (err) => {  
                this.notifier.alert(err.code, err.message, 'danger', 2000); }
        )
    }	
    loadQuizGroups(filter?:any){
		this.quizGroupService.getQuizGroup(filter).subscribe(
			(res)=>{
				this.quizGroups=res;
			},
			(err)=>{
				this.notifier.alert(err.code,err.message,'danger',1000);
			}
		);
    }
    setQsets(quizGroup){
        console.log(quizGroup);
        if(quizGroup){
            this.loadQuizGroupQsets(quizGroup);
        }
    }
    searchData(){
        if (!this.searchForm.valid) return;
        this.filter=this.searchForm.value;
        this.loadQuiz();
    }
    paginate(e) {
        if (!this.filter) { 
            this.filter = {};
        }
        this.countBase = e.rows * e.page + 1;
        this.filter.page = (e.page + 1);
        this.filter.limit = e.rows;
        this.loadQuiz();
    }
    showAssignQuiz(){
        this.displayAssignQuizModal=true;
        this.selectCount = Object.keys(this.selectedQuiz).length; 
        if(this.batches.length<1){
            this.getBatches();
        }
    }
    getBatches(){
        this.batchService.getBatch({limit : 'all'}).subscribe(res=>{
            this.batches = res;
        });
    }
    selectQuiz(event,id,index){
        if(event.target.checked){
            this.selectedQuiz[id]=id;
        }else{
            delete this.selectedQuiz[id];
        }
    }
    assignQuiz(){
        let data = this.assignQuizForm.value;
        data.quizs = Object.keys(this.selectedQuiz); 
        this.quizService.assignQuiz(this.assignQuizForm.value).subscribe(res=>{
            this.notifier.alert('Success','Quiz Assigned Successfully', 'success',3000);
            this.assignQuizForm.reset();
            this.selectedQuiz = {};
        },
        (err)=>{
            this.notifier.alert(err.code,err.message,'danger',1000);
        });
    }
}