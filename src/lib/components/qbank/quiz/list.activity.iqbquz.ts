import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { QuizListContent } from './list';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PaginatorModule, CalendarModule, DialogModule } from 'primeng/primeng';
import { QuizService } from '../../../services/quiz.service';
import { QsetService } from '../../../services/qset.service';
import { QuizGroupService } from '../../../services/quizgroup.service';
import { BatchService } from '../../../services/batch.service';

@NgModule({ 
    declarations:[QuizListContent],
    imports:[RouterModule,FormsModule,ReactiveFormsModule,CommonModule,PaginatorModule,CalendarModule,DialogModule],
    providers : [QuizService,QsetService,QuizGroupService,BatchService],
    exports:[QuizListContent]
})
export class QuizListModule { }