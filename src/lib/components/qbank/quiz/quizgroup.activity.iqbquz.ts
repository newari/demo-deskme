import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { QuizGroupContent } from './quizgroup';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaginatorModule, SidebarModule } from 'primeng/primeng';
import { QuizService } from '../../../services/quiz.service';
import { QsetService } from '../../../services/qset.service';
import { QuizGroupService } from '../../../services/quizgroup.service';

@NgModule({
    declarations:[QuizGroupContent],
    imports:[RouterModule,CommonModule,FormsModule,ReactiveFormsModule,PaginatorModule,SidebarModule],
    providers : [QuizService,QsetService,QuizGroupService],
    exports:[QuizGroupContent]
})
export class QuizGroupModule { }