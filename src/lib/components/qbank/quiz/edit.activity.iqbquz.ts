import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { QuizEditContent } from './edit';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CalendarModule } from 'primeng/primeng';
import { QuizService } from '../../../services/quiz.service';
import { QsetService } from '../../../services/qset.service';
import { QuizGroupService } from '../../../services/quizgroup.service';

@NgModule({
    declarations:[QuizEditContent],
    imports:[RouterModule, CommonModule, FormsModule, ReactiveFormsModule,CalendarModule],
    providers : [QuizService,QsetService,QuizGroupService],
    exports:[QuizEditContent]
})
export class QuizEditModule { }