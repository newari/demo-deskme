import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { QuizGroup } from '../../../models/quizgroup.model';
import { QuizService } from '../../../services/quiz.service';
import { QuizGroupService } from '../../../services/quizgroup.service';
import { NotifierService } from '../../notifier/notifier.service';
import { QsetService } from '../../../services/qset.service';
import { CoptionService } from '../../../services/coption.service';

@Component({
	selector : "ek-quiz-add",
    templateUrl:'./add.html'
})
export class QuizAddContent implements OnInit{
    quizForm:FormGroup;
	quizGroups:QuizGroup[];
	streams;
	courses;
	qsets;
	formStatus="Normal";
	frmLoader="none";
	@Input() routes:string;
	constructor(
		private fb:FormBuilder,
		private quizService:QuizService,
		private quizGroupService : QuizGroupService,
		private notifier: NotifierService,
		private qsetService : QsetService,
		private coptionService : CoptionService
	){}
	ngOnInit(): void {
		this.quizForm=this.fb.group({
			quizGroup:['', Validators.required],
			// course : ['', Validators.required],
			// stream : ['', Validators.required],
			qset:['', Validators.required],
			startDate:['', Validators.required],
			endDate:['', Validators.required],
			isFree : [false],
			// isPublic : [''],
			oneTimeSubmission : [false],
			status:[false, Validators.required],
		});
		this.loadQuizGroups();
		this.loadCourses();
		this.loadStreams();
	}
	addQuiz(): void {
		this.formStatus="Processing";
		this.quizService.addQuiz(this.quizForm.value).subscribe(
			res=>{
				this.quizForm.reset(); 
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
	loadQuizGroups(filter?:any){
		this.quizGroupService.getQuizGroup(filter).subscribe(
			(res)=>{
				this.quizGroups=res;
			},
			(err)=>{
				this.notifier.alert(err.code,err.message,'danger',1000);
			}
		);
	}
	loadQsets(filter?:any){
		this.frmLoader="show";
		filter.limit='all';
        this.qsetService.getQsetTitles(filter).subscribe(
            (res) => {
				this.qsets = res;
                if(this.qsets == ''){
                    this.notifier.alert('No Qset Exist',"Quiz Group of Course and Stream", 'danger', 4000);
                }
                this.frmLoader="none";
            },
            (err) => {  
                this.frmLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 2000); }
        )
	}
	loadCourses(){
		this.frmLoader="show";
		let filter:any={};
		filter.option='COURSE';
        this.coptionService.getCoption(filter).subscribe(
			res=>{
                this.frmLoader="none";
                this.courses=res; 
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
        this.frmLoader="show";
        let filter={
            option : 'STREAM'
        };
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	getQsets(index){
		let filter: any = {};
		let data:any = this.quizGroups[index];
		if(data.stream && data.stream.id){
			filter.stream = data.stream.id;
		}
		if(data.course && data.course.id){
			filter.course = data.course.id;
		}
		this.quizForm.patchValue({quizGroup:data.id});
        this.loadQsets(filter);
	}
	setQsets(courseId,streamId){
        let filter: any = {};
        filter.stream = streamId;
        if (courseId && courseId!='') {
            filter.course = courseId;
        }
        this.loadQsets(filter);
	}
	freeQuiz(el){
		if (el.target.checked){
			this.quizForm.patchValue({isFree : true});
		} else {
			this.quizForm.patchValue({isFree : false});
		}
	}
}