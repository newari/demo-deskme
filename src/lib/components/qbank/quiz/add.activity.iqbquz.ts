import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuizAddContent } from './add';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CalendarModule } from 'primeng/primeng';
import { QuizGroupService } from '../../../services/quizgroup.service';
import { QsetService } from '../../../services/qset.service';
import { QuizService } from '../../../services/quiz.service';

@NgModule({
    declarations:[QuizAddContent],
    imports:[RouterModule, CommonModule, FormsModule, ReactiveFormsModule,CalendarModule],
    providers : [QuizService,QsetService,QuizGroupService],
    exports:[QuizAddContent]
})
export class QuizAddModule { }