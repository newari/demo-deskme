import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { QuizGroup } from '../../../models/quizgroup.model';
import { NotifierService } from '../../notifier/notifier.service';
import { QuizGroupService } from '../../../services/quizgroup.service';
import { CoptionService } from '../../../services/coption.service';

@Component({
    selector:'ek-quiz-group',
    templateUrl: './quizgroup.html'
})
export class QuizGroupContent implements OnInit {
    rows = [];
    addQuizGroupForm : FormGroup;
    editQuizGroupForm : FormGroup;
    displayAddQuizGroup :boolean=false;
    displayEditQuizGroup :boolean=false;
    quizGroupList : QuizGroup[];
    editQuizGroupIndex;
    panelLoader:any="none";
    examId;
    courses=[];
    streams=[];
    countBase : number=1;
    filter : any = {};
    totalRecords : number=0;
    @Input() routes : string;
    constructor(
        private fb : FormBuilder,
        private notifier: NotifierService,
        private quizGroupService : QuizGroupService,
        private coptionService : CoptionService
    ) {}

    ngOnInit(): void {
        this.addQuizGroupForm=this.fb.group({
            title:['',Validators.required],
            alias : ['',Validators.required],
            course : ['',Validators.required],
            stream : ['',Validators.required],
            status : [true,Validators.required],
        });
        this.editQuizGroupForm=this.fb.group({
            title:['',Validators.required],
            alias : ['',Validators.required],
            course : ['',Validators.required],
            stream : ['',Validators.required],
            status : [true,Validators.required],
        });
        this.loadCourses();
        this.loadStreams();
        this.loadQuizGroup();
    }
    loadQuizGroup() {
        this.panelLoader = "show";
        this.quizGroupService.getQuizGroup(this.filter,true).subscribe(
            (data) => {
                this.totalRecords = data.headers.get('totalRecords') || 0;
                this.quizGroupList = data.body;
                this.panelLoader = "none";
            },
            (err) => {
                this.panelLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    addQuizGroup(){
        this.panelLoader = "show";
        this.quizGroupService.addQuizGroup(this.addQuizGroupForm.value).subscribe(
            res=>{
                this.quizGroupList.unshift(res);
                this.displayAddQuizGroup=false;
                this.panelLoader = "none";
                this.addQuizGroupForm.reset();
                this.notifier.alert('success','Added Succesfully','success',1000);
            },
            err=>{
                this.panelLoader = "none";
                this.notifier.alert(err.code,err.message,'danger',1000);
        });
    }
    updateQuizGroup(){
        this.quizGroupService.updateQuizGroup(this.examId,this.editQuizGroupForm.value).subscribe(
            res=>{
                this.quizGroupList[this.editQuizGroupIndex]=res;
                this.displayEditQuizGroup=false;
                this.panelLoader = "none";
                this.notifier.alert('success','Added Succesfully','success',1000);
            },
            err=>{
                this.notifier.alert(err.code,err.message,'danger',1000);
            }
        );
    }
    deleteQuizGroup(coption,index) {
        this.panelLoader = "show";
        let confirm = window.confirm("Are you sure to delete " + coption.valueAlias + "?");
        if (!confirm) {
            this.panelLoader = "none";
            return;
        }
        this.quizGroupService.deleteQuizGroup(coption.id).subscribe(
            (res) => {
                this.quizGroupList.splice(this.quizGroupList.indexOf(coption,index), 1);
                this.panelLoader = "none";
                console.log('Deleted!');
            },
            (err) => {
                console.log('Error');
            }
        );
    }

    showAddQuizGroup(){
        this.displayAddQuizGroup=true;
    }
    showEditQuizGroup(id,i) {
        this.displayEditQuizGroup = true;
        this.examId=id;
        this.editQuizGroupIndex=i;
        let data :any = JSON.parse(JSON.stringify(this.quizGroupList[i]));

        if(data.course && data.course.id ){
            data.course = data.course.id;
        }
        if(data.stream && data.stream.id ){
            data.stream = data.stream.id;
        }
        this.editQuizGroupForm.reset();
        this.editQuizGroupForm.patchValue(data);
    }
    loadCourses(){
		this.panelLoader="show";
		let filter:any={};
		filter.option='COURSE';
        this.coptionService.getCoption(filter).subscribe(
			res=>{
                this.panelLoader="none";
                this.courses=res; 
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
        this.panelLoader="show";
        let filter={
            option : 'STREAM'
        };
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				this.panelLoader="none";
				this.streams=res;
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
    }
    paginate(e) {
        if (!this.filter) { 
            this.filter = {};
        }
        this.countBase = e.rows * e.page + 1;
        this.filter.page = (e.page + 1);
        this.filter.limit = e.rows;
        this.loadQuizGroup();
    }
}
