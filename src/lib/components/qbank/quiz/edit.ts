import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { NotifierService } from '../../notifier/notifier.service';
import { QuizService } from '../../../services/quiz.service';
import { CoptionService } from '../../../services/coption.service';
import { QsetService } from '../../../services/qset.service';
import { QuizGroupService } from '../../../services/quizgroup.service';
@Component({
	selector : "ek-quiz-edit",
    templateUrl:'./edit.html'
})
export class QuizEditContent implements OnInit{
    quizForm:FormGroup;
	courses;
    streams;
    quizId;
	qsets;
	quiz;
    quizGroups=[];
	formStatus="Normal";
	frmLoader = "none";
	@Input() routes:string;
	constructor(
        private fb:FormBuilder,
        private notifier : NotifierService,
		private activatedRoute: ActivatedRoute,
        private quizService:QuizService,
        private coptionService : CoptionService,
        private qsetService :QsetService,
        private quizGroupService : QuizGroupService
    ){ }
	ngOnInit(): void {
		this.quizForm=this.fb.group({
			// course : ['',Validators.required],
			// stream : ['',Validators.required],
            // quizGroup:['', Validators.required],
			// qset:['', Validators.required],
			startDate:[''],
			endDate:[''],
			isFree : [''],
			// isPublic: [''],
			oneTimeSubmission : [''],
			status:[false, Validators.required]
		});
		this.loadCourses();
        this.loadStreams();
        this.loadQuizGroups();
		this.activatedRoute.params.subscribe((params: Params) => {
	        this.quizId = params['quizId'];
			this.getQuiz();
		});
	}
	getQuiz(){
		this.quizService.getOneQuiz(this.quizId).subscribe(
			(res)=>{
				this.quiz=JSON.parse(JSON.stringify(res));
				if(res.quizGroup && res.quizGroup.id ){
                    res.quizGroup = res.quizGroup.id;
				}
                if(res.qset && res.qset.id){
					this.qsets=[];
                    this.qsets.push(res.qset);
                    res.qset = res.qset.id;
				}
				delete res.startDate;
				delete res.endDate;
				this.quizForm.patchValue(res);
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		)
	}
	  
	updateQuiz(): void {
		let data=this.quizForm.value;
		if(!data.startDate){
			data.startDate=new Date(this.quiz.startDate);
		}
		if(!data.endDate){
			data.endDate=new Date(this.quiz.endDate);	
		}
		data.startDate=new Date(data.startDate.setSeconds(0));
		data.endDate=new Date(data.endDate.setSeconds(0));
		
		this.formStatus="Processing";
		this.quizService.updateQuiz(this.quizId, data).subscribe(
			(res)=>{
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Updated Successfully', 'success', 5000 );
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
	loadQuizGroups(filter?:any){
		// filter.limit="all";
		this.quizGroupService.getQuizGroup(filter).subscribe(
			(res)=>{
				this.quizGroups=res;
			},
			(err)=>{
				this.notifier.alert(err.code,err.message,'danger',1000);
			}
		);
	}
	loadQsets(filter?:any){
		this.frmLoader="show";
		filter.limit='all';
        this.qsetService.getQsetTitles(filter).subscribe(
            (res) => {
				this.qsets = res;
                if(this.qsets == ''){
                    this.notifier.alert('danger',"No Qset Exist with Course and Stream", 'danger', 2000);
                }
                this.frmLoader="none";
            },
            (err) => {  
                this.frmLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 2000); }
        )
	}
	
	loadCourses(){
		this.frmLoader="show";
		let filter={option:'COURSE'};
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				this.frmLoader="none";
				this.courses=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
    }
    setQsets(courseId,streamId){
        let filter: any = {};
        filter.stream = streamId;
        if (courseId && courseId!='') {
            filter.course = courseId;
        }
        this.loadQsets(filter);
	}
	publicQuiz(el){ 								
		if (el.target.checked){
			this.quizForm.patchValue({isPublic : true});
		} else {
			this.quizForm.patchValue({isPublic : false});
		}
	}
	freeQuiz(el){ 								
		if (el.target.checked){
			this.quizForm.patchValue({isFree : true});
		} else {
			this.quizForm.patchValue({isFree : false});
		}
	}
}