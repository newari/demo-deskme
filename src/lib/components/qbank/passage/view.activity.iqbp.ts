import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { PassageViewContent } from './view';
import { PassageService } from '../../../services/passage.service';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';

@NgModule({
    declarations:[PassageViewContent],
    imports:[RouterModule, CommonModule,SafeHtmlPipeModule], 
    providers:[PassageService],
    exports:[PassageViewContent] 
})
export class PassageViewModule {}
