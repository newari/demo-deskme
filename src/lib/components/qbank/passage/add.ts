import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NotifierService } from '../../notifier/notifier.service';
import { Passage } from '../../../models/passage.model';
import { PassageService } from '../../../services/passage.service';

@Component({
    selector:'ek-passage-add',
    templateUrl:'./add.html'
})
export class PassageAddContent implements OnInit{
    passageForm:FormGroup;
	pData:Passage;
	questionType:string;
	formStatus="Normal";
	frmLoader="none";
	@Input() routes:string;
	constructor(
		private fb:FormBuilder,
		private passageService:PassageService,
		private notifier: NotifierService,
		){ 
			
		}
	
	addPassage(): void {
		this.formStatus="Processing";
		let data:any={};
		data=this.passageForm.value;
		 
		this.pData=data;
		 
		this.passageService.addPassage(this.pData).subscribe(
			res=>{
				this.passageForm.reset(); 
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
	ngOnInit(): void {
		this.passageForm=this.fb.group({
			title:['', Validators.required],
			passageContent:['', Validators.required],
			status:['null',Validators.required]
			
		});
		
	}
}
