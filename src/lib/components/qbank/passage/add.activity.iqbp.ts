import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PassageAddContent } from './add';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PassageService } from '../../../services/passage.service';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';

@NgModule({
    declarations:[PassageAddContent], 
    imports:[RouterModule,CommonModule, FormsModule, ReactiveFormsModule,CKEditor4Module],
    providers:[PassageService],
    exports:[PassageAddContent]
})
export class PassageAddModule { }

