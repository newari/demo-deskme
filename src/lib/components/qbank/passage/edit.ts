import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { PassageService } from '../../../services/passage.service';
import { NotifierService } from '../../notifier/notifier.service';

@Component({
    selector:'ek-passage-edit',
    templateUrl:'./edit.html'
})
export class PassageEditContent implements OnInit{
    passageUpdateForm:FormGroup;
	passage;
	passageType;
	passageId;
	frmLoader="none";
	formStatus="Normal";
	panelLoader="none";
	@Input() routes:string;
	constructor(
		private fb:FormBuilder,
		private activatedRoute: ActivatedRoute,
		private passageService:PassageService,
		private notifier: NotifierService){ 
	}

	getPassage(passageId){
		this.panelLoader="show";
		this.passageService.getOnePassage(passageId).subscribe(
			(res)=>{
				this.passage=res;
				this.passageUpdateForm.patchValue(this.passage);
				this.panelLoader="none";
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		)
	}
	updatePassage(): void {
		if(!this.passageUpdateForm.dirty){
			return;
		}
		this.formStatus="Processing";
		this.passage=this.passageUpdateForm.value;  
		this.passageService.updatePassage(this.passageId, this.passage).subscribe(
			(res)=>{
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
 

	ngOnInit(): void {
		this.passageUpdateForm=this.fb.group({
			title:[''],
			passageContent:[''],
			status:['']
		}); 
		this.activatedRoute.params.subscribe((params: Params) => {
	        this.passageId = params['passageId'];
			this.getPassage(this.passageId);
		}); 
		
	}
 
}
