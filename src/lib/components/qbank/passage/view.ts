import { Component, OnInit, Input  } from '@angular/core';
import { Passage } from '../../../models/passage.model';
import { PassageService } from '../../../services/passage.service';
import { ActivatedRoute } from '@angular/router';
import { NotifierService } from '../../notifier/notifier.service';


@Component({
    selector:'ek-passage-view',
    templateUrl:'./view.html'
})
export class PassageViewContent implements OnInit{ 
    passage:Passage;
    passageId;
    getPassageQuestions; 
    questionLevel = [null,"Easy","Average","Medium","Difficult","Very Difficult"];
    frmLoader="none";
	formStatus="Normal";
    panelLoader="none";
    @Input() routes:string;
    constructor(
        private passageService:PassageService,
        private activatedRoute:ActivatedRoute, 
		private notifier: NotifierService){}


    ngOnInit(): void{
    //    this.activatedRoute.params.subscribe((params: Params) => {
    //         this.passageId = params['passageId'];
    //         this.getPassage(this.passageId);
    //     });
        this.activatedRoute.queryParams.subscribe(params=>{
            this.passageId = params['passageId'];
            this.getPassage(this.passageId);
        });
        this.loadPassageQuestions();
    }
    loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub]);
        }, 10);
    }
    getPassage(passageId){
        this.passageService.getOnePassage(passageId).subscribe( (data)=>{
                this.passage=data;
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader="deleted";
            }
        );
    } 
    loadPassageQuestions():void{
        this.passageService.getPassageQuestions(this.passageId).subscribe(
            (data) =>{ 
                this.getPassageQuestions = data; 
                this.loadMathJax();
            },
            (err) => console.log(err)
        );
    }
    deletePassage(passageId){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete this?");
        if(!confirm){
            return this.panelLoader="none";
        }
        this.passageService.deletePassage(passageId).subscribe(
            (res)=>{
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    deletePassageQuestion(passageQuestionId){
        let confirm=window.confirm("Are you sure to delete ?");
        if(!confirm){
            return this.panelLoader="none";
        }
        this.passageService.deletePassageQuestion(passageQuestionId).subscribe(
            (res)=>{
                this.getPassageQuestions.splice(this.getPassageQuestions.indexOf(passageQuestionId), 1);
                this.notifier.alert('Success', 'deleted Successfully', 'success', 1000 );
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
}