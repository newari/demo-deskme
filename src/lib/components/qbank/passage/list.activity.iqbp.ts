import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {  PassageListContent } from './list';
import { CommonModule } from '@angular/common';
import { PassageService } from '../../../services/passage.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EkDropdownModule } from '../../dropdown/dropdown.module';
import { PaginatorModule } from 'primeng/primeng';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';

@NgModule({
    declarations:[PassageListContent],
    imports:[CommonModule, RouterModule, PaginatorModule,FormsModule,ReactiveFormsModule, EkDropdownModule ,SafeHtmlPipeModule ], 
    providers:[PassageService],
    exports:[PassageListContent]
})
export class PassageListModule{}
