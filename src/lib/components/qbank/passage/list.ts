import { Component, OnInit, Input  } from '@angular/core';
import { PassageService } from '../../../services/passage.service';
import { NotifierService } from '../../notifier/notifier.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
    selector:'ek-passage-list',
    templateUrl:'./list.html'
})
export class PassageListContent implements OnInit{ 
    rows;
    passageForm : FormGroup;
    columns; 
    panelLoader="none";
    countBase : number = 1;
    filter : any={};
    totalRecords : number;
    @Input() routes:string;
    constructor(
        private fb : FormBuilder,
        private passageService:PassageService,
		private notifier: NotifierService
        ){}
    ngOnInit(): void{
        this.passageForm= this.fb.group({
            id : [''],
            title : [''],
            status : ['']
        });
        this.getPassages();
    }
    getPassages(filter?:any):void{
        this.passageService.getPassage(filter,true).subscribe(
            (res)=>{
                this.rows = res.body;
                this.totalRecords = res.headers.get('totalRecords') || 0;
            },
            (err)=>this.notifier.alert(err.code, err.message, 'danger', 5000 )
        );
    }
    deletePassage(passage){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete "+passage.title+"?");
        if(!confirm){
            return this.panelLoader="none";
        }
        this.passageService.deletePassage(passage.id).subscribe(
            (res)=>{
                this.rows.splice(this.rows.indexOf(passage), 1);
                this.panelLoader="none";
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );

            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    searchData(){
        var merged = Object.assign( this.filter, this.passageForm.value);
        this.getPassages(merged);
    }
    paginate(e) {
        if (!this.filter) { 
            this.filter = {};
        }
        this.countBase = e.rows * e.page + 1;
        this.filter.page = (e.page + 1);
        this.filter.limit = e.rows;
        this.getPassages(this.filter);
    }
}
