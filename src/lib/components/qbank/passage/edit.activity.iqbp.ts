
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { PassageEditContent } from './edit';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { PassageService } from '../../../services/passage.service';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';


@NgModule({
    declarations:[PassageEditContent], 
    imports:[RouterModule, FormsModule, ReactiveFormsModule, CommonModule,CKEditor4Module],
    providers:[PassageService],
    exports:[PassageEditContent]

})
export class PassageEditModule {
    
 }
