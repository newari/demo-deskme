import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { OptionExamListContent } from './examlist';
import { CommonModule } from '@angular/common';
import { TabViewModule, SidebarModule } from 'primeng/primeng';
import { AddCoptionModule } from '../../coption/add.module';
import { EditCoptionModule } from '../../coption/edit.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OptionService } from '../../../services/option.service';

@NgModule({
    declarations: [OptionExamListContent],
    imports: [ReactiveFormsModule,FormsModule,TabViewModule, CommonModule, SidebarModule, RouterModule],
    providers:[OptionService],
    exports:[OptionExamListContent]
})
export class OptionExamListModule {}
 