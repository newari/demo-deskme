import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OptionSourceListContent } from './qsource';
import { CommonModule } from '@angular/common';
import { TabViewModule, SidebarModule } from 'primeng/primeng';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { OptionService } from '../../../services/option.service';

@NgModule({
    declarations: [OptionSourceListContent],
    imports: [ReactiveFormsModule,FormsModule,TabViewModule, CommonModule, SidebarModule, RouterModule],
    providers:[OptionService],
    exports:[OptionSourceListContent]
})
export class OptionQSourceModule {}
 