import { Component, OnInit, Input } from '@angular/core';
import { NotifierService } from '../../notifier/notifier.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OptionService } from '../../../services/option.service';
import { Option } from '../../../models/option.model';

@Component({
    selector:'ek-option-qsource',
    templateUrl: './qsource.html'
})
export class OptionSourceListContent implements OnInit {
    rows = [];
    addForm : FormGroup;
    editForm : FormGroup;
    displayAddOption :boolean=false;
    displayEditOption :boolean=false;
    editOptionIndex;
    panelLoader:any="none";
    sourceId;
    @Input() routes : string;
    constructor(
        private fb : FormBuilder,
        private notifier: NotifierService,
        private optionService: OptionService
    ) {}

    ngOnInit(): void {
        this.addForm=this.fb.group({
            value:['',Validators.required],
            valueAlias : ['',Validators.required],
            name:['SOURCE'],
            status : [true,Validators.required],
        });
        this.editForm=this.fb.group({
            value:['',Validators.required],
            valueAlias : ['',Validators.required],
            name:[''],
            status : ['',Validators.required],
        });
        this.loadOption();
    }
    loadOption() {
        this.panelLoader = "show";
        this.optionService.getOption({ name: "SOURCE", limit:'all', sort:'createdAt DESC'}).subscribe(
            (data) => {
                this.rows = data;
                this.panelLoader = "none";
            },
            (err) => {
                this.panelLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    addSource(){
        this.panelLoader = "show";
        this.optionService.addOption(this.addForm.value).subscribe(
            res=>{
                this.rows.unshift(res);
                this.displayAddOption=false;
                this.addForm.controls['value'].reset();
                this.addForm.controls['valueAlias'].reset();
                this.panelLoader = "none";
                this.notifier.alert('success','Added Succesfully','success',1000);
            },
            err=>{
                this.panelLoader = "none";
                this.notifier.alert(err.code,err.message,'danger',1000);
        });
    }
    updateSource(){
        this.optionService.updateOption(this.sourceId,this.editForm.value).subscribe(
            res=>{
                this.rows[this.editOptionIndex]=res;
                this.displayEditOption=false;
                this.panelLoader = "none";
                this.notifier.alert('success','Added Succesfully','success',1000);
            },
            err=>{
                this.notifier.alert(err.code,err.message,'danger',1000);
            }
        );
    }
    deleteOption(coption) {
        this.panelLoader = "show";
        let confirm = window.confirm("Are you sure to delete " + coption.valueAlias + "?");
        if (!confirm) {
            this.panelLoader = "none";
            return;
        }
        this.optionService.deleteOption(coption.id).subscribe(
            (res) => {
                this.rows.splice(this.rows.indexOf(coption), 1);
                this.panelLoader = "none";
                console.log('Deleted!');
            },
            (err) => {
                console.log('Error');
            }
        );
    }
   
    showAddOption(){
        this.displayAddOption=true;
    }
    showEditOption(id,i) {
        this.displayEditOption = true;
        this.sourceId=id;
        this.editOptionIndex=i;
        this.editForm.patchValue(this.rows[i]);
    }
  
}
