import { Component, OnInit, Input } from '@angular/core';
import { NotifierService } from '../../notifier/notifier.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { OptionService } from '../../../services/option.service';
import { Option } from '../../../models/option.model';

@Component({
    selector:'ek-option-examlist',
    templateUrl: './examlist.html'
})
export class OptionExamListContent implements OnInit {
    rows = [];
    addExamForm : FormGroup;
    editExamForm : FormGroup;
    displayAddOption :boolean=false;
    displayEditOption :boolean=false;
    examList: Option[];
    editOptionIndex;
    panelLoader:any="none";
    examId;
    @Input() routes : string;
    constructor(
        private fb : FormBuilder,
        private notifier: NotifierService,
        private optionService: OptionService
    ) {}

    ngOnInit(): void {
        this.addExamForm=this.fb.group({
            value:['',Validators.required],
            valueAlias : ['',Validators.required],
            name:['EXAM'],
            status : [true,Validators.required],
        });
        this.editExamForm=this.fb.group({
            value:['',Validators.required],
            valueAlias : ['',Validators.required],
            name:[''],
            status : ['',Validators.required],
        });
        this.loadOption();
    }
    loadOption() {
        this.panelLoader = "show";
        this.optionService.getOption({ name: "EXAM", limit:200, sort:'createdAt DESC' }).subscribe(
            (data) => {
                this.examList = data;
                this.panelLoader = "none";
            },
            (err) => {
                this.panelLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    addExam(){
        this.panelLoader = "show";
        this.optionService.addOption(this.addExamForm.value).subscribe(
            res=>{
                this.examList.unshift(res);
                this.displayAddOption=false;
                this.addExamForm.controls['value'].reset();
                this.addExamForm.controls['valueAlias'].reset();
                this.panelLoader = "none";
                this.notifier.alert('success','Added Succesfully','success',1000);
            },
            err=>{
                this.panelLoader = "none";
                this.notifier.alert(err.code,err.message,'danger',1000);
        });
    }
    updateExam(){
        this.optionService.updateOption(this.examId,this.editExamForm.value).subscribe(
            res=>{
                this.examList[this.editOptionIndex]=res;
                this.displayEditOption=false;
                this.panelLoader = "none";
                this.notifier.alert('success','Added Succesfully','success',1000);
            },
            err=>{
                this.notifier.alert(err.code,err.message,'danger',1000);
            }
        );
    }
    deleteOption(coption) {
        this.panelLoader = "show";
        let confirm = window.confirm("Are you sure to delete " + coption.valueAlias + "?");
        if (!confirm) {
            this.panelLoader = "none";
            return;
        }
        this.optionService.deleteOption(coption.id).subscribe(
            (res) => {
                this.examList.splice(this.rows.indexOf(coption), 1);
                this.panelLoader = "none";
                console.log('Deleted!');
            },
            (err) => {
                console.log('Error');
            }
        );
    }

    showAddOption(){
        this.displayAddOption=true;
    }
    showEditOption(id,i) {
        this.displayEditOption = true;
        this.examId=id;
        this.editOptionIndex=i;
        this.editExamForm.patchValue(this.examList[i]);
    }

}
