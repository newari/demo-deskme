import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QsetMakerContent } from './qsetmaker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MultiSelectModule } from 'primeng/primeng';
import { SubjectService } from '../../../services/subject.service';
import { TopicService } from '../../../services/topic.service';
import { QsetService } from '../../../services/qset.service';
import { QuestionService } from '../../../services/question.service';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';

@NgModule({
    declarations:[QsetMakerContent],
    imports:[FormsModule,CommonModule, ReactiveFormsModule,RouterModule,MultiSelectModule,SafeHtmlPipeModule],
    providers : [QsetService,QuestionService, SubjectService,TopicService], 
    exports : [QsetMakerContent]
})
export class QsetMakerModule { }