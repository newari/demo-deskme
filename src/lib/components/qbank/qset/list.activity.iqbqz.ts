import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QsetListContent } from './list';
import { CommonModule } from '@angular/common';
import { DialogModule, PaginatorModule, CalendarModule, SidebarModule } from 'primeng/primeng';
import { EkDropdownModule } from '../../dropdown/dropdown.module';
import { QuestionService } from '../../../services/question.service';
import { QsetService } from '../../../services/qset.service';
import { ReactiveFormsModule } from '@angular/forms';
import { FacultyService } from '../../../services/faculty.service';
import { CoptionService } from '../../../services/coption.service';
import { TestService } from '../../../services/test.service';
import { TestInstructionService } from '../../../services/testInstruction.service';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';
import { EdukitStoreService } from '../../../services/edukit-store.serivce';

 
@NgModule({  
    declarations:[QsetListContent],
    imports:[CommonModule,RouterModule,DialogModule,ReactiveFormsModule, EkDropdownModule,PaginatorModule,CalendarModule,SidebarModule,SafeHtmlPipeModule],
    providers: [QsetService,QuestionService,FacultyService,CoptionService,TestService,TestInstructionService,EdukitStoreService],
    exports:[QsetListContent]
})
export class QsetListModule{
    
 }