import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { QsetViewContent } from './view';
import { DialogModule } from 'primeng/primeng';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { QsetService } from '../../../services/qset.service';
import { QuestionService } from '../../../services/question.service';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';
import { EdukitStoreService } from '../../../services/edukit-store.serivce';

@NgModule({ 
    declarations:[QsetViewContent], 
    imports:[CommonModule, RouterModule,FormsModule,DialogModule,ReactiveFormsModule,CKEditor4Module,SafeHtmlPipeModule],
    providers:[QsetService,QuestionService,EdukitStoreService],
    exports:[QsetViewContent] 
})
export class QsetViewModule {}