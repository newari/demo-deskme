import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Qset } from '../../../models/qset.model';
import { QsetService } from '../../../services/qset.service';
import { NotifierService } from '../../notifier/notifier.service';
import { CoptionService } from '../../../services/coption.service';
import { AuthService } from '../../../services/auth.service';
import { FacultyService } from '../../../services/faculty.service';
import { Faculty } from '../../../models/faculty.model';
import { ClientService } from '../../../services/client.service';

@Component({
	selector : "ek-qset-add",
    templateUrl:'./add.html'
})
export class QsetAddContent implements OnInit{
    qsetForm:FormGroup;
	qsetData:Qset;
	faculties:Faculty[];
	streams;
	formStatus="Normal";
	frmLoader="none";
	courses;
	user;
	clientConfig : any={};
	// validMessage :any=" ' '  \" \" Special character are not Allowed";
	@Input() routes : string;
	@Input() courseId : string;
	@Input() streamId : any;
	// @Input() user:any;
	constructor(
		private fb:FormBuilder,
		private qsetService:QsetService,
		private notifier: NotifierService,
		private coptionService: CoptionService,
		private authService : AuthService,
		private facultyService : FacultyService,
		private clientService : ClientService
	){}
	ngOnInit(): void {
		this.user = this.authService.user();
		if(this.user){
			this.clientService.getClientConfig({client : this.user.client}).subscribe(
				res=>{
				this.clientConfig = res;
				},err=>{console.log(err);}
			);
		}
		this.qsetForm=this.fb.group({
			title:['', Validators.required],
			alias:['', Validators.required],
			paperType:['Objective', Validators.required],
			type:['PRACTICE', Validators.required],
			description:[''],
			duration:['', Validators.required],
			course:['', Validators.required],
			stream:['', Validators.required],
			totalMarks:['', Validators.required],
			defaultLang:['EN', Validators.required],
			totalQs:['', Validators.required],
			createdBy:[this.user.id],
			assignedTo:[''],
			assignedBy:[''],
			isPublished : [false],
			forSell : [false],
			cost : [0,Validators.required],
			mrp : [0,Validators.required],
			discount : [0,Validators.required],
			customParam : this.fb.group({
				publicQuiz : false,
				// allowForSale : false
			}),
			status:[false, Validators.required],
		});
		if(this.courseId){
			this.qsetForm.patchValue({assignedTo:this.user.id});
		}
		this.loadCourses();
		this.loadStreams();
	}
	addQset(): void {
		this.formStatus="Processing";
		this.qsetData=this.qsetForm.value;
		if(this.qsetData.assignedTo !=''){
			if(this.qsetData.assignedTo != this.qsetData.createdBy){
				this.qsetData.assignedBy=this.user.id;
			}
		}
		this.qsetService.addQset(this.qsetData).subscribe(
			res=>{
				this.qsetFormReset();
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
	qsetFormReset(){
		this.qsetForm.reset(); 
		let data : any = {
			paperType:'Objective',
			type:'PRACTICE',
			defaultLang:'EN',
			createdBy:this.user.id,
			isPublished : false,
			forSell : false,
			cost : 0,
			mrp : 0,
			discount : 0,
			customParam : {
				publicQuiz : false,
				// allowForSale : false
			}
		};
		if(this.courseId) data.assignedTo=this.user.id;
		this.qsetForm.patchValue(data);
	}
	changeCourse(id){
		if(!this.courseId){
			this.loadFaculty({course:id});
		}
	}
	loadFaculty(filter?:any){
		this.facultyService.getFaculty(filter ,true).subscribe(
			(res)=>{
				this.faculties=res.body;
			},
			(err)=>{
				this.notifier.alert(err.code,err.message,'danger',1000);
			}
		);
	}
	loadCourses(){
		this.frmLoader="show";
		let filter={};
		if(this.courseId){
			filter ={id : this.courseId}; 
		}
		else{
			filter ={option:'COURSE'};
		}
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				this.frmLoader="none";
				if(!res[0]){
					this.courses=[res];
					this.qsetForm.patchValue({course : this.courses[0].id});
				}else{
					this.courses=res;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
				if(this.streamId && this.streams.length >0){
					let newStream = [];
					for(var i=0;i<this.streams.length;i++){
						for(var j=0;j<this.streamId.length;j++){
							if( this.streams[i].id == this.streamId[j]){
								newStream.push(this.streams[i]);
							}
						}
					}
					this.streams = newStream;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	addPublicQuiz(el){ 								// (typeof (el) == "boolean" && el == true) 
		if (el.target.checked){
			this.qsetForm.patchValue({customParam : {publicQuiz : true}});
		} else {
			this.qsetForm.patchValue({customParam : {publicQuiz : false}});
		}
	}
	qsetForSell(el){ 								// (typeof (el) == "boolean" && el == true) 
		if (el.target.checked){
			this.qsetForm.patchValue({forSell : true});
		} else {
			this.qsetForm.patchValue({forSell : false});
		}
	}
}