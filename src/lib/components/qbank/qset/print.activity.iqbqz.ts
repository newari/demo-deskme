import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {  QsetPrintContent } from './print';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { QsetService } from '../../../services/qset.service';
import { ClientService } from '../../../services/client.service';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';

export const ROUTES:Routes=[
    {path: '', component: QsetPrintContent, pathMatch:'full'}, 
];
 
@NgModule({
    declarations:[QsetPrintContent],
    imports: [CommonModule, RouterModule,FormsModule, ReactiveFormsModule,SafeHtmlPipeModule],
    providers :[QsetService,ClientService], 
    exports:[QsetPrintContent] 
})
export class QsetPrintModule {}
