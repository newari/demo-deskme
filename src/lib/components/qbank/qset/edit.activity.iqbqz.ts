import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QsetEditContent } from './edit';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { QsetService } from '../../../services/qset.service';
import { FacultyService } from '../../../services/faculty.service';
import { DialogModule } from 'primeng/primeng';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';
@NgModule({
    declarations:[QsetEditContent],
    imports:[RouterModule, FormsModule, CommonModule, ReactiveFormsModule,DialogModule,CKEditor4Module],
    providers: [QsetService,FacultyService],
    exports:[QsetEditContent]
})
export class QsetEditModule {}