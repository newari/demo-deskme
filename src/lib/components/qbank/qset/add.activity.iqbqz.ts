import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { QsetAddContent } from './add';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { QsetService } from '../../../services/qset.service';
import { FacultyService } from '../../../services/faculty.service';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';

@NgModule({
    declarations:[QsetAddContent],
    imports:[RouterModule, FormsModule,CommonModule, ReactiveFormsModule,CKEditor4Module],
    providers: [QsetService,FacultyService],
    exports:[QsetAddContent]
})
export class QsetAddModule { }