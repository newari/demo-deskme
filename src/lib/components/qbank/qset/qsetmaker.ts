import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { Qset } from '../../../models/qset.model';
import { Coption } from '../../../models/coption.model';
import { QsetService } from '../../../services/qset.service';
import { QuestionService } from '../../../services/question.service';
import { NotifierService } from '../../notifier/notifier.service';
import { SubjectService } from '../../../services/subject.service';
import { TopicService } from '../../../services/topic.service';
import { CoptionService } from '../../../services/coption.service';

@Component({
	selector : "ek-qset-maker",
    templateUrl:'./qsetmaker.html'
})
export class QsetMakerContent implements OnInit{
    genForm:FormGroup;
	qsetData:Qset;
	courses:any;
	streams : Coption[];
	subjectUnit = [];
	generatedData = [];
	subjects=[];
	qsetId;
	topics = [];
	questionLevel = [null,"Easy","Average","Medium","Difficult","Very Difficult"];
	questions=[];
	formStatus="Normal";
	frmLoader="none";
	cars;
	subjectObj;
	unitObj;
	@Input() routes: string;
	@Input() courseId : string;
	@Input() streamId : any;
	constructor(
		private fb:FormBuilder,
		private qsetService:QsetService,
		private questionService : QuestionService,
		private notifier: NotifierService,
		private subjectService : SubjectService,
		private topicService : TopicService,
		private activatedRoute : ActivatedRoute,
		private coptionService: CoptionService,
		private router : Router
	){}
	ngOnInit(): void {
		this.genForm=this.fb.group({
			qType:['', Validators.required],
			totalQs:['', Validators.required],
			course:['', Validators.required],
			stream:['', Validators.required],
			unit:[''],
			topic:[''],
			subject:[''],
			mp:[''],
			mn:[''],
			defaultLang : ['',Validators.required]
		});
		this.activatedRoute.params.subscribe((params: Params) => {
	        this.qsetId = params['qsetId'];
		});
		this.loadCourses();
		this.loadStreams();
	}
	loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub]);
        }, 10);
    }
	loadCourses(){
		this.frmLoader="show";
		let filter={};
		if(this.courseId){
			filter ={id : this.courseId}; 
		}
		else{
			filter ={option:'COURSE'};
		}
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				this.frmLoader="none";
				if(!res[0]){
					this.courses=[res];
					this.genForm.patchValue({course : this.courses[0].id});
				}else{
					this.courses=res;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
				if(this.streamId && this.streams.length >0){
					let newStream = [];
					for(var i=0;i<this.streams.length;i++){
						for(var j=0;j<this.streamId.length;j++){
							if( this.streams[i].id == this.streamId[j]){
								newStream.push(this.streams[i]);
							}
						}
					}
					this.streams = newStream;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadSubjects(filter){
		this.subjects=[];
		this.subjectService.getSubject(filter).subscribe(
			(res)=>{
				this.frmLoader="none";
				for (let index = 0; index < res.length; index++) {
					let obj= {label: res[index].name, value: res[index].id};
					this.subjects.push(obj);
				}
			},
			(err)=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
	}
	loadSubjectUnit(subjectid){
		this.subjectUnit=[];
		this.subjectService.getMultipleSubjectUnit(subjectid).subscribe(
			(res)=>{
				this.frmLoader="none";
				for (let index = 0; index < res.length; index++) {
					let obj= {label: res[index].name, value: res[index].id};
					this.subjectUnit.push(obj);
				}
				this.subjectObj=[];
			},
			(err)=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
	}
	loadTopics(filter){
		this.topics=[];
		this.topicService.getMultipleUnitTopic(filter).subscribe(
			(res)=>{
				this.frmLoader="none";
				for (let index = 0; index < res.length; index++) {
					let obj= {label: res[index].name, value: res[index].id};
					this.topics.push(obj);
				}
				this.unitObj=[];
			},
			(err)=>{this.notifier.alert(err.code, err.message, 'danger', 5000);}
		)
	}
	setSubjects(streamId){
		let filter:any={};
		filter.stream=this.streams[streamId].id;
		this.loadSubjects(filter);
	}
	subjectOnChange(event){
		this.subjectObj=event.value;
	}
	setUnits(){
		this.loadSubjectUnit(this.subjectObj);
	}
	unitOnChange(event){
		this.unitObj=event.value;
	}
	setTopics(){
		this.loadTopics(this.unitObj);
	}
	generateQuestions(){
		let data = JSON.parse(JSON.stringify(this.genForm.value));
		data.course=this.courses[data.course].id;
		data.stream=this.streams[data.stream].id;
		this.questionService.getRandomQuestions(data).subscribe(
		(res)=>{
			if(res.length > 1){
				this.questions= this.questions.concat(res);
				this.setSearchData(this.genForm.value,res.length);
				this.loadMathJax();
			}
			else{
				this.notifier.alert('Warning','No Question Find or Generated','danger',5000);
			}
			this.subjectUnit=[];
			this.topics=[];
			this.subjects=[];
			this.genForm.reset();
		},
		(err)=>{console.log(err)}
		);
	}
	setSearchData(data,availableQs){
		data.availableQs = availableQs;
		data.course=this.courses[data.course].value;
		data.stream=this.streams[data.stream].valueAlias;
		this.qsetService.convertGenData(data).subscribe(
			(res)=>{
				this.generatedData.push(res);
			},(err)=>console.log(err)
		);
	}
	deleteQuestion(index){
		this.questions.splice(index,1);
		this.notifier.alert("success","Removed sucessfully","success",1000);
	}
	shuffleQuestions(array) {
		var currentIndex = array.length, temporaryValue, randomIndex;
		while (0 !== currentIndex) {
		  randomIndex = Math.floor(Math.random() * currentIndex);
		  currentIndex -= 1;
		  temporaryValue = array[currentIndex];
		  array[currentIndex] = array[randomIndex];
		  array[randomIndex] = temporaryValue;
		}
		return array;
	}
	assignQuestions(){
		let data=[];
		 this.questions.map(object=>{
			 data.push(object._id);
		 })
		this.qsetService.assignGenQs(data,this.qsetId).subscribe(
			(res)=>{
				if(res){
					this.notifier.alert('success','Created sucessfully','success',5000);
					this.router.navigate(['/'+this.routes+'/qbank/qset/view'], { queryParams: {qsetId: this.qsetId}});
				}
			},(err)=>console.log(err)
		);
	}
    confirmAssign(){
        let confirm=window.confirm("Are you sure you want to Assign?");
        if(!confirm){
            return;
		}
		this.assignQuestions();
	}
	
}