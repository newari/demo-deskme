import { Component, OnInit, Input, ElementRef, ViewChild,} from '@angular/core';
import { QsetService } from '../../../services/qset.service';
import { NotifierService } from '../../notifier/notifier.service';
import { AuthService } from '../../../services/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CoptionService } from '../../../services/coption.service';
import { Faculty } from '../../../models/faculty.model';
import { FacultyService } from '../../../services/faculty.service';
import { Qset } from '../../../models/qset.model';
import { TestService } from '../../../services/test.service';
import { TestInstructionService } from '../../../services/testInstruction.service';
import { TestInstruction } from '../../../models/testInstruction.model';
import { ClientService } from '../../../services/client.service';
import { EdukitStoreService } from '../../../services/edukit-store.serivce';

@Component({
    selector : 'ek-qset-list',
    templateUrl:'./list.html'
})
export class QsetListContent implements OnInit{ 
    searchForm:FormGroup;
    testForm : FormGroup;
    rows;
    columns; 
    user;
    display = false;
    qsetId;
    totalRecords:any;
    courses:any;
    streams:any;
    faculties:Faculty[];
    panelLoader="none";
    formStatus="Normal";
    countBase:number=1;
    BuyCountBase:number=1;
    filter : any={};
    assignIndex : any;
    showAssignModal : boolean=false;
    displayQuickTest : boolean=false;
    instructions:TestInstruction[];
    buyQsetFilterForm : FormGroup;
    buyQsets = [];
    sellers =[];
    showBuyQsetModal : boolean = false;
    showBuyQsetQsModal : boolean = false;
    buyQsetQs :any={};
    totalBuyQsetsRecords :any;
    sellerCourses:any;
    sellerStreams:any;
    lang : string;
    questionLevel = [null,"Easy","Average","Medium","Difficult","Very Difficult","Tough"];
    orderSummary : any;
    displayOrderSummary : boolean = false;
    activeClient : any;
    activeClientConfig:any;
    order : any;
    displayPaymentModal : boolean = false;
    selectedBuyQsets : any={};
    @ViewChild('mathload', { read: ElementRef }) mathload: ElementRef;
    @Input() routes : string;
    @Input() courseId : string;
    @Input() streamId : any;
    constructor(
        private fb : FormBuilder,
        private qsetService:QsetService,
        private notifier: NotifierService,
        private authService : AuthService,
        private coptionService : CoptionService,
        private facultyService : FacultyService,
        private testService : TestService,
        private clientService : ClientService,
        private testInstructionService : TestInstructionService,
        private edukitStoreService : EdukitStoreService,
        ){}
    ngOnInit(): void{
        this.user=this.authService.user();
        if(this.user && this.user.client){
            this.activeClient = this.user.client;
        }
        this.loadClientConfig();
        // if(this.authService.admin())
        // this.activeClient = this.authService.admin().client;
        this.searchForm = this.fb.group({
            title:[''],
            stream:[''],
            course:[''],
            status:[''],
            assignedTo:[''],
            assignedBy:[''],
            createdBy:[''],
        })
        this.testForm=this.fb.group({
			title:[''],
			alias:[''],
			testType:[''],
			theme : ['', Validators.required],
			duration:[],
			course:[''],
			stream:[''],
			maxScore:[''],
			defaultLang:[''],
			instruction:[''],
			totalQs:[''],
			resultDate : [''],
			startDate:['', Validators.required],
			endDate:['', Validators.required],
			status:[false],
			description:[''],
			isCalculator:[false],
			sections: this.fb.array([
                {
                    title:[''],
                    totalMarks:[''],
                    totalQs:[''],
                    maxTime:[''],
                    bonusMarks : [0],
                    qset: Qset,
                    isQsetAssigned:false
                }
            ])
        });
        this.buyQsetFilterForm=this.fb.group({
            client : ['',Validators.required],
            course : ['',Validators.required],
            stream : ['',Validators.required],
        });
        if(this.courseId){
            this.filter.course=this.courseId;
            this.filter.assignedTo=this.user.id;
        }
        if(this.streamId){
            this.filter.stream=this.streamId;
        }
        if(this.routes == 'ims'){
            this.loadCourses();
            this.loadStreams();
            this.loadFaculty();
        }
        
        this.loadQsets();
    }
    loadQsets(){
        this.panelLoader="show";
        this.qsetService.getQset(this.filter,true).subscribe(
            (data)=>{
                this.totalRecords=data.headers.get('totalRecords')|| 0;
                this.rows = data.body;
                this.panelLoader="none";
            },
            (err)=>{this.panelLoader="none"; console.log(err)}
        );
    }

    deleteQset(qset){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete "+qset.title+" ?");
        if(!confirm){
            this.panelLoader="none";
            return ;
        }
        this.qsetService.deleteQset(qset.id).subscribe(
            (res)=>{
                this.rows.splice(this.rows.indexOf(qset), 1);
                this.panelLoader="none";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    searchData(){
        if (!this.searchForm.valid) {
            return;
        }
        this.filter=this.searchForm.value;
        this.loadQsets();
    }
    updateStatus(event,index){
        this.panelLoader="show";
        let status = !this.rows[index].status;
        if(status){var statusName= "Active";}
        else{var statusName= "InActive";}
        let confirm=window.confirm("Are you sure you want to "+statusName+" this Test Series?");
        if(!confirm){
            this.panelLoader="none";
            if(event.target.checked){event.target.checked=false;}
            else{event.target.checked=true;}
            return;
        }
        this.qsetService.updateQset(this.rows[index].id,{status : status}).subscribe(
            (res)=>{
                this.rows[index].status=res.status;
                this.panelLoader = "none";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader = "none";
            }
        )
    }
    assignModal(index){
        this.assignIndex=index;
        this.showAssignModal=true;
    }
    assignFaculty(value){
        if(!value){return;}
        let data : any={
            assignedTo : value
        };
        let id = this.rows[this.assignIndex].id;
        this.panelLoader="show";
        this.qsetService.assignFaculty(id,data).subscribe(
            res=>{
                this.rows[this.assignIndex].assignedTo=res.assignedTo;
                this.rows[this.assignIndex].assignedBy=res.assignedBy;
                this.panelLoader="none";
                this.showAssignModal=false;
                this.notifier.alert('success','Assigned SuccessFully', 'success',1000);
            },
            err=>{
                this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
            }
        );
    }
    loadCourses(){
		this.panelLoader="show";
		this.coptionService.getCoption({option:'COURSE'}).subscribe(
			res=>{
                this.courses=res;
				this.panelLoader="none";
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.panelLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.panelLoader="none";
				this.streams=res;
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
    }
    loadFaculty(){
        this.panelLoader="show";
        this.facultyService.getFaculty().subscribe(
            res=>{
                this.panelLoader="none";
                this.faculties=res;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code,err.message,'danger',1000);
            }
        );
    }
    loadInstructions(){
        this.testInstructionService.getInstruction().subscribe(
            (data)=> {this.instructions = data;},
            (err)=> this.notifier.alert(err.code, err.message, 'danger', 5000 )
        );
    }
    paginate(e) {
        if (!this.filter) { 
            this.filter = {};
        }
        this.countBase = e.rows * e.page + 1;
        this.filter.page = (e.page + 1);
        this.filter.limit = e.rows;
        this.loadQsets();
    }
    quickTestModal(index){
        
        let testData :any= JSON.parse(JSON.stringify(this.rows[index]));
        if(testData.course && testData.course.id){
            testData.course = testData.course.id;
        }
        if(testData.stream && testData.stream.id){
            testData.stream = testData.stream.id;
        }
        if(testData.totalMarks){
            testData.maxScore = testData.totalMarks;
        }
        if(testData.type){
            testData.testType = testData.type;
        }
        testData.sections=[{
            title:testData.title,
			totalMarks:testData.totalMarks,
			totalQs: testData.totalQs,
			maxTime: testData.duration,
			bonusMarks : {},
			qset: testData.id,
			isQsetAssigned:true
        }];
        testData.status=false;
        this.testForm.patchValue(testData);
        this.displayQuickTest=true;
        if(!this.instructions){
            this.loadInstructions();
        }

    }
    addQuickTest(){
        this.formStatus="Processing";
        this.testService.createQuickTest(this.testForm.value).subscribe(
			res=>{
                this.testForm.reset();
                this.displayQuickTest=false;
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Quick Test Created Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
    }
    createDuplicate(value){
        let confirm=window.confirm("Are you sure you want to Create Duplicate "+value.title+" ?");
        if(!confirm){
            return;
        }
        let data = JSON.parse(JSON.stringify(value));
        delete data.id;
        delete data.createdAt;
        delete data.updatedAt;
        data.title += " 1";
        data.course= data.course.id;
        data.stream= data.stream.id;
        data.assignedTo = '';
        data.assignedBy = '';
        data.status = false;
        this.qsetService.addQset(data).subscribe(
			res=>{
                res.course=value.course;
                res.stream=value.stream;
                this.rows.unshift(res);
                this.notifier.alert('Success', 'Duplicate Created Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		);
    }
  
    loadClients(filter?:any) {
        this.panelLoader = "show";
        if(!filter){
            filter={};
        }
        filter.isESSeller=true;
        this.clientService.getClients(filter).subscribe(
            (res) => {
                this.sellers = res;
                for (let i = 0; i < res.length; i++) {
                    const element = res[i];
                    if(element.client.id == this.activeClient){
                        this.sellers.splice(i,1);
                        break;
                    }
                }
                this.panelLoader = "none";
            },
            (err) => {
                this.panelLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    loadSellerCourses(client){
		this.panelLoader="show";
		this.coptionService.getCoption({option:'COURSE',client : client}).subscribe(
			res=>{
                this.sellerCourses=res;
				this.panelLoader="none";
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadSellerStreams(client){
		this.panelLoader="show";
		this.coptionService.getCoption({option:'STREAM',client : client}).subscribe(
			res=>{
                this.sellerStreams=res;
                this.panelLoader="none";
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
    }
    selectClient(client){
        this.buyQsetFilterForm.patchValue({client : client,course:'',stream : ''});
        this.loadSellerCourses(client);
        this.loadSellerStreams(client);
    }
    displayBuyQsetModal(){
        this.loadClients();
        this.showBuyQsetModal=true;
    }
    getBuyQsets(filter?:any){
        if(!filter) filter={};
        filter.forSell=true;
        filter.status = true;
        filter.isPublished = true;
        let fData = Object.assign(this.buyQsetFilterForm.value, filter);
        this.panelLoader="show";
        this.qsetService.getQset(fData,true).subscribe(
            (data)=>{
                this.totalBuyQsetsRecords=data.headers.get('totalRecords')|| 0;
                this.buyQsets = data.body;
                this.panelLoader="none";
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code,err.message,"danger",2000);
            }
        );
    }
    displayBuyQsetQsModal(index){
        this.getBuyQsetQuestions(index);
    }
    getBuyQsetQuestions(index){
        let qset = this.buyQsets[index];
        this.panelLoader="show";
        let data={
            id : qset.id,
            defaultLang : qset.defaultLang,
            client : qset.client
        }
        this.qsetService.getQsetQuestion(data).subscribe(
            (res) =>{ 
                this.buyQsetQs = res; 
                this.lang= qset.defaultLang;
                this.showBuyQsetQsModal=true;
                this.panelLoader="none";
                this.loadMathJax();
            },
            (err) =>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader="none";  
            }
        );
    }
    buyQsetPaginate(e) {
        let filter:any = {};
        this.BuyCountBase = e.rows * e.page + 1;
        filter.page = (e.page + 1);
        filter.limit = e.rows;
        this.getBuyQsets(filter);
    }
    loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub,this.mathload]);
        }, 10);
    }
    loadClientConfig() {
        this.clientService.getClientConfig().subscribe(
            res=>{
                this.activeClientConfig=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    selectBuyQsets(event,id,index){
        if(event.target.checked){
            this.selectedBuyQsets[id]=this.buyQsets[index].id;
        }else{
            delete this.selectedBuyQsets[id];
        }
    }
    showOrderSummary(){
        this.order=null;
        let keys = Object.keys(this.selectedBuyQsets);
        if(!keys || keys.length<1){ return this.notifier.alert('Warning','Select Qset First !!','danger',2000);}
        this.displayOrderSummary = true;
        this.qsetService.getQsetsOrderSummary({qsets : keys}).subscribe(
            res=>{
                if(res){
                    this.orderSummary = res;
                }
            },
            err=>{this.notifier.alert(err.code,err.message,'danger',1000);}
        );
    }
    placeOrder(){
        if(this.order){
            this.displayPaymentModal=true;
            return ;
        }
        if(!this.orderSummary) return this.notifier.alert('Danger','OrderSummary not Exist','danger', 5000);
        let orderData:any = {
            buyer:this.activeClient,
            order:{
                orderTitle:"Full Question Sets",
                total:(this.orderSummary.totalAmount + (this.orderSummary.totalAmount*0.18)),
                more:{
                    totalAmount:(this.orderSummary.totalAmount + (this.orderSummary.totalAmount*0.18)),
                    totalQsets: this.orderSummary.totalQsets,
                    sale : "Qsets", 
                },
                items : []
            },
        };  
        for (let index = 0; index < this.orderSummary.qsets.length; index++) {
            const qset = this.orderSummary.qsets[index];
            orderData.order.items.push({
                title : qset.title,
                quantity:1,
                amount : qset.cost,
                discountAmt : qset.discountAmt,
                discount : qset.discount,
                seller : qset.client,
                qsetId : qset.id
            });
        } 
        this.panelLoader="show";
        this.edukitStoreService.placeOrder(orderData).subscribe(
            res=>{
                this.order=res;
                this.displayPaymentModal=true;
                // this.loadClientConfig();
                this.panelLoader="none";
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code,err.message, 'danger',2000);
            }
        );
    }
    makePayment(){
        let confirm = window.confirm("Are u Sure to make payment. Amount will be debited from your EdukitStore Wallet. \n After Successfull payment, you will not be able to add more question from other sellers.");
        if(!confirm) return ;
        this.panelLoader="show";
        this.edukitStoreService.makePayment(this.order.id).subscribe(
            res=>{
                this.loadQsets();
                this.displayPaymentModal=false;
                this.showBuyQsetModal=false;
                this.displayOrderSummary = false;
                this.notifier.alert("Success", "Paid Successfully", 'success', 2000);
                this.panelLoader="none";
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
}