import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QsetService } from '../../../services/qset.service';
import { ClientService } from '../../../services/client.service';
import { NotifierService } from '../../notifier/notifier.service';

@Component({
    selector:'ek-qset-print',
    templateUrl:'./print.html',
})
export class QsetPrintContent implements OnInit{ 
    rows;
    activeFilter;
    totalRecords;
    panelLoader="none";
    qsetId;
    title;
    counter : number=0; 
    clientId : any;
    clientConfig;
    testData:any=[];
    defaultLang:any;
    questionLevel = [null,"Easy","Average","Medium","Difficult","Very Difficult","Tough"];
    @Input() routes : string;
    constructor(
        private qsetService:QsetService,
        private activatedRoute : ActivatedRoute,
    ){}
    ngOnInit(): void{
        this.activatedRoute.queryParams.subscribe(params=>{
            this.qsetId= params['qsetId'];
            this.defaultLang=params['defaultLang'];
        });
        // this.activatedRoute.queryParams.subscribe(params=>{
        //     this.qsetId= params['qsetId'];
        // });
        this.loadQuestions();
    }
    loadQuestions():void{
        this.panelLoader="show";
        let data:any={id : this.qsetId};
        if(this.defaultLang){
            data.defaultLang=this.defaultLang;
        }
        this.qsetService.getQsetQuestion(data).subscribe(
            (data) =>{ 
                if(data[this.defaultLang])
                this.rows=data[this.defaultLang];
                else
                this.rows=data;
                this.panelLoader="none";
                this.loadMathJax();
            },
            (err) =>{
                this.panelLoader="none";
            }
        );
    }
    loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub]);
        }, 10);
    }

    loadVariantQs(lang){
        this.loadQuestions();
    }
}
