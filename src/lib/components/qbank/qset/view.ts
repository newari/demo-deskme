import { Component, OnInit, Input, ElementRef, ViewChild  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Qset } from '../../../models/qset.model';
import { QsetService } from '../../../services/qset.service';
import { QuestionService } from '../../../services/question.service';
import { NotifierService } from '../../notifier/notifier.service';
import { AuthService } from '../../../services/auth.service';
import { EdukitStoreService } from '../../../services/edukit-store.serivce';
import { ClientService } from '../../../services/client.service';

@Component({
    selector:"ek-qset-view",
    templateUrl:'./view.html'
})
export class QsetViewContent implements OnInit{ 
    editQuesForm : FormGroup;
    qset:Qset;
    qsetId;
    qIndex;
    getQsetQuestions : any=[];
    displayQEdit = false;
    answers =[];
	options = ["A","B","C","D","E"]; 
    questionId;
    questionLevel = [null,"Easy","Average","Medium","Difficult","Very Difficult","Tough"];
    panelLoader="none";
    questionLoader="none";
    questionType;
    lang:string;
    @Input() routes : string;
    qsetQs : any={};
    oShipEnable : boolean=false;
    oShipTitle = [null,'SELF','PENDING','FINALISED'];
    activeClient : any;
    displayOrderSummary : boolean;
    orderSummary : any;
    order: any;
    activeClientConfig:any;
    displayPaymentModal:boolean;
    user : any;
    mtqAns : any={};
    @ViewChild('mathload', { read: ElementRef }) mathload: ElementRef;
    constructor(
        private qsetService:QsetService,
        private questionService : QuestionService,
        private fb : FormBuilder,
        private activatedRoute:ActivatedRoute,
        private notifier: NotifierService,
        private authService : AuthService,
        private edukitStoreService:EdukitStoreService,
        private clientService:ClientService,
        private router : Router
    ){}
    ngOnInit(): void{
        this.editQuesForm=this.fb.group({
            qContent:['', Validators.required],
            qType : [],
			qOption:this.fb.group({
				optionA:[''],
				optionB:[''],
				optionC:[''],
				optionD:[''],
				optionE:[''],
				optionP:[''],
				optionQ:[''],
				optionR:[''],
				optionS:[''],
				optionT:[''],
			}),
			ans:['', Validators.required],
            qSolution:['', Validators.required],
            defaultFont:[''],
        });
        this.loadClientConfig();
        this.user = this.authService.user();
		if(this.user){
            this.activeClient = this.user.client;
		}
        this.activatedRoute.queryParams.subscribe(params=>{
            this.qsetId = params['qsetId'];
            this.getQset();
        });
    }
    loadClientConfig() {
       this.clientService.getClientConfig().subscribe(
           res=>{
               this.activeClientConfig=res;
           },
           err=>{
               this.notifier.alert(err.code, err.message, 'danger', 5000);
           }
       );
    }
    loadQuestions():void{
        this.questionLoader="show";
        let data={
            id : this.qsetId,
            defaultLang : this.lang
        }
        this.qsetService.getQsetQuestion(data).subscribe(
            (res) =>{ 
                this.qsetQs = res; 
                this.questionLoader="none"; 
                this.loadMathJax();
            },
            (err) =>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.questionLoader="none";  
            }
        );
    }
    loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub,this.mathload]);
        }, 10);
    }
    getQset(){
        this.panelLoader="show";  
        this.qsetService.getOneQset(this.qsetId).subscribe( (data)=>{
                this.qset=data;
                this.lang = data.defaultLang;
                if(this.lang){
                    this.loadQuestions();
                }
                if(this.qset.oShip != '2'){
                    this.oShipEnable = true;
                }
                this.panelLoader="none";  
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader="none";  
            }
        );
    }
    showQuickEdit(index){
        this.displayQEdit = true;
        this.questionId = this.qsetQs[this.lang][index].question._id;
        this.questionType = this.qsetQs[this.lang][index].question.qType;
        let ansExist = this.qsetQs[this.lang][index].question.ans;
        if(ansExist){
            if(this.questionType == 'MAQ'){
                var ans=ansExist.split(',');
                this.answers = ans;
            }
            else if(this.questionType == 'MTQ'){
                var ans=ansExist.split(',');
                for (let i = 0; i < ans.length; i++) {
                    let element = ans[i];
                    element = element.split('-');
                    if(element[0]&&element[1]){
                        this.mtqAns[element[0]]= element[1].split('');
                    }
                }
            }
        }
        this.editQuesForm.patchValue(this.qsetQs[this.lang][index].question);
    }
    insertCheckbxValue(event){		
		if(event.target.checked){
			this.answers.push(event.target.value);
		}else{
			let index = this.answers.indexOf(event.target.value);
			this.answers.splice(index,1);
		}
    }
    mtqAnsChange(event,option){
		if(event.target.checked){
			if(!this.mtqAns[option]){
				this.mtqAns[option]=[];
			}
			this.mtqAns[option].push(event.target.value);
		}else{
			if(this.mtqAns[option]){
				let index = this.mtqAns[option].indexOf(event.target.value);
				this.mtqAns[option].splice(index,1);
			}
		}
	}
    updateEditQuestion(){
        let data=this.editQuesForm.value;
        if(data.qType == 'MAQ' && this.answers.length > 0){
			data.ans=this.answers;
		}
		else if(data.qType == 'MTQ'){
			data.ans='';
			for(let key in this.mtqAns){
				if(this.mtqAns[key].length>0){
					let ans = key+'-';
					ans+= this.mtqAns[key].sort().join('');
					data.ans+= ans+',';
				}
			}
			data.ans=data.ans.slice(0, -1);
        }
        this.questionService.updateQuestion(this.questionId,data).subscribe(
			(res)=>{
                this.notifier.alert('Success', 'Saved changes Successfully', 'success', 5000 );
                this.loadQuestions();
                this.displayQEdit = false;
                this.answers=[];
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		);
    }
    deleteQset(qsetId){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete this?");
        if(!confirm){
            this.panelLoader="none";
            return;
        }
        this.qsetService.deleteQset(qsetId).subscribe(
            (res)=>{
                this.qsetQs = [];
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }

    deleteQsetQuestion(qsetQs,index){
        this.questionLoader="show";
        if((this.qsetQs[this.lang].length-1) == index){
            let confirm=window.confirm("Are you sure !! you want to delete this Question? this will effect on report, if this 'Question Set' is already assigned in Test or Quiz.");
            if(!confirm){
                this.questionLoader="none";
                return;
            }
            this.qsetService.deleteOneQsetQuestion(qsetQs).subscribe(
                (res)=>{
                    this.qsetQs[this.lang].splice(index,1);
                    this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                    this.questionLoader="none";
                },
                (err)=>{
                    this.notifier.alert(err.code, err.message, 'danger', 5000 );
                }
            );
        }
        else{
            let confirm=window.confirm('If you want to delete this question. Go to the Question bank and double click the question or drag question from another side. "Press OK" to open Question Bank.');
            if(!confirm){
                this.questionLoader="none";
                return;
            }
            this.router.navigate(['/ims/qbank/question/studioplus'],{queryParams : {qsetId : this.qset.id,lang : this.qset.defaultLang }});
        }

        
    }
    changeLanguage(){
        if(!this.qsetQs[this.lang]){
            this.loadQuestions();
        }
    }
    showOrderSummary(){
        this.displayOrderSummary = true;
        if(this.orderSummary){
            return;
        }
        this.qsetService.getQuestionsOrderSummary(this.qsetId).subscribe(
            res=>{this.orderSummary = res},
            err=>{this.notifier.alert(err.code,err.message,'danger',1000);}
        );
    }
    placeOrder(){
        let orderData:any = {
            order:{},
            buyer:this.activeClient
        };
         
        if(this.orderSummary&&this.orderSummary.clients&&this.orderSummary.clients.length>0){
           let order = {
               orderTitle:this.qset.title +" for "+this.orderSummary.paidQs+" paid Questions",
               total:(this.orderSummary.totalAmount + (this.orderSummary.totalAmount*18/100)),
               items:[],
               more:{
                paidQs: this.orderSummary.paidQs,
                totalAmount:(this.orderSummary.totalAmount + (this.orderSummary.totalAmount*18/100)),
                totalQs:this.orderSummary.totalQs,
                yourQs:this.orderSummary.yourQs,
                sale : "Questions",
                qsetId:this.qsetId
               }
            }
            for (let index = 0; index < this.orderSummary.clients.length; index++) {
                const item = this.orderSummary.clients[index];
                 order.items.push({
                    quantity:1,
                    amount: item.totalPrice,
                    seller: item.client?item.client.id:'',
                    totalQuestions: item.totalQuestions
                 });
            }
            orderData.order=order;
        }
        this.edukitStoreService.placeOrder(orderData).subscribe(
            res=>{
                this.order=res;
                
                this.displayPaymentModal=true;
            },
            err=>{
                this.notifier.alert(err.code,err.message, 'danger',2000);
            }
        );
    }

    makePayment(){
        let confirm = window.confirm("Are u Sure to make payment. Amount will be debited from your EdukitStore Wallet. \n After Successfull payment, you will not be able to add more question from other sellers.");
        if(!confirm) return ;
        
        this.edukitStoreService.makePayment(this.order.id).subscribe(
            res=>{
                this.notifier.alert("Success", "Paid Successfully", 'success', 500);
                this.displayPaymentModal=false;
                this.displayOrderSummary=false;
                this.qset.oShip =3;
                this.oShipEnable=true
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
}