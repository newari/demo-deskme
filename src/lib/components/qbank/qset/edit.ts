import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Qset } from '../../../models/qset.model';
import { QsetService } from '../../../services/qset.service';
import { CoptionService } from '../../../services/coption.service';
import { NotifierService } from '../../notifier/notifier.service';
import { AuthService } from '../../../services/auth.service';
import { FacultyService } from '../../../services/faculty.service';
import { Faculty } from '../../../models/faculty.model';
import { ClientService } from '../../../services/client.service';

@Component({
	selector : "ek-qset-edit",
    templateUrl:'./edit.html'
})
export class QsetEditContent implements OnInit{
    qsetForm:FormGroup;
	qset:Qset;
	faculties:Faculty[];
	qsetId;
	validMessage :any=" ' '  \" \" Special character are not Allowed";
	courses;
	streams;
	user;
	formStatus="Normal";
	frmLoader = "none";
	publicQuiz : boolean = false;
	clientConfig : any={};
	@Input() routes : string;
	@Input() courseId : string;
	@Input() streamId : any;
	constructor(
		private fb:FormBuilder,
		private activatedRoute: ActivatedRoute,
		private qsetService:QsetService,
		private coptionService:CoptionService,
		private notifier: NotifierService,
		private facultyService: FacultyService,
		private authService : AuthService,
		private clientService : ClientService
		){ 
	}
	ngOnInit(): void {
		this.user=this.authService.user();
		if(this.user){
			this.clientService.getClientConfig({client : this.user.client}).subscribe(
				res=>{
				this.clientConfig = res;
				},err=>{console.log(err);}
			);
		}
		this.qsetForm=this.fb.group({
			title:['', Validators.required],
			alias:['', Validators.required],
			paperType:['', Validators.required],
			type:['', Validators.required],
			description:[''],
			duration:['', Validators.required],
			course:['', Validators.required],
			stream:['', Validators.required],
			totalMarks:['', Validators.required],
			defaultLang:['', Validators.required],
			totalQs:['', Validators.required],
			createdBy:[''],
			assignedTo:[''],
			assignedBy:[''],
			customParam : [''],
			forSell : [false],
			isPurchased : [false],
			cost : [0,Validators.required],
			mrp : [0,Validators.required],
			discount : [0,Validators.required],
			isPublished: ['',Validators.required],
			status:['', Validators.required],
		});
		this.loadCourses();
		this.loadStreams();
		this.activatedRoute.params.subscribe((params: Params) => {
	        this.qsetId = params['qsetId'];
			this.getQset(this.qsetId);
		});
	}
	getQset(qsetId){
		this.qsetService.getOneQset(qsetId).subscribe(
			(res)=>{
				this.qset=res;
				if(this.qset.course && this.qset.course !=''){
					this.qset.course = this.qset.course.id;
					if(!this.routes){
						this.loadFaculty({course:this.qset.course});
					}
				}
				if(this.qset.stream && this.qset.stream !=''){
					this.qset.stream = this.qset.stream.id;
				}
				if(this.qset.assignedTo && this.qset.assignedTo !=''){
					this.qset.assignedTo = this.qset.assignedTo.id;
				}
				if(this.qset.customParam && this.qset.customParam.publicQuiz){
					this.publicQuiz = true;
				}
				this.qsetForm.patchValue(this.qset);
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		)
	}
	  
	updateQset(): void {
		// if(!this.qsetForm.dirty){
		// 	return;
		// }
		this.formStatus="Processing";
		this.qset=this.qsetForm.value;
		this.qsetService.updateQset(this.qsetId, this.qset).subscribe(
			(res)=>{
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
	changeCourse(id){
		if(!this.routes){
			this.loadFaculty({course:id});
		}
	}
	changeFaculty(){
		if(this.qsetForm.value.assignedTo !=''){
			this.qsetForm.patchValue({assignedBy:this.user.id});
		}
	}
	loadFaculty(filter?:any){
		this.facultyService.getFaculty(filter ,true).subscribe(
			(res)=>{
				this.faculties=res.body;
			},
			(err)=>{
				this.notifier.alert(err.code,err.message,'danger',1000);
			}
		);
	}
	loadCourses(){
		this.frmLoader="show";
		let filter={};
		if(this.courseId){
			filter ={id : this.courseId}; 
		}
		else{
			filter ={option:'COURSE'};
		}
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				this.frmLoader="none";
				if(!res[0]){
					this.courses=[res];
					this.qsetForm.patchValue({course : this.courses[0].id});
				}else{
					this.courses=res;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
				if(this.streamId && this.streams.length >0){
					let newStream = [];
					for(var i=0;i<this.streams.length;i++){
						for(var j=0;j<this.streamId.length;j++){
							if( this.streams[i].id == this.streamId[j]){
								newStream.push(this.streams[i]);
							}
						}
					}
					this.streams = newStream;
				}
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	addPublicQuiz(el){ 				// (typeof (el) == "boolean" && el == true) 
		if (el.target.checked){
			this.qsetForm.patchValue({customParam : {publicQuiz : true}});
		} else {
			this.qsetForm.patchValue({customParam : {publicQuiz : false}});
		}
	}
	qsetForSell(el){ 								// (typeof (el) == "boolean" && el == true) 
		if (el.target.checked){
			this.qsetForm.patchValue({forSell : true});
		} else {
			this.qsetForm.patchValue({forSell : false});
		}
	}
}