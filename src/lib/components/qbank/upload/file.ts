import { Component, OnInit, ElementRef, Input } from "@angular/core";
import { Params, ActivatedRoute, Router } from "@angular/router";
import { Coption } from "../../../models/coption.model";
import { Subject } from "../../../models/subject.model";
import { Subjectunit } from "../../../models/subjectunit.model";
import { Topic } from "../../../models/topic.model";
import { QuestionService } from "../../../services/question.service";
import { QsetService } from "../../../services/qset.service";
import { FilemanagerService } from "../../filemanager/filemanager.service";
import { NotifierService } from "../../notifier/notifier.service";
import { CoptionService } from "../../../services/coption.service";
import { QcodeService } from "../../../services/qcode.service";
import { Session } from "../../../models/session.model";
import { SessionService } from "../../../services/session.service";
import { calcBindingFlags } from "@angular/core/src/view/util";
import { OptionService } from "../../../services/option.service";
import { Option } from "../../../models/option.model";

@ Component({
    selector : "ek-upload-question",
    templateUrl:"./file.html"
})
export class uploadFileContent implements OnInit{
    qsetId;
    content;
    errors=[];
    uploadQData = {url : null,qsetId : null,lang:null};
    questions=[];
    displayIns=true;
    courses:any;
	streams:Coption[];
	subjects:Subject[];
    subjectUnit:Subjectunit[];
    sessions : Session[];
	topics:Topic[];
    qLevelLabel = [null,"Easy","Average","Medium","Difficult","Very Difficult","Tough"];
    fileStatus="Normal";
    panelLoader="none";
    validateError : string=null;
    defaultLang : string;
    lang : string;
    tag : string;
    font : string='';
    sourceList  :Option[];
    newUpload : boolean;
    @Input() routes:string;
    @Input() courseId : string;
	@Input() streamId : any;
    constructor(
        private questionService : QuestionService,
        private qsetService : QsetService,
        private activatedRoute : ActivatedRoute,
        private fmService : FilemanagerService,
        private notifier : NotifierService,
        private coptionService : CoptionService,
        private qcodeService : QcodeService,
        private sessionService : SessionService,
        private el : ElementRef,
        private optionService : OptionService,
        private router : Router
    ){}
    ngOnInit(){
        this.activatedRoute.queryParams.subscribe((params: Params) => {
            this.qsetId = params['qsetId'];
            this.defaultLang = params['lang'];
            this.lang = params['lang'];
        });
        if(this.qsetId && this.qsetId !=''){
            this.uploadQData.qsetId = this.qsetId;
        }
        if(this.qsetId && this.qsetId !=''){
            this.uploadQData.lang = this.lang;
        }
        this.loadCourses();
        this.loadStreams();
        this.loadSessions();
        this.loadSourceOption();
    }
    codeFetch(){
        if(!this.qsetId){
            if(!this.tag) return this.notifier.alert('Danger','Enter tag First','danger',5000);
            if(this.tag && this.validateError) return this.notifier.alert('Danger','Remove Tag Error First','danger',5000);
        }
        let tagMap={Q:'qContent',ANS:"ans",SOLN:"qSolution",P :"passage", A:"optionA", B:"optionB",C:"optionC",D:"optionD",E:"optionE",MP:"optionP",MQ:"optionQ",MR:"optionR",MS:"optionS",MT:"optionT"};
        let infoTagMap={
            mp:"mp",
            mn:"mn",
            subject : "subject",
            course : "course",
            type : "qType",
            stream : "stream",
            level : "level",
            dlang : "defaultLang",
            topic : "topic",
            unit : "unit",
            title : "title",
            passageQs  : "passageQs",
            session : "session",
            stage : "stage",
            duration : "",
            source : "source",
            price : "price",
            tags : 'tags',
            forSell:'forSell'
        }; 
        let setFont = '';
        if(this.lang == 'HI' && this.font != ''){
            setFont = this.font;
        }
        this.errors=[];
        let prevTag = null;
        var elems=$("#wordHtml").children();
        let codes : any={alpha:'', beta:'', gamma:''};
        let info: any={};
        for(var i=0; i<elems.length; i++){
            var el=elems[i];
            var elTxt=$(el).text().trim();
            if(elTxt[0]=="[" && elTxt[1]==":"){
                var elTxtArr=elTxt.split("]"); 
                var elTagArr=elTxtArr[0].split("[:"); 
                var elTag=elTagArr[1];
                prevTag = elTag;
                if(elTag == 'ALPHA'){
                    codes.alpha = elTxtArr[1].trim();
                }
                if(elTag == 'BETA'){
                    codes.beta = elTxtArr[1].trim();
                }
                if(elTag == 'GAMMA'){
                    codes.gamma = elTxtArr[1].trim();
                }
                if(codes.alpha && codes.beta && codes.gamma){
                    break;
                }
            }
        }
        if(codes){
            this.qcodeService.getAllCodes(codes).subscribe(
                (res)=>{
                    codes = res;
                    for(var i=0; i<elems.length; i++){
                        var el=elems[i];
                        var elTxt=$(el).text().trim();
                        if(elTxt[0]=="[" && elTxt[1]==":"){
                            var elTxtArr=elTxt.split("]"); 
                            var elTagArr=elTxtArr[0].split("[:"); 
                            var elTagNameArr=elTagArr[1].split(".");
                            var elTag = elTagNameArr[0];
                            prevTag = elTag;
                         
                            if(elTag=="END"){
                                for(var key in info){
                                    rawQ[key]=info[key];
                                }
                                if(!rawQ.title && rawQ.qContent){
                                    let text= rawQ.qContent.replace(/(<([^>]+)>)/ig,"");
                                    rawQ.title = text.substr(0, 50);
                                }
                                if(!rawQ.defaultLang && this.lang){
                                    rawQ.defaultLang = this.lang;
                                }
                                if(!rawQ.qType){
                                    this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Question type is Compulsary in [:INFO] Tag type = MCQ, MAQ, ARQ, MTQ, TFQ, MPQ');
                                }
                                if( (rawQ.qType !='DTQ' && rawQ.qType !='TFQ') && (!rawQ.qOption.optionA  || !rawQ.qOption.optionB || !rawQ.qOption.optionC || !rawQ.qOption.optionD)){  
                                    this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' has Option A,B,C,D are Compulsary in Q.Type - MCQ, MAQ, ARQ');
                                }
                                if((rawQ.qType == 'MTQ') && (!rawQ.qOption.optionP  || !rawQ.qOption.optionQ || !rawQ.qOption.optionR || !rawQ.qOption.optionS)){
                                    this.errors.push(' Error : Question No - '+(this.questions.length + 1)+', MTQ Question Type P,Q,R,S options are Compulsary');
                                }
                                if(!rawQ.qContent){
                                    this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Question is Compulsary with [:Q] Tag ');
                                }
                                if(!rawQ.ans){
                                    this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Answer is Compulsary with [:ANS] Tag ');
                                }else{
                                    rawQ.ans=rawQ.ans.toUpperCase();
                                }
                                if(rawQ.havePassage){
                                    if(!rawQ.passage['passageContent']){
                                        this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Passage [:P] Tag is defined with no information ');
                                    }
                                }
                                // if(!rawQ.qSolution){
                                //     this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Solution is Compulsary with [:SOLN] Tag ');
                                // }
                                this.questions.push(rawQ);
                                break;
                            } 
                            if(elTag=="NQ"){
                                if(rawQ){
                                    for(var key in info){
                                        rawQ[key]=info[key];
                                    }
                                    if(!rawQ.title && rawQ.qContent){
                                        let text= rawQ.qContent.replace(/(<([^>]+)>)/ig,"");
                                        rawQ.title = text.substr(0, 50);
                                    }
                                    if(!rawQ.defaultLang && this.lang){
                                        rawQ.defaultLang = this.lang;
                                    }
                                    if( (rawQ.qType !='DTQ' && rawQ.qType !='TFQ') && (!rawQ.qOption.optionA  || !rawQ.qOption.optionB || !rawQ.qOption.optionC || !rawQ.qOption.optionD)){  
                                        this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' has Option A,B,C,D are Compulsary in Q.Type - MCQ, MAQ, ARQ');
                                    }
                                    if((rawQ.qType == 'MTQ') && (!rawQ.qOption.optionP  || !rawQ.qOption.optionQ || !rawQ.qOption.optionR || !rawQ.qOption.optionS)){
                                        this.errors.push(' Error : Question No - '+(this.questions.length + 1)+', MTQ Question Type P,Q,R,S options are Compulsary');
                                    }
                                    if(!rawQ.qContent){
                                        this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Question is Compulsary with [:Q] Tag ');
                                    }
                                    if(!rawQ.ans){
                                        this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Answer is Compulsary with [:ANS] Tag ');
                                    }else{
                                        rawQ.ans=rawQ.ans.toUpperCase();
                                    }
                                    if(rawQ.havePassage){
                                        if(!rawQ.passage['passageContent']){
                                            this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Passage [:P] Tag is defined with no information ');
                                        }
                                    }
                                    // if(!rawQ.qSolution){
                                    //     this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Solution is Compulsary with [:SOLN] Tag ');
                                    // }
                                    this.questions.push(rawQ);
                                }
                                var rawQ:any={
                                    title : null,
                                    qContent : null,
                                    qOption:{
                                        // optionA : null,optionB : null,optionC : null,optionD : null,
                                        // optionP : null,optionQ : null,optionR : null,optionS : null,
                                    },
                                    havePassage : false,
                                    passage:{},
                                    qType : null,
                                    ans : null,
                                    defaultFont : setFont,
                                    qSolution : null
                                };
                            }
                            else{
                                let data = $(el).html();
                                let tag="[:"+elTagNameArr.join(".")+"]";
                                data=data.replace(tag,"").trim();
                                if(['A', 'B', 'C','D','E','MP','MQ','MR','MS','MT'].indexOf(elTag)>-1){
                                    // if(data != ''){
                                        rawQ.qOption[tagMap[elTag]]=data;
                                    // }
                                    // else{
                                    //     this.errors.push(' Error : [:'+elTag+'] No information');
                                    // }
                                }
                                else{
                                    switch (elTag) {
                                        case 'P':
                                            rawQ['havePassage']=true;
                                            rawQ.passage['passageContent'] = data;
                                            if(elTagNameArr[1]){ rawQ['passageQs'] = elTagNameArr[1];}
                                            else{ rawQ['passageQs'] = 1;}
                                        break;
                                        case 'INFO' : 
                                            var dataExist = data.search('=');
                                            if(dataExist != -1){

                                                let data1= data.replace(/(<([^>]+)>)/ig,"");
                                                var infoArr = data1.split(',');
                                                for(var j=0;j<infoArr.length;j++){
                                                    var information = infoArr[j].split('=');
                                                    let infoTag = information[0].trim();
                                                    let infoContent="";
                                                    if(information[1]){
                                                        infoContent = information[1].trim();
                                                    }
                                                    if(infoContent !='' && ['mp','mn','subject','course','type','stream','level','dlang','topic','chapter','title','passageQs','session','stage','tags','source','price','forSell'].indexOf(infoTag)>-1){
                                                        if(infoTag == 'tags'){
                                                            let tags=[];
                                                            if(infoContent.search('|') != -1){
                                                                tags = infoContent.replace(' ','').split('|');
                                                                info[infoTagMap[infoTag]]=tags;
                                                            }else{
                                                                info[infoTagMap[infoTag]]=tags.push(infoContent.replace(' ',''));
                                                            }
                                                        }else{
                                                            info[infoTagMap[infoTag]]=infoContent;
                                                        }
                                                    }
                                                    else{
                                                        this.errors.push(' Error : Question No - '+(this.questions.length+1)+' [:INFO] '+infoTag+' = '+infoContent+' invalid Tag');
                                                    }
                                                }
                                                if(codes.alpha){
                                                    info[infoTagMap['stream']] = codes.alpha.stream;
                                                    info[infoTagMap['subject']] = codes.alpha.subject;
                                                    info[infoTagMap['unit']] = codes.alpha.unit;
                                                    info[infoTagMap['topic']] = codes.alpha.topic;
                                                    info['alpha'] = codes.alpha.id;
                                                }
                                                if(codes.beta){
                                                    info[infoTagMap['course']] = codes.beta.course;
                                                    info['beta'] = codes.beta.id;
                                                    info.qType= codes.beta.qType;
                                                    if(codes.beta.extraQType){
                                                        rawQ.extraQType= codes.beta.extraQType;
                                                    }
                                                }
                                                if(codes.gamma){
                                                    info[infoTagMap['level']] = codes.gamma.level;
                                                    info['gamma'] = codes.gamma.id;
                                                }
                                            }
                                        break;
                                        default:
                                            if(['Q', 'SOLN', 'ANS'].indexOf(elTag)>-1){
                                                if(elTag =='ANS'){
                                                    rawQ[tagMap[elTag]]=data.replace(/(<([^>]+)>)/ig,"");
                                                }
                                                else rawQ[tagMap[elTag]]=data;
                                            }
                                            else{
                                                if('ALPHA' != elTag && 'BETA' != elTag && 'GAMMA' != elTag){
                                                    this.errors.push(' Error : Question No - '+(this.questions.length+1)+' [:'+elTag+'] invalid Tag');
                                                }
                                            }
                                        break;
                                    }
                                }
                            }
                        }
                        else if((elTxt[0]==":")){
                            this.errors.push(' Error : Question No - '+(this.questions.length+1)+' Do not start a new line with ":" special character  ', elTxt);
                        }
                        else{
                            var con = $(el).html();
                            if(['A', 'B', 'C','D','E','MP','MQ','MR','MS','MT'].indexOf(prevTag)>-1){
                                rawQ.qOption[tagMap[prevTag]] += '<br>'+con;
                            }
                            else if(prevTag == 'P'){
                                rawQ.passage['passageContent'] += '<br>'+con;
                            }
                            else if(['Q', 'SOLN', 'ANS'].indexOf(prevTag)>-1){
                                rawQ[tagMap[prevTag]] += '<br>'+con;
                            }
                            else{
                                this.errors.push('Error : Question No -'+(this.questions.length+1)+' Some Content : -'+con);
                            }
                        }
                    }
                    if(this.errors.length > 0){
                        return this.notifier.alert('danger','Error in your File. Follow specific pattern','danger',5000); 
                    }
                },
                (err)=>{console.log(err)}
            );
        }
        this.loadMathJax();
    }
    fetch(){
        if(!this.qsetId){
            if(!this.tag) return this.notifier.alert('Danger','Enter tag First','danger',5000);
            if(this.tag && this.validateError) return this.notifier.alert('Danger','Remove Tag Error First','danger',5000);
        }
        let tagMap={Q:'qContent',ANS:"ans",SOLN:"qSolution",P :"passage", A:"optionA", B:"optionB",C:"optionC",D:"optionD",E:"optionE",MP:"optionP",MQ:"optionQ",MR:"optionR",MS:"optionS",MT:"optionT"};
        let infoTagMap={
            mp:"mp",
            mn:"mn",
            subject : "subject",
            course : "course",
            type : "qType",
            stream : "stream",
            level : "level",
            dlang : "defaultLang",
            topic : "topic",
            unit : "unit",
            title : "title",
            passageQs  : "passageQs",
            session : "session",
            stage : "stage",
            source : "source",
            price : "price",
            tags : 'tags',
            forSell:'forSell'
        };
        let setFont = '';
        if(this.lang == 'HI' && this.font != ''){
            setFont = this.font;
        }
        this.errors=[];
        let prevTag = null;
        var elems=$("#wordHtml").children();
        let info: any={};
        for(var i=0; i<elems.length; i++){
            var el=elems[i];
            var elTxt=$(el).text().trim();
            if(elTxt[0]=="[" && elTxt[1]==":"){
                var elTxtArr=elTxt.split("]"); 
                var elTagArr=elTxtArr[0].split("[:"); 
                var elTagNameArr=elTagArr[1].split(".");
                var elTag = elTagNameArr[0];
                prevTag = elTag;
                if(elTag=="END"){
                    for(var key in info){
                        rawQ[key]=info[key];
                    }
                    if(!rawQ.title && rawQ.qContent){
                        let text= rawQ.qContent.replace(/(<([^>]+)>)/ig,"");
                        rawQ.title = text.substr(0, 50);
                    }
                    if(!rawQ.defaultLang && this.lang){
                        rawQ.defaultLang = this.lang;
                    }
                    if( (rawQ.qType !='DTQ' && rawQ.qType !='TFQ') && (!rawQ.qOption.optionA  || !rawQ.qOption.optionB || !rawQ.qOption.optionC || !rawQ.qOption.optionD)){  
                        this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' has Option A,B,C,D are Compulsary in Q.Type - MCQ, MAQ, ARQ');
                    }
                    if((rawQ.qType == 'MTQ') && (!rawQ.qOption.optionP  || !rawQ.qOption.optionQ || !rawQ.qOption.optionR || !rawQ.qOption.optionS)){
                        this.errors.push(' Error : Question No - '+(this.questions.length + 1)+', MTQ Question Type P,Q,R,S options are Compulsary');
                    }
                    if(!rawQ.qContent){
                        this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Question is Compulsary with [:Q] Tag ');
                    }
                    if(!rawQ.ans){
                        this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Answer is Compulsary with [:ANS] Tag ');
                    }else{
                        rawQ.ans=rawQ.ans.toUpperCase();
                    }
                    if(rawQ.havePassage){
                        if(!rawQ.passage['passageContent']){
                            this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Passage [:P] Tag is defined with no information ');
                        }
                    }
                    // if(!rawQ.qSolution){
                    //     this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Solution is Compulsary with [:SOLN] Tag ');
                    // }
                    this.questions.push(rawQ);
                    break;
                } 
                if(elTag=="NQ"){
                    if(i>1){
                        for(var key in info){
                            rawQ[key]=info[key];
                        }
                        if(!rawQ.title && rawQ.qContent){
                            let text= rawQ.qContent.replace(/(<([^>]+)>)/ig,"");
                            rawQ.title = text.substr(0, 50);
                        }
                        if(!rawQ.defaultLang && this.lang){
                            rawQ.defaultLang = this.lang;
                        }
                        if(rawQ.qType && rawQ.qType == 'ARQ'){
                            rawQ.qOption['optionA'] = "Both A & R are true and R is the correct explanation of A.";
                            rawQ.qOption['optionB'] = "Both A & R are true but R is NOT the correct explanation of A.";
                            rawQ.qOption['optionC'] = "A is true but R is false.";
                            rawQ.qOption['optionD'] = "A is false but R is true.";
                        }
                        if( (rawQ.qType !='DTQ' && rawQ.qType !='TFQ') && (!rawQ.qOption.optionA  || !rawQ.qOption.optionB || !rawQ.qOption.optionC || !rawQ.qOption.optionD)){  
                            this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' has Option A,B,C,D are Compulsary in Q.Type - MCQ, MAQ, ARQ');
                        }
                        if((rawQ.qType == 'MTQ') && (!rawQ.qOption.optionP  || !rawQ.qOption.optionQ || !rawQ.qOption.optionR || !rawQ.qOption.optionS)){
                            this.errors.push(' Error : Question No - '+(this.questions.length + 1)+', MTQ Question Type P,Q,R,S options are Compulsary');
                        }
                        if(!rawQ.qContent){
                            this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Question is Compulsary with [:Q] Tag ');
                        }
                        if(!rawQ.ans){
                            this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Answer is Compulsary with [:ANS] Tag ');
                        }else{
                            rawQ.ans=rawQ.ans.toUpperCase();
                        }
                        if(rawQ.havePassage){
                            if(!rawQ.passage['passageContent']){
                                this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Passage [:P] Tag is defined with no information ');
                                
                            }
                        
                        }
                        // if(!rawQ.qSolution){
                        //     this.errors.push(' Error : Question No - '+(this.questions.length + 1)+' Solution is Compulsary with [:SOLN] Tag ');
                        // }
                        this.questions.push(rawQ);
                    }
                    var rawQ : any={
                        title : null,
                        qContent : null,    
                        qOption:{
                            // optionA : '',optionB : '',optionC : '',optionD : '',
                            // optionP : '',optionQ : '',optionR : '',optionS : '',
                        },
                        havePassage : false,
                        passage:{},
                        tags : [],
                        qType : null,
                        ans : null,
                        defaultFont : setFont,
                        qSolution : null};
                }
                else{
                    let data = $(el).html();
                    let tag="[:"+elTagNameArr.join(".")+"]";
                    data=data.replace(tag,"").trim();
                    if(['A', 'B', 'C','D','E','MP','MQ','MR','MS','MT'].indexOf(elTag)>-1){
                            rawQ.qOption[tagMap[elTag]]=data;
                    }
                    else{
                        switch (elTag) {
                            case 'P':
                                    rawQ.havePassage=true;
                                    rawQ.passage['passageContent'] = data;
                                    if(elTagNameArr[1]){ rawQ['passageQs'] = elTagNameArr[1];}
                                    else{ rawQ['passageQs'] = 1;}
                            break;
                            case 'INFO' : 
                                var dataExist = data.search('=');
                                if(dataExist != -1){
                                    let data1= data.replace(/(<([^>]+)>)/ig,"");
                                    var infoArr = data1.split(',');
                                    for(var j=0;j<infoArr.length;j++){
                                        if(infoArr[j] != ''){
                                            var information = infoArr[j].split('=');
                                            let infoTag = information[0].trim();
                                            let infoContent="";
                                            if(information[1]){
                                                infoContent = information[1].trim();
                                            }
                                            if(infoContent !='' && ['mp','mn','subject','course','type','stream','level','unit','dlang','topic','title','session','tags','stage','source','price', 'forSell'].indexOf(infoTag)>-1){
                                                if(infoTag == 'tags'){
                                                    let tags=[];
                                                    if(infoContent.search('|') != -1){
                                                        tags = infoContent.replace(' ','').split('|');
                                                        info[infoTagMap[infoTag]]=tags;
                                                    }else{
                                                        info[infoTagMap[infoTag]]=tags.push(infoContent.replace(' ',''));
                                                    }
                                                }else{
                                                    info[infoTagMap[infoTag]]=infoContent;
                                                }
                                            }
                                            else{
                                                this.errors.push(' Error : Question No - '+(this.questions.length+1)+' [:INFO] '+infoTag+' = '+infoContent+' invalid Tag');
                                            }
                                        }
                                    }
                                }
                            break;
                            default:
                                
                                if(['Q', 'SOLN', 'ANS'].indexOf(elTag)>-1){
                                    if(elTag =='ANS'){
                                        rawQ[tagMap[elTag]]=data.replace(/(<([^>]+)>)/ig,"").trim();
                                    }
                                    else rawQ[tagMap[elTag]]=data;
                                }
                                else{
                                    this.errors.push(' Error : Question No - '+(this.questions.length+1)+' [:'+elTag+'] invalid Tag');
                                }
                            break;
                        }
                    }
                }
            }
            // (elTxt[0]=="[" && elTxt[1]!=":") || 
            else if((elTxt[0]==":")){
                this.errors.push(' Error : Question No - '+(this.questions.length+1)+' Do not start a new line with ":" special character  ', elTxt);
            }
            else{
                var con = $(el).html();
                if(['A', 'B', 'C','D','E','MP','MQ','MR','MS','MT'].indexOf(prevTag)>-1){
                    rawQ.qOption[tagMap[prevTag]] += '<p>'+con+'</p>';
                }
                else if(prevTag == 'P'){
                    rawQ.passage['passageContent'] += '<p>'+con+'</p>';
                }
                else if(['Q', 'SOLN', 'ANS'].indexOf(prevTag)>-1){
                    rawQ[tagMap[prevTag]] += '<p>'+con+'</p>';
                }
                else{
                    this.errors.push('Error : Question No -'+(this.questions.length+1)+' Some Content : -'+con);
                }
            }
        }
        if(this.errors.length > 0){
            return this.notifier.alert('danger','Error in your File. Follow specific pattern','danger',5000); 
        }
        this.loadMathJax();
    }
    tableFormatFetch(){
        if(!this.qsetId){
            if(!this.tag) return this.notifier.alert('Danger','Enter tag First','danger',5000);
            if(this.tag && this.validateError) return this.notifier.alert('Danger','Remove Tag Error First','danger',5000);
        }
        let optionKey=['optionA','optionB','optionC','optionD']
        // this.errors=[];
        let setFont = '';
        if(this.lang == 'HI' && this.font != ''){
            setFont = this.font;
        }
        var elems=$("#wordHtml tbody").children();
        var question : any={};
        for(var i=0; i<elems.length; i++){
            let row=$(elems[i]).html();
            if(row){
                let columnArr=row.split('<td>');
                columnArr.splice(0,1);
                let newQ = $(columnArr[0]).text().trim();
                if(newQ[0]&& newQ[0] !=""){
                    if(i>1){
                        this.questions.push(question);
                    }
                    question={
                        title : null,
                        havePassage : false,
                        passage:{},
                        qOption:{},
                        tags : [],
                        qType : 'MCQ',
                        ans : null,
                        defaultLang : this.lang,
                        defaultFont : setFont,
                        qSolution : null
                    };
                    question['qContent'] = columnArr[1].replace('</td>','');
                    question['ans'] = $(columnArr[2]).text().trim();
                    if(question.qContent){
                        let text= $(question.qContent).text();
                        question['title'] = text.substr(0, 50);
                    }
                    var option = 0;
                    if(columnArr[3]){
                        let qType = $(columnArr[3]).text().trim();
                        if(qType){
                            question['qType']=qType;
                        }
                    }
                    if(columnArr[4]){
                        let marks = $(columnArr[4]).text().trim();
                        let marksArr = marks.split(',');
                        if(marksArr[0]){
                            question['mp'] = marksArr[0];
                        }
                        if(marksArr[1]){
                            question['mn'] = marksArr[1];
                        }
                    }
                    if(columnArr[5]){
                        question['qSolution'] = columnArr[5].replace('</td>','');
                        
                    }
                }else{
                    if(columnArr[1] && optionKey[option]){
                        question.qOption[optionKey[option]] = columnArr[1].replace('</td>','');
                        option++;
                    }
                }
            }
        }
        if(this.questions.length>0){
            this.questions.push(question);
        }
        if(this.errors.length > 0){
            return this.notifier.alert('danger','Error in your File. Follow specific pattern','danger',5000); 
        }
        this.loadMathJax();
    }
    loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub]);
        }, 10);
    }
    getFilePath(id: string){
        this.content = null;
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#' + id);
        let fileCount: number = inputEl.files.length;
        if(fileCount<1) return this.notifier.alert('danger',' No file Selected','danger',4000);
        let formData = new FormData();
        let nameArr = inputEl.files[0].name.split('.');
        let extension = nameArr[nameArr.length-1];
		if(fileCount > 0 && (extension =='docx')){
            formData.append('ekFile', inputEl.files.item(0));
            this.panelLoader = "show";
			this.fmService.uploadFileLocal(formData).subscribe(
				res => {
					if (id == 'questionsFile') {
                        this.uploadQData.url = res.url;
                    }
                    this.uploadFile();
				},
				err =>console.log(err)
			);
        } 
        else{
            return this.notifier.alert('danger','upload "DOCX" file only','danger',5000);
        }
    }
    uploadFile(){
        this.panelLoader="Processing";
        this.uploadQData.lang = this.lang;
        this.questionService.uploadQuestionsDocx(this.uploadQData).subscribe(
            (data)=> {this.content=data;
                this.displayIns=false;
                this.questions=[];
                this.panelLoader="none";
            },
            (err)=>{
                console.log(err);
                this.panelLoader ="none";
            }
        );
    }
    getFilePathTable(id: string){
        this.content = null;
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#' + id);
		let fileCount: number = inputEl.files.length;
        if(fileCount<1) return this.notifier.alert('danger',' No file Selected','danger',4000);
        let formData = new FormData();
        let nameArr = inputEl.files[0].name.split('.');
        let extension = nameArr[nameArr.length-1];
		if(fileCount > 0 && (extension =='docx')){
            formData.append('ekFile', inputEl.files.item(0));
            this.panelLoader = "show";
			this.fmService.uploadFileLocal(formData).subscribe(
				res => {
					if (id == 'questionsFiless') {
                        this.uploadQData.url = res.url;
                    }
                    this.readTableFormatFile();
				},
				err =>console.log(err)
			);
        } 
        else{
            return this.notifier.alert('danger','upload "DOCX" file only','danger',5000);
        }
    }
    readTableFormatFile(){
        this.panelLoader="Processing";
        this.uploadQData.lang = this.lang;
        this.questionService.readTableFormatQsDocx(this.uploadQData).subscribe(
            (data)=> {
                this.content=data.questions;
                this.questions=data.questions;
                this.displayIns=false;
                this.newUpload = false;
                this.panelLoader="none";
                this.loadMathJax();
            },
            (err)=>{
                console.log(err);
                this.panelLoader ="none";
            }
        );
    }
    saveFileQuestions(){
        this.fileStatus="Processing";
        let data:any={
            questions : this.questions,
        };
        if(this.qsetId){
            let fieldSet="question";
            if(this.defaultLang != this.lang){
                fieldSet="variantQuestion";
            }
            data.qsetId = this.qsetId;
            this.qsetService.saveFileQuestions(data,fieldSet).subscribe(
                (res)=>{
                    this.fileStatus="Normal";
                    this.notifier.alert('success','Upload Sucessfully','success',5000);
                    this.router.navigate(['/ims/qbank/qset/view'],{queryParams : {qsetId : this.qsetId}});
                },
                (err)=>{
                    this.notifier.alert(err.code, err.message, 'danger',5000);
                }
            );
        }
        else{
            data.lang = this.lang;
            data.tag=this.tag;
            this.questionService.saveQBFileQuestions(data).subscribe(
                (res)=>{
                    this.fileStatus="Normal";
                    this.notifier.alert('success','Upload Sucessfully','success',5000);
                    this.router.navigate(['/ims/qbank/question']);
                },
                (err)=>{
                    this.notifier.alert(err.code, err.message, 'danger',5000);
                    this.fileStatus="Normal";
                }
            );
        }
    }
    showIns(){
        this.displayIns=!this.displayIns;
    }
    loadCourses(){
		let filter={};
		if(this.courseId){
			filter ={id : this.courseId}; 
		}
		else{
			filter ={option:'COURSE'};
		}
		this.coptionService.getCoption(filter).subscribe(
			res=>{
				if(!res[0]){
					this.courses=[res];
				}else{
					this.courses=res;
				}
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.streams=res;
				if(this.streamId && this.streams.length >0){
					let newStream = [];
					for(var i=0;i<this.streams.length;i++){
						for(var j=0;j<this.streamId.length;j++){
							if( this.streams[i].id == this.streamId[j]){
								newStream.push(this.streams[i]);
							}
						}
					}
					this.streams = newStream;
				}
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
    }
    loadSessions(){
		this.sessionService.getSession({status:true}).subscribe(
			res=>{
				this.sessions=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
    }
    loadSourceOption() {
        // this.frmLoader = "show";
        this.optionService.getOption({ name: "SOURCE" }).subscribe(
            (data) => {
				this.sourceList = data;
                // this.frmLoader = "none";
            },
            (err) => {
                // this.frmLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
	}
    changeTag(){
        let filter= {defaultLang : this.lang, tags : this.tag,limit:1};
        this.validateError=null;
        if(this.tag){
            this.questionService.getQuestion(filter,true).subscribe(
                res=>{
                    if(res && res.body.length>0){
                        this.validateError= 'This Tag Questions already exist with selected language';
                        this.notifier.alert('danger','This Tag Questions already exist with selected language','danger',5000);
                    }else{
                        this.notifier.alert('Success','Valid Tag','success',1000);
                    }
                },
                err=>{
                    this.notifier.alert(err.code,err.message,'danger',5000);
                }
            );
        }
    }
}