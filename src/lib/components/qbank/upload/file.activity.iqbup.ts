import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { uploadFileContent } from './file';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { QuestionService } from "../../../services/question.service";
import { QsetService } from "../../../services/qset.service";
import { FilemanagerService } from "../../filemanager/filemanager.service";
import { CoptionService } from "../../../services/coption.service";
import { QcodeService } from "../../../services/qcode.service";
import { SessionService } from '../../../services/session.service';
import { OptionService } from '../../../services/option.service';
import { DialogModule } from 'primeng/primeng';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';

export const ROUTES:Routes=[
    {path: '', component: uploadFileContent, pathMatch:'full'},
];

@NgModule({ 
    declarations:[uploadFileContent],
    imports:[RouterModule.forChild(ROUTES),CommonModule, FormsModule, ReactiveFormsModule,DialogModule,SafeHtmlPipeModule],
    providers : [QuestionService,FilemanagerService,QsetService,QcodeService,CoptionService,SessionService,OptionService],
    exports:[uploadFileContent]
})
export class QuestionUploadFileModule { }

