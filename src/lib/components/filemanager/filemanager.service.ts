import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../../ezukit.config';

@Injectable()
export class FilemanagerService{
    AWS:any =(window as any).AWS;

    constructor(private http:HttpClient){
        // this.AWS.config.update({region: 'ap-south-1'});
     }
    addFilem(fileInfo): Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/filemanager/file", fileInfo);
    }
    uploadFile(fileObj:any) : Observable<any>{

        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/filemanager/filem/upload", fileObj);
    }

    getFiles(filter?:any, withHeaders?:boolean) : Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe="response";
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/filemanager/files", opts);
    }
    getOneFile(id?:string) : Observable<any>{

        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/filemanager/filem/"+id);
    }
    deleteFile(fileId):Observable<any>{
        return this.http.delete<any>(EdukitConfig.BASICS.API_URL+"/filemanager/filem/"+fileId);
    }

    uploadFileLocal(fileObj: any): Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL + "/public/file/upload", fileObj);
    }
    uploadSiteMap(fileObj:any): Observable<any>{
        return this.http.post<any>(EdukitConfig.BASICS.API_URL+"/public/file/upload-sitemap",fileObj);
    }

    getAwsTempCreds() : Observable<any>{
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/public/file/get-temp-awstkn");
    }

    uploadDirctToS3(awsCreds, reqFile, fileName, clientPath){
        if(!awsCreds){
            return;
        }
        this.AWS.config={
            accessKeyId: awsCreds.AccessKeyId,
            secretAccessKey: awsCreds.SecretAccessKey,
            sessionToken:awsCreds.SessionToken,
            region:'ap-south-1'
        }

        let s3=new this.AWS.S3({apiVersion: '2012-11-05', Bucket:'eduhubstorage'});
        return new Promise((resolve, reject)=>{
            let time= new Date();
            let s3FilePath= clientPath+'/'+time.getTime()+'-'+fileName;
            s3.upload({
                Bucket: 'eduhubstorage',
                Key: s3FilePath,
                Body: reqFile,
                ACL: "public-read"
            },
            function(err, data){
                if(err) {
                    return reject(err)
                }
                if(data){
                    console.log(data);
                    return resolve({filePath:s3FilePath})
                }
            }).on('httpUploadProgress', function(evt) {
                console.log('Progress:', evt.loaded, '/', evt.total);
            })
        })


        // var upload=new this.AWS.S3.ManagedUpload({
        //     params: {
        //         Bucket: 'eduhubstorage',
        //         Key: s3FilePath,
        //         Body: reqFile,
        //         ACL: "public-read"
        //     }
        // })
        // // var promise=  upload.promise();
        // // console.log("Upload Promise");
        // // promise.then(
        // //     (data)=> {
        // //         console.log(data);
        // //         if(data){
        // //             return {error:null,filePath:s3FilePath};
        // //         }
        // //     },
        // //     (err)=> {
        // //         console.log(err);
        // //         return {error:err};
        // //     }
        // // );
    }
    uploadDirctToS3Obs(awsCreds, reqFile, fileName, clientPath): Observable<any>{
        let s3=new this.AWS.S3({apiVersion: '2012-11-05', Bucket:'eduhubstorage'});
        if(!awsCreds){
            return;
        }
        this.AWS.config={
            accessKeyId: awsCreds.AccessKeyId,
            secretAccessKey: awsCreds.SecretAccessKey,
            sessionToken:awsCreds.SessionToken,
            region:'ap-south-1'
        }
        // this.AWS.config.update({region: 'us-east-1'});
        return new Observable(function(observer){
            let time= new Date();
            let s3FilePath= clientPath+'/'+time.getTime()+'-'+fileName;
            s3.upload({
                Bucket: 'eduhubstorage',
                Key: s3FilePath,
                Body: reqFile,
                ACL: "public-read"
            }, {leavePartsOnError: true},
            function(err, data){
                if(err) {
                    observer.error(err)
                }
                if(data){
                    console.log(data);
                    observer.next( {progress:1, done:true, body:data});
                    observer.complete()
                }
            }).on('httpUploadProgress', function(evt:any) {
                observer.next( {progress:evt.loaded/evt.total});
            })
        });



        // var upload=new this.AWS.S3.ManagedUpload({
        //     params: {
        //         Bucket: 'eduhubstorage',
        //         Key: s3FilePath,
        //         Body: reqFile,
        //         ACL: "public-read"
        //     }
        // })
        // // var promise=  upload.promise();
        // // console.log("Upload Promise");
        // // promise.then(
        // //     (data)=> {
        // //         console.log(data);
        // //         if(data){
        // //             return {error:null,filePath:s3FilePath};
        // //         }
        // //     },
        // //     (err)=> {
        // //         console.log(err);
        // //         return {error:err};
        // //     }
        // // );
    }
    getVideos(filter?:any, withHeaders?:boolean) : Observable<any>{
        let opts:any={params:filter};
        if(withHeaders){
            opts.observe="response";
        }
        return this.http.get<any>(EdukitConfig.BASICS.API_URL+"/filemanager/videos", opts);
    }
}
