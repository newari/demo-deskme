import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FileinputComponent } from './fileinput.component';
import { FilemanagerService } from './filemanager.service';

@NgModule({
    declarations:[FileinputComponent],
    imports:[CommonModule, FormsModule, ReactiveFormsModule],
    providers:[FilemanagerService],
    exports:[FileinputComponent]
})
export class FileinputModule {}
