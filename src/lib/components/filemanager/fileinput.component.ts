import { Component, Input, Output, EventEmitter, SimpleChange, forwardRef, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { FilemanagerService } from './filemanager.service';
import { NotifierService } from '../notifier/notifier.service';

@Component({
    selector:'ek-fileinput',
    templateUrl:'./fileinput.component.html',
    providers: [
        {
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => FileinputComponent),
        multi: true
        }
    ]
})
export class FileinputComponent implements ControlValueAccessor{
    panelLoader='none';
    files:any[]=[];
    selectedFileIndex:any=null;
    selectedFileUrl:string=null;

    activeBox:string="listBx"
    fileType:any;
    @Input() type:string; //File Type png, gif, jpg etc
    @Input() typeCategory:string; //File Type Category IMAGE, PDF, DOC etc
    @Input() owner:string; //User ID
    @Input('fmModal') fmModal={status:'hide'};
    @Input('customClass') customClass="";
    showURL: boolean;
    selectedPdfIndex: number;
    countBase=1;
    filter:any;
    totalRecords: any;
    @Input('fmModalStatus')///to detect hide show changes
    set fmModalStatus(value:string){
        if(value=='show'){
            this.loadFiles();
        }
        this.fmModal.status=value;
    }
    get fmModalStatus():string{
        return this.fmModal.status;
    }

    @Output('done') done=new EventEmitter();
    @Output('onFileSelect') onFileSelect=new EventEmitter();
    // fmModalStatus=this.fmModal.status;
    constructor(
        private notifier:NotifierService,
        private el: ElementRef,
        private fmService:FilemanagerService

    ){
        this.fileType={
            "JPG":{
                category:'IMAGE',
                type:'JPG',
                icon:'file-image-o',
            },
            "JPEG":{
                category:'IMAGE',
                type:'JPG',
                icon:'file-image-o',
            },
            "PNG":{
                category:'IMAGE',
                type:'PNG',
                icon:'file-image-o',
            },
            "GIF":{
                category:'IMAGE',
                type:'GIF',
                icon:'file-image-o',
            },
            "PDF":{
                category:'PDF',
                type:'PDF',
                icon:'file-pdf-o',
            },
            "DOC":{
                category:'DOC',
                type:'DOCX',
                icon:'file-word-o',
            },
            "DOCX":{
                category:'DOC',
                type:'DOCX',
                icon:'file-word-o',
            },
            "TXT":{
                category:'TEXT',
                type:'TXT',
                icon:'file-alt-o',
            },
            "XLSX":{
                category:'EXCEL',
                type:'XLSX',
                icon:'file-excel-o',
            },
            "XLS":{
                category:'EXCEl',
                type:'XLSX',
                icon:'file-excel-o',
            },
            "CSV":{
                category:'EXCEL',
                type:'CSV',
                icon:'file-excel-o',
            },
            "ZIP":{
                category:'ARCHIVE',
                type:'ZIP',
                icon:'file-archive-o',
            },
            "RAR":{
                category:'ARCHIVE',
                type:'RAR',
                icon:'file-archive-o',
            }
        }
    }

    ngOnInit():void{

        this.files=[
        ];
    }





    loadFiles(fltr?:any){
        this.panelLoader="show";
        //  fltr={page:1, limit:10, sort:'createdAt DESC'};
        if(!fltr){
            fltr={page:1, limit:12};
            if(this.type){
                fltr.type=this.type;
            }

        }
        if(this.typeCategory){
            fltr.category=this.typeCategory;
        }
        if(this.owner){
            fltr.owner=this.owner;
        }
        if(!fltr.page){
            fltr.page=1;
        }
        if(!fltr.limit){
            fltr.limit=12;
        }
        fltr.sort='createdAt DESC';
        this.fmService.getFiles(fltr, true).subscribe(
            res=>{
                this.files=res.body;
                this.totalRecords = res.headers.get('totalRecords')||0;
                this.panelLoader="none";
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger');
            }
        )
    }

    selectFile(fileIndex){
        this.selectedFileIndex=fileIndex;
    }

    useSelectedFile(){
        if(this.selectedFileIndex!==null){
            this.selectedFileUrl=this.files[this.selectedFileIndex].url;
            this.onFileSelect.emit(this.files[this.selectedFileIndex]);
            this.propagateChange(this.selectedFileUrl);
            this.fmModal.status="hide";
        }


    }
    cancelSelectedFile(){
        this.selectedFileIndex=null;
    }
    deleteSelectedFile(){
        let conf=confirm("Are you sure to delete this file?");
        if(!conf){
            return
        }
        if(this.selectedFileIndex!==null){
            this.panelLoader="show";
            this.fmService.deleteFile(this.files[this.selectedFileIndex].id).subscribe(
                res=>{
                    this.files.splice(this.selectedFileIndex, 1);
                    this.selectedFileIndex=null;
                    this.panelLoader="none";
                },
                err=>{
                    this.panelLoader="none";
                    this.notifier.alert(err.code, err.message, 'danger');
                }
            )
        }
    }
    writeValue(value: any) {
        if (value !== undefined) {
            this.selectedFileUrl = value;
        }

    }

    propagateChange = (_: any) => {};

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched() {}

    uploadFile() {
    	//locate the file element meant for the file upload.
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#fmFile');
    	//get the total amount of files attached to the file input.
        let fileCount: number = inputEl.files.length;
    	//create a new fromdata instance
        this.panelLoader="show";
    	//check if the filecount is greater than zero, to be sure a file was selected.
        if (fileCount > 0) { // a file was selected
            let formData = new FormData();
            //append the key name 'photo' with the first file in the element
			formData.append('fmFile', inputEl.files.item(0));
            //call the angular http method
			this.fmService.uploadFile(formData).subscribe(
                res=>{
                    this.files.unshift(res);
                    this.activeBox="listBx";
                    this.panelLoader="none";
                },
                err=>{
                    this.panelLoader="none";
                    this.notifier.alert(err.code, err.message, 'danger');
                    // this.fh.upload1Status=err.message;

                }
            )

    	}
    }
    copyText(index?:number){
        this.showURL=true;
        this.selectedPdfIndex=index

        let elem:any = window.document.getElementById('fle'+this.files[index].id);
        console.log(elem);
        elem.select();
        /* Select the text field */
        // elem.setSelectionRange(0, 99999); /*For mobile devices*/

        /* Copy the text inside the text field */
        window.document.execCommand('copy');
        this.notifier.alert("Coppied!", "Successfully Copied!", 'success', 500);
        // window.document.body.removeChild(elem);
    }

    paginate(e) {
        if (!this.filter) {
            this.filter = {};
        }
        this.countBase = e.rows * e.page + 1;
        this.filter.page = (e.page + 1);
        this.filter.limit = e.rows;
        this.loadFiles(this.filter);
    }
    openImg(url){
        window.open(url,'new')
    }
    setSelectedFile(event,index){
        this.selectedFileIndex=null;
        if(event.target&&event.target.checked){
            this.selectedFileIndex=index;
        }else{
            this.selectedFileIndex=null;
        }
    }
}
