import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { Session } from "../../models/session.model";
import { Center } from "../../models/center.model";
import { Coption } from "../../models/coption.model";
import { Product } from "../../models/product.model";
import { UserService } from "../../services/user.service";
import { CoptionService } from "../../services/coption.service";
import { SessionService } from "../../services/session.service";
import { CenterService } from "../../services/center.service";
import { ProductService } from "../../services/product.service";
import { NotifierService } from "../notifier/notifier.service";
import { StudentService } from '../../services/student.service';

import * as moment from  'moment';
import { Student } from "../../models/student.model";
@Component({
    selector:'user-product',
    templateUrl:'./user-product.component.html'
})

export class UserProduct implements OnInit{
    student:Student;
    panelLoader="none";
    userProducts:any[];
    showAddProductModal:boolean;
    productAddForm:FormGroup;
    sessions:Session[];
    centers:Center[];
    productCategories:Coption[];
    productTypes:Coption[];
    streams:Coption[];
    products:Product[];
    formStatus:string;
    extednAccessValidity:boolean
    validTill:any;
    selectedIndex:number;
    routes:any={
        '3030707264637463636c7370':'admission',
        '5b47f7ef1e899567332d1d0a':'testment',
        '5b267b05851670048a6820a9':'conventional-exam',
        '5cf0cfc4e6636f4d815ccb0a':'course-creator',
        '5e96d9bbf3c3f6fc7ad17cd1':'e-books'
    }
    @Input('studentId') studentId:string;
    @Input('route') route:string;
    @Input('title') title:string;
    constructor(
        private studentService:StudentService,
        private userService:UserService,
        private fb:FormBuilder,
        private coptionService:CoptionService,
        private sessionService:SessionService,
        private centerService:CenterService,
        private productService:ProductService,
		private notifier: NotifierService
    ){
       
    }
     ngOnInit(){
        this.productAddForm=this.fb.group({
            productCategory:[null, Validators.required],
            productType:[null, Validators.required],
            product:[null, Validators.required],
            validTill:['']
        });

        if(this.studentId){
            this.getStudent(this.studentId);
        }
     }

     
    getUserProducts(){
        this.panelLoader="show";
        this.userService.getUserProduct({user:this.student.user.id}).subscribe(
            res=>{
                this.userProducts=res;
                this.panelLoader="none";
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
                this.panelLoader="none";
            }
        )
    }
    addUserProductAndFulfill(){
        this.formStatus="Processing";
        let frm=this.productAddForm.value;
        let data:any={
            user:this.student.user.id, product:frm.product,
        }
        if(frm.validTill&& moment(frm.validTill).isValid()==true){
            data.validTill=frm.validTill;
        }
        this.userService.addUserProduct(data).subscribe(
            res=>{
                this.notifier.alert('Done', "Product/Program added and fulfilled successfully!", "success", 2000);
                this.formStatus="none";
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
                this.formStatus="none";
            }
        )
    }
    showAddForm(){
        this.showAddProductModal=true;
        this.loadProductCats();
    }
    loadSessions(){
        let filter:any={status:true};
        if(this.sessions){
            filter.id=this.sessions;
        }
        this.sessionService.getSession(filter).subscribe(
            res=>{
                
                this.sessions=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }

    loadCenters(){
        let filter:any={status:true};
        if(this.centers){
            filter.id=this.centers;
        }
        this.centerService.getCenter(filter).subscribe(
            res=>{
                
                this.centers=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }
    loadProductCats(){
        this.coptionService.getCoptionProductCategory().subscribe(
            res=>{
                this.productCategories=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }
    loadProductTypes(){
        let filter:any={
            option:'PRODUCTTYPE'
        }
        if(this.productAddForm.value&&this.productAddForm.value.productCategory&&this.productAddForm.value.productCategory!='null'){
            filter.parentOption=this.productAddForm.value.productCategory
        }
        this.coptionService.getCoption(filter).subscribe(
            res=>{
                this.productTypes=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }
    loadStreams(){
        this.coptionService.getCoption({option:'STREAM', status:true}).subscribe(
            res=>{
                this.streams=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }

    loadProducts(){
        let frm=this.productAddForm.value;
        if(!frm.productCategory){
            this.notifier.alert("Error", "Product Category required!", "danger", 1000);
            return;
        }
        if(!frm.productType){
            this.notifier.alert("Error", "Product Type required!", "danger", 1000);
            return;
        }
        this.productService.getProduct({productCategory:frm.productCategory, productType:frm.productType, limit:100}).subscribe(
            res=>{
                this.products=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }

    extendValidity(){ 
        if(this.userProducts[this.selectedIndex]&& moment(this.userProducts[this.selectedIndex].validTill).isBefore(this.validTill)){
            let userProductId= this.userProducts[this.selectedIndex].id;
            this.userService.updateUserProduct(userProductId, moment(this.validTill).endOf('day')).subscribe(
                res=>{
                    this.userProducts[this.selectedIndex].validTill= moment(this.validTill).endOf('day')
                    this.selectedIndex=null;
                },
                err=>{

                }
            );
        }else{
            return;
        }
    }  
    
    getStudent(studentId){
        this.panelLoader="show";
        this.studentService.getOneStudent(studentId).subscribe( (data)=>{
                this.student=data;
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
                this.getUserProducts();
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader="deleted";
            }
        );
    }

    removeUserProduct(i){
        let confirm = window.confirm("Are you sure to remove "+this.userProducts[i].product.title +" from "+this.student.firstName);
        if(!confirm) return;

        this.userService.removeUserProduct(this.userProducts[i].id).subscribe(
            res=>{
                this.userProducts.splice(i,1);
            },err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
}