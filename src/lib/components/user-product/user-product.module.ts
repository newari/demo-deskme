import { NgModule } from "@angular/core";
import { UserProduct } from "./user-product.component";
import { CommonModule } from "@angular/common";
import { DialogModule, CalendarModule } from "primeng/primeng";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { ProductService } from "../../services/product.service";
import { SessionService } from "../../services/session.service";
import { CenterService } from "../../services/center.service";
import { CoptionService } from "../../services/coption.service";
import { RouterModule } from "@angular/router";

@NgModule({
    declarations:[UserProduct],
    imports:[CommonModule,DialogModule, ReactiveFormsModule,CalendarModule, FormsModule, RouterModule],
    exports:[UserProduct],
    providers:[ProductService, SessionService, CenterService, CoptionService]

}) 

export class UserProductModule{}