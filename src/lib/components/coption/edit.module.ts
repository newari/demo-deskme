import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CoptionService } from "../../services/coption.service";
import { NotifierService } from "../notifier/notifier.service";
import { EditCoptionComponent } from "./edit";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FileinputModule } from "../filemanager/fileinput.module";

@NgModule({
    declarations: [EditCoptionComponent],
    imports: [CommonModule, FormsModule, ReactiveFormsModule, FileinputModule],
    exports: [EditCoptionComponent],
    providers: [CoptionService, NotifierService]
})
export class EditCoptionModule {
}