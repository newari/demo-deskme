import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { CoptionService } from "../../services/coption.service";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { NotifierService } from "../notifier/notifier.service";
import { Coption } from "../../models/coption.model";
// import { EventEmitter } from "events";
import { Option } from "../../models/option.model";

@Component({
    selector: 'add-coption',
    templateUrl: './add.html'
})

export class AddCoptionComponent implements OnInit {
    optionForm:FormGroup;
    formStatus='none';
    optionData;
    myOption;
    @Input() option: any;
    @Output() optionCreated = new EventEmitter<Coption>();
    @Output() onSuccess = new EventEmitter<Coption>();

    productCategories:Coption[];
    apps:any=["ADMISSION","COURSE_CREATOR","EBOOK", "CONVENTIONAL_EXAM","TESTMENT","BOOK_STORE","CLASS_TEST"]
    constructor(
        private coptionService:CoptionService,
        private fb:FormBuilder,
        private notifier:NotifierService
    ) {
        console.log(this.option);
    }
    addOption(): void {
        this.formStatus = "Processing";
        this.optionData = this.optionForm.value;
        if(this.optionData.parentOption&&this.optionData.parentOption!=='null'){
            this.optionData.isChild=true;
        }
        this.coptionService.addCoption(this.optionData).subscribe(
            res => {
                //  this.optionCreated.emit(res);
                let data:any={event:"ItemAdded",item:res};
                 this.onSuccess.emit(data);
                this.optionForm.reset(); 
                this.formStatus = "Normal" ;
                this.optionForm.patchValue({ option: this.option });
                },
            err => {
                this.formStatus = "Normal";
                this.notifier.alert(err.code, err.message, "danger");
            }
        );
    }
    
     
    getProductCategories(): void {
        this.coptionService.getCoptionProductCategory({option:"PRODUCTCATEGORY"}).subscribe(
            res => {
                this.notifier.alert('Ready', 'Option Names loaded', 'success', 2000);
                this.productCategories = res;
            },
            err => {
                console.log(err);
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }

    ngOnInit(): void {
        this.optionForm = this.fb.group({
            option: ['', Validators.required],
            value: ['', Validators.required],
            valueAlias: ['', Validators.required],
            code: ['', [Validators.required,Validators.minLength(5), Validators.maxLength(10)]],
            isChild: [false],
            parentOption: [null],
            thumb:[''],
            seq : [''],
            status: [true, Validators.required],
            relatedApp:[null]
        });
        // this.optionForm.value.option=this.option;
        this.getProductCategories();
        this.optionForm.patchValue({option:this.option});
    }
    // setRelatedApp(categoryId){

    //     if(categoryId==''){

    //     }
    // }
}