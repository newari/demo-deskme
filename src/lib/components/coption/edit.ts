import { Component, OnInit, Input, AfterViewInit, DoCheck, AfterViewChecked, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Coption } from '../../models/coption.model';
import { CoptionService  } from '../../services/coption.service';
import { NotifierService } from '../notifier/notifier.service';


@Component({
    selector: 'edit-coption',
    templateUrl: './edit.html'
})
export class EditCoptionComponent implements OnInit, OnChanges {
    productCategories: Coption[];
     ngOnChanges(){
         console.log(this.coptionId);
         if(this.coptionId&&this.coptionId!=undefined){
             this.getOption(this.coptionId);
         }
     }
    
     
    // ngDoCheck(): void {
    //    console.log(this.coptionId);
    //     this.getOption(this.coptionId);
    // }
    optionForm: FormGroup;
    optionData: Coption;
    // coptionId;
    formStatus = "Normal";
    apps:any=["ADMISSION","COURSE_CREATOR","EBOOK", "CONVENTIONAL_EXAM","TESTMENT","BOOK_STORE","CLASS_TEST"]

    @Input() coptionId:string;
    @Output() optionUpdated = new EventEmitter<Coption>();
    @Output() onSuccess = new EventEmitter<Coption>();
    constructor(
        private fb: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private optionService: CoptionService,
        private notifier: NotifierService
    ) {

    }

    getOption(coptionId) {
        console.log(coptionId);
        
        this.optionService.getOneCoption(coptionId).subscribe(
            res => {
                this.optionForm.reset();
                 this.setForm(res),
                 this.optionData=res;
            },

            err => console.log(err)
        )
    }

    updateCoption(): void {
        if (!this.optionForm.dirty) {
            return;
        }
        this.formStatus = "Processing";
        this.optionData = this.optionForm.value;
        this.optionService.updateCoption(this.coptionId, this.optionData).subscribe(
            res => {
                // console.log(res);
                this.optionData=res;
                let self=this;
                res.client=res.client.id;
                let coption :any= Object.assign({event:"ItemUpdated"},{item:[res]});
                self.onSuccess.emit(coption);
                this.formStatus = "Normal";
            },
            err => console.log(err)
        );
    }

    setForm(option: Coption) {
        this.optionForm.patchValue(option);
    }

    ngOnInit(): void {
        this.optionForm = this.fb.group({
            option: [''],
            value: [''],
            valueAlias: [''],
            code: [''],
            isChild: [''],
            parentOption: [""],
            thumb:[''],
            status: [''],
            seq : [''],
            isCommon:[false],
            relatedApp:[null]

        });
        // this.activatedRoute.params.subscribe((params: Params) => {
        //     this.coptionId = params['coptionId'];
        //     this.getOption(this.coptionId);
        // });
        this.getProductCategories()
      if(this.coptionId&&this.coptionId!=undefined){
             this.getOption(this.coptionId);
         }
        
        
    }

    getProductCategories(): void {
        this.optionService.getCoptionProductCategory({ option: "PRODUCTCATEGORY" }).subscribe(
            res => {
                this.notifier.alert('Ready', 'Option Names loaded', 'success', 2000);
                this.productCategories = res;
            },
            err => {
                console.log(err);
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
   
}
