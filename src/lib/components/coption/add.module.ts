import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { CoptionService } from "../../services/coption.service";
import { NotifierService } from "../notifier/notifier.service";
import { AddCoptionComponent } from "./add";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FileinputModule } from "../filemanager/fileinput.module";
// import { FilemanagerService } from "../filemanager/filemanager.service";

@NgModule({
    declarations: [AddCoptionComponent],
    imports : [CommonModule, FormsModule,ReactiveFormsModule, FileinputModule],
    exports : [AddCoptionComponent],
     providers: [CoptionService, NotifierService]
})
export class AddCoptionModule {
}