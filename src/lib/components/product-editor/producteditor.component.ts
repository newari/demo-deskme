import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Product } from '../../models/product.model';
import { Coption } from '../../models/coption.model';
import { ProductService } from '../../services/product.service';
import { CoptionService } from '../../services/coption.service';
import { NotifierService } from '../notifier/notifier.service';


@Component({
    selector:'ek-product-editorcomponent',
    templateUrl:'./producteditor.component.html'
})
export class ProductEditorComponent implements OnInit{
    productForm:FormGroup;
	product:Product;
	@Input() productId;
	formStatus="Normal";
	panelLoader="none";
	frmLoader="none";

	courses:Coption[];
	productCategories:Coption[];
	productTypes:Coption[];
	streams:Coption[];
	products:Product[];

	cmbPanelLoader:string="none";
	addedProducts:any[]=[];
	constructor(
		private fb:FormBuilder,
		private productService:ProductService,
		private coptionService:CoptionService,
		private notifier: NotifierService){ 
			
	}

	getProduct(productId){
		this.panelLoader="show";
		this.productService.getOneProduct(productId).subscribe(
			(res)=>{
				let product=res;
				this.product=res;
				if(res.category&&res.category.id){
					product.category=res.category.id;
				}
				if(res.stream&&res.stream.id){
					product.stream=res.stream.id;
				}
				if(res.course&&res.course.id){
					product.course=res.course.id;
				}
				
				if(res.type&&res.type.id){
					product.type=res.type.id;
				}
				
				
				this.productForm.patchValue(product);
				if(product.type=='30707264637474636f6d626f'){
					this.setProductType();
					this.addedProducts=res.comboProducts;
				}
				this.panelLoader="none";
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		)
	}

	loadCourses(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'COURSE'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.courses=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}

	loadProductCategories(){
		this.frmLoader="show";
		this.coptionService.getCoptionProductCategory({option:'PRODUCTCATEGORY'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.productCategories=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadProductTypes(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'PRODUCTTYPE'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.productTypes=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
	}

	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		)
	}
	
	updateProduct(): void {
		
		this.product=this.productForm.value;
		if(this.product.type=='Combo'){
			if(this.addedProducts.length<2){
				this.notifier.alert("Combo Error", "Please add atleast 2 products in this combo!", "danger", 5000);
				return;
			}
			this.product.comboProducts=this.addedProducts;
		}else{
			if(this.addedProducts.length>0){
				this.product.comboProducts=[];
			}
			this.product.sourceModel=this.getSourceModel(this.product.type);
		}
		this.formStatus="Processing";
		
		this.productService.updateProduct(this.productId, this.product).subscribe(
			(res)=>{
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.productForm=this.fb.group({
			title:['', Validators.required],
			alias:['', Validators.required],
			course:['', Validators.required],
			stream:['', Validators.required],
			category:['', Validators.required],
			type:['', Validators.required], 
			description:['', Validators.required],
			fullDescription:['', Validators.required],
			mrp:['', Validators.required],
			cost:['', Validators.required],
			manageStock:[true, Validators.required],
			thumb:[''],
			featureImg:[''],
			sourceId:[''],
			uri:['', Validators.required],
			storeFronts:[[]],
			status:[true, Validators.required],
		});
		this.getProduct(this.productId);
		this.loadCourses();
		this.loadProductCategories();
		this.loadProductTypes();
		this.loadStreams();
	}

	

	addComboProduct(pIndex):void{
		
		let product=this.products[pIndex];
		this.addedProducts.push({
			id:product.id,
			title:product.title,
			alias:product.alias,
			type:product.type
		});
	}

	removeComboProduct(cpIndex):void{
		this.addedProducts.splice(cpIndex, 1);
	}

	getSourceModel(type:string){
		let sourceModels = {
            'Test': 'test',
            'Test Series': 'testseries',
            'Test Package': 'testpackage',
            'Book': 'book'
        };
		return sourceModels[type];
	}
	linkStoreFront(storeCode){
		if(!this.product.storeFronts){
			this.product.storeFronts=[storeCode];
			
		}else if(this.product.storeFronts&&this.product.storeFronts.indexOf(storeCode)==-1){
			this.product.storeFronts.push(storeCode);
		}else{
			this.product.storeFronts.splice(this.product.storeFronts.indexOf(storeCode), 1);
		}
		this.productForm.value.storeFronts=this.product.storeFronts;
		console.log(storeCode);
	}
	setProductType(){
		if(this.productForm.value.type!='30707264637474636f6d626f'){ //here 30707264637474636f6d626f=Combo
			return;
		}
		this.cmbPanelLoader="show";
		this.productService.getProduct().subscribe(
			res=>{
				this.products=res.filter(function(p){
					return (p.type&&p.type.id!='30707264637474636f6d626f');
				});
				this.cmbPanelLoader="none";
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger");
				this.cmbPanelLoader="none";
			}
		)
	}
}
