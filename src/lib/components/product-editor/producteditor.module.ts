import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ProductEditorComponent } from './producteditor.component';
import { FileinputModule } from '../filemanager/fileinput.module';
import { ProductService } from '../../services/product.service';

@NgModule({
    declarations:[ProductEditorComponent],
    imports:[FileinputModule, RouterModule, FormsModule, ReactiveFormsModule, CommonModule],
    providers:[ProductService],
    exports:[ProductEditorComponent]
})
export class ProductEditorModule {
    
 }
