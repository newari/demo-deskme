import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import {NgIf} from '@angular/common';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import { User } from '../../models/user.model';
import { Order } from '../../models/order.model';
import { Session } from '../../models/session.model';
import { Coption } from '../../models/coption.model';
import { Product } from '../../models/product.model';
import { OrderService } from '../../services/order.service';
import { CenterService } from '../../services/center.service';
import { ProductService } from '../../services/product.service';
import { SessionService } from '../../services/session.service';
import { CoptionService } from '../../services/coption.service';
import { StudentService } from '../../services/student.service';
import { NotifierService } from '../notifier/notifier.service';
import { Center } from '../../models/center.model';

@Component({
    selector:'ek-quick-order',
    templateUrl:'./quick-order.component.html'
})
export class QuickOrderComponent implements OnInit{ 
    quickOrderForm:FormGroup;
    activeStep:number=1;
    selectedUser:User;
    userOldOrders:Order[];
    theOrder:Order;
    centers:Center[];
    sessions:Session[];
    courses:Coption[];
    streams:Coption[];
    productTypes:Coption[];
    products:Product[];
    selectedProduct:Product;
    formStatus:string;
    fh:any={
        personalImg:'https://iesmaster.org/public/images/dummy-photo.png',
        signImg:'https://iesmaster.org/public/images/dummy-photo-sign.png',
        status:'Normal',
        error:null
    };
    isExStd: boolean = false;
    @Input() windowState:string;
    @Output() onWindowClose:EventEmitter<any>=new EventEmitter<any>();
    @Output() onQuickOrderCompelete:EventEmitter<any>=new EventEmitter<any>();
    @Input() productCategory:string[];//=[Classroom, Book];  //Category Coptio Ids
    constructor(
        private fb:FormBuilder,
        private orderService:OrderService,
        private centerService:CenterService,
        private sessionService:SessionService,
        private productService:ProductService,
        private coptionService:CoptionService,
        private studentService:StudentService,
        private notifier: NotifierService
    ){

    }

    ngOnInit(): void{

        this.quickOrderForm=this.fb.group({
            center:[null, Validators.required],
            course:[null, Validators.required],
            productType:[null, Validators.required],
            stream:[null, Validators.required],
            session:[null, Validators.required],
            product:[null, Validators.required],
            batch:[null],
            user:this.fb.group({
                firstName:['', Validators.required],
                lastName:['', Validators.required],
                email:['', Validators.required],
                mobile:['', Validators.required],
                gender:['', Validators.required],
                dob:['', Validators.required],
                address:this.fb.group({
                    address:[''],
                    landmark:[''],
                    city:[''],
                    state:[''],
                    postalCode:[''],
                    country:['India'],
                }),
                permanantAddress:this.fb.group({
                    address:[''],
                    landmark:[''],
                    city:[''],
                    state:[''],
                    postalCode:[''],
                    country:['India']
                }),
                exStudent: this.fb.group({
                    srn: [''],
                    session: ['']
                }),
            })
        });
        let self=this;
        window.setTimeout(function(){
            self.setInit();
        }, 1);
        
    }

    setInit(){
        
        this.loadCenters();
        this.loadCourse();
        this.loadProductTypes();
        this.loadStreams();
        this.loadSessions();
    }

    onUserSelect(user){
        this.selectedUser=user;
        this.orderService.getOrder({user:user.id, populateCenter:true, populateCourse:true, populateStream:true, populateSession:true, populateBatch:true}).subscribe(
            res=>{
                this.userOldOrders=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000);
            }
        )
    }

    resetSelectedUser(){
        this.selectedUser=null;
        this.userOldOrders=null;
        this.quickOrderForm.reset();
    }
    onCenterChanged(){
        this.loadCourse();
        this.quickOrderForm.patchValue({course:null});
        this.unSelectProduct();
        
    }

    onCourseChanged(){
        this.loadProductTypes();
        this.quickOrderForm.patchValue({productType:null});
        this.unSelectProduct();
        
    }

    onProgramTypeChanged(){
        this.loadStreams();
        this.quickOrderForm.patchValue({stream:null});
        this.unSelectProduct();
        
    }

    onStreamChanged(){
        this.loadSessions();
        this.quickOrderForm.patchValue({session:null});
        this.unSelectProduct();
    }

    onSessionChanged(){
        this.unSelectProduct();
        
        this.loadProducts();
    }
    onProductChanged(){
        this.quickOrderForm.patchValue({batch:null});
        this.setProductDepParams();
    }

    checkProductDepParams(){
        let frmData=this.quickOrderForm.value;
        if(frmData.course==""||frmData.productType==""||frmData.stream==""){
            alert("Please select Course, Program & Stream First!");
            return;
        }
    }
    setProductDepParams(){
        if(this.quickOrderForm.value.product){
            this.selectedProduct=this.products[this.quickOrderForm.value.product];
            
        }else{
            this.selectedProduct=null;
        }
    }
    unSelectProduct(){
        this.quickOrderForm.patchValue({product:null});
        this.selectedProduct=null;
    }

    loadCenters(){
        let filter:any={};
        if(this.centers){
            filter.id=this.centers;
        }
        
        this.productService.getCenters(filter).subscribe(
            res=>{
                
                this.centers=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    loadCourse(){
        let filter:any={};
        let frmData=this.quickOrderForm.value;
        if(frmData.center){
            filter.center=frmData.center;
        }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        this.productService.getCourses(filter).subscribe(
            res=>{
                
                this.courses=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    loadProductTypes(){
        let filter:any={};
        let frmData=this.quickOrderForm.value;
        if(frmData.center){
            filter.center=frmData.center;
        }
        if(frmData.course){
            filter.course=frmData.course
        }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        this.productService.getProductTypes(filter).subscribe(
            res=>{
                
                this.productTypes=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    loadStreams(){
        let filter:any={};
        let frmData=this.quickOrderForm.value;
        if(frmData.center){
            filter.center=frmData.center;
        }
        if(frmData.course){
            filter.course=frmData.course;
        }
        if(frmData.productType){
            filter.type=frmData.productType;
        }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        this.productService.getStreams(filter).subscribe(
            res=>{
                
                this.streams=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    loadSessions(){
        let filter:any={};
        if(this.sessions){
            filter.id=this.sessions;
        }
        let frmData=this.quickOrderForm.value;
        if(frmData.center){
            filter.center=frmData.center;
            
        }
        if(frmData.course){
            filter.course=frmData.course;
        }
        if(frmData.productType){
            filter.type=frmData.productType;
            
        }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        this.productService.getSessions(filter).subscribe(
            res=>{
                this.sessions=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    loadProducts(){
        let frmData=this.quickOrderForm.value;
        if(frmData.course==""||frmData.productType==""||frmData.stream==""||frmData.center==""||frmData.session==""){
            return;
        }
        let filter={
            center:frmData.center,
            course:frmData.course,
            productType:frmData.productType,
            stream:frmData.stream,
            session:frmData.session,
            populateBatches:true,
            status:true
        };

        this.productService.getProducts(filter).subscribe(
            res=>{
                this.products=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    addUpdateStudent(placeOrder?:boolean){
        this.fh.error=null;
        if(!this.quickOrderForm.valid ){
            this.fh.error="Please fill all the mandatory fields!";
            return;
        }
        let userData=this.quickOrderForm.value.user;
        if(userData.personalImg==""||userData.signImg==""){
            userData.personalImg=this.fh.personalImg;
            userData.signImg=this.fh.signImg;
        }
        userData.stream=this.quickOrderForm.value.stream;
        userData.center=this.quickOrderForm.value.center;
        userData.course=this.quickOrderForm.value.course;
        userData.session=this.quickOrderForm.value.session;
        if(this.selectedProduct.haveBatch){
            if(!this.quickOrderForm.value.batch||this.quickOrderForm.value.batch==null){
                this.fh.error="Please select Batch first!";
                return;
            }
            userData.batch=this.selectedProduct.batches[this.quickOrderForm.value.batch].id;
        }
        userData.dob=moment(userData.dob).format("DD/MM/YYYY");
        let stdData:any={student:userData};
        if(this.selectedUser){
            stdData.user={
                id:this.selectedUser.id
            }
        }else{
            stdData.createLogin=true;
            stdData.user={
                password:Math.random()*100000|0
            }
        }
            
        this.formStatus="Processing";
        this.studentService.quickRegisterStudent(stdData).subscribe(
            res=>{
                this.selectedUser=res.user;
                if(placeOrder){
                    this.placeOrder();
                }else{
                    this.formStatus="Normal";
                }
            },
            err=>{
                this.formStatus="Normal";
                this.fh.error=err.message;
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }

    placeOrder(){
		if(!this.selectedUser){
			this.notifier.alert("Warning", "Please add user first!", "danger", 10000);
			return;
		}
		
		let orderData:any={};
		orderData.store=this.selectedProduct.store.id||this.selectedProduct.store; //BOOKSTORE ID
		orderData.product=[];
        orderData.isShipping=this.selectedProduct.shipping;
        let prdctData:any={id:this.selectedProduct.id,  quantity:1, discountDetail:{}, taxDetail:{}};
        orderData.product.push(prdctData);
        orderData.total=this.selectedProduct.cost;
		
        orderData.discountDetail={};
        orderData.shippingAddress={};
		orderData.shippingAddress.name=this.selectedUser.firstName+" "+this.selectedUser.lastName;
        orderData.center=this.quickOrderForm.value.center;
        orderData.course=this.quickOrderForm.value.course;
        orderData.stream=this.quickOrderForm.value.stream;
        orderData.session=this.quickOrderForm.value.session;
        if(this.selectedProduct.batches&&this.quickOrderForm.value.batch){
            orderData.batch=this.selectedProduct.batches[this.quickOrderForm.value.batch].id||null;
        }else{
            orderData.batch=null;
        }
		orderData.tracer={
			form:'IMS-QUICK-ORDER-ADD'
        };
        this.formStatus="Processing";
		this.orderService.placeOrder(this.selectedUser, orderData).subscribe(
			res=>{
				this.theOrder=res;
                this.activeStep=3;
			},
			err=>{
                this.formStatus="Normal";
                this.fh.error=err.message;

				this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
			}
		)
	}

    proceedToStep2(){
        
        this.activeStep=2;
        if(this.selectedUser){
            this.quickOrderForm.patchValue({user:this.selectedUser});
        }
    }
    
    proceedToStep3(oldOrderIndex){
        this.theOrder=this.userOldOrders[oldOrderIndex];
        this.activeStep=3;
    }
    
    onPaymentAdd(pmt){
        this.notifier.alert("Success", "Payment added successfully!", "success", 5000);
        this.onQuickOrderCompelete.emit(this.theOrder);
        
	}

    closeWindow(){
        this.activeStep=1;
        this.onWindowClose.emit(true)
    }
    
    
    
}
