import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuickOrderComponent } from './quick-order.component';
import { UserSearchFormModule } from '../user-search-form/user-search-form.module';
import { OrderPaymentModule } from '../order-payment/order-payment.module';
import { CalendarModule } from 'primeng/primeng';
import { OrderService } from '../../services/order.service';


@NgModule({
    declarations:[QuickOrderComponent],
    exports:[QuickOrderComponent],
    providers:[OrderService],
    imports:[CommonModule,UserSearchFormModule, CalendarModule, FormsModule, OrderPaymentModule, ReactiveFormsModule, RouterModule]
})
export class QuickOrderModule {
    
}