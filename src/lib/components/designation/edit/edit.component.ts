import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Designation } from '../../../models/designation.model';
import { DesignationService } from '../../../services/designation.service';
import { NotifierService } from '../../notifier/notifier.service';


@Component({
    selector:'ek-designation-edit-form',
    templateUrl:'./edit.component.html'
})
export class DesignationEditFormComponent{
    designationForm:FormGroup;
	designation:Designation;
	formStatus="Normal";
	panelLoader:string="none";

	@Input() designationId:any;
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
	
	constructor(
		private fb:FormBuilder,
		private designationService:DesignationService,
		private notifier: NotifierService
	){ }
	
	getDesignation(designationId){
		this.panelLoader="show;"
		this.designationService.getOneDesignation(designationId).subscribe(
			(res)=>{
				this.designation=res;
				this.designationForm.patchValue(res);
				this.panelLoader="none";
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
			}
		)
	}
		
	updateDesignation(): void {
		if(!this.designationForm.dirty){
			return;
		}
		this.formStatus="Processing";
		this.designation=this.designationForm.value;
		this.designationService.updateDesignation(this.designationId, this.designation).subscribe(
			(res)=>{
				this.onSuccess.emit(res);
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.designationForm=this.fb.group({
			title:['', Validators.required],
			status:[true, Validators.required],
		});

		let self=this;
		window.setTimeout(function(){
			self.getDesignation(self.designationId);
		}, 0);
    }
}
