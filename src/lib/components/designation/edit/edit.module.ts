import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DesignationEditFormComponent } from './edit.component';
import { FileinputModule } from '../../filemanager/fileinput.module';

@NgModule({
    declarations:[DesignationEditFormComponent],
    imports:[FileinputModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[DesignationEditFormComponent]
})
export class DesignationEditFormModule{
}
