import { Component, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Designation } from '../../../models/designation.model';
import { DesignationService } from '../../../services/designation.service';
import { NotifierService } from '../../notifier/notifier.service';


@Component({
    selector:'ek-designation-add-form',
    templateUrl:'./add.component.html'
})
export class DesignationAddFormComponent{
    designationForm:FormGroup;
	designationData:Designation;
    formStatus="Normal";
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
    
    constructor(
		private fb:FormBuilder,
		private designationService:DesignationService,
		private notifier: NotifierService
    ){ }
	
	addDesignation(): void {
		this.formStatus="Processing";
		this.designationData=this.designationForm.value;
		this.designationService.addDesignation(this.designationData).subscribe(
			res=>{
                this.onSuccess.emit({event:'ItemAdded', item:res});
				this.designationForm.reset(); 
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.designationForm=this.fb.group({
			title:['', Validators.required],
			status:[true, Validators.required],
        });
        
    }

   
    
    
}
