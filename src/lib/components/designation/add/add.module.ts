import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DesignationAddFormComponent } from './add.component';
import { FileinputModule } from '../../filemanager/fileinput.module';

@NgModule({
    declarations:[DesignationAddFormComponent],
    imports:[FileinputModule, FormsModule, ReactiveFormsModule],
    exports:[DesignationAddFormComponent]
})
export class DesignationAddFormModule{
}
