import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesignationInfoComponent } from './info.component';

@NgModule({
    declarations:[DesignationInfoComponent],
    imports:[CommonModule],
    exports:[DesignationInfoComponent]
})
export class DesignationInfoModule {
    
}
