import { Component, OnInit, Input } from '@angular/core';
import {NgIf} from '@angular/common';
import { ActivatedRoute, Params } from '@angular/router';
import { Designation } from '../../../models/designation.model';
import { DesignationService } from '../../../services/designation.service';
import { NotifierService } from '../../notifier/notifier.service';


@Component({
    selector:'ek-designation-info',
    templateUrl:'./info.component.html'
})
export class DesignationInfoComponent implements OnInit{ 
    designation:Designation;
    panelLoader="none";
    activeTab:number=0;
    
    @Input() designationId:any;

    constructor(
        private designationService:DesignationService,
        private activatedRoute:ActivatedRoute,
        private notifier: NotifierService
    ){}


    ngOnInit(): void{
        let self=this;
		window.setTimeout(function(){
			self.getDesignation(self.designationId);
		}, 0);
    }

    getDesignation(designationId){
        this.panelLoader="show";
        this.designationService.getOneDesignation(designationId).subscribe( (data)=>{
                this.designation=data;
                this.panelLoader="none";
                this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }

    deleteDesignation(designationId){
        let condesignation=window.confirm("Are you sure to delete this?");
        if(!condesignation){
            return;
        }
        this.panelLoader="show";
        this.designationService.deleteDesignation(designationId).subscribe(
            (res)=>{
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
            }
        );
    }
}
