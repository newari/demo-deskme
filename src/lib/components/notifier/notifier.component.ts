import { Component, Input, OnInit } from '@angular/core';
import { NotifierService } from './notifier.service';

@Component({
    selector:'ek-notifier',
    template:`<div class="ek-notifier ek-progressbar" [style.width]="progress + '%'" [ngClass]="progressbarStatus"></div>
    <div class="ek-alert" [ngClass]="msg.status">
        <div class="msg-box {{msg.type}}">
            <div class="heading">{{msg.heading}}</div>
            <div class="content" *ngIf="msg.content!=''">{{msg.content}}</div>
            <div class="close-opt" (click)="hideAlert()"><span class="glyphicon glyphicon-remove-circle"></span></div>
        </div>
    </div>
    <div class="ek-spinner" *ngIf="spinner">
        <div class="spinner clockwise">
            <div class="spinner-icon" style="border-top-color: rgb(204, 24, 30); border-left-color: rgb(204, 24, 30);"></div>
        </div>
    </div>
    <div class="ek-nav-spinner" *ngIf="navSpinner">
        <div class="spinner clockwise">
            <div class="spinner-icon" style="border-top-color: #2196F3; border-left-color: #2196F3;"></div>
        </div>
    </div>
    `
})
export class NotifierComponent implements OnInit{
    @Input() progress:number=0;
    @Input() spinner:boolean=false;
    @Input() navSpinner:boolean=false;
    @Input() progressbarStatus:string='vhide';
    @Input() msg={
        status:'hide',
        heading:'Processing...',
        content:'',
        type:''
    };
    constructor(
        private notifier:NotifierService
    ){}

    ngOnInit(){
        this.notifier.data().subscribe(
            (config)=>{
                this.progress=config.progressBar.progress;
                this.progressbarStatus=config.progressBar.status;
                this.msg=config.msg;
                this.spinner=config.spinner;
                this.navSpinner=config.navSpinner;
            },
            (err)=>console.log(err)
        );

    }

    hideAlert(){
        this.msg.status="hide";
    }

}
