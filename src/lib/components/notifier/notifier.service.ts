import { Injectable } from '@angular/core';
import { Observable, Observer, Subject } from 'rxjs';

@Injectable()
export class NotifierService{
    private _intervalCounterId:any = 0;
    private _progress:number=0;
    private alrtTimer:any;

    // data:Observable<any>;
    dataObserver=new Subject<any>();
    config={
        progressBar:{
            status:'vshow',
            progress:0,
            interval:50
        },
        spinner:false,
        navSpinner:false,
        msg:{
            heading:'Processing',
            content:'',
            type:'info',
            status:'hide',
            duration:5000
        },
        overlay:{
            status:'hide'
        }
    }
    constructor(){
        // this.data1 = new Observable(observer => this.dataObserver = observer);
    }

    data(): Observable<any> {
        return this.dataObserver.asObservable();
    }

    
    setProgress(progress:Number){
        // this.dataObserver.next(progress);
    }

    set progress(value:number) {
        if (value > 0) {
            this.config.progressBar.status = 'vshow';
        }
        
        this._progress=value;
        this.config.progressBar.progress=this._progress;
        
        this.dataObserver.next(this.config);
        
    }

    get progress():number {
        return this._progress;
    }

    set spinner(value:boolean) {
        this.config.spinner=value;
        this.dataObserver.next(this.config);
        
    }

    get spinner():boolean {
        return this.config.spinner;
    }

    set navSpinner(value:boolean) {
        this.config.navSpinner=value;
        this.dataObserver.next(this.config);
        
    }

    get navSpinner():boolean {
        return this.config.navSpinner;
    }

    start(config=null){
        if(config!==null){
            this.config=config;
        }
        this.startProgressBar();

    }

    private startProgressBar(){
        if(this.progress>0){
            this.completeProgressBar();
        }
        

        this.progress=5;
        this._intervalCounterId = window.setInterval(() => {
            // Increment the progress and update view component
            
            
            this.progress++;
            // If the progress is 100% - call complete
            if (this.progress === 100) {
                this.completeProgressBar();
            }
        }, this.config.progressBar.interval);
    }

    stopProgressBar() {
        if (this._intervalCounterId) {
            clearInterval(this._intervalCounterId);
            this._intervalCounterId = null;
        }
    }

    completeProgressBar() {
        this.progress = 100;
        this.stopProgressBar();
        setTimeout(() => {
            // Hide it away
            this.config.progressBar.status = 'vhide';
            setTimeout(() => {
                // Drop to 0
                this.progress = 0;
            }, 250);
        }, 250);
    }

    alert(heading:string, content?:string, type?:string, duration?:number){
        if(this.alrtTimer){
            clearTimeout(this.alrtTimer);
        }
        this.config.msg.status='show';
        this.config.msg.heading=heading;
        this.config.msg.content=content;
        this.config.msg.type=type;
        this.config.msg.duration=duration;
        this.dataObserver.next(this.config);
        var _self=this;
        if(duration){
            this.alrtTimer=setTimeout(function(){
                _self.config.msg.status='hide';
                _self.dataObserver.next(_self.config);
            }, duration);
        }
        
    }

    clearAlert(){
        clearTimeout(this.alrtTimer);
        this.config.msg.status='hide';
        this.dataObserver.next(this.config);
    }
}