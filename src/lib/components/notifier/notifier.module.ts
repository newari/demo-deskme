import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotifierComponent } from './notifier.component';
import { NotifierService } from './notifier.service';

@NgModule({
    declarations:[
        NotifierComponent
    ],
    imports:[CommonModule],
    exports:[NotifierComponent],
    providers:[NotifierService]

})
export class NotifierModule { }