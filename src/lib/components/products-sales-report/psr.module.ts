import { NgModule } from '@angular/core';
import { ProductSalesReportComponent } from './psr.component';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SalesReportService } from '../../services/sales-report.service';
import { StoreService } from '../../services/store.service';

@NgModule({
    declarations:[ProductSalesReportComponent],
    imports:[CommonModule, RouterModule, FormsModule, ReactiveFormsModule],
    exports:[ProductSalesReportComponent],
    providers:[SalesReportService, StoreService]
})
export class ProductSalesReportModule{
    
}