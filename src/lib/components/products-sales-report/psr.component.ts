import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Angular2Csv } from 'angular2-csv';
import { Store } from '../../models/store.model';
import { Coption } from '../../models/coption.model';
import { SalesReportService } from '../../services/sales-report.service';
import { StoreService } from '../../services/store.service';
import { CoptionService } from '../../services/coption.service';
import { NotifierService } from '../notifier/notifier.service';

@Component({
    selector:'ek-product-sales-report',
    templateUrl:'./psr.component.html'
})
export class ProductSalesReportComponent implements OnInit{
    
    @Input() store:string;

    products:any[];
    filterForm:FormGroup;
    panel1Loader:string='none';
    stores:Store[];
    orderStatus:Coption[];

    filterOpen:boolean = false;
    constructor(
        private srService:SalesReportService,
        private storeService:StoreService,
        private coptionService:CoptionService,
        private notifier:NotifierService,
        private fb:FormBuilder
    ){

    }
    ngOnInit(){
        this.filterForm=this.fb.group({
            from:[""],
            to:[""],
            status:[""],
            paymentMode:[""],
            paymentStatus:[""],
            store:['']
        })
        this.loadProducts();
    }

    loadProducts(fltr?:any){
        this.panel1Loader="show";
        if(!fltr){
            fltr={};
        }
        if(this.store){
            fltr.store=this.store;
        }

        this.srService.productWiseSales(fltr).subscribe(
            res=>{
                this.products=res;
                this.panel1Loader='none';
                this.filterOpen=false;
            },
            err=>{
                this.panel1Loader='none';
                this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
            }
        );
    }

    setFilterBox(){
        
        this.storeService.getStore().subscribe(
            res=>{
                
                this.stores=res;
            },
            err=>{
                this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
            }
        );
        this.coptionService.getCoption({option:'ORDERSTATUS'}).subscribe(
            res=>{
                this.orderStatus=res;
            },
            err=>{
                this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
            }
        );

        this.filterOpen=true;
    }

    loadProductsWithFilter(){
        let fltr=this.filterForm.value;
        this.loadProducts(fltr);
    }

    exportData(){
        var data=[];
        this.products.forEach(function(e){
            data.push({
                "Product Name":e.product.title,
                "Total Sales":e.salesCount,
                "Total Amount(INR)":e.totalAmount
            })
        })    

        new Angular2Csv(data, 'Product wise Sales Report', { 
                fieldSeparator: ',',
                quoteStrings: '"',
                decimalseparator: '.',
                showLabels: true, 
                showTitle: true 
              });
    }
}