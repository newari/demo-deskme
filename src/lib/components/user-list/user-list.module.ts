import { NgModule } from '@angular/core';
import { TabViewModule, PaginatorModule } from 'primeng/primeng';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserSearchFormModule } from '../user-search-form/user-search-form.module';

import { UserListComponent } from './user-list.component';
import { FileinputModule } from '../filemanager/fileinput.module';
import { UserService } from '../../services/user.service';
import { ClientService } from '../../services/client.service';

@NgModule({
    declarations:[UserListComponent],
    imports:[FileinputModule,PaginatorModule, TabViewModule, UserSearchFormModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[UserListComponent],
    providers:[UserService, ClientService]
})
export class UserListModule{

}