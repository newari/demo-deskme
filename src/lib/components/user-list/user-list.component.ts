import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { NotifierService } from '../notifier/notifier.service';
import { ClientService } from '../../services/client.service';
@Component({
    selector:'ek-user-list',
    templateUrl:'./user-list.component.html'
})
export class UserListComponent implements OnInit{
    @Input() type;
    @Input() app;
    @Input() filter:any;
    rows:any;
    totalRecords:number;
    countBase:number=1;
    activeFilter:any;
    filterRowForm:FormGroup;
    panelLoader: string;
    constructor(
        private userService:UserService,
        private notifier:NotifierService,
        private clientService:ClientService,
        private fb:FormBuilder
    ){}
    ngOnInit(){
        this.filterRowForm=this.fb.group({
            firstName:['', Validators.required],
            email:['', Validators.required],
            mobile:['', Validators.required],
            type:[null, Validators.required],
            status:[null,Validators.required]
        });
        if(this.filter){
            this.activeFilter=this.filter;
        }
        this.getUsers(this.activeFilter);

    }

    getUsers(filter?:any){
        console.log(this.type);
        if(!filter){
            filter={type:''};
        }
        if(this.type&&this.type!==''&&(!filter.type||filter.type==''||filter.type==null||filter.type=='null')){
            filter.type=this.type;
        }
        this.panelLoader="Processing";
        this.userService.getUser(filter, true).subscribe(
            res=>{
                this.rows= res.body;
                this.filterRowForm.reset();
                this.totalRecords= res.headers.get('totalRecords')||0;
                this.panelLoader="none";
            }
        );

    }

    paginate(e){
        if(!this.activeFilter){
            this.activeFilter={};
        }
        this.countBase=e.rows*e.page+1;
        this.activeFilter.page=(e.page+1);
        this.activeFilter.limit=e.rows;
        this.getUsers(this.activeFilter);
    }

    loadUserWithFilter(){
        let filter:any={}=this.filterRowForm.value;
        if(!this.activeFilter){
            this.activeFilter={};
        }
        if(filter.firstName&&filter.firstName!=null&&filter.firstName!==''){
            this.activeFilter.firstName=filter.firstName;
        }
        if(filter.email&&filter.email!=null&&filter.email!==''){
            this.activeFilter.email=filter.email;
        }
        if(filter.mobile&&filter.mobile!=null&&filter.mobile!==''){
            this.activeFilter.mobile=filter.mobile;
        }
        if(filter.type&&filter.type!=null&&filter.type!==''){
            this.activeFilter.type=filter.type;
        }
        if(filter.status&&(filter.status!=null||filter.status!==''||filter.status!='null')){
            this.activeFilter.status=filter.status;
        }
        this.countBase=1;
        this.getUsers( this.activeFilter);
    }
}
