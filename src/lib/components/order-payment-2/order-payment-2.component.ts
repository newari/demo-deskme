import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PaymentService } from '../../services/payment.service';
import { SimpleChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { NotifierService } from '../notifier/notifier.service';

@Component({
    selector:'ek-order-payment-2',
    templateUrl:'./order-payment-2.component.html'
})
export class OrderPayment2Component implements OnInit, OnChanges{
    paymentForm:FormGroup;
    formStatus:string="Normal";
    oldPayments:any[];
    panelLoader:string="none";
    activePmtOptionIndex:number;
    pmtOptions:any[]=[];
    remAmt:number=0;
    duePaymentNodeForm: FormGroup;
    @Input() order:any;
    @Input() paymentHistory:boolean;
    @Output() onPaymentAdd:EventEmitter<any>=new EventEmitter<any>();
    showPaymentNode: boolean;
    paymentOptions:any=[];
    selectedPaymentNodeIds:string[];
    selectedPaymentNodes:any[];
    amtOpt:number;
    totalAmount:number=0;
    discountType:any;
    promocode:any;
    coupon:any;
    fullPaymentDiscount:any;
    other:any;
    discountDetail:any={
        type:'',
        discountVal:'',
        code:'',
        discountType:''
    };
    discountAmt:number=0;
    customPaymentForm: FormGroup;
    displayCustomPaymentForm:boolean;
    constructor(
        private fb:FormBuilder,
        private paymentService:PaymentService,
        private notifier:NotifierService,
    ){

    }

    ngOnInit(){
        this.paymentForm=this.fb.group({
            primaryMode:[null, Validators.required],
            amount:[0, Validators.required],
            extraAmount:[0, Validators.required],
            currency:['INR', Validators.required],
            note:[''],
            bankRefId:[''],
            selectedNodes:[]
        });
        this.duePaymentNodeForm = this.fb.group({
            title:['Previous Due Amount'],
            order:[this.order?this.order.id:null, Validators.required],
            orderNo: [this.order?this.order.orderNo:null, Validators.required],
            currency: ["INR"],
            amount: [0, Validators.required],
            status: false,
        });
        this.customPaymentForm= this.fb.group({
            title:['Other Charges'],
            primaryMode:[null, Validators.required],
            amount:[0,Validators.required],
            extraAmount:[0, Validators.required],
            currency:['INR', Validators.required],
            note:[''],
            bankRefId:[''],
            selectedNodes:[[]]
        })
        if(this.paymentHistory){
            let self=this;
            setTimeout(function(){
                self.loadOldPayments();
            }, 10);
        }
            
    }
    getPaymentOptions(){
        this.pmtOptions=[];
        this.paymentOptions=[];
        this.paymentService.getOrderPaymentNodes({order:this.order.id}).subscribe(
            res=>{
                this.paymentForm.patchValue({amount:null});
                let selectedNodesData:any=[];
                this.pmtOptions=res;
                for (let index = 0; index < res.length; index++) {
                    this.paymentOptions.push({label:res[index].title, value:res[index]})
                    if(res[index].paid==res[index].amount){
                        selectedNodesData.push(res[index])
                    }
                }
                if(selectedNodesData.length>0){
                    this.paymentForm.patchValue({selectedNodes:selectedNodesData});
                }
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 2000);
            }
        )
    }
    ngOnChanges(changes: SimpleChanges): void {
        if (this.duePaymentNodeForm&&changes['order'] && changes['order'].currentValue) {
            if (changes['order'].currentValue) {
                this.order = changes['order'].currentValue
            }
            // this.order =changes['order'].currentValue
            if (this.order && this.order.id){
                this.duePaymentNodeForm.patchValue({order :this.order.id ,orderNo:this.order.orderNo});
                this.getPaymentOptions();
            }
        }
    }
    showAddPaymentNode(){
        this.showPaymentNode=true;
    }
    
    loadOldPayments(){
        if(!this.order||!this.order.id){
            this.oldPayments=null;
            return;
        }
        this.panelLoader="Processing";
        this.paymentService.getPayment({order:this.order.id}).subscribe(
            res=>{
                this.oldPayments=res;
                this.panelLoader="none";
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )
    }

    addPayment(){
        if(this.formStatus=="Processing"){
            return;
        }
        let frmData=this.paymentForm.value;
        if(!this.paymentForm.valid){
            this.notifier.alert('Error', 'Please fill all the mandatory field!', "danger", 5000);
            return
        }
        if(frmData.amount<=0&&frmData.extraAmount<=0){
            this.notifier.alert('Error', 'Amount must be greater than 0', "danger", 5000);
            return;
        }
        if(this.selectedPaymentNodeIds.length<1){
            this.notifier.alert('Error', 'Select At least on payment option', "danger", 5000);
            return
        }
        let data:any={
            // title:this.pmtOptions[this.activePmtOptionIndex].title,
            title:null,
            orderId:this.order.id,
            orderNo:this.order.orderNo,
            primaryMode:frmData.primaryMode,
            amount:frmData.amount,
            extraAmount:frmData.extraAmount,
            user:this.order.user.id||this.order.user,
            store:this.order.store.id||this.order.store,
            currency:frmData.currency,
            transactionStatus:'Success',
            pmtNodeIds:this.selectedPaymentNodeIds,
            details:{
                mode:frmData.primaryMode,
                note:frmData.note,
                bankRefId:frmData.bankRefId
            }
        };
        for (let index = 0; index < this.selectedPaymentNodes.length; index++) {
             if(this.selectedPaymentNodes[index].paid!==this.selectedPaymentNodes[index].amount){
                 data.title=(data.title==''||data.title==null? this.selectedPaymentNodes[index].title:data.title+" + "+this.selectedPaymentNodes[index].title)
             }
             if(this.selectedPaymentNodes[index].title=='Books'){
                 data.store=this.selectedPaymentNodes[index].store;
             }
        }
        this.formStatus="Processing";
        this.paymentService.addPayment(data).subscribe(
            res=>{
                if(this.oldPayments){
                    this.oldPayments.unshift(res);
                }
                for (let index = 0; index < res.nodes.length; index++) {
                    for (let pOptionsIndex = 0; pOptionsIndex < this.selectedPaymentNodes.length; pOptionsIndex++) {
                        if(res.nodes[index].node==this.selectedPaymentNodes[pOptionsIndex].id){
                            this.selectedPaymentNodes[pOptionsIndex].paid=res.nodes[index].paid;
                            this.selectedPaymentNodes[pOptionsIndex].extraAmount=res.nodes[index].extraAmount;
                        }
                    }
                }
                // this.pmtOptions[this.activePmtOptionIndex].paid=frmData.amount;
                // this.pmtOptions[this.activePmtOptionIndex].extraAmount=frmData.extraAmount;
                this.onPaymentAdd.emit(res);
                this.formStatus="Normal";
                this.paymentForm.reset();
                this.paymentForm.patchValue({currency:'INR'});
            },
            err=>{
                console.log(err);
                this.formStatus="Normal";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
        
    }

   

    setPaymentOption(event,optIndex){
        console.log(event);
        console.log("Index",optIndex)
        this.activePmtOptionIndex=optIndex;
        let discount=0;
        let paid=0;
        if(this.pmtOptions[optIndex].discountTotal){
            discount=parseFloat(this.pmtOptions[optIndex].discountTotal);
        }
        if(this.pmtOptions[optIndex].paid){
            paid=parseFloat(this.pmtOptions[optIndex].paid);
        }
        let amt=parseFloat(this.pmtOptions[optIndex].amount)-discount-paid;
        this.remAmt=amt;
        this.paymentForm.patchValue({amount:amt});
    }
    setPaymentOptions(event){
        let nodes= event.value;
        this.totalAmount=0;
        this.selectedPaymentNodes=[];
        this.selectedPaymentNodeIds=[];
        let discount=0;
        let paid=0;
        let amt=0;
        for (let index = 0; index < nodes.length; index++) {
            if(nodes[index]&&nodes[index].paid!==nodes[index].amount){
                this.totalAmount=this.totalAmount +(nodes[index].amount-(nodes[index].paid||0));
                // this.totalAmount=this.totalAmount +((nodes[index].amount+(nodes[index].extraAmount||0))-(nodes[index].paid||0));
                this.selectedPaymentNodeIds.push(nodes[index].id);
                this.selectedPaymentNodes.push(nodes[index]);
                if(nodes[index].discountTotal){
                    discount+=parseFloat(nodes[index].discountTotal);
                }
                if(nodes[index].paid){
                    paid+=parseFloat(nodes[index].paid);
                }
                this.discountAmt=discount;
                amt+=parseFloat(nodes[index].amount)-discount-paid;
                // amt+=parseFloat(nodes[index].amount+(nodes[index].extraAmount||0))-discount-paid;
            }
        }
        this.remAmt=amt;
        // this.totalAmount=amt;
        this.paymentForm.patchValue({amount:amt});
    }

    addDuePaymentNode(){
        if (!this.duePaymentNodeForm.valid) {
            return;
        }
        if (this.duePaymentNodeForm.value.amount<=0) {
            return this.notifier.alert("Error", "Amount must be greater than zero", 'danger', 5000);
        }
        console.log(this.duePaymentNodeForm.value);
        this.paymentService.addPaymentNode(this.duePaymentNodeForm.value).subscribe(
            (res)=>{
                this.duePaymentNodeForm.reset();
                this.showPaymentNode = false;
                this.notifier.alert("Success", "Successfully Added!", 'success', 500);
                this.loadOldPayments();
                this.getPaymentOptions();
                this.duePaymentNodeForm.patchValue({ order: this.order.id, orderNo: this.order.orderNo})
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }
    applyPromocode(){
        console.log("Promocode Discount");
    }
    applyDiscount(){
        this.discountDetail.type=this.discountType;
        this.discountDetail.discountVal=this.discountDetail.discountVal;
        this.paymentService.addDiscount(this.order.id, {discountDetail:this.discountDetail}).subscribe(
            res=>{
                this.getPaymentOptions();
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    clearDiscount(){
        this.discountDetail={
            type:'',// PROMOCODE , COUPON, DIRECT, 
            discountVal:'',
            code:'',
            discountType:'' //PER , FIX
        }
    }
    
    addCustomPayment(){
        if(this.customPaymentForm.value.extraAmount<1){
            this.notifier.alert("ERROR", "Please enter amount greater than 0", 'danger', 500);
            return;
        }
        this.formStatus="Processing";
        let data:any={
            title:this.customPaymentForm.value.title,
            orderId:this.order.id,
            orderNo: this.order.orderNo,
            amount:0,
            extraAmount: this.customPaymentForm.value.extraAmount,
            pmtNodeIds:[this.customPaymentForm.value.selectedNodes],
            primaryMode:this.customPaymentForm.value.primaryMode,
            user:this.order.user.id||this.order.user,
            store:this.order.store.id||this.order.store,
            currency:"INR",
            transactionStatus:'Success',
            details:{
                mode:this.customPaymentForm.value.primaryMode,
                note:this.customPaymentForm.value.note,
                bankRefId:this.customPaymentForm.value.bankRefId
            }
        }
        this.paymentService.addCustomPayment(data).subscribe(
            res=>{
                this.displayCustomPaymentForm=false;
                if(this.oldPayments){
                    this.oldPayments.unshift(res);
                }
                this.onPaymentAdd.emit(res);
                this.formStatus="Normal";
                this.customPaymentForm.reset();
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
}