import { NgModule } from '@angular/core';
import { OrderPayment2Component } from './order-payment-2.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaymentService } from '../../services/payment.service';
import {MultiSelectModule, DialogModule} from 'primeng/primeng';
@NgModule({
    declarations:[OrderPayment2Component],
    imports:[CommonModule, FormsModule, ReactiveFormsModule,MultiSelectModule, DialogModule],
    providers:[PaymentService ],
    exports:[OrderPayment2Component]
})
export class OrderPayment2Module{

}