import { NgModule } from '@angular/core';
import { TabViewModule } from 'primeng/primeng';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserSearchFormModule } from '../user-search-form/user-search-form.module';

import { UserFormComponent } from './user-form.component';
import { FileinputModule } from '../filemanager/fileinput.module';
import { UserService } from '../../services/user.service';
import { ClientService } from '../../services/client.service';

@NgModule({
    declarations:[UserFormComponent],
    imports:[FileinputModule, TabViewModule, UserSearchFormModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[UserFormComponent],
    providers:[UserService, ClientService]
})
export class UserFormModule{

}