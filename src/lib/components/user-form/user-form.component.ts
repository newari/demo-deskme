import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { NotifierService } from '../notifier/notifier.service';
import { ClientService } from '../../services/client.service';
@Component({
    selector:'ek-user-form',
    templateUrl:'./user-form.component.html'
})
export class UserFormComponent implements OnInit{
    userForm:FormGroup;
    userData:User;
    formStatus:string="Normal";
    resetPassword:boolean=false;
    imsApps:any[]=[];
    addedApps:any[]=[];
    @Output() onUserSaved:EventEmitter<any>=new EventEmitter<any>();
    @Input() submitLabel:string='Save';
    @Input() userSearchOpt:boolean=false;
    @Input() defaultUserValue:any;
    @Input() userType:string;
    @Input('user')
    set user(u){
        if(this.userForm){
            if(u){
                if(!u.address){
                    u.address={};
                }
                if(!u.address.shipping){
                    u.address.shipping={address:'', landmark:'', city:'', state:'', postalCode:'', country:'India'};
                }
                if(!u.address.billing){
                    u.address.billing={address:'', landmark:'', city:'', state:'', postalCode:'', country:'India'};
                }
                this.userForm.patchValue(u);
                this.userData=u;
                this.resetPassword=false;
            }else{
                this.userForm.patchValue({});
                this.userData=null;
                this.resetPassword=true;
            }
        }
            
    }
    constructor(
		private fb:FormBuilder,
        private userService:UserService,
        private clientService:ClientService,
		private notifier: NotifierService
		){ 
			
    }
    ngOnInit(): void {
		this.userForm=this.fb.group({
			firstName:['', Validators.required],
			lastName:['', Validators.required],
			email:['', Validators.required],
			mobile:['', Validators.required],
            profileImg:[''],
            isRecurringCustomer:[false],
            recurringCustomerOf:[[]],
			status:[true, Validators.required],
			address:this.fb.group({
                shipping:this.fb.group({
                    address:[''],
                    landmark:[''],
                    city:[''],
                    state:[''],
                    postalCode:[''],
                    country:['India']
                }),
                billing:this.fb.group({
                    address:[''],
                    landmark:[''],
                    city:[''],
                    state:[''],
                    postalCode:[''],
                    country:['India']
                })
            }),
            username:[''],
            password:[''],
        });
        if(this.userData){
            this.userForm.patchValue(this.userData);
        }
        this.clientService.getApps({isInstalled:true}).subscribe(
            res=>{
                console.log("here");
                
                if (res.length>0) {
                    for (let index = 0; index < res.length; index++) {
                        if (res[index].app && res[index].app.code) {
                            this.imsApps.push(res[index].app.code);
                        }
                    }
                }else{
                    this.imsApps = ["ACADEMICS", "ADMINISTRATION", "HR", "BOOK_STORE", "TESTMENT", "VIDEOVIBE", "ACCOUNTING", "FORUM", "MORE"];
                }
            },
            err=>{
                console.log(err);
                
            }
        )
    }

    addUpdateUser(){
        if(this.defaultUserValue){
            if(this.defaultUserValue.recurringCustomerOf){
                let oldValue=this.userForm.value.recurringCustomerOf||[];
                if(oldValue.indexOf(this.defaultUserValue.recurringCustomerOf)==-1){
                    oldValue.push(this.defaultUserValue.recurringCustomerOf);
                }
                
                this.defaultUserValue.recurringCustomerOf=oldValue;
            }
            this.userForm.patchValue(this.defaultUserValue);
        }
        let userData=this.userForm.value;
        if(!this.userData&&userData.password.toString().trim()==""){
            userData.password=Math.floor((Math.random() * 100000) + 1);;
        }
        if((!this.resetPassword&&this.userData)||userData.password.toString().trim()==""){
            delete userData.password;
        }
        userData.username=userData.email;

        if(this.userType&&this.userType=="ADMIN"){
            userData.type="ADMIN";
            userData.apps={ims:this.addedApps};
            let perms: any = {};
            for (let index = 0; index < this.addedApps.length; index++) {
                perms[this.addedApps[index]] = { all: true }
            }
            userData.activityPermission = perms;
        }
        
        
        this.formStatus="Processing";
        if(this.userData&&this.userData.id){
            console.log("Updating...");
            // userData.email=this.userData.email;
            // userData.username=this.userData.username;
            this.userService.updateUser(this.userData.id, userData).subscribe(
                res=>{
                    this.formStatus="Normal";
                    userData.password="";
                    this.onUserSaved.emit(res);
                },
                err=>{
                    this.formStatus="Normal";
                    this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
                }
            )
        }else{
            this.userService.addUser(userData).subscribe(
                res=>{
                    this.formStatus="Normal";
                    this.userData=res;
                    this.onUserSaved.emit(res);
                },
                err=>{
                    this.formStatus="Normal";
                    this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
                }

            )
        }
    }

    generatePassword(){
        this.userForm.patchValue({password:Math.floor((Math.random() * 100000) + 1)});
    }

    setPasswordOpt(state){
        this.resetPassword=state;
    }

    setBillingAddress(sameAsShipping){
        if(sameAsShipping){
            this.userForm.patchValue({address:{billing:this.userForm.value.address.shipping}});
        }else{
            this.userForm.patchValue({address:{billing:((this.userData&&this.userData.address&&this.userData.address.billing)?this.userData.address.billing:{address:'', landmark:'', city:'', state:'', postalCode:'', country:'India'})}});
        }
    }

    onUserSelect(user){
		this.userService.getOneUser(user.id).subscribe(
			res=>{
                this.user=res;
                
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
    }
    
    addAppAccess(appIndex){
        console.log(appIndex)
        this.addedApps.push(this.imsApps[appIndex]);
    }

    removeAddedApp(i){
        this.addedApps.splice(i, 1);
    }
    
}