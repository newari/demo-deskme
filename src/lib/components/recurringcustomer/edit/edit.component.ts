import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Recurringcustomer } from '../../../models/recurringcustomer.model';
import { NotifierService } from '../../notifier/notifier.service';
import { RecurringcustomerService } from '../../../services/recurringcustomer.service';


@Component({
    selector:'ek-recurringcustomer-edit-form',
    templateUrl:'./edit.component.html'
})
export class RecurringcustomerEditFormComponent{
    recurringcustomerForm:FormGroup;
	recurringcustomer:Recurringcustomer;
	formStatus="Normal";
	panelLoader:string="none";

	@Input() recurringcustomerId:any;
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
	
	constructor(
		private fb:FormBuilder,
		private recurringcustomerService:RecurringcustomerService,
		private notifier: NotifierService
	){ }
	
	getRecurringcustomer(recurringcustomerId){
		this.panelLoader="show;"
		this.recurringcustomerService.getOneRecurringcustomer(recurringcustomerId).subscribe(
			(res)=>{
				this.recurringcustomer=res;
				this.recurringcustomerForm.patchValue(res);
				this.panelLoader="none";
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 1000 );
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.details||err.message, 'danger', 10000 );
			}
		)
	}
		
	updateRecurringcustomer(): void {
		if(!this.recurringcustomerForm.dirty){
			return;
		}
		this.formStatus="Processing";
		this.recurringcustomer=this.recurringcustomerForm.value;
		this.recurringcustomerService.updateRecurringcustomer(this.recurringcustomerId, this.recurringcustomer).subscribe(
			(res)=>{
				this.onSuccess.emit(res);
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	ngOnInit(): void {
		this.recurringcustomerForm=this.fb.group({
			title:['', Validators.required],
			status:[true, Validators.required],
		});

		let self=this;
		window.setTimeout(function(){
			self.getRecurringcustomer(self.recurringcustomerId);
		}, 0);
    }
}
