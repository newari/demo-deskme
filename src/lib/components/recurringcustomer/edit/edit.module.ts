import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RecurringcustomerEditFormComponent } from './edit.component';
import { FileinputModule } from '../../filemanager/fileinput.module';

@NgModule({
    declarations:[RecurringcustomerEditFormComponent],
    imports:[FileinputModule, CommonModule, FormsModule, ReactiveFormsModule],
    exports:[RecurringcustomerEditFormComponent]
})
export class RecurringcustomerEditFormModule{
}
