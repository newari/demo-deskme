import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Recurringcustomer } from '../../../models/recurringcustomer.model';
import { RecurringcustomerService } from '../../../services/recurringcustomer.service';
import { NotifierService } from '../../notifier/notifier.service';


@Component({
    selector:'ek-recurringcustomer-add-form',
    templateUrl:'./add.component.html'
})
export class RecurringcustomerAddFormComponent{
    recurringcustomerForm:FormGroup;
	recurringcustomerData:Recurringcustomer;
    formStatus="Normal";
    @Output() onSuccess:EventEmitter<any>=new EventEmitter<any>();
    @Input() store:any;
    constructor(
		private fb:FormBuilder,
		private recurringcustomerService:RecurringcustomerService,
		private notifier: NotifierService
    ){ }
	
	addRecurringcustomer(): void {
		this.formStatus="Processing";
		this.recurringcustomerData=this.recurringcustomerForm.value;
		this.recurringcustomerService.addRecurringcustomer(this.recurringcustomerData).subscribe(
			res=>{
                this.onSuccess.emit({event:'ItemAdded', item:res});
				this.recurringcustomerForm.reset(); 
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}

	onCustomerAdded(e){
		this.onSuccess.emit({event:'ItemAdded', item:e});
	}

	ngOnInit(): void {
		this.recurringcustomerForm=this.fb.group({
			title:['', Validators.required],
			status:[true, Validators.required],
        });
        
    }

   
    
    
}
