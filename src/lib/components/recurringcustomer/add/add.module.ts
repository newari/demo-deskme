import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RecurringcustomerAddFormComponent } from './add.component';
import { UserFormModule } from '../../user-form/user-form.module';
import { FileinputModule } from '../../filemanager/fileinput.module';

@NgModule({
    declarations:[RecurringcustomerAddFormComponent],
    imports:[FileinputModule, UserFormModule, FormsModule, ReactiveFormsModule],
    exports:[RecurringcustomerAddFormComponent]
})
export class RecurringcustomerAddFormModule{
}
