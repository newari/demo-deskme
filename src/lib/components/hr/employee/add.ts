import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Employee } from '../../../models/employee.model';
import { Designation } from '../../../models/designation.model';
import { EmployeeService } from '../../../services/employee.service';
import { DesignationService } from '../../../services/designation.service';
import { NotifierService } from '../../notifier/notifier.service';

@Component({
	selector : "ek-employee-add",
    templateUrl:'./add.html'
})
export class EmployeeAddContent implements OnInit{
    employeeForm:FormGroup;
	employeeData:Employee;
	formStatus="Normal";
	designations:Designation[];
	@Input() routes : string;
	@Input() designation : string;
	constructor(
		private fb:FormBuilder,
		private employeeService:EmployeeService,
		private designationService:DesignationService,
		private notifier: NotifierService
		){ 
			
	}
	
	addEmployee(): void {
		if(!this.employeeForm.valid) return;
		this.formStatus="Processing";
		this.employeeData=this.employeeForm.value;
		this.employeeService.addEmployee(this.employeeData).subscribe(
			res=>{
				this.notifier.alert('Success', 'Employee Added Successfully', 'success', 2000 );
				this.employeeForm.reset(); this.formStatus="Normal"
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal"
			}
		);
	}

	ngOnInit(): void {
		this.employeeForm=this.fb.group({
			firstName:['', Validators.required],
			lastName:['', Validators.required],
			email:['', Validators.required],
			mobile:['', Validators.required],
			phone:[''],
			address: [''],
			profileImg:[''],
			designation:['', Validators.required],
			description:[''],
			status:[true, Validators.required],
			pass:['', Validators.required]
		});
		if(this.designation){
			this.employeeForm.patchValue({designation : this.designation});
		}else{
			this.loadDesignations();
		}
	}

	loadDesignations():void{
		this.designationService.getDesignation().subscribe(
			(res)=>{
				this.designations=res;
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
}
