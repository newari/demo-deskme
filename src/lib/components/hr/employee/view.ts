import { Component, OnInit, Input  } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { EmployeeService } from '../../../services/employee.service';
import { Employee } from '../../../models/employee.model';

@Component({
    selector : "ek-employee-view",
    templateUrl:'./view.html'
})
export class EmployeeViewContent implements OnInit{ 
    employee:Employee;
    employeeId;
    panelLoader="none";
    @Input() routes : string;
    constructor(
        private employeeService:EmployeeService,
        private activatedRoute:ActivatedRoute){}


    ngOnInit(): void{
        this.activatedRoute.params.subscribe((params: Params) => {
	        this.employeeId = params['id'];
			this.getEmployee(this.employeeId);
	    });
    }

    getEmployee(employeeId){
        this.employeeService.getOneEmployee(employeeId).subscribe( (data)=>{
                this.employee=data;
            },
            (err)=>this.panelLoader="deleted"
        );
    }

    deleteEmployee(employeeId){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete this?");
        if(!confirm){
            return;
        }
        this.employeeService.deleteEmployee(employeeId).subscribe(
            (res)=>{
                
                this.panelLoader="deleted";
            },
            (err)=>{
                console.log('Error');
            }
        );
    }
}
