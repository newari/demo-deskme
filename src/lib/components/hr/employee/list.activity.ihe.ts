import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CommonModule } from '@angular/common';
import { EmployeeListContent } from './list';
import { EmployeeService } from '../../../services/employee.service';
import { PaginatorModule } from 'primeng/primeng';

@NgModule({
    declarations:[EmployeeListContent],
    imports:[CommonModule, RouterModule, RouterModule,PaginatorModule],
    providers:[EmployeeService],
    exports : [EmployeeListContent]
})
export class EmployeeListModule {
    
}
