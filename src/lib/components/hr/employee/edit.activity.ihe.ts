import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeeEditContent } from './edit';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { UserChangePasswordModule } from '../../user/change-password/change-password.module';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';
import { FileinputModule } from '../../filemanager/fileinput.module';
import { EmployeeService } from '../../../services/employee.service';
import { DesignationService } from '../../../services/designation.service';
import { UserGeneralAccessModule } from '../../user-general-access/user-general-access.module';

@NgModule({
    declarations:[EmployeeEditContent],
    imports: [CommonModule, UserChangePasswordModule, CKEditor4Module, FileinputModule, FormsModule, ReactiveFormsModule, RouterModule, UserGeneralAccessModule],
    providers:[DesignationService, EmployeeService],
    exports : [EmployeeEditContent]
})
export class EmployeeEditModule {
    
}
