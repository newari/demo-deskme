import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { EmployeeAddContent } from './add';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';
import { FileinputModule } from '../../filemanager/fileinput.module';
import { EmployeeService } from '../../../services/employee.service';
import { DesignationService } from '../../../services/designation.service';

@NgModule({
    declarations:[EmployeeAddContent],
    imports:[CommonModule, FormsModule, ReactiveFormsModule, CKEditor4Module, FileinputModule, RouterModule],
    providers:[DesignationService,EmployeeService],
    exports : [EmployeeAddContent]
})
export class EmployeeAddModule { }
