import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';

import { EmployeeViewContent } from './view';
import { EmployeeService } from '../../../services/employee.service';

@NgModule({
    declarations:[EmployeeViewContent],
    imports:[RouterModule, CommonModule],
    providers:[EmployeeService],
    exports :[EmployeeViewContent]
})
export class EmployeeViewModule {
    
}
