import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Params } from '@angular/router';
import { Employee } from '../../../models/employee.model';
import { Designation } from '../../../models/designation.model';
import { EmployeeService } from '../../../services/employee.service';
import { DesignationService } from '../../../services/designation.service';
import { NotifierService } from '../../notifier/notifier.service';
import { UserService } from '../../../services/user.service';

@Component({
	selector : "ek-employee-edit",
    templateUrl:'./edit.html'
})
export class EmployeeEditContent implements OnInit{
	employeeForm:FormGroup;
	defaultParamsForm:FormGroup;
	employee:Employee;
	formStatus="Normal";
	dfFormStatus:string="Normal";
	designations:Designation[];
	fswState:string="hide";
	allActivities:any[];
	userPerms:any;
	@Input() routes : string;
	@Input() designation : string;
	constructor(
		private fb:FormBuilder,
		private employeeService:EmployeeService,
		private designationService:DesignationService,
		private notifier: NotifierService,
		private activatedRoute:ActivatedRoute,
		private userService:UserService,
		){ 
			
	}

	loadEmployee(employeeId){
		this.employeeService.getOneEmployee(employeeId).subscribe(
			(res)=>{
				this.employee=res;
				let data={
					firstName:res.user.firstName,
					lastName:res.user.lastName,
					email:res.user.email,
					mobile:res.user.mobile,
					phone:res.phone,
					designation: res.designation.id,
					profileImg: res.user.profileImg,
					status:res.status,
					description:res.description
				}
				// if(this.designation){
					// if(this.designation != data.designation){
					// }
				// }
				this.userPerms=res.user.activityPermission;
				this.employeeForm.patchValue(data);
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	
	updateEmployee(): void {
		if(!this.employeeForm.dirty){
			return;
		}
		let data=this.employeeForm.value;
		this.formStatus="Processing";
		this.employeeService.updateEmployee(this.employee.id, data).subscribe(
			res=>{
				this.formStatus="Normal";
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		);
	}

	

	ngOnInit(): void {
		this.activatedRoute.params.subscribe((params: Params) => {
	        let empId = params['id'];
			this.loadEmployee(empId);
		});
		
		this.employeeForm=this.fb.group({
			firstName:['', Validators.required],
			lastName:['', Validators.required],
			email:['', Validators.required],
			mobile:['', Validators.required],
			phone:[''],
			designation: ['', Validators.required],
			profileImg:[''],
			status:[true, Validators.required],
			description:['']
		});
		this.defaultParamsForm=this.fb.group({
			session:[null],
			center:[null],
			firm:[null],
			store:[null],
			financialYear:[null]
		});
		if(!this.designation){
			this.loadDesignations();
		}
	}

	loadDesignations():void{
		this.designationService.getDesignation().subscribe(
			(res)=>{
				this.designations=res;
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}

	loadAccessWindow(){
		this.loadAppActivities();
		this.fswState="show";
		
	}

	loadAppActivities(){
		this.userService.getAppWiseActivity().subscribe(
			res=>{
				this.allActivities=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	setPermission(APP, ACTIVITY, OPTION, e){
		if(e.target.checked){
			if(!this.userPerms){
				this.userPerms={};
			}
			if(!this.userPerms[APP]){
				this.userPerms[APP]={};
			}
			if(!this.userPerms[APP][ACTIVITY]){
				this.userPerms[APP][ACTIVITY]={};
				
			}
			this.userPerms[APP][ACTIVITY][OPTION]=true;
			

		}else{
			delete this.userPerms[APP][ACTIVITY][OPTION];
		}
		
	}

	updateUserPermission(){
		this.userService.updateUserActivityPermission(this.employee.user.id, this.userPerms).subscribe(
			res=>{
				this.employee.user.activityPermission=res;
				this.notifier.alert("Success", "Updated Successfully!", "success", 10000);
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	updateDFParams(){

	}

	closeWindow(){
        this.fswState='hide';
    }
	
}
