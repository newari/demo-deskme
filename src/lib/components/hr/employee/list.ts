import { Component, OnInit, Input  } from '@angular/core';
import { EmployeeService } from '../../../services/employee.service';
import { NotifierService } from '../../notifier/notifier.service';

@Component({
    selector : "ek-employee-list",
    templateUrl:'./list.html'
})
export class EmployeeListContent implements OnInit{ 
    employees;
    columns; 
    filter : any={};
    countBase : number=1;
    panelLoader="none";
    totalRecords  : any;
    @Input() routes : string;
    @Input() designation : string;
    @Input() title : string;
    constructor(
        private employeeService:EmployeeService,
        private notifierService : NotifierService
    ){}

    ngOnInit(): void{
        this.getEmployees();
    }
    getEmployees(){
        if(this.designation){
            this.filter.designation = this.designation;
        }
        this.employeeService.getEmployee(this.filter,true).subscribe(
            (res)=>{
                this.employees = res.body;
                this.totalRecords = res.headers.get('totalRecords')||0;
            },
            (err)=>this.notifierService.alert(err.code,err.message,"danger",2000)
        );
    }
    deleteEmployee(employee){
        let confirm=window.confirm("Are you sure to delete "+employee.name+"?");
        if(!confirm){
            return;
        }
        this.panelLoader="show";
        this.employeeService.deleteEmployee(employee.id).subscribe(
            (res)=>{
                this.employees.splice(this.employees.indexOf(employee), 1);
                this.panelLoader="none";
            },
            (err)=>{
                this.panelLoader="show";
                this.notifierService.alert(err.code,err.message,"danger",2000)
            }
        );
    }
    paginate(e) {
        if (!this.filter) { 
            this.filter = {};
        }
        this.countBase = e.rows * e.page + 1;
        this.filter.page = (e.page + 1);
        this.filter.limit = e.rows;
        this.getEmployees();
    }
}
