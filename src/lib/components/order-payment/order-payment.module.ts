import { NgModule } from '@angular/core';
import { OrderPaymentComponent } from './order-payment.component';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PaymentService } from '../../services/payment.service';
@NgModule({
    declarations:[OrderPaymentComponent],
    imports:[CommonModule, FormsModule, ReactiveFormsModule],
    providers:[PaymentService ],
    exports:[OrderPaymentComponent]
})
export class OrderPaymentModule{

}