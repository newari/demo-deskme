import { Component, OnInit, Input, EventEmitter, Output, OnChanges, SimpleChanges } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PaymentService } from '../../services/payment.service';
import { NotifierService } from '../notifier/notifier.service';

@Component({
    selector:'ek-order-payment',
    templateUrl:'./order-payment.component.html'
})
export class OrderPaymentComponent implements OnInit, OnChanges{
    paymentForm:FormGroup;
    formStatus:string="Normal";
    oldPayments:any[];
    panelLoader:string="none";
    activePmtOptionIndex:number;
    pmtOptions:any[]=[];
    @Input() order:any;
    @Input() paymentHistory:boolean;
    @Output() onPaymentAdd:EventEmitter<any>=new EventEmitter<any>();
    constructor(
        private fb:FormBuilder,
        private paymentService:PaymentService,
        private notifier:NotifierService,
    ){

    }

    ngOnInit(){
        this.paymentForm=this.fb.group({
            primaryMode:[null, Validators.required],
            amount:[0, Validators.required],
            extraAmount:[0, Validators.required],
            currency:['INR', Validators.required],
            note:[''],
            bankRefId:[''],
        });

        if(this.paymentHistory){
            let self=this;
            setTimeout(function(){
                self.loadOldPayments();


            }, 10);
        }

    }
    getPaymentOPtions(){
        let disabled:boolean=false;
        this.pmtOptions=[];
        for(let i=0; i<this.order.availablePaymentOptions.length; i++){
            if(this.order.availablePaymentOptions[i].removed){
                continue;
            }else if(!this.order.availablePaymentOptions[i].paid||this.order.availablePaymentOptions[i].paid<this.order.availablePaymentOptions[i].amount){
                this.pmtOptions.push({index:i,title:this.order.availablePaymentOptions[i].title,amount:this.order.availablePaymentOptions[i].amount, disabled:disabled});
                // disabled=true;
            }else{
                this.pmtOptions.push({index:i,title:this.order.availablePaymentOptions[i].title+" (Paid)", amount:this.order.availablePaymentOptions[i].amount, disabled:true});

            }
        }
    }
    ngOnChanges(changes: SimpleChanges): void {
        if(changes['order']) {
            if(this.order&&this.order.availablePaymentOptions){
                this.getPaymentOPtions();
            }
        }
    }


    loadOldPayments(){
        if(!this.order||!this.order.id){
            this.oldPayments=null;
            return;
        }
        this.panelLoader="Processing";
        this.paymentService.getPayment({order:this.order.id}).subscribe(
            res=>{
                this.oldPayments=res;
                this.panelLoader="none";
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )
    }

    addPayment(){
        if(this.formStatus=="Processing"){
            return;
        }
        let frmData=this.paymentForm.value;
        if(!this.paymentForm.valid){
            this.notifier.alert('Error', 'Please fill all the mandatory field!', "danger", 5000);
            return
        }
        if(frmData.amount<=0&&frmData.extraAmount<=0){
            this.notifier.alert('Error', 'Amount must be greater than 0', "danger", 5000);
            return
        }
        let data:any={
            orderId:this.order.id,
            orderNo:this.order.orderNo,
            primaryMode:frmData.primaryMode,
            amount:frmData.amount,
            extraAmount:frmData.extraAmount,
            user:this.order.user.id||this.order.user,
            store:this.order.availablePaymentOptions[this.activePmtOptionIndex].store||this.order.store.id||this.order.store,
            currency:frmData.currency,
            transactionStatus:'Success',
            paymentOption:this.order.availablePaymentOptions[this.activePmtOptionIndex].title,
            pmtOptIndex:this.activePmtOptionIndex,
            details:{
                mode:frmData.primaryMode,
                note:frmData.note,
                bankRefId:frmData.bankRefId
            }
        };
        this.formStatus="Processing";
        this.paymentService.addPayment(data).subscribe(
            res=>{
                if(this.oldPayments){
                    this.oldPayments.unshift(res);
                }
                this.order.availablePaymentOptions[this.activePmtOptionIndex].paid=data.amount;
                this.getPaymentOPtions();
                this.onPaymentAdd.emit(res);
                this.formStatus="Normal";
                this.paymentForm.reset();
                this.paymentForm.patchValue({currency:'INR'});
            },
            err=>{
                console.log(err);
                this.formStatus="Normal";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )

    }



    setPaymentOption(optIndex){
        this.activePmtOptionIndex=optIndex;
        let amt=this.order.availablePaymentOptions[optIndex].amount;
        this.paymentForm.patchValue({amount:amt});
    }
}
