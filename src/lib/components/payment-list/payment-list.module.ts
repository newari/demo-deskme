import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CommonModule } from '@angular/common';
import { PaymentListComponent } from './payment-list.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { PaginatorModule } from 'primeng/components/paginator/paginator';
import { PaymentService } from '../../services/payment.service';
import { ProductService } from '../../services/product.service';
import { StoreService } from '../../services/store.service';
import { CenterService } from '../../services/center.service';
import { FinancialYearService } from '../../services/financial-year.service';

@NgModule({
    declarations:[PaymentListComponent],
    exports:[PaymentListComponent],
    providers:[PaymentService, ProductService, StoreService, CenterService, FinancialYearService],
    imports:[CommonModule, FormsModule,
        ReactiveFormsModule, RouterModule, PaginatorModule]
})
export class PaymentListModule {
    
}
