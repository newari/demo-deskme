import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Angular2Csv } from 'angular2-csv';
import { Coption } from '../../models/coption.model';
import { PaymentService } from '../../services/payment.service';
import { NotifierService } from '../notifier/notifier.service';
import { CoptionService } from '../../services/coption.service';
import { ProductService } from '../../services/product.service';
import { StoreService } from '../../services/store.service';
import { CenterService } from '../../services/center.service';
import { FinancialYearService } from '../../services/financial-year.service';
import { FinancialYear } from '../../models/financial-year.model';
import { AuthService } from '../../services/auth.service';

@Component({
    selector:'ek-payment-list-component',
    templateUrl:'./payment-list.component.html'
})
export class PaymentListComponent implements OnInit{ 
    payments:any[];

    columns; 
    panelLoader="none";
    filterOpen:boolean=false;
    filterForm:FormGroup;
    orderStatus:Coption[];
    approvedSearchForm:FormGroup;
    filterRowForm:FormGroup;
    filteredTags:any;
    filterData:any={};
    activeFilter:any;
    countBase:number=1;
    totalRecords:number;
    exportMenuStatus:string;
    fys:FinancialYear[];
    activeFY:string;
    perms?:any
    @Input() store:string;
    @Input() viewOpt:string;
    @Input() invoiceOpt:boolean;
    @Input() deleteOpt:boolean;
    @Input() filter:any;
    @Input() populateStore:boolean;
    constructor(
        private paymentService:PaymentService,
		private notifier: NotifierService,
        private coptionService: CoptionService,
        private productService:ProductService,
        private storeService:StoreService,
        private centerService:CenterService,
        private financialYearService:FinancialYearService,
        private fb: FormBuilder,
        private authService:AuthService
    ){}

    ngOnInit(): void{
        let filter:any={};
        if(this.filter){
            filter=this.filter;
        }
        if(this.store){
            filter.store=this.store;
        }
        if(this.populateStore){
            filter.populateStore=this.populateStore;
        }
        this.activeFilter=filter;
        
        let isp:boolean=null;
        this.filterForm=this.fb.group({
            from:[""],
            to:[""],
            status:[""],
            isApproved:[isp],
            store:[null],
            orderNo:[""],
            invoiceNo:[""]
        });
        this.filterRowForm=this.fb.group({
            orderNo:[""],
            receiptNo:[""],
            customerEmail:[""],
            paymentStatus:[null],
            product:[null],
            createdAt:[""],
            primaryMode:[null],
            transactionStatus:[null]
        });
        this.approvedSearchForm=this.fb.group({
            approvedDateFrom:[null],
            approvedDateTo:[null],
            isApproved:[true],
            store:[null]
        });
        this.setFilterRow();
        let admin= this.authService.admin();
        let storeFilter:any={status:true};
        if(admin.defaultParams&&admin.defaultParams.firm){
            storeFilter.firm=admin.defaultParams.firm.id
        }
        this.loadStores(storeFilter);
        this.loadFYs();
    }

    loadPayments(filter?:any):void{
        this.panelLoader="Processing";
        if(!filter){
            filter={};
        }
        filter.populateOrder=true;
        if(!filter.financialYear&&this.activeFY){
            filter.financialYear=this.activeFY;
            this.activeFilter.financialYear=this.activeFY;
        }
        this.paymentService.getPayment(filter, true).subscribe(
            (data)=>{

                this.payments=data.body;
                this.totalRecords = data.headers.get('totalRecords')||0;
                this.panelLoader="none";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000);
                this.panelLoader="none";
                
            }
        );
        
    }

    deletePayment(order){
        this.panelLoader="show";
        let confirm=window.confirm("Are can not delete "+order.name+"?");
        return;
        
        
    }
    approvePayment(paymentIndex){
        console.log(this.payments[paymentIndex].transactionStatus);
        if(this.payments[paymentIndex].transactionStatus!=="Success"){
            let conf=window.confirm("This payment is not completed yet properly. Are you sure to generate Invoice for this payment?");
            if(!conf){
                return;
            }
        }
        this.panelLoader="show";
        this.paymentService.approvePayment(this.payments[paymentIndex].id).subscribe(
            res=>{
                this.panelLoader="none";
                this.payments[paymentIndex].isApproved=res.isApproved;
                this.payments[paymentIndex].receiptNo=res.receiptNo;
                this.payments[paymentIndex].status=res.status;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger");
                this.panelLoader="none";
            }
        )
    }
    setFilterBox(){
        
        this.coptionService.getCoption({option:'ORDERSTATUS'}).subscribe(
            res=>{
                this.orderStatus=res;
            },
            err=>{
                this.notifier.alert(err.code, err.details||err.message, "danger", 10000);
            }
        );
        

        this.filterOpen=true;
    }
    loadStores(filter?:any){
        // let filter:any={};

        this.storeService.getStore(filter).subscribe(
            res=>{
                this.filterData.stores=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000);
            }
        )
    }
    loadFYs(){
		this.financialYearService.getFinancialYear().subscribe(
			res=>{
                this.fys=res;
                console.log(this.fys);
                for(let i=0; i<res.length; i++){
                    if(res[i].isDefault){
                        this.activeFY=res[i].id;
                        break;
                    }
                }
                if(!this.activeFY){
                    this.activeFY=res[res.length-1].id;
                }
                this.activeFilter.financialYear=this.activeFY;
                this.loadPayments(this.activeFilter);
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
	}
    setFilterRow(){
        this.productService.getProduct({status:true}).subscribe(
            res=>{
                this.filterData.products=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000);
            }
        );
        this.centerService.getCenter({status:true}).subscribe(
            res=>{
                this.filterData.centers=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 10000);
            }
        )
        
    }
    loadPaymentsWithFilter(){
        let fltr=this.filterForm.value;
        this.filteredTags=fltr;
        if(this.populateStore){
            fltr.populateStore=this.populateStore;
        }
        this.activeFilter=fltr;
        this.activeFilter.financialYear=this.activeFY;
        this.countBase=1;
        this.loadPayments(fltr);
    }
    loadPaymentsWithRowFilter(){
        let fltr=this.filterRowForm.value;
        if(this.populateStore){
            fltr.populateStore=this.populateStore;
        }
        this.activeFilter=fltr;
        this.activeFilter.financialYear=this.activeFY;
        this.countBase=1;
        this.loadPayments(fltr);
    }
    filterWithFY(fyId:string){
        if(!this.activeFilter){
            this.activeFilter={populateStore:this.populateStore};
        }
        this.activeFilter.financialYear=fyId;
        this.countBase=1;
        this.loadPayments(this.activeFilter);
    }
    clearFilter(){
        this.filterOpen=false;
        this.filterForm.reset();
        this.filteredTags=null;
        this.loadPayments();
    }
    exportDataOld(){
        var data=[];
        this.payments.forEach(function(pmt){
            data.push({
                "Order No.":pmt.orderNo,
                "Receipt No.":pmt.receiptNo||'NA',
                "Program/Product(1st)":pmt.order.products[0].title,
                "Customer Name":pmt.order.buyerDetail.name,
                "Email":pmt.order.buyerDetail.email,
                "Mobile":pmt.order.buyerDetail.mobile,
                "Payment Amount(INR)":pmt.amount,
                "Order Total Paid(INR)":pmt.order.paid,
                "Order Total(INR)":pmt.order.total,
                "Mode":pmt.primaryMode,
                "Payment Status":pmt.transactionStatus,
                "Order Status":pmt.order.status,
                "Payment Date":pmt.createdAt,
                "Order Date":pmt.order.createdAt
            })
        })    

        new Angular2Csv(data, 'Payments List', { 
                fieldSeparator: ',',
                quoteStrings: '"',
                decimalseparator: '.',
                showLabels: true, 
                showTitle: true 
              });
    }
    exportData(){
        let fltr:any={};
        if(this.activeFilter){
            fltr=this.activeFilter;
        }
        
        fltr.limit='all';
        fltr.sort="receiptIndex";
        fltr.orderBy="ASC";
        this.panelLoader="show";
        this.paymentService.exportOrders(fltr).subscribe(
            res=>{
                this.panelLoader="none";
                this.notifier.alert("Done!", "Data exported successfully.", "success", 5000);
                window.location.href=res.fileUrl;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'daner', 10000);
            }
        )
    }
    exportWithTax(){
        let fltr:any={};
        if(this.activeFilter){
            fltr=this.activeFilter;
        }
        
        fltr.limit='all';
        fltr.sort="receiptIndex";
        fltr.orderBy="ASC";
        this.panelLoader="show";
        this.paymentService.exportPaymentsWithTax(fltr).subscribe(
            res=>{
                this.panelLoader="none";
                this.notifier.alert("Done!", "Data exported successfully.", "success", 5000);
                window.location.href=res.fileUrl;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'daner', 10000);
            }
        )
    }
    exportWithoutTax(){
        let fltr:any={};
        if(this.activeFilter){
            fltr=this.activeFilter;
        }
        
        fltr.limit='all';
        fltr.sort="receiptIndex";
        fltr.orderBy="ASC";
        this.panelLoader="show";
        this.paymentService.exportPaymentsWithoutTax(fltr).subscribe(
            res=>{
                this.panelLoader="none";
                this.notifier.alert("Done!", "Data exported successfully.", "success", 5000);
                window.location.href=res.fileUrl;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'daner', 10000);
            }
        )
    }
    loadApprovedPayments(){
        let fltr=this.approvedSearchForm.value;
        if(this.populateStore){
            fltr.populateStore=this.populateStore;
        }
        this.activeFilter=fltr;
        this.countBase=1;
        this.loadPayments(fltr);
    }
    paginate(e){
        if(!this.activeFilter){
            this.activeFilter={populateOrder:true};
        }
        this.countBase=e.rows*e.page+1;
        this.activeFilter.page=(e.page+1);
        this.activeFilter.limit=e.rows;
        this.loadPayments(this.activeFilter);
    }

    exportWithExtraOption(){
        let fltr:any={};
        if(this.activeFilter){
            fltr=this.activeFilter;
        }
        
        fltr.limit='all';
        fltr.sort="receiptIndex";
        fltr.orderBy="ASC";
        this.panelLoader="show";
        this.paymentService.exportPaymentWithExtraOption(fltr).subscribe(
            res=>{
                this.panelLoader="none";
                this.notifier.alert("Done!", "Data exported successfully.", "success", 5000);
                window.location.href=res.fileUrl;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'daner', 10000);
            }
        )
    }
    exportWithTaxAndExtraOption(){
        let fltr:any={};
        if(this.activeFilter){
            fltr=this.activeFilter;
        }
        
        fltr.limit='all';
        fltr.sort="receiptIndex";
        fltr.orderBy="ASC";
        this.panelLoader="show";
        this.paymentService.exportPaymentWithTaxWithExtraOption(fltr).subscribe(
            res=>{
                this.panelLoader="none";
                this.notifier.alert("Done!", "Data exported successfully.", "success", 5000);
                window.location.href=res.fileUrl;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'daner', 10000);
            }
        )
    }
    exportWithoutTaxAndExtraOption(){
        let fltr:any={};
        if(this.activeFilter){
            fltr=this.activeFilter;
        }
        
        fltr.limit='all';
        fltr.sort="receiptIndex";
        fltr.orderBy="ASC";
        this.panelLoader="show";
        this.paymentService.exportPaymentWithoutTaxWithExtraOption(fltr).subscribe(
            res=>{
                this.panelLoader="none";
                this.notifier.alert("Done!", "Data exported successfully.", "success", 5000);
                window.location.href=res.fileUrl;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'daner', 10000);
            }
        )
    }
    exportRefundedPayments(){
        let fltr:any={};
        if(this.activeFilter){
            fltr=this.activeFilter;
        }
        
        fltr.limit='all';
        fltr.sort="receiptIndex";
        fltr.orderBy="ASC";
        this.panelLoader="show";
        this.paymentService.exportRefundedPayments(fltr).subscribe(
            res=>{
                this.panelLoader="none";
                this.notifier.alert("Done!", "Data exported successfully.", "success", 5000);
                window.location.href=res.fileUrl;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'daner', 10000);
            }
        )
    }
}
