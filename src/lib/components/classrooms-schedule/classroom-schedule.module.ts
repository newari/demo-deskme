import { NgModule } from '@angular/core';
import { ClassroomsScheduleComponent } from './component';
import { CommonModule } from '@angular/common';
import { TimeinapPipeModule } from '../../filters/timeinap.pipe';
import { CalendarService } from '../../services/calendar.service';
import { ClassroomService } from '../../services/classroom.service';
import { DialogModule, CalendarModule, SharedModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
    declarations: [ClassroomsScheduleComponent],
    imports:[CommonModule, TimeinapPipeModule, DialogModule, CalendarModule,
        SharedModule, FormsModule,
        ReactiveFormsModule],
    exports:[ClassroomsScheduleComponent],
    providers:[CalendarService, ClassroomService]
})
export class ClassroomScheduleModule {

}