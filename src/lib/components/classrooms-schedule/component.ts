import { Component, OnInit, Input } from '@angular/core';
import { ClassroomCoordinatorService } from '../../services/classroom-coordinator.service';
import * as moment from 'moment';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Event } from '../../models/event.model';
import { AuthService } from '../../services/auth.service';
import { EventService } from '../../services/event.service';
import { ClassroomService } from '../../services/classroom.service';
import { NotifierService } from '../notifier/notifier.service';

@Component({
    selector: 'ek-classrooms-schedule', //cc=>class coordinator classrooms schedule
    templateUrl: './component.html'
})
export class ClassroomsScheduleComponent implements OnInit { 
    crEvents:Event[];
    today:string;
    selectedEvent=null;
    finishFrm:FormGroup;
    startFrm:FormGroup;
    startBreakFrm:FormGroup;
    finishBreakFrm:FormGroup;
    startedOn:Date;
    endedOn:Date;
    defSDate:Date;
    defEDate:Date;
    emd={//Event Modifier Dialogue/Modal
        display:false,
        bef:false,
        bsf:false,
        heading:''
    }

    @Input() classroomIds:string[];

    constructor(
      private classroomService:ClassroomService,
      private authService:AuthService,
      private eventService:EventService,
      private notifier:NotifierService,
      private fb:FormBuilder
    ){
        
    }
    ngOnInit(){
        this.today=moment().format("DD/MM/YYYY");
        var thisMember=this.authService.admin();
        this.classroomService.getCoordinatorClassrooms(thisMember.id).subscribe(
            res=>{
                if(res.length<1){
                    return this.notifier.alert("Opps!", 'No classroom associated to this admin/employee', "info", 10000);
                }
                let cids=[];
                for(let i=0; i<res.length; i++){
                    cids.push(res[i].id);
                }
                this.loadClassroomsSchedule(cids);
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )

        this.finishFrm=this.fb.group({
            breakTime:[0],
            endedAt:[this.endedOn, Validators.required]
        });
        this.startFrm=this.fb.group({
            startedOn:[this.startedOn, Validators.required]
        });
        this.startBreakFrm=this.fb.group({
            breakStart:[this.startedOn, Validators.required]
        });
        this.finishBreakFrm=this.fb.group({
            breakEnd:[this.startedOn, Validators.required]
        })
    }
    
    loadClassroomsSchedule(classroomIds, filter?:any){
        this.classroomService.getUpcomingSchedule(classroomIds, filter).subscribe(
            (res)=>{
                this.crEvents=res;
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }

    selectEvent(ik){
        this.selectedEvent=this.crEvents[ik];
        this.startedOn=new Date(this.selectedEvent.start);
        this.endedOn=new Date(this.selectedEvent.end);
        // console.log(new Date(moment(this.selectedEvent.start).utc().format()));
        this.defSDate=this.startedOn;
        this.defEDate=this.endedOn;
        this.finishFrm.value.endedAt=this.endedOn;
    }
    startEvent(){
        if(!this.startFrm.value.startedOn){
            console.log('Error!');
            return;
        }
        this.eventService.startEvent(this.selectedEvent.id, this.startFrm.value.startedOn).subscribe(
            (res)=>{
                console.log(res);
                this.emd.display=false;
                this.selectedEvent.status="RUNNING";
                this.selectedEvent.startedAt=this.startFrm.value.startedOn;
            },
            (err)=>{
                console.log(err);
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }

    finishEvent(){
        if(!this.finishFrm.value.endedAt){
            console.log('Error!');
            return;
        }
        let frm=this.finishFrm.value;
        this.eventService.finishEvent(this.selectedEvent.id, frm.endedAt, frm.breakTime).subscribe(
            (res)=>{
                this.emd.display=false;
                this.selectedEvent.status="FINISHED";
                this.selectedEvent.endedAt=frm.endedAt;
                this.selectedEvent.breakDuration=frm.breakTime;
            },
            (err)=>{
                console.log(err);
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }

    changeEventStatus(newStatus){
        this.eventService.changeStatus(this.selectedEvent.id, newStatus).subscribe(
            (res)=>{
                this.selectedEvent.status=newStatus;
            },
            (err)=>{
                console.log(err);
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }

    startBreak(){
        if(!this.startBreakFrm.value.breakStart){
            console.log('Error!');
            return;
        }
        this.eventService.startBreak(this.selectedEvent.id, this.startBreakFrm.value.breakStart).subscribe(
            (res)=>{
                console.log(res);
                this.emd.display=false;
                if(!this.selectedEvent.breaks){
                    this.selectedEvent.breaks=[];
                }
                this.selectedEvent.breaks.push({
                    start:this.startBreakFrm.value.breakStart,
                    end:null
                });
            },
            (err)=>{
                console.log(err);
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }

    finishBreak(){
        if(!this.finishBreakFrm.value.breakEnd){
            console.log('Error!');
            return;
        }
        var ttlDuration=0;
        
        for(let bi=0; bi<this.selectedEvent.breaks.length; bi++){
            ttlDuration+=moment(this.finishBreakFrm.value.breakEnd).diff(this.selectedEvent.breaks[bi].start, 'minutes');
        }
        
        
        this.eventService.finishBreak(this.selectedEvent.id, this.finishBreakFrm.value.breakEnd, ttlDuration).subscribe(
            (res)=>{
                console.log(res);
                this.emd.display=false;
                this.selectedEvent.breakDuration=ttlDuration;
                this.selectedEvent.breaks[this.selectedEvent.breaks.length-1].end=this.finishBreakFrm.value.breakEnd;
                // this.selectedEvent.startedAt=this.startFrm.value.startedOn;
            },
            (err)=>{
                console.log(err);
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }

    startEventEMD():void{
        this.startFrm.patchValue({
            startedOn:this.startedOn
        });
        this.emd.display=true;
        this.emd.bsf=false;
        this.emd.bef=false;
        this.emd.heading='Start this '+this.selectedEvent.type;
    }

    finishEventEMD():void{
        this.finishFrm.patchValue({
            breakTime:0,
            endedAt:this.endedOn
        });
        this.emd.display=true;
        this.emd.bsf=false;
        this.emd.bef=false;
        this.emd.heading='Finish this '+this.selectedEvent.type;
    }

    startBreakEMD():void{
        this.emd.display=true;
        this.emd.bsf=true;
        this.emd.bef=false;
        this.emd.heading='Break Start Time';
    }

    finishBreakEMD():void{
        this.emd.display=true;
        this.emd.bsf=false;
        this.emd.bef=true;
        this.emd.heading='Break End Time';
    }
}
