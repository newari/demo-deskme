import { Component, Input } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
//Import Component form the angular core package
//Compoent Decorator
@Component({
  //Name of our tag
  selector: 'ek-simple-slider',
  //Template for the tag
  template: `
  <div class="ek-simple-slider">
    <ul>
        <ng-container *ngFor="let item of slideItems; let i=index" >
            <li [ngClass]="i==activeIndex?'active':(i==nextIndex?'next':(i==prevIndex?'prev':''))" >
                <img src="{{item.url}}">
            </li>
        </ng-container>
    </ul>
</div>
  `,
  //Styles for the tag
  styles: [`
  .ek-simple-slider{
    width: 100%;
    height: 100%;
}
.ek-simple-slider ul{
    width: 100%;
    padding:0px;
    margin:0px;
    position: relative;
    height: 100%;
    overflow:hidden
}
.ek-simple-slider ul li{
    display: inline-block;
    position: absolute;
    width: 100%;
    height: 100%;
    left:100%;

}
.ek-simple-slider ul li img{
    width: 100%;
    height: 100%;
}
.ek-simple-slider ul li.active{
    left:0px;
    transition: left 1s linear;
}

.ek-simple-slider ul li.prev{
    left:-100%;
    transition: left 1s linear;

}

.ek-simple-slider ul li.next{
    left:100%;
}

  `],
})
//Carousel Component itself
export class EkSimpleSlider implements OnInit{
    //images data to be bound to the template
    @Input() slideItems:any[];
    @Input() duration:number;
    activeIndex:number;
    nextIndex:number;
    prevIndex:number;
    constructor(){

    }
    ngOnInit(){
        this.activeIndex=0;
        this.nextIndex=1;
        this.prevIndex=-1;
        if(!this.duration){
            this.duration=5000;
        }
        
        let self=this;
        setTimeout(() => {
            if(self.slideItems&&self.slideItems.length>1){
                setInterval(function(){
                    self.nextSlide();
                }, self.duration);
            }
        }, 5000);
            
            
    }
    nextSlide(){
        this.prevIndex=this.activeIndex;
        this.activeIndex=this.nextIndex;
        if(this.activeIndex==(this.slideItems.length-1)){
            this.nextIndex=0;
        }else{
            this.nextIndex++;
        }
        
    }
}