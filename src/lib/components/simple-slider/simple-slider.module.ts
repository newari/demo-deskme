import { NgModule } from "@angular/core";
import { EkSimpleSlider } from './simple-slider.component';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations:[EkSimpleSlider],
    imports:[CommonModule],
    exports:[EkSimpleSlider]
})
export class EkSimpleSliderModule {

}