import { Component,OnInit, forwardRef, Input, AfterViewInit } from '@angular/core';
import { CKEditor4 } from 'ckeditor4-angular/ckeditor';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { EdukitConfig } from '../../../ezukit.config';

@Component({
    selector: 'ck-editor',
    template: `
    <ckeditor editorUrl="https://edkt.net/ckeditor/ckeditor.js" [data]="content" (change)="onChange($event)" [config]="ckConfig" type="divarea"></ckeditor>
    `,
    providers: [
      { 
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CkEditorComponent),
      multi: true
      }
  ]
})  
export class CkEditorComponent implements OnInit, AfterViewInit {
  showImage : boolean=false;
  finalData='';
  content:string;
  API_URL;
  @Input()customHeight?:number;
  ckConfig : any={};
  ngOnInit(): void {
    this.API_URL = EdukitConfig.BASICS.API_URL;
    this.ckConfig={
      height : this.customHeight?this.customHeight:'7em',
      // skin:'kama',
      // customConfig:'/plugins/pasteUploadImage',
      extraPlugins: 'mathjax,div,panel,panelbutton,button,floatpanel,colorbutton,justify,pastefromword,image2',
      removePlugins: 'image',  
      mathJaxLib : '//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML',  
      filebrowserUploadUrl: this.API_URL+'/public/file/upload-ckeditor',  
      cloudServices_tokenUrl:'123ed',   
      cloudServices_uploadUrl: this.API_URL+'/public/file/upload-ckeditor-onpaste', 
      imageUploadUrl : this.API_URL+'/public/file/upload-ckeditor',    
      removeButtons : '',
      allowedContent : true,
      
      // toolbar : [
      //   { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
      //   { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
      //   { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
      //   { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
      //   '/',
      //   { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat' ] },
      //   { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
      //   { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
      //   { name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
      //   '/',
      //   { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
      //   { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
      //   { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
      //   { name: 'others', items: [ '-' ] },
      //   // { name: 'about', items: [ 'About' ] }
      // ],
      toolbarGroups : [
        { name: 'document',    groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
        { name: 'insert' },
        // { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
        // { name: 'forms' },
        // '/',
        // { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph',   groups: [ 'list',  'blocks','align'] }, //'indent','bidi'
        { name: 'basicstyles'},
        { name: 'links' },
        // '/',
        { name: 'styles' },
        { name: 'colors' },
        { name: 'tools' },
        { name: 'others' },
        // { name: 'about' }
      ]
    };
  };
  
  public onChange( event: CKEditor4.EventInfo ) {
    let _this=this;
    setTimeout(function(){
        _this.finalData=event.editor.getData();
      _this.propagateChange(_this.finalData);
      },0);
  }
  addCKImage(src){
    if(src){
      this.finalData=this.finalData + '<p><img src="'+src+'"/></p>'; 
      this.content = this.finalData;
      this.showImage = false;
    }
  }
  writeValue(value: any) {
      if (value !== undefined) {
          this.content = value;
      }
  }
  propagateChange = (_: any) => {};

  registerOnChange(fn) {
      this.propagateChange = fn;
  }

  registerOnTouched() {}

  ngAfterViewInit(): void {
     this.ckConfig.height= this.customHeight||'7em';
  }
}

// <div class="ck-btn">
//   <a (click)="showImage=true" ><i class="fa fa-picture-o"></i></a>
// </div>

// <p-dialog header=" upload Image" [(visible)]="showImage" [width]="400">
// <div class="row">
//   <div class="col-md-10"><ek-fileinput class="form-control" [(ngModel)]="src"></ek-fileinput></div>
//   <div class="col-md-2"><a class="ek-btn btn-primary" (click)="addCKImage(src)">ADD</a></div>
// </div>
// </p-dialog>