import { NgModule } from '@angular/core';

import { CKEditorModule } from 'ckeditor4-angular'
import { CkEditorComponent } from './ckeditor4.component';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FileinputModule } from '../filemanager/fileinput.module';
import { DialogModule } from 'primeng/dialog';
@NgModule({
  declarations: [CkEditorComponent],
  imports:[CommonModule,FormsModule, CKEditorModule,DialogModule,FileinputModule],
  exports: [CkEditorComponent]
})
export class CKEditor4Module {}
