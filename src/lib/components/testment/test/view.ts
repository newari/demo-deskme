import { Component, OnInit, Input  } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Test } from '../../../models/test.model';
import { Coption } from '../../../models/coption.model';
import { TestService } from '../../../services/test.service';
import { CoptionService } from '../../../services/coption.service';
import { NotifierService } from '../../notifier/notifier.service';
import { QsetService } from '../../../services/qset.service';
import { AuthService } from '../../../services/auth.service';

@Component({
    selector:'ek-tm-test-view',
    templateUrl:'./view.html'
})
export class TestViewContent implements OnInit{ 
    test:Test;
    testId;
    questions;
    display: boolean = false;
    courses:Coption[];
    streams;
    qsetTitles;
    sectionQsCount;
    sectionIndex;
    frmLoader="none";
    formStatus="Normal";
    filter: any = {};
    panelLoader="none";
    @Input() routes : string;
    @Input() title : string;
    @Input() APP : string;
    @Input() ACTIVITY:string;
    @Input()userPerms:any[]=[];
    constructor(
        private testService:TestService,
        private activatedRoute:ActivatedRoute,
        private coptionService:CoptionService,
        private notifier: NotifierService,
        private qsetService : QsetService,
        private authService : AuthService,
        private router: Router
    ){}

    ngOnInit(): void{
        
        this.activatedRoute.params.subscribe((params: Params) => {
	        this.testId = params['id'];
            this.getTest(this.testId);
        });
        this.loadCourses();
        this.loadStreams();
        this.loadTitles();
    }
    getTest(testId){
        this.panelLoader="show";
        this.testService.getOneTest(testId).subscribe( (data)=>{
                this.test=data;
                this.panelLoader="none";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader="none";
            }
        );
    }
    assignQsetDialog(sectionIndex) {
        this.display = true;
        this.sectionIndex = sectionIndex;
    }
    assignQset(qsetId){
        let data={
            qsetId : qsetId,
            testId : this.testId,
            sectionIndex : this.sectionIndex,
        }
        this.qsetService.assignQset(data).subscribe(
            (res)=>{
                this.test = res[0];
                this.display=false;
                this.formStatus="Normal";
                this.notifier.alert('Success', 'Assigned Successfully', 'success', 5000 );
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.formStatus="Normal";
            }
        );
    }
    deleteTest(testId){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete ?");
        if(!confirm){
            this.panelLoader="none";
            return;
        }
        this.testService.deleteTest(testId).subscribe(
            (res)=>{
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
                this.panelLoader="deleted";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    loadCourses(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'COURSE'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.courses=res;
			
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
			
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
    }
    setTitle(streamId,courseId){
        let filter: any = {};
        filter.stream = streamId;
        if (courseId && courseId!='') {
            filter.course = courseId;
        }
        this.loadTitles(filter);
    }
    loadTitles(filter?:any){
        let fData = Object.assign({limit : 'all',oShip : true,isPublished : true}, filter);
        this.qsetService.getQsetTitles(fData).subscribe(
            (res) => {
                this.qsetTitles = res;
                if(this.qsetTitles == ''){
                    this.notifier.alert('danger',"No Data Found", 'danger', 5000);
                }
            },
            (err) => { this.notifier.alert(err.code, err.message, 'danger', 5000); }
        )
    }
}
