import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { Coption } from '../../../models/coption.model';
import { TestService } from '../../../services/test.service';
import { NotifierService } from '../../notifier/notifier.service';
import { CoptionService } from '../../../services/coption.service';
import { TestInstructionService } from '../../../services/testInstruction.service';
import { EdukitConfig } from '../../../../ezukit.config';
import { ClientService } from '../../../services/client.service';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
    selector:'ek-tm-test-add',
    templateUrl:'./add.html'
})
export class TestAddContent implements OnInit{
    testForm:FormGroup;
	formStatus="Normal";
	frmLoader="none";
	courses:Coption[];
	streams;
	instructions;
	basicAPI;
	sectionBoxStatus : boolean;
	sections;
	user :any={};
	validMessage :any=" ' '  \" \" Special character are not Allowed";
	clientConfig : any={};
	@Input() routes : string;
	@Input() title : string;
	@Input() isPaper : boolean = false; 
	@Input() APP:string='TESTMENT';
	@Input() ACTIVITY:string='TEST';
	@Input () userPerms:any;
	constructor(
		private fb:FormBuilder,
		private testService:TestService,
		private notifier: NotifierService,
		private coptionService:CoptionService,
		private testInstructionService : TestInstructionService,
		private clientService : ClientService,
		private authService : AuthService,
		private router : Router
		){}
	ngOnInit(): void {
			
		this.basicAPI = EdukitConfig.BASICS.API_URL;
		this.user = this.authService.user();
		if(this.user){
			this.clientService.getClientConfig({client : this.user.client}).subscribe(
				res=>{
				this.clientConfig = res;
				},err=>{console.log(err);}
			);
		}
		this.form();
		this.loadCourses();	
		this.loadStreams();
		this.getInstructions();
	}
	form(){
		this.testForm=this.fb.group({
			title:['', Validators.required],
			alias:['', Validators.required],
			testType:['', Validators.required],
			theme : ['', Validators.required],
			duration:['', Validators.required],
			course:['', Validators.required],
			stream:['', Validators.required],
			maxScore:['', Validators.required],
			defaultLang:['EN', Validators.required],
			instruction:[''],
			totalQs:['', Validators.required],
			resultDate : [''],
			startDate:['', Validators.required],
			endDate:['', Validators.required],
			status:[false,Validators.required],
			description:[''],
			solVideos: this.fb.group({
				source : [''],
				ids :[]
			}),
			preDiscussionVideos : this.fb.group({
				source : [''],
				ids :[]
			}),
			isCalculator:[false],
			oneTimeSubmission : [false,Validators.required],
			sections: this.fb.array([]),
			isPaper : [this.isPaper],
			inTM : [false],
			forSell : [false],
			cost : [0,Validators.required],
			mrp : [0,Validators.required],
			discount : [0,Validators.required],
			maqPM : [false],
			mtqPM : [false],
			isFree : [false],
		});
		this.setSections(1);
	}
	setSections(sectionCount?:number) {
		this.sections = this.testForm.get('sections') as FormArray;
		for(var i=0;i<sectionCount; i++){
			this.sections.push(this.setControls());
		}
	}
	deleteSection(index){
		this.sections = this.testForm.get('sections') as FormArray;
		 this.sections.removeAt(index,1);
	}
	setControls(): FormGroup {
		return this.fb.group({
			title:['', Validators.required],
			totalMarks:[''],
			totalQs:[''],
			maxTime:[''],
			attemptLimit : [''],
			bonusMarks : [{}],
			qset: "",
			isQsetAssigned:false
		});
	}
	sourceChange(){
		window.alert('Please Make sure your video ids is from selected Source');
	}
	addTest(): void {
		let testData:any={};
		testData=this.testForm.value;
		delete testData.sectionCount;
		var totalSecQuestion = 0;
		var totalSecMarks = 0;
		var totalSecDuration = 0;
		testData.startDate=new Date(testData.startDate.setSeconds(0));
		testData.endDate=new Date(testData.endDate.setSeconds(0));
		for(var i = 0; i< testData.sections.length; i++){
			totalSecQuestion += testData.sections[i].totalQs; 
			totalSecMarks += testData.sections[i].totalMarks;
			totalSecDuration += testData.sections[i].maxTime;
		}
		if(testData.totalQs != totalSecQuestion){
			return this.notifier.alert('Danger ', 'Total Question is not equal to Section Total Question', 'danger', 5000 );
		}
		if(testData.maxScore != totalSecMarks){
			return this.notifier.alert('Danger ', 'Total Marks is not equal to Section Total Marks', 'danger', 5000 );
		}
		if(testData.duration != totalSecDuration){
			return this.notifier.alert('Danger ', 'Total Duration is not equal to Section Total Max Time', 'danger', 5000 );
		}
		if(testData.testType == "PRACTICE"){
			testData.resultDate="";
		}
		this.formStatus="Processing"; 
		this.testService.addTest(testData).subscribe(
			res=>{
				this.form(); 
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Added Successfully', 'success', 5000 );
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
		
	}
	loadCourses(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'COURSE'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.courses=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.streams=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	getInstructions(){
        this.testInstructionService.getInstruction().subscribe(
            (data)=> {this.instructions = data;},
            (err)=> this.notifier.alert(err.code, err.message, 'danger', 5000 )
        );
	}
	testForSell(el){ 								// (typeof (el) == "boolean" && el == true) 
		if (el.target.checked){
			this.testForm.patchValue({forSell : true});
		} else {
			this.testForm.patchValue({forSell : false});
		}
		console.log(this.testForm.value);
	}
}
