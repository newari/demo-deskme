import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestAddContent } from './add';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CalendarModule, ChipsModule } from 'primeng/primeng';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';
import { TestService } from '../../../services/test.service';
import { CoptionService } from '../../../services/coption.service';
import { TestInstructionService } from '../../../services/testInstruction.service';

@NgModule({
    declarations:[TestAddContent],
    imports:[RouterModule,CommonModule, FormsModule, ChipsModule, ReactiveFormsModule,CalendarModule,CKEditor4Module],
    providers : [CoptionService,TestService,TestInstructionService],
    exports:[TestAddContent]
})
export class TestAddModule { }

