import { Component, OnInit, Input  } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TestService } from '../../../services/test.service';
import { NotifierService } from '../../notifier/notifier.service';
import { CoptionService } from '../../../services/coption.service';
import { AuthService } from '../../../services/auth.service';
@Component({
    selector:'ek-tm-test-list',
    templateUrl:'./list.html'
})

export class TestListContent implements OnInit{ 
    rows;
    columns; 
    searchForm:FormGroup;
    panelLoader="none";
    filter : any={};
    courses:any;
    streams:any;
    countBase:number=1;
    totalRecords;
    // isPaper : boolean = false;
    @Input() routes : string;
    @Input() title : string;
    @Input() isPaper : boolean = false;
    @Input() APP:any;//like TESTMENT
    @Input() ACTIVITY:any;//like TEST
    @Input() userPerms:any;
    // reqPerms:any=['LIST','VIEW','EDIT','ADD','VIEW_REPORT', 'VIEW_FEEDBACK','PRINT','DUPLICATE'];
    constructor(
        private testService:TestService,
        private fb : FormBuilder,
        private notifier: NotifierService,
        private coptionService : CoptionService,
        private authService:AuthService
        ){}
    ngOnInit(): void{
        // let sessionUser= this.authService.admin();
        // this.userPerms= this.authService.getUserPerms(sessionUser.activityPermission,this.APP,this.ACTIVITY,this.reqPerms)
        this.searchForm=this.fb.group({
			title:[''],
			course:[''],
            stream:[''],
            testType:[''],
			theme : [''],
			startDate:[''],
            endDate:[''],
            resultDate : [''],
            status:[''],
            isPaper : [false]
		});
        this.getTests();
        this.loadCourses();
        this.loadStreams();
        
    }
    getTests(){
        this.panelLoader="show";
        this.filter.isPaper = this.isPaper;
        this.testService.getTest(this.filter,true).subscribe(
            (data)=> {
                this.panelLoader="none";
                this.totalRecords=data.headers.get('totalRecords') || 0;
                this.rows = data.body;
            },
            (err)=>{this.panelLoader="none"; console.log(err)}
        );
    }
    searchData(){
        this.filter=this.searchForm.value;
        this.getTests();
    }
    deleteTest(test){
        this.panelLoader="show";
        let confirm=window.confirm("Are you sure to delete "+test.title+" ?");
        if(!confirm){
            this.panelLoader="none";
            return;
        }
        this.testService.deleteTest(test.id).subscribe(
            (res)=>{
                this.rows.splice(this.rows.indexOf(test), 1);
                this.panelLoader="none";
                this.notifier.alert('Success', 'Deleted Successfully', 'success', 1000 );
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader="none";
            }
        );
    }
    createDuplicate(value){
        let confirm=window.confirm("Are you sure you want to Create Duplicate "+value.title+" ?");
        if(!confirm){
            return;
        }
        let data = JSON.parse(JSON.stringify(value));
        delete data.id;
        delete data.createdAt;
        delete data.updatedAt;
        data.title += " 1";
        data.course= data.course.id;
        data.stream= data.stream.id;
        data.status = false;
        for( var i = 0 ; i < data.sections.length; i++){
            delete data.sections[i].qset;
            delete data.sections[i].questionLevel;
            data.sections[i].isQsetAssigned = false;
        }
        this.panelLoader="show";
        this.testService.addTest(data).subscribe(
			res=>{
                res.course=value.course;
                res.stream=value.stream;
                this.rows.unshift(res);
                this.panelLoader="none";
                this.notifier.alert('Success', 'Duplicate Created Successfully', 'success', 5000 );
			},
			err=>{
                this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		);
    }
    updateStatus(event,index){
        this.panelLoader="show";
        let status = !this.rows[index].status;
        if(status){var statusName= "Active";}
        else{var statusName= "InActive";}
        let confirm=window.confirm("Are you sure you want to "+statusName+" this Test ?");
        if(!confirm){
            if(event.target.checked){event.target.checked=false;}
            else{event.target.checked=true;}
            this.panelLoader="none";
            return;
        }
        this.testService.updateTest(this.rows[index].id,{status : status}).subscribe(
            (res)=>{
                this.rows[index].status=res.status;
                this.panelLoader = "none";
            },
            (err)=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader = "none";
            }
        )
    }
    loadCourses(){
		this.panelLoader="show";
		this.coptionService.getCoption({option:'COURSE'}).subscribe(
			res=>{
                this.panelLoader="none";
                this.courses=res;
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.panelLoader="show";
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.panelLoader="none";
				this.streams=res;
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
    }
    paginate(e) {
        if (!this.filter) { 
            this.filter = {};
        }
        this.countBase = e.rows * e.page + 1;
        this.filter.page = (e.page + 1);
        this.filter.limit = e.rows;
        this.getTests();
    }
  
}
