import { Component, OnInit, Input, ElementRef, ViewChild  } from '@angular/core';
import { ClientService } from '../../../services/client.service';
import { AuthService } from '../../../services/auth.service';
import { NotifierService } from '../../notifier/notifier.service';
import { CoptionService } from '../../../services/coption.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TestService } from '../../../services/test.service';
import { QsetService } from '../../../services/qset.service';
import { EdukitStoreService } from '../../../services/edukit-store.serivce';
import { Router } from '@angular/router';

@Component({
    selector:'ek-tm-test-sale',
    templateUrl:'./sale.html'
})
export class TestSaleContent implements OnInit{ 
    filterForm : FormGroup;
    @Input() routes : string;
    @Input() title : string;
    @Input() isPaper : Boolean = false;
    panelLoader :string="none";
    sellers =[];
    sellerCourses =[];
    sellerStreams =[];
    totalRecords :number=0;
    countBase : number = 1;
    tests=[];
    test :any={};
    activeClient : any;
    activeClientConfig : any;
    filter:any = {};
    selectedTests : any={};
    displayTestModal : boolean= false;
    user : any;
    displayOrderSummary : boolean= false;
    displayPaymentModal : boolean=false;
    order : any;
    displaySectionQsModal : boolean;
    secIndex : any;
    orderSummary : any;
    @ViewChild('mathload', { read: ElementRef }) mathload: ElementRef;

    @Input() APP:string;
    @Input() ACTIVITY:string;
    userPerms:any;
    reqPerms:any=['SALE'];
    constructor(
        private fb : FormBuilder,
        private clientService : ClientService,
        private authService : AuthService,
        private notifier : NotifierService,
        private coptionService : CoptionService,
        private testService : TestService,
        private qsetService : QsetService,
        private edukitStoreService :EdukitStoreService,
        private router : Router
    ){}
    ngOnInit(): void{
        let sessionUser= this.authService.admin();
        this.userPerms= this.authService.getUserPerms(sessionUser.activityPermission, this.APP, this.ACTIVITY,this.reqPerms);
        if(!this.userPerms.SALE){
            this.router.navigateByUrl(this.routes);
            return;
        }
        this.user=this.authService.user();
        this.activeClient = this.authService.admin().client;
        this.filterForm=this.fb.group({
            client : ['',Validators.required],
            course : ['',Validators.required],
            stream : ['',Validators.required],
        });
        this.loadClients();
    }
    loadClients(filter?:any) {
        this.panelLoader = "show";
        if(!filter){
            filter={};
        }
        filter.isESSeller=true;
        this.clientService.getClients(filter).subscribe(
            (res) => {
                this.sellers = res;
                for (let i = 0; i < res.length; i++) {
                    const element = res[i];
                    if(element.client.id == this.activeClient){
                        this.sellers.splice(i,1);
                        break;
                    }
                }
                this.panelLoader = "none";
            },
            (err) => {
                this.panelLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
            }
        );
    }
    selectClient(client){
        this.filterForm.patchValue({client : client,course:'',stream : ''});
        this.loadSellerCourses(client);
        this.loadSellerStreams(client);
    }
    loadClientConfig() {
        this.clientService.getClientConfig().subscribe(
            res=>{
                this.activeClientConfig=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    loadSellerCourses(client){
		this.panelLoader="show";
		this.coptionService.getCoption({option:'COURSE',client : client}).subscribe(
			res=>{
                this.sellerCourses=res;
				this.panelLoader="none";
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadSellerStreams(client){
		this.panelLoader="show";
		this.coptionService.getCoption({option:'STREAM',client : client}).subscribe(
			res=>{
                this.sellerStreams=res;
                this.panelLoader="none";
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
    }
    getTests(){
        this.filter.forSell=true;
        this.filter.status = true;
        this.filter.isPaper = true;
        let fData = Object.assign(this.filterForm.value, this.filter);
        this.panelLoader="show";
        this.testService.getTest(fData,true).subscribe(
            (data)=>{
                this.totalRecords=data.headers.get('totalRecords')|| 0;
                this.tests = data.body;
                this.panelLoader="none";
            },
            (err)=>{
                this.panelLoader="none";
                this.notifier.alert(err.code,err.message,"danger",2000);
            }
        );
    }
    showTestModal(id,index){
        this.test = this.tests[index];
        this.selectedTests[id] = id;
        this.displayTestModal=true;
    }
    showSectionQsModal(index){
        this.secIndex = index;
        let qset = this.test.sections[this.secIndex].qset;
        if(!qset && !this.test.sections[this.secIndex].isQsetAssigned) return this.notifier.alert('Warning','Test Section not linked ','danger',2000);
        if(!this.test.sections[this.secIndex].questions){
            this.getSectionQuestions(qset);
        }else{
            this.displaySectionQsModal = true;
            this.loadMathJax();
        }
    }
    getSectionQuestions(qset){
        this.panelLoader="show";
        let data={
            id : qset,
            client : this.test.client
        }
        this.qsetService.getSalesQsetQuestions(data).subscribe(
            (res) =>{ 
                this.test.sections[this.secIndex].questions=res;
                this.displaySectionQsModal=true;
                this.panelLoader="none";
                this.loadMathJax();
            },
            (err) =>{
                this.notifier.alert(err.code, err.message, 'danger', 5000 );
                this.panelLoader="none";  
            }
        );
    }
    paginate(e) {
        this.countBase = e.rows * e.page + 1;
        this.filter.page = (e.page + 1);
        this.filter.limit = e.rows;
        this.getTests();
    }
    loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub,this.mathload]);
        }, 10);
    }
    selectTest(event,id,index){
        if(event.target.checked){
            this.selectedTests[id]=id;
        }else{
            delete this.selectedTests[id];
        }
    }
    showOrderSummary(){
        this.order=null;
        let keys = Object.keys(this.selectedTests);
        if(!keys || keys.length<1){ return this.notifier.alert('Warning','Select Qset First !!','danger',2000);}
        this.testService.getTestsOrderSummary({tests : keys}).subscribe(
            res=>{
                if(res){
                    this.orderSummary = res;
                    this.displayOrderSummary=true;
                }
            },
            err=>{this.notifier.alert(err.code,err.message,'danger',1000);}
        );
    }
    placeOrder(){
        if(this.order){
            this.displayPaymentModal=true;
            return;
        }
        if(!this.orderSummary) return this.notifier.alert('Danger','OrderSummary not Exist','danger', 5000);
        let orderData:any = {
            buyer:this.activeClient,
            order:{
                orderTitle:"Paper Sale",
                total:(this.orderSummary.totalAmount + (this.orderSummary.totalAmount*0.18)),
                more:{
                    totalAmount:(this.orderSummary.totalAmount + (this.orderSummary.totalAmount*0.18)),
                    totalTests: this.orderSummary.totalTests,
                    sale : "Papers"
                },
                items : []
            },
        };  
        for (let index = 0; index < this.orderSummary.tests.length; index++) {
            const test = this.orderSummary.tests[index];
            orderData.order.items.push({
                title : test.title,
                quantity:1,
                amount : test.cost,
                discountAmt : test.discountAmt,
                discount : test.discount,
                seller : test.client,
                testId : test.id
            });
        } 
        this.panelLoader="show";
        this.edukitStoreService.placeOrder(orderData).subscribe(
            res=>{
                this.order=res;
                this.displayPaymentModal=true;
                this.loadClientConfig();
                this.panelLoader="none";
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code,err.message, 'danger',2000);
            }
        );
    }
    makePayment(){
        let confirm = window.confirm("Are u Sure to make payment. Amount will be debited from your EdukitStore Wallet. \n After Successfull payment, you will not be able to add more question from other sellers.");
        if(!confirm) return ;
        this.panelLoader="show";
        this.edukitStoreService.makePayment(this.order.id).subscribe(
            res=>{
                this.displayPaymentModal=false;
                this.displayTestModal=false;
                this.displayOrderSummary = false;
                this.notifier.alert("Success", "Paid Successfully", 'success', 2000);
                this.panelLoader="none";
                this.router.navigate([this.routes]);
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
}
