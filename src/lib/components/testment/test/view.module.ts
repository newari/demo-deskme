import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TestViewContent } from './view';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogModule } from 'primeng/primeng';
import { TestService } from '../../../services/test.service';
import { CoptionService } from '../../../services/coption.service';
import { QsetService } from '../../../services/qset.service';

@NgModule({
    declarations:[TestViewContent],
    imports:[RouterModule,DialogModule, CommonModule,FormsModule,ReactiveFormsModule],
    providers :[TestService,CoptionService,QsetService],
    exports:[TestViewContent]
})
export class TestViewModule {}
