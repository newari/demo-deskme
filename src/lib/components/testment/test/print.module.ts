import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {  TestPrintContent } from './print';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { TriStateCheckboxModule, FieldsetModule, DialogModule, SidebarModule } from 'primeng/primeng';
import { FileinputModule } from '../../filemanager/fileinput.module';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';
import { ClientService } from '../../../services/client.service';
import { FilemanagerService } from '../../filemanager/filemanager.service';
import { TestService } from '../../../services/test.service';


@NgModule({
    declarations:[TestPrintContent],
    imports: [CommonModule, RouterModule, 
        ReactiveFormsModule,
        TriStateCheckboxModule,
        FileinputModule,
        FieldsetModule,
        SafeHtmlPipeModule,
        DialogModule,
        SidebarModule,
        CKEditor4Module,
        FormsModule
    ],
    providers :[ClientService,FilemanagerService,TestService],
    exports : [TestPrintContent]
})
export class TestPrintModule {}
