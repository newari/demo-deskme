import { Component, OnInit, Input, HostListener, Directive } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TestService } from '../../../services/test.service';
import { ClientService } from '../../../services/client.service';
import { NotifierService } from '../../notifier/notifier.service';
import { AuthService } from '../../../services/auth.service';
declare var $ :any;

@Component({
    selector:'ek-tm-test-print',
    templateUrl:'./print.html',
})
export class TestPrintContent implements OnInit{ 
    row;
    countBase : number=2;
    testPrintForm : FormGroup;
    contentForm : FormGroup;
    activeFilter;
    totalRecords;
    changeLogo : boolean=false;
    panelLoader="none";
    setData="content-box";
    testId;
    counter : number=0;
    clientId : any;
    clientConfig;
    testData:any=[];
    questionLevel = [null,"Easy","Average","Medium","Difficult","Very Difficult","Tough"];
    charSeq = ["A","B","C","D","E","F","G","H"];
    printStatus : string="FORM";
    showTestCustomize : boolean=false;
    @Input() routes : string;
    @Input() redirectRoutes: string;
    @Input() title : string;
    @Input() APP : string='TESTMENT';
    @Input() ACTIVITY:string='TEST';
    content:any;
    customChanges : any={};
    formStatus="Normal";
    needVariantQs:boolean;
    defaultLang:string;
    @Input() userPerms:any[]=[];
    constructor(
        private testService:TestService,
        private fb : FormBuilder,
        private clientService : ClientService,
        private activatedRoute : ActivatedRoute,
        private notifier:NotifierService,
        private router: Router,
        private authService : AuthService
    ){}
     

    ngOnInit(): void{
        this.testPrintForm = this.fb.group({
            title:['', Validators.required],
            smpq : [],                              // Show Marks Per Question.
            numericalOption : [],
            answerKey : [],                         // Answer Options like (1),(2) instead of (a),(b).
            resSectionNum : [],                     // Reset Question Numbering For Each Section.
            showSecName : [],                       // Show Section Name
            sswnc : [],                             // Section Start with next column
            logo : [''],
            showLogo:[],
            pageSize : ['1000'],
            instyInfo : [''],
            showInstyInfo : [],
            roughPage : [],
            frontPage:['',Validators.required],
            footerImg : [],
        });
        this.contentForm= this.fb.group({
            id : [''],
            content:['']
        });
        this.activatedRoute.params.subscribe(params=>{
            this.testId= params['id'];
        });
       
        this.loadTestPrintQs();
    }
    loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub]);
        }, 10);
    }
    loadVariantQs(lang){

        if(lang!==this.row.defaultLang){
            this.loadTestPrintQs({prinVariantQs:true})
        }else{
            this.loadTestPrintQs();
        }
        
    }
    loadTestPrintQs(filter?:any):void{
        this.panelLoader="show";
        if(!filter){
            filter={}
        }
        this.testService.getTestPrintQs(this.testId,filter).subscribe(
            (data) =>{ 
                this.row=data;
                if(!this.defaultLang){
                    this.defaultLang=data.defaultLang;
                }
                this.getClient(this.row.client);
                this.testPrintForm.patchValue({title : this.row.title});
                this.panelLoader="none";
                this.loadMathJax();
            },
            (err) =>{
                this.panelLoader="none";
                this.notifier.alert(err.code,err.message,'danger',500)
            }
        );
    }
    getClient(clientId){
        this.clientService.getClientConfig({client:clientId}).subscribe(
            res=>{this.clientConfig=res;
                this.testPrintForm.patchValue({logo : this.clientConfig.logo})
            },
            err=>{this.notifier.alert(err.code,err.message,'danger',500)}
        );
    }
    convertData(data?:any){
        this.formStatus="Processing";
        this.testData=[];
        let printForm = this.testPrintForm.value;
        let content :any;
        if(data){
            content=data;
        }else{
            $(".view-only").hide();
            $(".content-box").hide();
            content = $('.con-piece');
        }
        let column=[];
        let numOption = ['1','2','3','4','5'];
        let defaultOption = ['a','b','c','d','e'];
        let page = [];
        let qOptions;
        let qSeq=0;
        let optSeq=0;
        let optHeights = [];
        let optHeight=0;
        let optionBlock = false;
        let pageSize = printForm.pageSize;
        let pageHeight = pageSize;
        if(printForm.numericalOption){
            defaultOption = [];
            defaultOption = numOption;
        }
        let currentType='';
        let setSeq = false;
        let optionObj:any={};
        for(var i=0;i<content.length;i++){
            let obj : any={
                content: $(content[i]).html(),
                type : $(content[i]).attr('el-type'),
                qType : $(content[i]).attr('q-type'),
                width: $(content[i]).width(),
                height : $(content[i]).height()
            };
            if((currentType != obj.type)|| (currentType == obj.type && currentType == 'q')){
                currentType = obj.type;
                setSeq = true;
            }
            switch (obj.type){
                case 'q':
                    // let qq = $(content[i]).children();
                    if(setSeq){
                        qSeq++;
                        obj.content = '<div class="ques">' + obj.content.replace('<div class="ques">','<span class="q_seq">'+qSeq+'.</span>');
                        setSeq=false;
                    }
                    if(obj.height <= pageHeight){
                        pageHeight = pageHeight - obj.height;
                        column.push(obj);
                    }
                    else{
                        page.push(column);
                        column=[];
                        pageHeight = pageSize - obj.height;
                        column.push(obj);
                        if(page.length == 2){
                            this.testData.push(page);
                            page=[];
                        }
                    }
                break;
                
                case 'opt_start':
                    optHeights = [];
                    optHeight=0;
                    optSeq=0;
                    qOptions = '';
                    optionBlock = false;
                break;

                case 'opt':
                    optHeights.push(obj.height);
                    optHeight += obj.height;
                    if(obj.qType == 'MTQ'){
                        qOptions = qOptions + '<div class="option">'+obj.content+'</div>';
                    }
                    else{
                        qOptions = qOptions + '<div class="option"><span class="opt_seq">('+defaultOption[optSeq]+')</span>'+obj.content+'</div>';
                    }
                    optSeq++;
                    if(obj.width > 159){
                        optionBlock = true;
                    }
                break;
                case 'opt_break':
                    optionObj={};
                    if(optionBlock){
                        optionObj.height = optHeight+10;
                        optionObj.content= '<div class="option_block">'+qOptions+'</div>';
                    }
                    else{
                        let o1h=0;
                        let o2h=0;
                        let o3h=0;
                        if(optHeights[0]&&optHeights[1]){
                            o1h=(optHeights[0]>optHeights[1]?optHeights[0]:optHeights[1]);
                        }
                        else if(optHeights[0]){
                            o1h=optHeights[0];
                        }

                        if(optHeights[2]&&optHeights[3]){
                            o2h=(optHeights[2]>optHeights[3]?optHeights[2]:optHeights[3]);
                        }
                        else if(optHeights[2]){
                            o2h = optHeights[2];
                        }

                        if(optHeights[4]){
                            o3h=optHeights[4];
                        }
                        optionObj.height = o1h+o2h+o3h+10;
                        optionObj.content= '<div class="option_inline">'+qOptions+'</div>';
                    }
                    if(optionObj.height <= pageHeight){
                        pageHeight = pageHeight - optionObj.height;
                        column.push(optionObj);
                    }
                    else{
                        page.push(column);
                        column=[];
                        pageHeight = pageSize - optionObj.height;
                        column.push(optionObj);
                        if(page.length == 2){
                            this.testData.push(page);
                            page = [];
                        }
                    }
                    optHeights = [];
                    optHeight=0;
                    qOptions = '';
                break;

                case 'opt_end':
                    // +10 in height is for padding  in option_block, option_inline
                    optionObj={};
                    if(optionBlock){
                        optionObj.height = optHeight + 10;
                        optionObj.content= '<div class="option_block">'+qOptions+'</div>';
                    }
                    else{
                        let o1h=0;
                        let o2h=0;
                        let o3h=0;
                        if(optHeights[0]&&optHeights[1]){
                            o1h=(optHeights[0]>optHeights[1]?optHeights[0]:optHeights[1]);
                        }
                        else if(optHeights[0]){
                            o1h=optHeights[0];
                        }
                        if(optHeights[2]&&optHeights[3]){
                            o2h=(optHeights[2]>optHeights[3]?optHeights[2]:optHeights[3]);
                        }
                        else if(optHeights[2]){
                            o2h = optHeights[2];
                        }
                        if(optHeights[4]){
                            o3h=optHeights[4];
                        }
                        optionObj.height = o1h+o2h+o3h + 10;
                        optionObj.content= '<div class="option_inline">'+qOptions+'</div>';
                    }
                    if(optionObj.height <= pageHeight){
                        pageHeight = pageHeight - optionObj.height;
                        column.push(optionObj);
                    }
                    else{

                        page.push(column);
                        column=[];
                        pageHeight = pageSize - optionObj.height;
                        column.push(optionObj);
                        if(page.length == 2){
                            this.testData.push(page);
                            page = [];
                        }
                    }
                    
                break;
                case 'sec_name':
                    if(obj.height <= pageHeight){
                        pageHeight = pageHeight-obj.height;
                        column.push(obj);
                    }
                    else{
                        page.push(column);
                        column=[];
                        pageHeight = pageSize - obj.height;
                        column.push(obj);
                        if(page.length == 2){
                            this.testData.push(page);
                            page=[];
                        }
                    }
                break;
                case 'sec_end':
                    if(printForm.resSectionNum){
                        qSeq = 0;
                    }
                    if(printForm.sswnc){
                        pageHeight = 0;
                    }
                break;
                default:
                break;
            }
            if(content.length-1 == i){
                page.push(column);
                this.testData.push(page);
            }
        }
        this.printStatus="EXAM";
        this.formStatus="Normal";
        // this.loadMathJax();
    }

    displayTestCustomization(){
        this.showTestCustomize=true;
        $(".boxx .view-only").show();
        if(this.customChanges){
            let self=this;
            setTimeout(function(){
                for(var key in self.customChanges){
                    let data=self.customChanges[key];
                    $('#'+key).html(data);
                }
            },100);
        }
    }
    testCustomization(){
        $(".view-only").hide();
        $(".content-box").hide();
        let content = $('.con-piece');
        this.convertData(content);
        this.showTestCustomize=false;
    }
    clickContent(idName){
        let qData= $('#'+idName).html();
        this.contentForm.patchValue({
            content : qData,
            id : idName
        });
    }
    updateContent(){
        let data = this.contentForm.value;
        if(data.content.search('[[break]]') > -1){
            let qId= data.id.search('q');
            let optId= data.id.search('opt');
            if(qId > -1){
                data = this.updateQs(data);
            }else if(optId > -1){
                data = this.updateOption(data);
            }
        }
        $('#'+data.id).html(data.content);
        this.customChanges[data.id]=data.content;
    }
    updateQs(data){
        let contentArr = data.content.split('[[break]]');
        let contentData ='';
        let addData=`
                    </div>
                </div>
            </div>
            <div class="con-piece content-box" el-type="q">
                <div class="ques">
                    <div class="q_content">
        `;
        for (let i = 0; i < contentArr.length; i++) {
            let element = contentArr[i];
            if((contentArr.length-1) == i){
                contentData = contentData + element;
            }else{
                contentData = contentData + element + addData;
            }
            contentData = contentData.replace(/<div class="con-piece content-box">/g,'<div class="con-piece content-box" el-type="q">');
        }
        return ({id : data.id,content : contentData});
        // $('#'+data.id).html(contentData);
        // this.customChanges.push(data);
        // this.contentForm.patchValue({content:contentData});
    }
    updateOption(data){
        let contentData ='';
        let contentArr = data.content.split('[[break]]');
        let addData=`
                    </div>
                </div>
                <div class="con-piece" el-type="opt_break">
        `;
        for (let i = 0; i < contentArr.length; i++) {
            let element = contentArr[i];
            let removeDiv = false;
            if((contentArr.length-1) == i){
                contentData = contentData + element;
            }else{
                contentData = contentData + element + addData;
                removeDiv=true;
            }
            if(removeDiv && contentArr[i+1]){
                contentArr[i+1] = contentArr[i+1].replace('</div>',''); 
                removeDiv=false;
            }
            contentData = contentData.replace(/<div class="con-piece content-box">/g,'<div class="con-piece content-box" el-type="opt">'); 
        }

        return ({id : data.id,content : contentData});
        // $('#'+data.id).html(contentData);
        // this.customChanges.push(data);
    }
    displaySolution(){
        this.printStatus = 'SOLUTION';
        this.loadMathJax();
    }
}