import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TestSaleContent } from './sale';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogModule, SidebarModule, PaginatorModule } from 'primeng/primeng';
import { TestService } from '../../../services/test.service';
import { CoptionService } from '../../../services/coption.service';
import { QsetService } from '../../../services/qset.service';
import { SafeHtmlPipeModule } from '../../../filters/safehtml.pipe';
import { ClientService } from '../../../services/client.service';
import { EdukitStoreService } from '../../../services/edukit-store.serivce';

@NgModule({ 
    declarations:[TestSaleContent],
    imports:[
        RouterModule,DialogModule,SidebarModule, CommonModule,FormsModule,
        ReactiveFormsModule,PaginatorModule,SafeHtmlPipeModule],
    providers :[TestService,CoptionService,ClientService,QsetService,EdukitStoreService],
    exports:[TestSaleContent]
})
export class TestSaleModule {}
