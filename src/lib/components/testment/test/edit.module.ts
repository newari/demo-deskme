
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TestEditContent } from './edit';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CalendarModule, DialogModule, ChipsModule } from 'primeng/primeng';
import { TestService } from '../../../services/test.service';
import { CoptionService } from '../../../services/coption.service';
import { TestInstructionService } from '../../../services/testInstruction.service';
import { CKEditor4Module } from '../../ckeditor4/ckeditor4.module';

@NgModule({
    declarations:[TestEditContent],
    imports:[RouterModule, FormsModule, ReactiveFormsModule, ChipsModule, CommonModule,CalendarModule,DialogModule,CKEditor4Module],
    providers : [TestService,CoptionService,TestInstructionService],
    exports:[TestEditContent] 
})
export class TestEditModule {
    
 }
