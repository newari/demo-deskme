import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {  TestListContent } from './list';
import { CommonModule } from '@angular/common';
import { MenuModule, InputSwitchModule, PaginatorModule, CalendarModule} from 'primeng/primeng';
import { ReactiveFormsModule } from '@angular/forms';
import { CoptionService } from '../../../services/coption.service';
import { EkDropdownModule } from '../../dropdown/dropdown.module';
import { TestService } from '../../../services/test.service';

@NgModule({
    declarations:[TestListContent],
    imports:[CommonModule, RouterModule,MenuModule,EkDropdownModule,InputSwitchModule,ReactiveFormsModule,PaginatorModule,CalendarModule],
    providers:[CoptionService,TestService],
    exports :[TestListContent]
})
export class TestListModule {}
