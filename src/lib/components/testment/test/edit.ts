import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Test } from '../../../models/test.model';
import { Coption } from '../../../models/coption.model';
import { TestService } from '../../../services/test.service';
import { CoptionService } from '../../../services/coption.service';
import { NotifierService } from '../../notifier/notifier.service';
import { TestInstructionService } from '../../../services/testInstruction.service';
import { ClientService } from '../../../services/client.service';
import { AuthService } from '../../../services/auth.service';

@Component({
    selector:'ek-tm-test-edit',
    templateUrl:'./edit.html'
})
export class TestEditContent implements OnInit{
    testForm:FormGroup;
	testData:Test;
	testId;	
	sections;
	streams:Coption[];
	courses:Coption[];
	frmLoader="none";
	formStatus="Normal";
	panelLoader="none";
	instructions;
	validMessage :any=" ' '  \" \" Special character are not Allowed";
	showBonusMarksModal : boolean=false; 
	sectionIndex : number;
	sectionQs : any=[];
	bonusMarks : any={};
	user : any={};
	clientConfig : any={};
	@Input() routes : string;
	@Input() title : string;
	@Input() isPaper : boolean= false;
	@Input() APP:any='TESTMENT'
	@Input() ACTIVITY:any='TEST';
	@Input() userPerms:any;
	constructor(
		private fb:FormBuilder,
		private activatedRoute: ActivatedRoute,
		private testService:TestService,
		private coptionService:CoptionService,
		private notifier: NotifierService,
		private testInstructionService : TestInstructionService,
		private clientService : ClientService,
		private authService : AuthService,
		private router : Router
	){}
	ngOnInit(): void {
		 
		this.user = this.authService.user();
		if(this.user){
			this.clientService.getClientConfig({client : this.user.client}).subscribe(
				res=>{
				this.clientConfig = res;
				},err=>{console.log(err);}
			);
		}
		this.testForm=this.fb.group({
			title:['', Validators.required],
			alias:['', Validators.required],
			testType:['', Validators.required],
			theme:['', Validators.required],
			description:[''],
			duration:['', Validators.required],
			course:['', Validators.required],
			stream:['', Validators.required],
			maxScore:['', Validators.required],
			defaultLang:['', Validators.required],
			instruction : [''],
			totalQs:['', Validators.required],
			startDate:[''],
			resultDate : [''],
			endDate:[''],	
			preDiscussionVideos : this.fb.group({
				source : [''],
				ids :[]
			}),
			solVideos: this.fb.group({
				source : [''],
				ids :[]
			}),
			status:['',Validators.required],
			isCalculator:[''],
			oneTimeSubmission : [''],
			sections: this.fb.array([]),
			isPaper : [this.isPaper],
			isPurchased  : [false], 
			inTM : [false],
			forSell : [false],
			cost : [0,Validators.required],
			mrp : [0,Validators.required],
			discount : [0,Validators.required],
			maqPM : [false],
			mtqPM : [false],
			isFree : [false]
		});
		this.loadCourses();
		this.loadStreams();
		this.getInstructions();
		this.activatedRoute.params.subscribe((params: Params) => {
	        this.testId = params['id'];
			this.getTest(this.testId);
		});
	}
	setSections(sectionCount?:number) {
		this.sections = this.testForm.get('sections') as FormArray;
		for(var i=0;i<sectionCount; i++){
			this.sections.push(this.setControls());
		}
	}
	deleteSection(index){
		this.sections = this.testForm.get('sections') as FormArray;
		 this.sections.removeAt(index,1);
	}
	setControls(): FormGroup {
		return this.fb.group({
			title:['', Validators.required],
			totalMarks:[''],
			totalQs:[''],
			maxTime:[''],
			attemptLimit:[''],
			questionLevel : [''],
			bonusMarks : [{}],
			qset : "",
			isQsetAssigned:false
		});
	}
	sourceChange(){
		window.alert('Please Make sure your video ids is from selected Source');
	}
	getTest(testId){
		this.panelLoader="show";
		this.testService.getOneTest(testId).subscribe(
			(res)=>{
				this.testData= JSON.parse(JSON.stringify(res));
				if(res.course&&res.course.id){
					res.course=res.course.id;
					this.testData.course=res.course;
				}
				if(res.stream&&res.stream.id){
					res.stream=res.stream.id;
					this.testData.stream=res.stream;
				}
				if(res.instruction&&res.instruction.id){
					res.instruction=res.instruction.id;
					this.testData.instruction=res.instruction;
				}
				if(res.sections){
					this.setSections(this.testData.sections.length);
					this.testData.sections=res.sections;
					// for (let i = 0; i < this.testData.sections.length; i++) {
					// 	if(this.testData.sections[i].bonusMarks>0){
					// 		this.showBonusMarksModal=true;
					// 	}
					// }
				}
				delete res.startDate;
				delete res.endDate;	
				delete res.resultDate;
				this.testForm.patchValue(res);
				this.panelLoader="none";
			},
			(err)=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
			}
		)
	}
	getInstructions(){
        this.testInstructionService.getInstruction().subscribe(
            (data)=> {this.instructions = data;},
            (err)=> this.notifier.alert(err.code, err.message, 'danger', 5000 )
        );
    }
	loadCourses(){
		this.frmLoader="show";
		this.coptionService.getCoption({option:'COURSE'}).subscribe(
			res=>{
				this.frmLoader="none";
				this.courses=res;
			},
			err=>{
				this.frmLoader="none";
				this.notifier.alert(err.code, err.message, 'danger');
			}
		);
	}
	loadStreams(){
		this.coptionService.getCoption({option:'STREAM'}).subscribe(
			res=>{
				this.streams=res;
			},
			err=>{this.notifier.alert(err.code, err.message, 'danger',1000);}
		);
	}
	displayBonusMarks(index,id){
		this.sectionIndex=index;
		if(!this.sectionQs[this.sectionIndex]){
			this.getBonusMarkQs();
		}
		if(this.testForm.value.sections[this.sectionIndex].bonusMarks == 0){
			this.testForm.value.sections[this.sectionIndex].bonusMarks={};
		}
		this.showBonusMarksModal=true;
	}
	getBonusMarkQs(){
		this.testService.getBonusMarkQs(this.testData.sections[this.sectionIndex].qset).subscribe(
			res=>{
				this.sectionQs[this.sectionIndex] = res;
			},
			err=>{this.notifier.alert(err.code, err.message, 'danger',2000);
		});
	}
	inputBonus(qId,data){

		if(data){
			this.testForm.value.sections[this.sectionIndex].bonusMarks[qId]=parseFloat(data);
		}else if(this.testForm.value.sections[this.sectionIndex].bonusMarks[qId]){
			delete this.testForm.value.sections[this.sectionIndex].bonusMarks[qId];
		}
	}
	updateTest(): void {
		let formData=this.testForm.value;
		if(!formData.startDate){
			formData.startDate=new Date(this.testData.startDate);
		}
		if(!formData.endDate){
			formData.endDate=new Date(this.testData.endDate);	
		}
		if(!formData.resultDate){
			formData.resultDate=new Date(this.testData.resultDate);	
		}
		formData.startDate=new Date(formData.startDate.setSeconds(0));
		formData.endDate=new Date(formData.endDate.setSeconds(0));
		formData.resultDate=new Date(formData.resultDate.setSeconds(0));
		var totalSecQuestion = 0;
		var totalSecMarks = 0;
		var totalSecDuration = 0;
		for(var i = 0; i< formData.sections.length; i++){
			totalSecQuestion += formData.sections[i].totalQs; 
			totalSecMarks += formData.sections[i].totalMarks;
			totalSecDuration += formData.sections[i].maxTime;
		}
		if( formData.totalQs != totalSecQuestion){
			return this.notifier.alert('Danger ', 'Total Question is not equal to Section Total Question', 'danger', 5000 );
		}
		if( formData.maxScore != totalSecMarks){
			return this.notifier.alert('Danger ', 'Total Marks is not equal to Section Total Marks', 'danger', 5000 );
		}
		if( formData.duration != totalSecDuration){
			return this.notifier.alert('Danger ', 'Total Duration is not equal to Section Total Max Time', 'danger', 5000 );
		}
		if(formData.testType == "PRACTICE"){
			formData.resultDate="";
		}
		this.formStatus="Processing";
		this.testService.updateTest(this.testId, formData).subscribe(
			(res)=>{
				this.formStatus="Normal";
				this.notifier.alert('Success', 'Saved Successfully', 'success', 5000 );
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000 );
				this.formStatus="Normal";
			}
		);
	}
	testForSell(el){ 								// (typeof (el) == "boolean" && el == true) 
		if (el.target.checked){
			this.testForm.patchValue({forSell : true});
		} else {
			this.testForm.patchValue({forSell : false});
		}
	}
}
