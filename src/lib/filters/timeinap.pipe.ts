import { Pipe, PipeTransform, NgModule } from '@angular/core';

@Pipe({name: 'timeinap'})
export class TimeinapPipe implements PipeTransform {
  transform(value: string): any {
    // string must be in fromat of HH:MM or HH:MM:SS
    if (!value) return value;
    var arr=value.split(":");
    var arrv1=parseInt(arr[0]);
    var h=(arrv1==12?arrv1:arrv1%12);
    var ap=((arrv1/12)>=1?'PM':'AM');
    return h+":"+arr[1]+" "+ap;
  }
}

@NgModule({
  declarations:[TimeinapPipe],
  exports:[TimeinapPipe]
})
export class TimeinapPipeModule { }