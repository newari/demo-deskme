import { Pipe, PipeTransform, NgModule } from '@angular/core';
import * as moment from 'moment';

@Pipe({name: 'moment'})
export class MomentPipe implements PipeTransform {
  transform(value: string, format): any {
    // string must be in fromat of HH:MM or HH:MM:SS
    if (!value) return value;
    
    return moment(value).utc().format(format);
  }
}

@NgModule({
  declarations:[MomentPipe],
  exports:[MomentPipe]
})
export class MomentPipeModule {}