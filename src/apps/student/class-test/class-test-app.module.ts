import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";
import { ClassTestAppComponent } from './app.component';

@NgModule({
    declarations: [ClassTestAppComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[]
})
export class StudentClassTestAppModule { }
