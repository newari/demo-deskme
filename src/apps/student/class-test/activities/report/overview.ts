import { Component, HostListener } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { FormGroup} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';
import { ClasstestresultService } from '../../../../../lib/services/classtestresult.service';
import { QsetService } from '../../../../../lib/services/qset.service';
import { PassageService } from '../../../../../lib/services/passage.service';
import { BookmarkQuestionService } from '../../../../../lib/services/bookmarkquestion.service';

@Component({
  templateUrl: './overview.html'
})
export class ClassTestOverViewContent{
	userReport;
	sessionStd:any;
	feedBackForm:FormGroup;
	classTestResultId:any;
	userId : any;
	data:any;
	qBookmark ={};
	paginateFilter : any;
	totalRecords: number;
	testParticipants : any;
	toppers;
	temp : any;
	rankData;
	diffLevelReport : any;
	qLevelLabel = ["Easy","Average","Medium","Difficult","Very Difficult"];
	// countBase;
	sections:any;
	qwise:any;
	solutionSections : any;
	activeSection=0;
	subjects: any=[];
	topics:any[];
	topicWiseData:any;
	averageScore : number=0;
	constructor(
		private authService: AuthService,
		private notifier : NotifierService,
		private activatedRoute: ActivatedRoute,
		private classTestResultService : ClasstestresultService,
		private qBookmarkService : BookmarkQuestionService,
		private passageService : PassageService,
		private qsetService : QsetService
	){}

	ngOnInit(){
		this.sessionStd=this.authService.student();
		this.activatedRoute.queryParams.subscribe(params=>{
			this.classTestResultId = params['classTestResultId'];
		})
		this.userId=this.sessionStd.id;
		this.loadTest();
	}
	@HostListener('scroll', ['$event']) onScroll(event) {
		console.log("scrolling",event);	
	}
	loadTest(){
		this.classTestResultService.getOneClasstestresult(this.classTestResultId).subscribe(
		  res=>{							
				this.userReport=res;
				this.sections= JSON.parse(JSON.stringify(this.userReport.test.sections));
				this.qwise=this.getQwiseResultData(res.qwise);
				this.solutionSections=JSON.parse(JSON.stringify(this.userReport.test.sections));
				this.setAverageScore(this.userReport.sectionScore);
				// this.userReport.totalTimeTaken=this.getSecondsAsDigitalClock(this.userReport.totalTimeTaken);
		  },
		  err=>{
			  this.notifier.alert(err.code, err.message, "danger", 10000);
		  }
		)
	}
	setAverageScore(sectionScore){
		let sectionSum =0;
		if(this.sections.length >0){
			for (let i = 0; i < sectionScore.length; i++) {
				sectionSum = sectionSum + parseFloat(sectionScore[i].score);
			}
			this.averageScore = (sectionSum/sectionScore.length); 
		}
	}
	getQwiseResultData(qwise: any): any {
		let data = qwise.split("|");
		let subjects = [];
		this.subjects=[];
		let rows:any=[];
		let currentSubIndex=-1;
		for (let index = 0; index < data.length; index++) {
			let newArr = data[index].split(";");
			if (subjects.indexOf(newArr[2])==-1) {
				currentSubIndex++;
				subjects.push(newArr[2]);
				this.subjects.push({title:newArr[2], topics:[]});
			}
			if (this.subjects[currentSubIndex].topics.indexOf(newArr[3])==-1) {
				this.subjects[currentSubIndex].topics.push(newArr[3]);
			}
		
			let row : any= {
				correctAns: newArr[0].toLowerCase(),
				stdResponse: newArr[1].toLowerCase(),
				subject: newArr[2],
				topic: newArr[3],
				timeTaken : ''
			}
			if(newArr[4] && newArr[4] !=""){
				row.timeTaken = this.getSecondsAsDigitalClock(newArr[4]);
			}
			rows.push(row);
		}
		this.getTopicWiseData(this.subjects, rows);
		return rows;
	}
	getTopicWiseData(subjects: any[], rows: any): any {
		this.topicWiseData=[];
		for (let subIndex = 0; subIndex < this.subjects.length; subIndex++) {
			for (let topicIndex = 0; topicIndex < subjects[subIndex].topics.length; topicIndex++) {
				let row={
					correctQs:0,
					inCorrectQs:0,
					unAttempted:0,
					topic:subjects[subIndex].topics[topicIndex],
					subject:'',
					totalQs:0
				}; 
				for (let qIndex = 0; qIndex < rows.length; qIndex++) {
					
					if (subjects[subIndex].title== rows[qIndex].subject&&subjects[subIndex].topics[topicIndex] == rows[qIndex].topic && rows[qIndex].stdResponse!='') {
						row.totalQs+=1;
						row.subject = rows[qIndex].subject;
						let correctAns = rows[qIndex].correctAns.toLowerCase();
						let stdResponse = rows[qIndex].stdResponse.toLowerCase();
						if (stdResponse != 'null' || stdResponse!='') {
							if (correctAns == stdResponse) {
								row.correctQs += 1;
							}
							if (stdResponse != correctAns) {
								let stdResArr = [];
								if (stdResponse.search(",") == -1) {
									stdResArr.push(stdResponse);
								} else {
									stdResArr = stdResponse.split(",");
								}
								let correctAnsArr = [];
								if (correctAns.search(",") == -1) {
									correctAnsArr.push(correctAns);
								} else {
									correctAnsArr = correctAns.split(",");
								}
								if (stdResArr.length <= correctAnsArr.length) {
									let correctOptCount = 0;
									for (let index = 0; index < stdResArr.length; index++) {
										if (stdResArr[index] != 'null' || stdResArr[index] != '') {
											let isExist = correctAnsArr.indexOf(stdResArr[index]);
											if (isExist == -1) {
												break;
											} else {
												correctOptCount += 1;
											}
										}
									}
									if (correctOptCount == stdResArr.length) {
										row.correctQs += 1;
									} else {
										row.inCorrectQs += 1;
									}
								}
							}
						}
					} 
					if (subjects[subIndex].title== rows[qIndex].subject&&subjects[subIndex].topics[topicIndex] == rows[qIndex].topic &&rows[qIndex].stdResponse == '') {
						row.unAttempted+=1;
						row.totalQs += 1;
						row.subject = rows[qIndex].subject;
					}
					
				}
				this.topicWiseData.push(row);
			}
		}
	}
	getSecondsAsDigitalClock(inputSeconds: number) {
		const secNum = parseInt(inputSeconds.toString(), 10); // don't forget the second param
		const hours = Math.floor(secNum / 3600);
		const minutes = Math.floor((secNum - (hours * 3600)) / 60);
		const seconds = secNum - (hours * 3600) - (minutes * 60);
		let hoursString = '';
		let minutesString = '';
		let secondsString = '';
		let time = '';
		if(hours !=0){
			hoursString = (hours < 10) ? '0' + hours : hours.toString();
			time = hoursString + ' h ';
		}
		if(minutes !=0){
			minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
			time += minutesString + ' m ';
		}
		secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
		time += secondsString+' sec';
		return time;
	  }
	 
	paginate(e) {
        if (!this.paginateFilter) {
            this.paginateFilter = {};
        }
        // this.countBase = e.rows * e.page + 1;
        this.paginateFilter.page = (e.page + 1);
        this.paginateFilter.limit = e.rows;
        // // this.getTestParticipants();
	}
}
