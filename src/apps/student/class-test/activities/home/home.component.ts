import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { ClasstestresultService } from '../../../../../lib/services/classtestresult.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import * as moment from 'moment';
import { FilemanagerService } from '../../../../../lib/components/filemanager/filemanager.service';
import { FeedbackService } from '../../../../../lib/services/feedback.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { VideoService } from '../../../../../lib/services/video.service';
import { SettingsService } from '../../../../../lib/services/settings.service';

@Component({
  templateUrl: './home.component.html'
})
export class StudentClassTestHomeComponent {
	ceTestSeries:any[];
	videoSolutions:any;
    activeTest:any;
    activeRowIndex:number;
    answerUploadModal:boolean;
    panelLoader:string;
	sessionStd:any;
	feedbackList:any;
	viewPdfModal:boolean;
	viewTopSolutionModal:boolean;
	instructionModal:boolean;
	confirmToCheck:boolean;
	topSolutions:any;
	showFeedBackOption:boolean;
	feedBackForm:FormGroup;
	showVideo:boolean;
	videoLimit:number;
	videoTimerId:any;
	videoData:any;
	viewLimitMsg:string;
	showTestSolnAlways:boolean=false;
	activeTestSeriesIndex;
	activeTestSeries : number = 0;
    constructor(
		private ctService: ClasstestresultService,
      	private authService: AuthService,
        private el: ElementRef,
        private notifier: NotifierService,
		private fmService:FilemanagerService,
		private feedbackService:FeedbackService,
		private fb:FormBuilder,
		private videoService:VideoService,
		private sanitizer:DomSanitizer,
		private settingService: SettingsService
    ){
        
    }

    ngOnInit(){
		this.sessionStd=this.authService.student();
		this.settingService.getSettingByKey('CONV_EXAM_SOLN_ALWAYS_DWNLD').subscribe(
			res=>{
				this.showTestSolnAlways=res.value;
				this.loadTests(this.sessionStd.id);
			},
			err=>{
				this.showTestSolnAlways=false;
				this.loadTests(this.sessionStd.id);
			}
		)
		
		this.feedBackForm=	this.fb.group({
			question:['null', Validators.required],
			response:['null', Validators.required]
		})
    }
    loadTests(userId){
      	this.ctService.getUserTests(userId).subscribe(
	        res=>{				
				for (let i = 0; i < res.length; i++) {
					let testCount =res[i].tests.length;
					for (let testIndex = 0; testIndex < testCount; testIndex++) {
						
						res[i].tests[testIndex].enablePaperDownload = moment().isSameOrAfter(res[i].tests[testIndex].test.startDate);
						res[i].tests[testIndex].enablePaperUpload = moment().isBetween(res[i].tests[testIndex].test.startDate, moment(res[i].tests[testIndex].test.endDate).endOf("day"));;
						res[i].tests[testIndex].enableSolnDownload =  moment().isSameOrAfter(res[i].tests[testIndex].test.startDate)&&this.showTestSolnAlways;
					}									
				}				
				  this.ceTestSeries=res;				  
	        },
	        err=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
	        }
      	)
    }
    showAnswerUploadModal(testSeriesIndex,testIndex){
		this.activeTest=this.ceTestSeries[testSeriesIndex].tests[testIndex];
		this.activeRowIndex=testIndex;
		this.activeTestSeriesIndex=testSeriesIndex;
      	this.answerUploadModal=true;
	}
	showViewPdfModal(testSeriesIndex,testIndex){
		this.activeTest=this.ceTestSeries[testSeriesIndex].tests[testIndex];
		this.activeRowIndex=testIndex;
		this.activeTestSeriesIndex=testSeriesIndex;
		this.viewPdfModal=true;
	  }
	showInstructionModal(){
		this.instructionModal=true;
	}
    // uploadFile(cb) {
    // 	//locate the file element meant for the file upload.
    //     let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#fmFile');
    // 	//get the total amount of files attached to the file input.
    //     let fileCount: number = inputEl.files.length;
    // 	//create a new fromdata instance
    //     // this.panelLoader="show";
    // 	//check if the filecount is greater than zero, to be sure a file was selected.
    //     if (fileCount > 0) { // a file was selected
    //         let formData = new FormData();
    //         //append the key name 'photo' with the first file in the element
	// 		      formData.append('fmFile', inputEl.files.item(0));
    //         //call the angular http method
    // 			  this.fmService.uploadFile(formData).subscribe(
    //             res=>{
                    
    //                 return cb(null, res);
    //             },
    //             err=>{
                    
    //                 return cb(err);
    //                 // this.notifier.alert(err.code, err.message, 'danger');
    //                 // this.fh.upload1Status=err.message;
                    
    //             }
    //         )

    // 	}
    // }
    // uploadAnswersheet(){
	// 	if(!this.activeTest){
	// 		this.answerUploadModal=false;
	// 		return;
	// 	}
	// 	var self=this;
	// 	this.panelLoader="show";		
    //   	// this.uploadFile(function(err, file){
	//     //     if(err){
	// 	// 		self.panelLoader="none";
	// 	// 		return self.notifier.alert(err.code, err.message, "danger", 5000);
	//     //     }
	//     //     let data={
	// 	// 		stdResponseFile:file.url
	// 	// 	}
	// 	// 	self.ctService.updateStudentResponse(self.activeTest._id, data).subscribe(
	// 	// 		res=>{
	// 	// 			self.panelLoader="none";
	// 	// 			self.answerUploadModal=false;
	// 	// 			self.ceTestSeries[self.activeTestSeriesIndex].tests[self.activeRowIndex].stdResponseFile=res[0].stdResponseFile;
	// 	// 			self.notifier.alert('Uploaded', 'Your response uploded successfully!', "success", 500);
	// 	// 		},
	// 	// 		err=>{
	// 	// 			self.panelLoader="none";
	// 	// 			self.notifier.alert(err.code, err.message, "danger", 5000);
	// 	// 		}
	// 	// 	)
    //   	// });
	// }
	pageRendered(e: CustomEvent) {
		console.log('(page-rendered)', e);
	}
	
	// confirmedToCheck(testSeriesIndex,testIndex){
	// 	let confirmedAt=new Date();
	// 	let data={confirmedToCheck:true, confirmedAt:confirmedAt};
	// 	this.ctService.updateStudentResponse(this.ceTestSeries[testSeriesIndex].tests[testIndex]._id, data).subscribe(
	// 		res=>{
	// 			this.panelLoader="none";
	// 			this.ceTestSeries[testSeriesIndex].tests[testIndex].confirmedToCheck=true;
	// 			this.notifier.alert('Confirmed', 'Confirmed to Check Successfully!', "success", 500);
	// 		},
	// 		err=>{
	// 			this.panelLoader="none";
	// 			this.notifier.alert(err.code, err.message, "danger", 5000);
	// 		}
	// 	)
	// }
	
	loadFeedBack(){
		this.feedbackService.getFeedback().subscribe(
			(res)=>{
				this.feedbackList=res;
				this.notifier.alert('Success', 'Loaded Successfully', 'success', 500);
			},
			(err)=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}
	
	showFeedBackModal(testSeriesIndex,testIndex){
		this.activeTest = this.ceTestSeries[testSeriesIndex].tests[testIndex];
		this.activeRowIndex = testIndex;
		this.showFeedBackOption=true;
		this.loadFeedBack();
	}
	submitFeedBack(quesId){
		if (!this.feedBackForm.valid) {
			return;
		}

		let formData= this.feedBackForm.value;
		formData.question=quesId;
		formData.user=this.sessionStd;
		 this.feedbackService.addFeedbackResponse(formData).subscribe(
			 (res)=>{
				 this.notifier.alert('success','Added Success Fully', 'success', 500);
			 },
			 (err) => {
				 this.notifier.alert(err.code, err.message, 'danger', 5000);
			 }
		 );
	}
	showVideoModal(testSeriesIndex,testIndex, videoIndex){
		this.viewLimitMsg=null;
		this.activeTest = this.ceTestSeries[testSeriesIndex].tests[testIndex];
		this.videoSolutions=null;
		this.showVideo = true;
		this.panelLoader="show";
		this.videoService.getOneVideo(this.activeTest.test.solVideos[videoIndex]).subscribe(
			(res) => {
				this.videoData = res;
				if (this.videoData) {
					this.videoService.getVideoViews(this.activeTest.user, this.activeTest.test.solVideos[videoIndex]).subscribe(
						(res) => {
							this.videoLimit = res;
							if (!res.totalViews || (res.totalViews < this.activeTest.test.videoViewsLimit)) {

								let self = this;
								self.panelLoader = 'none';
								self.videoSolutions = self.sanitizer.bypassSecurityTrustResourceUrl(self.videoData.videoSource);
								self.videoTimerId = window.setTimeout(() => {
									self.videoService.addVideoView({ user: self.activeTest.user, video: self.activeTest.test.solVideos[videoIndex] }).subscribe(
										(res) => { },
										(err) => { }
									)
								}, 500000);
							} else {
								this.showVideo = false;
								this.viewLimitMsg = "Video view limt(" + this.activeTest.test.videoViewsLimit + " views) has been exceeded!";
							}
						},
						(err) => {
							this.notifier.alert(err.code, err.message, 'danger', 5000);
						}
					);
				}
			},
			(err) => {
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		)
				
		
		// this.videoSolutions = this.sanitizer.bypassSecurityTrustResourceUrl(this.activeTest.solVideos[videoIndex]);
		// this.showVideo=true;
		// let self=this;
		// let views=0;
		// if (self.activeTest.views) {
		// 	views = self.activeTest.views;
		// }else{
		// 	views= 0;
		// }
		// self.videoTimerId=window.setTimeout(() => { 			
		// 	self.ctService.updateStudentResponse(self.activeTest.id, {views:views+1}).subscribe(
		// 		(res)=>{},
		// 		(err)=> {}
		// 	)
		// }, 5000);
	}
	 
	closeVideo(){
		clearTimeout(this.videoTimerId);	
	}
}
