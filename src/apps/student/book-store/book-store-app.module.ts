import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";
import { BookStoreAppComponent } from './app.component';

@NgModule({
    declarations: [BookStoreAppComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[]
})
export class StudentBookStoreAppModule { }
