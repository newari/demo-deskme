import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentBookStoreHomeComponent } from './home.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { DialogModule } from 'primeng/primeng';
import { BookstoreService } from '../../../../../lib/services/bookstore.service';
export const ROUTES:Routes=[
    {path: '', component: StudentBookStoreHomeComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentBookStoreHomeComponent],
    imports:[
        CommonModule,
        DialogModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[BookstoreService]
})
export class StudentBookStoreHomeActivity { }