import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { EzukitAppService } from '../../../../../lib/services/ezukit-app.service';
import { AuthService } from '../../../../../lib/services/auth.service';
import { ClientConfig } from '../../../../../lib/models/clientconfig.model';
import { StudentService } from '../../../../../lib/services/student.service';

@Component({
    selector:'student-panel-header',
    templateUrl: './student-panel-header.html'
})
export class StudentPanelHeader {
    @Input('client') client:ClientConfig;
    @Input() EkAppName: Observable<any>;
    headerItems:any;
    subHeaderItems:any;
    displayMenuItems:boolean;
    display:string='show';
    menuOpened:boolean;
    notifications:any[];
    nbLoader:string;
    nbOpened:boolean;
    public items:any[]= [
            {
                label: 'Logout',
                icon: 'fa-sign-out',
                command: (event) => {
                    if(this.authService.logout()){
                        if(this.client&&this.client.defaultLogoutPage){
                           window.location.href=this.client.defaultLogoutPage
                        }else{
                            this.router.navigate(['/student/auth/login']);
                        }
                    }
                }
            }
        ];
    constructor(private appService:EzukitAppService, private authService:AuthService, private router:Router, private studentService:StudentService){
        // let self=this;


    }

    ngOnInit(){
        let headerMenuItems={
            "ALPHA":[
                 {url:"/student/dashboard", icon:'glyphicon glyphicon-home'},
                 {title:"My Courses", url:"/student/online-courses"},
                 {title:"Browse Courses", url:"/student/online-courses/buy"}
            ],
            "DEFAULT":[
                 {title:"My Courses", url:"/student/course"},
                 {title:"Apply Online", url:"/student/shop/order/new"}
            ],
        };
        let subHeaderItems={
            "ALPHA":[
                {title:"Video Courses", url:"/student/online-courses" },
                {
                    title:"Test Series",
                    items:[
                        {title:"My Test Series", url:"/student/testment/testseries"},
                        {title:"Repots", url:"/student/testment/report"},
                        {title:"Bookmark Question", url:'/student/testment/bookmark'}
                    ]
                }
           ],
        //    "DEFAULT":[
        //         {title:"Video Courses", url:"/student/online-courses", },
        //         {title:"Test Series", items:[
        //             {title:"", url:""}
        //         ]}
        //    ],
        //    "PRIMA":[
        //         {title:"Video Courses", url:"/student/online-courses", },
        //         {title:"Test Series", items:[
        //             {title:"", url:""}
        //         ]}
        //    ]
        };
        if(this.client&&this.client.studentZoneTheme&&headerMenuItems[this.client.studentZoneTheme]){
            this.headerItems=headerMenuItems[this.client.studentZoneTheme];
        }else{
            this.headerItems=headerMenuItems['DEFAULT'];
        }
        if(this.client&&this.client.studentZoneTheme&&subHeaderItems[this.client.studentZoneTheme]){
            this.subHeaderItems=subHeaderItems[this.client.studentZoneTheme];
        }else{
            this.subHeaderItems=subHeaderItems['DEFAULT'];
        }
        console.log(this.subHeaderItems);

        this.appService.data.subscribe(appConfig=>this.EkAppName=appConfig.name);
    }

    logout(){
        if(this.authService.logout()){
            if(this.client&&this.client.defaultLogoutPage){
                window.location.href=this.client.defaultLogoutPage
            }else{
                this.router.navigate(['/student/auth/login']);
            }
        }
    }
    showMenu(){
        if(!this.displayMenuItems){
            this.displayMenuItems=true;
        }else{
            this.displayMenuItems=false;
        }
    }

    toggleDisplay(){
        if(this.display){
            if(this.display=='show'){
                this.display='hide'
            }else{
                this.display=='show'
            }
        }
    }
    showSubMenu(){
        this.display='show';
    }
    loadNoticeboard(){
        this.nbLoader="show";
        this.nbOpened=true;
        this.studentService.getNotifications().subscribe(
            res=>{
                this.nbLoader="normal";
                this.notifications=res;
            },
            err=>{
                this.nbLoader="normal";
                console.log(err);
            }
        )
    }
}
