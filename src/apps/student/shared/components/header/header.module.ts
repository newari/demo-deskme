import { NgModule } from '@angular/core';
import { StudentPanelHeader } from './student-panel-header';
import { CommonModule } from '@angular/common';
import { StudentService } from '../../../../../lib/services/student.service';

@NgModule({
    declarations:[StudentPanelHeader],
    imports:[CommonModule],
    exports:[StudentPanelHeader],
    providers:[StudentService]
})
export class StudentHeaderModule{

}
