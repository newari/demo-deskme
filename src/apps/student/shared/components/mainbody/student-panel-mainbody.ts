import { Component, Input } from '@angular/core';
import { ClientConfig } from '../../../../../lib/models/clientconfig.model';

@Component({
    selector: 'student-panel-mainbody',
    templateUrl: './student-panel-mainbody.html'
})
export class StudentPanelMainbody { 
    sidebarState='CLOSED';
    @Input() client:ClientConfig;
    toggleSidebar(){
        if(this.sidebarState=="OPENED"){
            this.sidebarState="CLOSED";
        }else{
            this.sidebarState="OPENED";
        }
    }
}
