import { Component, OnInit, Input, EventEmitter, Output, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import * as moment from 'moment';
import { Product } from '../../../../../lib/models/product.model';
import { UserService } from '../../../../../lib/services/user.service';
import { SessionService } from '../../../../../lib/services/session.service';
import { CoptionService } from '../../../../../lib/services/coption.service';
import { CenterService } from '../../../../../lib/services/center.service';
import { ProductService } from '../../../../../lib/services/product.service';
import { StudentService } from '../../../../../lib/services/student.service';
import { SettingsService } from '../../../../../lib/services/settings.service';
import { User } from '../../../../../lib/models/user.model';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';
import { Student } from '../../../../../lib/models/student.model';
import { AuthService } from '../../../../../lib/services/auth.service';
import { OrderService } from '../../../../../lib/services/order.service';
import { ClientService } from '../../../../../lib/services/client.service';

@Component({
    selector: 'ek-student-reg-form-2',
    templateUrl: './full-order-2-form.component.html'
})
export class StudentRegForm2Component implements OnInit {
    studentRegForm: FormGroup;
    userData: User;
    formStatus: string = "Normal";
    resetPassword: boolean = false;
    courses: any[] = [];
    productTypes: any[] = [];
    products: Product[];
    selectedProduct: Product;
    selectedProductType:any;
    validationError: string;
    paymentMethod: string = "";
    paymentOption: string = "";
    fh: any = {
        personalImg: 'https://iesmaster.org/public/images/dummy-photo.png',
        signImg: 'https://iesmaster.org/public/images/dummy-photo-sign.png',
        status: 'Normal',
        error: null
    };
    user:any
    formSettings: any;
    tandCSetting:any;
    // myDatePickerOptions: IMyDpOptions = {
    //     // other options...
    //     dateFormat: 'dd/mm/yyyy',
    // };
    stdHavePastExams: boolean = false;
    isExStd: boolean = false;
    pastCrses: number[] = [0, 1, 2];
    @Input() submitLabel: string = 'Save';
    @Input() userSearchOpt: boolean = false;
    @Input() defaultUserValue: any;
    @Input() formCourses: any[];//=["ESE+GATE"];  //Course Coptio Values
    @Input() formProductTypes: any[];//=["Postal Book Program"];  //ProductType Coptio Values
    @Input() streams: any[]//=['CE', 'ME']; //'EE', 'EC'
    @Input() centers: any[]//=['centerId'];
    @Input() sessions: any[]//=['sessionId'];
    @Output() onStudentRegistered: EventEmitter<any> = new EventEmitter<any>();
    @Input() student: Student;
    @Input() productCategory: string[];
    siblings: FormArray;
    orderData: any;
    batchSeats: any;
    cartTotal: any;
    addedProducts: any=[];
    client: any;
    appliedDiscount: any;
    order: any;
    activeStep: number=1;
    currentStdDoc: any;
    currentStdSrn: any;
    formData: any;
    discountAmt: any;
    tandcAccepted: any;
    isCurrentStd: any;
    paymentOptions: boolean;
    activePmtOptionIndex: number;
    pmtOptAmount: any;
    nonCouponDiscounts: any;
    allRelatedProducts: any;
    texTotal: number;
    taxTotal: number;
    selectedSession:any;
    // @Input('user')
    // set user(u) {
    //     if (this.studentRegForm) {
    //         if (u) {
    //             let uData: any = {};
    //             uData.firstName = u.firstName;
    //             uData.lastName = u.lastName;
    //             uData.email = u.email;
    //             uData.mobile = u.mobile;
    //             uData.personalImg = u.profileImg;
    //             if (!u.address) {
    //                 uData.address = {};
    //                 uData.permanantAddress = {};
    //             } else {
    //                 if (!u.address.shipping) {
    //                     uData.address = { address: '', landmark: '', city: '', state: '', postalCode: '', country: 'India' };
    //                 } else {
    //                     uData.address = u.address.shipping
    //                 }
    //                 if (!u.address.billing) {
    //                     uData.permanantAddress = { address: '', landmark: '', city: '', state: '', postalCode: '', country: 'India' };
    //                 } else {
    //                     uData.permanantAddress = u.address.billing
    //                 }
    //             }
    //             this.studentRegForm.patchValue({ user: uData });
    //             this.userData = u;
    //             this.resetPassword = false;
    //         } else {
    //             this.studentRegForm.patchValue({ user: {} });
    //             this.userData = null;
    //             this.resetPassword = true;
    //         }
    //     }

    // }
    constructor(
        private fb: FormBuilder,
        private userService: UserService,
        private notifier: NotifierService,
        private el: ElementRef,
        private sessionService: SessionService,
        private coptionService: CoptionService,
        private centerService: CenterService,
        private productService: ProductService,
        private studentService: StudentService,
        private settingService: SettingsService,
        private authService: AuthService,
        private orderService: OrderService,
        private clientService:ClientService
    ) {

    }
    ngOnInit(): void {
        let self = this;
        window.setTimeout(function () {
            self.setInit();
        }, 1);
    }

    setInit() {
        this.loadClient();
        this.loadCenters();
        // this.loadCourse();
        // this.loadProductTypes();
        // this.loadStreams();
        // this.loadSessions();
        this.loadSettings();
        this.loadTandCSetting();
    }
    loadTandCSetting() {
        this.settingService.getSettingByKey("TANDC").subscribe(
            res=>{
                this.tandCSetting=res;
            },err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    loadClient() {
        this.clientService.getClientConfig().subscribe(
            res => {
                this.client = res;
            },
            err => {
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }
    loadSettings() {
        this.settingService.getSettingByKey('ADMISSION_SETTING').subscribe(
            (res) => {
                this.formSettings = res.more;
                if (this.formSettings) {
                    this.studentRegForm = this.fb.group({
                        center: ['', Validators.required],
                        course: ['', Validators.required],
                        productType: ['', Validators.required],
                        stream: ['', Validators.required],
                        session: ['', Validators.required],
                        product: ['', Validators.required],
                        batch: [''],
                        user: this.fb.group({
                            firstName: ['', Validators.required],
                            lastName: ['', (this.formSettings.lastName&&this.formSettings.lastName.mandatory ? Validators.required : '')],
                            middleName: ['', (this.formSettings.middleName&&this.formSettings.middleName.mandatory ? Validators.required : '')],
                            mobile: ['', (this.formSettings.mobile&&this.formSettings.mobile.mandatory ? Validators.required : '')],
                            email: ['', Validators.required],
                            category: ['null', (this.formSettings.category&&this.formSettings.category.mandatory ? Validators.required : '')],
                            gender: ['null', (this.formSettings.gender&&this.formSettings.gender.mandatory ? Validators.required : '')],
                            dob: [null, (this.formSettings.dob&&this.formSettings.dob.mandatory ? Validators.required : '')],
                            personalImg: ['', (this.formSettings.personalImg&&this.formSettings.personalImg.mandatory ? Validators.required : '')],
                            signImg: ['', (this.formSettings.signImg&&this.formSettings.signImg.mandatory ? Validators.required : '')],
                            idProof:['',(this.formSettings.idProof&&this.formSettings.idProof.mandatory ? Validators.required : '')],
                            father: this.fb.group({
                                name: ['', (this.formSettings.father.fields.name.mandatory ? Validators.required : '')],
                                mobile: ['', (this.formSettings.father.fields.mobile.mandatory ? Validators.required : '')],
                                email: ['', (this.formSettings.father.fields.email.mandatory ? Validators.required : '')],
                                qualification: ['', (this.formSettings.father.fields.qualification.mandatory ? Validators.required : '')],
                                designation: ['', (this.formSettings.father.fields.designation.mandatory ? Validators.required : '')],
                                occupation: ['', (this.formSettings.father.fields.occupation.mandatory ? Validators.required : '')],
                                officeAddress: ['', (this.formSettings.father.fields.officeAddress.mandatory ? Validators.required : '')],
                                age: ['', (this.formSettings.father.fields.age.mandatory ? Validators.required : '')],
                                photo: ['', (this.formSettings.father.fields.photo.mandatory ? Validators.required : '')],
                                nationality: ['INDIAN', (this.formSettings.father.fields.nationality.mandatory ? Validators.required : '')],
                                aadhaarNo: ['', (this.formSettings.father.fields.aadhaarNo.mandatory ? Validators.required : '')],
                                annualIncome: ['', (this.formSettings.father.fields.annualIncome.mandatory ? Validators.required : '')]

                            }),
                            mother: this.fb.group({
                                name: ['', (this.formSettings.mother.fields.name.mandatory ? Validators.required : '')],
                                mobile: ['', (this.formSettings.mother.fields.mobile.mandatory ? Validators.required : '')],
                                email: ['', (this.formSettings.mother.fields.email.mandatory ? Validators.required : '')],
                                qualification: ['', (this.formSettings.mother.fields.qualification.mandatory ? Validators.required : '')],
                                designation: ['', (this.formSettings.mother.fields.designation.mandatory ? Validators.required : '')],
                                occupation: ['', (this.formSettings.mother.fields.occupation.mandatory ? Validators.required : '')],
                                officeAddress: ['', (this.formSettings.mother.fields.officeAddress.mandatory ? Validators.required : '')],
                                age: ['', (this.formSettings.mother.fields.age.mandatory ? Validators.required : '')],
                                photo: ['', (this.formSettings.mother.fields.photo.mandatory ? Validators.required : '')],
                                nationality: ['INDIAN', (this.formSettings.mother.fields.nationality.mandatory ? Validators.required : '')],
                                aadhaarNo: ['', (this.formSettings.mother.fields.aadhaarNo.mandatory ? Validators.required : '')],
                                annualIncome: ['', (this.formSettings.mother.fields.annualIncome.mandatory ? Validators.required : '')]
                            }),
                            urbanOrRural: ['URBAN', (this.formSettings.urbanOrRural&&this.formSettings.urbanOrRural.mandatory ? Validators.required : '')],
                            motherTongue: ['HINDI', (this.formSettings.motherTongue&&this.formSettings.motherTongue.mandatory ? Validators.required : '')],
                            nationality: ['INDIAN', (this.formSettings.nationality&&this.formSettings.nationality.mandatory ? Validators.required : '')],
                            languageKnown: ['HINDI', (this.formSettings.languageKnown&&this.formSettings.languageKnown.mandatory ? Validators.required : '')],
                            aadhaarNo: ['', (this.formSettings.aadhaarNo&&this.formSettings.aadhaarNo.mandatory ? Validators.required : '')],
                            religion: ['HINDU', (this.formSettings.religion&&this.formSettings.religion.mandatory ? Validators.required : '')],
                            bloodGroup: ['null', (this.formSettings.bloodGroup&&this.formSettings.bloodGroup.mandatory ? Validators.required : '')],
                            address: this.fb.group({
                                address: ['', (this.formSettings.address&&this.formSettings.address.fields&&this.formSettings.address.fields.address&&this.formSettings.address.fields.address.mandatory ? Validators.required : '')],
                                landmark: [''],
                                city: [''],
                                state: [''],
                                postalCode: [''],
                                country: ['India'],
                            }),
                            permanantAddress: this.fb.group({
                                address: ['', (this.formSettings.address&&this.formSettings.address.fields&&this.formSettings.address.fields.permanentAddress&&this.formSettings.address.fields.permanentAddress.mandatory ? Validators.required : '')],
                                landmark: [''],
                                city: [''],
                                state: [''],
                                postalCode: [''],
                                country: ['India']
                            }),
                            siblings: this.fb.array([]),
                            exStudent: this.fb.group({
                                srn: ['',  ],
                                session: [''],
                                idProof: [''],
                                program: ['']
                            }),
                            pastCourses: this.fb.array([
                                this.initPastCourseFrm("", this.formSettings.pastCourses&&this.formSettings.pastCourses.fields&&this.formSettings.pastCourses.fields.college&&this.formSettings.pastCourses.fields.college.mandatory?true:false),
                                this.initPastCourseFrm(""),
                                this.initPastCourseFrm("Other")
                            ]),
                            pastExams: this.fb.array([
                                this.initPastExamFrm("GATE"),
                                this.initPastExamFrm("ESE")
                            ]),
                            emergencyContact: this.fb.group({
                                name: ['', (this.formSettings.emergencyContact&&this.formSettings.emergencyContact.fields&&this.formSettings.emergencyContact.fields.name&&this.formSettings.emergencyContact.fields.name.mandatory ? Validators.required : '')],
                                mobile: ['', (this.formSettings.emergencyContact&&this.formSettings.emergencyContact.fields&&this.formSettings.emergencyContact.fields.mobile&&this.formSettings.emergencyContact.fields.mobile.mandatory ? Validators.required : '')],
                                relationship: ['', (this.formSettings.emergencyContact&&this.formSettings.emergencyContact.fields&&this.formSettings.emergencyContact.fields.relationship&&this.formSettings.emergencyContact.fields.relationship.mandatory ? Validators.required : '')],
                            }),
                            medicalHistory: this.fb.group({
                                birthHistory: this.fb.group({
                                    birthDetail: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.birthHistory&&this.formSettings.medicalHistory.birthHistory.fields&&this.formSettings.medicalHistory.birthHistory.fields.birthDetail&&this.formSettings.medicalHistory.birthHistory.fields.birthDetail.mandatory ? Validators.required : '')],
                                    birthCry: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.birthHistory&&this.formSettings.medicalHistory.birthHistory.fields&&this.formSettings.medicalHistory.birthHistory.fields.birthCry&&this.formSettings.medicalHistory.birthHistory.fields.birthCry.mandatory ? Validators.required : '')],
                                    dischargeInDays: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.birthHistory&&this.formSettings.medicalHistory.birthHistory.fields&&this.formSettings.medicalHistory.birthHistory.fields.dischargeInDays&&this.formSettings.medicalHistory.birthHistory.fields.dischargeInDays.mandatory ? Validators.required : '')],
                                    specialCare: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.birthHistory&&this.formSettings.medicalHistory.birthHistory.fields&&this.formSettings.medicalHistory.birthHistory.fields.specialCare&&this.formSettings.medicalHistory.birthHistory.fields.specialCare.mandatory ? Validators.required : '')],
                                    ifNICU: ['',],
                                    explaination: ['',],
                                }),
                                hearing: this.fb.group({
                                    difficulty: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.hearing&&this.formSettings.medicalHistory.hearing.fields&&this.formSettings.medicalHistory.hearing.fields.difficulty&&this.formSettings.medicalHistory.hearing.fields.difficulty.mandatory ? Validators.required : false)],
                                    consultation: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.hearing&&this.formSettings.medicalHistory.hearing.fields&&this.formSettings.medicalHistory.hearing.fields.consultation&&this.formSettings.medicalHistory.hearing.fields.consultation.mandatory ? Validators.required : false)],
                                    explaination: ['']
                                }),
                                vision: this.fb.group({
                                    useLenses: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.vision.fields&&this.formSettings.medicalHistory.vision.fields.useLenses&&this.formSettings.medicalHistory.vision.fields.useLenses.mandatory ? Validators.required : false)],
                                    consultation: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.vision.fields&&this.formSettings.medicalHistory.vision.fields.consultation&&this.formSettings.medicalHistory.vision.fields.consultation.mandatory ? Validators.required : false)],
                                    explaination: ['']

                                }),
                                motorMileStone: this.fb.group({
                                    sitting: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.motorMileStone&&this.formSettings.medicalHistory.motorMileStone.fields&&this.formSettings.medicalHistory.motorMileStone.fields.sitting&&this.formSettings.medicalHistory.motorMileStone.fields.sitting.mandatory ? Validators.required : false)],
                                    standing: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.motorMileStone&&this.formSettings.medicalHistory.motorMileStone.fields&&this.formSettings.medicalHistory.motorMileStone.fields.standing&&this.formSettings.medicalHistory.motorMileStone.fields.standing.mandatory ? Validators.required : false)],
                                    walking: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.motorMileStone&&this.formSettings.medicalHistory.motorMileStone.fields&&this.formSettings.medicalHistory.motorMileStone.fields.walking&&this.formSettings.medicalHistory.motorMileStone.fields.walking.mandatory ? Validators.required : false)],
                                    speech: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.motorMileStone&&this.formSettings.medicalHistory.motorMileStone.fields&&this.formSettings.medicalHistory.motorMileStone.fields.speech&&this.formSettings.medicalHistory.motorMileStone.fields.speech.mandatory ? Validators.required : false)],
                                    medicationForMedicalCondition: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.motorMileStone&&this.formSettings.medicalHistory.motorMileStone.fields&&this.formSettings.medicalHistory.motorMileStone.fields.medicationForMedicalCondition&&this.formSettings.medicalHistory.motorMileStone.fields.medicationForMedicalCondition.mandatory ? Validators.required : false)],
                                    medicationForWellBeing: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.motorMileStone&&this.formSettings.medicalHistory.motorMileStone.fields&&this.formSettings.medicalHistory.motorMileStone.fields.medicationForWellBeing&&this.formSettings.medicalHistory.motorMileStone.fields.medicationForWellBeing.mandatory ? Validators.required : false)],
                                    medicationForAlergy: ['', (this.formSettings.medicalHistory&&this.formSettings.medicalHistory.motorMileStone&&this.formSettings.medicalHistory.motorMileStone.fields&&this.formSettings.medicalHistory.motorMileStone.fields.medicationForAlergy&&this.formSettings.medicalHistory.motorMileStone.fields.medicationForAlergy.mandatory ? Validators.required : false)],
                                })
                            }),
                            miscellaneous: this.fb.group({
                                howDidHearAboutInstitue: this.fb.group({
                                    website: [''],
                                    magzine: [''],
                                    newsPaper: [''],
                                    others: ['']
                                })
                            }),
                            distanceFromSchool: ['', (this.formSettings.distanceFromSchool&&this.formSettings.distanceFromSchool.mandatory ? Validators.required : '')]
                        }),
                        // cart: this.fb.group({
                        //     discountCode: ['']
                        // })
                    });

                    this.createSiblingByInput(2);
                    this.user = this.authService.student();
                    this.setStudent();

                }

            },
            (err) => {
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    setExStudent(value){
        if(value=='mandatory'){
            let user= this.studentRegForm.get('user') as FormGroup;
            let exStudent= user.get('exStudent') as FormGroup;
            exStudent.setControl('srn', new FormControl('',Validators.required))
            exStudent.setControl('session', new FormControl('',Validators.required))
            exStudent.setControl('idProof', new FormControl('',Validators.required))
            exStudent.setControl('program', new FormControl('',Validators.required))

        }else{
            let user= this.studentRegForm.get('user') as FormGroup;
            let exStudent= user.get('exStudent') as FormGroup;
            exStudent.setControl('srn', new FormControl(''))
            exStudent.setControl('session', new FormControl(''))
            exStudent.setControl('idProof', new FormControl(''))
            exStudent.setControl('program', new FormControl(''))
        }
    }
    get exStudent() {
        return this.studentRegForm.get('exStudnet') as FormControl;
      }
    createSiblings(): FormGroup {
        return this.fb.group({
            name: [''],
            age: [''],
            institute: [''],
            standard: ['']
        })
    }

    createSiblingByInput(totalSibling) {
        if (totalSibling != '') {
            let user = this.studentRegForm.get('user') as FormGroup;
            this.siblings = user.get('siblings') as FormArray;
            for (let index = 0; index < totalSibling; index++) {
                this.siblings.push(this.createSiblings())
            };

        }
    }
    removeSibling(index) {
        this.siblings.removeAt(index);
    }
    initPastCourseFrm(crsName: string, required?: boolean) {
        return this.fb.group({
            course: [crsName, (required ? Validators.required : null)],
            courseDuration: [''],
            college: ['', (required ? Validators.required : null)],
            year: ['', (required ? Validators.required : null)],
            marks: ['', (required ? Validators.required : null)],
            remarks: [''],
        })
    }

    initPastExamFrm(examName: string) {
        return this.fb.group({
            name: [examName],
            year: ['2016-17'],
            rank: [''],
            remarks: [''],
        })
    }

    loadSessions() {
        let filter: any = {   };
        this.studentRegForm.patchValue({session:null});
        if (this.studentRegForm.value.center) {
            filter.center = this.studentRegForm.value.center;
        }
        this.productService.getSessions(filter).subscribe(
            res => {
                this.sessions = res;
            },
            err => {
                console.log(err);
            }
        );
    }

    loadCenters() {
        let filter: any = { status: true };
        this.centerService.getCenter(filter).subscribe(
            res => {
                this.centers = res;
                if(this.studentRegForm&&this.studentRegForm.value&&this.studentRegForm.value.center){
                    this.loadSessions()
                }
            },
            err => {
                console.log(err);
            }
        );
    }

    loadCourse(session?:any) {
        // this.coptionService.getCoption({ option: 'COURSE', status: true }).subscribe(
        //     res => {
        //         if (this.formCourses) {
        //             var self = this;
        //             res = res.filter(function (crs: any) {
        //                 for (let ci = 0; ci < self.formCourses.length; ci++) {
        //                     if (crs.value == self.formCourses[ci]) {
        //                         return true;
        //                     }
        //                 }
        //                 return false;
        //             })
        //         }
        //         this.courses = res;
        //     },
        //     err => {
        //         console.log(err);
        //     }
        // );
        let filter:any={

        }

        if(this.studentRegForm.value.center){
            filter.center=this.studentRegForm.value.center
        }
        if(this.studentRegForm.value.session){
            filter.session=this.studentRegForm.value.session
            this.selectedSession = this.sessions.find((session)=>{
                if(session.id===filter.session) return session;
            })
        }
        this.studentRegForm.patchValue({course:''});

        this.productService.getCourses(filter).subscribe(
            res=>{
                // if (this.formCourses) {
                //     var self = this;
                //     res = res.filter(function (crs: any) {
                //         for (let ci = 0; ci < self.formCourses.length; ci++) {
                //             if (crs.value == self.formCourses[ci]) {
                //                 return true;
                //             }
                //         }
                //         return false;
                //     })
                // }
                this.courses = res;
            },
            err=>{

            }
        );
    }
    loadProductTypes() {
        // this.coptionService.getCoption({ option: 'PRODUCTTYPE', status: true }).subscribe(
        //     res => {
        //         if (this.formProductTypes) {
        //             var self = this;
        //             res = res.filter(function (pt: any) {
        //                 for (let ci = 0; ci < self.formProductTypes.length; ci++) {
        //                     if (pt.value == self.formProductTypes[ci]) {
        //                         return true;
        //                     }
        //                 }
        //                 return false;
        //             })
        //         }
        //         this.productTypes = res;
        //     },
        //     err => {
        //         console.log(err);
        //     }
        // );
        let filter:any={};
        if(this.studentRegForm.value.center){
            filter.center=this.studentRegForm.value.center
        }
        if(this.studentRegForm.value.session){
            filter.session=this.studentRegForm.value.session
        }
        if(this.studentRegForm.value.course){
            filter.course=this.studentRegForm.value.course
        }
        if(this.studentRegForm.value.stream){
            filter.stream=this.studentRegForm.value.stream
        }
        this.studentRegForm.patchValue({product:'',batch:'',productType:''});
        this.selectedProduct=null;
        this.productService.getProductTypes( filter).subscribe(
            res => {
                // if (this.formProductTypes) {
                //     var self = this;
                //     res = res.filter(function (pt: any) {
                //         for (let ci = 0; ci < self.formProductTypes.length; ci++) {
                //             if (pt.value == self.formProductTypes[ci]) {
                //                 return true;
                //             }
                //         }
                //         return false;
                //     })
                // }
                this.productTypes = res;
            },
            err => {
                console.log(err);
            }
        );

    }
    loadStreams() {
        // this.coptionService.getCoption({ option: 'STREAM', status: true }).subscribe(
        //     res => {
        //         if (this.streams) {
        //             var self = this;
        //             res = res.filter(function (strm: any) {
        //                 for (let ci = 0; ci < self.streams.length; ci++) {
        //                     if (strm.value == self.streams[ci]) {
        //                         return true;
        //                     }
        //                 }
        //                 return false;
        //             })
        //         }
        //         this.streams = res;
        //     },
        //     err => {
        //         console.log(err);
        //     }
        // );
        let filter:any={}
        this.studentRegForm.patchValue({stream:''});
        if(this.studentRegForm.value.center){
            filter.center=this.studentRegForm.value.center
        }
        if(this.studentRegForm.value&&this.studentRegForm.value.course){
            filter.course=this.studentRegForm.value.course
        }
        if(this.studentRegForm.value.session){
            filter.session=this.studentRegForm.value.session
        }
        // if(this.studentRegForm.value.productType){
        //     filter.productType=this.studentRegForm.value.productType
        // }
        // if(this.studentRegForm.value.stream){
        //     filter.stream=this.studentRegForm.value.stream
        // }
        this.productService.getStreams(filter).subscribe(
            res => {
                // if (this.streams) {
                //     var self = this;
                //     res = res.filter(function (strm: any) {
                //         for (let ci = 0; ci < self.streams.length; ci++) {
                //             if (strm.value == self.streams[ci]) {
                //                 return true;
                //             }
                //         }
                //         return false;
                //     })
                // }
                this.streams = res;
            },
            err => {
                console.log(err);
            }
        );
    }
    loadRelatedProducts(): any {
        let filter: any = {
            productCategory: this.selectedProduct.category.id,
            customParam: "relProduct",
            productType: this.selectedProduct.type.id
        }
        this.productService.getProducts(filter).subscribe(
            (res) => {
                this.allRelatedProducts = res;
            },
            err => {
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    checkProductDepParams() {
        let frmData = this.studentRegForm.value;
        if (frmData.course == "" || frmData.productType == "" || frmData.stream == "" || frmData.session == "" || frmData.center == "") {
            alert("Please select Center, Course, Program, Session & Stream First!");
            return;
        }
    }
    setProductDepParams() {
        this.selectedProduct = this.products[this.studentRegForm.value.product];
        if (this.selectedProduct&&this.selectedProduct.customParam && this.selectedProduct.customParam.haveRelatedProduct) {
            this.loadRelatedProducts();
        }
    }
    onProductChanged(productIndex: number) {
        // this.loadCourse();
        this.studentRegForm.patchValue({ batch: ''});
        // if (this.products[productIndex]) {
        //     this.addedProducts = [productIndex];
        // } else {
        //     this.addedProducts = [];
        // }
        // this.setCartTotal();
        this.setProductDepParams();
    }
    loadProducts() {
        this.studentRegForm.patchValue({product:'',batch:''});
        this.selectedProduct=null;
        let frmData = this.studentRegForm.value;
        if (frmData.course == "" || frmData.productType == "" || frmData.stream == "" || frmData.center == "" || frmData.session == "") {
            return;
        }
        let filter = {
            center: frmData.center,
            course: frmData.course,
            productType: frmData.productType,
            stream: frmData.stream,
            session: frmData.session,
            populateBatches: true,
            status: true
        };

        this.productService.getProduct(filter).subscribe(
            res => {
                this.products = res;
            },
            err => {
                console.log(err);
            }
        );
    }

    setAddressSimilarity(el: any) {
        if (el.target.checked) {
            let corsAdrs = this.studentRegForm.value.user.address;
            this.studentRegForm.patchValue({ user: { permanantAddress: corsAdrs } });
        }
    }

    getValue(arr: any[], id?: string) {
        console.log(this.studentRegForm.value.session);

        if(!id) return;
        if(arr&&Array.isArray(arr)&&arr.length<1){
            return null;
        }
        if(!Array.isArray(arr)) return null;
        console.log(arr);
        arr.filter( (el) =>{
            console.log(el.id);
            console.log(id);
            if (el.id == id) {
                return true;
            }else{
                return false;
            }
        });
        return arr[0];
    }

    generatePassword() {
        // this.regForm.patchValue({password:Math.floor((Math.random() * 100000) + 1)});
    }

    setPasswordOpt(state) {
        this.resetPassword = state;
    }

    setBillingAddress(sameAsShipping) {
        if (sameAsShipping) {
            // this.regForm.patchValue({address:{billing:this.regForm.value.address.shipping}});
        } else {
            // this.regForm.patchValue({address:{billing:((this.userData&&this.userData.address&&this.userData.address.billing)?this.userData.address.billing:{address:'', landmark:'', city:'', state:'', postalCode:'', country:'India'})}});
        }
    }

    onUserSelect(user) {
        this.userService.getOneUser(user.id).subscribe(
            res => {
                this.user = res;
            },
            err => {
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }

    addUpdateStudent(placeOrder?: boolean) {
        let userData = this.studentRegForm.value.user;
        if (!this.studentRegForm.valid) {
            this.validationError = 'show-error';
            this.fh.error = "Please Fill All Mandatory fields";
            return;
        }

        if (this.formSettings) {
            if ((userData.personalImg == "" && this.formSettings.personalImg && this.formSettings.personalImg.mandatory) || (userData.signImg == "" && this.formSettings.signImg && this.formSettings.signImg.mandatory)) {
                this.fh.error = "Personal Image and Sign Image are mandatory!";
                return;
            }
            if(userData.idProof==''&&this.formSettings.idProof&&this.formSettings.idProof.mandatory){
                this.fh.error="ID Proof is mandatory!";
                return;
            }
            if (this.formSettings.father && this.formSettings.father.fields && this.formSettings.father.fields.photo && this.formSettings.father.fields.photo.mandatory && !userData.father.photo) {
                this.fh.error = "Father Photo Required!";
                return;
            }
            if (this.formSettings.mother && this.formSettings.mother.fields && this.formSettings.mother.fields.photo && this.formSettings.mother.fields.photo.mandatory && !userData.mother.photo) {
                this.fh.error = "Mother Photo Required!";
                return;
            }
        }
        userData.customParams = {
            urbanOrRural: userData.urbanOrRural,
            motherTongue: userData.motherTongue,
            nationality: userData.nationality,
            languageKnown: userData.languageKnown,
            aadhaarNo: userData.aadhaarNo,
            religion: userData.religion,
            bloodGroup: userData.bloodGroup,
            emergencyContact: userData.emergencyContact,
            medicalHistory: userData.medicalHistory,
        }
        userData.stream = this.studentRegForm.value.stream;
        userData.course = this.studentRegForm.value.course;
        userData.session = this.studentRegForm.value.session;
        userData.center = this.studentRegForm.value.center;
        userData.productCategory = this.selectedProduct.category.id;
        if (this.selectedProduct.haveBatch) {
            if (!this.studentRegForm.value.batch || this.studentRegForm.value.batch == null) {
                this.fh.error = "Please select Batch first!";
                return;
            }
            userData.batch = this.studentRegForm.value.batch;
        }
        userData.dob = moment(userData.dob).format("DD/MM/YYYY");
        let restOrderData: any = {
            stream: userData.stream,
            course: userData.course,
            center: userData.center,
            session: userData.session
        };
        if (userData.batch) {
            restOrderData.batch = this.selectedProduct.batches[userData.batch].id
        }
        if (userData.store) {
            restOrderData.store = userData.store;
        }
        let stdData: any = {
            createLogin: true,
            student: userData,
            user: {
                password: Math.random() * 100000 | 0,
                id:this.user.id
            }
        }
        this.studentService.addStudent(stdData).subscribe(
            res => {
                this.user = res.user;
                // let evtData = {
                //     student: res,
                //     restOrderData: restOrderData,
                //     selectedProduct: this.selectedProduct
                // }
                if (placeOrder) {
                    this.placeOrder();
                } else {
                    this.fh.status = "Normal";
                }

            },
            err => {
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
    }

    setStudent() {

        if (this.student) {
        console.log("STUDENT ", this.student);
            let stdPatchableData: any = this.getStudentPatchableStudent(this.student);
            this.studentRegForm.patchValue(stdPatchableData);
            return;
        }

        console.log("RELOAD Student");

        this.studentService.getSessionStudent({populateUser:true,populateSession:true,populateCourse:true,populateBatch:true,populateProduct:true}).subscribe(
            res => {
                this.student = res;
                let stdPatchableData: any = this.getStudentPatchableStudent(this.student);
                console.log(stdPatchableData);

                this.studentRegForm.patchValue(stdPatchableData);
            },
            err => {
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )

    }

    getStudentPatchableStudent(student: Student) {
        let dob = null;
        if (student.dob) {
            let dobArr = student.dob.split("/");
            if (dobArr[0] && dobArr[1] && dobArr[2]) {
                dob = new Date(dobArr[1] + "/" + dobArr[0] + "/" + dobArr[2]);
            }

        }
        let center:any;
        let session:any;
        let course:any;
        let stream:any;
        if (student.center){
            center= student.center.id || student.center;
            this.loadCenters();
        }
        if (student.session){
            session= student.session.id || student.session;
            this.loadSessions();
        }
        if (student.majorCourse) {
            course=  student.majorCourse.id || student.majorCourse;
            this.loadCourse();
        }
        if (student.majorStream){
            stream= student.majorStream.id || student.majorStream;
            this.loadStreams();
        }
        if(student.center &&student.session&&student.majorStream&&student.majorCourse){
            this.loadProductTypes();
        }

        let data = {
             center:center||null,
             session:session||null,
             course:course||null,
             stream:stream||null,
            user: {
                firstName: student.firstName,
                lastName: student.lastName,
                middleName:student.middleName,
                mobile: student.mobile,
                email: student.email,
                category: student.category,
                gender: student.gender,
                dob: dob,
                personalImg: student.personalImg,
                signImg: student.signImg,
                idProof: student.idProof,
                father: student.father,
                mother: student.mother,
                address: student.address||{address:'',city:'',state:'',postalCode:'',country:'INDIA'},
                permanantAddress: student.permanantAddress||{address:'',city:'',state:'',postalCode:'',country:'INDIA'},
                exStudent: student.exStudent ||{},
                pastCourses: student.pastCourses || [],
                pastExams: student.pastExams || [],
                customParams:student.customParams||{}
            }
        };

        return data;
    }
    processOrder() {
        this.fh.error = null;
        this.validationError = "";


        if (!this.studentRegForm.valid) {
            this.validationError = "show-error";
            this.fh.error = "Please fill all the mandatory fields!!";
            return;
        }

        this.addUpdateStudent(true)


    }
    processOrderToStep2() {
        this.fh.error = null;
        this.validationError = "";
        if (!this.selectedProduct || !this.addedProducts) {
            this.validationError = "show-error";
            this.fh.error = "Please select valid program/product first!";
            return;
        }
        if (this.selectedProduct && this.selectedProduct.haveBatch && !this.studentRegForm.value.batch) {
            this.validationError = "show-error";
            this.fh.error = "Please select batch first!";
            return;
        }
        if (!this.studentRegForm.valid) {
            this.validationError = "show-error";
            this.fh.error = "Please fill all the mandatory fields.!";
            return;
        }
        if (!this.tandcAccepted) {
            this.validationError = "show-error";
            this.fh.error = "Please accept the terms adn conditions first.!";
            return;
        }
        this.formData = this.studentRegForm.value;
        if (this.selectedProduct && this.selectedProduct.discount && this.selectedProduct.discount.exStudentDiscount && this.isExStd) {
            this.discountAmt = this.selectedProduct.discount.exStudentDiscount;
        } else if (this.selectedProduct && this.selectedProduct.discount && this.selectedProduct.discount.currentStudentDiscount && this.isCurrentStd) {
            this.discountAmt = this.selectedProduct.discount.currentStudentDiscount;

        }
        this.setCartTotal();
        this.activeStep = 2;
    }
    // setCartTotal() {
    //     let ttl = 0;
    //     for (let i = 0; i < this.addedProducts.length; i++) {
    //         ttl += parseFloat("" + this.products[this.addedProducts[i]].cost + "");
    //     }

    //     this.setDiscount();
    //     this.cartTotal = ttl;

    //     if (this.paymentOptions && this.activePmtOptionIndex > -1 && this.selectedProduct) {
    //         this.pmtOptAmount = this.paymentOptions[this.activePmtOptionIndex].amount;
    //     } else {
    //         this.pmtOptAmount = this.cartTotal;
    //     }
    // }
    setDiscount() {
        let discountAmt = 0;
        if (this.isCurrentStd) {
            this.currentStdDoc = this.studentRegForm.value.user.currentStudent.idProof;
            this.currentStdSrn = this.studentRegForm.value.user.currentStudent.srn;
        }
        if (this.nonCouponDiscounts) {
            for (let ci = 0; ci < this.nonCouponDiscounts.length; ci++) {
                let disc = this.nonCouponDiscounts[ci];
                if (disc.type == "CART_ITEM_COUNT" && this.addedProducts.length >= disc.condition.minItemCount) {
                    discountAmt = parseFloat(disc.discountAmount);
                    this.appliedDiscount = {
                        id: disc.id,
                        discountType: 'FIX',
                        discountAmount: discountAmt
                    };
                    break;
                } else if (disc.type == "EX_STUDENT" && this.selectedProduct && this.selectedProduct && this.selectedProduct.discount && this.selectedProduct.discount.exStudentDiscount && this.isExStd) {
                    discountAmt = parseFloat(this.selectedProduct.discount.exStudentDiscount);
                    this.appliedDiscount = {
                        discountCode: 'ExStudent',
                        discountType: 'Fix',
                        discountAmount: this.selectedProduct.discount.exStudentDiscount,
                        reqDoc: this.studentRegForm.value.user.exStudent.idProof,
                        reqDocNo: this.studentRegForm.value.user.exStudent.srn

                    };
                    break;

                } else if (disc.type == "CURRENT_STUDENT" && this.selectedProduct && this.selectedProduct.discount && this.selectedProduct.discount.currentStudentDiscount && this.isCurrentStd) {
                    discountAmt = parseFloat(this.selectedProduct.discount.currentStudentDiscount);
                    this.appliedDiscount = {
                        discountCode: 'CurrentStudent',
                        discountType: 'Fix',
                        discountAmount: this.selectedProduct.discount.currentStudentDiscount,
                        reqDoc: this.currentStdDoc,
                        reqDocNo: this.currentStdSrn
                    };
                    break;

                }
            }
        }
        this.discountAmt = discountAmt;
    }
    placeOrder() {
        if (!this.user) {
            this.validationError = "show-error";
            this.fh.error = "Something wrong, Please login again!";
            return;
        }
        if (!this.client) {
            this.validationError = "show-error";
            this.fh.error = "Something wrong, Please refresh the page!";
            return;
        }
        let formData = this.studentRegForm.value;
        this.orderData = {};

        // this.orderData.store=; //BOOKSTORE ID
        if (this.selectedProduct && this.selectedProduct.store) {
            this.orderData.store = this.selectedProduct.store.id || this.selectedProduct.store;
        } else if (this.addedProducts.length > 0 && this.products[this.addedProducts[0]].store) {
            this.orderData.store = this.products[this.addedProducts[0]].store.id || this.products[this.addedProducts[0]].store;
        } else {
            this.validationError = "show-error";
            this.fh.error = "This Program/Product is not available for Online Payment Option, Contact support!";
            return;
        }
        // if(!this.activePmtOptionIndex){
        //     this.validationError="show-error";
        //     this.fh.error="Please first select Payment Method and Option to proceed!";
        //     return;
        // }
        this.orderData.center = formData.center;
        this.orderData.course = formData.course;
        this.orderData.stream = formData.stream;
        this.orderData.session = formData.session;
        if (this.selectedProduct && this.selectedProduct.haveBatch) {
            this.orderData.batch = this.selectedProduct.batches[formData.batch].id;;
        }
        if(this.selectedProduct&&this.selectedProduct.category){
            this.orderData.productCategory=this.selectedProduct.category.id||this.selectedProduct.category
        }
        if(this.selectedProduct&&this.selectedProduct.type){
            this.orderData.productType=this.selectedProduct.type.id||this.selectedProduct.type
        }
        // this.orderData.product=this.products[formData.product].id;
        // this.orderData.discountDetail={};
        // this.orderData.total=this.products[formData.product].cost;
        this.orderData.product = [];
        this.orderData.product.push(this.selectedProduct.id);
        for (let api = 0; api < this.addedProducts.length; api++) {
            this.orderData.product.push(this.allRelatedProducts[this.addedProducts[api]].id);
        }
        this.orderData.discountDetail = {};
        // this.orderData.total = this.cartTotal;
        this.orderData.total = this.selectedProduct.cost;

        this.orderData.total = parseFloat(this.orderData.total);
        if (this.cartTotal) {
            this.orderData.total = this.orderData.total + this.cartTotal;
        }

        this.orderData.isShipping = true;
        this.orderData.shippingAddress = formData.user.address;
        this.orderData.shippingAddress.name = formData.user.firstName + " " + formData.user.lastName;
        // this.orderData.paymentOptionIndex=this.activePmtOptionIndex;
        // this.orderData.pmtForProducts=this.paymentOptions[this.activePmtOptionIndex].pmtForProducts;
        let customParams = formData.customParams;

        if (this.batchSeats && this.batchSeats.length > 0) {
            this.orderData.customParams = {
                batchSeatTitle: this.batchSeats[formData.batchSeat].title,
                batchSeatId: this.batchSeats[formData.batchSeat].id,
                currentESESession: customParams.currentESE.session,
                currentESERollNo: customParams.currentESE.rollNo,
                ese2018AdmitCard: { val: customParams.currentESE.eseAdmitCard, type: 'url' },
                prelimsExpectedMarks: customParams.prelimsExpectedMarks,
                expectedPaperImarks: customParams.expectedPaperImarks,
                expectedPaperIImarks: customParams.expectedPaperIImarks,

            };
            if (this.selectedProduct && this.selectedProduct.customParam && this.selectedProduct.customParam.mode) {
                this.orderData.customParams.mockInterviewMode = 'online';
            } else {
                this.orderData.customParams.mockInterviewMode = formData.mode;
            }
            if (formData.skypeId) {
                this.orderData.customParams.skypeId = formData.skypeId;
            }
        }
        this.orderData.paymentMethod = this.paymentMethod;
        // let payableAmount=this.paymentOptions[this.activePmtOptionIndex].amount;
        // let payableAmount=parseFloat(this.paymentOptions[this.activePmtOptionIndex].amount);
        if (this.selectedProduct && this.selectedProduct.discount && this.selectedProduct.discount.exStudentDiscount && this.isExStd) {
            // payableAmount=payableAmount-parseFloat(this.selectedProduct.discount.exStudentDiscount);
            this.orderData.discountDetail = {
                discountCode: 'ExStudent',
                discountType: 'Fix',
                discountAmount: this.selectedProduct.discount.exStudentDiscount,
                reqDoc: formData.user.exStudent.idProof,
                reqDocNo: formData.user.exStudent.srn

            };
        } else if (this.selectedProduct && this.selectedProduct.discount && this.selectedProduct.discount.currentStudentDiscount && this.isCurrentStd) {
            // payableAmount=payableAmount-parseFloat(this.selectedProduct.discount.currentStudentDiscount);
            this.orderData.discountDetail = {
                discountCode: 'CurrentStudent',
                discountType: 'Fix',
                discountAmount: this.selectedProduct.discount.currentStudentDiscount,
                reqDoc: this.currentStdDoc,
                reqDocNo: this.currentStdSrn
            };
        } else if (this.appliedDiscount) {
            // payableAmount=payableAmount-this.appliedDiscount.discountAmount;
            this.orderData.discountDetail = this.appliedDiscount;
        }
        this.fh.status = "Processing";
        // let storeId=this.paymentOptions[this.activePmtOptionIndex].store;
        this.orderData.tracer={
			form:'STUDENT-ZONE-ORDER-ADD'
        };

        // Calculate Tax from front side
        if(!this.selectedProduct.costIncludedTax&& this.selectedProduct.tax&&(this.selectedProduct.tax.sgst||this.selectedProduct.tax.cgst)){
            let taxtotal= 0;
            if(this.selectedProduct.tax.sgst&&this.selectedProduct.tax.sgst>0){
                taxtotal+= (this.selectedProduct.cost* this.selectedProduct.tax.sgst)/100;
            }

            if(this.selectedProduct.tax.cgst&&this.selectedProduct.tax.cgst>0){
                taxtotal+= (this.selectedProduct.cost* this.selectedProduct.tax.cgst)/100;
            }

            this.orderData.total +=taxtotal;
        }

        if(this.selectedProduct&&this.selectedProduct.customParam&&this.selectedProduct.customParam.autoAssigned){
            this.orderData.autoAssigned=true;
        }
        if(this.orderData.discountDetail&&this.orderData.discountDetail.discountAmount){
            this.orderData.total= this.orderData.total-this.orderData.discountDetail.discountAmount;
        }
        this.orderService.placeOrder(this.user, this.orderData).subscribe(
            res => {
                // console.log(res);
                this.order = res;
                this.activeStep = 4;
                // if(this.orderData.total<0.1){
                //     window.location.href=this.client.baseUrl+"/api/sales/order/online-free-payment-response/"+storeId+"?orderId="+res.id;
                // }else{
                //     window.location.href=this.client.baseUrl+"/api/sales/order/make-online-payment?storeId="+storeId+"&orderId="+res.id+"&orderNo="+res.orderNo+"&totalAmount="+payableAmount+"&redirectUrl="+this.client.baseUrl+"/api/sales/order/online-payment-response/"+storeId+"&cancelUrl="+this.client.baseUrl+"/api/sales/order/online-payment-cancel-response/"+storeId+"&responsePreviewUrl="+this.client.baseUrl+"/api/sales/order/online-payment-response/"+storeId;

                // }
            },
            err => {
                this.fh.status = "Normal";
                this.validationError = "show-error";
                this.fh.error = err.message;
                // console.log(err);
            }
        )
    }
    setTandC(el: any) {
        if (el.target.checked) {
            this.tandcAccepted = true;
        } else {
            this.tandcAccepted = false;

        }
    }


    addProductToCart(productIndex: number) {
        // let isSelectedProductExist= this.addedProducts.indexOf(this.selectedProduct.id);
        // if(isSelectedProductExist==-1) this.addedProducts.push(this.selectedProduct.id);
        if (this.allRelatedProducts[productIndex].isCombo) {
            this.addedProducts = [];
            console.log("here");

        } else if (this.addedProducts.length > 0 && this.allRelatedProducts[this.addedProducts[0]].isCombo) {
            this.addedProducts = [];
            console.log("heres");
        }
        this.addedProducts.push(productIndex);

        this.setCartTotal();

    }
    removeCartProduct(productIndex: number) {
        let ind = this.addedProducts.indexOf(productIndex);
        if (ind > -1) {
            this.addedProducts.splice(ind, 1);
        }
        this.setCartTotal();
    }

    setCartTotal() {
        let ttl = 0;
        let txTtl = 0;
        for (let i = 0; i < this.addedProducts.length; i++) {
            ttl += parseFloat(this.allRelatedProducts[this.addedProducts[i]].cost);
            if(this.allRelatedProducts[this.addedProducts[i]]&&this.allRelatedProducts[this.addedProducts[i]].tax&&(this.allRelatedProducts[this.addedProducts[i]].tax.sgst||this.allRelatedProducts[this.addedProducts[i]].tax.cgst)){
                if(this.allRelatedProducts[this.addedProducts[i]].tax.sgst&&this.allRelatedProducts[this.addedProducts[i]].tax.sgst>0){
                    txTtl+=parseFloat(this.allRelatedProducts[this.addedProducts[i]].cost)*this.allRelatedProducts[this.addedProducts[i]].tax.sgst;
                }
                if(this.allRelatedProducts[this.addedProducts[i]].tax.cgst&&this.allRelatedProducts[this.addedProducts[i]].tax.cgst>0){
                    txTtl+=parseFloat(this.allRelatedProducts[this.addedProducts[i]].cost)*this.allRelatedProducts[this.addedProducts[i]].tax.cgst;
                }
            }
            ttl += parseFloat(this.allRelatedProducts[this.addedProducts[i]].cost);

        }
        this.taxTotal=txTtl;
        this.cartTotal = ttl;

        // if (this.paymentOptions && this.activePmtOptionIndex > -1 && this.selectedProduct && this.selectedProduct.disableFullPriceOptOnOrder) {
        //     this.pmtOptAmount = this.paymentOptions[this.activePmtOptionIndex].amount;
        // } else {
        //     this.pmtOptAmount = this.cartTotal;
        // }
    }


    convertToFloat(number) {
        return parseFloat(number);
    }
    setTotal(cartTotal, cost) {
        return cartTotal + parseFloat(cost);
    }
    setTaxTotal() {
        let txTotal=0;
        if(!this.selectedProduct.costIncludedTax&& this.selectedProduct.tax&&(this.selectedProduct.tax.sgst||this.selectedProduct.tax.cgst)){
            if(this.selectedProduct.tax.sgst&&this.selectedProduct.tax.sgst>0){
                txTotal+= (this.selectedProduct.cost* this.selectedProduct.tax.sgst)/100;
            }
            if(this.selectedProduct.tax.cgst&&this.selectedProduct.tax.cgst>0){
                txTotal+= (this.selectedProduct.cost* this.selectedProduct.tax.cgst)/100;
            }
        }
        return parseFloat(this.selectedProduct.cost)+ txTotal+this.taxTotal;
    }
}
