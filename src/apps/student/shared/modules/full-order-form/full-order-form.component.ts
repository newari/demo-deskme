import { Component, OnInit, Input, EventEmitter, Output, ElementRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import * as moment from 'moment';
import { User } from '../../../../../lib/models/user.model';
import { UserService } from '../../../../../lib/services/user.service';
import { AuthService } from '../../../../../lib/services/auth.service';
import { EdukitConfig } from '../../../../../ezukit.config';
import { Session } from '../../../../../lib/models/session.model';
import { Coption } from '../../../../../lib/models/coption.model';
import { Product } from '../../../../../lib/models/product.model';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';
import { Student } from '../../../../../lib/models/student.model';
import { SessionService } from '../../../../../lib/services/session.service';
import { CoptionService } from '../../../../../lib/services/coption.service';
import { CenterService } from '../../../../../lib/services/center.service';
import { ProductService } from '../../../../../lib/services/product.service';
import { StudentService } from '../../../../../lib/services/student.service';
import { OrderService } from '../../../../../lib/services/order.service';
import { Center } from '../../../../../lib/models/center.model';
import { OrderDataService } from '../../services/order-data.service';
import { ClientService } from '../../../../../lib/services/client.service';
import { BatchService } from '../../../../../lib/services/batch.service';
import { Order } from '../../../../../lib/models/order.model';

@Component({
    selector:'ek-full-order-form',
    templateUrl:'./full-order-form.component.html'
})
export class StudentFullOrderComponent implements OnInit{
    studentRegForm:FormGroup;
    userData:User;
    selectedUser:User;
    formStatus:string="Normal";
    resetPassword:boolean=false;
    centers:Center[];
    sessions:Session[];
    courses:Coption[];
    productTypes:Coption[];
    streams:Coption[];
    products:Product[];
    selectedProduct:Product;
    addedProducts:number[]=[];
    itemsForModal:any;
    cartTotal:number=0;
    pmtOptAmount:number=0;
    validationError:string;
    paymentMethod:string="";
    paymentOption:string="";
    fh:any={
        personalImg:'https://iesmaster.org/public/images/dummy-photo.png',
        signImg:'https://iesmaster.org/public/images/dummy-photo-sign.png',
        status:'Normal',
        error:null
    };
    activeStep:number=1;
    tandcAccepted:boolean;
    formData:any;
    orderData:any;
    user:User;
    
    selectedProductType:any;

    stdHavePastExams:boolean=false;
    isExStd:boolean=false;
    isCurrentStd:boolean=false;
    currentStdSrn:string;
    currentStdDoc:string;
    pastCrses:number[]=[0,1,2];
    paymentOptions:any[];
    activePmtOptionIndex:number;
    nonCouponDiscounts:any[];
    appliedDiscount:any;
    discountAmt:number;
    showModal:boolean;
    formCourses:any[];
    client:any;
    batchSeats:any;
    @Output() onStudentRegistered:EventEmitter<any>=new EventEmitter<any>();
    @Input() submitLabel:string='Save';
    @Input() student:Student;
    @Input() productCategory:string[];
    order:Order;
    
    constructor(
		private fb:FormBuilder,
		private userService:UserService,
        private notifier: NotifierService,
        private el: ElementRef,
        private sessionService:SessionService,
        private coptionService:CoptionService,
        private centerService:CenterService,
        private productService:ProductService,
        private studentService:StudentService,
        private orderService:OrderService,
        private authService:AuthService,
        private dataService:OrderDataService,
        private clientService:ClientService,
        private batchService: BatchService
	){ 
			
    }
    ngOnInit():void{
        this.user=this.authService.student();
        this.studentRegForm=this.fb.group({
            center:['', Validators.required],
            course:['', Validators.required],
            productType:['', Validators.required],
            stream:['', Validators.required],
            session:['', Validators.required],
            product:[''],
            batch:[''],
            batchSeat:[null],
            mode: [null],
            skypeId: [''],
            user:this.fb.group({
                firstName:['', Validators.required], 
                lastName:['', Validators.required],
                mobile:['', Validators.required],
                email:['', Validators.required],
                category:['', Validators.required],
                gender:['', Validators.required],
                dob:[null, Validators.required],
                personalImg:[''],
                signImg:[''],
                father:this.fb.group({
                    name:['', Validators.required],
                    mobile:['', Validators.required],
                }),
                mother:this.fb.group({
                    name:['', Validators.required]
                }),
                address:this.fb.group({
                    address:['', Validators.required],
                    landmark:['', Validators.required],
                    city:['', Validators.required],
                    state:['', Validators.required],
                    postalCode:['', Validators.required],
                    country:['India', Validators.required],
                }),
                permanantAddress:this.fb.group({
                    address:[''],
                    landmark:[''],
                    city:[''],
                    state:[''],
                    postalCode:[''],
                    country:['India']
                }),
                exStudent:this.fb.group({
                    srn:[''],
                    session:[''],
                    idProof:['']
                }),
                currentStudent: this.fb.group({
                    srn: [''],
                    session: [''],
                    idProof: ['']
                }),
                pastCourses:this.fb.array([
                    this.initPastCourseFrm("B.E./B.Tech.", true),
                    this.initPastCourseFrm("M.E./M.Tech."),
                    this.initPastCourseFrm("Other")
                ]),
                pastExams:this.fb.array([
                    this.initPastExamFrm("GATE"),
                    this.initPastExamFrm("ESE")
                ])
            }),
            customParams: this.fb.group({
                currentESE: this.fb.group({
                    session: ['2018'],
                    rollNo: [''],
                    eseAdmitCard: [''],
                    prelimsExpectedMarks:[''],
                    expectedPaperImarks: [''],
                    expectedPaperIImarks:['']
                }),
                
            }),
            cart:this.fb.group({
                discountCode:['']
            })
        });
        let self=this;
        window.setTimeout(function(){
            self.setInit();
        }, 1);
    }
    loadClient(){
        this.clientService.getClientConfig().subscribe(
            res=>{
                this.client=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
    }
    setInit(){
        this.loadClient();
        this.loadCenters();
        this.loadCourse();
        this.loadProductTypes();
        this.loadStreams();
        this.loadSessions();
        this.loadNonCouponDiscounts();
        this.setStudent();
        
    }

    loadNonCouponDiscounts(){
        this.dataService.getNonCouponDiscounts().subscribe(
            res=>{
                this.nonCouponDiscounts=res;
            },
            err=>{

            }
        )
    }

    loadCenters(){
        let filter:any={};
        if(this.centers){
            filter.id=this.centers;
        }
        
        this.dataService.getCenters(filter).subscribe(
            res=>{
                
                this.centers=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    loadSessions(){
        let filter:any={};
        // if(this.sessions){
        //     filter.id=this.sessions;
        // }
        let frmData=this.studentRegForm.value;
        if(frmData.center){
            filter.center=frmData.center;
            
        }
        // if(frmData.course){
        //     filter.course=frmData.course;
        // }
        // if(frmData.productType){
        //     filter.type=frmData.productType;
            
        // }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        console.log(filter);
        
        this.dataService.getSessions(filter).subscribe(
            res=>{
                this.sessions=res;
            },
            err=>{
                console.log(err);
            }
        );
    }

    loadStreams(){
        let filter:any={};
        let frmData=this.studentRegForm.value;
        if(frmData.center){
            filter.center=frmData.center;
        }
        if(frmData.session){
            filter.session=frmData.session;
        }
        // if(frmData.course){
        //     filter.course=frmData.course;
        // }
        // if(frmData.productType){
        //     filter.type=frmData.productType;
        // }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        this.dataService.getStreams(filter).subscribe(
            res=>{
                // if(this.streams){
                //     var self=this;
                //     res=res.filter(function(strm:any){
                //         for(let ci=0; ci<self.streams.length; ci++){
                //             if(strm.value==self.streams[ci]){
                //                 return true;
                //             }
                //         } 
                //         return false;
                //     })
                // }
                this.streams=res;
            },
            err=>{
                console.log(err);
            }
        );
    }

    loadProductTypes(){
        let filter:any={};
        let frmData=this.studentRegForm.value;
        if(frmData.center){
            filter.center=frmData.center;
        }
        if(frmData.session){
            filter.session=frmData.session
        }
        if(frmData.stream){
            filter.stream=frmData.stream;
        }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        this.dataService.getProductTypes(filter).subscribe(
            res=>{
                this.productTypes=res;
            },
            err=>{
                console.log(err);
            }
        );
    }

    loadProducts(){
        let frmData=this.studentRegForm.value;
        if(frmData.productType==""||frmData.stream==""||frmData.center==""||frmData.session==""){
            return;
        }
        let filter:any={
            center:frmData.center,
            // course:frmData.course,
            productType:frmData.productType,
            stream:frmData.stream,
            session:frmData.session,
            status:true
        }
        if(this.productCategory){
            filter.productCategory=this.productCategory;
        }
        this.dataService.getProducts(filter).subscribe(
            res=>{
                this.products=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    loadCourse(){
        let filter:any={};
        let frmData=this.studentRegForm.value;
        if(frmData.center){
            filter.center=frmData.center;
        }
        if(frmData.session){
            filter.session=frmData.session;
        }
        if(frmData.stream){
            filter.stream=frmData.stream;
        }
        // console.log(this.selectedProduct);
        // if(this.selectedProduct){
        //     filter.product=this.selectedProduct.id;
        // }
        if(frmData.productType){
            filter.type=frmData.productType;
        }
        
        this.dataService.getCourses(filter).subscribe(
            res=>{
                if(this.formCourses){
                    var self=this;
                    res=res.filter(function(crs:any){
                        for(let ci=0; ci<self.formCourses.length; ci++){
                            if(crs.value==self.formCourses[ci]){
                                return true;
                            }
                        } 
                        return false;
                    })
                }
                this.courses=res;
            },
            err=>{
                console.log(err);
            }
        );
    }
    
    
    
    checkProductDepParams(){
        let frmData=this.studentRegForm.value;
        if(frmData.center==""||frmData.session==""||frmData.productType==""||frmData.stream==""){
            alert("Please select Center, Session, Program & Stream First!");
            return;
        }
    }
    setProductDepParams(){
        if(this.studentRegForm.value.product){
            this.selectedProduct=this.products[this.studentRegForm.value.product];
            if(this.selectedProduct.batches){
                this.selectedProduct.batches=this.selectedProduct.batches.filter(function(el:any){
                    return el.status&&el.admissionStatus;
                });
            }
            // this.getPaymentOptions();
        }else{
            this.selectedProduct=null;
        }
    }
    unSelectProduct(){
        this.studentRegForm.patchValue({product:''});
        this.selectedProduct=null;
        this.addedProducts=[];
    }

    onCenterChanged(){
        this.loadSessions();
        this.studentRegForm.patchValue({session:''});
        this.unSelectProduct();
        
    }
    onSessionChanged(){
        this.loadStreams();
        this.studentRegForm.patchValue({stream:''});
        this.unSelectProduct();
    }

    onStreamChanged(){
        this.loadProductTypes();
        // this.loadSessions();
        this.studentRegForm.patchValue({productType:''});
        this.unSelectProduct();
    }

    onProgramTypeChanged(pTypeIndex:number){
        if(this.productTypes[pTypeIndex]){
            this.selectedProductType=this.productTypes[pTypeIndex];
            this.studentRegForm.patchValue({productType:this.selectedProductType.id});
        }else{
            this.selectedProductType=null;
        }
        
        this.unSelectProduct();
        this.loadProducts();
        
    }

    onProductChanged(productIndex:number){
        this.loadCourse();
        this.studentRegForm.patchValue({batch:'', course:''});
        if(this.products[productIndex]){
            this.addedProducts=[productIndex];
        }else{
            this.addedProducts=[];
        }
        this.setCartTotal();
        this.setProductDepParams();
    }

    onCourseChanged(){
        this.studentRegForm.patchValue({batch:''});
        this.setProductDepParams();
    }

    setStudent(){
        if(this.student){
            let stdPatchableData:any=this.getStudentPatchableStudent(this.student);
            this.studentRegForm.patchValue(stdPatchableData);
            return;
        }
        
        this.studentService.getSessionStudent().subscribe(
            res=>{
                this.student=res;
                let stdPatchableData:any=this.getStudentPatchableStudent(this.student);
                this.studentRegForm.patchValue(stdPatchableData);
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )

    }

    getStudentPatchableStudent(student:Student){
        let dob=null;
        if(student.dob){
            let dobArr=student.dob.split("/");
            if(dobArr[0]&&dobArr[1]&&dobArr[2]){
                dob=new Date(dobArr[1]+"/"+dobArr[0]+"/"+dobArr[2]);
            }
            
        }
        let data={
            center:student.center.id||student.center,
            session:student.session.id||student.session,
            course:student.majorCourse.id||student.majorCourse,
            stream:student.majorStream.id||student.majorStream,
            user:{
                firstName:student.firstName, 
                lastName:student.lastName,
                mobile:student.mobile,
                email:student.email,
                category:student.category,
                gender:student.gender,
                dob:dob,
                personalImg:student.personalImg,
                signImg:student.signImg,
                father:student.father,
                mother:student.mother,
                address:student.address,
                permanantAddress:student.permanantAddress,
                exStudent:student.exStudent||[],
                pastCourses:student.pastCourses||[],
                pastExams:student.pastExams||[]
            }
            
        };
        return data;
    }

    initPastCourseFrm(crsName:string, required?:boolean){
        return this.fb.group({
            course:[crsName, (required?Validators.required:null)],
            courseDuration:[''],
            college:['', (required?Validators.required:null)],
            year:['', (required?Validators.required:null)],
            marks:['', (required?Validators.required:null)],
            remarks:[''],
        })
    }

    initPastExamFrm(examName:string){
        return this.fb.group({
            name:[examName],
            year:['2016-17'],
            rank:[''],
            remarks:[''],
        })
    }
    
    
    
    setAddressSimilarity(el:any){
        if(el.target.checked){
            let corsAdrs=this.studentRegForm.value.user.address;
            this.studentRegForm.patchValue({user:{permanantAddress:corsAdrs}});
        }
    }

    getValue(arr:any[], id:string){
        if(!arr){
            return false;
        }
        let newArr = arr.filter(function(el){
            if(el.id==id){
                return true;
            }
            return false;
        });
        return newArr[0];
    }

    generatePassword(){
        // this.regForm.patchValue({password:Math.floor((Math.random() * 100000) + 1)});
    }

    setPasswordOpt(state){
        this.resetPassword=state;
    }

    setBillingAddress(sameAsShipping){
        if(sameAsShipping){
            // this.regForm.patchValue({address:{billing:this.regForm.value.address.shipping}});
        }else{
            // this.regForm.patchValue({address:{billing:((this.userData&&this.userData.address&&this.userData.address.billing)?this.userData.address.billing:{address:'', landmark:'', city:'', state:'', postalCode:'', country:'India'})}});
        }
    }

    addUpdateStudent(placeOrder?:boolean){
        let userData=this.studentRegForm.value.user;
        if(userData.personalImg==""||userData.signImg==""){
            this.fh.error="Personal Image and Sign Image are mandatory!";
            return;
        }
        if(!this.selectedProduct&&this.addedProducts.length<1){
            this.fh.error="Please select Program/Product first!";
            return;
        }
        userData.stream=this.studentRegForm.value.stream;
        userData.course=this.studentRegForm.value.course;
        userData.session=this.studentRegForm.value.session;
        userData.center=this.studentRegForm.value.center;
        userData.productCategory=this.selectedProduct.category;
        if(this.selectedProduct.haveBatch){
            if(!this.studentRegForm.value.batch||this.studentRegForm.value.batch==null){
                this.fh.error="Please select Batch first!";
                return;
            }
            userData.batch=this.studentRegForm.value.batch;
        }
        // if(!this.activePmtOptionIndex){
        //     this.validationError="show-error";
        //     this.fh.error="Please first select Payment Method and Option to proceed!";
        //     return;
        // }
        if(userData.dob){
            userData.dob=moment(userData.dob).format("DD/MM/YYYY");
        }
        
        let stdData:any={
            createLogin:true,
            student:userData,
            user:{
                password:Math.random()*100000|0
            }
        }
        this.fh.status="Processing";
        
        this.studentService.addStudent(stdData).subscribe(
            res=>{
                if(placeOrder){
                    this.placeOrder();
                }else{
                    this.fh.status="Normal";
                }
            },
            err=>{
                this.fh.status="Normal";
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
    }

    getPaymentOptions(){
        this.paymentOptions=[];
        if(!this.selectedProduct&&this.addedProducts.length>0){
            let pmtForProducts=[];
            for(let pfi=0; pfi<this.addedProducts.length; pfi++){
                pmtForProducts.push(pfi);
            }
            let storeId=this.products[this.addedProducts[0]].store.id||this.products[this.addedProducts[0]].store;
            this.paymentOptions.push({
                title:'Full Amount',
                amount:this.cartTotal,
                store:storeId,
                pmtForProducts:pmtForProducts
            });
            return;
        }
        if(!this.selectedProduct){
            return;
        }
        
        if(!this.selectedProduct.comboProducts||!this.selectedProduct.isCombo||this.selectedProduct.pmtOptAsComboItems!=true){
            this.paymentOptions.push({
                title:'Full Amount',
                amount:this.selectedProduct.cost,
                store:this.selectedProduct.store.id||this.selectedProduct.store,
                pmtForProducts:[0]
            });
            return;
        }
        let stores:any={};
        let storeType=0        
       
        for(let i=0; i<this.selectedProduct.comboProducts.length; i++){
            let storeId=this.selectedProduct.comboProducts[i].store;
            if(this.selectedProduct.comboProducts[i].paid==this.selectedProduct.comboProducts[i].total){
                continue;
            }
            this.paymentOptions.push({
                title:this.selectedProduct.comboProducts[i].title,
                amount:this.selectedProduct.comboProducts[i].total,
                store:storeId,
                pmtForProducts:[0, i]
            });
            if(!stores[storeId]){
                storeType++;
                stores[storeId]=[i];
            }else{
                
                stores[storeId].push(i);
                
                let cmbAmt=0;
                let cmbTitle="";
                for(let spi=0; spi<stores[storeId].length; spi++){
                    if(spi>0){
                        cmbTitle+=" + ";
                    }
                    cmbAmt+=parseFloat(this.selectedProduct.comboProducts[stores[storeId][spi]].total);
                    cmbTitle=cmbTitle+this.selectedProduct.comboProducts[stores[storeId][spi]].title;
                    
                }
                this.paymentOptions.push({
                    title:cmbTitle,
                    amount:cmbAmt,
                    store:storeId,
                    pmtForProducts:[0, stores[storeId]]
                });

            }
        }
        
        if(storeType===1){
            this.paymentOptions.push({
                title:'Full Amount',
                amount:this.selectedProduct.cost
            });
        }

    }
    setPaymentOption(optIndex){
        this.activePmtOptionIndex=optIndex;
    }

    addProductToCart(productIndex:number){
        if(this.products[productIndex].isCombo){
            this.addedProducts=[];
        }else if(this.addedProducts.length>0&&this.products[this.addedProducts[0]].isCombo){
            this.addedProducts=[];
        }
        this.addedProducts.push(productIndex);
        this.setCartTotal();
        // this.getPaymentOptions();
        
    }
    removeCartProduct(productIndex:number){
        let ind=this.addedProducts.indexOf(productIndex);
        if(ind>-1){
            this.addedProducts.splice(ind,1);
        }
        this.setCartTotal();
        // this.getPaymentOptions();
    }

    setCartTotal(){
        let ttl=0;
        for(let i=0; i<this.addedProducts.length; i++){
            ttl+=parseFloat(""+this.products[this.addedProducts[i]].cost+"");
        }
        
        this.setDiscount();
        this.cartTotal=ttl;
        
        if(this.paymentOptions&&this.activePmtOptionIndex>-1&&this.selectedProduct){
            this.pmtOptAmount=this.paymentOptions[this.activePmtOptionIndex].amount;
        }else{
            this.pmtOptAmount=this.cartTotal;
        }
    }

    addCouponDiscount(){

    }
    setDiscount(){
        let discountAmt=0;
         if(this.isCurrentStd){
             this.currentStdDoc = this.studentRegForm.value.user.currentStudent.idProof;
             this.currentStdSrn= this.studentRegForm.value.user.currentStudent.srn;
         }
        if(this.nonCouponDiscounts){
            for(let ci=0; ci<this.nonCouponDiscounts.length; ci++){
                let disc=this.nonCouponDiscounts[ci];
                if(disc.type=="CART_ITEM_COUNT"&&this.addedProducts.length>=disc.condition.minItemCount){
                    discountAmt=parseFloat(disc.discountAmount);
                    this.appliedDiscount={
                        id:disc.id,
                        discountType:'FIX',
                        discountAmount:discountAmt
                    };
                    break;
                }else if(disc.type=="EX_STUDENT"&&this.selectedProduct&&this.selectedProduct&&this.selectedProduct.discount&&this.selectedProduct.discount.exStudentDiscount&&this.isExStd){
                    discountAmt=parseFloat(this.selectedProduct.discount.exStudentDiscount);
                    this.appliedDiscount={
                        discountCode:'ExStudent',
                        discountType:'Fix',
                        discountAmount:this.selectedProduct.discount.exStudentDiscount,
                        reqDoc:this.studentRegForm.value.user.exStudent.idProof,
                        reqDocNo:this.studentRegForm.value.user.exStudent.srn
        
                    };
                    break;
                    
                }else if(disc.type=="CURRENT_STUDENT"&&this.selectedProduct&&this.selectedProduct.discount&&this.selectedProduct.discount.currentStudentDiscount&&this.isCurrentStd){
                    discountAmt=parseFloat(this.selectedProduct.discount.currentStudentDiscount);
                    this.appliedDiscount={
                        discountCode:'CurrentStudent',
                        discountType:'Fix',
                        discountAmount:this.selectedProduct.discount.currentStudentDiscount,
                        reqDoc:this.currentStdDoc,
                        reqDocNo:this.currentStdSrn
                    };
                    break;
                    
                }
            }
        }
        this.discountAmt=discountAmt;
    }

    placeOrder(){
        
        if(!this.user){
            this.validationError="show-error";
            this.fh.error="Something wrong, Please login again!";
            return;
        }
        if(!this.client){
            this.validationError="show-error";
            this.fh.error="Something wrong, Please refresh the page!";
            return;
        }
        let formData=this.studentRegForm.value;
        this.orderData={};
        
        // this.orderData.store=; //BOOKSTORE ID
        if(this.selectedProduct&&this.selectedProduct.store){
            this.orderData.store=this.selectedProduct.store.id||this.selectedProduct.store;
        }else if(this.addedProducts.length>0&&this.products[this.addedProducts[0]].store){
            this.orderData.store=this.products[this.addedProducts[0]].store.id||this.products[this.addedProducts[0]].store;
        }else{
            this.validationError="show-error";
            this.fh.error="This Program/Product is not available for Online Payment Option, Contact support!";
            return;
        }
        // if(!this.activePmtOptionIndex){
        //     this.validationError="show-error";
        //     this.fh.error="Please first select Payment Method and Option to proceed!";
        //     return;
        // }
        this.orderData.center=formData.center;
        this.orderData.course=formData.course;
        this.orderData.stream=formData.stream;
        this.orderData.session=formData.session;
        if(this.selectedProduct&&this.selectedProduct.haveBatch){
            this.orderData.batch=this.selectedProduct.batches[formData.batch].id; ;
        }
        // this.orderData.product=this.products[formData.product].id;
        // this.orderData.discountDetail={};
        // this.orderData.total=this.products[formData.product].cost;
        this.orderData.product=[];
        for(let api=0; api<this.addedProducts.length; api++){
            this.orderData.product.push(this.products[this.addedProducts[api]].id);
        }
        this.orderData.discountDetail={};
        this.orderData.total=this.cartTotal;
        this.orderData.isShipping=true;
        this.orderData.shippingAddress=formData.user.address;
        this.orderData.shippingAddress.name=formData.user.firstName+" "+formData.user.lastName;
        // this.orderData.paymentOptionIndex=this.activePmtOptionIndex;
        // this.orderData.pmtForProducts=this.paymentOptions[this.activePmtOptionIndex].pmtForProducts;
        let customParams = formData.customParams;
        
        if (this.batchSeats && this.batchSeats.length > 0) {
            this.orderData.customParams = {
                batchSeatTitle: this.batchSeats[formData.batchSeat].title,
                batchSeatId: this.batchSeats[formData.batchSeat].id,
                currentESESession: customParams.currentESE.session,
                currentESERollNo: customParams.currentESE.rollNo,
                ese2018AdmitCard: { val: customParams.currentESE.eseAdmitCard, type: 'url' },
                prelimsExpectedMarks: customParams.prelimsExpectedMarks,
                expectedPaperImarks: customParams.expectedPaperImarks,
                expectedPaperIImarks: customParams.expectedPaperIImarks,

            };
            if (this.selectedProduct && this.selectedProduct.customParam && this.selectedProduct.customParam.mode) {
                this.orderData.customParams.mockInterviewMode = 'online';
            } else {
                this.orderData.customParams.mockInterviewMode = formData.mode;
            }
            if (formData.skypeId) {
                this.orderData.customParams.skypeId = formData.skypeId;
            }
        }
        this.orderData.paymentMethod=this.paymentMethod;
        // let payableAmount=this.paymentOptions[this.activePmtOptionIndex].amount;
        // let payableAmount=parseFloat(this.paymentOptions[this.activePmtOptionIndex].amount);
        if(this.selectedProduct&&this.selectedProduct.discount&&this.selectedProduct.discount.exStudentDiscount&&this.isExStd){
            // payableAmount=payableAmount-parseFloat(this.selectedProduct.discount.exStudentDiscount);
            this.orderData.discountDetail={
                discountCode:'ExStudent',
                discountType:'Fix',
                discountAmount:this.selectedProduct.discount.exStudentDiscount,
                reqDoc:formData.user.exStudent.idProof,
                reqDocNo:formData.user.exStudent.srn

            };
        }else if(this.selectedProduct&&this.selectedProduct.discount&&this.selectedProduct.discount.currentStudentDiscount&&this.isCurrentStd){
            // payableAmount=payableAmount-parseFloat(this.selectedProduct.discount.currentStudentDiscount);
            this.orderData.discountDetail={
                discountCode:'CurrentStudent',
                discountType:'Fix',
                discountAmount:this.selectedProduct.discount.currentStudentDiscount,
                reqDoc:this.currentStdDoc,
                reqDocNo:this.currentStdSrn
            };
        }else if(this.appliedDiscount){
            // payableAmount=payableAmount-this.appliedDiscount.discountAmount;
            this.orderData.discountDetail=this.appliedDiscount;
        }
        this.fh.status="Processing";
        // let storeId=this.paymentOptions[this.activePmtOptionIndex].store;
        this.orderService.placeOrder(this.user, this.orderData).subscribe(
            res=>{
                // console.log(res);
                this.order=res;
                this.activeStep=4;
                // if(this.orderData.total<0.1){
                //     window.location.href=this.client.baseUrl+"/api/sales/order/online-free-payment-response/"+storeId+"?orderId="+res.id;
                // }else{
                //     window.location.href=this.client.baseUrl+"/api/sales/order/make-online-payment?storeId="+storeId+"&orderId="+res.id+"&orderNo="+res.orderNo+"&totalAmount="+payableAmount+"&redirectUrl="+this.client.baseUrl+"/api/sales/order/online-payment-response/"+storeId+"&cancelUrl="+this.client.baseUrl+"/api/sales/order/online-payment-cancel-response/"+storeId+"&responsePreviewUrl="+this.client.baseUrl+"/api/sales/order/online-payment-response/"+storeId;
                    
                // }
            },
            err=>{
                this.fh.status="Normal";
                this.validationError="show-error";
                this.fh.error=err.message;
                // console.log(err);
            }
        )
    }

    processOrderToStep2(){
        this.fh.error=null;
        this.validationError="";
        if(!this.selectedProduct&&this.addedProducts.length<1){
            this.validationError="show-error";
            this.fh.error="Please select valid program/product first!";
            return;
        }
        if(this.selectedProduct&&this.selectedProduct.haveBatch&&!this.studentRegForm.value.batch){
            this.validationError="show-error";
            this.fh.error="Please select batch first!";
            return;
        }
        if(!this.studentRegForm.valid){
            this.validationError="show-error";
            this.fh.error="Please fill all the mandatory fields.!";
            return;
        }
        if(!this.tandcAccepted){
            this.validationError="show-error";
            this.fh.error="Please accept the terms adn conditions first.!";
            return;
        }
        this.formData=this.studentRegForm.value;
        if(this.selectedProduct&&this.selectedProduct.discount&&this.selectedProduct.discount.exStudentDiscount&&this.isExStd){
            this.discountAmt=this.selectedProduct.discount.exStudentDiscount;
        }else if(this.selectedProduct&&this.selectedProduct.discount&&this.selectedProduct.discount.currentStudentDiscount&&this.isCurrentStd){
            this.discountAmt=this.selectedProduct.discount.currentStudentDiscount;

        }
        this.setCartTotal();
        this.activeStep=2;
    }
    processOrderToStep3(){
        this.activeStep=3;
        return; 
    }

    processOrder(){
        this.fh.error=null;
        this.validationError="";
       
        
        if(!this.studentRegForm.valid){
            this.validationError="show-error";
            this.fh.error="Please fill all the mandatory fields!!";
            return;
        }
        
        this.addUpdateStudent(true)
        
        
    }
    

    setTandC(el:any){
        if(el.target.checked){
            this.tandcAccepted=true;
        }else{
            this.tandcAccepted=false;
            
        }
    }
    showProductStrucutre(pi:number){
        this.itemsForModal=null;
        this.showModal=true;
        this.productService.getProductComboItems(this.products[pi].id).subscribe(
            res=>{
                this.itemsForModal=res;
            },
            err=>{
                console.log(err);
            }
        )
    }

    hideModal(){
        this.showModal=false;
    }
    loadBatchSeat(batchIndex: number) {
        let batch: any = {};
        this.studentRegForm.patchValue({ batchSeat: null, mode: null });
        batch = this.selectedProduct.batches[batchIndex];
        console.log(batch);
        if (!batch.seatWiseReg) {
            this.batchSeats = null;
            return;
        }
        this.batchService.getPerBatchSeat(batch.id).subscribe(
            (res) => {
                this.batchSeats = res;
                console.log(this.batchSeats);
            },
            (err) => {
                console.log(err);

            }
        );
    }
    
}