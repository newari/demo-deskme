import { Component, OnInit, ElementRef } from '@angular/core';

import { ForumQuestionService } from '../../services/forum-question.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Coption } from '../../../../../lib/models/coption.model';
import { Subject } from '../../../../../lib/models/subject.model';
import { Topic } from '../../../../../lib/models/topic.model';
import { CoptionService } from '../../../../../lib/services/coption.service';
import { SubjectService } from '../../../../../lib/services/subject.service';
import { TopicService } from '../../../../../lib/services/topic.service';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';
import { ClientService } from '../../../../../lib/services/client.service';
import { SettingsService } from '../../../../../lib/services/settings.service';
import { UserService } from '../../../../../lib/services/user.service';
import { Subjectunit } from 'lib/models/subjectunit.model';

@Component({
    selector:'ask-forum-question',
    templateUrl:'./ask-question.component.html'
})
export class AskForumQuestion implements OnInit{
    questionForm:FormGroup;
    formStatus:string="Normal";
    streams:Coption[];
    subjects:Subject[];
    units:Subjectunit[];
    topics:Topic[];
    fh:any={
        status:'Normal',
        file1:null,
        file2:null,
        file3:null,
        file1Status:'',
        file2Status:'',
        file3Status:'',
        error:null
    };
    panelLoader:string="none";
    client: any;
    isBatchAsThread:boolean;
    studentInfo:any;
    userProducts:any=[];
    constructor(
        private questionService:ForumQuestionService,
        private fb:FormBuilder,
        private coptionServise:CoptionService,
        private subjectService:SubjectService,
        private topicService:TopicService,
        private el: ElementRef,
        private authService:AuthService,
        private notifier:NotifierService,
        private clientService: ClientService,
        private settingService:SettingsService,
        private userService: UserService
    ){

    }
    ngOnInit(){
        this.client= this.clientService.getLocalClient();
        this.questionForm=this.fb.group({
            title:['', Validators.required],
            content:['', Validators.required],
            stream:[null],
            subject:[null],
            topic:[null],
            unit:[null],
            // group:[null, ],
            source:[null],
            bookPage:[""],
            bookQNumber:[""],
            bookTitle:[""],
            bookAuthor:[''],
            thread:[""],
            // product:[null]
        });
        // this.loadStreams();
        // this.getSetting();

        // this.getUserProducts();
        this.studentInfo=this.authService.studentInfo();
        this.loadSubjects(this.studentInfo.majorCourse, this.studentInfo.majorStream.id)
    }
    // getUserProducts() {
    //     this.userService.getUserProduct({productCategory:"5cf0cfc4e6636f4d815ccb0a"}).subscribe(
    //         res=>{
    //             this.userProducts=res;
    //         }
    //     );
    // }
    getSetting() {
        this.settingService.getSettingByKey("FORUM_DOUBT_LINK_TO_BATCH").subscribe(
            res=>{
                if(res&&res.value=='true'){
                    this.isBatchAsThread=true;
                    if(this.authService.student().type=='STUDENT'){
                        this.studentInfo=this.authService.studentInfo();
                        this.loadSubjects(this.studentInfo.majorCourse, this.studentInfo.majorStream)
                        if(this.studentInfo&&this.studentInfo.batches&&this.studentInfo.batches.length>0)
                        this.questionForm.patchValue({thread:this.studentInfo.batches[0].batch.id});
                    }
                }
            }
        );
    }

    loadStreams(){
        this.coptionServise.getCoption({option:'STREAM', status:true, parentOption:"forum"}).subscribe(
            res=>{
                this.streams=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
    }

    loadSubjects(course, stream){
        this.subjects=null;
        this.subjectService.getSubject({course:course, stream:stream, forForum:true}).subscribe(
            res=>{
                this.subjects=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
    }
    loadUnits(subject){
        this.units=null;
        this.subjectService.getUnit(subject).subscribe(
            res=>{
                this.units=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
    }

    loadTopics(subject){
        this.topics=null;
        this.topicService.getTopic({subject:subject}).subscribe(
            res=>{
                console.log(res);
                this.topics=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        );
    }

    submitQuestion(){
        let thisUser=this.authService.student();
        let questionData=this.questionForm.value;
        let question:any={};
        question.user=thisUser.id;
        question.status=true;
        question.uri=questionData.title.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
        question.attachments=[];
        question.title=questionData.title;
        question.content=questionData.content;
        question.stream=questionData.stream;
        question.subject=questionData.subject;
        question.topic=questionData.topic;
        // question.group=questionData.group;
        question.typeParams={};
        // question.typeParams.questionSource=questionData.source;
        // if(question.typeParams.questionSource=="Other"){
        //     question.typeParams.bookTitle=questionData.bookTitle;
        //     question.typeParams.bookAuthor=questionData.bookAuthor;
        // }else{
        //     question.typeParams.bookPage=questionData.bookPage;
        //     question.typeParams.bookQNumber=questionData.bookQNumber;
        // }

        question.typeParams.solStatus=false;

        if(this.fh.file1){
            question.attachments.push(this.fh.file1);
        }
        if(this.fh.file2){
            question.attachments.push(this.fh.file2);
        }
        if(this.fh.file3){
            question.attachments.push(this.fh.file3);
        }

        if(questionData.thread&&questionData.thread!==''){
            question.thread=questionData.thread;
        }
        this.panelLoader="show";
        this.formStatus="Processing";
        this.questionService.createQuestion(question).subscribe(
            res=>{
                this.questionForm.reset();
                this.fh.file1=null;
                this.fh.file2=null;
                this.fh.file3=null;
                this.panelLoader="none";
                this.formStatus="Normal";
                if(this.studentInfo&&this.studentInfo.batches&&this.studentInfo.batches.length>0)
                this.questionForm.patchValue({thread:this.studentInfo.batches[0].batch.id});
            },
            err=>{
                this.formStatus="Normal";
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )
    }

    upload(id:string) {
    	//locate the file element meant for the file upload.
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#'+id);
		console.log(inputEl);
    	//get the total amount of files attached to the file input.
        let fileCount: number = inputEl.files.length;
    	//create a new fromdata instance
        let formData = new FormData();
    	//check if the filecount is greater than zero, to be sure a file was selected.
        if (fileCount > 0) { // a file was selected
            //append the key name 'photo' with the first file in the element
			formData.append('ekFile', inputEl.files.item(0));
            //call the angular http method
			this.fh[id+"Status"]="Uploading...";
            this.questionService.uploadFile(formData).subscribe(
                res=>{
                    this.fh[id]=res.url;

                    this.fh[id+"Status"]="";
                    // document.getElementById(id).value="";
                },
                err=>{
                    console.log(err);
                    this.fh[id+"Status"]="Error in uploading, Try again!";
                    // document.getElementById(id).value="";
                }
            );


    	}
    }
}
