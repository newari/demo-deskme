import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentProfileCardComponent } from './student-profile-card.component';
import { StudentService } from '../../../../../lib/services/student.service';


@NgModule({
    declarations:[StudentProfileCardComponent],
    imports:[CommonModule],
    exports:[StudentProfileCardComponent],
    providers:[StudentService]
})
export class StudentProfileCardModule{

}