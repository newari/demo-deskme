import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../../../../../lib/services/user.service';
import { StudentOrderPaymentComponent } from './order-payment.component';
import { OrderService } from '../../../../../lib/services/order.service';
import { PaymentService } from '../../../../../lib/services/payment.service';
import { ProductService } from '../../../../../lib/services/product.service';
import { DialogModule } from 'primeng/dialog';


@NgModule({
    declarations:[StudentOrderPaymentComponent],
    imports:[CommonModule, FormsModule, ReactiveFormsModule, DialogModule],
    exports:[StudentOrderPaymentComponent],
    providers:[UserService, OrderService,PaymentService, ProductService]
})
export class StudentOrderPaymentModule{

}
