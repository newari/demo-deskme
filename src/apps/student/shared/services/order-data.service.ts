import { Injectable } from "@angular/core";

import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { EdukitConfig } from '../../../../ezukit.config';

@Injectable()
export class OrderDataService{
    constructor(
        private http:HttpClient
    ){

    }

    getBanners() : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/testment/webfront/banner");
    }


    getPageSections(pageName:string) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/testment/webfront/data/page-sections/"+pageName);
    }
    getProductPageSections(productUri:string) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/testment/webfront/data/product-page-sections/"+productUri);
    }

    getProductData(productUri:string) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/testment/webfront/data/product/"+productUri);
    }

    getSlider() : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/testment/webfront/banner");
    }
    getProduct() : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/book-store/webfront/data/products");
    }

    getProductFromTestment(productId:any): Observable<any>{
        return this.http.get(EdukitConfig.BASICS.TM_URL+"/product-data/"+productId);
    }

    // uploadPassportPhoto(data:any) : Observable<any>{
    //     return this.http.uploadFile(EdukitConfig.BASICS.TM_URL+"/upload-pp-photo", data);
    // }

    // uploadSignPhoto(data:any) : Observable<any>{
    //     return this.http.uploadFile(EdukitConfig.BASICS.TM_URL+"/upload-sign-photo", data);
    // }

    // registerOnEdument(data:any, pmtData:any) : Observable<any>{
    //     return this.http.edumentPost(EdukitConfig.BASICS.EM_URL+"/registration", {student:data, online_payment:pmtData});
    // }

    registerOnTstment(stdData:any) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.TM_URL+"/account/register-student", stdData);
    }

    registerOnEdukit(data:any) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/public/user/student", data);
    }

    placeOrder(user:any, order:any) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/public/order", {user:user, order:order});
    }

    updateStdOnTestment(stdData:any) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.TM_URL+"/account/update-student", stdData);
    }
    getCoupon(cpnCode:string) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.TM_URL+"/apply-cpn", {cpn_code:cpnCode});
    }

    // uploadCpnDocFile(data:any) : Observable<any>{
    //     return this.http.uploadFile(EdukitConfig.BASICS.TM_URL+"/upload-cpn-doc", data);
    // }

    // uploadFile(data:any) : Observable<any>{
    //     return this.http.uploadFile(EdukitConfig.BASICS.API_URL+"/public/file/upload", data);
    // }

    // enquiry(data:any) : Observable<any>{
    //     return this.http.edumentPost(EdukitConfig.BASICS.EM_URL+"/enquiry", data);
    // }

    getCenters(filter?:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/centers", {params:filter});
    }

    getCourses(filter?:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/courses", {params:filter});
    }
    getPrograms(filter?:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/programs", {params:filter});
    }
    getProductCategories(filter?:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/product-categories", {params:filter});
    }
    getProductTypes(filter?:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/product-types", {params:filter});
    }

    getStreams(filter?:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/streams", {params:filter});
    }
    getSessions(filter?:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/sessions", {params:filter});
    }
    getProducts(filter?:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/products", {params:filter});
    }
    getBatches(filter?:any) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/batches", {params:filter});
    }
    getProductComboItems(productId:string) : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/product/"+productId+"/combo-items");
    }
    getNonCouponDiscounts() : Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/public/data/non-coupon-discounts");
    }


}
