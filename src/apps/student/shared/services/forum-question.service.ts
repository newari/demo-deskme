import { Injectable } from "@angular/core";

import "rxjs";
import { Observable } from "rxjs";

import { EdukitConfig } from '../../../../ezukit.config';

import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ForumQuestionService{
    constructor(private http:HttpClient){ }


    getQuestion(filter?:any) :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/forum/question", {params:filter});
    }

    getOneQuestion(questionId) :Observable<any>{
        return this.http.get(EdukitConfig.BASICS.API_URL+"/forum/question/"+questionId);
    }

    createQuestion(question) :Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/forum/question", question);
    }
    uploadFile(data:any) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/public/file/upload", data);
    }

    addForumquestionReply(forumquestion:any) : Observable<any>{
        return this.http.post(EdukitConfig.BASICS.API_URL+"/forum/question/reply", forumquestion);
    }

    getForumquestionReply(questionId:string, filter?:any) :Observable<any[]>{
        if(!filter){
            var filter:any={};
        }
        filter.parentId=questionId;
        return this.http.get<any[]>(EdukitConfig.BASICS.API_URL+"/forum/question", {params:filter});
    }

    updateForumquestionReply(forumquestionId, forumquestion:any) : Observable<any>{

        return this.http.put(EdukitConfig.BASICS.API_URL+"/forum/question/reply/"+forumquestionId, forumquestion);
    }


}

