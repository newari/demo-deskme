import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CanActivate, CanActivateChild } from '@angular/router';
import { AuthService } from '../../../../lib/services/auth.service';
import { NotifierService } from '../../../../lib/components/notifier/notifier.service';
// Import our authentication service

@Injectable()
export class StudentAuthGuard implements CanActivate, CanActivateChild {

  constructor(private authService: AuthService, private router: Router, private notifier:NotifierService) {}

  canActivate() {
      console.log("ca");
    // this.notifier.start();
    // If user is not logged in we'll send them to the homepage

    if (!this.authService.loggedIn()) {
        this.notifier.alert('Permission denied!', "Login Again!", "danger", 100000);
        this.authService.logout();
        // this.router.navigate(['/student/auth/login']);
        return false;
    }

    var student=this.authService.student();
    if (student==null||student.type!="STUDENT") {
        this.notifier.alert('Permission denied!', "Login Again!", "danger", 100000);
        this.authService.logout();
        // this.router.navigate(['/student/auth/login']);
        return false;
    }
    return true;
  }

  canActivateChild() {
    // If user is not logged in we'll send them to the homepage
    if (!this.authService.loggedIn()) {
        this.authService.logout();

        this.router.navigate(['/student/auth/login']);
        return false;
    }
    var student=this.authService.student();
    if (student==null||student.type!="STUDENT") {
        this.authService.logout();

        this.router.navigate(['/student/auth/login']);
        return false;
    }

    return true;
  }

}
