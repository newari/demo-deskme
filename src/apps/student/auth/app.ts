import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';


import { StudentLoginActivity } from './activities/login/login.activity';
import { AuthService } from '../../../lib/services/auth.service';
import { AppActivity } from './app.activity';
import { APP_ROUTES } from './app.routes';
// import { AuthGuard } from '../../../lib/services/auth-guard.service';
import { NotifierModule } from "../../../lib/components/notifier/notifier.module";
import { HttpClientModule } from '@angular/common/http';
import { StudentAuthGuard } from '../shared/services/student-auth-guard';

@NgModule({
    declarations: [
        AppActivity,
    ],
    imports: [
        StudentLoginActivity,
        NotifierModule,
        HttpClientModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(APP_ROUTES)
    ],
    providers:[StudentAuthGuard, AuthService]
})
export class AppAuthModule { }
