import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector:'ek-auth',
    templateUrl: './app.activity.html'
})
export class AppActivity implements OnInit { 
    constructor(){
    }
    pageLoader='show';
    ngOnInit(){
      var _this=this;
      setTimeout(function(){
        _this.pageLoader='hide';
      }, 100)
      
      
    }

}
