import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
    templateUrl:'./register.component.html'
})
export class StudentRegisterContent implements OnInit { 
   loginWith: any;
    constructor(
        private activatedRoute:ActivatedRoute,
        private router: Router
    ){}

    ngOnInit(){
        this.activatedRoute.queryParams.subscribe(
            params=>{
               this.loginWith=params;
                
            }
        );
    }
}