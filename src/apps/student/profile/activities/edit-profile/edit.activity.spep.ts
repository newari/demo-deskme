import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentProfileEditComponent } from './edit.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UserService } from '../../../../../lib/services/user.service';
import { StudentAuthGuard } from '../../../shared/services/student-auth-guard';
import { StudentService } from '../../../../../lib/services/student.service';
import { ButtonModule } from 'primeng/button';

export const ROUTES:Routes=[
    {path: '', component: StudentProfileEditComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'My Profile'}},
];

@NgModule({
    declarations: [StudentProfileEditComponent],
    imports:[
        ButtonModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[StudentService, UserService]
})
export class StudentProfileEditActivity { }
