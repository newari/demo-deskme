import { Routes, CanActivateChild } from '@angular/router';

import { StudentProfileAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  {
    path: '',
    component:StudentProfileAppComponent,
    children: [
      	{ path: '', loadChildren: () => import('./activities/home/home.activity.sph').then(m => m.StudentProfileHomeActivity) },
      	{ path: 'edit', loadChildren: () => import('./activities/edit-profile/edit.activity.spep').then(m => m.StudentProfileEditActivity) },
    ]
  }
]
