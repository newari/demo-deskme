import { Routes, CanActivateChild } from '@angular/router';

import { ConventionalExamAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  { path: '',
    component: ConventionalExamAppComponent,
    children: [
      { path: '', loadChildren: './activities/dashboard/dashboard.activity.sced#StudentConventionalExamDashboardActivity' },
      { path: 'online-test', loadChildren: './activities/online-test/list.activity.sceonl#StudentConventionalExamOnlineTestActivity' },
      { path: 'offline-test', loadChildren: './activities/offline-test/list.activity.sceofl#StudentConventionalExamOfflineTestListActivity' }
      // { path: 'question', loadChildren: './activities/question/questions.activity.sfq#StudentForumQuestionsActivity' },
      // { path: 'question/ask', loadChildren: './activities/question/ask.activity.sfq#StudentForumQuestionAskActivity' },
      // { path: 'question/:questionId', loadChildren: './activities/question/question.activity.sfq#StudentForumQuestionActivity' },
    ]
  }
]
