import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";
import { ConventionalExamAppComponent } from './app.component';

@NgModule({
    declarations: [ConventionalExamAppComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[]
})
export class StudentConventionalExamAppModule { }
