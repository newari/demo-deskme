import { Routes, CanActivateChild } from '@angular/router';

import { ResourcesAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  { path: '',
    component:ResourcesAppComponent,
    children: [
      { path: 'downloads', loadChildren: './activities/downloads/home.activity.srd#StudentResourcesDownloadsActivity' },
      { path: 'downloads/:catId', loadChildren: './activities/downloads/downloads.activity.srd#StudentResourcesDownloadsListActivity' },
    ]
  }
]
