import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";
import { ResourcesAppComponent } from './app.component';

@NgModule({
    declarations: [ResourcesAppComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[]
})
export class StudentResourcesAppModule { }
