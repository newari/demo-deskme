import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { DownloadcategoryService } from '../../../../../lib/services/downloadcategory.service';
import { Downloadcategory } from '../../../../../lib/models/downloadcategory.model';
import { ActivatedRoute, Params } from '@angular/router';
import { Download } from '../../../../../lib/models/download.model';
import { DownloadService } from '../../../../../lib/services/download.service';
@Component({
	templateUrl: './downloads.component.html'
})
export class StudentDownloadsComponent {
	panelLoader: string;
	sessionStd: any;
	downloadCat: Downloadcategory;
	downloads:Download[];
	activeFilter: any;
	countBase: number;
	totalRecords: any;
	constructor(
		private authService: AuthService,
		private notifier: NotifierService,
		private dcService: DownloadcategoryService,
		private downloadService: DownloadService,
		private activatedRoute:ActivatedRoute
	) {

	}

	ngOnInit() {
		this.sessionStd = this.authService.student();
		this.activatedRoute.params.subscribe((params: Params) => {
	        let catId = params['catId'];
			this.loadDownloadCategory(catId);
			this.activeFilter={category:catId};
			
        });
		
	}
	loadDownloadCategory(catId) {
		this.dcService.getOneDownloadcategoryData(catId).subscribe(
			res => {
				this.downloadCat = res;
				if (res) {
					this.loadDownloads(this.activeFilter);
				}
			},
			err => {
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
	}
	loadDownloads(filter?:any) {
		if (!this.downloadCat){
			return;
		}
		this.downloadService.getDownload(filter, true).subscribe(
			res => {
				this.totalRecords = res.headers.get('totalRecords') || 0;
				this.downloads = res.body;
			},
			err => {
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
	}

	paginate(e) {
		if (!this.activeFilter) {
			this.activeFilter = {};
		}
		this.countBase = e.rows * e.page + 1;
		this.activeFilter.page = (e.page + 1);
		this.activeFilter.limit = e.rows;
		this.loadDownloads(this.activeFilter);
	}

}
