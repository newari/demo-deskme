import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { DownloadcategoryService } from '../../../../../lib/services/downloadcategory.service';
import { Downloadcategory } from '../../../../../lib/models/downloadcategory.model';
import { StudentService } from '../../../../../lib/services/student.service';
import { Student } from '../../../../../lib/models/student.model';
@Component({
	templateUrl: './home.component.html'
})
export class StudentDownloadsHomeComponent {
	panelLoader: string;
	sessionStd: any;
	downloadCats: Downloadcategory[];
	std:Student;
	constructor(
		private authService: AuthService,
		private notifier: NotifierService,
		private dcService: DownloadcategoryService,
        private studentService:StudentService
	) {

	}

	ngOnInit() {
		this.sessionStd = this.authService.student();
		this.loadStd();
		
	}
	loadStd(){
		this.studentService.getSessionStudent().subscribe(
			res=>{
				this.std=res;
				this.loadDownloadCategories();
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
	}
	loadDownloadCategories() {
		let filter: any = {};
		if (this.std&&this.std.majorStream) {
			filter.stream = this.std.majorStream.id ? this.std.majorStream.id : this.std.majorStream;
		}
		this.dcService.getStdDownloadCategory(filter).subscribe(
			res => {
				this.downloadCats = res;
			},
			err => {
				this.notifier.alert(err.code, err.message, "danger", 5000);
			}
		)
	}

}
