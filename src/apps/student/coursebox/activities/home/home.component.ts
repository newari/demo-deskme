import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { Router } from '@angular/router';
import { VideoService } from '../../../../../lib/services/video.service';

@Component({
  templateUrl: './home.component.html'
})
export class StudentCourseBoxHomeComponent {
    panelLoader:string;
		sessionStd:any;
		subscribedBooks:any[];
		lectures:any={
			'PD Session':[],
			'Current Affairs Session':[],
			'Technical Session':[]
		};
    constructor(
      	private authService: AuthService,
        private notifier: NotifierService,
				private router:Router,
				private videoService:VideoService
    ){
        
    }

	ngOnInit(){
		this.sessionStd=this.authService.student();
		this.loadVideoLecture();
	}
	playVideo(vidId, vidSrcId){
		if(!vidSrcId){
			alert("Video will be available soon!");
			return;
		}
		this.router.navigate(['/student/courses/1/lectures/'+vidId]);
	}
	loadVideoLecture(){
			this.videoService.getVideo({status:true, tags:'InterviewGuidance'}).subscribe(
				res=>{
					let vids={
						'PD Session':[],
						'Current Affairs Session':[],
						'Technical Session':[]
					};
					for(let i=0; i<res.length; i++){
							let vid=res[i];
							if(vid.customParams&&vid.customParams.group){
								vids[vid.customParams.group].push(vid);
							}
					}
					this.lectures=vids;
				},
				err=>{

				}
			)
	}
	

}
