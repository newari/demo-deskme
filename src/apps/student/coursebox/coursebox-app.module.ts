import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";
import { CourseBoxAppComponent } from './app.component';

@NgModule({
    declarations: [CourseBoxAppComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[]
})
export class StudentCourseBoxAppModule { }
