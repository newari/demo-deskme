import { Routes, CanActivateChild } from '@angular/router';

import { CourseBoxAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  { path: '',
    component:CourseBoxAppComponent,
    children: [
      { path: '', loadChildren: './activities/home/home.activity.scbh#StudentCourseBoxHomeActivity' },
      { path: ':courseId', loadChildren: './activities/course/course.activity.scbc#StudentCourseBoxCourseActivity' },
      { path: ':courseId/lectures/:lectureId', loadChildren: './activities/course/lecture.activity.scbl#StudentCourseBoxLectureActivity' },
    ]
  }
]
