import {NgModule} from "@angular/core";
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import {STUDENT_PANEL_ROUTES} from "./student-panel.routes";
import { StudentPanelComponent } from './student-panel.component';
import { StudentMainbodyModule } from './shared/components/mainbody/mainbody.module';
import { StudentSidebarModule } from './shared/components/sidebar/sidebar.module';
import { StudentHeaderModule } from './shared/components/header/header.module';
import { StudentFooterModule } from "./shared/components/footer/footer.module";
import { CoptionService } from '../../lib/services/coption.service';
import { NotifierModule } from "../../lib/components/notifier/notifier.module";
import { LoginboxModule } from '../../lib/components/loginbox/loginbox.module';
import { StudentAuthGuard } from "./shared/services/student-auth-guard";
import { ClientService } from '../../lib/services/client.service';
import {ButtonModule} from 'primeng/button';
import { DialogModule } from "primeng/dialog";

@NgModule({
    declarations:[
      StudentPanelComponent,
    ],
    imports:[StudentSidebarModule, DialogModule, ButtonModule, StudentHeaderModule, StudentFooterModule, StudentMainbodyModule,  NotifierModule, LoginboxModule, CommonModule, RouterModule.forChild(STUDENT_PANEL_ROUTES)],
    providers:[StudentAuthGuard,
      CoptionService, ClientService],
    bootstrap: [StudentPanelComponent]
})
export class StudentPanel { }
