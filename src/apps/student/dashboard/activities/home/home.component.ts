import { Component, ElementRef } from "@angular/core";
import { AuthService } from '../../../../../lib/services/auth.service';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { FilemanagerService } from "../../../../../lib/components/filemanager/filemanager.service";
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { FormService } from "../../../../../lib/services/form.service";
import { StudentPanelService } from "../../../../../lib/services/student-panel.service";
import { SettingsService } from "../../../../../lib/services/settings.service";
import { User } from "../../../../../lib/models/user.model";
import { DomSanitizer } from "@angular/platform-browser";
import { TestMentService } from "../../../../../lib/services/testment.service";
import { ProductService } from "../../../../../lib/services/product.service";
import { TestseriesService } from "../../../../../lib/services/testseries.service";
import { Product } from "../../../../../lib/models/product.model";
import { EventService } from "../../../../../lib/services/event.service";
import * as moment from 'moment';
import { StudentService } from "../../../../../lib/services/student.service";
import { NewsCategoryService } from "../../../../../lib/services/newscategory.service";
import { Student } from "../../../../../lib/models/student.model";
// const {shell} = require('electron');
@Component({
	templateUrl: './home.component.html'
})
export class StudentDashboardHomeComponent {
	
	images: any[] = [];
	mobileVarification: FormGroup;
	stdForm : FormGroup;
	otpVarification: FormGroup;
	panelLoader = 'none';
	showSuccess: boolean;
	showMobileNumberNeccessary: boolean;
	responseData: any;
	otpData;
	setting: any;
	std : any;
	user : any;
	haveOTSApp: boolean;
	overAllReportList: any[];
	upComingTests: any[];
	batches: any;
	activeBatch:any;
	notifications: any;
	sessionStudent:any;
	streams: any;
	displayStdUpdFrm:boolean;
	stdUpdateForm:FormGroup;
	formId: any;
	approvalStatus:string;
	showAlreadyUpdloaded: boolean; 
	displayStdModal : boolean;
	displayNotificationModal : boolean;
	notification : any;
	totalNotifications:number;
	activeFilter:any;
	days:any;
    // productCategories:any[]=[{id:'3030707264637463636c7370',title:"Classroom Program"},{id:'5b267b05851670048a6820a9', title:"Conventional Program"},{id:'5b47f7ef1e899567332d1d0a', title:"Online Test Series"},{id:'5b47f7ef1e899567332d1d0b',title:'Offline Quiz'},{id:'5cf0cfc4e6636f4d815ccb0a', title:"Video Course"}];
	// showProductCategories
	// rangeDates:Date[];
	// minDateValue:Date= new Date();
	// maxDateValue:any= new Date(moment().add(30,'days').calendar());
	step:number;
	nfLoader:string;
	totalRecords:number;
	noticeContent:any;
	displayNotice:boolean;
	pdfViewer:any={show:false, link:""};
	constructor(
		private authService: AuthService,
		private spService: StudentPanelService,
		private fmService: FilemanagerService,
		private fb: FormBuilder,
		private el: ElementRef,
		private notifier: NotifierService,
		private formService: FormService,
		private settingsService: SettingsService,
		private sanitizer: DomSanitizer,
		private testmentService: TestMentService,
		private studentService:StudentService,
		private productService: ProductService
	) {
		this.user = this.authService.student();
		// this.std= this.authService.studentInfo();
	}
	ngOnInit() {
		// if (!this.std) {
		// 	this.authService.logout();
		// }
		if (!this.user) {
			this.authService.logout();
		}
		// this.loadTSProduct(this.user.id);
		this.stdUpdateForm= this.fb.group({
			majorStream:[null,Validators.required],
			majorCourse:[null,Validators.required],
		})
		this.stdForm = this.fb.group({
			majorStream:[null,Validators.required],
			majorCourse:[null,Validators.required],
			category : ['',Validators.required],
			personalImg : ['',Validators.required],
			signImg : ['',Validators.required],
			dob : ['',Validators.required],
			gender : ['',Validators.required],
			"permanantAddress" : this.fb.group( {
				"address" : [''],
				"landmark" : [''],
				"city" : [''],
				"state" : [''],
				"postalCode" : [''],
				"country" : "India"
			}),
			"address" : this.fb.group( {
				"address" : ['',Validators.required],
				"landmark" : ['',Validators.required],
				"city" : ['',Validators.required],
				"state" : ['',Validators.required],
				"postalCode" : ['',Validators.required],
				"country" : "India"
			}),
			"exStudent" : this.fb.group({
				"srn" : [""],
				"session" : [""]
			}),			
			"father" : this.fb.group({
				"name" : ['',Validators.required],
				"mobile" : [''],
			}),
			"mother" : this.fb.group({
				"name" : ['',Validators.required]
			}),
			"pastCourses" : this.fb.array([  
				this.fb.group({
					"course" : ['',Validators.required],
					"courseDuration" : ['',Validators.required],
					"college" : ['',Validators.required],
					"year" : ['',Validators.required],
					"marks" : ['',Validators.required],
					"remarks" : ['',Validators.required]
				}), 
				this.fb.group({
					"course" : [''],
					"courseDuration" : [''],
					"college" : [''],
					"year" : [''],
					"marks" : [''],
					"remarks" : [''],
				}), 
				this.fb.group({
					"course" : [''],
					"courseDuration" : [''],
					"college" : [''],
					"year" : [''],
					"marks" : [''],
					"remarks" : [''],
				})
			]),
			"pastExams" : this.fb.array([ 
				this.fb.group({
					"name" : [""],
					"year" : [""],
					"rank" : [""],
					"remarks" : [""]
				}), 
				this.fb.group({
					"name" : [""],
					"year" : [""],
					"rank" : [""],
					"remarks" : [""]
				}) 
			]),
		});
		this.loadSessionStudent();
		
		
		
		this.mobileVarification = this.fb.group({
			mobile: ['', Validators.required]
		});
		this.otpVarification = this.fb.group({
			otp: ['', Validators.required]
		});

		this.loadSlider();
		this.loadNotifications();
		
		
	}
	
	loadSessionStudent() {
		 this.studentService.getSessionStudent().subscribe(
			 res=>{
				 this.sessionStudent=res;
				 window.localStorage.setItem("student", JSON.stringify(res));
				 this.std = res;
				 let filter={
					 stream: this.sessionStudent.majorStream?this.sessionStudent.majorStream.id:this.sessionStudent.majorStream,
					 course: this.sessionStudent.majorCourse||null,
					 session:this.sessionStudent.session||null,
					 centre:this.sessionStudent.center||null,
					 product:this.sessionStudent.products&&this.sessionStudent.products.length>0?this.sessionStudent.products:null,
					 batch:null,
					 limit:10
				 }

				 if(this.sessionStudent.batches&&this.sessionStudent.batches.length>0){
					filter.batch= this.sessionStudent.batches.map((batch)=>{
						 return batch.batch.id;
					 })
				 }
				 this.activeFilter=filter;
				 this.panelLoader='show';
				
				 if(this.std.profileStatus && this.std.profileStatus == "Initiated"){
					if(this.std.majorStream && this.std.majorStream.id){
						this.std.majorStream = this.std.majorStream.id;
						this.loadStreams();
					}
					if(this.std.majorCourse && this.std.majorCourse.id){
						this.std.majorCourse = this.std.majorCourse.id;
					}
					this.stdForm.patchValue(this.std);
					this.displayStdModal=true;
				 }else{
					if(this.sessionStudent&&!this.sessionStudent.majorStream){
						this.stdUpdateForm.patchValue({majorCourse:this.sessionStudent.majorCourse, majorStream:this.sessionStudent.majorStream?this.sessionStudent.majorStream.id:this.sessionStudent.majorStream});
					   this.displayStdUpdFrm=true;
					   this.loadStreams();
					}
				 }
				 
			 },err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			 }
		 )
	}
	loadStreams() {
		this.productService.getStreams().subscribe(
			res=>{
				this.streams=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		)
	}
	loadStudentBatches() {
		this.spService.getStudentBathces().subscribe(
			res=>{
				this.batches=res;
				let colors=[
					'#ea7128','#e62f2d','#55a9eb',"#65cae9",'#2c52cb','#7e45b1','#5ec612','#7f7f7f'
				]
				if(this.batches&&this.batches.length>0){
					this.activeBatch=this.batches[0].batch?this.batches[0].batch.id:'';
					for (let index = 0; index < 7; index++) {
						let date= new Date();
						this.days.push({
							title: moment(date).add(index,'day').format('dddd') ,
							from: moment(date).add(index,'day').startOf('day').format("YYYY-MM-DD , h:mm:ss a"),
							date: moment(date).add(index,'day'),
							to:moment(date).add(index,'day').endOf('day').format("YYYY-MM-DD , h:mm:ss a"),
							color:colors[index]
						})
					}
				}
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}

	loadSlider() {
		this.spService.getDashboardSlider().subscribe(
			res => {
				for (let i = 0; i < res.slides.length; i++) {
					this.images.push({
						source: res.slides[i].imgUrl,
						href:res.slides[i].href,
						alt: '',
						title: ''
					})
				}
			}
		)
	}

	uploadFile(cb) {
		//locate the file element meant for the file upload.
		let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#fmFile');
		//get the total amount of files attached to the file input.
		let fileCount: number = inputEl.files.length;
		//create a new fromdata instance
		// this.panelLoader="show";
		//check if the filecount is greater than zero, to be sure a file was selected.
		if (fileCount > 0) { // a file was selected
			let formData = new FormData();
			//append the key name 'photo' with the first file in the element
			formData.append('fmFile', inputEl.files.item(0));
			//call the angular http method
			this.fmService.uploadFile(formData).subscribe(
				res => {

					return cb(null, res);
				},
				err => {

					return cb(err);
					// this.notifier.alert(err.code, err.message, 'danger');
					// this.fh.upload1Status=err.message;

				}
			)

		}
	}

	sendOtp() {

		this.spService.sendOtp({ user: this.user.id }).subscribe(
			(res) => {
				this.otpData = res;
				this.notifier.alert("Successful", 'Sent Successfully!!', 'success', 500);
				this.panelLoader = "none";
				this.step = 2;
			},
			(err) => {
				this.notifier.alert(err.code, err.message, 'danger', 5000);
				this.panelLoader = "none";
			}
		);
	}

	varifyOTP() {
		if (!this.otpVarification.valid) {
			return;
		}
		let data: any = {};
		data.user = this.user.id;
		data.id = this.otpData.id;
		data.otp = this.otpVarification.value.otp;
		data.verifyOption = "mobile";
		this.spService.varifyOtp(data).subscribe(
			(res) => {
				if (res.success) {
					this.user.mobileVerified = true;
					let userStr = JSON.stringify(this.user);
					window.localStorage.setItem("user", userStr);
					this.notifier.alert("Successful", 'Mobile Number Varified Successfully!!', 'success', 500);
					this.panelLoader = "none";
					this.showMobileNumberNeccessary = false;

				} else {
					this.notifier.alert("ERROR", 'Something Wrong. Try Again!!', 'danger', 500);
				}

			},
			(err) => {
				this.notifier.alert(err.code, err.message, 'danger', 5000);
				this.panelLoader = "none";
			}
		);
	}
	// Returns safe URL
	getSafeUrl(videoUrl) {
		let self = this;
		return self.sanitizer.bypassSecurityTrustResourceUrl(videoUrl);
	}
	
	getUpComingTests() {
		let data = {
			user: this.user.id,
			limit: 5
		}
		this.testmentService.getUpComingTests(data).subscribe(
			res => {
				this.upComingTests = res;
			},
			err => { this.notifier.alert(err.code, err.message, "danger", 10000); }
		)
	}
	
	loadNotifications(filter?:any){
		this.notifications=[];
		this.nfLoader="show";
		this.studentService.getNoticeBoard(filter).subscribe(
			res=>{
				this.nfLoader="none"
				this.notifications= res;
			},
			err=>{
				this.nfLoader='none';
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		)
	}

	
	updateStd(){
		if(!this.stdUpdateForm.valid)
		return;
		this.studentService.updateStudent(this.sessionStudent.id, this.stdUpdateForm.value).subscribe(
			res=>{
				this.displayStdUpdFrm=false;
				this.stdUpdateForm.reset();
				this.loadSessionStudent();
				this.notifier.alert("Success", 'Successfully updated', 'success', 5000);
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}
	
	sameAddress(event){
		if(event.target.checked){
			let data=this.stdForm.value;
			this.stdForm.patchValue({permanantAddress : data.address});
		}else{
			this.stdForm.patchValue({
				permanantAddress : {
					"address" : '',
					"landmark" : '',
					"city" : '',
					"state" : '',
					"postalCode" : '',
				}
			});
		}
	}
	updateStudent(){
		this.stdForm.value.profileStatus='Completed';
		this.studentService.updateStudent(this.sessionStudent.id, this.stdForm.value).subscribe(
			res=>{
				this.displayStdModal=false;
				this.std.profileStatus='Completed';
				this.notifier.alert("Success", 'Successfully updated', 'success', 5000);
				this.loadSessionStudent();
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}

	sliderImgClikced(e){
		// console.log(e);
		if(e.href){
			window.location.href=e.href;
		}
	}

	openLink(url){
		window.require('electron').shell.openExternal(url);
	}

	showNoticeContent(i:number){
		if(!this.notifications[i]||!this.notifications[i].campaign||this.notifications[i].campaign.msg.contentType!="html"){
			return;
		}
		console.log("hello")
		
		this.displayNotice=true;
		this.noticeContent=this.notifications[i].campaign.msg.html;


	}

	showInappNotice(i:number){
		if(!this.notifications[i]||!this.notifications[i].campaign||this.notifications[i].campaign.msg.contentType!="inapp"){
			return;
		}

		if(this.notifications[i].campaign.msg.pdf){
			window.require('electron').shell.openExternal(this.notifications[i].campaign.msg.pdf);
		}
	}
	
}

