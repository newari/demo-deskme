import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentForumHomeComponent } from './home.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";

export const ROUTES:Routes=[
    {path: '', component: StudentForumHomeComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentForumHomeComponent],
    imports:[
        CommonModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[]
})
export class StudentForumHomeActivity { }