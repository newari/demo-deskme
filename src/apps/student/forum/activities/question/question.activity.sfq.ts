import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentForumQuestionComponent } from './question.component';
import { ForumQuestionService } from '../../../shared/services/forum-question.service';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { QuillEditorModule } from '../../../../../lib/components/ngx-quill-editor/quillEditor.module';

export const ROUTES:Routes=[
    {path: '', component: StudentForumQuestionComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Forum Questions'}},
];

@NgModule({
    declarations: [StudentForumQuestionComponent],
    imports:[
        QuillEditorModule,
        FormsModule, ReactiveFormsModule,
        CommonModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[ForumQuestionService]
})
export class StudentForumQuestionActivity { }