import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentForumQuestionsComponent } from './questions.component';
import { ForumQuestionService } from '../../../shared/services/forum-question.service';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";

export const ROUTES:Routes=[
    {path: '', component: StudentForumQuestionsComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Forum Questions'}},
];

@NgModule({
    declarations: [StudentForumQuestionsComponent],
    imports:[
        CommonModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[ForumQuestionService]
})
export class StudentForumQuestionsActivity { }