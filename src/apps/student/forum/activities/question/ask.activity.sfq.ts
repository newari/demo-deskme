import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { AskForumQuestionModule } from '../../../shared/modules/ask-question/ask-question.module';
import { StudentForumQuestionAskComponent } from './ask.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";

export const ROUTES:Routes=[
    {path: '', component: StudentForumQuestionAskComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Ask Question'}},
];

@NgModule({
    declarations: [StudentForumQuestionAskComponent],
    imports:[
        AskForumQuestionModule,
        CommonModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[]
})
export class StudentForumQuestionAskActivity { }