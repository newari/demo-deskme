import {Component} from "@angular/core";
import { AuthService } from '../../../../../lib/services/auth.service';
import { ForumQuestionService } from '../../../shared/services/forum-question.service';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';
import { ActivatedRoute } from "@angular/router";

@Component({
	templateUrl: './questions.component.html'
})
export class StudentForumQuestionsComponent {
    questions:any[];
    panelLoader:string="none";
    case:string;
    constructor(
        private questionService:ForumQuestionService,
        private authService:AuthService,
        private notifier:NotifierService,
        private activatedRoutes:ActivatedRoute
    ){
            
    }

    ngOnInit(){
        this.activatedRoutes.queryParams.subscribe(params => {
            this.case=params['case']||'my';
            this.loadQuestions(params['case'])
        });
        

    }
    loadQuestions(casee:any){
        this.panelLoader="show";
        let thisUser=this.authService.student();
        let fltr:any={user:thisUser.id}
        if(casee=="all"){
            fltr.isPublic=true;
        }else{
            fltr.myDoubts=true;
        }
        this.questionService.getQuestion(fltr).subscribe(
            res=>{
                this.panelLoader="none";
                this.questions=res;
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 10000);
            }
        )
    }
}
