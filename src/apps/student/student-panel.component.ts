import { Component, OnInit, OnChanges, SimpleChanges, AfterContentChecked } from '@angular/core';
import { Router } from '@angular/router';
import { NotifierService } from '../../lib/components/notifier/notifier.service';
import { ClientService } from '../../lib/services/client.service';
import { ClientConfig } from '../../lib/models/clientconfig.model';
import { AuthService } from '../../lib/services/auth.service';

@Component({
  selector: 'ezukit-student-panel',
  templateUrl: './student-panel.component.html',
  styleUrls: ['./student-panel.component.scss']
})
export class StudentPanelComponent implements OnInit, AfterContentChecked {
  pageLoader='show';
  client:ClientConfig;
  themeCss:string;
  theme:string;
  favicon:any;
  blocked:boolean;
  reason:string;
  student:any;
  updtPopup: boolean;
  appInfo: any;

  constructor(private notifier:NotifierService, private authService:AuthService, private router:Router, private clientService:ClientService){
    var _this=this;
    this.router.events.subscribe(event => {
      if (event.constructor.name === 'NavigationStart') {
        _this.pageLoader="show";
        _this.notifier.start();
        _this.notifier.navSpinner=true;
      }else if (event.constructor.name === 'RouteConfigLoadStart') {
        _this.notifier.progress+=20;
        // _this.notifier.completeProgressBar();
        // _this.notifier.navSpinner=false;
      }else if (event.constructor.name === 'RouteConfigLoadEnd') {

        _this.notifier.progress+=20;
        // _this.notifier.navSpinner=false;
      }else if (event.constructor.name === 'RoutesRecognized') {
        _this.notifier.progress+=20;
        // _this.notifier.completeProgressBar();
        // _this.notifier.navSpinner=false;
      }else if (event.constructor.name === 'NavigationEnd') {
        _this.pageLoader="hide";
        _this.notifier.completeProgressBar();
        _this.notifier.navSpinner=false;
      }
    });
  }
  ngOnInit(){
      this.notifier.clearAlert();
      var _this=this;
      setTimeout(function(){
        _this.pageLoader='hide';
      }, 100);
      this.loadClient();
      this.loadAppInfo();
      this.student=this.authService.user();

  }
  ngAfterContentChecked(): void {
    let std:any =  JSON.stringify(window.localStorage.getItem('student'));
    if(std&&std.isBlocked){
      this.blocked=std.isBlocked;
    }
  }
  loadClient(){
    this.clientService.getClientConfig().subscribe(
        res=>{
            this.client=res;
            console.log("Client", res);
            this.changeFavicon(this.client.favicon);
            // window.localStorage.getItem("authToken");
            let clientStr=JSON.stringify(res);
            window.localStorage.setItem("client", clientStr);

            if(this.client.studentZoneTheme){
                this.theme=this.client.studentZoneTheme;
              this.themeCss='/assets/css/ezukit-student-'+this.client.studentZoneTheme.toLowerCase()+'.css';
            }
        },
        err=>{
            this.notifier.alert(err.code, err.message, "danger", 1000);
        }
    )
  }

  changeFavicon (link?:any) {
    let $favicon = window.document.querySelector('link[rel="icon"]');
    if ($favicon !== null) {
      $favicon['href'] = link;
    } else {
      $favicon = window.document.createElement("link");
      $favicon['rel'] = "icon";
      $favicon['href'] = link;
      window.document.head.appendChild($favicon)
    }
  }
  logout(){
        this.authService.logout()
  }
  loadAppInfo() {
    this.clientService.getAppInfo().subscribe(
        (res) => {
            this.appInfo = res;
            let crntAppVersionNo = 100;
            if (res.vNumber && Number(res.vNumber) > crntAppVersionNo) {
                this.updtPopup = true;
            }
        },
        (err) => {
            console.log(err);
        }
    );
}
}
