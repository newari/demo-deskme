import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { OnlineCourseProductContent } from './product';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { FilemanagerService } from '../../../../../lib/components/filemanager/filemanager.service';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { StudentOrderPaymentModule } from "../../../shared/modules/order-payment/order-payment.module";
import { ProductService } from "../../../../../lib/services/product.service";
import { StudentService } from "../../../../../lib/services/student.service";
import { DialogModule } from 'primeng/dialog';
import { FieldsetModule } from 'primeng/fieldset';
export const ROUTES:Routes=[
    {path: '', component: OnlineCourseProductContent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [OnlineCourseProductContent],
    imports:[
        CommonModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        FieldsetModule,
        RouterModule.forChild(ROUTES),
        StudentOrderPaymentModule,
    ],
    providers: [FilemanagerService, ProductService,StudentService]
})
export class StudentOnlineCourseProductActivity { }
