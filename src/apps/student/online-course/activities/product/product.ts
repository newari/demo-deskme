import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../../../../../lib/services/product.service';
import { Product } from '../../../../../lib/models/product.model';
import { StudentService } from '../../../../../lib/services/student.service';
import { Student } from '../../../../../lib/models/student.model';

@Component({
  templateUrl: './product.html'
})
export class OnlineCourseProductContent{
	orderForm:FormGroup;
	order:any;
	panelLoader="none";
	productId;
	sessionStd;
	activeStep =1;
	products:Product[]
	student: Student;
    constructor(
      	private authService: AuthService,
        private notifier: NotifierService,
		private fb:FormBuilder,
		private studentService : StudentService,
		private productService:ProductService

    ){}
    ngOnInit(){
		this.orderForm=this.fb.group({
			course:[null],
			product:[null],
			stream:[null],
			session:[null],
			store:[null],
			total :[],
			tracer:[{form:'STUDENT-OC-ORDER-ADD'}],
			discountDetail : [{}],
		});
		// this.activatedRoute.params.subscribe(params=>{
		// 	this.productId = params['productId'];
		// });
		this.loadStudent();
		this.sessionStd=this.authService.student();
		
    }
	 
	 
	loadProducts(filter?:any){
		filter.populateCourse=true;
		this.productService.getProducts(filter).subscribe(
			res=>{
				this.products=res;
			}
		);
	}
	loadStudent(){
		this.studentService.getSessionStudent({populateUser:true, populateSession:true, populateCourse:true, populateBatch:true, populateProduct:true}).subscribe(
            res=>{
                this.panelLoader="none";
				this.student=res;
				let filter:any={
					course:res.majorCourse.id,
					stream:res.majorStream.id,
					category:'5cf0cfc4e6636f4d815ccb0a'
				}
				this.loadProducts(filter);
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
	}
	
}
