import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentOnlineCourseCourseComponent } from './course.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { ProductService } from "../../../../../lib/services/product.service";
import { StudentService } from "../../../../../lib/services/student.service";
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import {CalendarModule} from 'primeng/calendar';
import { FormsModule } from "@angular/forms";
export const ROUTES:Routes=[
    {path: '', component: StudentOnlineCourseCourseComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentOnlineCourseCourseComponent],
    imports:[
        CommonModule,
        FormsModule,
        DialogModule,
        RouterModule.forChild(ROUTES),
        ConfirmDialogModule,
        CalendarModule

    ],
    providers:[ProductService,StudentService]
})
export class StudentOnlineCourseActivity { }
