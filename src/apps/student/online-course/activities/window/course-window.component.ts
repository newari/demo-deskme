import { Component, Renderer2 } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { SlideUpDownAnimation } from '../../../../../lib/animations/slide-updown';
import { CourseCurriculumService } from '../../../../../lib/services/coursecurriculum.service';
import { ProductService } from '../../../../../lib/services/product.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { LessonService } from '../../../../../lib/services/lesson.service';
import { DomSanitizer } from '@angular/platform-browser';
import { QsetService } from '../../../../../lib/services/qset.service';
import { ClientService } from '../../../../../lib/services/client.service';
import { CommonFeedbackService } from '../../../../../lib/services/common-feedback.service';
import { EdukitConfig } from '../../../../../ezukit.config';
import * as moment from 'moment';
import Player from '@vimeo/player';
try {
    var macaddress = window.require('macaddress');
} catch (error) {
    console.log(error)
}

@Component({
    templateUrl: './course-window.component.html',
    animations: [SlideUpDownAnimation]
})
export class StudentOnlineCourseWindowComponent{
    panelLoader: string;
    sessionStd: any;
    sessionStdInfo:any;
    chapters: any[];
    activeLesson: any;
    activeChapter: any;
    activeLessonIndex: number;
    activeChapterIndex: number;
    subjects: any[];
    productId: any;
    product: any;
    loadChapterForm: FormGroup;
    activeSubject: any;
    activeSubjectIndex: number;
    lessonContent: any;
    activeClass = '';
    userProduct: any;
    activeLessonFilter: any;
    isReadyToAttemptQuiz: boolean;
    quiz: any;
    quizResponse: any;
    activeQuestion: any;
    activeQuestionIndex: number;
    previousQsIndex: number;
    nextQsIndex: number;
    questions: any;
    questionResponseForm: FormGroup;
    responseList: any = [];
    qsetqsIds: any = [];
    submitted: boolean = true;
    activeResponse: any;
    responses: any;
    viewSolution: boolean;
    activeResponseIndex: number;
    report: any = {
        totalCorrect: 0,
        totalInCorrect: 0
    };
    activeSolutionIndex: number;
    displaySideBar: boolean;
    leftbarState: string="open";
    client:any;
    localClient:any;
    blockVideoCourse:boolean;
    assignmentResponse: string;
    userLesson: any;
    feedBackQs: any;
    feedBackResponseForm:FormGroup;
    feedbackResponses:any=[];
    feedbackQuesitons: any[];
    feedBackResponseSubmitted: boolean;
    player:any;
    currentTime:0;
    lessonStats:any={
        isCompleted:false,
        duration:0,
        currentTime:0
    };
    totalDuration=0;
    myInterval:any=null;
    totalWatchedTime=0;
    accessKey:any;
    isCounted:boolean;
    viewLimit:number=0;
    confirmationHeading: string;
    acceptVisible:boolean;
    msgs:any;
    liveTimer:any={secs:0};
    intrvl:any;
    apiUrl:string;
    pdfViewer:any={show:false, link:""};
    curriculums:any[]=[];
    activeCurriculumId:string;
    frmPage:string;
    constructor(
        private authService: AuthService,
        private notifier: NotifierService,
        private router: Router,
        private renderer: Renderer2,
        private courseCurriculumService: CourseCurriculumService,
        private productService: ProductService,
        private activatedRoutes: ActivatedRoute,
        private fb: FormBuilder,
        private lessonService: LessonService,
        private sanitizer: DomSanitizer,
        private quizService: QsetService,
        private clientService: ClientService,
        private commonFeedBackService: CommonFeedbackService,
    ) {

    }
    removeShare(videoUrl){
        let videContent = window.document.getElementById('videocontent');
        // console.log(videContent);
        let cssClass=window.document.querySelectorAll('.ytp-chrome-top-buttons');
        if(cssClass&&cssClass[0]){
            // console.log(cssClass[0]);
            cssClass[0].remove();
        }
        return videoUrl;
    }
    ngOnInit() {
        this.sessionStd = this.authService.student();
        this.sessionStdInfo = this.authService.studentInfo();
        // this.chapters=[0, 1,2];
        this.activeLesson = { activeContentType: 'theory' };
        let localClient:any= this.clientService.getLocalClient();
        this.apiUrl=EdukitConfig.BASICS.API_URL;

        // if(localClient&&(localClient.id=='5ce3ed37f32253256831eaac')){
        //     this.blockVideoCourse=true;
        //     this.client=localClient;
        //     return;
        // }
        this.geClient();
        this.activatedRoutes.params.subscribe((params: Params) => {
            this.productId = params['productId'];

            this.loadProduct(this.productId);
            this.setCurriculumList(this.productId);
        });
        this.activatedRoutes.queryParams.subscribe((params: Params) => {
            this.userProduct = params['up'];
            this.frmPage = params['frm'];
            this.activeLessonFilter = {
                lesson: params['lesson'],
                subject: params['subject'],
                chapter: params['chapter'],
                curriculum: params['curriculum']
            }
        })
        this.loadChapterForm = this.fb.group({
            subject: ['']
        });
        this.questionResponseForm = this.fb.group({
            question: [''],
            answer: [''],
            qsetQs: [''],
            qType: [''],
            status: [''],
            user: [''],
            isSubmitted:[false],
            isCorrect:[false],
            timeTaken:[0]
        });

        this.feedBackResponseForm=this.fb.group({
            // feedbackQs:[''],
            // response:['', Validators.required],
            // chosenOption:['', Validators.required],
            // user:this.sessionStd.id,
            // more:this.fb.group({
            //     lesson:this.activeLesson._id
            // })
            responses:this.fb.array([])
        });
    }

    getResponseForm(res): any {
        let responseGroup = this.feedBackResponseForm.get("responses") as FormArray;
        for (let index = 0; index < res.length; index++) {
            responseGroup.push(this.getResponseFormGroup(res[index].id));
        }
    }
    getResponseFormGroup(id):FormGroup{
        return this.fb.group({
            feedbackQs:[id],
            response:['', Validators.required],
            chosenOption:['', Validators.required],
            user:this.sessionStd.id,
            client:this.sessionStd.client,
            more:this.fb.group({
                lesson:this.activeLesson._id
            })
        })
    }
    geClient() {
        this.clientService.getClientConfig().subscribe(
            res=>{
                this.client=res;
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    loadProduct(productId: any) {
        this.productService.getOneProduct(productId, {checkAccess:true}).subscribe(
            res => {
                this.product = res;
                if (res.sourceId && res.sourceId.length > 0) {
                    this.activeCurriculumId=res.sourceId[0];
                    // this.loadProductSubjectWiseCurriculums(res.sourceId[0]);
                    this.loadCurriculumSubjects(res.sourceId[0]);
                }
                // if(!this.product.allowAccessOnWeb){
                //     let message='Sorry this Course is not accessible on web.';
                //     // if(this.client.androidAppUrl){
                //     //     message=message+`<br>If you want to access this course please <a class="btn btn-success text-white" target="new" href="${this.client.androidAppUrl}">Install Now</a> our android app.`
                //     // }

                // }else{
                //     if (res.sourceId && res.sourceId.length > 0) {
                //         this.loadProductSubjectWiseCurriculums(res.sourceId);
                //     }
                // }

            },
            err => {
                this.notifier.alert(err.code, err.message, 'danger')
            }
        );
    }
    loadCurriculumSubjects(currId:string){
        this.panelLoader="show";
        this.courseCurriculumService.getCurriculumSubjects(currId).subscribe(
            res=>{
                this.panelLoader="none";
                this.subjects = res;
                this.activeSubjectIndex = 0;
                this.loadChapterForm.patchValue({ subject: this.activeSubjectIndex })
                this.loadLessons(this.activeSubjectIndex);
            },
            err=>{
                this.panelLoader="none";
            }
        )
    }
    loadProductSubjectWiseCurriculums(sourceId: any) {
        
        this.panelLoader="show";
        let fltr:any={ curriculums: sourceId, batch:this.sessionStdInfo.mainBatch }
        this.courseCurriculumService.getSubjectwiseCurriculums(fltr).subscribe(
            res => {
                this.panelLoader="none";
                if (res.length < 1) {
                    this.notifier.alert('ERROR', "No Subject and Lessons available!!", 'danger', 5000);
                    return;
                }
                this.subjects = res;
                this.activeSubjectIndex = 0;
                if (this.activeLessonFilter && this.activeLessonFilter.subject) {
                    for (let index = 0; index < this.subjects.length; index++) {
                        if (this.subjects[index] && this.subjects[index]._id && this.subjects[index]._id.subject && this.subjects[index]._id.subject._id && this.subjects[index]._id.subject._id == this.activeLessonFilter.subject && this.subjects[index]._id.curriculum == this.activeLessonFilter.curriculum) {
                            this.activeSubjectIndex = index;
                            this.loadChapterForm.patchValue({ subject: this.activeSubjectIndex })
                            this.loadLessons(this.activeSubjectIndex);
                            break;
                        }
                    }
                } else {
                    this.loadChapterForm.patchValue({ subject: this.activeSubjectIndex })
                    this.loadLessons(this.activeSubjectIndex);
                }

            },
            err => {
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    // openLesson(i){
    //   this.activeLesson={index:i, id:'id'+i, title:'Demo - '+i, activeContentType:'theory'};
    // }
    
    selectSubject(i){
        // this.colapsible.state=!this.colapsible.state;

        this.loadLessons(i);
    }

    selectChapter(subjectIndex:number, chapterIndex:number){
        this.activeChapterIndex=chapterIndex;
        this.activeChapter=this.chapters[chapterIndex];
        if(this.activeChapter.lessons&&this.activeChapter.lessons[0]){
            this.loadLessonContent(0);
            this.toggleLeftBar();
        }

    }

    loadLessons(subIndex) {
        if (subIndex == 'null') return;
        this.activeSubjectIndex = subIndex;
        this.activeSubject = this.subjects[subIndex];
        this.panelLoader = "show";
        if (!this.activeSubject) {
            this.panelLoader = "none";
            return;
        }
        let subject = this.activeSubject._id.id;
        let curriculum = this.activeCurriculumId;
        this.chapters = [];
        this.activeChapterIndex = -1;
        this.activeLessonIndex = -1;
        this.activeChapter = {};
        this.activeLesson = {};
        let fltr:any={ product: this.productId, batch:this.sessionStdInfo.mainBatch };
        this.lessonService.getUserLessons(subject, curriculum, fltr).subscribe(
            res => {
                this.panelLoader = "none";
                this.chapters = res;
                if (this.chapters && this.chapters.length > 0) {
                    if (this.activeLessonFilter.chapter && this.activeLessonFilter.lesson) {
                        // this.activeChapterIndex= this.chapters.indexOf(this.activeLessonFilter.chapter);
                        for (let index = 0; index < this.chapters.length; index++) {
                            if (this.chapters[index] && this.chapters[index]._id && this.chapters[index]._id.unit && this.chapters[index]._id.unit._id && this.chapters[index]._id.unit._id == this.activeLessonFilter.chapter) {
                                this.activeChapterIndex = index;
                                if (this.chapters[index].lessons && this.chapters[index].lessons.length > 0) {
                                    for (let lIndex = 0; lIndex < this.chapters[index].lessons.length; lIndex++) {
                                        if (this.chapters[index].lessons[lIndex] && this.chapters[index].lessons[lIndex]._id == this.activeLessonFilter.lesson) {
                                            this.activeLessonIndex = lIndex;
                                            this.loadLessonContent(this.activeLessonIndex)
                                            break;
                                        }
                                    }
                                }
                                break;
                            }
                        }
                    } else {
                        this.activeChapterIndex = 0;
                        this.activeLessonIndex = 0;
                        this.loadLessonContent(this.activeLessonIndex)
                    }
                }else{
                    this.notifier.alert("No Lessons!", "No lessons available under this section.", 'danger', 3000);
                }
            },
            err => {
                this.panelLoader = "none";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    async loadLessonContent(lessonIndex?: number) {
        // if(!this.activeChapter.lessons||!this.activeChapter.lessons[lessonIndex]){
        //     return;
        // }
        this.panelLoader = "none";
        this.activeLessonIndex = lessonIndex;
        this.activeChapter = this.chapters[this.activeChapterIndex];
        this.activeLesson = {};
        if (this.activeChapter && this.activeChapter.lessons && this.activeChapter.lessons.length > 0) {
            this.activeLesson = this.activeChapter.lessons[lessonIndex];
            this.activeLesson.activeContentType = 'theory';
        } else {
            return;
        }
        this.activeLesson.activeContentType = 'theory';
        // if(this.activeLesson&&this.activeLesson.type=='')
        if (this.userProduct == 'no' && this.activeLesson.type == 'paid') {
            this.panelLoader = 'none';
            return;
        }
        
        let fltr:any={batch:this.sessionStdInfo.mainBatch };
        // fltr.did=
        try {
            fltr.did=await macaddress.one();
        } catch (error) {
            console.log(error)
        }
        fltr.deviceType="DESKTOP";
        this.lessonService.getSingleLessonContent(this.activeLesson._id, this.productId, fltr).subscribe(
            res => {
                this.panelLoader = 'none';
                if (res&&res.lessonContent && res.lessonContent.blocks && res.lessonContent.blocks.length > 0) {
                    this.accessKey=res.accessKey;
                    this.activeLesson.hlsv=res.hlsv;
                    this.activeLesson.content = res.lessonContent.blocks;
                    if(res.lessonCompleted){
                        this.activeLesson.completeStatus=true;
                    }else{
                        this.activeLesson.completeStatus=false;
                    }
                    this.addOnLessonUsers();
                    for (let index = 0; index < this.activeLesson.content.length; index++) {
                        if (this.activeLesson.content[index].type == 'audio' || this.activeLesson.content[index].type == 'slides') {
                            this.activeLesson.content[index].content = this.getSafeURL(this.activeLesson.content[index].content);
                        } else if (this.activeLesson.content[index].type == 'video') {
                            if (this.activeLesson.content[index].source == 'YouTube') {
                                this.activeLesson.content[index].content = this.getSafeURL("https://youtube.com/embed/" + this.activeLesson.content[index].content);
                            }
                            if (this.activeLesson.content[index].source == 'vimeo') {
                                this.activeLesson.content[index].hlsPlayerUrl=this.getSafeURL(EdukitConfig.BASICS.API_URL+"/video-player/video/"+ this.activeLesson.content[index].content+"?uid="+this.sessionStd.id);
                                // this.activeLesson.content[index].content = this.getSafeURL("https://player.vimeo.com/video/" + this.activeLesson.content[index].content);
                                this.activeLesson.content[index].content = this.getSafeURL(EdukitConfig.BASICS.API_URL+"/public/video-player/lesson/" + this.activeLesson.content[index].content+'?accessKey='+res.accessKey+'&uid='+this.sessionStd.id);
                                // this.activeLesson.content[index].content=this.getSafeURL("http://localhost:1337/api/video-player/lesson/557554619?accessKey=abc");
                            }
                            if (this.activeLesson.content[index].source == 'Vidrize') {
                                this.activeLesson.content[index].content = this.getSafeURL(EdukitConfig.BASICS.API_URL+'/vidrize/player/'+this.activeLesson.content[index].content+'?autoplay=false&uid='+this.sessionStd.id);
                            }
                            console.log(this.activeLesson.content[index].content)
                        }else if(res.lessonContent.live&&this.activeLesson.publishDate){
                            this.activeLesson.live=res.lessonContent.live;
                            let lt=0;
                            if(moment(this.activeLesson.publishDate).isSameOrBefore(new Date())){
                                this.liveTimer.secs=0;
                            }else{
                                this.setLiveWaitingTimer(this.activeLesson.publishDate);
                            }
                        }
                    }
                } else {
                    this.panelLoader = 'none';
                    if(this.activeLesson.quiz){
                        this.activeLesson.activeContentType='quiz';
                        this.loadQuiz(this.activeLesson.quiz)
                    }else{
                        this.notifier.alert("Error", "Lesson Content", "danger", 5000);
                    }
                    
                }
            },
            err => {
                this.panelLoader = 'none';
                this.activeLesson.content = null;
                if(this.activeLesson.quiz){
                    this.activeLesson.activeContentType='quiz';
                    this.loadQuiz(this.activeLesson.quiz)
                }else{
                    this.notifier.alert(err.code, err.message, 'danger', 5000);
                }
                
                
            }
        );

    }
    addOnLessonUsers() {
        let lessonUser: any = {
            lesson: this.activeLesson._id,
            product: this.productId,
            status: "IN PROGRESS"
        }
        this.lessonService.addOnLessonUsers(lessonUser).subscribe(
            res => {
                if(this.activeLesson.viewLimit&&res.viewsCount>=this.activeLesson.viewLimit){
                    this.activeLessonIndex+=1;
                    this.loadLessonContent(this.activeLessonIndex);
                }else{
                    this.setLessonViews(lessonUser,res.currentTime);
                }
            },
            err => {
                this.notifier.alert(err.code, err.mesage, 'danger', 5000);
            }
        );
    }


    getSafeURL(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }

    lessonCompletedAndLoadNext() {
        let data: any = {
            lesson: this.activeLesson._id,
            product: this.productId
        };

        this.lessonService.updateLessonUsers(data).subscribe(
            res => {
                this.activeLessonIndex=this.activeLessonIndex+1;

                this.activeLesson = this.activeChapter.lessons[this.activeLessonIndex];
                this.loadLessonContent(this.activeLessonIndex);
            },
            err => {
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    selectConentType(cType:string){
        this.activeLesson.activeContentType=cType;
        if(cType=="quiz"&&this.activeLesson.quiz){
            this.loadQuiz(this.activeLesson.quiz)
        }
    }
    loadQuiz(quizId) {
        this.quiz = {};
        this.panelLoader = "show";
        this.quizService.getOneQset(quizId).subscribe(
            res => {
                this.quiz = res;
                this.questions = [];
                this.responses = null;
                this.activeQuestion = {};
                this.activeQuestionIndex = null;
                this.panelLoader = "none";
                this.report = {
                    totalCorrect: 0,
                    totalInCorrect: 0
                };
                this.checkIsQuizSubmitted(this.quiz.id);
            },
            err => {
                console.log(err);

                // this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    checkIsQuizSubmitted(quizId) {
        this.quizService.getOneQuizReport({qset:quizId,user:this.sessionStd.id}).subscribe(
            res=>{
                if(!res){
                    this.isReadyToAttemptQuiz = false;
                    this.responses = null;
                    this.submitted = false;
                    // this.loadQuizQuestions(this.quiz.id)
                }else{
                    this.responses=res;
                    this.submitted = true;
                }

            },
            err=>{
                console.log(err);
                // this.notifier.alert(err.code,err.mesage,'danger', 5000);
                this.isReadyToAttemptQuiz = false;
                this.responses = null;
                this.submitted = false;
                // this.loadQuizQuestions(this.quiz.id)
            }
        );
    }

        // this.quizService.checkIsQuizFinished({ qSet: quizId, user: this.sessionStd.id, limit: this.quiz.totalQs, sort: 'createdAt DESC' }).subscribe(
        //     res => {
        //         if (res.length > 0) {
        //             this.panelLoader = "show";
        //             this.responses = res;
        //             this.activeResponse = this.responses[0];
        //             this.activeResponseIndex = 0;
        //             this.loadQuestions(this.quiz)
        //         } else {
        //             this.isReadyToAttemptQuiz = false;
        //             this.responses = [];
        //             this.submitted = false;
        //             // this.loadQuizQuestions(this.quiz.id)
        //         }
        //     }
        // );
    // loadQuestions(quiz) {
    //     this.quizService.getQsetQuestion({ id: quiz.id, defaultLang:quiz.defaultLang }).subscribe(
    //         res => {
    //             this.questions = res[quiz.defaultLang];
    //             this.panelLoader = "none";
    //             if (this.questions && this.responses && this.responses.length > 0) {
    //                 this.calculateResult();
    //             }
    //         },
    //         err => {
    //             this.notifier.alert(err.code, err.message, 'danger', 5000);
    //             this.panelLoader = "none";
    //         }
    //     )
        // }
    // }


    loadQuizQuestions(quizId?: any) {
        this.isReadyToAttemptQuiz=true;
        this.quizService.getQuizData(quizId).subscribe(
            res => {
                if(res&&res.questions&&res.questions.EN){

                    this.questions = res['questions']['EN'];
                    this.activeQuestionIndex = 0;
                    this.activeQuestion = {};
                    this.activeQuestion = this.questions[this.activeQuestionIndex];
                    this.panelLoader = 'none';
                }else if(res&&res.questions&&res.questions.HI){

                    this.questions = res['questions']['HI'];
                    this.activeQuestionIndex = 0;
                    this.activeQuestion = {};
                    this.activeQuestion = this.questions[this.activeQuestionIndex];
                    this.panelLoader = 'none';
                }else{
                    this.notifier.alert("ERROR", "Something went wrong questions not found!", 'danger', 5000);
                }
            },
            err => {
                this.panelLoader = 'none';
                console.log(err);

                // this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        )
    }


    setActiveQuestion(index) {
        if (index == undefined || index === null){return;}
        if (this.activeQuestionIndex == index){return;}
        this.activeQuestionIndex = index;
        this.activeQuestion = this.questions[this.activeQuestionIndex];
        for (let index = 0; index < this.responseList.length; index++) {
            if(this.responseList[index].question==this.activeQuestion.question._id){
                this.questionResponseForm.patchValue({answer:this.responseList[index].answer});
                break;
            }else{
                this.questionResponseForm.reset();
            }
        }

    }

    saveAndNext() {
        if(!this.questionResponseForm.value.answer){
            window.alert("Please Choose a option to get next one!!");
            return;
        }

        if (this.questionResponseForm.value.answer !== ''||this.questionResponseForm.value.answer!==null||this.questionResponseForm.value.answer!=='null') {
            let response: any = {};
            response = this.questionResponseForm.value;
            response.user = this.sessionStd.id;
            response.question = this.activeQuestion.question._id;
            response.qsetQs = this.activeQuestion._id;
            response.qType = this.activeQuestion.question.qType;
            response.status = 'answered';
            response.client = this.quiz.client.id;
            response.qSet = this.quiz.id;
            if (this.qsetqsIds.indexOf(response.qsetQs) == -1) {
                this.responseList.push(response);
                this.qsetqsIds.push(response.qsetQs);
            }
            this.questionResponseForm.reset();
            this.setActiveQuestion(this.activeQuestionIndex + 1);
        } else {
            window.alert("Please Choose a option to get next one!!");
        }
    }

    submitAndFinish() {
        this.panelLoader="show";
        if(!this.questionResponseForm.value.answer){
            window.alert("Please Seclect your Answer befor submit");
            this.panelLoader="none";
            return;
        }
        let response: any = {};
        response = this.questionResponseForm.value;
        response.user = this.sessionStd.id;
        response.question = this.activeQuestion.question._id;
        response.qsetQs = this.activeQuestion._id;
        response.qType = this.activeQuestion.question.qType;
        response.status = 'answered';
        response.client = this.quiz.client.id;
        response.qSet = this.quiz.id;
        if (this.qsetqsIds.indexOf(response.qsetQs) == -1) {
            this.responseList.push(response);
            this.qsetqsIds.push(response.qsetQs);
        }
        this.questionResponseForm.reset();
        this.quizService.submitQuiz(this.quiz.id, this.sessionStd.id,this.responseList).subscribe(
            res => {
                this.notifier.alert("Success", "Successfully Submitted", 'success', 500);
                this.submitted = true;
                this.responses=res;
                this.panelLoader="none";
            },
            err => {
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }
    setSolution(index?: number) {
        this.activeSolutionIndex = index;
    }

    calculateResult() {
        for (let index = 0; index < this.responses.length; index++) {
            let response: any = this.responses[index];

            if (response.answer && response.question && response.answer == response.question.ans) {
                this.report.totalCorrect += 1;
            } else {
                this.report.totalInCorrect += 1;
            }
        }
    }
    toggleLeftBar() {
        if (this.leftbarState == "open") {
            this.leftbarState = "closed"
        } else {
            this.leftbarState = "open"
        }
    }
    getUserLesson(){
        this.lessonService.getSignleLessonOfUser(this.activeLesson._id, this.productId,).subscribe(
            res=>{
                this.userLesson=res;
            }
        );
    }
    addAssignmentResponse(){
        if(!this.assignmentResponse)
        return;
        let data={stdAssignmentUrl:this.assignmentResponse};
        this.lessonService.updateUserLesson(this.activeLesson._id, this.productId,data).subscribe(
            res=>{
                this.userLesson.stdResponseUrl=this.assignmentResponse
            }
        );
    }
    loadFeedBacks() {
		let filter={
			lesson:this.activeLesson._id,
			limit:50
		}
		this.commonFeedBackService.getFeedbackResponse(filter).subscribe(
			res=>{
                if(!res||(res.length<1)){
                    this.loadFeedBackQs();
                }else
                this.feedbackResponses=res;
			}
		);
	}
    loadFeedBackQs(){
        let filter:any={feedbackFor:"COURSE_LESSON"};
        this.commonFeedBackService.getFeedback(filter).subscribe(
            res=>{
                this.feedbackQuesitons=res;
                const control = <FormArray>this.feedBackResponseForm.controls['responses'];
                for(let i = control.length-1; i >= 0; i--) {
                    control.removeAt(i)
                }
                this.getResponseForm(res);
            },
            err=>{
                this.notifier.alert(err.code, err.message, 'danger', 5000);
            }
        );
    }

    addFeedbackQsResponse(){

    }

    submitFeedBackResponses(){
        // console.log(this.feedBackResponseForm.value);
        this.commonFeedBackService.addFeedbackResponse(this.feedBackResponseForm.value.responses).subscribe(
            res=>{
                this.feedBackResponseSubmitted=true;
            },
            err=>{

            }
        );
    }


    setLessonViews(lessonUser: any,currentTime?:number,totalWatchedTime?:number,) {
        return;
        // var iframe = document.querySelector('iframe');
        var iframe = document.querySelector('#videocontent');
        // console.log(iframe);
        if(!iframe){
            return;
        }
        this.player = new Player('videocontent');
        var currentTime=currentTime||0;
        this.lessonStats={
            isCompleted:false,
            duration:0,
            currentTime:0,
            totalWatchedTime:0
        };
        let self=this;
        self.player.getDuration().then(function(duration){
            self.totalDuration=duration;
            self.lessonStats.duration=duration;
        });
        self.getVideoCurrentTime();
    }
    getVideoCurrentTime(){
          this.myInterval=  setInterval(()=>{
              this.player.getCurrentTime().then((time)=>{

                  if(time!=this.currentTime){
                    this.currentTime=time;
                    this.totalWatchedTime+=time;
                    this.lessonStats.totalWatchedTime=this.totalWatchedTime;
                    if(this.totalDuration<=this.currentTime){
                        this.lessonStats.currentTime=this.currentTime;
                        this.lessonStats.isCompleted=true;
                        this.updateLessonViewData(this.lessonStats);
                        this.updateLessonStats(this.lessonStats);
                    }else if(this.totalDuration>this.currentTime){
                        this.lessonStats.currentTime=this.currentTime;
                        this.updateLessonViewData(this.lessonStats);
                        this.updateLessonStats(this.lessonStats);
                    }
                  }

              })
          },10000)
    }

    updateLessonViewData(data){
        this.lessonService.updateUserLessonViewData(this.accessKey,data).subscribe(
            res=>{
                if(res.isCompleted){
                    clearInterval(this.myInterval);
                }
            },
            err=>{
                this.notifier.alert(err.code, err.mesage, 'danger', 5000);
            }
        );
    }
    updateLessonStats(data: any) {
        if(this.isCounted){
            return;
        }
        let watched =parseFloat((data.currentTime*100/data.duration).toFixed(2));
        if(watched<75){
            return;
        }
        this.isCounted=true;
        data.productId=this.productId;
        this.lessonService.updateLessonViewsStats(this.activeLesson._id,data).subscribe(
            res=>{

            },
            err=>{

            }
        );
    }
    setLiveWaitingTimer(startTime){
        window.clearInterval(this.intrvl);
        this.liveTimer.secs=(moment(startTime).diff(moment(new Date()), "seconds"));
        if(this.liveTimer.secs>3600){
            return;
        }
        let _this=this;
        this.intrvl=window.setInterval(function(){
            _this.liveTimer.secs--;
            _this.liveTimer.mm=Math.ceil(_this.liveTimer.secs/60);
            _this.liveTimer.ss=_this.liveTimer.secs%60;
            if(_this.liveTimer.secs<0){
                window.clearInterval(_this.intrvl);
            }

        }, 1000)
    }
    launchHostUrl(srcType){
        console.log(this.activeLesson);
        // [href]="getSafeURL(apiUrl+'/course-creator/lesson-content/'+activeLesson._id+'/'+productId+'/j-live-class/'+activeLesson?.live?.id+'?accessKey='+accessKey)"
        document.cookie = this.activeLesson.live.meetingID+"="+this.accessKey;
        window.open(this.apiUrl+"/vive/live-class/"+this.productId+"/"+this.activeLesson._id+"/"+this.activeLesson.live.meetingID+"?accessKey="+this.accessKey, '_blank')
    }

    showPdf(pdfUrl:string){
        this.pdfViewer.link=this.getSafeURL('https://static.edkt.net/pdf-viewer?pdf='+pdfUrl);
        this.pdfViewer.show=true;
    }

    setCurriculumList(productId){

        this.courseCurriculumService.getProductCurriculum(productId).subscribe(
            res=>{
                this.curriculums=res.sourceId;
            },
            err=>{
                console.log(err);
            }
        );
    }

}
