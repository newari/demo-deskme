import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentOnlineCourseWindowComponent } from './course-window.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { CourseCurriculumService } from "../../../../../lib/services/coursecurriculum.service";
import { ProductService } from "../../../../../lib/services/product.service";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { LessonService } from "../../../../../lib/services/lesson.service";
import { QsetService } from "../../../../../lib/services/qset.service";
import { ForumThreadModule } from "../../../../../lib/components/forumthread/ft.module";
import { FileinputModule } from "../../../../../lib/components/filemanager/fileinput.module";
import { CommonFeedbackService } from "../../../../../lib/services/common-feedback.service";
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { SidebarModule } from 'primeng/sidebar';
import { AskForumQuestionModule } from '../../../shared/modules/ask-question/ask-question.module';

export const ROUTES:Routes=[
    {path: '', component: StudentOnlineCourseWindowComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentOnlineCourseWindowComponent],
    imports:[
        CommonModule,
        DialogModule,
        RouterModule.forChild(ROUTES),
        FormsModule,
        ReactiveFormsModule,
        AskForumQuestionModule,
        ForumThreadModule,
        FileinputModule,
        ConfirmDialogModule,
        SidebarModule
    ],
    providers:[CourseCurriculumService, ProductService,LessonService, QsetService, CommonFeedbackService]
})
export class StudentOnlineCourseWindowActivity { }
