import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { ProductService } from "../../../../../lib/services/product.service";
import { StudentService } from "../../../../../lib/services/student.service";
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import {CalendarModule} from 'primeng/calendar';
import { FormsModule } from "@angular/forms";
import { StudentOnlineCourseLibComponent } from "./lib.component";
export const ROUTES:Routes=[
    {path: '', component: StudentOnlineCourseLibComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentOnlineCourseLibComponent],
    imports:[
        CommonModule,
        FormsModule,
        DialogModule,
        RouterModule.forChild(ROUTES),
        ConfirmDialogModule,
        CalendarModule

    ],
    providers:[ProductService,StudentService]
})
export class StudentOnlineCourseLibActivity { }
