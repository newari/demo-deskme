import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { Router } from '@angular/router';
import { ProductService } from '../../../../../lib/services/product.service';
import { UserService } from '../../../../../lib/services/user.service';
import { StudentService } from '../../../../../lib/services/student.service';
import { Student } from '../../../../../lib/models/student.model';
import { ClientService } from '../../../../../lib/services/client.service';
import { LessonService } from '../../../../../lib/services/lesson.service';
import { DomSanitizer } from '@angular/platform-browser';
import { EdukitConfig } from '../../../../../ezukit.config';
import * as moment from 'moment';

import Player from '@vimeo/player';
try {
    var macaddress = window.require('macaddress');
} catch (error) {
    console.log(error)
}

@Component({
  templateUrl: './lib.component.html'
})
export class StudentOnlineCourseLibComponent {
	panelLoader:string;
	sessionStd:any;
	userProducts:any[];
	student: Student;
	freeProducts: any[];
	selectedProduct:any;
	msgs:any[];
	clientDetails:any
	confirmationHeading:string;
	acceptVisible: boolean;
	activeProduct:any;
	activeProductIndex:number;
	selectedDate:any=new Date();
	lessons:any[];
	activeLesson:any;
    constructor(
      	private authService: AuthService,
        private notifier: NotifierService,
		private router:Router,
		private productService:ProductService,
		private userService:UserService,
		private studentService:StudentService,
        private sanitizer: DomSanitizer,
		private lessonService:LessonService,
		private clientService: ClientService
    ){

    }

	async ngOnInit(){
		this.sessionStd=this.authService.student();
		this.clientDetails=this.clientService.getLocalClient();
		if(this.sessionStd){
			this.loadStudentProducts();
			this.loadStudent();

        }


	}
	loadStudent(){
		this.studentService.getSessionStudent({populateUser:true, populateSession:true, populateCourse:true, populateBatch:true, populateProduct:true}).subscribe(
            res=>{
                this.panelLoader="none";
				this.student=res;
				// this.loadFreeProducts();
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
	}

	loadStudentProducts(){
		let fltr:any={
            user:this.sessionStd.id,
            productCategory:'5cf0cfc4e6636f4d815ccb0a',
			productType:"609b4df80ac4df3a549bc785"
        }
        this.panelLoader="show";
		this.userService.getUserProduct(fltr).subscribe(
			res=>{
                this.panelLoader="none";
				this.userProducts=res;
			},
			err=>{
                this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}

	loadFreeProducts(){
		let filter:any={cost:'0',
			stream:this.student.majorStream.id,
			course:this.student.majorCourse.id,
			productCategory:'5cf0cfc4e6636f4d815ccb0a',
			// type:'5d0b5ce7d721ff179f779fc4'
		}
        this.userService.getFreeProductForUser(filter).subscribe(
			res=>{
                this.freeProducts=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}

	getValidity(date){
		if(!date){
			return false;
		}
		let currentDate= new Date();

		let validTill = moment(date).format();

		if(moment(validTill).isAfter(currentDate)){
			return false
		}else{
			return true
		}


	}

	confirm(i){

	}
	loadCourse(i){
		this.activeLesson=null;
		this.activeProductIndex=i;
		this.activeProduct=this.userProducts[i];
		if(!this.activeProduct.product||!this.activeProduct.product.sourceId||!this.activeProduct.product.sourceId[0]){
			return;
		}

		let currId=this.activeProduct.product.sourceId[0];
		let filter:any={};
		filter.curriculums=this.activeProduct.product.sourceId.join(",");
		filter.contentTypes="Video,Notes";
		this.panelLoader="show";
		this.lessonService.getDaywiseLesson(currId, filter).subscribe(
			res=>{
                this.lessons=res;
				this.panelLoader="none";

			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 3000);
			}
		);

	}
	loadProductList(){
		this.activeProduct=null;
	}

	async loadLessonContent(i){
		this.activeLesson=this.lessons[i];
		if(!this.activeLesson){
			return;
		}

		this.panelLoader="show";
		let fltr:any={};
        // fltr.did=
        try {
            fltr.did=await macaddress.one();
        } catch (error) {
            console.log(error)
        }
		this.lessonService.getSingleLessonContent(this.activeLesson.id, this.activeProduct.product.id, fltr).subscribe(
            res => {
                this.panelLoader = 'none';
                if (res&&res.lessonContent && res.lessonContent.blocks && res.lessonContent.blocks.length > 0) {
                    // this.accessKey=res.accessKey;
                    this.activeLesson.hlsv=res.hlsv;
                    this.activeLesson.content = res.lessonContent.blocks;
                    if(res.lessonCompleted){
                        this.activeLesson.completeStatus=true;
                    }else{
                        this.activeLesson.completeStatus=false;
                    }
                    // this.addOnLessonUsers();
                    for (let index = 0; index < this.activeLesson.content.length; index++) {
                        if (this.activeLesson.content[index].type == 'audio' || this.activeLesson.content[index].type == 'slides') {
                            this.activeLesson.content[index].content = this.getSafeURL(this.activeLesson.content[index].content);
                        } else if (this.activeLesson.content[index].type == 'video') {
                            if (this.activeLesson.content[index].source == 'YouTube') {
                                this.activeLesson.content[index].content = this.getSafeURL("https://youtube.com/embed/" + this.activeLesson.content[index].content);
                            }
                            if (this.activeLesson.content[index].source == 'vimeo') {
                                this.activeLesson.content[index].hlsPlayerUrl=this.getSafeURL(EdukitConfig.BASICS.API_URL+"/video-player/video/"+ this.activeLesson.content[index].content+"?uid="+this.sessionStd.id);
                                this.activeLesson.content[index].content = this.getSafeURL("https://player.vimeo.com/video/" + this.activeLesson.content[index].content);
                            }
                        }else if(res.lessonContent.live&&this.activeLesson.publishDate){
                            this.activeLesson.live=res.lessonContent.live;
                            let lt=0;
                            // if(moment(this.activeLesson.publishDate).isSameOrBefore(new Date())){
                            //     this.liveTimer.secs=0;
                            // }else{
                            //     this.setLiveWaitingTimer(this.activeLesson.publishDate);
                            // }
                        }
                    }
                } else {
                    this.panelLoader = 'none';
                    
                    
                }
            },
            err => {
                this.panelLoader = 'none';
                this.activeLesson.content = null;
                
                
                
            }
        );

	}
	getSafeURL(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
}
