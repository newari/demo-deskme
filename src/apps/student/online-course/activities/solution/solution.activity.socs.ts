import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentOnlineCourseSolutionComponent } from './solution.component';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { ProductService } from "../../../../../lib/services/product.service";
import { StudentService } from "../../../../../lib/services/student.service";
import { DialogModule } from 'primeng/dialog';
export const ROUTES:Routes=[
    {path: '', component: StudentOnlineCourseSolutionComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentOnlineCourseSolutionComponent],
    imports:[
        CommonModule,
        DialogModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[ProductService,StudentService]
})
export class StudentOnlineCourseSolutionActivity { }
