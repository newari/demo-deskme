import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { Router } from '@angular/router';

@Component({
  templateUrl: './home.component.html'
})
export class StudentOnlineCourseHomeComponent {
    panelLoader:string;
		sessionStd:any; 
    constructor(
      	private authService: AuthService,
        private notifier: NotifierService,
				private router:Router,
    ){
        
    }

	ngOnInit(){
		this.sessionStd=this.authService.student();
	}
	 
	

}
