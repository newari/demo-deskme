import { Component, ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { Router } from '@angular/router';
import { ProductService } from '../../../../../lib/services/product.service';
import { UserService } from '../../../../../lib/services/user.service';
import { StudentService } from '../../../../../lib/services/student.service';
import { Student } from '../../../../../lib/models/student.model';
import { ClientService } from '../../../../../lib/services/client.service';
import { LessonService } from '../../../../../lib/services/lesson.service';
import { DomSanitizer } from '@angular/platform-browser';
import { EdukitConfig } from '../../../../../ezukit.config';
import * as moment from 'moment';
import Player from '@vimeo/player';
try {
    var macaddress = window.require('macaddress');
} catch (error) {
    console.log(error)
}
// var macaddress;
@Component({
  templateUrl: './classes.component.html'
})
export class StudentOnlineCourseClassesComponent {
	panelLoader:string;
	sessionStd:any;
	userProducts:any[];
	student: Student;
	freeProducts: any[];
	selectedProduct:any;
	msgs:any[];
	clientDetails:any
	confirmationHeading:string;
	acceptVisible: boolean;
	activeProduct:any;
	activeProductIndex:number;
	selectedDate:any=new Date();
	lessons:any[];
	activeLesson:any;
	timers:any[]=[];
	livClss:any={open:false};
    constructor(
      	private authService: AuthService,
        private notifier: NotifierService,
		private router:Router,
		private productService:ProductService,
		private userService:UserService,
		private studentService:StudentService,
        private sanitizer: DomSanitizer,
		private lessonService:LessonService,
		private clientService: ClientService
    ){

    }

	async ngOnInit(){
		this.sessionStd=this.authService.student();
		this.clientDetails=this.clientService.getLocalClient();
		if(this.sessionStd){
			this.loadStudentProducts();
			this.loadStudent();

        }

		console.log("here before perms!");

		window['electron'].ipcRenderer.send('ask-perms');


	}
	loadStudent(){
		this.studentService.getSessionStudent({populateUser:true, populateSession:true, populateCourse:true, populateBatch:true, populateProduct:true}).subscribe(
            res=>{
                this.panelLoader="none";
				this.student=res;
				// this.loadFreeProducts();
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        )
	}

	loadStudentProducts(){
		let fltr:any={
            user:this.sessionStd.id,
            productCategory:'5cf0cfc4e6636f4d815ccb0a',
			productType:"5f6cae4019892277a5888764"
        }
        this.panelLoader="show";
		this.userService.getUserProduct(fltr).subscribe(
			res=>{
                this.panelLoader="none";
				this.userProducts=res;
			},
			err=>{
                this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}

	loadFreeProducts(){
		let filter:any={cost:'0',
			stream:this.student.majorStream.id,
			course:this.student.majorCourse.id,
			productCategory:'5cf0cfc4e6636f4d815ccb0a',
			// type:'5d0b5ce7d721ff179f779fc4'
		}
        this.userService.getFreeProductForUser(filter).subscribe(
			res=>{
                this.freeProducts=res;
			},
			err=>{
				this.notifier.alert(err.code, err.message, 'danger', 5000);
			}
		);
	}

	getValidity(date){
		if(!date){
			return false;
		}
		let currentDate= new Date();

		let validTill = moment(date).format();

		if(moment(validTill).isAfter(currentDate)){
			return false
		}else{
			return true
		}


	}

	confirm(i){

	}
	loadCourse(i, date?:any){
		this.activeLesson=null;
		this.activeProductIndex=i;
		this.activeProduct=this.userProducts[i];
		if(!this.activeProduct.product||!this.activeProduct.product.sourceId||!this.activeProduct.product.sourceId[0]){
			return;
		}

		let currId=this.activeProduct.product.sourceId[0];
		let filter:any={
			// batch:this.student.mainBatch.id
			date:moment().format("yyyy-MM-DD")
		};
		if(date){
			filter.date=moment(date).format("yyyy-MM-DD");
		}
		// filter.curriculums=this.activeProduct.product.sourceId.join(",");
		// filter.contentTypes="Video,Notes";
		this.panelLoader="show";
		this.lessonService.getDaywiseLiveLesson(currId, filter).subscribe(
			res=>{
				this.lessons=res;
				for(let li=0; li<this.lessons.length; li++){
					if(this.lessons[li].publishDate&&moment(this.lessons[li].publishDate).isAfter(new Date())){
						this.lessons[li].pending=true;
						this.lessons[li].remTime=moment(this.lessons[li].publishDate).diff(moment(new Date()), "seconds");
						if(this.lessons[li].remTime<=1800){
							this.setRemTime(li);
						}
					}
					if(this.lessons[li].endDate&&moment(this.lessons[li].endDate).isBefore(new Date())){
						this.lessons[li].expired=true;
					}

				}
				this.panelLoader="none";

			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, 'danger', 3000);
			}
		);

	}
	loadProductList(){
		this.selectedDate=new Date();

		this.activeProduct=null;
	}

	async loadLessonContent(i){
		this.activeLesson=this.lessons[i];
		if(!this.activeLesson){
			return;
		}

		this.panelLoader="show";
		let fltr:any={};
        // fltr.did=
        try {
            fltr.did=await macaddress.one();
        } catch (error) {
            console.log(error)
        }
		this.lessonService.getSingleLessonContent(this.activeLesson.id, this.activeProduct.product.id, fltr).subscribe(
            res => {
				this.panelLoader = 'none';
				if(!res.lessonContent||!res.lessonContent.blocks||res.lessonContent.blocks.length<1){
					this.activeLesson.content = [];
					this.notifier.alert("Wait", "Class is not created yet!", "danger", 3000);
					return;
                }

				let blks=res.lessonContent.blocks;
				let liveObj=res.lessonContent.live;
				if(!liveObj||!liveObj.id){
					this.notifier.alert("Wait", "Class is not started yet!", "danger", 3000);
					this.lessons[i].pending=true;
					this.lessons[i].remTime=60;
					this.setRemTime(i);
					return;
				}
				if(blks[0]&&blks[0].source=="zoom"&&liveObj&&!liveObj.finished){
					this.lessons[i].joined=true;
					let joinUrl=EdukitConfig.BASICS.API_URL+'/course-creator/lesson-content/'+this.activeLesson.id+'/'+this.activeProduct.id+'/j-live-class/'+liveObj.id+'?accessKey='+res.accessKey;
					// window.location.href=joinUrl;
					let params  = 'width='+screen.width+',height='+screen.height+',top=0,left=0';
					
					// let newWindow=window.open(joinUrl, blks[0].title,params);

					// if (window.focus) {newWindow.focus()}
					this.livClss.joinUrl=this.getSafeURL(joinUrl);
					// console.log()
					this.livClss.open=true;
					return;
				}
				this.activeLesson.hlsv=res.hlsv;
				this.activeLesson.content = res.lessonContent.blocks;
				if(res.lessonCompleted){
					this.activeLesson.completeStatus=true;
				}else{
					this.activeLesson.completeStatus=false;
				}
				for (let index = 0; index < this.activeLesson.content.length; index++) {
					if (this.activeLesson.content[index].type == 'audio' || this.activeLesson.content[index].type == 'slides') {
						this.activeLesson.content[index].content = this.getSafeURL(this.activeLesson.content[index].content);
					} else if (this.activeLesson.content[index].type == 'video') {
						if (this.activeLesson.content[index].source == 'YouTube') {
							this.activeLesson.content[index].content = this.getSafeURL("https://youtube.com/embed/" + this.activeLesson.content[index].content);
						}
						if (this.activeLesson.content[index].source == 'vimeo') {
							// this.activeLesson.content[index].hlsPlayerUrl=this.getSafeURL(EdukitConfig.BASICS.API_URL+"/video-player/video/"+ this.activeLesson.content[index].content+"?uid="+this.sessionStd.id);
							this.activeLesson.content[index].content = this.getSafeURL(EdukitConfig.BASICS.API_URL+"/public/video-player/lesson/" + this.activeLesson.content[index].content+'?accessKey='+res.accessKey+'&uid='+this.sessionStd.id);
							// this.activeLesson.content[index].content = this.getSafeURL("https://player.vimeo.com/video/" + this.activeLesson.content[index].content);
						}
						if (this.activeLesson.content[index].source == 'Vidrize') {
							this.activeLesson.content[index].content = this.getSafeURL(EdukitConfig.BASICS.API_URL+'/vidrize/player/'+this.activeLesson.content[index].content+'?autoplay=false&uid='+this.sessionStd.id);
						}
					}else if(res.lessonContent.live&&this.activeLesson.publishDate){
						this.activeLesson.live=res.lessonContent.live;
						let lt=0;
						// if(moment(this.activeLesson.publishDate).isSameOrBefore(new Date())){
						//     this.liveTimer.secs=0;
						// }else{
						//     this.setLiveWaitingTimer(this.activeLesson.publishDate);
						// }
					}
				}
				
            },
            err => {
				this.panelLoader = 'none';
				this.notifier.alert("Wait", "Class is not started yet! Wait for some time!", "danger", 3000);
				return;
				
                
                
                
            }
        );

	}
	getSafeURL(url) {
        return this.sanitizer.bypassSecurityTrustResourceUrl(url);
    }
	onDateChange(){
		// console.log(new Date(this.selectedDate));
		this.loadCourse(this.activeProductIndex, this.selectedDate)
	}

	joinZoomClass(){

	}

	setRemTime(lessonIndex:number){
		if(this.timers[lessonIndex]){
			clearInterval(this.timers[lessonIndex])
		}
		this.timers[lessonIndex]=setInterval(()=>{
			this.lessons[lessonIndex].remTime--;
			this.lessons[lessonIndex].remMm=Math.floor(this.lessons[lessonIndex].remTime/60);
			this.lessons[lessonIndex].remSec=Math.floor(this.lessons[lessonIndex].remTime%60);
			if(this.lessons[lessonIndex].remTime<1){
				this.lessons[lessonIndex].pending=false;
			}
			
		}, 1000);
	}
}
