import { Routes, CanActivateChild } from '@angular/router';

import { OnlineCourseAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  { path: '',
    component:OnlineCourseAppComponent,
    children: [
      { path: '', loadChildren: () => import('./activities/course/course.activity.socc').then(m => m.StudentOnlineCourseActivity) },
      { path: 'lectures', loadChildren: () => import('./activities/lectures/course.activity.socc').then(m => m.StudentOnlineCourseLectureActivity) },
      { path: 'elibrary', loadChildren: () => import('./activities/elib/course.activity.socl').then(m => m.StudentOnlineCourseLibActivity) },
      { path: 'assignments', loadChildren: () => import('./activities/assignments/course.activity.socc').then(m => m.StudentOnlineCourseActivity) },
      { path: 'classes', loadChildren: () => import('./activities/classes/classes.activity.socc').then(m => m.StudentOnlineClassesActivity) },
      { path: 'dashboard', loadChildren: () => import('./activities/home/home.activity.soch').then(m => m.StudentOnlineCourseHomeActivity) },
      { path: 'buy', loadChildren: () => import('./activities/product/product.activity.socp').then(m => m.StudentOnlineCourseProductActivity) },
      { path: 'video-solution', loadChildren: () => import('./activities/solution/solution.activity.socs').then(m => m.StudentOnlineCourseSolutionActivity) },
      { path: ':productId', loadChildren: () => import('./activities/window/course-window.activity.socw').then(m => m.StudentOnlineCourseWindowActivity) },

    ]
  }
]
