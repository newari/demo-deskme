import { Component, OnInit  } from '@angular/core';
import { Product } from '../../../../../lib/models/product.model';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';
import { ProductService } from '../../../../../lib/services/product.service';
import { StudentPanelService } from '../../../../../lib/services/student-panel.service';

@Component({
    templateUrl:'./list.html'
})

export class ProductListContent implements OnInit{
    panelLoader="none";
    sessionStudent : any;
    productCategories:any[];
    products: Product[];
    myRecommandedProducts: Product[];
    selectedProductCategory: any;
	selectedCourse: any;
	allCourses: any;
	selectedStream : any;
	streams: any;
    constructor(
        private authService: AuthService,
        private notifier: NotifierService,
        private productService: ProductService,
        private spService : StudentPanelService,
    ){}
    ngOnInit(){
        this.sessionStudent=this.authService.studentInfo();
        this.loadProductCategories();
    }

    loadProductCategories() {
		this.productService.getLiveProductsCategories({status:true}).subscribe(
			res=>{
				
                this.productCategories=res;
				this.loadCourses();
				if(res.length>0){
					this.selectedProductCategory=res[0];
					let filter:any={
						productCategories: [this.selectedProductCategory.id],
						products: this.sessionStudent&&this.sessionStudent.products?this.sessionStudent.products:[],
						needBoughtProduct: false
					}
					if(this.sessionStudent&&this.sessionStudent.products&&this.sessionStudent.products.length>0){
						filter.products=this.sessionStudent.products
					}
					if(this.sessionStudent&&this.sessionStudent.majorStream){
						// filter.stream=this.sessionStudent.majorStream.id||this.sessionStudent.majorStream;
						this.selectedStream=null;
					}
					if(this.sessionStudent&&this.sessionStudent.majorCourse){
						filter.course = this.sessionStudent.majorCourse.id||this.sessionStudent.majorCourse;
						this.selectedCourse = this.sessionStudent.majorCourse.id||this.sessionStudent.majorCourse;
					}
					this.loadMyRecommandedProducts(filter);
				}
				
			}
		);
    }
    loadMyRecommandedProducts(filter?:any) {
        this.panelLoader="show";
		this.spService.getMyRecommandedProdcuts(filter).subscribe(
			res => {
				if(res&&res[this.selectedProductCategory.id])
				this.myRecommandedProducts = res[this.selectedProductCategory.id].products;
				this.panelLoader='none';
			},
			err => {
				this.notifier.alert(err.code, err.message, 'danger', 3000);
				this.panelLoader='none';

			}
		)
	}
	loadCourses() {
		let filter:any= {status:true, populateStream:true};
		if(this.selectedProductCategory){
			filter.productCategory=this.selectedProductCategory.id
		}
		this.productService.getAllCourses(filter).subscribe(
			res=>{
				this.allCourses=res;
				if(this.selectedCourse){
					this.loadStreams(this.selectedCourse);
				}
				// this.selectedStream=null;
				
			},
			err=>{
				this.notifier.alert(err.code,err.message, 'danger', 5000)
			}
		);
	}
	loadStreams(courseId:string) {
		console.log(courseId)
		
		for(let ci=0; ci<this.allCourses.length; ci++){
			if(this.allCourses[ci].id==courseId){
				this.streams=this.allCourses[ci].childOptions;
				return;
			}
		}

		this.streams=[];
		
	}
    setTitle(title){
		if(title=='Online Quiz'){
			return 'Online Test Series';
		}else if(title=='Online Course'){
			return 'Video Course';
		}else{
			return title;
		}
	}
    onProductTabChange(event){
		let index= event.index;
		this.selectedProductCategory= this.productCategories[index]
		this.panelLoader="show";
		let filter:any={
			status:true,
			productCategories:[this.selectedProductCategory.id],
			products:this.sessionStudent&&this.sessionStudent.products?this.sessionStudent.products:[]
		}
		if(this.sessionStudent){
			filter.course= this.sessionStudent.majorCourse;
			// filter.stream= this.sessionStudent.majorStream.id||this.sessionStudent.majorStream;
		}
		if(this.selectedCourse){
			filter.course=this.selectedCourse;
		}
		if(this.selectedStream){
			filter.stream=this.selectedStream;
		}
		this.myRecommandedProducts=[];
		this.loadMyRecommandedProducts(filter)
	}
	setSelectedCourse(courseId){
		this.selectedCourse=courseId;
		let filter:any={
			status:true,
			productCategories:[this.selectedProductCategory.id],
			products:this.sessionStudent&&this.sessionStudent.products?this.sessionStudent.products:[]
		}
		if(this.sessionStudent){
			filter.course= this.sessionStudent.majorCourse;
			// filter.stream= this.sessionStudent.majorStream.id||this.sessionStudent.majorStream;
		}
		if(this.selectedCourse){
			filter.course=this.selectedCourse;
		}
		this.myRecommandedProducts=[];
		this.selectedStream=null;
		this.loadStreams(courseId);
		this.loadMyRecommandedProducts(filter);
	}
	setSelectedStream(streamId){
		this.selectedStream=streamId;
		let filter:any={
			status:true,
			productCategories:[this.selectedProductCategory.id],
			products:this.sessionStudent&&this.sessionStudent.products?this.sessionStudent.products:[]
		}
		if(this.sessionStudent){
			filter.course= this.sessionStudent.majorCourse;
			filter.stream= this.sessionStudent.majorStream.id||this.sessionStudent.majorStream;
		}
		if(this.selectedCourse){
			filter.course=this.selectedCourse;
		}
		if(this.selectedStream&&this.selectedStream!="null"){
			filter.stream=this.selectedStream;
		}
		this.myRecommandedProducts=[];
		this.loadMyRecommandedProducts(filter)
	}
}