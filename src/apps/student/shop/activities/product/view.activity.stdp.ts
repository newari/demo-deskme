import {NgModule} from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentProductContent } from './view';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TestseriesService } from "../../../../../lib/services/testseries.service";
import { OrderService } from "../../../../../lib/services/order.service";
import { StudentOrderPaymentModule } from "../../../shared/modules/order-payment/order-payment.module";
import { ProductService } from "../../../../../lib/services/product.service";
import { PaymentService } from "../../../../../lib/services/payment.service";
import { StudentService } from "../../../../../lib/services/student.service";
import { ConventionalExamTestseriesService } from "../../../../../lib/services/conventional-exam-test-series.service";
import { SafeHtmlPipeModule } from "../../../../../lib/filters/safehtml.pipe";
import { DialogModule } from 'primeng/dialog';
import { FieldsetModule } from 'primeng/fieldset';

export const ROUTES:Routes=[
    {path: '', component: StudentProductContent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Product - Dashboard'}},
];

@NgModule({
    declarations: [StudentProductContent],
    imports:[
        CommonModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        FieldsetModule,
        RouterModule.forChild(ROUTES),
        StudentOrderPaymentModule,
        FieldsetModule,
        SafeHtmlPipeModule
    ],
    providers: [TestseriesService,OrderService,ProductService,PaymentService, StudentService,ConventionalExamTestseriesService]
})
export class ProductViewActivity { }
