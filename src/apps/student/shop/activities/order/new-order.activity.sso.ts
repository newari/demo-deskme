import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentShopNewOrderComponent } from './new-order.component';
import { StudentFullOrderFormModule } from '../../../shared/modules/full-order-form/full-order-form.module';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { OrderService } from "../../../../../lib/services/order.service";
import { StudentFullOrder2FormModule } from "../../../shared/modules/full-order-2-form/full-order-2-form.module";

export const ROUTES:Routes=[
    {path: '', component: StudentShopNewOrderComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];
 
@NgModule({
    declarations: [StudentShopNewOrderComponent],
    imports:[
        CommonModule,
        StudentFullOrderFormModule,
        StudentFullOrder2FormModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[OrderService]
})
export class StudentShopNewOrderActivity { 
    
}