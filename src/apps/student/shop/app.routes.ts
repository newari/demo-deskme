import { Routes, CanActivateChild } from '@angular/router';

import { StudentShopAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  {
    path: '',
    component:StudentShopAppComponent,
    children: [
      	{ path: '', loadChildren: () => import('./activities/home/home.activity.ssh').then(m => m.StudentShopHomeActivity) },
      	{ path: 'order/new', loadChildren: () => import('./activities/order/new-order.activity.sso').then(m => m.StudentShopNewOrderActivity) },
      	{ path: 'order/:orderId/payment', loadChildren: () => import('./activities/order/order-payment.activity.sso').then(m => m.StudentShopOrderPaymentActivity) },
          { path: 'product', loadChildren: () => import('./activities/product/index.stdp').then(m => m.StudentProductModule)  },
        ]
  }
]
