import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";
import { StudentShopAppComponent } from './app.component';

@NgModule({
    declarations: [StudentShopAppComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[]
})
export class StudentShopAppModule { }
