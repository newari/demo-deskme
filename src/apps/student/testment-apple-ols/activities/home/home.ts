import { Router } from '@angular/router';
import { Component, ViewChild,ElementRef } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { TestMentService } from '../../../../../lib/services/testment.service';
import { letProto } from 'rxjs-compat/operator/let';
import { EdukitConfig } from '../../../../../ezukit.config';
var Chart = require('chart.js');

@Component({
  templateUrl: './home.html'
})
export class StudentTestMentHomeComponent {
    panelLoader:string;
	sessionStd:any;
	upComingTests;
	barChartData;
	overAllReportList=[];
	testChartContent;
	lastAttemptedTest;
	basicAPI;
	subscribedBooks:any[];
	@ViewChild('overAllChart', { read: ElementRef }) overAllChart: ElementRef;
	@ViewChild('testChart', { read: ElementRef }) testChart: ElementRef;
	@ViewChild('lastAttemptedChart', { read: ElementRef }) lastAttemptedChart: ElementRef;
    constructor(
      	private authService: AuthService,
        private notifier: NotifierService,
		private testmentService:TestMentService,
		private router:Router
    ){
        
    }
	ngOnInit(){
		this.sessionStd=this.authService.student();
		this.basicAPI = EdukitConfig.BASICS.API_URL;
		this.getOverAllReport();
		this.getUpComingTests();
		this.getLastAttemptedTest();
	}
	getOverAllReport(){
		let data={
			user : this.sessionStd.id,
			top : 3
		}
		this.testmentService.getOverAllReport(data).subscribe(
			res=>{
				this.overAllReportList=res;
				if(this.overAllReportList.length > 0){
					this.overAllReportChart();
					this.setTopTestChart(this.overAllReportList[0]);
				}	
			},
			err=>{ 
				// this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	getUpComingTests(){
		let data={
			user : this.sessionStd.id
		}
		this.testmentService.getUpComingTests(data).subscribe(
			res=>{
				this.upComingTests = res;
			},
			err=>{ 
				// this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	getLastAttemptedTest(){
		this.testmentService.getLastAttemptedTest(this.sessionStd.id).subscribe(
			res=>{
				this.lastAttemptedTest = res;
				if(this.lastAttemptedTest){
					this.displayTestChart(res,this.lastAttemptedChart);
				}
			},
			err=>{ 
				// this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	setTopTestChart(data){
		this.testChartContent= data;
		this.displayTestChart(data,this.testChart);
	}
	displayTestChart(data,chart){ 
		let chartData =  [data.correctQs,data.inCorrectQs,(data.test.totalQs - data.unAttemptedQs),data.unAttemptedQs];
		new Chart(chart.nativeElement,{
			type: 'doughnut',
			data: {
				labels: [
					'Correct',
					'In Correct',
					'Attempted',
					'Un Attempted',
					
				],
				datasets: [
					{
					data: chartData,
					backgroundColor: [
						'hsl(122, 41%, 49%)',
						'rgb(228, 74, 63)',
						'rgba(54, 162, 235, 1)',
						'rgb(255, 152, 0)',
					],
					borderWidth: 1
				}]
			},
			animationEnabled: true,
			animationSteps: 60,
			options: {
				legend:{
					position : 'bottom'
				},
			}
		});
	}
	testWindow(data) {	
		let view='w';
        if(screen.width < 672){
            view='m';
        }
		let url = this.basicAPI +'/student/testment/testwindow/'+data.test.theme+'/'+view+'/'+data._id;
		let params  = 'width='+screen.width+',height='+screen.height+',top=0,left=0,fullscreen=yes,resizable=0';
		let newWindow=window.open(url,'EXAM',params);
		if (window.focus) {newWindow.focus()}
		return false;
	}
	getSecondsAsDigitalClock(inputSeconds: number) {
		const secNum = parseInt(inputSeconds.toString(), 10); // don't forget the second param
		const hours = Math.floor(secNum / 3600);
		const minutes = Math.floor((secNum - (hours * 3600)) / 60);
		const seconds = secNum - (hours * 3600) - (minutes * 60);
		let hoursString = '';
		let minutesString = '';
		let secondsString = '';
		let time = '';
		if(hours !=0){
			hoursString = (hours < 10) ? '0' + hours : hours.toString();
			time = hoursString + ' h ';
		}
		if(minutes !=0){
			minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
			time += minutesString + ' m ';
		}
		secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
		time += secondsString+' sec';
		return time;
	  }
	overAllReportChart(){
		let label = [];
		let data = [];
		for(var i=0;i < this.overAllReportList.length;i++){
			label.push(this.overAllReportList[i].test.title.substr(0,7) +'...');
			data.push(this.overAllReportList[i].percentage);
		}
		var vdata = {
			labels: label,
			datasets: [{
			  		label: 'Percentage',
					backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(54, 162, 235, 0.2)',
						'rgba(255, 206, 86, 0.2)',
						'rgba(75, 192, 192, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(255, 159, 64, 0.2)',
						'rgba(255, 99, 132, 0.2)',
						'rgba(54, 162, 235, 0.2)',
						'rgba(255, 206, 86, 0.2)',
						'rgba(75, 192, 192, 0.2)',
						'rgba(153, 102, 255, 0.2)',
						'rgba(255, 159, 64, 0.2)'
					],
					borderColor: [
						'rgba(255,99,132,1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)',
						'rgba(75, 192, 192, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(255, 159, 64, 1)',
						'rgba(255,99,132,1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)',
						'rgba(75, 192, 192, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(255, 159, 64, 1)'
					],
					borderWidth: 1,
					data: data,
			}]
		  };
		  var options = {
			maintainAspectRatio: false,
			scales: {
			  yAxes: [{
				stacked: true,
				gridLines: {
				  display: true,
				  color: "rgba(255,99,132,0.2)"
				}
			  }],
			  xAxes: [{
				gridLines: {
				  display: false
				}
			  }]
			}
		  };
		  
		new Chart(this.overAllChart.nativeElement, {
			type : 'bar',
			options: options,
			data: vdata
		});
	}
}
