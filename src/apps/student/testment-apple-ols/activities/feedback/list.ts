import { Component } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { PassageService } from '../../../../../lib/services/passage.service';
import { BookmarkQuestionService } from '../../../../../lib/services/bookmarkquestion.service';
import { TmFeedbackService } from '../../../../../lib/services/tmfeedback.service';

@Component({
  templateUrl: './list.html'
})
export class TestMentFeedbackListContent{
	feedbacks : any={
		TEST_SERIES : [],
		TEST : [],
		QUESTION : []
	};
	activeTab : string;
    constructor(
		private notifier: NotifierService,
		private tmFeedbackService : TmFeedbackService
    ){ }
    ngOnInit(){
		this.getFeedbackQs('TEST_SERIES');	
	}
	loadMathJax(){
		setTimeout(function(){
				window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub]);
		}, 10);
	}
	getFeedbackQs(type){
		if(this.feedbacks[type].length >0){
			return;
		}
		let filter : any={type : type};
        this.tmFeedbackService.getStudentFeedbackQs(filter).subscribe(
            (res)=> {
				this.feedbacks[type] = res;
				if(type == 'QUESTION') this.loadMathJax();
            },
            (err)=>{this.notifier.alert(err.code,err.message,'danger',2000)}
        );
	}
	onTabChange(event){
		let index = event.index;
        if(index == 0 && this.activeTab != 'TEST_SERIES'){
		   this.activeTab = 'TEST_SERIES'; 
		   this.getFeedbackQs('TEST_SERIES');
        }
        else if(index == 1 && this.activeTab != 'TEST'){
			this.activeTab = 'TEST'; 
            this.getFeedbackQs('TEST');            
        }
        else if(index == 2 && this.activeTab != 'QUESTION'){
			this.activeTab = 'QUESTION'; 
            this.getFeedbackQs('QUESTION');
        }
    }
}
