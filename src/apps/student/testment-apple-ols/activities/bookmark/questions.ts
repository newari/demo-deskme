import { Component } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { PassageService } from '../../../../../lib/services/passage.service';
import { BookmarkQuestionService } from '../../../../../lib/services/bookmarkquestion.service';

@Component({
  templateUrl: './questions.html'
})
export class TestMentBookmarkQuestionsContent{
	sessionStd;
	qBookmark;
	qLevelLabel = [null,"Easy","Average","Medium","Difficult","Very Difficult"];
    constructor(
		private qbookmarkService : BookmarkQuestionService,
		private passageService : PassageService,
      	private authService: AuthService,
        private notifier: NotifierService,
    ){
    }
    ngOnInit(){
			this.sessionStd=this.authService.student();
			this.loadBookmarkQuestion(this.sessionStd.id);
		}
		loadMathJax(){
			setTimeout(function(){
					window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub]);
			}, 10);
		}
	loadBookmarkQuestion(userId){
		let filter={
			user : userId,
			limit : 'all'
		};
		this.qbookmarkService.getBookmarkQuestion(filter).subscribe(
		  res=>{							
			this.qBookmark = res;
			this.loadMathJax();
		  },
		  err=>{
			  console.log(err);
			//   this.notifier.alert(err.code, err.message, "danger", 10000);
		  }
		)
	}
	getPassage(passageId,index){
		this.passageService.getOnePassage(passageId).subscribe(
		  res=>{							
				this.qBookmark[index].question.passage=res;
				this.loadMathJax();
		  },
		  err=>{
			  this.notifier.alert(err.code, err.message, "danger", 10000);
		  }
		)
	}		

}
