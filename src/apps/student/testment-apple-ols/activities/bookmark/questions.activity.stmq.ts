import {NgModule} from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TestMentBookmarkQuestionsContent } from './questions';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { PassageService } from "../../../../../lib/services/passage.service";
import { BookmarkQuestionService } from "../../../../../lib/services/bookmarkquestion.service";
import { SafeHtmlPipeModule } from "../../../../../lib/filters/safehtml.pipe";

export const ROUTES:Routes=[
    {path: '', component: TestMentBookmarkQuestionsContent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Test'}},
];
@NgModule({
    declarations: [TestMentBookmarkQuestionsContent],
    imports:[
        CommonModule,
        RouterModule.forChild(ROUTES),SafeHtmlPipeModule
    ],
    providers: [BookmarkQuestionService,PassageService]
})
export class StudentTestMentBookmarkActivity { }