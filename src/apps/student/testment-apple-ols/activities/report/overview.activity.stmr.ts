import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TestMentOverViewContent } from './overview';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { ConventionalExamService } from '../../../../../lib/services/conventional-exam.service';
import { DialogModule, AccordionModule, TabViewModule, ChartModule, PaginatorModule } from 'primeng/primeng';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TestMentService } from "../../../../../lib/services/testment.service";
import { QsetService } from "../../../../../lib/services/qset.service";
import { PassageService } from "../../../../../lib/services/passage.service";
import { BookmarkQuestionService } from "../../../../../lib/services/bookmarkquestion.service";
import { QuestionService } from "../../../../../lib/services/question.service";
import { SafeHtmlPipeModule } from "../../../../../lib/filters/safehtml.pipe";
import { TmFeedbackService } from "../../../../../lib/services/tmfeedback.service";

export const ROUTES:Routes=[
    {path: '', component: TestMentOverViewContent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Report'}},
];
 
@NgModule({
    declarations: [TestMentOverViewContent],
    imports:[
        CommonModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ROUTES),
        AccordionModule,
        TabViewModule,
        ChartModule,
        PaginatorModule,
        SafeHtmlPipeModule,
        DialogModule
    ],
    providers: [ConventionalExamService,TestMentService,QuestionService,BookmarkQuestionService,PassageService,TmFeedbackService]
})
export class StudentTestMentOverViewActivity { }