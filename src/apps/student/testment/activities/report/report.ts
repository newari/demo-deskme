import { Component } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { FormGroup} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';
import { TestMentService } from '../../../../../lib/services/testment.service';

@Component({
  templateUrl: './report.html'
})
export class TestMentReportContent {
	userTestSeries;
	testseriesId = null;
	sessionStd:any;
	sessionStdInfo:any;
	feedBackForm:FormGroup;
	testId:any;
	courses:any;
	courseIds:string[]=[];
	activeCourse;
	activeTestSeries;
	panelLoader = 'none';
	constructor(
		private authService: AuthService,
		private notifier : NotifierService,
		private activatedRoute: ActivatedRoute,
		private testmentService : TestMentService
	){}
	ngOnInit(){
		this.sessionStd=this.authService.student();
		this.sessionStdInfo=this.authService.studentInfo();
		this.loadTests(this.sessionStd.id);
	}
	loadTests(userId){
		this.panelLoader = 'show';
		this.testmentService.getUserReportTests(userId).subscribe(
		  res=>{							
				this.userTestSeries=res;
				this.courses={};
				for(let ci=0; ci<res.length; ci++){
					if(!this.courses[res[ci].course._id]){
						this.courses[res[ci].course._id]=res[ci].course;
						this.courseIds.push(res[ci].course._id);
					}
				}
				if(res.length > 1){
					this.activeCourse = this.courseIds[0];
					this.activeTestSeries = this.userTestSeries[0].tsDetail._id;	
				}
				this.panelLoader = 'none';
		  },
		  err=>{
				this.panelLoader = 'none';
				this.notifier.alert(err.code, err.message, "danger", 10000);
		  }
		)
	}
}
