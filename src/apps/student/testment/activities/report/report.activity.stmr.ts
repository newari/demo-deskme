import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TestMentReportContent } from './report';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { ConventionalExamService } from '../../../../../lib/services/conventional-exam.service';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TestMentService } from "../../../../../lib/services/testment.service";
import { DialogModule } from 'primeng/dialog';
import { AccordionModule } from 'primeng/accordion';
import { TabViewModule } from 'primeng/tabview';
import { FieldsetModule } from 'primeng/fieldset';
export const ROUTES:Routes=[
    {path: '', component: TestMentReportContent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Report'}},
];

@NgModule({
    declarations: [TestMentReportContent],
    imports:[
        CommonModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(ROUTES),
        AccordionModule,
        TabViewModule,
        FieldsetModule
    ],
    providers: [ConventionalExamService,TestMentService]
})
export class StudentTestMentReportActivity { }
