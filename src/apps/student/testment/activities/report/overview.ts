import { Component, ElementRef, ViewChild, OnInit, HostListener} from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NotifierService } from '../../../../../lib/components/notifier/notifier.service';
import { TestMentService } from '../../../../../lib/services/testment.service';
import { PassageService } from '../../../../../lib/services/passage.service';
import { BookmarkQuestionService } from '../../../../../lib/services/bookmarkquestion.service';
import { QuestionService } from '../../../../../lib/services/question.service';
import { DomSanitizer } from '@angular/platform-browser';
// import { TestSeriesAddActivity } from '../../../../ims/conventional-exam/activities/testseries/add.activity.icets';
import { TmFeedbackSubjects } from '../../../../../lib/models/tmfeedbacksubjects.model';
import { TmFeedbackService } from '../../../../../lib/services/tmfeedback.service';
import { ClientService } from '../../../../../lib/services/client.service';
import { ClientConfig } from '../../../../../lib/models/clientconfig.model';
var Chart = require('chart.js');
@Component({
  templateUrl: './overview.html'
})

export class TestMentOverViewContent implements OnInit{
	userReport;
	sessionStd:any;
	feedbackForm:FormGroup;
	uotId:any;
	userId : any;
	data:any;
	qBookmark ={};
	paginateFilter : any;
	totalRecords: number;
	testParticipants : any;
	toppers : any=[];
	temp : any;
	rankData;
	diffLevelReport : any;
	qLevelLabel = ["Easy","Average","Medium","Difficult","Very Difficult","Tough"];
	// countBase;
	displaySection : boolean = false;
	sections:any;
	solutionSections : any;
	activeSolSec=0;
	activeTopSec=0;
	activeQueSec=0;
	lang : string;
	displaySolutionVideosModal : boolean = false;
	solutionVideoURL : any;
	activeVideo : number = 0;
	displayFeedbackModal : boolean=false;
	fbSubjects : any={
		TEST : [],
		QUESTION : []
	};
	panelLoader = 'none';
	activeFbType : string='';
	displayTestAttemptsModal : boolean=false;
	testAttempts : any=[];
	activeTab = 0;
	client : ClientConfig;
	@ViewChild('overviewChart', { read: ElementRef }) overviewChart: ElementRef;
	@ViewChild('sectionChart', { read: ElementRef }) sectionChart: ElementRef;
	@ViewChild('sectionBarChart', { read: ElementRef }) sectionBarChart: ElementRef;
	@ViewChild('compareChart', { read: ElementRef }) compareChart: ElementRef;
	@ViewChild('dLevelBarChart', { read: ElementRef }) dLevelBarChart: ElementRef;
	constructor(
		private authService: AuthService,
		private fb : FormBuilder,
		private notifier : NotifierService,
		private activatedRoute: ActivatedRoute,
		private testmentService : TestMentService,
		private qBookmarkService : BookmarkQuestionService,
		private passageService : PassageService,
		private questionService : QuestionService,
		private tmFeedbackService : TmFeedbackService,
		private clientService  : ClientService,
		private sanitizer : DomSanitizer
	){}
	@HostListener('window:keydown',['$event'])
	keyEvent(event: KeyboardEvent) {
		if(event.ctrlKey){
			this.notifier.alert("ERROR", "Your Have Pressed a restricted Key 'CTRL'", "danger", 1000);
			// window.document.location.href="/student/dashboard";
			return;
		}
	}
	ngOnInit(){
		this.sessionStd=this.authService.student();
		this.client = this.clientService.getLocalClient();
		this.activatedRoute.queryParams.subscribe(params=>{
			this.uotId = params['uotId'];
		})
		this.userId=this.sessionStd.id;
		this.feedbackForm= this.fb.group({
			type : [''],
			user : ['',Validators.required],
			subject :['',Validators.required],
			testSeries : [''],
			test : [''],
			question : [''],
			qSeq : [''],
			stream : [''],
			query : ['',Validators.required],
		});
		this.loadTest();
	}
	loadMathJax(){
        setTimeout(function(){
            window['MathJax'].Hub.Queue(["Typeset",window['MathJax'].Hub]);
        }, 10);
    }
	loadTest(){
		this.testmentService.getUserReportOverview(this.uotId).subscribe(
			res=>{
					this.userReport=res;
					// for(let i=0;i<this.userReport.test.sections.length; i++){
					// 	let title=this.userReport.test.sections[i].title;
					// 	if(title.length >= 25){
					// 		this.userReport.test.sections[i].title=title.substr(0,25) + '...';
					// 	}
					// }
					this.sections= JSON.parse(JSON.stringify(this.userReport.test.sections));
					this.solutionSections=JSON.parse(JSON.stringify(this.userReport.test.sections));
					this.userReport.totalTimeTaken=this.getSecondsAsDigitalClock(this.userReport.totalTimeTaken);
					this.lang=this.userReport.test.defaultLang;
					if (this.userReport.test.solutionVideo) {
						this.userReport.test.solutionVideo = this.sanitizer.bypassSecurityTrustResourceUrl(this.userReport.test.solutionVideo);
					}
					this.generateTestRank();
					this.displayOverviewChart(this.userReport,this.overviewChart);
					this.displaySectionChart(this.userReport.sectionScore,this.sectionChart);
					// this.barChart(this.userReport.sectionScore);
			},
			err=>{
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	loadTestAttempts(){
		let filter={
			userOnlineTest : this.uotId
		};
		this.testmentService.getTestAttempts(filter).subscribe(res=>{
			this.testAttempts = res;
		},
		err=>{
			this.notifier.alert(err.code, err.message, "danger", 10000);
		});
	}

	getSecondsAsDigitalClock(inputSeconds: number) {
		if(!inputSeconds) return;
		const secNum = parseInt(inputSeconds.toString(), 10); // don't forget the second param
		const hours = Math.floor(secNum / 3600);
		const minutes = Math.floor((secNum - (hours * 3600)) / 60);
		const seconds = secNum - (hours * 3600) - (minutes * 60);
		let hoursString = '';
		let minutesString = '';
		let secondsString = '';
		let time = '';
		if(hours !=0){
			hoursString = (hours < 10) ? '0' + hours : hours.toString();
			time = hoursString + ' h ';
		}
		if(minutes !=0){
			minutesString = (minutes < 10) ? '0' + minutes : minutes.toString();
			time += minutesString + ' m ';
		}
		secondsString = (seconds < 10) ? '0' + seconds : seconds.toString();
		time += secondsString+' sec';
		return time;
	  }
	onQuestionSecChange(index){
		if(index > 0 && this.sections[index].isQsetAssigned && !this.sections[index].questions){
			this.getReportQuestionWise(index);
		}
	}
	onTopicSecChange(index){
		if(index > 0 && this.sections[index].isQsetAssigned && !this.sections[index].topicQs){
			this.getReportTopicWise(index);
		}
	}
	onSolutionSecChange(index){
		if(index > 0 && this.solutionSections[index].isQsetAssigned && !this.solutionSections[index].questions){
			this.solutionSections[index].questions={};
			this.getReportSolution(index);
		}
		this.activeSolSec=index;
	}
	onTabChange(index) {
		// var index=event.index;
		// var value=event.originalEvent.target.innerText;
		if(!this.sections || !this.sections[0]) return;
		if(index == 1 && !this.displaySection){
			setTimeout(() => {
				this.displaySection = true;
				this.secBarChart(this.userReport.sectionScore);
			}, 10);
		}
		else if(index == 2 && this.sections[0].isQsetAssigned && !this.sections[0].topicQs){
			this.getReportTopicWise(0);
			this.getQuestionBookmark();
		}
		else if(index == 3 && this.sections[0].isQsetAssigned && !this.sections[0].questions){
			this.getReportQuestionWise(0);
		}
		else if(index == 4 && this.toppers.length<1){
			this.getCompareWithToppers();
		}
		else if(index == 5 && this.solutionSections[0].isQsetAssigned && !this.solutionSections[0].questions){
			this.solutionSections[0].questions={};
			this.getReportSolution(0);
			this.getQuestionBookmark();
		}
		else if(index == 6 && !this.diffLevelReport){
			this.getReportDifficultLevel();
		}
		else if(index == 7 && !this.testParticipants){
			this.getTestParticipants();
		}
		this.activeTab = index;
	}
	getCompareWithToppers(){
		let data={
			test : this.userReport.test.id,
			testSeries : this.userReport.testSeries,
		};
		this.testmentService.getCompareWithToppers(data).subscribe(
			res=>{
				this.toppers= res;
				this.compareBarChart();
			},
			err=>{ this.notifier.alert(err.code, err.message, "danger", 10000);}
		)
	}
	generateTestRank(){
		let data={
			test : this.userReport.test.id,
			testSeries : this.userReport.testSeries,
		};
		this.testmentService.generateTestRank(data,this.userId).subscribe(
			res=>{
				this.rankData=res;
			},
			err=>{ this.notifier.alert(err.code, err.message, "danger", 10000);}
		)
	}
	getReportQuestionWise(sectionIndex){
		let data : any={
			user : this.sessionStd.id,
			test : this.userReport.test.id,
			testSeries : this.userReport.testSeries,
			sectionIndex : sectionIndex
		};
		if(this.userReport.uotAttempt){
			data.uotAttempt = this.userReport.uotAttempt;
		}
		let qsetId = this.sections[sectionIndex].qset;
		this.panelLoader = 'show';
		this.testmentService.getReportQuestionWise(data,qsetId).subscribe(
			res=>{
				this.sections[sectionIndex].questions = res;
				this.panelLoader = 'none';
			},
			err=>{
				this.panelLoader = 'none';
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	getReportTopicWise(sectionIndex){
		let data:any={
			user : this.sessionStd.id,
			test : this.userReport.test.id,
			testSeries : this.userReport.testSeries,
			sectionIndex : sectionIndex
		};
		if(this.userReport.uotAttempt){
			data.uotAttempt = this.userReport.uotAttempt;
		}
		let qsetId = this.sections[sectionIndex].qset;
		this.testmentService.getReportTopicWise(data,qsetId).subscribe(
			res=>{
				this.sections[sectionIndex].topicQs = res;
			},
			err=>{ this.notifier.alert(err.code, err.message, "danger", 10000);}
		)
	}
	dislpayTopicQs(sectionIndex,topicIndex){
		let topic = this.sections[sectionIndex].topicQs[topicIndex];
		if(topic.questions[0].question.id){
			topic.showQuestions = !topic.showQuestions;
		}
		else{
			for (let i = 0; i < topic.questions.length; i++) {
				let questionId = topic.questions[i].question;
				this.questionService.getOneQuestion(questionId).subscribe(
					res=>{
						topic.questions[i].question=res;
						topic.showQuestions=true;
					}
				);
			}
		}
	}
	getReportSolution(sectionIndex){
		if(!this.solutionSections[sectionIndex].questions[this.lang]){
			this.solutionSections[sectionIndex].questions[this.lang]=[];
			let data : any={
				user : this.userId,
				test : this.userReport.test.id,
				testSeries : this.userReport.testSeries,
				sectionIndex : sectionIndex,
			};
			if(this.userReport.uotAttempt){
				data.uotAttempt = this.userReport.uotAttempt;
			}
			let qsetId = this.solutionSections[sectionIndex].qset;
			let fieldSet="question";
			if(this.userReport.test.defaultLang != this.lang){
				fieldSet="variantQuestion";
			}
			this.panelLoader="show";
			this.testmentService.getReportSolution(data,qsetId,fieldSet).subscribe(
				res=>{
					console.log(res);
					this.solutionSections[sectionIndex].questions[this.lang]= res;
					this.loadMathJax();
					this.panelLoader="none";
				},
				err=>{
					this.panelLoader="none";
					this.notifier.alert(err.code, err.message, "danger", 10000);
				}
			);
		}
	}
	getQuestionPassage(passageId,index){
		this.passageService.getOnePassage(passageId).subscribe(
		  	res=>{
				this.solutionSections[this.activeSolSec].questions[this.lang][index].question.passage=res;
				this.loadMathJax();
		  	},
		  err=>{
			  this.notifier.alert(err.code, err.message, "danger", 10000);
		  }
		)
	}
	addQuestionBookmark(questionId){
		let data={
			question : questionId,
			user : this.sessionStd.id,
			test : this.userReport.test.id,
		};
		this.qBookmarkService.addBookmarkQuestion(data).subscribe(
			res=>{
				this.qBookmark[res.question] = res;
			},
			err=>{ this.notifier.alert(err.code, err.message, "danger", 10000);}
		)
	}
	getQuestionBookmark(){
		let data={
			user : this.sessionStd.id,
			test : this.userReport.test.id,
		};
		this.qBookmarkService.getReportBookmarkQuestion(data).subscribe(
			res=>{
				this.qBookmark = res;
			},
			err=>{ this.notifier.alert(err.code, err.message, "danger", 10000);}
		)
	}
	removeQuestionBookmark(bookmarkId){
		this.qBookmarkService.deleteBookmarkQuestion(bookmarkId).subscribe(
			res=>{
				delete this.qBookmark[res.question.id];
			},
			err=>{ this.notifier.alert(err.code, err.message, "danger", 10000);}
		)
	}
	getReportDifficultLevel(){
		let data={
			user : this.sessionStd.id,
			test : this.userReport.test.id,
			testSeries : this.userReport.testSeries,
		};
		this.testmentService.getReportDifficultLevel(data).subscribe(
			res=>{
				let reportData = res;
				this.diffLevelReport = [];
				let levelTotals = [0,0,0,0,0];
				for(var i = 0; i< this.sections.length; i++){
					for(var key in this.sections[i].questionLevel){
						levelTotals[parseInt(key)-1]+=this.sections[i].questionLevel[key];
					}
				}
				for(var i=0; i < levelTotals.length; i++){
					let data={
						title:'',
						totalQs : 0,
						response : ''
					}
					data.totalQs= levelTotals[i];
					data.title = this.qLevelLabel[i];
					for(var j=0;j< reportData.length;j++){
						if(reportData[j]._id == i+1){
							data.response = reportData[j];
							break;
						}
					}
					this.diffLevelReport.push(data);

				}
				this.difficultyBarChart();
			},
			err=>{ this.notifier.alert(err.code, err.message, "danger", 10000);}
		)
	}
	getTestParticipants(){
		let filter={
			test : this.userReport.test.id,
			testSeries : this.userReport.testSeries,
			page : 1,
			limit : 10
		};
		if(this.paginateFilter){
			filter.page = this.paginateFilter.page;
			filter.limit = this.paginateFilter.limit;
		}
		this.panelLoader="show";
		this.testmentService.getTestParticipants(filter, true).subscribe(
			res=>{
				 this.testParticipants = res.body;
				 this.totalRecords = res.headers.get('totalRecords') || 0;
				 this.panelLoader="none";
			},
			err=>{
				this.panelLoader="none";
				this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	displaySectionChart(sectionData,chart){
		let labelData=[];
		let chartData=[];
		if(!sectionData || sectionData.length <1) return;
		for(var i=0; i<sectionData.length; i++){
			labelData.push(this.sections[i].title);
			chartData.push(sectionData[i].score);
		}
		new Chart(chart.nativeElement,{
			type: 'doughnut',
			data: {
				labels: labelData,
				datasets: [
					{
					data: chartData,
					backgroundColor: [
						'hsl(122, 41%, 49%)',
						'rgb(228, 74, 63)',
						'rgba(54, 162, 235, 1)',
						'rgb(255, 152, 0)',
						'rgb(63, 81, 181)',
					],
					borderWidth: 1
				}]
			},
			animationEnabled: true,
			animationSteps: 60,
			options: {
				legend:{
					position : 'bottom'
				},
			}
		});
	}
	displayOverviewChart(data,chart){
		let chartData =  [data.correctQs,data.inCorrectQs,(data.test.totalQs - data.unAttemptedQs),data.unAttemptedQs];
		// ---------------type : doughnut , pie ---------------
		new Chart(chart.nativeElement,{
			type: 'pie',
			data: {
				labels: [
					'Correct',
					'In Correct',
					'Attempted',
					'Un Attempted',
				],
				datasets: [
					{
					data: chartData,
					backgroundColor: [
						'hsl(122, 41%, 49%)',
						'rgb(228, 74, 63)',
						'rgba(54, 162, 235, 1)',
						'rgb(255, 152, 0)',
						'rgb(63, 81, 181)',
					],
					borderWidth: 1
				}]
			},
			animationEnabled: true,
			animationSteps: 60,
			options: {
				legend:{
					position : 'bottom'
				},

			}
		});
	}
	secBarChart(sectionData){
		let label=[];
		let score = [];
		let correct = [];
		let inCorrect = [];
		let attempted = [];
		let unAttempted = [];
		for(var i=0; i<sectionData.length; i++){
			label.push(this.sections[i].title.substr(0,7) +'...');
			score.push(sectionData[i].score);
			correct.push(sectionData[i].correctQs);
			inCorrect.push(sectionData[i].inCorrectQs);
			attempted.push(this.sections[i].totalQs - sectionData[i].unAttemptedQs);
			unAttempted.push(sectionData[i].unAttemptedQs);
		}
		var options = {
			maintainAspectRatio: false,
			scales: {
			  yAxes: [{
				// stacked: true,
				gridLines: {
				  display: true,
				  color: "rgba(255,99,132,0.2)"
				},
				ticks: {
					beginAtZero: true
				},
			  }],
			  xAxes: [{
				gridLines: {
				  display: false
				}
			  }]
			}
		  };
		new Chart(this.sectionBarChart.nativeElement, {
			type: 'bar',
			options : options,
			data: {
				labels: label,
				datasets: [
					{
					label: 'Your Score',
					data: score,
					backgroundColor: 'rgba(153, 102, 255, 0.2)',
					borderColor: 'rgba(153, 102, 255, 1)',
					borderWidth: 1
				},
				{
					label: 'Correct',
					data: correct,
					backgroundColor: 'rgba(75, 192, 192, 0.2)',
					borderColor:'rgba(75, 192, 192, 1)',
					borderWidth: 1
				},
				{
					label: 'InCorrect',
					data: inCorrect,
					backgroundColor: 'rgba(255, 99, 132, 0.2)',
					borderColor: 'rgba(255,99,132,1)',
					borderWidth: 1
				},
				{
					label: 'Attempted',
					data: attempted,
					backgroundColor: 'rgba(54, 162, 235, 0.2)',
					borderColor: 'rgba(54, 162, 235, 1)',
					borderWidth: 1
				},
				{
					label: 'Un Attempted',
					data: unAttempted,
					backgroundColor: 'rgba(255, 206, 86, 0.2)',
					borderColor:'rgba(255, 206, 86, 1)',
					borderWidth: 1
				}
			]
			},

			// options: {
			// 	scale: {
			// 		yAxes: [{
			// 			stacked: true
			// 		}]
			// 	},
			// 	spanGaps: false,
			// 	legend: {
			// 		display: true,
			// 		position: 'top',
			// 		labels: {
			// 			boxWidth: 80,
			// 			fontColor: 'black'
			// 		}
			// 	}
			// }
		});
	}
	compareBarChart(){
		let user = this.userReport;
		let topper = this.toppers[0];
		let label = ['T.Score','Correct','In Correct','Attempted','Un Attempted'];
        let userData = [user.stdScore,user.correctQs,user.inCorrectQs,(user.test.totalQs - user.unAttemptedQs),user.unAttemptedQs];
		let topperData = [topper.stdScore,topper.correctQs,topper.inCorrectQs,(user.test.totalQs - topper.unAttemptedQs),topper.unAttemptedQs];
		new Chart(this.compareChart.nativeElement, {
			type: 'bar',
			options : {
				maintainAspectRatio: false,
				scales: {
				  yAxes: [{
					// stacked: true,
					gridLines: {
					  display: true,
					  color: "rgba(255,99,132,0.2)"
					}
				  }],
				  xAxes: [{
					gridLines: {
					  display: false
					}
				  }]
				}
			  },
			data: {
				labels: label,
				datasets: [
					{
						label: 'Your Score',
						data: userData,
						backgroundColor: 'rgba(75, 192, 192, 0.2)',
						borderColor: 'rgba(75, 192, 192, 1)',
						borderWidth: 1
					},
					// {
					// 	label: 'Average',
					// 	data: average,
					// 	backgroundColor: 'rgba(255, 99, 132, 0.2)',
					// 	borderColor: 'rgba(255,99,132,1)',
					// 	borderWidth: 1
					// },
					{
						label: 'Topper',
						data: topperData,
						backgroundColor: 'rgba(255, 99, 132, 0.2)',
						borderColor: 'rgba(255,99,132,1)',
						borderWidth: 1
					}
				]
			}
		});
	}
	difficultyBarChart(){
		let label=[];
		let totalQs = [];
		let correct = [];
		let inCorrect = [];
		let attempted = [];
		let unAttempted = [];
		for(var i=0; i<this.diffLevelReport.length; i++){
			if(this.diffLevelReport[i].totalQs >0){
				label.push(this.diffLevelReport[i].title.substr(0,7) +'...');
				totalQs.push(this.diffLevelReport[i].totalQs);
				correct.push(this.diffLevelReport[i].response.correctQs);
				inCorrect.push(this.diffLevelReport[i].response.inCorrectQs);
				attempted.push(this.diffLevelReport[i].response.attemptedQs);
				unAttempted.push(this.diffLevelReport[i].totalQs - this.diffLevelReport[i].response.attemptedQs);
			}
		}
		new Chart(this.dLevelBarChart.nativeElement, {
			type: 'bar',
			options : {
				maintainAspectRatio: false,
				scales: {
				  yAxes: [{
					// stacked: true,
					gridLines: {
					  display: true,
					  color: "rgba(255,99,132,0.2)"
					}
				  }],
				  xAxes: [{
					gridLines: {
					  display: false
					}
				  }]
				}
			  },
			data: {
				labels: label,
				datasets: [
					{
					label: 'Total Question',
					data: totalQs,
					backgroundColor: 'rgba(153, 102, 255, 0.2)',
					borderColor: 'rgba(153, 102, 255, 1)',
					borderWidth: 1
				},
				{
					label: 'Correct',
					data: correct,
					backgroundColor: 'rgba(75, 192, 192, 0.2)',
					borderColor:'rgba(75, 192, 192, 1)',
					borderWidth: 1
				},
				{
					label: 'InCorrect',
					data: inCorrect,
					backgroundColor: 'rgba(255, 99, 132, 0.2)',
					borderColor: 'rgba(255,99,132,1)',
					borderWidth: 1
				},
				{
					label: 'Attempted',
					data: attempted,
					backgroundColor: 'rgba(54, 162, 235, 0.2)',
					borderColor: 'rgba(54, 162, 235, 1)',
					borderWidth: 1
				},
				{
					label: 'Un Attempted',
					data: unAttempted,
					backgroundColor: 'rgba(255, 206, 86, 0.2)',
					borderColor:'rgba(255, 206, 86, 1)',
					borderWidth: 1
				}
			]
			}
		});
	}
	paginate(e) {
        if (!this.paginateFilter) {
            this.paginateFilter = {};
        }
        // this.countBase = e.rows * e.page + 1;
        this.paginateFilter.page = (e.page + 1);
        this.paginateFilter.limit = e.rows;
        this.getTestParticipants();
	}
	getVideoURL(videoId,source){
		let url = '';
		if(source ==='youtube'){
			url = 'https://www.youtube.com/embed/' + videoId;
		}else if(source ==='vimeo'){
			url = 'https://player.vimeo.com/video/' + videoId;
		}
		return this.sanitizer.bypassSecurityTrustResourceUrl(url);
	}
	showSolutionVideosModal(){
		this.displaySolutionVideosModal = true;
		this.solutionVideoURL = this.getVideoURL(this.userReport.test.solVideos.ids[0],this.userReport.test.solVideos.source);
	}
	changeVideoURL(index,videoId,source){
		this.activeVideo = index;
		this.solutionVideoURL = this.getVideoURL(videoId,source);
	}
	ShowFeedbackModal(type,qData?:any){
		this.displayFeedbackModal=true;
		this.activeFbType = type;
		let patchValue : any ={
			type: type,
			user : this.userId,
			test : this.userReport.test.id,
			testSeries : this.userReport.testSeries,
			stream : this.userReport.test.stream,
		};
		if(qData){
			patchValue.question = qData.question._id;
			patchValue.qSeq = qData.questionSeqId;
		}
		this.feedbackForm.patchValue(patchValue);
		if(this.fbSubjects[type].length <1){
			this.loadFbSubjects();
		}
	}
	loadFbSubjects(){
		let filter={type: this.activeFbType,status : true,limit : 'all'};
		this.panelLoader = 'show';
		this.tmFeedbackService.getFbSubjects(filter,true).subscribe(
			res=>{
				this.fbSubjects[this.activeFbType]=res.body;
				this.panelLoader = 'none';
				console.log(this.fbSubjects);
			},
			err=>{
				this.notifier.alert(err.code,err.message,'danger',2000);
				this.panelLoader = 'none';
			}
		);
	}
	submitFeedback(){
		let formData=this.feedbackForm.value;
		this.panelLoader = 'show';
		this.tmFeedbackService.addFeedbackQs(formData).subscribe(
			res=>{
				this.panelLoader = 'none';
				this.displayFeedbackModal=false;
				this.feedbackForm.reset();
				this.notifier.alert('Success','Feedback Sent Successfully','success',2000);
			},
			err=>{
				this.notifier.alert(err.code,err.message,'danger',2000);
				this.panelLoader = 'none';
			}
		);
	}
	ShowTestAttemptsModal(){
		this.displayTestAttemptsModal = true;
		if(this.testAttempts.length <1){
			this.loadTestAttempts();
		}
	}
	viewPastReport(index){
		let data = this.testAttempts[index];

		this.userReport.uotAttempt = data.id;
		this.userReport.stdScore = data.stdScore;
		this.userReport.percentage = data.percentage;
		this.userReport.totalTimeTaken = this.getSecondsAsDigitalClock(data.totalTimeTaken);
		this.userReport.correctQs = data.correctQs;
		this.userReport.inCorrectQs = data.inCorrectQs;
		this.userReport.unAttemptedQs = data.unAttemptedQs;
		this.userReport.sectionScore = data.sectionScore;

		this.sections= JSON.parse(JSON.stringify(this.userReport.test.sections));
		this.solutionSections=JSON.parse(JSON.stringify(this.userReport.test.sections));
		this.displayOverviewChart(this.userReport,this.overviewChart);
		if(this.userReport.sectionScore) this.displaySectionChart(this.userReport.sectionScore,this.sectionChart);
		this.activeTab = 0;
		this.diffLevelReport=[];
		this.displayTestAttemptsModal = false;
		this.notifier.alert('Success','view Selected Past Attempted Report','success',2000);
	}
}


// datasets: [
// 	{
// 	label: 'Percentage',
// 	data: data,
// 	backgroundColor: [
// 		'rgba(255, 99, 132, 0.2)',
// 		'rgba(54, 162, 235, 0.2)',
// 		'rgba(255, 206, 86, 0.2)',
// 		'rgba(75, 192, 192, 0.2)',
// 		'rgba(153, 102, 255, 0.2)',
// 		'rgba(255, 159, 64, 0.2)',
// 		'rgba(255, 99, 132, 0.2)',
// 		'rgba(54, 162, 235, 0.2)',
// 		'rgba(255, 206, 86, 0.2)',
// 		'rgba(75, 192, 192, 0.2)',
// 		'rgba(153, 102, 255, 0.2)',
// 		'rgba(255, 159, 64, 0.2)'
// 	],
// 	borderColor: [
// 		'rgba(255,99,132,1)',
// 		'rgba(54, 162, 235, 1)',
// 		'rgba(255, 206, 86, 1)',
// 		'rgba(75, 192, 192, 1)',
// 		'rgba(153, 102, 255, 1)',
// 		'rgba(255, 159, 64, 1)',
// 		'rgba(255,99,132,1)',
// 		'rgba(54, 162, 235, 1)',
// 		'rgba(255, 206, 86, 1)',
// 		'rgba(75, 192, 192, 1)',
// 		'rgba(153, 102, 255, 1)',
// 		'rgba(255, 159, 64, 1)'
// 	],
