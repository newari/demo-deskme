import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { StudentTestMentHomeComponent } from './home';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { TestMentService } from '../../../../../lib/services/testment.service';
import { DialogModule } from 'primeng/dialog';
export const ROUTES:Routes=[
    {path: '', component: StudentTestMentHomeComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [StudentTestMentHomeComponent],
    imports:[
        CommonModule,
        DialogModule,
        RouterModule.forChild(ROUTES)
    ],
    providers:[TestMentService]
})
export class StudentTestMentHomeActivity { }
