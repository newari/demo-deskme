import {NgModule} from "@angular/core";

import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TestMentTestSeriesAppComponent } from './testseries';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { FilemanagerService } from '../../../../../lib/components/filemanager/filemanager.service';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { TestMentService } from "../../../../../lib/services/testment.service";
import { TestseriesService } from "../../../../../lib/services/testseries.service";
import { SafeHtmlPipeModule } from "../../../../../lib/filters/safehtml.pipe";
import { DialogModule } from 'primeng/dialog';
import { FieldsetModule } from 'primeng/fieldset';
export const ROUTES:Routes=[
    {path: '', component: TestMentTestSeriesAppComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];

@NgModule({
    declarations: [TestMentTestSeriesAppComponent],
    imports:[
        CommonModule,
        DialogModule,
        FormsModule,
        ReactiveFormsModule,
        FieldsetModule,
        RouterModule.forChild(ROUTES),
        SafeHtmlPipeModule
    ],
    providers: [FilemanagerService,TestMentService,TestseriesService]
})
export class StudentTestMentTestSeriesActivity { }
