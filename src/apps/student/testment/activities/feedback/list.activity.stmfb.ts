import {NgModule} from "@angular/core";
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { TestMentFeedbackListContent } from './list';
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { SafeHtmlPipeModule } from "../../../../../lib/filters/safehtml.pipe";
import { TmFeedbackService } from "../../../../../lib/services/tmfeedback.service";
import { TabViewModule } from 'primeng/tabview';

export const ROUTES:Routes=[
    {path: '', component: TestMentFeedbackListContent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Test'}},
];
@NgModule({
    declarations: [TestMentFeedbackListContent],
    imports:[
        CommonModule,
        RouterModule.forChild(ROUTES),SafeHtmlPipeModule,TabViewModule
    ],
    providers: [TmFeedbackService]
})
export class StudentTestMentFeedbackActivity { }
