import { Component } from '@angular/core';
import { AuthService } from '../../../../../lib/services/auth.service';
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { TestMentService } from '../../../../../lib/services/testment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EdukitConfig } from '../../../../../ezukit.config';
import { DomSanitizer } from '@angular/platform-browser';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { TmFeedbackService } from '../../../../../lib/services/tmfeedback.service';
import { TestseriesService } from '../../../../../lib/services/testseries.service';
@Component({
  templateUrl: './test.html'
})
export class TestMentTestContent{
	userTestSeries;
	feedbackForm : FormGroup;
	panelLoader ="none";
	activeProduct;
	activeTestSeries;
	courses:any;
	courseIds:string[]=[];
	sessionStd;
	testOpened = false;
	basicAPI;
	currentDate = new Date();
	preDisVideoURL : any;
	displayPreDisVideoModal : boolean =false;
	activeTest : any;
	activeVideo : number =0;
	displayFeedbackModal : boolean;
	fbSubjects : any;
	userProducts : any=[];
	pIndex : number=0;
	tsIndex : number=0;
	tIndex : number;
	testLoader = 'none';
	displayTestModal : boolean;
	test : any={}; 
    constructor(
      	private testmentService:TestMentService,
      	private authService: AuthService,
		private notifier: NotifierService,
		private router : Router,
		private activatedRoute: ActivatedRoute,
		private sanitizer : DomSanitizer,
		private tmFeedbackService : TmFeedbackService,
		private testseriesService : TestseriesService,
		private fb : FormBuilder
    ){}
    ngOnInit(){
		this.activatedRoute.queryParams.subscribe(params=>{
			this.activeProduct = params['product'],
			this.activeTestSeries = params['tsId']
		})
		this.feedbackForm= this.fb.group({
			type : ['',Validators.required],
			user : ['',Validators.required],
			subject :['',Validators.required],
			testSeries : ['',Validators.required],
			stream : ['',Validators.required],
			query : ['',Validators.required],
		});
		this.sessionStd=this.authService.student();
		// this.loadTests();
		this.loadUserProductTestSeries();
		this.basicAPI = EdukitConfig.BASICS.API_URL;
    }
	// loadTests(){
	// 	this.panelLoader ="show";
	// 	this.testmentService.getUserOnlineTests(this.sessionStd.id).subscribe(
	// 	  res=>{			
	// 			this.courseIds = [];
	// 			this.userTestSeries=[];				
	// 			this.userTestSeries=res;
	// 			this.courses={};
	// 			if(this.userTestSeries.length>0){
	// 				for(let ci=0; ci<res.length; ci++){
	// 					if(this.activeTestSeries == res[ci].tsDetail._id){
	// 						this.activeCourse = res[ci].course._id;
	// 						this.activeTestSeries = res[ci].tsDetail._id;	
	// 					}
	// 					if(!this.courses[res[ci].course._id]){
	// 						this.courses[res[ci].course._id]=res[ci].course;
	// 						this.courseIds.push(res[ci].course._id);
	// 					}
	// 				}
	// 				if(this.activeTestSeries == null){
	// 					this.activeCourse = this.courseIds[0];
	// 					this.activeTestSeries = this.userTestSeries[0].tsDetail._id;
	// 				}
	// 			}
	// 			this.testOpened=false;
	// 			this.panelLoader ="none";
	// 		},
	// 		err=>{
	// 			this.panelLoader ="none";
	// 			this.notifier.alert(err.code, err.message, "danger", 10000);
	// 		}
	// 	)
	// }
	loadUserProductTestSeries(){
		this.panelLoader = "show";
		this.testseriesService.getUserProductTestSeries().subscribe(
			res=>{							
				this.userProducts=res;
				if(this.userProducts.length>0){
					if(!this.activeProduct && this.activeTestSeries){
						for (let i = 0; i < this.userProducts.length; i++) {
							for (let j = 0; j < this.userProducts[i].testSeries.length; j++) {
								let ts = this.userProducts[i].testSeries[j];
								if(this.activeTestSeries == ts.id){
									this.pIndex = i;
									this.tsIndex = j;
									this.activeProduct = this.userProducts[i].id;
									this.loadTests();
									break;
								}
							}
						}
					}else if(this.activeProduct && this.activeTestSeries){
						for (let i = 0; i < this.userProducts.length; i++) {
							let up = this.userProducts[i];
							if(this.activeProduct == up.id){
								this.pIndex = i;
								this.activeProduct = up.id;
								for (let j = 0; j < up.testSeries.length; j++) {
									if(this.activeTestSeries == up.testSeries[j].id){
										this.tsIndex = j;
										this.loadTests();
										break;
									}
								}					
							}
						}
					}
					else{
						this.activeProduct = this.userProducts[0].id; 
						if(this.userProducts[0].testSeries && this.userProducts[0].testSeries[0].id){
							this.activeTestSeries = this.userProducts[0].testSeries[0].id;
							this.loadTests();
						} 
					}
				}
				this.panelLoader = "none";
			},
			err=>{
				this.panelLoader = "none";
				// this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	loadTests(){
		let filter={
			product : this.activeProduct,
			testSeries : this.activeTestSeries
		};
		this.testLoader = "show";
		this.testseriesService.getTsProductTest(filter).subscribe(
			res=>{						
				this.userProducts[this.pIndex].testSeries[this.tsIndex].tests=[];	
				this.userProducts[this.pIndex].testSeries[this.tsIndex].tests = res;  
				this.testOpened=false;
				this.tIndex = null;
				this.testLoader = "none";
			},
			err=>{
				this.testLoader = "none";
				// this.notifier.alert(err.code, err.message, "danger", 10000);
			}
		)
	}
	// reloadTest(p,ts,t){
	// 	let filter={
	// 		product :this.userProducts[p].id,
	// 		testSeries : this.userProducts[p].testSeries[ts].id,
	// 		test : this.userProducts[p].testSeries[ts].tests[t].id
	// 	};
	// 	this.testLoader = "show";
	// 	this.testseriesService.getTsProductTest(filter).subscribe(
	// 		res=>{						
	// 			this.userProducts[p].testSeries[ts].tests[t];	
	// 			// this.userProducts[this.pIndex].testSeries[this.tsIndex].tests = res;  
	// 			this.testOpened=false;
	// 			this.tIndex = null;
	// 			this.testLoader = "none";
	// 		},
	// 		err=>{
	// 			this.testLoader = "none";
	// 			// this.notifier.alert(err.code, err.message, "danger", 10000);
	// 		}
	// 	)
	// }
	viewTest(test){
		this.displayTestModal = true;
		this.test=test;
	}
	testWindow(product,ts,test,tIndex) {	
		let view='w';
        if(screen.width < 672){
            view='m';
		}
		let url = this.basicAPI +'/student/testment/testwindow2?view='+view+'&product='+product+'&testSeries='+ts+'&test='+test;
		url =url.toString();
		let params  = 'width='+screen.width+',height='+screen.height+',top=0,left=0,fullscreen=yes,resizable=0';
		let newWindow=window.open(url,'EXAM',params);
		this.testOpened = true;
		// this.tIndex = tIndex;
		if (window.focus) {newWindow.focus()}
		return false;
	}
	setQueryParams(pIndex,tsIndex,product,ts){
		this.pIndex = pIndex;
		this.tsIndex = tsIndex;
		this.activeTestSeries= ts;
		this.activeProduct = product;
		if(!this.userProducts[this.pIndex].testSeries[this.tsIndex].tests){
			this.loadTests();
		}
		this.router.navigate(['/student/testment/test-plus/'], { queryParams: {product : this.activeProduct,tsId: this.activeTestSeries}});
	}
	// showPreDisVideoModal(url){
	// 	this.displayPreDisVideoModal = true;
	// 	this.preDisVideoURL=this.sanitizer.bypassSecurityTrustResourceUrl(url);
	// }
	getVideoURL(videoId,source){
		let url = '';
		if(source ==='youtube'){
			url = 'https://www.youtube.com/embed/' + videoId;
		}else if(source ==='vimeo'){
			url = 'https://player.vimeo.com/video/' + videoId;
		}
		return this.sanitizer.bypassSecurityTrustResourceUrl(url);
	}
	showPreDisVideosModal(data){
		this.displayPreDisVideoModal = true;
		this.activeTest = data;
		this.preDisVideoURL= this.getVideoURL(this.activeTest.test.preDiscussionVideos.ids[0],this.activeTest.test.preDiscussionVideos.source);
	}
	changeVideoURL(index,videoId,source){
		this.activeVideo = index;
		this.preDisVideoURL = this.getVideoURL(videoId,source);
	}
	ShowFeedbackModal(testSeries){
		this.displayFeedbackModal=true;
		let patchValue : any ={
			type: 'TEST_SERIES',
			user : this.sessionStd.id,
			testSeries : testSeries._id,
			stream : testSeries.stream,
		};
		this.feedbackForm.patchValue(patchValue);
		if(!this.fbSubjects){
			this.loadFbSubjects();
		}
	}
	loadFbSubjects(){
		let filter={type: 'TEST_SERIES',status : true,limit : 'all'};
		this.panelLoader = 'show';
		this.tmFeedbackService.getFbSubjects(filter,true).subscribe(
			res=>{
				this.fbSubjects=res.body;
				this.panelLoader = 'none';
			},
			err=>{
				this.notifier.alert(err.code,err.message,'danger',2000);
				this.panelLoader = 'none';
			}
		);
	}
	submitFeedback(){
		let formData=this.feedbackForm.value;
		this.panelLoader = 'show';
		this.tmFeedbackService.addFeedbackQs(formData).subscribe(
			res=>{
				this.panelLoader = 'none';
				this.displayFeedbackModal=false;
				this.feedbackForm.reset();
				this.notifier.alert('Success','Feedback Sent Successfully','success',2000);
			},
			err=>{
				this.notifier.alert(err.code,err.message,'danger',2000);
				this.panelLoader = 'none';
			}
		);
	}
}
