import { Routes, CanActivateChild } from '@angular/router';

import { TestMentAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  { path: '',
    component:TestMentAppComponent,
    children: [
    //   { path: '', loadChildren: () => import('./activities/home/home.activity.stmh').then(m => m.StudentTestMentHomeActivity) },
      { path: '', loadChildren: () => import('./activities/testseries/testseries.activity.stmts').then(m => m.StudentTestMentTestSeriesActivity) },
      // { path: '', loadChildren: () => import('modulefile').then(m => m.StudentForumQuestionActivity) './activities/testseries/testseries.activity.stmts#StudentTestMentTestSeriesActivity' },
      { path: 'dashboard', loadChildren: () => import('./activities/home/home.activity.stmh').then(m => m.StudentTestMentHomeActivity) },
      { path: 'test', loadChildren: () => import('./activities/test/test.activity.stmt').then(m => m.StudentTestMentTestActivity) },
      { path: 'report', loadChildren: () => import('./activities/report/report.activity.stmr').then(m => m.StudentTestMentReportActivity) },
      { path: 'overview', loadChildren: () => import('./activities/report/overview.activity.stmr').then(m => m.StudentTestMentOverViewActivity) },
      { path: 'bookmark', loadChildren: () => import('./activities/bookmark/questions.activity.stmq').then(m => m.StudentTestMentBookmarkActivity) },
      { path: 'testseries', loadChildren: () => import('./activities/testseries/testseries.activity.stmts').then(m => m.StudentTestMentTestSeriesActivity) },
      { path: 'feedback', loadChildren: () => import('./activities/feedback/list.activity.stmfb').then(m => m.StudentTestMentFeedbackActivity) },
      { path: 'product/:productId', loadChildren: () => import('./activities/product/product.activity.stmp').then(m => m.StudentTestMentProductActivity) },
      { path: 'test-plus', loadChildren: () => import('./activities/test2/test.activity.stmt').then(m => m.StudentTestMentTestActivity) },
      { path: 'testseries-plus', loadChildren: () => import('./activities/testseries2/testseries.activity.stmts').then(m => m.StudentTestMentTestSeriesActivity) },
    ]
  }
]
