import { Routes, CanActivateChild } from '@angular/router';

import { BatchFeedAppComponent } from './app.component';

export const APP_ROUTES:Routes=[
  { path: '',
    component:BatchFeedAppComponent,
    children: [
      { path: '', loadChildren: () => import('./activities/home/home.activity.sbfh').then(m => m.BatchFeedDashBoard) },
      {
        path:'articles', loadChildren:() => import('./activities/articles/list.sbfa').then(m => m.StudentArticleListActivity)
      },
      {
        path:'notes', loadChildren:() => import('./activities/notes/list.sbfn').then(m => m.StudentNotesListActivity)
      },
      {
        path:'assignments', loadChildren:() => import('./activities/assignment/list.sbfas').then(m => m.StudentAssignmentListActivity)
      },

      {
        path:'quiz', loadChildren:() => import('./activities/quiz/list.sbfq').then(m => m.StudentQuizListActivity)
      }
    ]
  }
]
