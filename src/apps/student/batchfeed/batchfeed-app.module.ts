import {NgModule} from "@angular/core";
import {RouterModule} from "@angular/router";

import {APP_ROUTES} from "./app.routes";
import { BatchFeedAppComponent } from './app.component';
import { StudentPanelService } from "../shared/services/student-panel.service";

@NgModule({
    declarations: [BatchFeedAppComponent],
    imports: [RouterModule.forChild(APP_ROUTES)],
    providers:[StudentPanelService]
})
export class StudentBatchFeedAppModule { }
