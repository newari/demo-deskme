import { Component, OnInit } from "@angular/core";
import { StudentPanelService } from "../../../shared/services/student-panel.service";
import { NotifierService } from "../../../../../lib/components/notifier/notifier.service";
import { AuthService } from "../../../../../lib/services/auth.service";
import { QuizService } from "../../../../../lib/services/quiz.service";
import { EdukitConfig } from "../../../../../ezukit.config";


@Component({
    selector:'assignment-list',
    templateUrl:'./list.html'
})

export class StudentQuizListComponent implements OnInit{
    quizList:any=[];
    student:any;
    displayArticleDetail: boolean;
    selectedQuiz: any;
    answerUploadModal: boolean;
    panelLoader: string;
    activeIndex: number;
    basicAPI: any;
    quizOpened: boolean;
    activeBatch: any;

    constructor(
        private studentService: StudentPanelService,
        private notifier: NotifierService,
        private authService:AuthService ,
        private quizService:QuizService
    ){}
    setActiveBatch(index?:number){
        this.activeBatch=this.student.batches[index];
        this.getBatchQuiz();   
    }
    ngOnInit(){
        this.student= this.authService.studentInfo();
        this.basicAPI = EdukitConfig.BASICS.API_URL;
        if(!this.student){
            return;
        }
        if(!this.student.batches||(this.student.batches&&this.student.batches.length<1))
        return;
        this.activeIndex=0;
        this.activeBatch=this.student.batches[0];
        this.getBatchQuiz()
    }
    getBatchQuiz(filter?:any){
        this.panelLoader='show';
        this.studentService.getBatchQuizs(this.activeBatch.batch.id,filter,true).subscribe(
            res=>{
                this.quizList=res.body;
                this.panelLoader="none"
            },
            err=>{
                this.panelLoader="none";
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }


    getQuiz() {
        let batches:any= this.student.batches.map(
            (batch)=>{
                return batch.batch.id
            }
        )
        // this.studentService.getBatchFeed({batch:batches,contentType:"QUIZ"}).subscribe(
        //     res=>{
        //         this.quiz=res;
        //     },
        //     err=>{
        //         this.notifier.alert(err.code, err.message, "danger", 5000);
        //     }
        // );

        this.quizService.getUserQuizs().subscribe(
            res=>{
                this.quizList=res
            },
            err=>{
                this.notifier.alert(err.code, err.message, "danger", 5000);
            }
        );
    }
    showDetails(i){
        this.activeIndex=i;
        // this.displayArticleDetail=true;
        this.selectedQuiz=this.quizList[i];
    }

    quizWindow(quiz) {	
        let user= this.authService.student();
		let url = this.basicAPI +'/student/quiz/quizwindow/'+quiz+'?user='+user.id;
		let params  = 'width='+screen.width+',height='+screen.height+',top=0,left=0,fullscreen=yes,resizable=0';
		let newWindow=window.open(url,'EXAM',params);
		this.quizOpened = true;
		if (window.focus) {newWindow.focus()}
		return false;
	}

}