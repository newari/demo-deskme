import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StudentArtcleListComponent } from "./list";
import { Routes, RouterModule } from "@angular/router";
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { FormsModule } from "@angular/forms";
import { SidebarModule } from 'primeng/sidebar';

export const ROUTES:Routes=[
    {path: '', component: StudentArtcleListComponent, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Articles'}},
];
@NgModule({
    declarations:[StudentArtcleListComponent],
    imports:[CommonModule,FormsModule, SidebarModule, RouterModule.forChild(ROUTES)],
    exports:[],
})

export class StudentArticleListActivity{}
