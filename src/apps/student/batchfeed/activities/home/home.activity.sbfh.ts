import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { BatchFeedHome } from "./home";
import { StudentAuthGuard } from "../../../shared/services/student-auth-guard";
import { Routes, RouterModule } from "@angular/router";

export const ROUTES:Routes=[
    {path: '', component: BatchFeedHome, canActivate: [StudentAuthGuard], pathMatch:'full', data: {title: 'Dashboard'}},
];
@NgModule({
    declarations:[BatchFeedHome],
    imports:[CommonModule,
        RouterModule.forChild(ROUTES),
    ]
})

export class BatchFeedDashBoard{}