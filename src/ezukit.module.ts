import {NgModule} from "@angular/core";
import { CommonModule } from '@angular/common';
import {BrowserModule} from "@angular/platform-browser";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {RouterModule} from "@angular/router";

import {EzukitComponent} from "./ezukit.component";
import {rootRouterConfig} from "./ezukit.router";
import { AuthGuard } from './lib/services/auth-guard.service';
import { AuthService } from './lib/services/auth.service';
// import { FacultyAuthGuard } from './lib/services/faculty-auth-guard';
// import { StaffAuthGuard } from './lib/services/staff-auth-guard';
import { EzukitAppService } from './lib/services/ezukit-app.service';
// import { StudentAuthGuard } from './lib/services/student-auth-guard';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { AuthInterceptor } from "./lib/services/auth.interceptor";
import { NotifierService } from './lib/components/notifier/notifier.service';

@NgModule({
    declarations: [
        EzukitComponent,
    ],
    imports: [ HttpClientModule, BrowserModule, BrowserAnimationsModule, CommonModule, RouterModule.forRoot(rootRouterConfig)],
    providers:    [HttpClient,
        {
			provide: HTTP_INTERCEPTORS,
			useClass: AuthInterceptor,
			multi: true
        },EzukitAppService, AuthGuard, AuthService, NotifierService ],
    bootstrap: [EzukitComponent],

})
export class EzukitModule { }
